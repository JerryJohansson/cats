
ALTER TABLE Tblincident_Details	
	ADD "SIGNOFF_PERSON_ID_INC_4"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "FORWARD_PERSON_ID_INC_4"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "SIGNOFF_DATE_INC_4" 				 TIMESTAMP (6);
ALTER TABLE Tblincident_Details	
	ADD "MESSAGE_INC_4" 					 VARCHAR2(2000 BYTE);
Alter Table Tblincident_Details	
	ADD "STATUS_INC_4"	 					 VARCHAR2(10 BYTE);
	

ALTER TABLE Tblincident_Details	
	ADD "SIGNOFF_PERSON_ID_INC_5"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "FORWARD_PERSON_ID_INC_5"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "SIGNOFF_DATE_INC_5" 				 TIMESTAMP (6);
ALTER TABLE Tblincident_Details	
	ADD "MESSAGE_INC_5" 					 VARCHAR2(2000 BYTE);
Alter Table Tblincident_Details	
	ADD "STATUS_INC_5"	 					 VARCHAR2(10 BYTE);	
	
	
ALTER TABLE Tblincident_Details	
	ADD "SIGNOFF_PERSON_ID_INC_6"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "FORWARD_PERSON_ID_INC_6"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "SIGNOFF_DATE_INC_6" 				 TIMESTAMP (6);
ALTER TABLE Tblincident_Details	
	ADD "MESSAGE_INC_6" 					 VARCHAR2(2000 BYTE);
Alter Table Tblincident_Details	
	ADD "STATUS_INC_6"	 					 VARCHAR2(10 BYTE);		