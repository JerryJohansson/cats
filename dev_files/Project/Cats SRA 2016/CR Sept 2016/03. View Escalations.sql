Create Or Replace Force View "CATSDBA"."VIEW_ESCALATION_BASE" (
	"INCIDENT_NUMBER","INCIDENT_TITLE","INC_LAST_AUDIT_DATE","INV_LAST_AUDIT_DATE",
    "CURRENT_SUPERINTENDENT_ID", "ESCALATING_SUPERINTENDENT_ID", 
	"INCDISPLYID","INCIDENTSTATUS",
	"SIGNOFF_PERSON_ID_1","FORWARD_PERSON_ID_1","SIGNOFF_DATE_1","STATUS_1",
	"SIGNOFF_PERSON_ID_2","FORWARD_PERSON_ID_2","SIGNOFF_DATE_2","STATUS_2",
	"SIGNOFF_PERSON_ID_3","FORWARD_PERSON_ID_3","SIGNOFF_DATE_3","STATUS_3",
	"SIGNOFF_PERSON_ID_INC_4","FORWARD_PERSON_ID_INC_4","SIGNOFF_DATE_INC_4","STATUS_INC_4",
	"SIGNOFF_PERSON_ID_INC_5","FORWARD_PERSON_ID_INC_5","SIGNOFF_DATE_INC_5","STATUS_INC_5",
	"SIGNOFF_PERSON_ID_INC_6","FORWARD_PERSON_ID_INC_6","SIGNOFF_DATE_INC_6","STATUS_INC_6",
	"SIGNOFF_PERSON_ID_4","FORWARD_PERSON_ID_4","SIGNOFF_DATE_4","STATUS_4",
	"SIGNOFF_PERSON_ID_5","FORWARD_PERSON_ID_5","SIGNOFF_DATE_5","STATUS_5",
	"SIGNOFF_PERSON_ID_6","FORWARD_PERSON_ID_6","SIGNOFF_DATE_6","STATUS_6",
	"INVESTIGATION_TEAM_PERSONID",
	"CURRENT_SIGNOFF_EMAIL_1","CURRENT_SIGNOFF_EMAIL_2","CURRENT_SIGNOFF_EMAIL_3","CURRENT_SIGNOFF_EMAIL_INC_4","CURRENT_SIGNOFF_EMAIL_INC_5","CURRENT_SIGNOFF_EMAIL_INC_6",
	"CURRENT_SIGNOFF_EMAIL_4","CURRENT_SIGNOFF_EMAIL_5","CURRENT_SIGNOFF_EMAIL_6",
	"CURRENT_FORWARD_EMAIL_1","CURRENT_FORWARD_EMAIL_2","CURRENT_FORWARD_EMAIL_3","CURRENT_FORWARD_EMAIL_INC_4","CURRENT_FORWARD_EMAIL_INC_5","CURRENT_FORWARD_EMAIL_INC_6",
	"CURRENT_FORWARD_EMAIL_4","CURRENT_FORWARD_EMAIL_5","CURRENT_FORWARD_EMAIL_6",
	"CURRENT_SUPERINTENDENT_EMAIL","CURRENT_INV_TEAM_LEAD_EMAIL",
	"ESCALATION_EMAIL_SIGNOFF1","ESCALATION_EMAIL_SIGNOFF2","ESCALATION_EMAIL_SIGNOFF3","ESCALATION_EMAIL_SIGNOFF_INC4","ESCALATION_EMAIL_SIGNOFF_INC5","ESCALATION_EMAIL_SIGNOFF_INC6",
	"ESCALATION_EMAIL_SIGNOFF4","ESCALATION_EMAIL_SIGNOFF5","ESCALATION_EMAIL_SIGNOFF6",
	"ESCALATION_EMAIL_FORWARD1","ESCALATION_EMAIL_FORWARD2","ESCALATION_EMAIL_FORWARD3","ESCALATION_EMAIL_FORWARD_INC4","ESCALATION_EMAIL_FORWARD_INC5","ESCALATION_EMAIL_FORWARD_INC6",
	"ESCALATION_EMAIL_FORWARD4","ESCALATION_EMAIL_FORWARD5","ESCALATION_EMAIL_FORWARD6",
	"ESCALATION_EMPNUM_SIGNOFF1","ESCALATION_EMPNUM_SIGNOFF2","ESCALATION_EMPNUM_SIGNOFF3","ESCALATION_EMPNUM_SIGNOFF_INC4","ESCALATION_EMPNUM_SIGNOFF_INC5","ESCALATION_EMPNUM_SIGNOFF_INC6",
	"ESCALATION_EMPNUM_SIGNOFF4","ESCALATION_EMPNUM_SIGNOFF5","ESCALATION_EMPNUM_SIGNOFF6",
	"ESCALATION_EMPNUM_FORWARD1","ESCALATION_EMPNUM_FORWARD2","ESCALATION_EMPNUM_FORWARD3","ESCALATION_EMPNUM_FORWARD_INC4","ESCALATION_EMPNUM_FORWARD_INC5","ESCALATION_EMPNUM_FORWARD_INC6",
	"ESCALATION_EMPNUM_FORWARD4","ESCALATION_EMPNUM_FORWARD5","ESCALATION_EMPNUM_FORWARD6",
	"ESCALATION_EMAIL_SUPER","ESCALATION_EMAIL_INV_TEAMLEAD","ESCALATION_EMPNUM_INV_TEAMLEAD","INVESTIGATION_NUMBER"

  )
  AS(
 Select I.Incident_Number, I.Incident_Title, 
      Get_Last_Auditdate_Inc(I.Incident_Number) As Inc_Last_Audit_Date,
      (Get_Last_Auditdate_Inv(Inv.Investigation_Number)) As Inv_Last_Audit_Date,
      I.Superintendent_Id, E7.Related_Superintendent, I.Incdisplyid, I.Incidentstatus, 
      I.Signoff_Person_Id_1, I.Forward_Person_Id_1, I.Signoff_Date_1, I.Status_1, 
      I.Signoff_Person_Id_2, I.Forward_Person_Id_2, I.Signoff_Date_2, I.Status_2,
      I.Signoff_Person_Id_3, I.Forward_Person_Id_3, I.Signoff_Date_3, I.Status_3,

      I.Signoff_Person_Id_inc_4, I.Forward_Person_Id_inc_4, I.Signoff_Date_inc_4, I.Status_inc_4, --new
      I.Signoff_Person_Id_inc_5, I.Forward_Person_Id_inc_5, I.Signoff_Date_inc_5, I.Status_inc_5,
      I.Signoff_Person_Id_inc_6, I.Forward_Person_Id_inc_6, I.Signoff_Date_inc_6, I.Status_inc_6,

      
      Inv.Signoff_Person_Id_4, Inv.Forward_Person_Id_4, Inv.Signoff_Date_4, Inv.Status_4, 
      Inv.Signoff_Person_Id_5, Inv.Forward_Person_Id_5, Inv.Signoff_Date_5, Inv.Status_5,
      Inv.Signoff_Person_Id_6, Inv.Forward_Person_Id_6, Inv.Signoff_Date_6, Inv.Status_6,
      I.Investigation_Team_Personid,
      E1.Email_Address As Current_Signoff_Email_1,
      E2.Email_Address As Current_Signoff_Email_2,
      E3.Email_Address As Current_Signoff_Email_3,
      
      E15.Email_Address As Current_Signoff_Email_Inc_4, -- new
      E16.Email_Address As Current_Signoff_Email_Inc_5,
      E17.Email_Address As Current_Signoff_Email_Inc_6,
      
      E4.Email_Address As Current_Signoff_Email_4,
      E5.Email_Address As Current_Signoff_Email_5,
      E6.Email_Address As Current_Signoff_Email_6,
	  
	  E9.Email_Address As Current_Forward_Email_1,
	  E10.Email_Address As Current_Forward_Email_2,
	  E11.Email_Address As Current_Forward_Email_3,
	  E15.Email_Address As Current_Forward_Email_inc_4, --new
	  E16.Email_Address As Current_Forward_Email_inc_5, --new
	  E17.Email_Address As Current_Forward_Email_inc_6, --new
    
	  E12.Email_Address As Current_Forward_Email_4,
	  E13.Email_Address As Current_Forward_Email_5,
	  E14.Email_Address As Current_Forward_Email_6,

	  
      E7.Email_Address As Current_Superintendent_Email,
      E8.Email_Address As Current_Inv_Team_Lead_Email,
      
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E1.Related_Superintendent) As Escalation_Email_Signoff1,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E2.Related_Superintendent) As Escalation_Email_Signoff2,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E3.Related_Superintendent) As Escalation_Email_Signoff3,
      
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E15.Related_Superintendent) As Escalation_Email_Signoff_inc4, --new
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E16.Related_Superintendent) As Escalation_Email_Signoff_inc5,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E17.Related_Superintendent) As Escalation_Email_Signoff_inc6,
      
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E4.Related_Superintendent) As Escalation_Email_Signoff4,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E5.Related_Superintendent) As Escalation_Email_Signoff5,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E6.Related_Superintendent) As Escalation_Email_Signoff6,

      (Select Email_Address From Tblemployee_Details Where Employee_Number = E9.Related_Superintendent) As Escalation_Email_Forward1, 
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E10.Related_Superintendent) As Escalation_Email_Forward2,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E11.Related_Superintendent) As Escalation_Email_Forward3,

      (Select Email_Address From Tblemployee_Details Where Employee_Number = E18.Related_Superintendent) As Escalation_Email_Forward_inc4, --new
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E19.Related_Superintendent) As Escalation_Email_Forward_inc5,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E20.Related_Superintendent) As Escalation_Email_Forward_inc6,

      (Select Email_Address From Tblemployee_Details Where Employee_Number = E12.Related_Superintendent) As Escalation_Email_Forward4,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E13.Related_Superintendent) As Escalation_Email_Forward5,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E14.Related_Superintendent) As Escalation_Email_Forward6,
	  
	  E1.Related_Superintendent As Escalation_EmpNum_Signoff1,
	  E2.Related_Superintendent As Escalation_EmpNum_Signoff2,
	  E3.Related_Superintendent As Escalation_EmpNum_Signoff3,
    E15.Related_Superintendent As Escalation_EmpNum_Signoff_inc4, -- new signoff
    E16.Related_Superintendent As Escalation_EmpNum_Signoff_inc5,
    E17.Related_Superintendent As Escalation_EmpNum_Signoff_inc6,
    
	  E4.Related_Superintendent As Escalation_EmpNum_Signoff4,
	  E5.Related_Superintendent As Escalation_EmpNum_Signoff5,
	  E6.Related_Superintendent As Escalation_EmpNum_Signoff6,
    
	  
	  E9.Related_Superintendent As Escalation_EmpNum_Forward1,
	  E10.Related_Superintendent As Escalation_EmpNum_Forward2,
	  E11.Related_Superintendent As Escalation_EmpNum_Forward3,

--todo tomorrow [ here ]    
	  E18.Related_Superintendent As Escalation_EmpNum_Forward_inc4, -- new forward super
	  E19.Related_Superintendent As Escalation_EmpNum_Forward_inc5,
	  E20.Related_Superintendent As Escalation_EmpNum_Forward_inc6,

    
	  E12.Related_Superintendent As Escalation_EmpNum_Forward4,
	  E13.Related_Superintendent As Escalation_EmpNum_Forward5,
	  E14.Related_Superintendent As Escalation_EmpNum_Forward6,
	  
	  
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E7.Related_Superintendent) As Escalation_Email_Super,
      (Select Email_Address From Tblemployee_Details Where Employee_Number = E8.Related_Superintendent) As Escalation_Email_Inv_TeamLead,
	  E8.Related_Superintendent as ESCALATION_EMPNUM_INV_TEAMLEAD,
	  nvl(Inv.Investigation_number, 0) as Investigation_number
      
      From Tblincident_Details I,
            Tblinvestigation_Details Inv,
          TBLEMPLOYEE_DETAILS E1,
          TBLEMPLOYEE_DETAILS E2,
          TBLEMPLOYEE_DETAILS E3,
          Tblemployee_Details E4,
          Tblemployee_Details E5,
          Tblemployee_Details E6,
          Tblemployee_Details E7,
          Tblemployee_Details E8,
		  
          Tblemployee_Details E9,
          Tblemployee_Details E10,
          Tblemployee_Details E11,
          Tblemployee_Details E12,
          Tblemployee_Details E13,
          Tblemployee_Details E14,

          Tblemployee_Details E15, --new signoffs
          Tblemployee_Details E16,
          Tblemployee_Details E17,

          Tblemployee_Details E18, --new forwards
          Tblemployee_Details E19,
          Tblemployee_Details E20
	  
		  
      Where I.Incident_Number = Inv.Incident_Number (+) 
        AND I.SIGNOFF_PERSON_ID_1     	= E1.EMPLOYEE_NUMBER (+)
        And I.Signoff_Person_Id_2     	= E2.Employee_Number (+)
        And I.Signoff_Person_Id_3     	= E3.Employee_Number (+)
        And Inv.Signoff_Person_Id_4     = E4.Employee_Number (+)
        And Inv.Signoff_Person_Id_5     = E5.Employee_Number (+)
        And Inv.Signoff_Person_Id_6     = E6.Employee_Number (+)
        And I.Superintendent_Id         = E7.Employee_Number(+)
        And I.Investigation_team_personid = E8.Employee_Number(+)
		
		And I.Forward_Person_Id_1     = E9.Employee_Number (+)
		And I.Forward_Person_Id_2     = E10.Employee_Number (+)
		And I.Forward_Person_Id_3     = E11.Employee_Number (+)
		And Inv.Forward_Person_Id_4     = E12.Employee_Number (+)
		And Inv.Forward_Person_Id_5     = E13.Employee_Number (+)
		And Inv.Forward_Person_Id_6     = E14.Employee_Number (+)

		
    AND I.SIGNOFF_PERSON_ID_inc_4     	= E15.EMPLOYEE_NUMBER (+) -- new signoffs
    And I.Signoff_Person_Id_inc_5     	= E16.Employee_Number (+)
    And I.Signoff_Person_Id_inc_6     	= E17.Employee_Number (+)
    And I.Forward_Person_Id_inc_4     = E18.Employee_Number (+) --new forwards
		And I.Forward_Person_Id_inc_5     = E19.Employee_Number (+)
		And I.Forward_Person_Id_inc_6     = E20.Employee_Number (+)

        
      And Incidentstatus = 'Open'
);    
	  
	  
	  
--
-- UC 2 If the users for the open approvals have been set and it has not been updated within 14 days, 
-- the last allocated user in the approval workflow step is changed to his/hers manager
--
Create Or Replace Force View "CATSDBA"."VIEW_ESCALATION_UC2_4" (

	"INCIDENT_NUMBER","INCIDENT_TITLE","INC_LAST_AUDIT_DATE","INV_LAST_AUDIT_DATE",
    "CURRENT_SUPERINTENDENT_ID", "ESCALATING_SUPERINTENDENT_ID", 
	"INCDISPLYID","INCIDENTSTATUS",
	"SIGNOFF_PERSON_ID_1","FORWARD_PERSON_ID_1","SIGNOFF_DATE_1","STATUS_1",
	"SIGNOFF_PERSON_ID_2","FORWARD_PERSON_ID_2","SIGNOFF_DATE_2","STATUS_2",
	"SIGNOFF_PERSON_ID_3","FORWARD_PERSON_ID_3","SIGNOFF_DATE_3","STATUS_3",
	"SIGNOFF_PERSON_ID_INC_4","FORWARD_PERSON_ID_INC_4","SIGNOFF_DATE_INC_4","STATUS_INC_4",
	"SIGNOFF_PERSON_ID_INC_5","FORWARD_PERSON_ID_INC_5","SIGNOFF_DATE_INC_5","STATUS_INC_5",
	"SIGNOFF_PERSON_ID_INC_6","FORWARD_PERSON_ID_INC_6","SIGNOFF_DATE_INC_6","STATUS_INC_6",
	"SIGNOFF_PERSON_ID_4","FORWARD_PERSON_ID_4","SIGNOFF_DATE_4","STATUS_4",
	"SIGNOFF_PERSON_ID_5","FORWARD_PERSON_ID_5","SIGNOFF_DATE_5","STATUS_5",
	"SIGNOFF_PERSON_ID_6","FORWARD_PERSON_ID_6","SIGNOFF_DATE_6","STATUS_6",
	"INVESTIGATION_TEAM_PERSONID",
	"CURRENT_SIGNOFF_EMAIL_1","CURRENT_SIGNOFF_EMAIL_2","CURRENT_SIGNOFF_EMAIL_3","CURRENT_SIGNOFF_EMAIL_INC_4","CURRENT_SIGNOFF_EMAIL_INC_5","CURRENT_SIGNOFF_EMAIL_INC_6",
	"CURRENT_SIGNOFF_EMAIL_4","CURRENT_SIGNOFF_EMAIL_5","CURRENT_SIGNOFF_EMAIL_6",
	"CURRENT_FORWARD_EMAIL_1","CURRENT_FORWARD_EMAIL_2","CURRENT_FORWARD_EMAIL_3","CURRENT_FORWARD_EMAIL_INC_4","CURRENT_FORWARD_EMAIL_INC_5","CURRENT_FORWARD_EMAIL_INC_6",
	"CURRENT_FORWARD_EMAIL_4","CURRENT_FORWARD_EMAIL_5","CURRENT_FORWARD_EMAIL_6",
	"CURRENT_SUPERINTENDENT_EMAIL","CURRENT_INV_TEAM_LEAD_EMAIL",
	"ESCALATION_EMAIL_SIGNOFF1","ESCALATION_EMAIL_SIGNOFF2","ESCALATION_EMAIL_SIGNOFF3","ESCALATION_EMAIL_SIGNOFF_INC4","ESCALATION_EMAIL_SIGNOFF_INC5","ESCALATION_EMAIL_SIGNOFF_INC6",
	"ESCALATION_EMAIL_SIGNOFF4","ESCALATION_EMAIL_SIGNOFF5","ESCALATION_EMAIL_SIGNOFF6",
	"ESCALATION_EMAIL_FORWARD1","ESCALATION_EMAIL_FORWARD2","ESCALATION_EMAIL_FORWARD3","ESCALATION_EMAIL_FORWARD_INC4","ESCALATION_EMAIL_FORWARD_INC5","ESCALATION_EMAIL_FORWARD_INC6",
	"ESCALATION_EMAIL_FORWARD4","ESCALATION_EMAIL_FORWARD5","ESCALATION_EMAIL_FORWARD6",
	"ESCALATION_EMPNUM_SIGNOFF1","ESCALATION_EMPNUM_SIGNOFF2","ESCALATION_EMPNUM_SIGNOFF3","ESCALATION_EMPNUM_SIGNOFF_INC4","ESCALATION_EMPNUM_SIGNOFF_INC5","ESCALATION_EMPNUM_SIGNOFF_INC6",
	"ESCALATION_EMPNUM_SIGNOFF4","ESCALATION_EMPNUM_SIGNOFF5","ESCALATION_EMPNUM_SIGNOFF6",
	"ESCALATION_EMPNUM_FORWARD1","ESCALATION_EMPNUM_FORWARD2","ESCALATION_EMPNUM_FORWARD3","ESCALATION_EMPNUM_FORWARD_INC4","ESCALATION_EMPNUM_FORWARD_INC5","ESCALATION_EMPNUM_FORWARD_INC6",
	"ESCALATION_EMPNUM_FORWARD4","ESCALATION_EMPNUM_FORWARD5","ESCALATION_EMPNUM_FORWARD6",
	"ESCALATION_EMAIL_SUPER","ESCALATION_EMAIL_INV_TEAMLEAD","ESCALATION_EMPNUM_INV_TEAMLEAD","INVESTIGATION_NUMBER"

  )

  AS(
  
	select * from VIEW_ESCALATION_BASE 
	  where INVESTIGATION_NUMBER = 0 
	  and STATUS_INC_4 is not null
	  And STATUS_INC_5 Is Null
	  And STATUS_INC_6 Is Null
	  and (SIGNOFF_DATE_INC_4)  <= (sysdate - 14)
  	  and (INC_LAST_AUDIT_DATE)  <= (sysdate - 14)

);

Create Or Replace Force View "CATSDBA"."VIEW_ESCALATION_UC2_5" (

	"INCIDENT_NUMBER","INCIDENT_TITLE","INC_LAST_AUDIT_DATE","INV_LAST_AUDIT_DATE",
    "CURRENT_SUPERINTENDENT_ID", "ESCALATING_SUPERINTENDENT_ID", 
	"INCDISPLYID","INCIDENTSTATUS",
	"SIGNOFF_PERSON_ID_1","FORWARD_PERSON_ID_1","SIGNOFF_DATE_1","STATUS_1",
	"SIGNOFF_PERSON_ID_2","FORWARD_PERSON_ID_2","SIGNOFF_DATE_2","STATUS_2",
	"SIGNOFF_PERSON_ID_3","FORWARD_PERSON_ID_3","SIGNOFF_DATE_3","STATUS_3",
	"SIGNOFF_PERSON_ID_INC_4","FORWARD_PERSON_ID_INC_4","SIGNOFF_DATE_INC_4","STATUS_INC_4",
	"SIGNOFF_PERSON_ID_INC_5","FORWARD_PERSON_ID_INC_5","SIGNOFF_DATE_INC_5","STATUS_INC_5",
	"SIGNOFF_PERSON_ID_INC_6","FORWARD_PERSON_ID_INC_6","SIGNOFF_DATE_INC_6","STATUS_INC_6",
	"SIGNOFF_PERSON_ID_4","FORWARD_PERSON_ID_4","SIGNOFF_DATE_4","STATUS_4",
	"SIGNOFF_PERSON_ID_5","FORWARD_PERSON_ID_5","SIGNOFF_DATE_5","STATUS_5",
	"SIGNOFF_PERSON_ID_6","FORWARD_PERSON_ID_6","SIGNOFF_DATE_6","STATUS_6",
	"INVESTIGATION_TEAM_PERSONID",
	"CURRENT_SIGNOFF_EMAIL_1","CURRENT_SIGNOFF_EMAIL_2","CURRENT_SIGNOFF_EMAIL_3","CURRENT_SIGNOFF_EMAIL_INC_4","CURRENT_SIGNOFF_EMAIL_INC_5","CURRENT_SIGNOFF_EMAIL_INC_6",
	"CURRENT_SIGNOFF_EMAIL_4","CURRENT_SIGNOFF_EMAIL_5","CURRENT_SIGNOFF_EMAIL_6",
	"CURRENT_FORWARD_EMAIL_1","CURRENT_FORWARD_EMAIL_2","CURRENT_FORWARD_EMAIL_3","CURRENT_FORWARD_EMAIL_INC_4","CURRENT_FORWARD_EMAIL_INC_5","CURRENT_FORWARD_EMAIL_INC_6",
	"CURRENT_FORWARD_EMAIL_4","CURRENT_FORWARD_EMAIL_5","CURRENT_FORWARD_EMAIL_6",
	"CURRENT_SUPERINTENDENT_EMAIL","CURRENT_INV_TEAM_LEAD_EMAIL",
	"ESCALATION_EMAIL_SIGNOFF1","ESCALATION_EMAIL_SIGNOFF2","ESCALATION_EMAIL_SIGNOFF3","ESCALATION_EMAIL_SIGNOFF_INC4","ESCALATION_EMAIL_SIGNOFF_INC5","ESCALATION_EMAIL_SIGNOFF_INC6",
	"ESCALATION_EMAIL_SIGNOFF4","ESCALATION_EMAIL_SIGNOFF5","ESCALATION_EMAIL_SIGNOFF6",
	"ESCALATION_EMAIL_FORWARD1","ESCALATION_EMAIL_FORWARD2","ESCALATION_EMAIL_FORWARD3","ESCALATION_EMAIL_FORWARD_INC4","ESCALATION_EMAIL_FORWARD_INC5","ESCALATION_EMAIL_FORWARD_INC6",
	"ESCALATION_EMAIL_FORWARD4","ESCALATION_EMAIL_FORWARD5","ESCALATION_EMAIL_FORWARD6",
	"ESCALATION_EMPNUM_SIGNOFF1","ESCALATION_EMPNUM_SIGNOFF2","ESCALATION_EMPNUM_SIGNOFF3","ESCALATION_EMPNUM_SIGNOFF_INC4","ESCALATION_EMPNUM_SIGNOFF_INC5","ESCALATION_EMPNUM_SIGNOFF_INC6",
	"ESCALATION_EMPNUM_SIGNOFF4","ESCALATION_EMPNUM_SIGNOFF5","ESCALATION_EMPNUM_SIGNOFF6",
	"ESCALATION_EMPNUM_FORWARD1","ESCALATION_EMPNUM_FORWARD2","ESCALATION_EMPNUM_FORWARD3","ESCALATION_EMPNUM_FORWARD_INC4","ESCALATION_EMPNUM_FORWARD_INC5","ESCALATION_EMPNUM_FORWARD_INC6",
	"ESCALATION_EMPNUM_FORWARD4","ESCALATION_EMPNUM_FORWARD5","ESCALATION_EMPNUM_FORWARD6",
	"ESCALATION_EMAIL_SUPER","ESCALATION_EMAIL_INV_TEAMLEAD","ESCALATION_EMPNUM_INV_TEAMLEAD","INVESTIGATION_NUMBER"

  )

  AS(
  
	select * from VIEW_ESCALATION_BASE 
	  where INVESTIGATION_NUMBER = 0 
	  And STATUS_INC_5 Is Not Null
	  And STATUS_INC_6 Is Null
	  and (SIGNOFF_DATE_INC_5)  <= (sysdate - 14)
  	  and (INC_LAST_AUDIT_DATE)  <= (sysdate - 14)

);
	  


Create Or Replace Force View "CATSDBA"."VIEW_ESCALATION_UC2_6" (

	"INCIDENT_NUMBER","INCIDENT_TITLE","INC_LAST_AUDIT_DATE","INV_LAST_AUDIT_DATE",
    "CURRENT_SUPERINTENDENT_ID", "ESCALATING_SUPERINTENDENT_ID", 
	"INCDISPLYID","INCIDENTSTATUS",
	"SIGNOFF_PERSON_ID_1","FORWARD_PERSON_ID_1","SIGNOFF_DATE_1","STATUS_1",
	"SIGNOFF_PERSON_ID_2","FORWARD_PERSON_ID_2","SIGNOFF_DATE_2","STATUS_2",
	"SIGNOFF_PERSON_ID_3","FORWARD_PERSON_ID_3","SIGNOFF_DATE_3","STATUS_3",
	"SIGNOFF_PERSON_ID_INC_4","FORWARD_PERSON_ID_INC_4","SIGNOFF_DATE_INC_4","STATUS_INC_4",
	"SIGNOFF_PERSON_ID_INC_5","FORWARD_PERSON_ID_INC_5","SIGNOFF_DATE_INC_5","STATUS_INC_5",
	"SIGNOFF_PERSON_ID_INC_6","FORWARD_PERSON_ID_INC_6","SIGNOFF_DATE_INC_6","STATUS_INC_6",
	"SIGNOFF_PERSON_ID_4","FORWARD_PERSON_ID_4","SIGNOFF_DATE_4","STATUS_4",
	"SIGNOFF_PERSON_ID_5","FORWARD_PERSON_ID_5","SIGNOFF_DATE_5","STATUS_5",
	"SIGNOFF_PERSON_ID_6","FORWARD_PERSON_ID_6","SIGNOFF_DATE_6","STATUS_6",
	"INVESTIGATION_TEAM_PERSONID",
	"CURRENT_SIGNOFF_EMAIL_1","CURRENT_SIGNOFF_EMAIL_2","CURRENT_SIGNOFF_EMAIL_3","CURRENT_SIGNOFF_EMAIL_INC_4","CURRENT_SIGNOFF_EMAIL_INC_5","CURRENT_SIGNOFF_EMAIL_INC_6",
	"CURRENT_SIGNOFF_EMAIL_4","CURRENT_SIGNOFF_EMAIL_5","CURRENT_SIGNOFF_EMAIL_6",
	"CURRENT_FORWARD_EMAIL_1","CURRENT_FORWARD_EMAIL_2","CURRENT_FORWARD_EMAIL_3","CURRENT_FORWARD_EMAIL_INC_4","CURRENT_FORWARD_EMAIL_INC_5","CURRENT_FORWARD_EMAIL_INC_6",
	"CURRENT_FORWARD_EMAIL_4","CURRENT_FORWARD_EMAIL_5","CURRENT_FORWARD_EMAIL_6",
	"CURRENT_SUPERINTENDENT_EMAIL","CURRENT_INV_TEAM_LEAD_EMAIL",
	"ESCALATION_EMAIL_SIGNOFF1","ESCALATION_EMAIL_SIGNOFF2","ESCALATION_EMAIL_SIGNOFF3","ESCALATION_EMAIL_SIGNOFF_INC4","ESCALATION_EMAIL_SIGNOFF_INC5","ESCALATION_EMAIL_SIGNOFF_INC6",
	"ESCALATION_EMAIL_SIGNOFF4","ESCALATION_EMAIL_SIGNOFF5","ESCALATION_EMAIL_SIGNOFF6",
	"ESCALATION_EMAIL_FORWARD1","ESCALATION_EMAIL_FORWARD2","ESCALATION_EMAIL_FORWARD3","ESCALATION_EMAIL_FORWARD_INC4","ESCALATION_EMAIL_FORWARD_INC5","ESCALATION_EMAIL_FORWARD_INC6",
	"ESCALATION_EMAIL_FORWARD4","ESCALATION_EMAIL_FORWARD5","ESCALATION_EMAIL_FORWARD6",
	"ESCALATION_EMPNUM_SIGNOFF1","ESCALATION_EMPNUM_SIGNOFF2","ESCALATION_EMPNUM_SIGNOFF3","ESCALATION_EMPNUM_SIGNOFF_INC4","ESCALATION_EMPNUM_SIGNOFF_INC5","ESCALATION_EMPNUM_SIGNOFF_INC6",
	"ESCALATION_EMPNUM_SIGNOFF4","ESCALATION_EMPNUM_SIGNOFF5","ESCALATION_EMPNUM_SIGNOFF6",
	"ESCALATION_EMPNUM_FORWARD1","ESCALATION_EMPNUM_FORWARD2","ESCALATION_EMPNUM_FORWARD3","ESCALATION_EMPNUM_FORWARD_INC4","ESCALATION_EMPNUM_FORWARD_INC5","ESCALATION_EMPNUM_FORWARD_INC6",
	"ESCALATION_EMPNUM_FORWARD4","ESCALATION_EMPNUM_FORWARD5","ESCALATION_EMPNUM_FORWARD6",
	"ESCALATION_EMAIL_SUPER","ESCALATION_EMAIL_INV_TEAMLEAD","ESCALATION_EMPNUM_INV_TEAMLEAD","INVESTIGATION_NUMBER"

  )

  AS(
  
	select * from VIEW_ESCALATION_BASE 
	  where INVESTIGATION_NUMBER = 0 
	  And STATUS_INC_6 Is Not Null
	  and (SIGNOFF_DATE_INC_6)  <= (sysdate - 14)
  	  and (INC_LAST_AUDIT_DATE)  <= (sysdate - 14)

);

	  