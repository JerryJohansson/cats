CREATE OR REPLACE FORCE VIEW "CATSDBA"."ORIGINATING_INVESTIGATION" ("TYPE", "REPORT_ID", "SITE", "DEPARTMENT", "REPORT_DATE", "EMPLOYEE", "DETAILS", "INCDISPLYID")
AS
  (SELECT 'Incident' Type,
    Incident_Number Report_Id,
    Site,
    Department,
    Incident_Date Report_Date,
    Injured_Person Employee,
    Incident_Categories Details,
    INCDISPLYID
  FROM View_Incident_PrintView
  ) ;