
CREATE SEQUENCE "CATSDBA"."INVESTIGATION_SIGNOFF_SEQ" MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE NOORDER NOCYCLE ;

CREATE TABLE "CATSDBA"."TBLINVESTIGATION_SIGNOFF"
  (

	"SIGNOFF_NUMBER" NUMBER NOT NULL ENABLE,
    "INVESTIGATION_NUMBER" NUMBER NOT NULL ENABLE,

    "SIGNOFF_PERSON_ID"            NUMBER,
    "FORWARD_PERSON_ID"            NUMBER,
    "SIGNOFF_DATE" TIMESTAMP (6) NOT NULL ENABLE,

	"MESSAGE" VARCHAR2(2000 BYTE),
	"SIGNOFF_TYPE" VARCHAR2(3000 BYTE),
	"SIGNOFF_STAGE" NUMBER,
 CONSTRAINT "PKTBLSIGNOFFID01" PRIMARY KEY ("SIGNOFF_NUMBER") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 1179648 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "CATSINDEX" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    Initial 3276800 Next 1048576 Minextents 1 Maxextents 2147483645 Pctincrease 0 Freelists 1 Freelist Groups 1 Buffer_Pool Default
  );
CREATE OR REPLACE TRIGGER "CATSDBA"."INVESTIGATION_SIGNOFF_TRG" 
BEFORE INSERT 
  ON "TBLINVESTIGATION_SIGNOFF"
	REFERENCING NEW AS NEW  
	FOR EACH ROW 
		BEGIN
			SELECT CATSDBA.INVESTIGATION_SIGNOFF_SEQ.NEXTVAL
			INTO :NEW.SIGNOFF_NUMBER
			FROM DUAL; 
		END;
  
