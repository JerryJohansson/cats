update tblincidentform_checkboxes set checkbox_desc = 'Commerce', checkbox_comment = 'Department of Commerce' where checkbox_type = 'Reported_To' and checkbox_desc = 'DOIR';
update tblincidentform_checkboxes set checkbox_desc = 'DER', checkbox_comment = 'Department of Environment Regulations' where checkbox_type = 'Reported_To' and checkbox_comment = 'Department of Environment and Conservation';
update tblincidentform_checkboxes set checkbox_desc = 'DMP', checkbox_comment = 'Department of Mines and Petroleum' where checkbox_type = 'Reported_To' and checkbox_desc = 'RS - Resources Safety';
