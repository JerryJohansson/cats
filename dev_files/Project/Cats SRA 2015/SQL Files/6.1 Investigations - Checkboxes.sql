insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF1', 'Absent', '', 'Detection systems / procedures?', 1);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF1', 'Complete failure', '', 'Detection systems / procedures?', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF1', 'Some failure', '', 'Detection systems / procedures?', 3);





insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order)
values (0, 'ABS_OR_FAILED_DF8', 'Absent', '', 'PPE?', 1);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order)
values (0, 'ABS_OR_FAILED_DF8', 'Complete failure', '', 'PPE', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order)
values (0, 'ABS_OR_FAILED_DF8', 'Some failure', '', 'PPE?', 3);




insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF2', 'Absent', '', 'Protection systems / procedures?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF2', 'Complete failure', '', 'Protection systems / procedures?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF2', 'Some failure', '', 'Protection systems / procedures?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF3', 'Absent', '', 'Warning systems / procedures?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF3', 'Complete failure', '', 'Warning systems / procedures?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF3', 'Some failure', '', 'Warning systems / procedures?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF4', 'Absent', '', 'Guard systems / procedures?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF4', 'Complete failure', '', 'Guard systems / procedures?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF4', 'Some failure', '', 'Guard systems / procedures?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF5', 'Absent', '', 'Recovery systems / procedures?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF5', 'Complete failure', '', 'Recovery systems / procedures?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF5', 'Some failure', '', 'Recovery systems / procedures?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF6', 'Absent', '', 'Escape systems / procedures?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF6', 'Complete failure', '', 'Escape systems / procedures?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF6', 'Some failure', '', 'Escape systems / procedures?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF7', 'Absent', '', 'Rescue systems / procedures?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF7', 'Complete failure', '', 'Rescue systems / procedures?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF7', 'Some failure', '', 'Rescue systems / procedures?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF9', 'Absent', '', 'Hazard identification?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF9', 'Complete failure', '', 'Hazard identification?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF9', 'Some failure', '', 'Hazard identification?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF10', 'Absent', '', 'Risk Management?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF10', 'Complete failure', '', 'Risk Management?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF10', 'Some failure', '', 'Risk Management?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF11', 'Absent', '', 'Safe work procedure?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF11', 'Complete failure', '', 'Safe work procedure?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF11', 'Some failure', '', 'Safe work procedure?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF12', 'Absent', '', 'Supervision?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF12', 'Complete failure', '', 'Supervision?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF12', 'Some failure', '', 'Supervision?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF13', 'Absent', '', 'Other?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF13', 'Complete failure', '', 'Other?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF13', 'Some failure', '', 'Other?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF14', 'Absent', '', 'Other?', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF14', 'Complete failure', '', 'Other?', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ABS_OR_FAILED_DF14', 'Some failure', '', 'Other?', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'TREATMENT_OUTCOME', 'Back to Work', '', 'Other?', 1);----Changes Craig Wood SRA----
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'TREATMENT_OUTCOME', 'First Aid', '', 'Other?', 2);----Changes Craig Wood SRA----
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'TREATMENT_OUTCOME', 'Doctor', '', 'Other?', 3);----Changes Craig Wood SRA----
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'TREATMENT_OUTCOME', 'Home', '', 'Other?', 4);----Changes Craig Wood SRA----
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'TREATMENT_OUTCOME', 'Hospital', '', 'Other?', 5);----Changes Craig Wood SRA----


--------------------------
--------------------------

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT1', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT1', 'Inadequate', '', 'Inadequate', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT1', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT2', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT2', 'Inadequate', '', 'Inadequate', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT2', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT3', 'Exceeded', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT3', 'Unsuitable', '', 'Unsuitable', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT4', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT4', 'Exceed limits', '', 'Exceed limits', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT4', 'Misuse', '', 'Misuse', 3);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT4', 'Unsuitable', '', 'Unsuitable', 4);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT5', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT5', 'Exceed limits', '', 'Exceed limits', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT5', 'Misuse', '', 'Misuse', 3);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT5', 'Unsuitable', '', 'Unsuitable', 4);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT6', 'Partilly', '', 'Partilly', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT6', 'Not followed', '', 'Not followed', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT6', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT7', 'Inadequate', '', 'Inadequate', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT7', 'Unsuitable', '', 'Unsuitable', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT8', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT8', 'Inadequate', '', 'Inadequate', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT8', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT9', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT9', 'Inadequate', '', 'Inadequate', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT9', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT10', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT10', 'Inadequate', '', 'Inadequate', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT10', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT11', 'Absent', '', 'Absent', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT11', 'Inadequate', '', 'Inadequate', 2);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT11', 'Unsuitable', '', 'Unsuitable', 3);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT12', 'Mitigated', '', 'Mitigated', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT12', 'Unmitigated', '', 'Unmitigated', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT13', 'Other', '', 'Other', 1);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'INDIV_TEAM_IT14', 'Other', '', 'Other', 1);

---------------
---------------

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF1', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF1', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF2', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF2', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF3', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF3', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF4', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF4', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF5', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF5', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF6', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF6', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF7', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF7', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF8', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF8', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF9', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF9', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF10', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF10', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF11', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF11', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF12', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF12', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF13', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF13', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF14', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'WORKFACTOR_WF14', 'Significant', '', 'Significant', 2);


---------------
-------------------

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF1', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF1', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF2', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF2', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF3', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF3', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF4', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF4', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF5', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF5', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF6', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF6', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF7', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF7', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF8', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF8', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF9', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF9', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF10', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF10', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF11', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF11', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF12', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF12', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF13', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF13', 'Significant', '', 'Significant', 2);

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF14', 'Some', '', 'Some', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'HUMANFACTOR_HF14', 'Significant', '', 'Significant', 2);

--------------------
--------------------

insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_HW', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_TR', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_OR', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_CO', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_IG', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_PR', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_MM', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_DE', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_RM', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_MC', 'Contributing', '', 'Contributing', 1);
insert into tblIncidentForm_Checkboxes (checkbox_id, checkbox_type, checkbox_desc, checkbox_comment, dropdown_display, sort_order) values (0, 'ORGFACTOR_CM', 'Contributing', '', 'Contributing', 1);




--SELECT distinct Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC

---tblHazard_Checkboxvalues























