insert into tblsite
(site_id, site_description, email_from_address, mims_district, site_code, site_folder, next_pcr_no, pcr_admin_email, next_advice_no)
values
(5, 'RTD', 'chandalaCATSadmin@tronox.com', 'MUC2','RT', 'C:\', -1, 'chandalaPCRadmin@tronox.com', -1);

insert into tblsite
(site_id, site_description, email_from_address, mims_district, site_code, site_folder, next_pcr_no, pcr_admin_email, next_advice_no)
values
(6, 'Production Services', 'chandalaCATSadmin@tronox.com', 'MUC3','PS', 'C:\', -1, 'chandalaPCRadmin@tronox.com', -1);

insert into tblsite
(site_id, site_description, email_from_address, mims_district, site_code, site_folder, next_pcr_no, pcr_admin_email, next_advice_no)
values
(7, 'Logistics', 'chandalaCATSadmin@tronox.com', 'MUC4','LO', 'C:\', -1, 'chandalaPCRadmin@tronox.com', -1);

--commit;
