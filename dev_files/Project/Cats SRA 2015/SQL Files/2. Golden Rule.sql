ALTER TABLE TBLINCIDENT_DETAILS
ADD GOLDENRULE NUMBER;


ALTER TABLE TBLINCIDENT_DETAILS
ADD GOLDENRULEOTHER VARCHAR2(1000);

--delete from tblRefTable where reftable_type = 'Golden Rule'


insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Action Management (Std 17)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Air Emissions (Std 21)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Biodiversity (Std 22)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Change Management (Std 3)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Communication and Consultation (Std 7)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Confined Space (Std 31) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Contractor Selection and Management (Std 11)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Crisis and Emergency Management (Std 10)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Document Control and Records Management (Std 12)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Electrical Safety (Std 37) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Energy Isolation (Std 34) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Fire Prevention / Hotwork (Std 39) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Greenhouse Gas/Energy (Std 23)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Hazardous Substances (Std 36) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Housekeeping (Std 24)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Hydrocarbons (Std 25)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Incident Management (Std 13)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Legal and Other Requirements (Std 4)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Lifting & Material Handling (Std 33) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Management Review and Improvement (Std 18)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Mobile Equipment & Vehicles (Std 35) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Monitoring, Measurement and Reporting (Std 15)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Objectives, Targets and Improvement Plans (Std 5)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Occupational Health (Std 16)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Operational Controls (Std 6)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Other Activities / Equipment', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Policy/Leadership/Commitment (Std 1)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Pressure Hazards (Std 38) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Process Waste (Std 26)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Rehabilitation (Std 27)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Risk and Opportunity Management (Std 2)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Stakeholder Management (Std 28)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Structure and Responsibility (Std 9)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Training and Competency (Std 8)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Waste Management (Std 29)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Water Management (Std 30)', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Working at Height (Std 32) ', null, 0);
insert into tblRefTable (reftable_id, reftable_type, reftable_description, reftable_comment, site_id) values (0, 'Golden Rule', 'Working on or near water ', null, 0);
