-- Adding super intendent to employee, to get automatic selection for incident details creation

ALTER TABLE TBLEMPLOYEE_DETAILS
ADD RELATED_SUPERINTENDENT int;

Alter Table Tblincident_Details
ADD	"RISK_LIKELIHOOD_ACTUAL" VARCHAR2(20 BYTE);

Alter Table Tblincident_Details
	ADD "RISK_CONSEQUENCE_ACTUAL" Varchar2(20 Byte);
  
Alter Table Tblincident_Details
	ADD "RISK_RATING_ACTUAL" VARCHAR2(20 BYTE);

Alter Table Tblincident_Details
	ADD "RISK_LIKELIHOOD_POTENTIAL" Varchar2(20 Byte);
  
Alter Table Tblincident_Details
	ADD "RISK_CONSEQUENCE_POTENTIAL" Varchar2(20 Byte);
  
Alter Table Tblincident_Details
	ADD "RISK_RATING_POTENTIAL" VARCHAR2(20 BYTE);

	

	
-- Added 24.04.2015
ALTER TABLE Tblincident_Details
	ADD "TEAM_SELECTED_BY_PERSONID" int;	

ALTER TABLE Tblincident_Details
	ADD "INVESTIGATION_TEAM_PERSONID" int;	

ALTER TABLE Tblincident_Details
	ADD "WITNESS_1_PERSONID" int;	

ALTER TABLE Tblincident_Details
	ADD "WITNESS_2_PERSONID" int;	

ALTER TABLE Tblincident_Details
	ADD "DETAILED_DESCRIPTION_HOW" Varchar2(4000 Byte);

ALTER TABLE Tblincident_Details
	ADD "IMMEDIATE_CAUSES" Varchar2(4000 Byte);

ALTER TABLE Tblincident_Details
	ADD "UNDERLYING_CAUSES" Varchar2(4000 Byte);

ALTER TABLE Tblincident_Details	
	ADD "SIGNOFF_PERSON_ID_1"            NUMBER;
	
ALTER TABLE Tblincident_Details	
    ADD "FORWARD_PERSON_ID_1"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "SIGNOFF_DATE_1" 				 TIMESTAMP (6);
ALTER TABLE Tblincident_Details	
	ADD "MESSAGE_1" 					 VARCHAR2(2000 BYTE);
ALTER TABLE Tblincident_Details	
	ADD "STATUS_1"	 					 VARCHAR2(10 BYTE);

ALTER TABLE Tblincident_Details	
	ADD "SIGNOFF_PERSON_ID_2"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "FORWARD_PERSON_ID_2"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "SIGNOFF_DATE_2" 				 TIMESTAMP (6);
ALTER TABLE Tblincident_Details	
	ADD "MESSAGE_2" 					 VARCHAR2(2000 BYTE);
ALTER TABLE Tblincident_Details	
	ADD "STATUS_2"	 					 VARCHAR2(10 BYTE);

ALTER TABLE Tblincident_Details	
	ADD "SIGNOFF_PERSON_ID_3"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "FORWARD_PERSON_ID_3"            NUMBER;
ALTER TABLE Tblincident_Details	
    ADD "SIGNOFF_DATE_3" 				 TIMESTAMP (6);
ALTER TABLE Tblincident_Details	
	ADD "MESSAGE_3" 					 VARCHAR2(2000 BYTE);
Alter Table Tblincident_Details	
	ADD "STATUS_3"	 					 VARCHAR2(10 BYTE);
	
	

CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_EMPLOYEE_DETAILS" ("EMPLOYEE_NUMBER", "INDUCTION_NUMBER", "FIRST_NAME", "LAST_NAME", "SITE_ID", "DEPARTMENT_ID", "POSITION", "INDUCTION_DATE", "CAN_CHANGE_DUE_DATE", "INCIDENT_PRIVILEGED_USER", "ACTION_PRIVILEGED_USER", "TYPE_OF_EMPLOYEE", "CONTRACTOR_COMPANY", "EMAIL_ADDRESS", "TERMINATION_DATE", "ACCESS_ID", "GROUP_NAME", "GROUP_MASK", "PASSWORD", "USER_STATUS", "VIEWABLE_SITES", "SITE_ACCESS", "EMP_NAME", "COMPANYID", "SECTION_ID", "VERSION", "SUPERINTENDENT", "ENGINEER", "TERMINATION_DATE_STRING", "INDUCTION_DATE_STRING", "COMPANYNAME", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "POSITION_NAME", "SECTION_DESCRIPTION", "RELATED_SUPERINTENDENT")
AS
  (SELECT e.Employee_Number,
    e.INDUCTION_NUMBER,
    e.First_Name,
    e.Last_Name,
    e.Site_Id,
    e.Department_Id,
    e.Position,
    e.Induction_Date,
    e.Can_Change_Due_Date,
    e.Incident_Privileged_User,
    e.Action_Privileged_User,
    e.Type_Of_Employee,
    e.Contractor_Company,
    e.Email_Address,
    e.Termination_Date,
    e.Access_Id,
    e.Group_Name,
    e.GROUP_MASK,
    e.Password,
    e.User_Status,
    e.Viewable_Sites,
    e.SITE_ACCESS,
    e.Last_Name
    || ', '
    || e.First_Name Emp_Name,
    e.CompanyId,
    e.Section_Id,
    e.Version,
    e.Superintendent,
    e.Engineer,
    TO_CHAR(e.Termination_Date) Termination_Date_String,
    TO_CHAR(e.Induction_Date) Induction_Date_String,
    c.CompanyName,
    s.Site_Description,
    d.Department_Description,
    p.Position_Description Position_Name,
    a.Section_Description,
	e.Related_Superintendent
  FROM tblEmployee_Details e,
    tblSite s,
    tblDepartment d,
    tblPosition p,
    tblCompanies c,
    tblSection a
  WHERE e.Site_Id     = s.Site_Id
  AND e.Department_Id = d.Department_Id
  AND e.Position      = p.Position_Id(+)
  AND e.CompanyId     = c.CompanyId(+)
  AND e.Section_Id    = a.Section_Id(+)
  );