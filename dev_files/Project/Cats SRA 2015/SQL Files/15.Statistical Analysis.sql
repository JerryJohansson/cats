-- Create table for statistical analysis data


  
  
  
  create or replace
PROCEDURE SP_INCIDENT_STAT_ANALYSIS(L_CURSOR out SYS_REFCURSOR, SiteCodeParam IN STRING, YearParam IN STRING, AUorUSACalcParam IN number)
is

StartDate date;
StartDateStr varchar(20);

BEGIN

    SELECT '01-JAN-' || YearParam  into StartDateStr FROM dual;

    OPEN L_CURSOR FOR

    select sitecode, SiteName, dates, twelve_month_avg_DIFR_emp, twelve_month_avg_DIFR_cont, YTD_DIFR_emp, YTD_DIFR_cont, twelve_month_avg_AIFR_emp, twelve_month_avg_AIFR_cont, YTD_AIFR_RIFR_emp, YTD_AIFR_RIFR_cont, LostTimeInjuries_emp, LostTimeInjuries_cont,
          RestrWorkDayCases_emp, RestrWorkDayCases_cont, MedicalTreatCases_emp, MedicalTreatCases_cont, FirstAidCases_emp, FirstAidCases_cont, HrsWorkedProwatch_emp, HrsWorkedProwatch_cont, HeadcountProwatch_emp
    from (
      select SiteCodeParam as sitecode,
            '' as SiteName,
            TRUNC(TO_DATE('01-Jan-2010'),'YEAR') dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 1) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 2) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 3) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 4) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 5) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 6) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 7) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 8) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 9) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 10) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    union 
    
    select SiteCodeParam as sitecode,
            '' as SiteName,
            ADD_MONTHS(TRUNC(TO_DATE('01-Jan-2010'),'YEAR'), 11) dates, 
            0 twelve_month_avg_DIFR_emp,
            0 twelve_month_avg_DIFR_cont,
            0 YTD_DIFR_emp,
            0 YTD_DIFR_cont,
            0 twelve_month_avg_AIFR_emp,
            0 twelve_month_avg_AIFR_cont,
            0 YTD_AIFR_RIFR_emp,
            0 YTD_AIFR_RIFR_cont,
            0 LostTimeInjuries_emp,
            0 LostTimeInjuries_cont,
            0 RestrWorkDayCases_emp,
            0 RestrWorkDayCases_cont,
            0 MedicalTreatCases_emp,
            0 MedicalTreatCases_cont,
            0 FirstAidCases_emp,
            0 FirstAidCases_cont,
            0 HrsWorkedProwatch_emp,
            0 HrsWorkedProwatch_cont,
            0 HeadcountProwatch_emp
            
    from dual
    
    
    
    
    
    
    ) xxx;
    
END;    







  

  
  
  //------------------------------
  
  CREATE TABLE "CATSDBA"."DW_TBLINCIDENTANALYSIS"
  (
    "SITE_ID" NUMBER NOT NULL ENABLE,
    "MONTH" NUMBER NOT NULL ENABLE,
    "YEAR" NUMBER NOT NULL ENABLE,
	"SITE_CODE" VARCHAR2(2 BYTE) NOT NULL ENABLE,
	"SITE_DESCRIPTION" VARCHAR2(100 BYTE) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_DIFR_EMP_2K" NUMBER(8,2) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_DIFR_CONT_2K" NUMBER(8,2) NOT NULL ENABLE,
    "YTD_DIFR_EMP_2K"  NUMBER(8,2) NOT NULL ENABLE,
    "YTD_DIFR_CONT_2K"  NUMBER(8,2) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_AIFR_EMP_2K" NUMBER(8,2) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_AIFR_CONT_2K" NUMBER(8,2) NOT NULL ENABLE,
    "YTD_AIFR_RIFR_EMP_2K" NUMBER(8,2) NOT NULL ENABLE,
    "YTD_AIFR_RIFR_CONT_2K" NUMBER(8,2) NOT NULL ENABLE,

    "TWELVE_MONTH_AVG_DIFR_EMP_1M" NUMBER(8,2) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_DIFR_CONT_1M" NUMBER(8,2) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_AIFR_EMP_1M" NUMBER(8,2) NOT NULL ENABLE,
    "TWELVE_MONTH_AVG_AIFR_CONT_1M" NUMBER(8,2) NOT NULL ENABLE,
	
    "LOSTTIMEINJURIES_EMP" NUMBER NOT NULL ENABLE,
    "LOSTTIMEINJURIES_CONT" NUMBER NOT NULL ENABLE,
    "RESTRWORKDAYCASES_EMP" NUMBER NOT NULL ENABLE,
    "RESTRWORKDAYCASES_CONT" NUMBER NOT NULL ENABLE,
    "MEDICALTREATCASES_EMP" NUMBER NOT NULL ENABLE,
    "MEDICALTREATCASES_CONT" NUMBER NOT NULL ENABLE,
    "FIRSTAIDCASES_EMP" NUMBER NOT NULL ENABLE,
    "FIRSTAIDCASES_CONT" NUMBER NOT NULL ENABLE,
    "HRSWORKEDPROWATCH_EMP" NUMBER NOT NULL ENABLE,
    "HRSWORKEDPROWATCH_CONT" NUMBER NOT NULL ENABLE,
    "HEADCOUNTPROWATCH_EMP" NUMBER NOT NULL ENABLE,
	
    CONSTRAINT "PKTBLINCANALYSS01" PRIMARY KEY ("SITE_ID", "MONTH", "YEAR") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS STORAGE(INITIAL 1179648 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "CATSINDEX" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 3276800 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "CATSDATA" ;

  
  
  
  
  		











CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_INCIDENT_STATISTIC" (SITE_ID, MONTH, YEAR,SITE_CODE, SITE_DESCRIPTION, TWELVE_MONTH_AVG_DIFR_EMP_2K, TWELVE_MONTH_AVG_DIFR_CONT_2K,
YTD_DIFR_EMP_2K, YTD_DIFR_CONT_2K, TWELVE_MONTH_AVG_AIFR_EMP_2K, TWELVE_MONTH_AVG_AIFR_CONT_2K, YTD_AIFR_RIFR_EMP_2K, YTD_AIFR_RIFR_CONT_2K,
TWELVE_MONTH_AVG_DIFR_EMP_1M, TWELVE_MONTH_AVG_DIFR_CONT_1M, TWELVE_MONTH_AVG_AIFR_EMP_1M, TWELVE_MONTH_AVG_AIFR_CONT_1M,
LOSTTIMEINJURIES_EMP, LOSTTIMEINJURIES_CONT, RESTRWORKDAYCASES_EMP, RESTRWORKDAYCASES_CONT, MEDICALTREATCASES_EMP,
MEDICALTREATCASES_CONT, FIRSTAIDCASES_EMP, FIRSTAIDCASES_CONT, HRSWORKEDPROWATCH_EMP, HRSWORKEDPROWATCH_CONT, HEADCOUNTPROWATCH_EMP)
AS
(

	-- CH 2014

      SELECT 
			1 AS SITE_ID,
			1 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
    
    union 

      SELECT 
			1 AS SITE_ID,
			2 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
    
    union 

      SELECT 
			1 AS SITE_ID,
			3 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL

    union 

      SELECT 
			1 AS SITE_ID,
			4 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			1 AS SITE_ID,
			5 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL

    union 

      SELECT 
			1 AS SITE_ID,
			6 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			1 AS SITE_ID,
			7 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			1 AS SITE_ID,
			8 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			1 AS SITE_ID,
			9 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
	
    union 

      SELECT 
			1 AS SITE_ID,
			10 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			1 AS SITE_ID,
			11 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			1 AS SITE_ID,
			12 AS MONTH,
			2014 AS YEAR,
			'CH' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL

		union
---------------------
---------------------

	-- BE 2014

      SELECT 
			2 AS SITE_ID,
			1 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
    
    union 

      SELECT 
			2 AS SITE_ID,
			2 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
    
    union 

      SELECT 
			2 AS SITE_ID,
			3 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL

    union 

      SELECT 
			2 AS SITE_ID,
			4 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			2 AS SITE_ID,
			5 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL

    union 

      SELECT 
			2 AS SITE_ID,
			6 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			2 AS SITE_ID,
			7 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			2 AS SITE_ID,
			8 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			2 AS SITE_ID,
			9 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
	
    union 

      SELECT 
			2 AS SITE_ID,
			10 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			2 AS SITE_ID,
			11 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL
	
    union 

      SELECT 
			2 AS SITE_ID,
			12 AS MONTH,
			2014 AS YEAR,
			'BE' AS SITE_CODE,
            'SITE DESC' AS SITE_DESCRIPTION,
            0 TWELVE_MONTH_AVG_DIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_DIFR_CONT_2K,
            0 YTD_DIFR_EMP_2K,
            0 YTD_DIFR_CONT_2K,
            0 TWELVE_MONTH_AVG_AIFR_EMP_2K,
            0 TWELVE_MONTH_AVG_AIFR_CONT_2K,
            0 YTD_AIFR_RIFR_EMP_2K,
            0 YTD_AIFR_RIFR_CONT_2K,
			
            0 TWELVE_MONTH_AVG_DIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_DIFR_CONT_1M,
            0 TWELVE_MONTH_AVG_AIFR_EMP_1M,
            0 TWELVE_MONTH_AVG_AIFR_CONT_1M,
			
            0 LOSTTIMEINJURIES_EMP,
            0 LOSTTIMEINJURIES_CONT,
            0 RESTRWORKDAYCASES_EMP,
            0 RESTRWORKDAYCASES_CONT,
            0 MEDICALTREATCASES_EMP,
            0 MEDICALTREATCASES_CONT,
            0 FIRSTAIDCASES_EMP,
            0 FIRSTAIDCASES_CONT,
            0 HRSWORKEDPROWATCH_EMP,
            0 HRSWORKEDPROWATCH_CONT,
            0 HEADCOUNTPROWATCH_EMP
            
    FROM DUAL


	
	
);





