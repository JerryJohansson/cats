<?php
require_once('includes/conf.php');

if(cats_get_version()==3){
	set_time_limit ( 120 );
	$sql="select 
		employee_number, 
		access_id, 
		first_name, 
		last_name, 
		(first_name||' '||last_name) full_name, 
		(substr(upper(first_name),1,1)||substr(upper(last_name),1,1)) initials, 
		trim(email_address) email, 
		viewable_sites, 
		group_name 
	from tblemployee_details where user_status = 'Active'";
	$rs = db_query($sql);
	$db->debug=true;
	$j=2;
	$uers_xml = '';
	while($arr = $rs->FetchRow()){
		$site_access = '';
		$emp_no = $arr['EMPLOYEE_NUMBER'];
		$asites = explode("SITE_ID =",$arr['VIEWABLE_SITES']);
		$group_name = $arr['GROUP_NAME'];
		$x = count($asites);
		$j++;
		if($x>0){
			for($i=0; $i<$x; $i++){
				$asites[$i] = (int)$asites[$i];
				if($asites[$i]>0){
					$site_access .= "" . $asites[$i] . ",";
				}
			}
			$site_access = preg_replace("/,$/","",$site_access);
		}else{
			$site_access = '';
		}
		switch($group_name){
			case 'Super Administrator': case 'Administrators':
				$group_mask = 524288+4096+1024+512+32+16+8+4+2;
				break;
			case 'Admin Assistants':
				$group_mask = 512+32+16+8+4+2;
				break;
			case 'Incident Analysis':
				$group_mask = 32+16+8+4+2;
				break;
			case 'Edit (Privileged)':
				$group_mask = 16+8+4+2;
				break;
			case 'Edit (Restricted)':
				$group_mask = 8+4+2;
				break;
			case 'Reader (Privileged)':
				$group_mask = 4+2;
				break;
			case 'Reader':
				$group_mask = 2;
				break;
			default:
				$group_mask = 0;
				break;
		}
		
		$update_sql = "
		update tblemployee_details set 
			SITE_ACCESS = '$site_access' ,
			GROUP_MASK = $group_mask
		where employee_number = $emp_no";
		$success = $db->Execute($update_sql);
	}
	
	//echo($uers_xml);
	echo("DONE::success = ");
	print_r($success);
}

function cats_get_version(){
	return 3;
	$sql = "select configuration_value from configuration where configuation_key = 'CATS_VERSION'";
	return db_get_one($sql);
}
$db = db_close();
?>