// <!-- Hide
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
function CATS_get_item_title(itm){
	var title = "Field";
	if(((title=itm.getAttribute("title"))==null)||((title=itm.getAttribute("title"))=="")){
		title = String(itm.getAttribute("name")).replace(/_/g," ").toPropperCase();
	}
	return title;
}
function CATS_validateForm() { //v4.0 - renamed so not to get mixed up with inline MM_validateForm function
	try{
		top._show_message('Please wait while saving...');
		var i,p,q,nm,nm2,test,num,min,max,errors='',val,val2,args=arguments;
		var el=args[0];
		if(el.type) e = el.form.elements;
		else e = el.element;
		if(typeof(e)=='undefined') e=document.forms[0].elements;
		for (i=1; i<(args.length-2); i+=3) { 
			test=args[i+2]; val=e[args[i]]; // can use element() here... but just in case, lets not ;)
			if (val) {
				nm=CATS_get_item_title(val);
				if ((val=val.value)!="") {
					if (test.indexOf('isEmail') != -1) //p=val.indexOf('@');
					{
						//if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
						if (!is_email_address(val)) errors+='- '+nm+' must contain an e-mail address.\n';
					}
					else if (test == "filePath")
					{
						if(val.charAt(0) != "\\" || val.charAt(1) != "\\")
						{
							errors += '- '+nm+' must be a correctly formatted network path eg \\\\path\\to\\file';
						}	
					}
					else if (test!='R') 
					{
						if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
						if (test.indexOf('inRange') != -1) { 
							p=test.indexOf(':');
							min=test.substring(8,p); max=test.substring(p+1);
							if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
						}
					} 
					else if (test.indexOf('dtRange') != -1)
					{ 
						val2=e[args[i+1]];
						if (val2) { 
							nm2=val2.getAttribute("title"); 
							if ((val2=val2.value)!="") { 
								if (!check_date_range(val,val2)) 
									errors+='- '+nm+' date must be a previous date to '+nm2+'.\n';
							}
						}
					}
				}
				else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; 
			}
		} 
		if(typeof(_validate_extra)=="function") errors += _validate_extra();
		if (errors) {
			alert('The following error(s) occurred:\n'+errors);
			top._hide_message();
		}
		top.gui.session.changed=true;
		return (errors == '');
	}catch(e){
		format_error("Error while validating form",e,args);
		top._hide_message();
		return false;
	}
}
function alertMe(s){alert(s);return s;}

// ----------------------------
//	Validate an email address to
//	have all the correct components
// ----------------------------
function is_email_address (string) {
  var addressPattern = 
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  return addressPattern.test(string);
}
function check_email (itm) {
  if (!is_email_address(itm.value)) {
    alert("Please enter correct email address!");
		setupFocus(itm);
  }
}

// ----------------------------
//	change mapped drive to UNC path
// ----------------------------
function GetUNC(ThisObj, DriveIn) {

var WshNetwork = new ActiveXObject("WScript.Network");
var Drives = WshNetwork.EnumNetworkDrives()
var NewDrive = DriveIn

if(DriveIn.charAt(0) != "\\") {
	for (i = 0; i < Drives.length; i++) {
		if(Drives.Item(i).charAt(0) == DriveIn.charAt(0).toUpperCase()) {
			NewDrive = Drives.Item(i+1) + DriveIn.substring(2)
		}
	}
}

ThisObj.value = NewDrive
return NewDrive;
}


// ----------------------------
//	Check to and from dates
//		to date must be greater than from date
// NOTE: Dates must come from the calendar control or
// format must be yyyy-mm-dd
// ----------------------------
function check_date_range(from, to) {
	a=from.split("-");b=to.split("-");
	return (Date.parse(new Date(a[0],a[1],a[2]).toGMTString()) <= Date.parse(new Date(b[0],b[1],b[2]).toGMTString()));
}

// ----------------------------
//	Validates a number entered in a field
// ----------------------------
function check_number(itm,len) {
	var val=itm.value;
	var lbl = itm.getAttribute("title");
	if(isNaN(val)){
		alert(lbl+" requires a valid number.");
		itm.value="";
		setupFocus(itm);
	}else{
		if(!isNaN(len) && (val.length != len)){
			alert(lbl+" requires the number to be "+len+" digits in length.");
			itm.value = val.substring(0,len);
		}
	}
}
function check_number_in_clause(itm){
	var val=itm.value.replace(/,/gi,"").replace(/\s/gi,"");
	if(isNaN(val)){
		alert("Please enter a valid number.\nYou may seperate numbers with a comma to search within results\nField value will be erased.");
		itm.value="";
	}
}
function check_string_in_clause(itm){
	//TODO: add check for in clause as a string
	return true
}

function check_other(o1,o2){
	if(o1.checked) o2.checked=false;
}
function check_other_radio(o1,o2){
	var args=arguments, i=0, x=o2.length;
	var val=(args.length>2)?args[2]:'Yes';
	if(o1.value==val){
		for(i=0;i<x;i++)
			o2[i].checked=(o2[i].value!=o1.value);
	}
}

function sync_dd(o1,o2){
	o2.selectedIndex = o1.selectedIndex;
}

// ----------------------------
//	Uses timeout to set the focus to a given field
//	This is to get over the tab key bug
// ----------------------------
function setupFocus(item){
	// set a timeout for tab key bug
	var args = setupFocus.arguments;
	var bSelect = (args.length>1)?args[1]:false;
	if(item != null && typeof(item) == "object")
		setTimeout("setFocusToField(document.forms['" + item.form.name + "'].elements['" + item.name + "']," + bSelect + ");",10);
}
function setFocusToField(item,bSelect){
	if(item != null && typeof(item) == "object"){
		if(bSelect) item.select();
		item.focus();
	}
}


function add_sequence_row(itm, name){
	try{
		var tname = name+"_table";
		var t=element(tname);
		var add=element(name+"_add");
		var row = t.insertRow(t.rows.length);
		var i=0;
		for(i=0; i < add.rows[0].cells.length; i++){
			row.insertCell(i).innerHTML=add.rows[0].cells[i].innerHTML;
		}
		format_sequence_table(tname);
	}catch(e){
		format_error("Error adding sequence row",e,tname);
	}
}

function remove_sequence_row(itm){
	var id = itm.id.replace(/^b_/gi,"");
	var t=element(id+"_table");
	var r = itm.parentNode.parentNode;
	//if(empty(r.getAttribute('id'))) format_sequence_table(id+"_table");
	//t.deleteRow(r.getAttribute('id'));
	t.deleteRow(r.rowIndex);
	format_sequence_table(id+"_table");
}
function format_sequence_table(tid) {
	var rows = element(tid).rows;
	var i=0
	for (i = 0; i < rows.length; i++) {
		rows[i].setAttribute('id', i);
		if (i % 2 == 0) {
			rows[i].style.backgroundColor = "#eeeeee";
		} else {
			rows[i].style.backgroundColor = "#ffffff";
		}
	}
}

// ------------------------------
// NOTE: No longer used, replaced by Change_Risk_New below
// Calculate Risk Values for Major Hazards
// ------------------------------
function Change_Risk(Likelihood_Field, Consequence_Field, Risk_Field, Overall_Field) {
	var i = 5;
	var j = 5;
	
	var Likelihood_dd = Likelihood_Field + "_dd";
	var Consequence_dd = Consequence_Field + "_dd";
	var Risk_display = Risk_Field + "_display";
	
	// Put Values in Real Fields
	document.getElementById(Likelihood_Field).value = document.getElementById(Likelihood_dd).value;
	document.getElementById(Consequence_Field).value = document.getElementById(Consequence_dd).value;
	
	switch (document.getElementById(Likelihood_Field).value) {
		case "Almost Certain" :
			i = 0;
			break;
		case "Likely" :
			i = 1;
			break;
		case "Moderate" :
			i = 2;
			break;
		case "Unlikely" :
			i = 3;
			break;
		case "Rare" :
			i = 4;
			break;
		default :
			break;
	}
	
	switch (document.getElementById(Consequence_Field).value) {
		case "Catastrophic" :
			j = 0;
			break;
		case "Major" :
			j = 1;
			break;
		case "Moderate" :
			j = 2;
			break;
		case "Minor" :
			j = 3;
			break;
		case "Insignificant" :
			j = 4;
			break;
		default :
			break;
	}

	if ((i == 5) || (j == 5)) {
		document.getElementById(Risk_Field).value = "";
	} else {
		var Matx = new Array(5);
		// Almost Certain
		Matx[0] = new Array();
		Matx[0][0] = "1.1";
		Matx[0][1] = "1.3";
		Matx[0][2] = "1.6";
		Matx[0][3] = "2.6";
		Matx[0][4] = "2.8";
		// Likely
		Matx[1] = new Array();
		Matx[1][0] = "1.2";
		Matx[1][1] = "1.5";
		Matx[1][2] = "2.4";
		Matx[1][3] = "2.7";
		Matx[1][4] = "3.4";
		// Moderate
		Matx[2] = new Array();
		Matx[2][0] = "1.4";
		Matx[2][1] = "1.8";
		Matx[2][2] = "2.5";
		Matx[2][3] = "3.3";
		Matx[2][4] = "4.3";
		// UnLikely
		Matx[3] = new Array();
		Matx[3][0] = "1.7";
		Matx[3][1] = "2.2";
		Matx[3][2] = "3.1";
		Matx[3][3] = "4.1";
		Matx[3][4] = "4.4";
		// Rare
		Matx[4] = new Array();
		Matx[4][0] = "2.1";
		Matx[4][1] = "2.3";
		Matx[4][2] = "3.2";
		Matx[4][3] = "4.2";
		Matx[4][4] = "4.5";
		
		// Get Required Value
		document.getElementById(Risk_Field).value = Matx[i][j];
		//document.getElementById(Risk_Field).value = "Moderate";
		
	}
	// Set the Display Field
	document.getElementById(Risk_display).value = document.getElementById(Risk_Field).value;
	
	// Set the Light Field
	Light_Risk(Risk_Field);
	
	// Re-calculate Overall Risk
	Calculate_Overall_Risk(Overall_Field);
}

// ------------------------------
// Calculate Risk Values for Major Hazards
// ------------------------------
function Change_Risk_New(Likelihood_Field, Consequence_Field, Risk_Field, Overall_Field) {
	var i = 5;
	var j = 5;
	
	var Likelihood_dd = Likelihood_Field + "_dd";
	var Consequence_dd = Consequence_Field + "_dd";
	var Risk_display = Risk_Field + "_display";
	
	// Put Values in Real Fields
	document.getElementById(Likelihood_Field).value = document.getElementById(Likelihood_dd).value;
	document.getElementById(Consequence_Field).value = document.getElementById(Consequence_dd).value;
	
	var Likelihood_Value = document.getElementById(Likelihood_Field).value;
	var Consequence_Value = document.getElementById(Consequence_Field).value;
	var Result_Text = "";

	if (((Likelihood_Value == "Likely") && (Consequence_Value == "Insignificant")) 
		|| ((Likelihood_Value == "Moderate") && (Consequence_Value == "Insignificant"))
		|| ((Likelihood_Value == "Unlikely") && (Consequence_Value == "Insignificant"))
		|| ((Likelihood_Value == "Unlikely") && (Consequence_Value == "Minor"))
		|| ((Likelihood_Value == "Rare") && (Consequence_Value == "Insignificant")) 
		|| ((Likelihood_Value == "Rare") && (Consequence_Value == "Minor"))) {
	Result_Text = "Low";
	}
	
	if (((Likelihood_Value == "Almost Certain") && (Consequence_Value == "Insignificant")) 
		|| ((Likelihood_Value == "Likely") && (Consequence_Value == "Minor"))
		|| ((Likelihood_Value == "Moderate") && (Consequence_Value == "Minor"))
		|| ((Likelihood_Value == "Moderate") && (Consequence_Value == "Moderate"))
		|| ((Likelihood_Value == "Unlikely") && (Consequence_Value == "Moderate")) 
		|| ((Likelihood_Value == "Unlikely") && (Consequence_Value == "Major")) 
		|| ((Likelihood_Value == "Rare") && (Consequence_Value == "Moderate")) 
		|| ((Likelihood_Value == "Rare") && (Consequence_Value == "Major")) 
		|| ((Likelihood_Value == "Rare") && (Consequence_Value == "Catastrophic"))) {
	Result_Text = "Moderate";
	}
		
	if (((Likelihood_Value == "Almost Certain") && (Consequence_Value == "Minor")) 
		|| ((Likelihood_Value == "Almost Certain") && (Consequence_Value == "Moderate"))
		|| ((Likelihood_Value == "Likely") && (Consequence_Value == "Moderate"))
		|| ((Likelihood_Value == "Likely") && (Consequence_Value == "Major"))
		|| ((Likelihood_Value == "Moderate") && (Consequence_Value == "Major")) 
		|| ((Likelihood_Value == "Moderate") && (Consequence_Value == "Catastrophic")) 
		|| ((Likelihood_Value == "Unlikely") && (Consequence_Value == "Catastrophic"))) {
	Result_Text = "High";
	}
	
			
	if (((Likelihood_Value == "Almost Certain") && (Consequence_Value == "Major")) 
		|| ((Likelihood_Value == "Almost Certain") && (Consequence_Value == "Catastrophic"))
		|| ((Likelihood_Value == "Likely") && (Consequence_Value == "Catastrophic"))) {
	Result_Text = "Very High";
	}

	// Get Required Value
	//document.getElementById(Risk_Field).value = Matx[i][j];
	document.getElementById(Risk_Field).value = Result_Text;
		
	// Set the Display Field
	document.getElementById(Risk_display).value = document.getElementById(Risk_Field).value;
	
	// Set the Light Field
	Light_Risk_New(Risk_Field, Result_Text);
	
	// Re-calculate Overall Risk
	Calculate_Overall_Risk(Overall_Field);
}

// ------------------------------
// Calculate Risk Light Colour for Major Hazards
// ------------------------------
function Calculate_Overall_Risk(Overall_Field) {
	var prefix = "";
	
	var Overall_display = Overall_Field + "_display";
	var Overall_Risk = 0;
	var Highest_Risk = 0;
	var Current_Risk = 0;
	
	// Work out which risk we are working with
	switch(Overall_Field) {
		case "INIRISK_OR" :
			prefix = "INIRISK_";
			break;
		case "RESRISK_OR" :
			prefix = "RESRISK_";
			break;
	}
	
	// Find the Highest Value
	//for (var m=0; m<5; m++) {
		//if (document.getElementById(prefix+m+"_R").value > Overall_Risk)
			//Overall_Risk = document.getElementById(prefix+m+"_R").value;
	//}
	for (var m=0; m<5; m++) {
		if (document.getElementById(prefix+m+"_R").value == "Low") {
			Current_Risk = 1;
		}
		if (document.getElementById(prefix+m+"_R").value == "Moderate") {
			Current_Risk = 2;
		}
		if (document.getElementById(prefix+m+"_R").value == "High") {
			Current_Risk = 3;
		}
		if (document.getElementById(prefix+m+"_R").value == "Very High") {
			Current_Risk = 4;
		}
		
		if (Current_Risk > Highest_Risk) {
			Highest_Risk = Current_Risk;
		}
		//if (document.getElementById(prefix+m+"_R").value > Overall_Risk)
			//Overall_Risk = document.getElementById(prefix+m+"_R").value;
	}
		switch(Highest_Risk) {
		case 1 :
			Overall_Risk = "Low";
			break;
		case 2 :
			Overall_Risk = "Moderate";
			break;
		case 3 :
			Overall_Risk = "High";
			break;
		case 4 :
			Overall_Risk = "Very High";
			break;

	}

	
	// Set the Real Field
	document.getElementById(Overall_Field).value = Overall_Risk;
	
	// Set the Display Field
	document.getElementById(Overall_display).value = Overall_Risk;
	
	// Set the Light Field
	Light_Risk_New(Overall_Field);
}

// ------------------------------
// Calculate Risk Light Colour for Major Hazards
// ------------------------------
function Light_Risk(Risk_Field) {
	var Risk_colour = "#FFFFFF";
	
	var Risk_light = Risk_Field + "_light";

	// Set Display Colour
	var risk = parseInt(document.getElementById(Risk_Field).value,10);
	//alert(risk);
	
	// Determine the appropriate colour
	switch(risk) {
		case 0 :
			Risk_colour = "#FFFFFF";
			break;
		case 1 :
			Risk_colour = "#FF0000";
			break;
		case 2 :
			Risk_colour = "#FF9900";
			break;
		case 3 :
			Risk_colour = "#FF99CC";
			break;
		case 4 :
			Risk_colour = "#00FF00";
			break;
	} 
	
	// Set the Display Light background colour
	document.getElementById(Risk_light).style.backgroundColor = Risk_colour;
}

// ------------------------------
// Calculate Risk Light Colour for Major Hazards
// ------------------------------
function Light_Risk_New(Risk_Field) {
	var Risk_colour = "#FFFFFF";
		
	var Risk_light = Risk_Field + "_light";

	// Set Display Colour
	var risk = document.getElementById(Risk_Field).value;
	
	// This code just to handle old data which was stored as a number
	//if ((risk >= "4.1") && (risk <= "4.5")) {
	//	risk = "Low";
	//	document.getElementById(Risk_Field).value = "Low"
	//}
	
	// Determine the appropriate colour
	switch(risk) {
		case "Low" :
			//Green
			Risk_colour = "#426F42";
			break;
		case "Moderate" :
			//Yellow
			Risk_colour = "#FFFF00";
			break;
		case "High" :
			//Magenta
			Risk_colour = "#FF00FF";
			break;
		case "Very High" :
			// Red
			Risk_colour = "#FF0000";
			break;
	} 

// Set the Display Light background colour
	document.getElementById(Risk_light).style.backgroundColor = Risk_colour;
}

/* make radio buttons act as checkboxes - use this for non-mandatory radioset buttons
(
	function(){
		function down(){
			bmlRadioValue=this.checked;
			bmlRadioRef=this;
		}
		function click() {
			if((window.bmlRadioRef==this)&&window.bmlRadioValue) {
				this.checked=false;
				bmlRadioRef=null;
			}
		}
		function mU(radio){
			radio.onmousedown=down; 
			radio.onkeydown=down;
			radio.onclick=click;
		}
		var x,k,f,j;
		x=document.forms;
		for (k=0;k<x.length;++k){
			f=x[k];
			for(j=0;j<f.length;++j)
				if(f[j].type.toLowerCase()=="radio")
					mU(f[j]);
		}
	}
)
();
*/
// -->
