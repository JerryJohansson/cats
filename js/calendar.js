function _resize_flash(id,w,h) {
	var o;if((o=element(id))!=null){o.style.width=w;o.style.height=h;}
}
_resize_calendar=_resize_flash;
function _move_calendar(id,x,y){// + w,h
	var a=arguments;
	var o=element(id);
	if(o){
		o.style.top=y;
		o.style.left=x;
		var w,h;
		if(a.length>3) w=a[3];
		if(a.length>4) h=a[4];
		if(!isNaN(w+h)) _resize_calendar(id,w,h);
	}
}
function getCalendar(date,display){
	var args=arguments;
	var s="";
	var el=element("cal_container");
	var format = (args.length>2)?args[2]:"dd-mmm-yyyy HH:MM";
	var display_format = (args.length>3)?args[3]:"dd-mmm-yyyy HH:MM";
	var force=(new Date());
	var s=''+
	'<obj'+'ect classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" '+
	'	width="100%" height="100%" id="cal" align="top">'+
	'<param name="allowScriptAccess" value="sameDomain" />'+
	'<param name="salign" value="LT" />'+
	'<param name="scale" value="noscale" />'+
	'<param name="movie" value="style/loader.swf?u=style/cal.swf&d1='+date+'&d2='+display+'&f1='+format+'&f2='+display_format+'&tmp='+force+'" />'+
	'<param name="quality" value="high" />'+
	'<param name="bgcolor" value="#cccccc" />'+
	'<emb'+'ed src="style/loader.swf?u=style/cal.swf&d1='+date+'&d2='+display+'&f1='+format+'&f2='+display_format+'&tmp='+force+'" quality="high" scale="noscale" salign="LT" bgcolor="#cccccc" width="100%" height="100%" name="cal" align="top" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />'+
	'</obj'+'ect>';
	if(el){
		el.innerHTML=s;
	}else{
		var div=document.createElement('div');
		div.id='cal_container';
		div.style.position='absolute';
		div.style.visibility='hidden';
		div.style.width='1px';
		div.style.height='1px';
		div.style.zIndex=1000;
		div.innerHTML=s;
		document.body.appendChild(div);
	}
	el=element("cal_container");
	return el;
}
var CALENDAR_ELEMENT=null;
var CALENDAR_ELEMENT_DISPLAY=null;
var CALENDAR_OBJECT=null;
function _calendar_hide(){
	if(CALENDAR_OBJECT) CALENDAR_OBJECT.style.visibility='hidden';
	CALENDAR_OBJECT=null;
}
_hide_calendar = _calendar_hide;
function _show_calendar(item,format,display_format){
	_calendar_hide();
	CALENDAR_ELEMENT=item.previousSibling;//form.elements[item.name+"_d"];
	CALENDAR_ELEMENT_DISPLAY=item;
	CALENDAR_OBJECT = getCalendar(CALENDAR_ELEMENT_DISPLAY.value,CALENDAR_ELEMENT.value,format,display_format);
	
	var pos=getPos(CALENDAR_ELEMENT);
	var pg=getPageCoords();
	var oheight=(CALENDAR_OBJECT.offsetHeight==1)?190:CALENDAR_OBJECT.offsetHeight;
	var y=pos.y+25;
	var ocont=element("Lcontentbody");
	if(ocont){ // adjust y if container layer exists
		y=(y-ocont.scrollTop);
		pg.ch=(pg.h-ocont.scrollTop);
	}
	if(pg.ch<(y+oheight)) y=((y-25)-oheight);
	CALENDAR_OBJECT.style.top=y;
	CALENDAR_OBJECT.style.left=pos.x;
	CALENDAR_OBJECT.style.visibility='';
}
function _calendar_set(raw,display,update){
	if(CALENDAR_ELEMENT) CALENDAR_ELEMENT.value=display;
	if(CALENDAR_ELEMENT_DISPLAY) CALENDAR_ELEMENT_DISPLAY.value=raw;
	//alert(CALENDAR_ELEMENT_DISPLAY.id + ":"+ CALENDAR_ELEMENT_DISPLAY.value +"\n"+CALENDAR_ELEMENT.id +":"+CALENDAR_ELEMENT.value);
	if(update==true) CALENDAR_ELEMENT=null;
	if(update==true) CALENDAR_ELEMENT_DISPLAY=null;
	
}
function _calendar_set_date(raw,display,update){
	_calendar_set(raw,display,update);
	if(update==true) _calendar_hide();
}
function _calendar_resize(w,h){
	_resize_flash('cal_container',w,h);
}
function _calendar_set_focus(){
		if(CALENDAR_OBJECT) element("cal").setFocus();
}
