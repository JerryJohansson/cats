/*
+---------------------------------------------------------------------------
|	Return datetime in raw format
|	date should be in format dd-mmm-yyyy
|	time should be in format 00:00
|	raw date is returned in format YYYYMMDDHHMMSS
+---------------------------------------------------------------------------
*/	
function get_raw_date(itm,date,time,to){
	//alert(itm.form+":"+date+":"+time+":"+to)
	var form = itm.form.elements;
	//alert(form['cal_start_date_d'].value);
	var sdate =	form[date].value;
	//alert(sdate);
	var s_time=form[time].value;
	var stime = (trim(s_time)=='')?":":s_time;
	//alert(stime)
	var raw_item = form[to];
	//alert(raw_item.name+":"+raw_item.value)
	
	var adate=sdate.split("-");
	//alert(adate.join())
	var atime=stime.split(":");
	//alert(atime.join())
	var d_delim = "-";
	var t_delim = ":";
	var str_time="";
	if(trim(s_time)!='') str_time = ' ' + pad_left(atime[0]) + t_delim + pad_left(atime[1]);
	
	raw_item.value = adate[2] + d_delim + monthname_to_month(adate[1]) + d_delim + pad_left(adate[0]) + str_time;
		
}
function check_raw_date_range(from, to){
	var args=arguments;
	var retValue = true;
	var sFromName = (args.length>2)?"'"+args[2]+"'":"'From Date'";
	var sToName = (args.length>3)?"'"+args[3]+"'":"'To Date'";
	var bMand = (args.length>4)?args[4]:false;
	var bReturnValue = (args.length>5)?args[5]:false;
	var msg = "The following errors occured for date range:\n";
	var obj=to;
	
	var pat=/(:|-|\s)/gi;
	var s_date=from.value.replace(pat,"");
	var e_date=to.value.replace(pat,"");
	if(parseInt(s_date) <= parseInt(e_date)){
		msg="";
		retValue=true;
	}else{
		retValue=false;
		msg+=sFromName+" must be less or equal to "+sToName+".\n\nPlease re-enter the start and end dates.";
	}
	
	if(bReturnValue){ // return the msg back to caller
		return (retValue) ? "" : {msg:msg,obj:obj} ;
	}
	if(!retValue){
		alert(msg.replace(/\n$/,""));
		if(obj)
			obj.focus();
	}
	return retValue;
}
function monthname_to_month(monthname){
	var a=['','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var i=0;
	var len=a.length
	for(i=0;i<len;i++)if(a[i]==monthname) break;
	return pad_left(i);
}

function pad_left(number){
	var s = "00"+number;
	return s.substring(s.length-2);
}

function pad_right(number){
	var s = number+"00";
	return s.substring(0,2);
}
//-----------------------------

// ----------------------------
//	Check time entered into element
//	and format time to hh:mm
// ----------------------------
function checktime(item) {
	var args = checktime.arguments;
	var name = (args.length>1)?args[1]:"Time";
	var bReturnValue = (args.length>2)?args[2]:false;
	var timePat = /^(\d{1,2})(\/|-|\s|\.|:)(\d{1,2})$/;
	var sTime = item.value;
	var aMatch = sTime.match(timePat); // is the format ok?
	var sTmp, sYear, sMonth, sDay, sReturnDate = "";
	var sep = ":";
	var msg = "The following errors occured in the '" + name + "' field:\n";
	var iMsg = 0;
	var retValue = true;
	var empty = false;
	if (trim(sTime)=="") return false;
	if (aMatch == null) {
		msg += (++iMsg) + ". Time is not in a valid format\n";
		empty = true;
		retValue = false;
	}else{
		// parse time into variables
		sHH = aMatch[1];
		sMM = aMatch[3];
	}
	if(trim(sTime).length>0 && !isNaN(sTime)){
		var len = trim(sTime).length;
		if(len<=2){
			sHH = sTime;
			sMM = "00";
		}
		if(len>2){
			sHH = sTime.substring(0,2);
			sMM = sTime.substring(2,4);
		}
		retValue = true;
	}
	if ((typeof(sHH)!="undefined") && sHH.length < 2) {
		sTmp = "00"+sHH;
		sHH = sTmp.substring(sTmp.length-2);
	}
	if ((typeof(sMM)!="undefined") && sMM.length < 2) {
		sTmp = "00"+sMM;
		sMM = sTmp.substring(sTmp.length-2);
	}
	
	if((typeof(sHH)!="undefined") && parseInt(sHH)>23){
		msg += (++iMsg) + ". Hours part cannot exceed 23 hrs\n";
		retValue = false;
	}
	
	if((typeof(sMM)!="undefined") && parseInt(sMM)>59){
		msg += (++iMsg) + ". Minutes part cannot exceed 59 min\n";
		retValue = false;
	}
	
	if(retValue){
		sReturnTime = sHH + sep + sMM;
		if(bReturnValue) return sReturnTime;
		item.value = sReturnTime;
	}else{
		if(empty) item.value = "";
		alert(msg);
		setupFocus(item);
	}
	return retValue;
}

// ----------------------------
//	Validates a number entered in a field
// ----------------------------
function validateNumber(eField,iLength,sLabelName) {
	var args = validateNumber.arguments;
	var bError = false;
	if(args.length>3 && args[3])
		bMand = true;
	else
		if(trim(eField.value)=="") return true;
	if (isNaN(eField.value)) {
		bError = true;
		alert ("Please enter a valid number");
	}else{
		if(args.length>1){
			if(eField.value.length!=iLength){
				bError = true;
				sLabelName = (args.length>2)?sLabelName:"The field " + eField.name;
				alert(sLabelName + " requires a " + iLength + " digit number");
			}
		}
	}
	
	if(bError){
		setupFocus(eField);
	}
	return !bError;
}
// ----------------------------
//	Uses timeout to set the focus to a given field
//	This is to get over the tab key bug
// ----------------------------
function setupFocus(item){
	// set a timeout for tab key bug
	var args = setupFocus.arguments;
	var bSelect = (args.length>1)?args[1]:false;
	if(item != null && typeof(item) == "object")
		setTimeout("setFocusToField(document.forms['" + item.form.name + "'].elements['" + item.name + "']," + bSelect + ");",10);
}
function setFocusToField(item,bSelect){
	if(item != null && typeof(item) == "object"){
		if(bSelect) item.select();
		item.focus();
	}
}
// ----------------------------
//	Validates a given fields value[item.value] against a given RegExp[pat]
// ----------------------------
function checkBadCharacters(item,pat){
	//*******************************
	// BEGIN::Special Character Matching
	// Author: Vern - 20030203
	// Parameters:
	//		item	= the form element to check
	//		pat		= re pattern without the /'s. e.g. "\||:|;|a|z|~"; This function will 
	//		return false if any of the characters in this list are present in the items value
	//
	// Escape the following Special Characters by a preceeding backslash (\)
	// Special Characters:
	//		$()*+.[?\^{|
	//		To match one of these characters itself, use \c (where c=Special Character)
	
	// Create match pattern
	if(pat.replace(/\s/g,"")=="") return true;
	var re = new RegExp("[" + pat + "]","gi");
	// use the String objects match to return an array
	var aMatch = item.value.match(re);
	var sMsg = "";
	if(aMatch!=null && typeof(aMatch)=="object"){
		for(i=0;i<aMatch.length;i++) sMsg += (sMsg.indexOf(aMatch[i])==-1)?"\"" + aMatch[i] + "\", ":"";
		alert("The following illegal characters were found in the Subject field:\n"+ sMsg +"\nPlease try again.");
		setupFocus(item);
		return false;
	}
	return true;
	// END::Special character match
	//*****************************
}





// END::Character Length Check
//******************************

// ----------------------------
//	Validates file types using the files extension
// ----------------------------
function checkFileType(item, list){
	if (list != ""){
		var re = new RegExp("\.(" + list.replace(/,/gi,"|").replace(/\s/gi,"") + ")$","i");
    if (item.value != "") {
      if(list != "" && !re.test(item.value)) {
        alert("This file type is not allowed for uploading.\nOnly the following file extensions are allowed: " + list + ".\nPlease select another file and try again.");
				item.value = "";
				setupFocus(item,true);
				return;
      }
      else if(item.value[0] == '\\' && item.value[1] == '\\')
      {
      	alert("File paths should be specified as a fully qualified network address ie \\Computer\FilePath\file.txt .\nPlease select another file and try again.");
      			item.value = "";
				setupFocus(item,true);
				return;
      }
    }
	}
}

// ----------------------------
//	Returns a string without white space
// ----------------------------
function trim(s){return s.replace(/\s/g,"");}

// ----------------------------
//	Clears all elements values
// ----------------------------
function clearForm(item){
	var form = item.form;
	form.reset();
	for(var i=0;i<form.length;i++) {
		var type = form[i].type.toLowerCase();
		switch(type){
			case "radio"	: case "checkbox"	: form[i].checked=false;break;
			case "select-one":form[i].options[0].selected=true;break;
			default:if(type!="button"&&type!="hidden"&&type!="submit"&&type!="reset") form[i].value="";break;
		}
	}
}


// Execute function evaluates
//
function execute(s){
	try{
		eval(s);
	}catch(e){
		var caller = "" + execute.caller;
		caller = caller.replace(/\s/g,"").replace(/function([a-zA-Z0-9]+)\((.*)/,"function $1()");
		alert("ERROR:\nAn error occured while executing the script block:\n- "+ s +"\nThe calling method is:\n- "+ caller );
	}
}

function _validation(){
	var args=arguments;
	this.field_labels=null;
	this.field_names=null;
	this.field_types=null;
	this.msg="";
	this.return_value=true;
	this.setForm=function(o){
		this.form=o;
	}
	this.addFields=function(s){
		var a=s.split(";");
		var fields=[];
		var labels=[];
		var types=[];
		var len=a.length-2;
		var i=0;
		for(i=0;i<len;i+=3) {
			fields[fields.length]=a[i];
			types[types.length]=a[i+1];
			labels[labels.length]=a[i+2];
		}
		this.field_names=fields;
		this.field_labels=labels;
		this.field_types=types;
	}
	this.addFields(args[0]);
	
	this.validate=function(){
		var i = j = 0;
		var oStrings =	(args.length>2&&!args[2]) ? {empty:"",alertonly:"",number:"",email:"",same:""}:
										{empty:"Please enter a value into the ",alertonly:"Please enter/select a value into/from ",number:"Please enter a valid number into the ",email:"Please enter a valid email address into the ",same:" must match the value of "};
										
		var bFocus = true;
		var bAlert = true;
		var label="";
		var value="";
		var type,obj,name;
		var i=0;
		var len=this.field_names.length;
		for(i=0;i<len;i++){
			label=this.field_labels[i].split("|");
			name=this.field_names[i].split("|");
			obj=this.form.elements[name[0]];
			value=null;
			type=this.field_types[i];
			switch(type){
				case "empty":
					if(trim(obj.value).length==0){
						this.msg += oStrings.empty + label[0] + " field\n";
						this.return_value = false;
    			}
					break;
				case "same":
					if(trim(obj.value).length==0){
						this.msg += oStrings.empty + label[0] + " field\n";
						this.return_value = false;
    			}else if(obj.value!=this.form.elements[name[1]].value){
						this.msg += label[0] + oStrings.same + "'" + label[1] + "'\n";
						this.return_value = false;
					}
					break;
				case "alertonly":
	    		if(trim(obj.value).length==0){
						this.msg += oStrings.alertonly + label[0] + " field\n";
						this.return_value = false;
						bFocus = false;
	    		}
					break;
				case "number":
					if((trim(obj.value).length==0) || isNaN(obj.value)){
						this.msg += oStrings.number + label[0] + " field\n";
						this.return_value = false;
					}
					break;
				case "email": 
					if (!is_email_address(trim(obj.value))){
						this.msg += oStrings.email + label[0] + " field\n";
						this.return_value = false;
					}
					break;
				case "length":
					var o = check_length( obj, label, name[1], name[2], true );
					if(	typeof(o)=="object" ){
						this.msg += o.msg;
						obj.value = o.value;
						this.return_value = true;
					}
					if(name[3]=="empty"){
						if(trim(obj.value).length==0){
							this.msg += oStrings.empty + label[0] + " field\n";
							this.return_value = false;
	    			}
					}
					break;
				case "date":
					this.return_value = check_date(obj);
					bAlert = this.return_value;
					break;
				case "daterange":
					var o = check_date_range( this.form.elements[name[0]], this.form.elements[name[1]], label[0], label[1], true, true );
					if(	typeof(o)=="object" ){
						this.msg += o.msg;
						obj = o.obj;
						this.return_value = false;
					}
					break;
				case "rawdaterange":
					var o = check_raw_date_range( this.form.elements[name[0]], this.form.elements[name[1]], label[0], label[1], true, true );
					if(	typeof(o)=="object" ){
						this.msg += o.msg;
						obj = o.obj;
						this.return_value = false;
					}
					break;
			}
			if(!this.return_value) break;
		}
		if(!this.return_value){
			if(bAlert) alert(this.msg.replace(/\n$/,""));
			if(bFocus)
				obj.focus();
		}else if(args.length>3) execute(args[3]);
		return this.return_value;
	}
	
}

function check_date_range(from, to){
	var retValue = true;
	var args=arguments;
	var from_date=getDateFromString(from.value);
	var to_date=getDateFromString(to.value);
	var sFromName = (args.length>2)?"'"+args[2]+"'":"'Start Date'";
	var sToName = (args.length>3)?"'"+args[3]+"'":"'End Date'";
	var bMand = (args.length>4)?args[4]:false;
	var bReturnValue = (args.length>5)?args[5]:false;
	var msg = "The following errors occured for date range:\n";
	var obj=to;
	if(from_date.getIntegerDate()<=to_date.getIntegerDate()){
		msg="";
		retValue=true;
	}else{
		retValue=false;
		msg+=sFromName+" must be less or equal to "+sToName+".\n\nPlease re-enter the start and end dates.";
	}
	
	if(bReturnValue){ // return the msg back to caller
		return (retValue) ? "" : {msg:msg,obj:obj} ;
	}
	if(!retValue){
		alert(msg.replace(/\n$/,""));
		if(obj)
			obj.focus();
	}
	return retValue;
}
function getMonthIndex(month){
	var months='|January|February|March|April|May|June|July|August|September|October|November|December';
	var amonths=months.split("|");
	var i=0;
	var len=amonths.length;
	//alert(amonths[1].substr(0,3));
	for(i=0;i<len;i++) if(amonths[i].substr(0,3)==month||amonths[i]==month) break;
	return i;
}
function getDateFromString(string,format){
	var delim=(string.indexOf("-")!=-1)?"-":" ";
	var date_split=string.split(delim);
	var i=0;
	var d='';
	var m='';
	var yyyy='';
	if(date_split.length==3){
		var len=date_split.length;
		for(i=0;i<len;i++){
			switch(i){
				case 0:
					d=date_split[i];
					break;
				case 1:
					m=getMonthIndex(date_split[i]);
					break;
				case 2:
					yyyy=date_split[i];
					break;
			}
		}
	}
	return (new Date(yyyy, m, d));
}

// check_length
// Truncate the value to the max length
// USAGE:check_length(item(form element),name[label of element][,maxLength(number)][,nolinebreaks(boolean)][,returnMsgNoAlert(boolean)]);
//
function check_length(item, name){
	var args = arguments;
	var maxLength = (args.length>2) ? args[2] : 80 ;
	var noLineBreaks = (args.length>3) ? args[3] : false ;
	var bReturnValue = (args.length>4) ? args[4] : false ;
	var valOut = valIn = item.value;
	var msg = "The following errors occured for the field " + name + ":\n";
	var retValue = true;
	var iCount = 0;
	if(noLineBreaks){
		if(valIn.match(/\n|\r/g)){
			msg += (++iCount) + ". Linebreaks were detected. All linebreaks will be removed\n";
			valOut = valOut.replace(/\n|\r/g,"");
			retValue = false;
		}
	}
 	if (valIn.length > maxLength){
		msg += (++iCount) + ". Max Length exceeded, anything after " + maxLength + " characters will be truncated\n";
		valOut = valOut.substring(0, maxLength);
		retValue = false;
	}
	if(bReturnValue){ // return the msg back to caller
		return (retValue) ? "" : {msg:msg,value:valOut} ;
	}
	if(!retValue){
		alert(msg);
		item.value = valOut;
		setupFocus(item);
	}
	return retValue;
}
//******************************
// BEGIN::Character Length Check
// Checks the length of a given field
// Returns boolean
// Suppresses key entry after maxLength has been reached
//
function check_max_length (item, evt, maxLength){
	if (item.selected && evt.shiftKey) return true;
	var allowKey = false;
	if (item.selected && item.selectedLength > 0)
		allowKey = true;
	else {
		var keyCode = document.all ? evt.keyCode : evt.which;
		if (keyCode < 32 && keyCode != 13)
			allowKey = true;
		else
			allowKey = (item.value.length < maxLength) && (keyCode != 13);
	}
	item.selected = false;
	return allowKey;
}
function _deny_keypress (item, exception, evt){
	var keyCode = document.all ? evt.keyCode : evt.which;
	if (keyCode === exception)
		return true;
	else
		return (keyCode == exception);
}
// ----------------------------
//	Check date entered into date field
//	and format date to the prefered style
// ----------------------------
function check_date(date_element,format) {
	var args = arguments;
	var sDateStyle = (args.length>1)?args[1]:((typeof(sGLOBAL_DATE_STYLE)!="undefined")?sGLOBAL_DATE_STYLE:"AU");  // AU = Australian date style : US = United States date style
	var bShortDateFormat = (args.length>2)?args[2]:false; // true = "01/01/2005" : false = "1 Jan 2005"||"1-Jan-2005";
	var sep = (bShortDateFormat) ? "/" : "-";
	var bReturnValue = (args.length>3)?args[3]:false;
	var datePat = /^(\d{1,2})(\/|-|\s|\.)(\d{1,2}|\D{3})\2(\d{1,4})$/;
	var dateStr = date_element.value;
	var aMatch = dateStr.match(datePat); // is the format ok?
	var aMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var sTmp, sYear, sMonth, sDay, sReturnDate = "";
	var name = (args.length>4)?args[4]:"Date";
	var msg = "The following errors occured in the '" + name + "' field:\n";
	var iMsg = 0;
	var retValue = true;
	if (trim(dateStr)=="") return false;
	if (aMatch == null) {
		msg += (++iMsg) + ". Date is not in a valid format\n";
		retValue = false;
	}else{
		// parse date into variables
		sDay = aMatch[1];
		sMonth = aMatch[3];
		sYear = aMatch[4];
		if(isNaN(sMonth)){
			var i=0;
			for(i=0;i<aMonths.length;i++){
				if(aMonths[i].substr(0,3).toLowerCase()==sMonth.toLowerCase()){
					sMonth=(i+1);
					break;
				}
			}
		}
	}
	
	if ((typeof(sYear)!="undefined") && sYear.length < 4) {
		sTmp = "000"+sYear;
		sYear = "2" + sTmp.substring(sTmp.length-3);
	}
	
	if ((typeof(sYear)!="undefined") && sYear.length !=4) {
		msg += (++iMsg) + ". Please enter a 4 digit year.\n";
		retValue = false;
	}
	if(!isNaN(sMonth)){
		if (sMonth < 1 || sMonth > 12) { // check month range
			msg += (++iMsg) + ". Month must be between 1 and 12\n";
			retValue = false;
		}
	
		if (sDay < 1 || sDay > 31) {
			msg += (++iMsg) + ". Day must be between 1 and 31\n";
			retValue = false;
		}
	
		if ((sMonth==4 || sMonth==6 || sMonth==9 || sMonth==11) && sDay==31) {
			msg += (++iMsg) + ". " + aMonths[sMonth-1] + " does not have 31 days\n";
			retValue = false;
		}
	}
	if (sMonth == 2) { // check for february 29th
		var isleap = (sYear % 4 == 0 && (sYear % 100 != 0 || sYear % 400 == 0));
		if (sDay>29 || (sDay==29 && !isleap)) {
			msg += (++iMsg) + ". " + aMonths[sMonth-1] + " " + sYear + " doesn't have " + sDay + " days\n";
			retValue = false;
		}
	}
	
	if(retValue){
		if (bShortDateFormat){
			sDay = dtFixEl(sDay);sMonth = dtFixEl(sMonth);
			sReturnDate = (sDateStyle == "US")?
												(sMonth+sep+sDay+sep+sYear):
												(sDay+sep+sMonth+sep+sYear);
		}else{
			sReturnDate = (sDateStyle == "US")?
												(aMonths[parseInt((sMonth-0))-1].substr(0,3) + sep + sDay + sep + sYear):
												(sDay + sep + aMonths[parseInt((sMonth-0))-1].substr(0,3)  + sep + sYear);
		}
		if(bReturnValue) return sReturnDate;
		date_element.value = sReturnDate;
	}else{
		alert(msg);
		setupFocus(date_element);
	}
	return retValue;
}
// ----------------------------
//	Convert digit{1} to digit{2}
// ----------------------------
function dtFixEl(i){
	var s = ""+i;
	return (s.length>1)?((s.length>2)?s.substring(0,2):s):"0"+s;
}

// ----------------------------
//	Validate an email address to
//	have all the correct components
// ----------------------------
function is_email_address (string) {
  var addressPattern = 
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  return addressPattern.test(string);
}
function check_email (eMail) {
  if (!is_email_address(eMail.value)) {
    alert("Please enter correct email address!");
		setupFocus(eMail);
  }
}
function checknumber(itm) {
	if(isNaN(itm.value)) {
		alert("Please enter a number");
		setupFocus(itm);
	}
}
function check_format(itm,pat){
	var patterns = {
		'phone': /^(\+|\+\(|\+\[|\(\+|\[\+|\(|\[)(\d{1,3})(\.|\)|\])(-| |:)(\d{4})(-| |:)(\d{4})$/ ,
		'number': /^(\d{1,9})$/ ,
		'decimal': /^(\d+)(\.)(d{2})/ ,
		'postcode': /^(\d{4})$/ ,
		'email':  /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/,
	//				/^\w+((-\w+)|(\.\w+))*\@([A-Za-z0-9]+)((\.|-)([A-Za-z0-9]+))*\.([A-Za-z0-9]+)$/
		'date': /^(\d{1,2})(\/|-|\s|\.)(\d{1,2}|\D{3})\2(\d{1,4})$/ ,
		'datetime': /^(\d{1,4})(\/|-|\s|\.)(\d{1,2}|\D{3})\2(\d{1,4})(\s)(\d{2})(:|-|\s)(\d{2})$/
	};
	var matches = itm.value.match(patterns[pat]);
	if(matches==null){
		alert("Please adjust the format to the given requirements.\nYou'll find an example of the format to the right of the field.");
		itm.focus();
		return false;
	}else{
		return true;
	}
}



// validation helper funciton
function validateFields(form,sValidate){
	if(typeof(sValidate)=='undefined') sValidate="";
	var valid=new _validation(sValidate.replace(/^;()/,""));
	valid.setForm(form);
	var validated=valid.validate();
	if(validated)
		return true;
	else 
		return false;

}