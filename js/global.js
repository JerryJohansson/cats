//--Browser sniffer---------------
var IE	= document.all;
var DOM	= document.getElementById;
var NS	= document.layers;
var NS6	= DOM && !IE;
function printPage() {
	var id=0;
	var tabs = false;
	var ch="";
	var of="";
	var c=null;
	try{
		var b = element("tab_buttons");
		var a = b.getElementsByTagName("A");
		for(i=0;i<a.length;i++){
			if(a[i].className=='indent') id=i;
			element('tab_panel['+(i)+']').style.display='';
		}
		c=element("Lcontentbody");
		ch=c.style.height;
		of=c.style.overflow;
		c.style.overflow = "";
		c.style.height="auto";

		tabs=true;
	}catch(e){}
	print(document);
	if(tabs){
		try{
			var b = element("tab_buttons");
			var a = b.getElementsByTagName("A");
			for(i=0;i<a.length;i++){
				if(id!=i) element('tab_panel['+(i)+']').style.display='none';
			}
			c.style.overflow=of;
			c.style.height=ch;
		}catch(e){}
	}
}
//--------------------------------
// All initialise/onload functions should start with the following keyword:
// init followed by an underscore and then by a descriptive name
// e.g. init_hide_all_messages() or init_what_do_i_do_here()
function init_document_handlers(){
	document.onmousedown=function(){
		if(typeof(top.hideMenu)=="function") top.hideMenu(); // hides menu layers
		if(typeof(top.hideHelpers)=="function") top.hideHelpers(); // hides any helper layers
		if(typeof(_hide_calendar)=="function") _hide_calendar(); // hides any calendar components
	}
	if (typeof(_hide_calendar)=="function") {
	// load the calendar in the background so its ready for use on the first page that uses this script
	// IE should cache the main flash calendar file once loaded
		var cal = getCalendar('','');
	}
	if(typeof _m == "object") _m.loadTemplates(element("cats::templates"));
	// *HACK - hide loading message
	var args=arguments;
	var hide=(args.length>0 && args[0]==false)?false:true;
	if(hide) top._hide_message();
	// *HACK - again - reset the sessions object
	top.gui.session.reset();
}

function init_resize_results_container(){
	var a=arguments;
	var id=(a[0])?a[0]:FORM_TARGET;
	try{
	// get a handle on the container iframe so we can minipulate its size...
	var win=parent.document.getElementById(id);
	// first we display it because by default it is hidden
	win.style.display = 'block';
	// then we get the height of the search results table and ...
	var h=document.body.firstChild.offsetHeight;
	var w=document.documentElement.offsetWidth;
	// assign the value to the container iframes height attribute
	win.style.height=h;
	//var p=parent.document;
	var pw=win.offsetWidth;//p.documentElement.offsetWidth;
	//alert(pw+":"+w)
	//top.window.defaultStatus = "Resized: pw = "+pw+": w ="+w;
	if(pw<w){
		//alert(w)
		//win.style.overflow = "auto";
		//win.style.width=w;//-40;
	}/*else {
		top.window.defaultStatus = "No Resize: pw = "+pw+": w ="+w+"::"+win.parentElement.nodeName+":"+win.parentElement.offsetWidth;
		win.style.width = win.parentElement.offsetWidth;//'100% !important';//win.parentElement.offsetWidth;//p.getElementById('c_results').offsetWidth;//
	}*/
	//win.style.overflow = 'auto';
	}catch(e){}
}
function init_resize_editor(){
	if((tb=element("tool_bar_c"))!=null){
		var main=element("Lcontentbody");
		/*var s='';
		for(e in top.gui.screens) s+=e+"\nPage="+top.gui.screens[e].page+"\nToolbar="+top.gui.screens[e].toolbars+"\n\n";
		alert(s);*/
		var name='screen_'+MODULE+"_"+(typeof(PAGE)!='undefined'?PAGE:'edit');
		var bh=top.gui.screens[name].page.offsetHeight;
		//alert(bh +":"+ top.gui.screens[name].page.contentWindow.window.document.body.offsetHeight)
		//alert(bh + ":" + tb.offsetHeight + ":" + top.gui.screens['screen_'+MODULE+'_edit'].page.offsetHeight);
		main.style.height=(bh-(tb.offsetHeight+4));
	}
}
function init_resize_tab_editor(){
	//alert(get_panel_height())
	element("Lcontentbody").style.height=get_panel_height();
	element("Lcontentbody").style.width=get_panel_width();
}
function get_panel_width(){
	//top.window.defaultStatus = top.document.documentElement.offsetWidth;
	return (parent.document.body.clientWidth);
}
function get_panel_height(){
	return (document.documentElement.offsetHeight-(element('tool_bar_c').offsetHeight+4));
}

function isset(o){
	return (typeof(o)!='undefined');
}
function showModalWindow(url,opt,features){
	var name = (typeof opt=="object")?"ezw_popup":opt;
	if(!IE){
		return window.open(url,name,features);
	}else{
		return showModalDialog(url,opt,features)
	}
}

function showModelessWindow(url,opt,features){
	var name = (typeof opt=="object")?"ezw_popup":opt;
	if(!IE){
		return window.open(url,name,features);
	}else{
		return showModelessDialog(url,opt,features)
	}
}

function show_window(url,name,features){
	return window.open(url,name,features);
}

function window_withfocus(url,name){
	var win=window.open(url,name);
	win.focus();
}

function show_cats_main_window(url){
	return show_window(url,"CATS_MAIN_WINDOW",getPopupFeatures(screen.availWidth,screen.availHeight,screen.availLeft,screen.availTop));//+",fullscreen=1");
}

function getDialogFeatures(){
	var args = arguments;
	var sFeat = sCenter = sTop = sLeft = sWidth = sHeight = "";
	var bDlg = (document.all)?true:false;
	var w=(args.length > 0)?args[0]:300;
	var h=(args.length > 1)?args[1]:200;
	if(args.length > 0)
		sWidth	= bDlg ? "dialogWidth: "+w+"px; " : "width="+w+",";
	if(args.length > 1)
		sHeight	= bDlg ? "dialogHeight: "+h+"px; " : "height="+h+",";
	if(args.length > 2)
		sTop		= bDlg ? "dialogTop: "+args[2]+"px; " : "top="+args[2]+",";
	if(args.length > 3)
		sLeft		= bDlg ? "dialogLeft: "+args[3]+"px; " : "left="+args[3]+",";
	if(args.length < 2) sCenter = bDlg ? "center : Yes; " : "top="+((screen.height/2)-(h/2))+",left="+((screen.width/2)-(w/2))+",";
	sFeat = bDlg ? "edge: Raised; help: No; resizable: No; status: No; unadorned: Yes; " : ((args.length>4)?args[4]:"menubar=yes,dependant=yes");
	return  sWidth+sHeight+sTop+sLeft+sCenter+sFeat;
}
function getPopupFeatures(){
	var args = arguments;
	var sFeat = sCenter = sTop = sLeft = sWidth = sHeight = "";
	var w=(args.length > 0)?args[0]:300;
	var h=(args.length > 1)?args[1]:200;
	if(args.length > 0)
		sWidth	= "width="+w+",";
	if(args.length > 1)
		sHeight	= "height="+h+",";
	if(args.length > 2)
		sTop		= "top="+args[2]+",";
	if(args.length > 3)
		sLeft		= "left="+args[3]+",";
	if(args.length < 2) sCenter = "top="+((screen.height/2)-(h/2))+",left="+((screen.width/2)-(w/2))+",";
	sFeat = ((args.length>4)?args[4]:"menubar=yes,status=yes,resizable=yes,dependant=yes");
	return  sWidth+sHeight+sTop+sLeft+sCenter+sFeat;
}
//--------------------------------
// Forms default functions
function setDefaultFocus(){
	var args = setDefaultFocus.arguments;
	var tags = document.getElementsByTagName("input");
	for(i=0;i<tags.length;i++){
		if(tags[i].type.match(/text|password|checkbox|radio|select/g)){
			tags[i].focus();
			if(tags[i].type.match(/text|password/g))
				tags[i].select();
			break;
		}
	}
}
SetFocus = setDefaultFocus;

function focusValue(item){
  var s1=item.value, s2=item.getAttribute("val");
	if(s1==s2) item.value="";
	return true;
}
function blurValue(item){
	var s1=item.value, s2=item.getAttribute("val");
	if(s1=="") item.value=s2;
	return true;
}
function getPageCoords(){
	var r = {
		w: document.body.offsetWidth,
		h: document.body.offsetHeight,
		ch: document.body.clientHeight,
		sh: document.body.scrollHeight,
		st: document.documentElement.scrollTop
	};
	return r;
}
function getPos(el) {
	var r = { x: el.offsetLeft, y: el.offsetTop };
	if (el.offsetParent) {
		var tmp = getPos(el.offsetParent);
		r.x += tmp.x;
		r.y += tmp.y;
	}
	return r;
}

element=getObj=function(obj){
	if(document.layers)function getRefNS4(doc,obj){
		var fobj=0;
		var c=0
		while (c < doc.layers.length) {
			if (doc.layers[c].name==obj) return doc.layers[c];
			fobj=getRefNS4(doc.layers[c].document,obj)
			if (fobj != 0) return fobj
			c++;
		}
		return 0;
	}

	return (document.getElementById)?document.getElementById(obj):(document.all)?
				 document.all[obj]:(document.layers)?getRefNS4(document,obj):0;
}

function _show_hide_elements(tag_name, b_show){
	var els=document.getElementsByTagName(tag_name);
	var i=0;
	var len=els.length;
	var display = (b_show)?'':'none';
	for(i=0;i<len;i++){
		els[i].style.display = display;
	}
}

function show(id){
	element(id).style.display='block';
}
function hide(id){
	element(id).style.display='none';
}
function toggle_display(id){
	var e=null;if((e=element(id))!=null) e.style.display=(e.style.display=='none')?'block':'none';
}

function toggle_search(){
	toggle_display('search_form');
	window.scrollTo(0,0);
	if(arguments[0] && typeof(_m)=="object")
		_m.search();
}


function _trim(s,len){
	var args=arguments;
	var offset=(args.length>2 && args[2]<parseInt(len))?args[2]:0;
	return String(s).substr(offset,len);
}
trim=_trim;

function empty(s){
	return (s.replace(/\s/gi,"").length==0);
}


// Functions
function string_to_id(s){
	return s.replace(/\s|&|#|\?|@|\||!|\^|%|\+|\*/gi,"_");
}
function alertMe(s){alert(s);return s;}
//function element(s){return document.getElementById(s);}
function getAvailableHeight(sobj,offset){
	var el=element(sobj);
	var h=h1=0;
	var bh=(!document.all)?window.innerHeight:document.body.offsetHeight;
	while(((el=el.parentNode)!=null) && el.nodeName != "BODY" ){
		h1+=parseInt(el.offsetTop);
	}
	h=(bh-h1);
	return h-((typeof(offset)=="number")?offset:0);
}
function getAvailableWidth(sobj,offset){
	var w=(!document.all)?window.innerWidth:document.body.offsetWidth+2;
	return w-((typeof(offset)=="number")?offset:0);
}

function _row_over(o){
	o.className='tr_over';
}
function _row_out(o){
	if(!o.selected)
		o.className='';
}
function _row_down(o){
	var tmp=o.parentNode.getElementsByTagName("TR");
	for(i=0;i<tmp.length;i++){
		tmp[i].className = '';
		tmp[i].selected=false;
	}
	o.selected=true;
	o.className='tr_over';
}
// returns the target element for the current event(e)
function _target(e){
	return (document.all)?event.srcElement:e.target;
}
// returns true if o is in container
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}
// disables elements of a given tag
// if a 3rd arguments is supplied it will only disable
// those elements with an attribute equal to the argument
// e.g. _elements_disable('IMG',true,'name')
// This will only disable images with the name attribute
function _elements_disable(tag, disable){
	var attrib = (arguments[2]) ? arguments[2] : false ;
	var els = document.getElementsByTagName(tag);
	for(i=0;i<els.length;i++){
		if(attrib){
			//if(tag=="IMG") alert(els[i].getAttribute(attrib))
			if(els[i].getAttribute(attrib)!=null){
				els[i].disabled = disable;
			}
		}else els[i].disabled = disable;
	}
}
// BEGIN::Application active x stuff
function change_file_location(itm,browse) {
	try{
		var s=itm.value;
		var dir=s.substring(0,s.lastIndexOf("\\"));
		file = get_filename_dialog(dir);
		if(file) 
			itm.value = file;
			itm.value = GetUNC(itm,itm.value);
	}catch(e){
		//alert(e+":"+e.description);
		browse.disabled = false;
		browse.click();
		if(browse.value!="")
			itm.value = browse.value;
			itm.value = GetUNC(itm,itm.value);
	}
}

function get_filename_dialog(){
	var dir=(arguments.length>0 && !empty(arguments[0]))?arguments[0]:top.gui.session.initial_directory;
	var filter = "Word Documents (*.doc)|*.doc|Excel (*.xls)|*.xls|Data Files (*.csv)|*.csv|Text Files (*.txt)|*.txt|Adobe Acrobat Files (*.pdf)|*.pdf";
//	var filter = "Word Documents (*.doc)|*.doc|Excel (*.xls)|*.xls|Data Files (*.csv)|*.csv|Text Files (*.txt)|*.txt";
	// Get the file using the common open dialog
	var x = top.gui.dlg;
	x.Filter = filter;
	x.FilterIndex = 1;
	x.InitialDir = dir;
	//alert(x.InitialDir)
	var file = "";
	if( x.ShowOpen() ){
		file = x.FileName;
	}
	return file;
}


function get_folder_location() {
	var x = top.gui.shell, folder, dir=(arguments.length>0 && !empty(arguments[0]))?arguments[0]:top.gui.session.initial_directory;
	if((folder=x.BrowseForFolder(0, "Choose a folder", 0, dir))==null) return '';

	var path=folder.Items().Item();
	return path.Path;
}
function change_folder_location(itm,browse) {
	var folder = '';
	try{
		folder = get_folder_location(itm.value);
		if(folder) itm.value = folder;
	}catch(e){
		browse.disabled = false;
		browse.click();
		if(browse.value!=""){
			var v = browse.value
			itm.value = v.substring(0,v.lastIndexOf("\\"));
		}
	}

}
function get_value(itm){
	var v='';
	switch(itm.nodeName){
		case 'INPUT':
			v = itm.value;
			break;
		case 'SPAN': case 'DIV': case 'A':
			v = itm.innerHTML;
			break;
	}
	return v;
}
function open_folder(itm){
	var v = get_value(itm);
	try{
		var x = top.gui.shell;
		//x.Open(v);
		x.Explore(v);
	}catch(e){
		alert(v + " does not appear to be a valid folder.");
	}
}
function load_document(itm,browse) {
	var file = get_value(itm);
	var ext = file.substring(file.lastIndexOf("."));
	window.open(file,'CATS_FILE');
	return;


	try{
		switch (ext) {
			case ".doc":
				var x = new ActiveXObject("Word.Application");
				x.Visible = true;
				x.Documents.Open(file);
				break;
			case ".docx":
				var x = new ActiveXObject("Word.Application");
				x.Visible = true;
				x.Documents.Open(file);
				break;
			case ".xls":
				var x = new ActiveXObject("Excel.Application");
				x.Visible = true;
				x.Workbooks.Open(file);
				break;
			case ".xlsx":
				var x = new ActiveXObject("Excel.Application");
				x.Visible = true;
				x.Workbooks.Open(file);
				break;
			case ".ppt":
				var x = new ActiveXObject("PowerPoint.Application");
				x.Visible = true;
				x.Presentations.Open(file);
				break;
			case ".pptx":
				var x = new ActiveXObject("PowerPoint.Application");
				x.Visible = true;
				x.Presentations.Open(file);
				break;
			case ".pdf":
				var x  = new ActiveXObject("Shell.Application"); // global shell object for general shellexecute stuff - BrowseForFolder, ShellExecute etc...
				x.ShellExecute(File, "", "open", 1);
				break;
			case ".txt":
				var x = top.gui.shell;
				x.ShellExecute("notepad.exe", file, "", "open", 1);
				break;
			default:
				alert("Cannot open files of this type or file name is incorrect.");
				break;
		}
	}catch(e){
		alert("Exception: " + e.message)
		if(!empty(file)){
			browse_file_location(file);
		}
	}
}
// END::Application active x stuff
//--------------------------
function browse_file_location(file){
	if(!empty(file)){
		var prefix = "file:///";
		if(file.match(/^(file|\\\\)/))
		{
			
			try{
				window.open(prefix+file,'CATS_DOC');
				
			}catch(e){
				alert("Error opening. " + e.message);
			}
		}
		else{
			window.open(file,'CATS_DOC');
		}
	}
}

function addEvent(obj, evType, fn)
{
	if (obj.addEventListener) { obj.addEventListener(evType, fn, true); return true; }
	else if (obj.attachEvent) {  var r = obj.attachEvent("on"+evType, fn);  return r;  }
	else {  return false; }
}


function _filter(filter, list){
	var s = filter.value;
	var _ret=false;
	if(s != ""){
		if(typeof(list)=="string") list=element(list);
		var x = list.options.length;
		s = s.toLowerCase();
		var ilen = s.length;
		var sdat = s.substring(0,ilen);
		var val,dat;
		for(i=0;i<x; i++){
			val = list.options[i].text;
			dat = val.substring(0,ilen);
			dat = dat.toLowerCase();
			if(s == dat){
				list.options[i].selected = true;
				_ret=true;
				break;
			}
		}
		if(_ret) list.focus();
	}
	return _ret;
}



function print_object(o){
	var s='';for(e in o)s+='Name: '+e+" ::Value: "+_trim(o[e],100)+"\n";
	return s;
}

function showhide(itm){
	if(document.all)document.body.focus();
	tab_onclick(element('tab_button[0]'));
}
function tab_onclick(itm){
	if(typeof(itm)=="number") itm=element("tab_button["+itm+"]");
	else if(typeof(itm)=="string") itm=element(itm);
	else if(typeof(itm)=="object" && itm.blur) itm.blur();
	var b = element("tab_buttons");
	var a = b.getElementsByTagName("A");
	var id = 0;
	for(i=0;i<a.length;i++){
		if(a[i]==itm) id=i;
		a[i].className='';
		if(element('tab_panel['+(i)+']') != null)
			element('tab_panel['+(i)+']').style.display='none';
	}
	if(_m && typeof(_m.setCurrentTab)=="function") _m.setCurrentTab(id);
	element('tab_panel['+id+']').style.display='block';
	itm.className='indent';
	//return false;
}

function initTabs(id){
	//try{
		var a=(id)?id:0;
		tab_onclick(a);
	//}catch(e){alert("Error in Tab initialisation:\n"+e.description+"\n"+id+":"+a)};
}
init_tabs=initTabs;


/*********************************
Create Module Object
*********************************/
function _module(){
/*******************************
+ Editing functions object for modules
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
5. Refresh/Reload the current screen
*******************************/
	this.a=arguments;
	this.name=this.a[0];
	this.page=this.a[1];
	this.id=(this.a[2])?parseInt(this.a[2]):0;
	this.doc=(this.a[3])?this.a[3]:document;
	this.currentTab=0;
	this.setCurrentTab=function(id){
		this.currentTab=id;
	}
	// declare iframe names/targets
	this.targets = new Object();
	this.targets.results = "screens_"+this.name+"_results";
	this.targets.edit = "screens_"+this.name+"_edit";

	this.getReturnQry = function(){
		var a=arguments;
		var s="rm="+((a.length>0 && a[0])?a[0]:this.name);
		s+="&rp="+((a.length>1 && a[1])?a[1]:this.page);
		s+="&rid="+((a.length>2 && a[2])?a[2]:this.id);
		s+="&rt="+((a.length>3 && a[3])?a[3]:this.currentTab);
		return s;
	}
	// edit the module
	this.edit = function(){
		var a=arguments;
		this.ev('edit',a);
	}
	this.view = function(){
		var a=arguments;
		this.ev('view',a);
	}
	this.ev = function(){
		var a=arguments;
		//alert(a[1].length)
		var args=a[1];
		switch(a[0]){
			case 'view':
				func=top.view;
				break;
			case 'printpreview':
				func=top.printpreview;
				break;
			default:
				func=top.edit;
		}
		//var func=a=='view'?top.view:top.edit;
		switch(args.length){
		case 1:
			func(this.name,this.id,args[0]);
			break;
		case 2:
			func(this.name,this.id,args[0],args[1]);
			break;
		case 3:
			func(this.name,args[0],args[1],args[2]);
			break;
		case 4:
			func(args[0],args[1],args[2],args[3]);
			break;
		default:
			func(this.name,this.id);
		}
	}
	this.edit_child = function(m,id){
		this.edit(id,'edit',this.getReturnQry());
	}
	this.edit_parent = function(m,id){
		this.edit(id,'edit',this.getReturnQry());
	}
	// open the new module screen in the editing iframe
	this.newModule = function(){
		var args=arguments;
		var page = (args.length>1)?args[1]:'edit';
		if(args.length>0){
			top.show_edit_screen(args[0],"index.php?m="+args[0]+"&p=new&a=add&rm="+this.name+"&rp="+page+"$rid="+this.id+"&rt="+this.currentTab);
		}else
			top.show_edit_screen(this.name,"index.php?m="+this.name+"&p=new&a=add");
	}

	this.newPage = function(p){
		top.show_edit_screen(this.name,"index.php?m="+this.name+"&p="+p+"&a=add");
	}

	this.newAction = function(){
//"index.php?m=actions&p=new&a=add&REPORT_ID=$id&ORIGIN_TABLE=Originating_Meeting&REGISTER_ORIGIN=Meeting Minutes&rm=$m&rp=edit&rid=$id"
		top.show_edit_screen("actions","index.php?m=actions&p=new&a=add&"+this.getReturnQry());
	}

	this.newSchedule = function(){
		top.show_edit_screen("schedules","index.php?m=schedules&p=new&a=add&"+this.getReturnQry());
	}

	// post current form info
	this.saveModule = function(){
		var f = this.doc.forms.module;
		btn = f.elements["cats::Save"];
		if(btn) {
			btn.click();
		}else	f.submit();
	}
	function get_selected_element(itm){
		//alert(itm.type)
		switch(itm.type){
			case 'select-one':
				return itm.options[itm.selectedIndex].value;
				break;
			case 'select-multiple':
				var i=0;
				for(i=0;i<itm.options.length;i++){

				}
				return itm.value;
				break;
			case 'checkbox': case 'radio':
				if(itm.checked) return itm.value;
				else return '';
				break;
			default:
				return itm.value;
		}
	}
	function option_toggle(itm,attrib,value){
		var a=arguments;
		var i=j=0;
		for(i=3;i<a.length;i++){
			for(j=0;j<itm.options.length;j++){
				if(a[i]==itm[j].value) {
					itm[j].disabled=value;
					itm[j].style.color = (value)?"graytext":"menutext";
				}
			}
		}
		//alert(itm.innerHTML);
		//option_toggle(itm,'disabled',true,'Action::Save...','Action::Delete...');
	}
	this.current_template = 0;
	this.saveTemplate = function(itm){
		var val=itm.value;
		if(val=='')return;
		var func = 'get_edit_template';
		remote.module = this.name;

		if(val.indexOf("Action::")!=-1){
			var url = cats_get_path(CATS_REMOTE_PATH,"application");
			var s="";
			var f = this.doc.forms[0].elements;
			var w=window.open("","new")
			w.document.open();
			w.document.write(remote._query(f));
			w.document.close();
			for(i=0;i<f.length;i++){
				if(f[i].name && f[i].name.indexOf("cats::")==-1) {
					s+=""+f[i].name+"="+get_selected_element(f[i])+"&";
				}
			}
			if(val=="Action::New..."){
				name=prompt("Type a name for the template:","");
				if(name == null || name=='' || name == 'null') return;
				s+="TEMPLATE_NAME="+name;
				func = (this.page!='search')?'new_edit_template':'new_search_template';
				remote.get_options_add(func,'application',val,itm,itm,s.replace(/&$/,""));
			}else if(val=="Action::Save..."){
				func = (this.page!='search')?'save_edit_template':'save_search_template';
				remote.get_options_add(func,'application',this.current_template,itm,itm,s.replace(/&$/,""));
			}else{
				func = (this.page!='search')?'delete_edit_template':'delete_search_template';
				remote.get_options_add(func,'application',this.current_template,itm,itm,s.replace(/&$/,""));
			}
			option_toggle(itm,'disabled',true,'Action::Save...','Action::Delete...');
		}else{
			if(!isNaN(val)) {
				option_toggle(itm,'disabled',false,'Action::Save...','Action::Delete...');
				this.current_template = val;
			}
			this.loadTemplate(itm);
		}
	}
	this.loadTemplates = function(itm){
		if(itm){
			remote.module = this.name;
			var func = (this.page!='search')?'get_edit_templates_array':'get_search_templates_array'
			remote.get_options_add(func,'application',itm.options[itm.selectedIndex].value,itm,itm);
		}
	}
	this.loadTemplate = function(itm){
		remote.module = this.name;
		var obj={
			'a':(this.page!='search'?'get_edit_template':'get_search_template'),
			'm':'application',
			'n':itm.id,
			'p':itm.options[itm.selectedIndex].text,
			'id':itm.options[itm.selectedIndex].value,
			'rm':this.name
		};
		var o=remote.get_sync('application',obj);
		var e=o.options;//alert(e.options);
		var f = this.doc.forms[0].elements;
		var el=null;
		for(i=0;i<e.length;i++){
			if((el=f[e[i][0]])==null)
				el=f[e[i][0]+"[]"];
			if(el==null) continue;
			v=e[i][1];
			if(typeof(v)=="object") {
				//alert(v.length)
				for(j=0;j<v.length;j++){
					if(v[j][1]!='') el[j].checked=true;
				}
			}else{
				switch(el.type){
					case 'select-one':
						for(j=0;j<el.options.length;j++) if(el[j].value==v) {
							el[j].selected=true;
							break;
						}
						break;
					case 'checkbox': case 'radio':
						for(j=0;j<el.length;j++) if(el[j].value==v){
							el[j].checked=true;
						}
						break;
					default:
						el.value=v;
				}
			}
		}
	}
	// return to search screen or if one doesn't exist
	// create one automagically
	this.cancel = function(){
		//alert(top.gui.session.history.all);
		var a=top.gui.session.history.all;
		var hcnt = 1;
		/*var list=a.slice(a.length-2,-1);
		//alert(list[0][0])
		var hist=list[0];
		if(hist[0]==this.name && hist[1]==this.page){
			a=top.gui.session.history.all.pop();
			hcnt = 2;
		}
		if(hist[0]=='pcr_search' && hist[1]=='edit'){
			a=top.gui.session.history.all.pop();
			hcnt = 2;
		}*/
		//alert(hcnt+":"+hist[0]+"\n"+hist[1]);
		top.gui.session.history.back(hcnt);

		//top.show_screen(this.name,'search');
	}
	// deletes the current record if an id exists
	this.deleteModule = function(){
		if(this.id>0){
			if(confirm("Are you sure you want to delete this record?"))
				top.show_edit_screen(this.name,"index.php?m="+this.name+"&p=post&a=del&id="+this.id);
		}else{
			alert("Current record does not have an ID and therefore cannot be deleted.");// Performing such an operation might delete all records");
		}
	}
	// refresh the current modules details by reloading the page
	this.refresh = function(){
		this.doc.location.reload();
	}
	this.pager = function(q){
		top._show_message("Please wait while loading next page...");
		location.href = "index.php?m="+this.name+"&p=results&"+q;
	}

	this.pager_sort = function(q){
		top._show_message("Please wait while I sort your search results...");
		location.href = "index.php?m="+this.name+"&p=results&"+q;
	}

	this.search = function(btn){
		var search_type = "";
		if(btn.value=="Search") search_type = "submit";
		var f=btn.form;
		try{
			top._show_message("Please wait while loading search results...");
			toggle_search();
			var rwin=element(this.targets.results);
			var rdoc=rwin.contentWindow.window.document;
			rdoc.body.firstChild.style.display='none';
		}catch(e){}

		return true;
	}
	this.report = function(btn){
		var fld = btn.form.elements['cats::report_fields'];
		var o = showModalWindow("includes/helpers/fieldchooser.php?m="+this.name,{'module':this.name},getDialogFeatures(600,450));
		// var o = window.open("includes/helpers/fieldchooser.php?m="+this.name,"ezw_popup",getDialogFeatures(600,450));
		try {
			o.addEventListener("beforeunload", function(e){
			   	var ret = o.oReturn.action;
		   		if(ret){
		   			fld.value="";
		   			var i=0;
		   			var x=o.oReturn.a.length;

		   			for(i=1;i<x;i++){
		   				fld.value+=o.oReturn.a[i]+",";
		   			}
		   			fld.value=fld.value.replace(/,$/,"");
		   			btn.type = "submit";
		   			btn.click();
		   			btn.type = "button";
		   		}
			}, false);
		} catch (error) {
			o.attachEvent("onbeforeunload", function(e){
			   	var ret = o.oReturn.action;
		   		if(ret){
		   			fld.value="";
		   			var i=0;
		   			var x=o.oReturn.a.length;

		   			for(i=1;i<x;i++){
		   				fld.value+=o.oReturn.a[i]+",";
		   			}
		   			fld.value=fld.value.replace(/,$/,"");
		   			btn.type = "submit";
		   			btn.click();
		   			btn.type = "button";
		   		}
			}, false);
		}
	}

	//--------------------------------
	// used from the grid actioning dropdown.
	this.select_action = function(itm){
		var f=itm.form;
		var val=itm.options[itm.selectedIndex].value;
		var chks = f.elements["cats::act_chk[]"];
		var i=0,x=chks.length;
		var ok = false;
		var itm_action=f.elements[itm.name+"_act"];
		if(itm_action) itm_action.value = itm.options[itm.selectedIndex].value;
		if(x>0){
			for(i=0;i<x;i++){
				if(chks[i].type=="checkbox" && chks[i].checked) {
					ok=true;
					break;
				}
			}
		}else ok=chks.checked;
		if(ok){
			switch(val){
				case 'del':
					if(confirm("Are you sure you want to delete these records?")){
						f.setAttribute("action","index.php?m="+this.name+"&p=post&a="+itm.name);
						f.submit();
					}
					break;
				default:
					alert("This option is not available yet");
					return false;
			}
		}else{
			alert("Please select item/s from the list by checking the boxes and then try again.");
			itm.selectedIndex = 0;
			return false;
		}
	}
	this.print = function(itm){
		var f=itm.form;
		var button_bar=f.elements["button_panel"];
		button_bar.style.display = "none";
		print();
		button_bar.style.display = "block";
	}
	this.printpreview = function(){
		var a=arguments;
		this.ev('printpreview',a);
	}
}

/*
+------------------------------------------------------------------------
|  BEGIN:: Create Element Functions
+------------------------------------------------------------------------
*/
// returns the element with the given id or if it doesn't exist it
// creates an html element of the given tag and adds it to the document and
// returns the freshly created element
function create_element(id,tag,show){
	var el=element(id);
	if(el) return el;
	// that didn't work so lets create it instead ;)
	var o=document.createElement(tag);
	o.setAttribute('id',id);
	o.style.display = (typeof(show)=='undefined' || show==true)?'block':'none';
	var e=(arguments.length>3)?arguments[3]:document.body;
	var d=e.appendChild(o);
	return d;
}

function create_toolbar(name,html,zIndex){
	var o = null;
	var id = name+"_toolbar";
	if((o=element(id))!=null) return o;
	o=create_element(id,'div');//document.createElement('div');
	//o.setAttribute('id',id);
	o.style.position="absolute";
	o.style.top=TOOLBAR_Y;
	o.style.left=0;
	o.style.width="100%";
	o.style.height=TOOLBAR_HEIGHT;
	o.className="toolbar";
	if(html) o.innerHTML=html;
	if(zIndex) o.style.zIndex = zIndex;
	return document.body.appendChild(o);
}

function create_iframe(el_id,url,w,h,x,y,z,show){
	var o=null;
	if((o=element(el_id))!=null){
		if(w) o.style.width=w;
		if(x) o.style.left=x;
		alert("already created")
		return o;
	}
	if (document.createElement) {
		try {
			o=create_element(el_id,"iframe",show);
			o.setAttribute("src", url);
			o.style.position="absolute";
			o.style.top=y;
			o.style.left=x;
			o.style.backgroundColor='#ffffff';
			o.style.width=w;
			o.style.height=h;
			if(z) o.style.zIndex = z;
			return o;
		} catch(exception) {
			// This is for IE5 PC, which does not allow dynamic creation
			// and manipulation of an iframe object. Instead, we'll fake
			// it up by creating our own objects.
			// alert message while developing/testing
			alert("Error: "+exception.number+"\nDescription: "+exception.description+"\n\nPlease upgrade your browser to Internet Explorer 6 or later");
			element("main_screens").innerHTML+=('<iframe src="'+url+'" id="'+el_id+'" name="'+el_id+'" style="position:absolute;top:'+y+'px;left:'+x+'px;width:'+w+'px;height:'+h+'px;"></iframe>');

			var o = new Object();
			o.document = new Object();
			o.document.location = new Object();
			o.document.location.iframe = document.getElementById(el_id);
			o.document.location.replace = function(location) {
				this.iframe.src = location;
			}
			return o;
		}
	}
	return false;
}

// removes a node from the document
function remove_element(el_id){
	if((el=element(el_id))!=null){
		try{
			el.removeNode(true);
		}catch(exception){
			try{
				el.__parent__.removeChild(el);
			}catch(e){
				alert(e+"\n"+exception);
			}
		}
	}
}



// attempt to try and seperate headers from table body to create scrolling table body while headers stay fixed
function fix_table(group,offset_in){
	var offset = (offset_in)?offset_in:14;
	var tid = group+"_table";
	var hid = group+"_table_hdr";
	var hcells = element(hid).rows[0].cells;
	var row=element(tid).rows[0];
	var fcells = row.cells;
	var i=0
	for (i = 0; i < fcells.length; i++) {
		hcells[i].width = fcells[i].offsetWidth-offset;
	}
}

// Error Formatter
function format_error(s,e){
	var args=arguments;
	s+=":\n";
	if(IE){
		s+=e.description+"\n";
	}else s+=e+"\n";
	if(args.length>1){
		var i=1;
		s+="Arguments:\n"
		for(i=2;i<args.length;i++)s+=(i)+":"+format_arguments(args[i])+"\n";
	}
	alert(s);
}
function format_arguments(a){
	var s='',e=null;
	switch(typeof(a)){
		case "object":
			for(e in a)s+="\t"+e+":"+a[e]+"\n";
			break;
		default:
			s=a;
	}
	return s;
}