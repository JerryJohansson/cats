
	// Because of the old framework, and failure to work in Windows 8, the date popup was replaced.
	// To get the backend to work with a hidden field, with another date format, this function is used.
	function setHiddenDateTime(field)
	{
		var incDate_d = $("#" + field + "_d").val();
		var n = incDate_d.indexOf(" ");
		var dateStr = incDate_d.substring(0,n);
		var timeStr = incDate_d.substring(n + 1);
		n = incDate_d.indexOf("-");
		var dayStr = dateStr.substring(0,n);
		var monthYearStr = dateStr.substring(n + 1); 
		n = monthYearStr.indexOf("-");
		var monthStr = monthYearStr.substring(0,n);
		var yearStr = monthYearStr.substring(n + 1);
		
		var monthNo = '0';
		if(monthStr == 'Jan')
			monthNo = '01';
		else if(monthStr == 'Feb')
			monthNo = '02';
		else if(monthStr == 'Mar')
			monthNo = '03';
		else if(monthStr == 'Apr')
			monthNo = '04';
		else if(monthStr == 'May')
			monthNo = '05';
		else if(monthStr == 'Jun')
			monthNo = '06';
		else if(monthStr == 'Jul')
			monthNo = '07';
		else if(monthStr == 'Aug')
			monthNo = '08';
		else if(monthStr == 'Sep')
			monthNo = '09';
		else if(monthStr == 'Oct')
			monthNo = '10';
		else if(monthStr == 'Nov')
			monthNo = '11';
		else if(monthStr == 'Dec')
			monthNo = '12';

		var newFormat = yearStr + "-" + monthNo + "-" + dayStr + " " + timeStr;
		$("#" + field).val(newFormat);
	}

	function setHiddenDate(field)
	{
		var incDate_d = $("#" + field + "_d").val();
		if(incDate_d == null)
			return;
		
		var dateStr = incDate_d.substring(0);
		var n = incDate_d.indexOf("-");
		var dayStr = dateStr.substring(0,n);
		var monthYearStr = dateStr.substring(n + 1); 
		n = monthYearStr.indexOf("-");
		var monthStr = monthYearStr.substring(0,n);
		var yearStr = monthYearStr.substring(n + 1);
		
		var monthNo = '0';
		if(monthStr == 'Jan')
			monthNo = '01';
		else if(monthStr == 'Feb')
			monthNo = '02';
		else if(monthStr == 'Mar')
			monthNo = '03';
		else if(monthStr == 'Apr')
			monthNo = '04';
		else if(monthStr == 'May')
			monthNo = '05';
		else if(monthStr == 'Jun')
			monthNo = '06';
		else if(monthStr == 'Jul')
			monthNo = '07';
		else if(monthStr == 'Aug')
			monthNo = '08';
		else if(monthStr == 'Sep')
			monthNo = '09';
		else if(monthStr == 'Oct')
			monthNo = '10';
		else if(monthStr == 'Nov')
			monthNo = '11';
		else if(monthStr == 'Dec')
			monthNo = '12';

		var newFormat = yearStr + "-" + monthNo + "-" + dayStr;
		$("#" + field).val(newFormat);
	}
	