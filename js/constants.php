<?php
$config_details_only=true;
$cats_maintenance = false;
require_once("../includes/conf.php");

?>
// Constants
var CATS_SUPER_ADMINISTRATOR=(524288+9437184);
var CATS_ADMINISTRATOR=(1024+9437184);
var CATS_ACTION_ADMINISTRATOR=(4096+9437184);

var _cats_dbg = false;
var IFRAME_LOADER = 'loader';
var IFRAME_SESSION = 'session_dialog';
var IFRAME_MESSAGE = 'message_dialog';
var IFRAME_ALERT = 'alert_dialog';

var CATS_PATH="<?php echo(WS_PATH);?>";
var CATS_STYLE_PATH="<?php echo(WS_STYLE_PATH);?>";
var CATS_ICONS_PATH="<?php echo(WS_ICONS_PATH);?>";
var CATS_IMAGES_PATH="<?php echo(WS_IMAGES_PATH);?>";

var CATS_REMOTE_PATH="<?php echo(WS_REMOTE_PATH);?>";
var CATS_HELPER_PATH="<?php echo(WS_HELPER_PATH);?>";
var CATS_CLASSES_PATH="<?php echo(WS_CLASSES_PATH);?>";

var HISTORY_MIN_LENGTH=<?php 
if(isset($_SESSION['user_details']['HISTORY_MIN_LENGTH'])){
	echo($_SESSION['user_details']['HISTORY_MIN_LENGTH']);
} else {
	echo("0");
}
/*
var _status_message = "status_message";
var _tab_toolbar = "tab_toolbar";
var _tab_toolbar_cont = "toolbar";
var _top_bar = "top_logo";
var _message = "loading_message";

var _HELPERS="_helpers";
var _COMBO_MENU="_combos";
var _COMBO_LIST="_combo_list";
var _COMBO_MULTI="_combo_multi";

var ACTIVE_HELPER;

var IDX_DEFAULT=IDX_INTERVAL=0;
var IDX_NAME=<?php echo(IDX_NAME); ?>;
var IDX_TYPE=<?php echo(IDX_TYPE); ?>;
var IDX_LENGTH=<?php echo(IDX_LENGTH); ?>;
var IDX_VALUE=<?php echo(IDX_VALUE); ?>;
var IDX_COLUMNS=<?php echo(IDX_COLUMNS); ?>;
var IDX=null;
*/
?>;