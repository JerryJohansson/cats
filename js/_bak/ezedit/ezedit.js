//init variables
var isRichText = false;
var ezeRng;
var current_ezEdit;
var ezePath = '/cats/js/ezedit/';
var ezeImgPath = ezePath + 'icons/';
var ezeIndex = 0;
var ezeArray = new Array();
if(document.all) document.onmousedown = hide_ezePopups;
else onmousedown = hide_ezePopups;
function init_ezEdit() {
	//check to see if designMode mode is available
	if (document.getElementById) {
		if (document.all) {
			//check for internet explorer 5.5+
			if (document.URLUnencoded) isRichText = true;
		} else {
			//check for browsers that support designmode
			if (document.designMode) isRichText = true;
		}
	}
}
// Toolbar functions
function init_ezEditToolbar(id){
	var mb = document.getElementById(id);
	var s=document.body.innerHTML;
	window.open("","shite").document.write(s);
	alert(mb)
	if(mb){
		var aa = mb.getElementsByTagName("A");
		for(i=0;i<aa.length;i++){
			a = aa[i];
			a.setAttribute("css",a.className);
			if(a.className=="disabled"){
				a.setAttribute("disabled","true");
			}else{
				var oldClick = (a.onclick)?a.onclick:function(){};
				switch(a.getAttribute("tb")){
					case "toggle":
						if(a.innerHTML.indexOf("Design")!=-1) mb.activeTab=a;
						a.onclick=function(){
							if(this.parentNode.activeTab == this) return false;
							this.parentNode.activeTab.className = "";
							this.className = "pressed";
							this.parentNode.activeTab = this;
							oldClick();
							this.blur();
						}
						break;
					case "togglebutton":
						a.onclick=function(){
							this.blur();
							if(this.className=="pressed")
								this.className = this.css;
							else
								this.className = "pressed";
							oldClick();
						}
						break;
					case "button":
						a.onclick=function(){
							this.blur();
							oldClick();
						}
						break;
				}
			}
		}
	}
}

// buttons/icons coolbutton styles
function eze_over(eButton){
	eButton.style.borderBottom = "buttonshadow solid 1px";
	eButton.style.borderLeft = "buttonhighlight solid 1px";
	eButton.style.borderRight = "buttonshadow solid 1px";
	eButton.style.borderTop = "buttonhighlight solid 1px";
}
			
function eze_out(eButton){
	eButton.style.borderColor = "threedface";
}

function eze_char_out(eButton){
	eButton.style.borderColor = "#666666";
}

function eze_down(eButton){
	eButton.style.borderBottom = "buttonhighlight solid 1px";
	eButton.style.borderLeft = "buttonshadow solid 1px";
	eButton.style.borderRight = "buttonhighlight solid 1px";
	eButton.style.borderTop = "buttonshadow solid 1px";
}

function eze_up(eButton){
	eButton.style.borderBottom = "buttonshadow solid 1px";
	eButton.style.borderLeft = "buttonhighlight solid 1px";
	eButton.style.borderRight = "buttonshadow solid 1px";
	eButton.style.borderTop = "buttonhighlight solid 1px";
	//eButton = null; 
}

// choose the editor to display on the page
function ezEdit(eze, html, page, width, height, buttons) {
	if (isRichText) {
		write_ezEdit(eze, page, html, width, height, buttons);
	} else {
		write_ezText(eze, page, html, width, height, buttons);
	}
}

// display no frills textarea to edit html
function write_ezText(eze, page, html, width, height, buttons) {
	document.writeln('<textarea name="' + eze + '" id="' + eze + '" style="width: ' + width + 'px; height: ' + height + 'px;">' + html + '</textarea>');
}

function get_ezeIFRAME(name, file){
	return '<iframe id="eze'+name+'" height="1" src="'+file+'" marginwidth="0" marginheight="0" border="0" frameborder="no" scrolling="no" style="visibility:hidden; position: absolute;"></iframe>';
}
// display ezEditor
function write_ezEdit(eze, page, html, width, height, buttons) {
	var ezePage = (page!='')?page:ezePath + 'blank.php?';
	ezePage += "e="+eze+"&i="+ezeIndex;
	//alert(eze + "\n" + page + "\n" + html +"\n"+ width +"\n" + height + "\n" + buttons + "\n" + ezePage)
	//ezePage = '';
	if (typeof(current_ezEdit)=="undefined") {
		document.write('<link rel="stylesheet" href="' + ezePath + 'ezedit.css" type="text/css" />');
		document.write(get_ezeIFRAME('Color', ezePath + 'palette.htm'),
									 get_ezeIFRAME('Char', ezePath + 'special.htm'));
	}
	document.writeln('<table id="toolbar_' + eze + '" width="100%" class="toolbarmain"><tr><td>');
	if (buttons == true) {
		document.writeln('<div id="tb1_' + eze + '" class="toolbar">');
		document.write('<span><select id="formatblock" onchange="set_ezeSelect(\'' + eze + '\', this.id);">',
				'<option value="<p>">Normal</option>',
				'<option value="<p>">Paragraph</option>',
				'<option value="<h1>">Heading 1 <h1></option>',
				'<option value="<h2>">Heading 2 <h2></option>',
				'<option value="<h3>">Heading 3 <h3></option>',
				'<option value="<h4>">Heading 4 <h4></option>',
				'<option value="<h5>">Heading 5 <h5></option>',
				'<option value="<h6>">Heading 6 <h6></option>',
				'<option value="<address>">Address <ADDR></option>',
				'<option value="<pre>">Formatted <pre></option>',
				'</select></span>');
		document.write('<span><select id="fontname" name="selectFont" onchange="set_ezeSelect(\'' + eze + '\', this.id)">',
				'<option value="Font" selected>Font</option>',
				'<option value="Arial, Helvetica, sans-serif">Arial</option>',
				'<option value="Courier New, Courier, mono">Courier New</option>',
				'<option value="Times New Roman, Times, serif">Times New Roman</option>',
				'<option value="Verdana, Arial, Helvetica, sans-serif">Verdana</option>',
				'</select></span>');
		document.write('<span><select unselectable="on" id="fontsize" onchange="set_ezeSelect(\'' + eze + '\', this.id);">',
				'<option value="Size">Size</option>',
				'<option value="1">1</option>',
				'<option value="2">2</option>',
				'<option value="3">3</option>',
				'<option value="4">4</option>',
				'<option value="5">5</option>',
				'<option value="6">6</option>',
				'<option value="7">7</option>',
				'</select></span>');
		document.write('<span><select unselectable="on" id="styles'+eze+'" onchange="set_ezeStyle(\''+eze+'\',this[this.selectedIndex].value);get_ezEdit(\''+eze+'\').focus();this.selectedIndex=0;" style="width:80px;">',
				'<option selected>Style</option><option value="">None</option>',
				'</select></span>');
		document.write('<span><a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'cut.gif" alt="Cut" title="Cut" onClick="set_ezeCmd(\'' + eze + '\', \'cut\')"></a>',
				'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'copy.gif" alt="Copy" title="Copy" onClick="set_ezeCmd(\'' + eze + '\', \'copy\')"></a>',
				'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'paste.gif" alt="Paste" title="Paste" onClick="set_ezeCmd(\'' + eze + '\', \'paste\')"></a>',
				'<span class="sep"></span>',
				'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'undo.gif" alt="Undo" title="Undo" onClick="set_ezeCmd(\'' + eze + '\', \'undo\')"></a>',
				'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'redo.gif" alt="Redo" title="Redo" onClick="set_ezeCmd(\'' + eze + '\', \'redo\')"></a>',
				'</span>');
		document.write('</div><hr>');
		document.writeln('<div id="tb2_' + eze + '" class="toolbar">',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'bold.gif" alt="Bold" title="Bold" onClick="set_ezeCmd(\'' + eze + '\', \'bold\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'italic.gif" alt="Italic" title="Italic" onClick="set_ezeCmd(\'' + eze + '\', \'italic\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'underline.gif" alt="Underline" title="Underline" onClick="set_ezeCmd(\'' + eze + '\', \'underline\', \'\')"></a>',
			'<span class="sep"></span>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'left.gif" alt="Align Left" title="Align Left" onClick="set_ezeCmd(\'' + eze + '\', \'justifyleft\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'center.gif" alt="Center" title="Center" onClick="set_ezeCmd(\'' + eze + '\', \'justifycenter\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'right.gif" alt="Align Right" title="Align Right" onClick="set_ezeCmd(\'' + eze + '\', \'justifyright\', \'\')"></a>',
			'<span class="sep"></span>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'numlist.gif" alt="Ordered List" title="Ordered List" onClick="set_ezeCmd(\'' + eze + '\', \'insertorderedlist\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'bullist.gif" alt="Unordered List" title="Unordered List" onClick="set_ezeCmd(\'' + eze + '\', \'insertunorderedlist\', \'\')"></a>',
			'<span class="sep"></span>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'outdent.gif" alt="Outdent" title="Outdent" onClick="set_ezeCmd(\'' + eze + '\', \'outdent\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'indent.gif" alt="Indent" title="Indent" onClick="set_ezeCmd(\'' + eze + '\', \'indent\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'fgcolor.gif" id="forecolor'+eze+'" alt="Foreground Color" title="Foreground Color" onClick="set_ezeCmd(\'' + eze + '\', \'forecolor\', \'\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'bgcolor.gif" id="hilitecolor'+eze+'" alt="Background Color" title="Background Color" onClick="set_ezeCmd(\'' + eze + '\', \'hilitecolor\', \'\')"></a>');
		if (document.all) document.write('<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'chars.gif" id="special'+eze+'" alt="Insert Special Characters" title="Insert Special Characters" onClick="set_ezeCmd(\'' + eze + '\', \'special\', \'\')"></a>');
		document.write('<span class="sep"></span>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'hyperlink.gif" alt="Insert Link" title="Insert Link" onClick="set_ezeCmd(\'' + eze + '\', \'createlink\')"></a>',
			'<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'image.gif" alt="Add Image" title="Add Image" onClick="AddImage(\'' + eze + '\')"></a>');
		if (document.all) document.write('<a href="javascript:{}" tb="button"><img src="' + ezeImgPath + 'spellcheck.gif" alt="Check Spelling" title="Check Spelling" onClick="checkspell()"></a>');
		document.write('</div>');
	}
	document.writeln('	</td></tr><tr><td bgcolor="#ffffff" width="' + width + '" height="' + height + '">');
	document.writeln('<iframe id="' + eze + '" class="ezedit" width="100%" height="100%" src="' + ezePage + '"></iframe>');
	document.writeln('	</td></tr><tr><td height=1 class="ttcont">');
	document.write('<div id="tabs' + eze + '" class="toolbar"><a href="javascript:toggleHTMLSrc(false, \'' + eze + '\');" tb="toggle" class="pressed"><img src="' + ezeImgPath + 'design.gif">&nbsp;&nbsp;Design</a>',
			'<a href="javascript:toggleHTMLSrc(true, \'' + eze + '\');" tb="toggle"><img src="' + ezeImgPath + 'html.gif">&nbsp;&nbsp;HTML</a>');
	if (document.all){}else{document.write('<input checked type="checkbox" onclick="useCSS(\'' + eze + '\',this.checked)" /><span style="color:#333">Use CSS</span>');}
	document.write('<div id="statusbar' + eze + '" align="right"></div></div>');
	document.writeln('</td></tr></table>');	
	init_ezEditToolbar('tabs' + eze );
	//ezeArray[ezeIndex++]=[eze,html];
}
function get_ezEdit(eze,el){
	var d=null;
	var elPath = (el)?"." + el:"";
	try{
		d=eval("document.getElementById('" + eze + "').contentWindow" + elPath);
	}catch(e){}
	//alert(d);
	return d;
}

function get_ezeHTML(nEzeIndex){
	alert(ezeArray)
	return ezeArray[nEzeIndex][1];
}
function enableDesignMode(eze,index) {
	alert("enableDesignMode line 220")
	//setTimeout("set_ezeDesignMode('" + eze + "','" + index + "');", 500);
	//display_ezeStyles(eze);
	if (document.all){}else{
		get_ezEdit(eze).document.addEventListener("keypress", kb_handler, true);
	}
}
function set_ezeDesignMode(eze,index){
	alert("set desginMode")
	get_ezEdit(eze).document.designMode = 'On';
	setTimeout("set_ezeEditorHTML('" + eze + "','" + index + "');",2000);
}
function set_ezeEditorHTML(eze,index){
	alert("setEditorHTML")
	get_ezEdit(eze).document.body.innerHTML = get_ezeHTML(index);
}

function update_ezEditStatus(eze,s) {
	document.getElementById("statusbar"+eze).innerHTML = "<span class=body>" + s + "</span>&nbsp;"
}
function update_ezEditAll(){
	var i=0;
	for(i=0;i<ezeIndex;i++) update_ezEdit(ezeArray[i][0]);
	return true
}
function update_ezEdit(eze) {
	//set message value
	var oHdnMessage = document.getElementById(eze);
	var oMessageFrame = get_ezEdit(eze);
	
	if (isRichText) {
		if (oHdnMessage.value == null) oHdnMessage.value = "";
		oHdnMessage.value = oMessageFrame.document.body.innerHTML;
		//exception for Mozilla
		if (oHdnMessage.value.indexOf('<br>') > -1 && oHdnMessage.value.length == 8) oHdnMessage.value = "";
	}
}

function toggleHTMLSrc(bOn,eze) {
	var doc = document.getElementById(eze).contentWindow.document;
	if (bOn) {
		document.getElementById("tb1_" + eze).style.visibility = "hidden";
		document.getElementById("tb2_" + eze).style.visibility = "hidden";
		if (document.all) {
			doc.body.innerText = doc.body.innerHTML;
		} else {
			var htmlSrc = doc.createTextNode(doc.body.innerHTML);
			doc.body.innerHTML = "";
			doc.body.appendChild(htmlSrc);
		}
	} else {
		document.getElementById("tb1_" + eze).style.visibility = "visible";
		document.getElementById("tb2_" + eze).style.visibility = "visible";
		if (document.all) {
			doc.body.innerHTML = doc.body.innerText;
		} else {
			var htmlSrc = doc.body.ownerDocument.createRange();
			htmlSrc.selectNodeContents(doc.body);
			doc.body.innerHTML = htmlSrc.toString();
		}
	}
}
function useCSS(eze,source){
  get_ezEdit(eze).document.execCommand("useCSS", false, !(source));  
}
// Formatting and related functions below
function isAllowed(eze) {
	get_ezEdit(eze).focus();
	return true;
}
function hide_ezePopups(){
	document.getElementById('ezeChar').style.visibility="hidden";
	document.getElementById('ezeColor').style.visibility="hidden";
}

function set_ezeHTML(html) {
	var eze = current_ezEdit;
	var doc = get_ezEdit(eze);
	doc.focus();
	if(ezeRng!=null){
		ezeRng.pasteHTML(html);
		ezeRng.select();
	}
	hide_ezePopups();
	doc.focus();
	
}
//Function to format text in the text box
function set_ezeCmd(eze, command, option) {
	current_ezEdit = eze;
	var doc=get_ezEdit(eze);
	if ((command == "forecolor") || (command == "hilitecolor") || (command == "special")) {
		parent.command = command;
		var cpID = (command=="special")?"Char":"Color";
		buttonElement = document.getElementById(command + eze);
		cpFrmElement = document.getElementById('eze'+cpID);
		cpFrmElement.style.left = (getOffsetLeft(buttonElement) - 4) + "px";
		cpFrmElement.style.top = (getOffsetTop(buttonElement) + buttonElement.offsetHeight + 4) + "px";
		cpFrmElement.style.width = cpFrmElement.contentWindow.document.body.firstChild.offsetWidth + "px";
		cpFrmElement.style.height = cpFrmElement.contentWindow.document.body.firstChild.offsetHeight + "px";
		if (cpFrmElement.style.visibility == "hidden")
			cpFrmElement.style.visibility="visible";
		else {
			cpFrmElement.style.visibility="hidden";
		}
		
		//get current selected eze
		current_ezEdit = eze;
		
		//get current selected range
		var sel = (document.all)?doc.document.selection:doc.getSelection(); 
		if (sel!=null) {
			ezeRng = (document.all)?sel.createRange():doc.document.createRange();
		}
	}	else if (command == "createlink") {
		var szURL = prompt("Enter a URL:", "");
		doc.document.execCommand("Unlink",false,null);
		doc.document.execCommand("CreateLink",false,szURL);
	} else if (((command == "cut") || (command == "copy") || (command == "paste"))&&(!document.all)) {
		alert("The '"+command+"' command is only available to MS Internet Explorer browsers.\nPlease use the following shortcut keys instead:\n" +
					"Cut: Ctrl+X\nCopy: Ctrl+C\nPaste: Ctrl+V");
	}	else {
		doc.focus();
	  doc.document.execCommand(command, false, option);
		doc.focus();
	}
}

//Function to set color
function setColor(color) {
	var parentCommand = parent.command;
	var eze = current_ezEdit;
	if(document.all && parentCommand == "hilitecolor") parentCommand = "backcolor";
	if (document.all) {
		//retrieve selected range
		var sel = get_ezEdit(eze).document.selection; 
		if (parentCommand == "hilitecolor") parentCommand = "backcolor";
		if (sel!=null) {
			var newRng = sel.createRange();
			newRng = ezeRng;
			newRng.select();
		}
	}	else {
		get_ezEdit(eze).focus();
	}
	get_ezEdit(eze).focus();
	get_ezEdit(eze).document.execCommand(parentCommand, false, color);
	get_ezEdit(eze).focus();
	hide_ezePopups();
}

//Function to add image
function AddImage(eze) {
	imagePath = prompt('Enter Image URL:', 'http://');				
	if ((imagePath != null) && (imagePath != "")) {
		get_ezEdit(eze).focus()
		get_ezEdit(eze).document.execCommand('InsertImage', false, imagePath);
	}
	get_ezEdit(eze).focus()
}

//function to perform spell check
function checkspell() {
	try {
		var tmpis = new ActiveXObject("ieSpell.ieSpellExtension");
		tmpis.CheckAllLinkedDocuments(document);
	}
	catch(exception) {
		if(exception.number==-2146827859) {
			if (confirm("ieSpell not detected.  Click Ok to go to download page."))
				window.open("http://www.iespell.com/download.php","DownLoad");
		}
		else
			alert("Error Loading ieSpell: Exception " + exception.number);
	}
}

function getOffsetTop(elm) {
	var mOffsetTop = elm.offsetTop;
	var mOffsetParent = elm.offsetParent;
	
	while(mOffsetParent){
		mOffsetTop += mOffsetParent.offsetTop;
		mOffsetParent = mOffsetParent.offsetParent;
	}
	
	return mOffsetTop;
}

function getOffsetLeft(elm) {
	var mOffsetLeft = elm.offsetLeft;
	var mOffsetParent = elm.offsetParent;
	
	while(mOffsetParent) {
		mOffsetLeft += mOffsetParent.offsetLeft;
		mOffsetParent = mOffsetParent.offsetParent;
	}
	
	return mOffsetLeft;
}

function set_ezeSelect(eze, selectname)
{
	var cursel = document.getElementById(selectname).selectedIndex;
	// First one is always a label
	if (cursel != 0) {
		var selected = document.getElementById(selectname).options[cursel].value;
		get_ezEdit(eze).document.execCommand(selectname, false, selected);
		document.getElementById(selectname).selectedIndex = 0;
	}
	get_ezEdit(eze).focus();
}

// Apply style from styles list
function set_ezeStyle(eze, styleValue) {
	if (isAllowed(eze)){
		var done, foo = get_ezEdit(eze);
		var selectedArea = foo.document.selection.createRange()
		if (styleValue != "") {
			styleValue = styleValue.substring(1, styleValue.length);
		}
	
		if (foo.document.selection.type == "Control") {
			set_ezeStyleTo = selectedArea.commonParentElement();
		}  else {
			if (foo.document.selection.createRange().htmlText == "") {
				set_ezeStyleTo = selectedArea.parentElement();
			} else {
				if ((selectedArea.parentElement().tagName.toUpperCase() == "SPAN") || (selectedArea.parentElement().tagName.toUpperCase() == "A")) {
					set_ezeStyleTo = selectedArea.parentElement();
					if ((styleValue == "") && (selectedArea.parentElement().tagName.toUpperCase() == "SPAN")) {
						set_ezeStyleTo.removeNode(false);
						done = true;
					}
				} else {
					if (styleValue != "") {
						selectedArea.pasteHTML("<span class=" + styleValue + ">" + selectedArea.htmlText + "</span>");
					}
					done = true;
				}
			}
		}
		if (done != true) {
			set_ezeStyleTo.className = styleValue;
		}
	}
}
// displays a dropdown list of the current styles available
function display_ezeStyles(eze) {
	var theStyle = new Array();
	var theStyleText = new Array();
	var styleExists,cboStyles;
	var styleValue, styleText, styleLength;
	if((cboStyles = document.getElementById("styles"+eze))==null) return false;
	data = get_ezEdit(eze);
	noOfSheets = data.document.styleSheets.length;
	if (noOfSheets > 0) {
		for (i=1;i<=noOfSheets;i++) {
			noOfStyles = (document.all)?data.document.styleSheets[noOfSheets-1].rules.length:data.document.styleSheets[noOfSheets-1].cssRules.length;
				for (x=0;x<noOfStyles;x++){
					styleValue = (document.all)?data.document.styleSheets[noOfSheets-1].rules[x].selectorText:data.document.styleSheets[noOfSheets-1].cssRules[x].selectorText;

					// stylesheet rule contains a . (ignore any styles that dont contain a . they are NOT user styles)
					if (styleValue.indexOf(".") >= 0) {

						// stylesheet rule doesnt contain :
						if (styleValue.indexOf(":") < 0) {

							styleLength = (styleValue.indexOf(",")>0)?styleValue.indexOf(","):styleValue.length;
							
							// style contains a . at beginning
							if (styleValue.indexOf(".") == 0) {
								styleText = styleValue.substring(1,styleLength);
								theStyle[theStyle.length] = styleValue;
								theStyleText[theStyleText.length] = styleText;

							} else {
								// style contains a . not at beginning
								if (styleValue.indexOf(".") > 0) {
									styleText = styleValue.substring(styleValue.indexOf(".")+1,styleLength);
									styleValue = styleValue.substring(styleValue.indexOf("."),styleValue.length);

									theStyleText[theStyleText.length] = styleText;
									theStyle[theStyle.length] = styleValue;
								}						
							}

						// contains BOTH a . and a :
						} else {
							styleValue = styleValue.substring(styleValue.indexOf("."),styleValue.indexOf(":"))
						
							for (i=0;i<theStyle.length;i++) {
								if (styleValue == theStyle[i]) {
									styleExists = true;
								}
							}
						
							if (styleExists != true) {
								theStyle[theStyle.length] = styleValue;

								styleText = styleValue.substring(styleValue.indexOf(".")+1,styleValue.length);
								theStyleText[theStyleText.length] = styleText;
							}

							styleExists = false;
						}

					}

				} // End for

				for (z=0; z <= theStyle.length-1; z++) {						
					newOption = document.createElement("option");
					newOption.value = theStyle[z];
					newOption.text = theStyleText[z];
					cboStyles.options.add(newOption);
				} 

		} // End For
	} // End if
} // End function


function kb_handler(evt, eze) {
	if (evt.ctrlKey) {
		var key = String.fromCharCode(evt.charCode).toLowerCase();
		var cmd = '';
		switch (key) {
			case 'b': cmd = "bold"; break;
			case 'i': cmd = "italic"; break;
			case 'u': cmd = "underline"; break;
		};

		if (cmd) {
			evt.target.ownerDocument.execCommand(cmd,false,true);
			// stop the event bubble
			evt.preventDefault();
			evt.stopPropagation();
		}
 	}
}

init_ezEdit();
