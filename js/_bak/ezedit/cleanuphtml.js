/////////////////////////////////////////////////////////////////////////////
// Cleanup Microsoft Words lame attempt at html
//
// Purpose
//    Big thanx goes to Macromedia for giving me the structure and outline
//    for this script. This script is based on the Clean Up Word HTML scripts
//    found in Dreamweaver MX 2004.
//
/////////////////////////////////////////////////////////////////////////////
// Function
//    ProcessWord2000
//
// Purpose
//    This is the main function for doing Word 2000 processing on the
//    document.
//
function ProcessWord2000()
{
   if(doRemoveXMLFromHTML())
      removeXMLFromHTML();
   
   if(doRemoveXMLMarkup())
      removeXMLMarkup();

   if(doRemoveIfs())
     removeIfs();

   if(doRemoveMSOStyleAttr())
      removeMSOStyleAttr();

   if(doRemoveEmptyParas())
      removeEmptyParas();

   if(doRemoveCSSFromTables())
      removeCSSFromTables();

   if(doRemoveNonCSSDeclaration())
      removeNonCSSDeclaration();

   if(doRemoveInlineCSS())
      removeInlineCSS();

   if(doRemoveUnusedStyles())
      removeUnusedStyles();

   // We are done.  Do some general cleanup
   formatCSS();
   
}


/////////////////////////////////////////////////////////////////////////////
function RemoveEmptyTags()
{
   var body = DOM.body;
   var emptyTags = new Array();
   var tag;

   // First find all of the tags that are empty inside the body.
   traverse(body, findEmptyTags, null, null, emptyTags);
  
   // Now remove them.
   while((tag = emptyTags.pop()) != null)
   {
      if (dw.nodeExists(tag)){
	     tag.outerHTML = tag.innerHTML;
      }
   }
   
   // Now deal with tags that might have attributes but no
   // contents, or that are wrapped around nothing something stupid, 
   // like a <br>.
   
   // Find all <br> tags, and check if each one's parent is a SPAN.
   // If that SPAN has no other children, it's safe to remove it.
   var brTags = DOM.getElementsByTagName('br');
   for (var i=brTags.length-1; i >=0; i--){
     if (brTags[i].parentNode.tagName == 'SPAN' && brTags[i].parentNode.childNodes.length == 1){
       brTags[i].parentNode.outerHTML = brTags[i].parentNode.innerHTML;
     }
   }
   
   // Find all <br> tags, and check if each one's parent is a B.
   // If that B has no other children, it's safe to remove it.
   var brTags = DOM.getElementsByTagName('br');
   for (var i=brTags.length-1; i >=0; i--){
     if (brTags[i].parentNode.tagName == 'B' && brTags[i].parentNode.childNodes.length == 1){
       brTags[i].parentNode.outerHTML = brTags[i].parentNode.innerHTML;
     }
   }
   
   // Find all the SPAN tags that may have style attributes but 
   // no content, and remove them.
   var spans = DOM.getElementsByTagName('span');
   for (var i=spans.length-1; i >= 0; i--){
     if (spans[i].innerHTML == ""){
       spans[i].outerHTML = spans[i].innerHTML;
     }else{
       var match = spans[i].innerHTML.match(/[^<][\w]*/);
       if (!match){
         spans[i].outerHTML == "";
       }
     }
   }
   
   // Find all the SPAN tags that may have style attributes but 
   // no content, and remove them.
   var spans = DOM.getElementsByTagName('span');
   for (var i=spans.length-1; i >= 0; i--){
     if (spans[i].innerHTML == ""){
       spans[i].outerHTML = spans[i].innerHTML;
     }else{
       var match = spans[i].innerHTML.match(/[^<][\w]*/);
       if (!match){
         spans[i].outerHTML == "";
       }
     }
   }

   // Word usually nests SPAN tags inside B tags, so once
   // all the empty SPAN tags are removed, the B tags may
   // also have no content. Remove those.
   var btags = DOM.getElementsByTagName('b');
   for (var i=btags.length-1; i >= 0; i--){
     if (btags[i].innerHTML == ""){
       btags[i].outerHTML = btags[i].innerHTML;
     }
   }
   
}

/////////////////////////////////////////////////////////////////////////////
function findEmptyTags(tag, emptyTags)
{
   var tagName = tag.tagName;
   var html;
   var regx;
   var result;

   switch(tagName.toUpperCase())
   {
      // Add new empty tags to be removed here.
      case "DIV":
      case "SPAN":
      case "FONT":

         // Do a match to see if the tag is empty (no attributes)
         // If it is add it to the list of empty tags that we
         // will remove from the doc.
         html = tag.outerHTML;
         regx = new RegExp("<"+tagName+">", "i");
         result = regx.exec(html);

         // LMH: Fixed logic error where empty tags within a
         // non-empty tag were being matched by accident.
         // if(result != null && result.index != -1)
         if(result != null && result.index == 0)
            emptyTags.push(tag);

         break;
   }
   
   switch(tagName.toUpperCase())  // fix for bug 24752
   {
     case "FONT":{
       if (!tag.innerHTML || tag.innerHTML==" "){
         emptyTags.push(tag);
         if (tag.parentNode.tagName == "P"){
	       tag.parentNode.innerHTML = tag.outerHTML
	       tag.parentNode.setAttribute("cleanup","dwDelete");
	     }
       }
       break;
     }
     case "P":
       if (tag.getAttribute("CLEANUP") == "dwDelete")
         emptyTags.push(tag);
       break;
   }

   return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    ProcessWord97
//
// Purpose
//    This is the main function for doing Word 97 processing on the
//    document.
//
function ProcessWord97()
{
   if(doRemoveMetaLink())
      removeMetaLink();

   convertFontSizes();

   if(doFixInvalidNesting())
      fixInvalidNesting();
}


function convertFontSizes()
{
   traverse(DOM.documentElement, convertFontSizeHandler);

   // post processing will strip the empty <font> tags.
}


//////////////////////////////////////////////////////////////////////////////
// Function
//    convertFontSizeHandler
//
// Purpose
//    Callback that searches for font tags to convert.
//
function convertFontSizeHandler(tag)
{
   if(tag.tagName.toUpperCase() == "FONT")
   {
      var size = tag.getAttribute("size");
      var desiredSize;

      if(size != null && doConvertSize(size))
      {
         desiredSize = getDesiredFontSize(size);
         switch(desiredSize)
         {
            case "-1":  // don't change anything
               break;

            case "0":  // use default size
               tag.removeAttribute("size");
               gFontsConverted++;
               break;

            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
               tag.setAttribute("size", desiredSize);
               gFontsConverted++;
               break;

            case "h1":
            case "h2":
            case "h3":
            case "h4":
            case "h5":
            case "h6":
               // If this font tag is not contained within another block tag,
               // we can convert it to a header.  If the font is contained
               // within a block tag, it is an inline font size change.  We
               // don't want to convert those to headers since headers create
               // vertical white space.
               if(!isInsideTag(tag, "p,h1,h2,h3,h4,h5,h6"))
               {
                  // We remove the size attribute from the <font> tag
                  // and wrap the font tag and all its content with
                  // the appropriate heading.
                  tag.removeAttribute("size");

                  html = tag.outerHTML;

                  // Strip any internal <p>'s that we might have.  We don't
                  // need them since we are converting this to a header.
                  html = html.replace(/<\/?P[^>]*>/ig, "");

                  html = "<"+desiredSize+">" + html + "</"+desiredSize+">";
                  tag.outerHTML = html;

                  gFontsConverted++;

                  // Note, we could be leaving behind an empty <font> tag.
                  // But, this is OK.  The general post processing will
                  // clean these up.
               }
               break;
         }
      }
   }

   // Keep traversing...
   return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    fixInvalidNesting
//
// Purpose
//    Word 97 has no clue about HTML structure.  Most HTML documents that
//    it generates have overlapped tags, and invalid nesting structures.
//    This function aims to clean up that mess.
//
//    Note!  This is a very specialized case for Word 97.  This will
//    not fix all general cases of invalid nesting.
//
function fixInvalidNesting()
{
   traverse(DOM.documentElement, fixHandler);
   traverse(DOM.documentElement, removeMarkedTags);
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    fixHandler
//
// Purpose
//    Callback for fixing up invalidly nested tags.  This is very specific
//    to how Word 97 generates its HTML.  This will NOT fix any general
//    case of invalid HTML (that problem is actually quite difficult).
//
function fixHandler(tag, nestingFixes)
{
   var html;
   var tagName = tag.tagName.toUpperCase();

   // If this is a <p> or a header, we need to do some work.
   if(tagName == "P" || tagName == "LI" || (tagName.match(/h[1-6]/i) != null))
   {
      // Fix up them tags

      var pCase = tag.tagName;  // maintain upper/lower case
      var parent = tag.parentNode;
      var innerMostHTML = tag.innerHTML;

      while(parent != null)
      {
         if(parent.tagName)
         {
            switch(parent.tagName.toUpperCase())
            {
               case "FONT":
               case "B":
               case "I":
                  parent.removeAttribute("TO_BE_DELETED");
                  html = parent.outerHTML;
                  parent.setAttribute("TO_BE_DELETED",true);

                  // We use match here to make sure we maintain any tag attributes.
                  startTag = html.match(/<[^>]*>/);

                  if(startTag != null)
                  {
                     innerMostHTML = startTag[0] + innerMostHTML +
                        "</" + parent.tagName + ">";
                  }
                  break;
            }

         }

         parent = parent.parentNode;
      }
      tag.innerHTML = innerMostHTML;  //actually change the internal tag
   }

   return true;
}


function removeMarkedTags(tag) {
  if (tag.getAttribute("TO_BE_DELETED")) {
    tag.outerHTML = tag.innerHTML;         //blow away outer tag
  }
  return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    removeXMLFromHTML
//
// Purpose
//    Word puts some useless XML markup in the start <html> tag, nuke it.
//
function removeXMLFromHTML()
{
   var root = DOM.documentElement;
   var html = root.outerHTML;

   // We have 2 submatches, "<html", everything after "<html" to the ending
   // ">".  We want to throw out everything between "<html" and the end.
   // So, we will just keep $1. Search is case-insensitive to match both
   // <HTML and <html.
   html = html.replace(/(<html)([^>]*)/i, "$1");

   root.outerHTML = html;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    removeXMLMarkup
//
// Purpose
//    Word puts some random, useless XML markup in the body.  Strip it.
//
function removeXMLMarkup()
{
   var root = DOM.documentElement;
   var html = root.outerHTML;

   if(doShowLog())
   {
      var match;

      match = html.match(/<o:p>/g);
      gRemoveWordXML += (match != null ? match.length : 0);

/* LMH: I don't think we should be including end tags in the count.
      match = html.match(/<\/o:p>/g);
      gRemoveWordXML += (match != null ? match.length : 0);
*/
      match = html.match(/<o:SmartTagType[^>]*>/g);
      gRemoveWordXML += (match != null ? match.length : 0);
      
      match = html.match(/<st1:[\w\s"=]*>/gi);
      gRemoveWordXML += (match != null ? match.length : 0);
   }

   // Paragraphs that have <st1:address> in them are addresses, so
   // the line after the address shouldn't be in a new paragraph. Attempt
   // to substitute a <br> for the paragraph break before stripping
   // out all remaining <st1:> tags.
   html = html.replace(/<\/st1:address>(<\/st1:\w*>)?<\/p>[\n\r\s]*<p[\s\w="']*>/gi, "<br>");
   
   // Remove all instances of <o:p></o:p>
   html = html.replace(/<o:p>/g, "");
   html = html.replace(/<\/o:p>/g, "");
   html = html.replace(/<o:SmartTagType[^>]*>/g, "");

   // Remove all instances of <st1:whatever></st1:whatever>
   html = html.replace(/<st1:[\w\s"=]*>/gi, "");
   html = html.replace(/<\/st1:\w*>/gi, "");

   // If we find any other instances of XML markup, we can add it here.

   root.outerHTML = html;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    removeIfs
//
// Purpose
//    Word uses many <![if...]> style comments for its own internal
//    purposes, which are useless in HTML.  This function strips those.
//
function removeIfs()
{
   traverse(DOM.documentElement, null, null, ifHandler);

   var root = DOM.documentElement;
   var html = root.outerHTML;

   // clean up those empty comments!
   html = html.replace(/<!-*>/g, "");
   root.outerHTML = html;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    ifHandler
//
// Purpose
//    Find those pesky "if" conditionals that Word 2000 puts in the HTML
//    and nuke'em.
//
function ifHandler(comment)
{
   var html = comment.data;
   var matchif = html.match(/\[if /);
   var matchendif = html.match(/\[endif/);

   if(matchif != null || matchendif != null)
   {
      gRemoveConditionals++;
      comment.data = "";
   }

   return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    removeMSOStyleAttr
//
// Purpose
//    Microsoft Word uses many custom CSS attributes.  This function hunts
//    them down and removes them.
//
function removeMSOStyleAttr()
{
   var root = DOM.documentElement;
   var html = root.outerHTML;

   RegExp.multiline = true;

   if(doShowLog())
   {
      // NOTE!  This is highly ineffiecient since we are doing the regexp
      // searches twice (once to count, once to do the actual replaces).
      // If there is a better way to know how many times a replace()
      // does its thing, we should do that.     
      var match;

      match = html.match(/mso-[^:]*:"[^"]*";/g, "");
      if (match) gRemovemsoStyle += match.length;
      match = html.match(/mso-[^;'"]*;*(\n|\r)*/g, "");
      if (match) gRemovemsoStyle += match.length;
      match = html.match(/page-break-after[^;]*;/g, "");
      if (match) gRemovemsoStyle += match.length;
      match = html.match(/ style=['"]tab-interval:[^'"]*['"]/g, "");
      if (match) gRemovemsoStyle += match.length;
   }

   // This finds the mso-*:"SomeStuff"; style attributes and sets them to be nothing.
   html = html.replace(/mso-[^:]*:"[^"]*";/g, "");

   // This finds the other mso-* style attibutes.
   html = html.replace(/mso-[^;'"]*;*(\n|\r)*/g, "");

   // Remove some other Word-only css style attributes.
   html = html.replace(/page-break-after[^;]*;/g, "");
   html = html.replace(/ style=['"]tab-interval:[^'"]*['"]/g, "");

   root.outerHTML = html;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    removeEmptyParas
//
// Purpose
//    Word sets paragraph bottom margins to zero, then inserts empty
//    (containing &nbsp;) paragraphs to maintain the vertical spacing
//    expected.  This is soley for its own purpose and is redundant
//    for HTML.  This function removes margin definitions and removes
//    those pesky empty paragraphs.
//
function removeEmptyParas()
{
   var root = DOM.documentElement;
   var style = findTag("style",DOM.documentElement);
   var html = null;

   if(style != null)
   {
      // Clean out the nonsense zero margin definitions from the
      // style block.
      html = style.innerHTML;

      // Just strip all of those wacky margins that Word puts in there.
      html = html.replace(/margin[^:]*:[^\n\r]*/g, "");

      style.innerHTML = html;
   }

   // Next, go through the document and strip out those inline margins too.
   traverse(root, stripMargins);

   // Now go find those empty paragraphs and remove them.
   traverse(root, paraHandler);
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    stripMargins
//
// Purpose
//    Word uses a lot of CSS margin settings in attempt to make the HTML
//    version look exactly like the native Word version.  In general, this
//    is unwanted, so lets remove all this stuff.
//
function stripMargins(tag)
{
   var style = tag.getAttribute("style");

   if(style != null)
   {
      if(doShowLog())
      {
         // Note, if there is a better way to count (if replace can be forced
         // to report how many replaces it did), we should do that.  Because
         // this takes extra processing effort to count this stuff using
         // "match".

         var match;

         match = style.match(/margin[^"';]*;?/g);
         gRemoveMargins += (match != null ? match.length : 0);
         match = style.match(/text-indent[^"';]*;?/g);
         gRemoveMargins += (match != null ? match.length : 0);
         match = style.match(/tab-stops:[^'";]*;?/g);
         gRemoveMargins += (match != null ? match.length : 0);
      }

      style = style.replace(/margin[^"';]*;?/g, "");
      style = style.replace(/text-indent[^"';]*;?/g, "");
      style = style.replace(/tab-stops:[^'";]*;?/g, "");

      if(style == null || style == "" || style.search (/[^\s]/) == -1)
         tag.removeAttribute("style");
      else
         tag.setAttribute("style", style);
   }

   return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    paraHandler
//
// Purpose
//    Callback that looks for empty <p>'s and deletes them.  After
//    doing some processing removing stuff, we can easily end up
//    with empty paragraphs.  This just cleans up after ourselves.
//
function paraHandler(tag)
{
   tagName = tag.tagName;
   if(tagName.toUpperCase() == "P")
   {
      text = tag.innerHTML;

      // Make sure there are not any content generating HTML tags, this
      // prevents us from removing say, <img> in the next step.
      if(containsContentTags(text))
         return true; // Keep searching in the traverse

      // Ok, we don't have any content tags.  We are save to strip any
      // other tags (font, b, etc).
      text = text.replace(/<[^>]*>/g, "");

      // Strip whitespace
      text = text.replace(/\s/g, "");

      // Strip &nbsp;s
      text = text.replace(/&nbsp;/g, "");

      // After doing all that, if there is nothing left, this paragraph is empty.
      if(text == "" || text == null)
      {
         gRemoveEmptyParas++;
         tag.outerHTML = "";
      }
   }

   return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    containsContentTags
//
// Purpose
//    Given a string that contains some HTML, check to see if we have
//    any tags that generate visible content.
//
// Returns
//    zero (false) if no content generating tags are found.  Non-zero
//    value if anything is found.
//
function containsContentTags(text)
{
   var index = 0;

   // text.search returns -1 if it does not find anything.  So by adding
   // 1 and bitwise or-ing the result, we maintain zero if no match.
   index |= text.search(/<hr/i) + 1;
   index |= text.search(/<img/i) + 1;
   index |= text.search(/<input/i) + 1;
   index |= text.search(/<object/i) + 1;
   index |= text.search(/<table/i) + 1;
   index |= text.search(/<textarea/i) + 1;
   index |= text.search(/<embed/i) + 1;

   // if index is still zero after all that, we don't have any content tags.

   return index;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    formatCSS
//
// Purpose
//    When we modify or remove stuff from the <style> block we leave it in
//    a not so pretty state.  This will clean it up so it looks nice.
//
function formatCSS()
{
   var style = findTag("style",DOM.documentElement);

   if(style != null)
   {
      var html = style.innerHTML;

      // We need multiline turned on for this.
      var multiline = RegExp.multiline;
      RegExp.multiline = true;

      // Lets just get rid of those comments that Word puts in there
      html = html.replace(/\/\*.*\*\//g, "");

      // Clean up the whitespace between the start and end brackets.
      html = html.replace(/\s*\}/g, "}");
      html = html.replace(/\{\s*/g, "{");

      // Make sure anything that is indented is indented only one tab.
      html = html.replace(/^\t+/g, "\t");

      // Make sure the style names are on their own line.
      html = html.replace(/\}/g, "}\n");

      // This will delete blank lines in the style declaration
      html = html.replace(/^[ \t]*(\r|\n)+/g, "");

      // Set it back
      RegExp.multiline = multiline;

      style.innerHTML = html;
   }
}



/////////////////////////////////////////////////////////////////////////////
// Function
//    removeCSSFromTables
//
// Purpose
//    Word tends to go overboard with CSS with tables.  Almost all of it
//    is used to maintain the "Word appearance" and is generally undesirable
//    for use with HTML.  So, this function just strips it all.
//
function removeCSSFromTables()
{
  // Find each table tag and do some processing on it.
  var tables = DOM.getElementsByTagName('table');
  for (var i=0; i < tables.length; i++){
    traverse(tables[i], convertCSSInTables);
  }
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    convertCSSInTables
//
// Purpose
//    We want to strip the CSS applied to tables and their cells.
//    However, some of these styles can be converted into HTML
//    attributes.  This function converts any styles we can to
//    HTML attributes and then removes the style attribute.
//
function convertCSSInTables(tag)
{
   var tagName = tag.tagName.toUpperCase();
   var style;
   var match;

   if(tagName == "TABLE")
   {
      style = tag.getAttribute("style");

      if(style != null && style != "")
      {
         match = style.match(/border-color: *([^;'"]*)/);

         if(match != null)
            tag.setAttribute("bordercolor", match[1]);

         tag.removeAttribute("style");

         gRemoveTableCSS++;
      }
   }
   else if(tagName == "TR")
   {
      // TRs do not have any styles that we want to keep.
      if (tag.getAttribute("style")){
        tag.removeAttribute("style");
        gRemoveTableCSS++;
      }
   }
   else if(tagName == "TD")
   {
      style = tag.getAttribute("style");

      if(style != null && style != "")
      {
         match = style.match(/background: *([^;'"]*)/);

         if(match != null)
            tag.setAttribute("bgcolor", match[1]);

         // Remove the style attribute from the TD.
         tag.removeAttribute("style");

         gRemoveTableCSS++;
      }
   }

   return true;
}



/////////////////////////////////////////////////////////////////////////////
// Function
//    removeNonCSSDeclaration
//
// Purpose
//    Word puts a number of non-standard CSS style declarations in the
//    style block.  This will strip them, and remove any references to
//    them.
//    
//    While we're here, we're also going to remove any empty style 
//    declarations, and the references to them.
//
function removeNonCSSDeclaration()
{
   var root = DOM.documentElement;
   var data = new Array();
   var invalidList;
   var style;

   style = findTag("style",DOM.documentElement);
   if(style != null)
   {
      var html = style.innerHTML;
      var htmlLeft, htmlRight;

      // First, we need to get a list of all the invalid style names.  This
      // way, we can go through the file and remove the references to them.
      // An invalid style looks like this: @foo bar
      invalidList = html.match(/^@[^\s]*\s\w*/g);

      if(invalidList != null)
      {
         // Log the number of invalid CSS styles we find.
         gRemoveNonCSS += invalidList.length;

         for(var i = 0; i < invalidList.length; i++)
         {
            invalidList[i] = invalidList[i].replace(/@\w* /g, "");
            invalidList[i] = invalidList[i].replace(/(\r|\n)*/g, "");
         }
      }

      // This removes the invalid "@" CSS declarations
      html = html.replace(/^@[^\}]*\}/g, "");

      // Now we need to go and clean out everything that referenced the
      // invalid styles.  First lets finish cleaning the style block.
      if(invalidList != null)
      {
         var regx = new RegExp();
         var htmlLeft, htmlRight;
         var result;

         for(var j = 0; j < invalidList.length; j++)
         {
            // Find stuff of the form "div.Section1 ... { ... }"
            regx.compile("^.*\\."+invalidList[j]+"[^\\}]*\\}", "g");

            while((result = regx.exec(html)) != null)
            {
               htmlLeft = html.substring(0, result.index);
               htmlRight = html.substring(result.index + result[0].length);

               // Remove the match
               html = htmlLeft + htmlRight;
            }
         }

         style.innerHTML = html;
         
         // Now remove any empty style blocks
         var multiline = RegExp.multiline;
         html = DOM.getElementsByTagName('style')[0].innerHTML;
         RegExp.multiline = true;

         regx.compile("\\.?\\w*\\.([^\\{]*)\\{([^\\}]*)\\}", "g");

         while((result = regx.exec(html)) != null){
           result[1] = result[1].replace(/[\r\n\s]*/g,"");
           result[2] = result[2].replace(/[\r\n\s]*/g,"");
           if (result[2] == ""){
             htmlLeft = html.substring(0, result.index);
             htmlRight = html.substring(result.index + result[0].length);
             invalidList.push(result[1]);           
           }
           html = htmlLeft + htmlRight;
         }
         RegExp.multiline = multiline;
         style.innerHTML = html;
         
      }
/*
      else
      {
         style.innerHTML = html;
      }
*/      
   }

   // OK, we cleaned up the style block, now we just need to go
   // through the rest of the document and remove any references
   // to the invalid CSS classes.
   if(invalidList != null)
   {
      root = DOM.documentElement;
      var html = root.outerHTML;
      var regx = new RegExp();

      for(var k = 0; k < invalidList.length; k++)
      {
         regx.compile(' ?class=((")?)'+invalidList[k]+'\\1', 'g');
         while((result = regx.exec(html)) != null)
         {
            htmlLeft = html.substring(0, result.index);
            htmlRight = html.substring(result.index + result[0].length);

            html = htmlLeft + htmlRight;
         }
      }

      root.outerHTML = html;
   }
}



/////////////////////////////////////////////////////////////////////////////
// Function
//    removeMetaLink
//
// Purpose
//    Removes those nasty Microsoft-only Meta tags.
//
function removeMetaLink()
{
   var tag, html, rel, tagArr;

   // Clean up the META tags.
   tagArr = DOM.getElementsByTagName("META");
   while (tagArr.length > 0)
   {
      tag = tagArr.pop(); // Order is important here. Removing last first.

      if(tag.outerHTML.match(/(word|microsoft|mshtml)/i))
      {
         // This tag contains some Word junk, nuke it.
         tag.outerHTML="";
         gRemoveMetaTags++;
      }
   }

   // Clean up the LINK tags.
   tagArr = DOM.getElementsByTagName("LINK");
   while (tagArr.length > 0)
   {
      tag = tagArr.pop(); // Order is important here. Removing last first.
      rel = tag.getAttribute("rel");

      if(rel == "File-List")
      {
         tag.outerHTML = "";
         gRemoveMetaTags++;
      }
   }

   return true;
}


/////////////////////////////////////////////////////////////////////////////
function setBgColor()
{
   var body = findTag("body",DOM.documentElement);

   if(body != null)
   {
      var colorObj = dwscripts.findDOMObject("bgcolor_basic");
      var color = (colorObj == null ? null : colorObj.value);

      if(color != null)
      {
         if (!body.bgcolor || (body.bgcolor.toLowerCase() != color.toLowerCase())){
           body.setAttribute("bgcolor", color);
           gBackgroundSet = color;
         }
      }
   }
}



/////////////////////////////////////////////////////////////////////////////
// Function
//    removeInlineCSS
//
// Purpose
//    Word 2000 loves to declare "normal" styles and then apply them to
//    every block in the document.  Let's just set it on the body and
//    remove it from everything else.
//
function removeInlineCSS()
{

   var style = findTag("style",DOM.documentElement);

   if(style != null)
   {
      var index;

      html = style.innerHTML;

      index = html.search(/\.(MsoNormal)/i);
      if(index != -1)
      {
         // Lets strip out the "normal" styles and make only one.  Word
         // tends to have stuff like p.MsoNormal, li.MsoNormal, etc.
         html = html.replace(/^\s*\w*\.MsoNormal([A-Za-z]+)?/ig, ".TempNormal");

         // Change the first one we find to "FirstNormal"
         html = html.replace(/\.TempNormal/, ".FirstNormal");

         // Remove the rest.
         html = html.replace(/\.TempNormal/g, "");

         // Change the first normal to just "Normal". 
         // LMH: Make sure *not* to replace the opening curly brace if it
         // happens to be on the name line as the class name, or we'll end
         // up with an invalid style declaration.
         html = html.replace(/^.*\.FirstNormal[^\r\n\s\{]*/, ".Normal");

         style.innerHTML = html;

         // Now we need to go and remove all references to the old class.
         html = DOM.body.innerHTML;

         if(doShowLog())
         {
            var match = html.match(/ class=MsoNormal/g);
            gRemoveInlineCSS += (match != null ? match.length : 0);
         }

         html = html.replace(/ class=MsoNormal([A-Za-z]+)?/g, "");
         DOM.body.innerHTML = html;

         DOM.body.setAttribute("class", "Normal");

         // Since body styles do not filter down into table cells,
         // we need to set the styles on the table cells too.
         traverse(DOM.documentElement, setTDStyles);
      }
   }
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    setTDStyles
//
// Purpose
//    We have removed the styles from individual paragraphs and the body
//    style does not filter down into the table cells, so we need to set
//    the style on the table cells too.
//
function setTDStyles(tag)
{
   if(tag.tagName.toUpperCase() == "TD")
   {
      tag.setAttribute("class", "Normal");
   }

   return true;
}


/////////////////////////////////////////////////////////////////////////////
// Function
//    removeUnusedStyles
//
// Purpose
//    After we have done all of our house cleaning, some styles defined
//    in the head may no longer be used anywhere.  If they are no longer
//    used, we will blow them away.
//
function removeUnusedStyles()
{
   var style = findTag("style",DOM.documentElement);
   var html;
   var classes;

   if(style != null)
   {
      html = style.innerHTML;

      // Put each style class in an array.
      // LMH: Removed ? after \. to prevent us from considering
      // redefined HTML tags as classes (redefined HTML tags
      // aren't called from anywhere, but they're still used).
      classes = html.match(/\.\w*\s*\{[^\}]*\}/g);

      if(classes != null)
      {
         var classNames = new Array(classes.length);
         var regx = new RegExp();

         // Clean up the matches so we only have the class name.
         for(i = 0; i < classes.length; i++)
            classNames[i] = classes[i].replace(/^\s*\.?(\w*)\s*\{[^\}]*\}/g, "$1");

         body = findTag("body",DOM.documentElement);
         bodyhtml = body.outerHTML;

         // Now search in the body to see if we use them anywhere.
         for(i = 0; i < classes.length; i++)
         {
            regx.compile("class=['\"]?" + classNames[i], "g");

            result = regx.exec(bodyhtml);

            if(result == null)
            {
               // this style is not used.  Nuke it.
               classes[i] = "";

               gRemoveUnusedCSS++;
            }
         }

         // Now reconstruct the style block
         html = "\n<!--\n";

         for(i = 0; i < classes.length; i++)
            html += classes[i];

         html += "\n-->\n";

         style.innerHTML = html;
         
      }
   }
}

//*************** Pg1 Class *****************

//This is an example of a page class to be used with the TabControl.
//Uncomment the alert() calls to display the various events as they occur.

function Pg1(theTabLabel) {
  this.tabLabel = theTabLabel;
}
Pg1.prototype.getTabLabel = Pg1_getTabLabel;


function Pg1_getTabLabel() {
  return this.tabLabel;
}

//***************** End of Pg1 Class ******************
//*************** Pg2 Class *****************

//This is an example of a page class to be used with the TabControl.
//Uncomment the alert() calls to display the various events as they occur.

function Pg2(theTabLabel) {
  this.tabLabel = theTabLabel;
}
Pg2.prototype.getTabLabel = Pg2_getTabLabel;


function Pg2_getTabLabel() {
  return this.tabLabel;
}
