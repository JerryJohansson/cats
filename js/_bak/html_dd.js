var sAllNames = "";//variable to store the list of All names
var sActiveNames = "";//variable to store the list of Active names
var sInjuredNames = "";//variable to store the list of injured employees
var sEmpPositiosn = "";//variable to store the list of employee positions
var sEmpCompanies = "";//variable to store the list of employee companies

//get the list of all employees from the temp file
function ReadActiveEmps(){
	if(sAllNames == ""){
		var sTemp = ReturnDropFile('catsActiveEmployees');
		sAllNames = sTemp;
	}
	return sAllNames;
}

//get the list of all employee companies from the temp file
function ReadEmpCompanies(){
	if(sEmpCompanies == ""){
		var sTemp = ReturnDropFile('catsEmpCompanies');
		sEmpCompanies = sTemp;
	}
	return sEmpCompanies;
}

//get the list of all Active employees from the temp file
function ReadAllEmps(){
	if(sActiveNames == ""){
		var sTemp = ReturnDropFile('catsActiveEmployees');
		sActiveNames = sTemp;
	}
	return sActiveNames;
}

//get the list of all Injured employees from the temp file
function ReadInjuredEmployees(){
	if(sInjuredNames == ""){
		var sTemp = ReturnDropFile('catsInjuredEmployees');
		sInjuredNames = sTemp;
	}
	return sInjuredNames;
}

//get the list of all Active employees from the temp file
function ReadEmpPositions(){
	if(sEmpPositiosn == ""){
		var sTemp = ReturnDropFile('catsEmpPositions');
		sEmpPositiosn = sTemp;
	}
	return sEmpPositiosn;
}

//get the list of all employee numbers from the temp file
function ReadEmpNumbers(){
	var sTemp = ReturnDropFile('catsEmpNumbers');
	return sTemp;
}

//get the list of all Action numbers from the temp file
function ReadActionNumbers(){
	var sTemp = ReturnDropFile('catsActionNumbers');
	return sTemp;
}

//get the list of all site specific legislations from the temp file
function ReadLegislations(){
	var sTemp = ReturnDropFile('catsLegislations');
	return sTemp;
}

//get the list of all government locations from the temp file
function ReadGovLocations(){
	var sTemp = ReturnDropFile('catsGovLocations');
	return sTemp;
}

//get the list of all government locations from the temp file
function ReadGovDepartments(){
	var sTemp = ReturnDropFile('catsGovDepartments');
	return sTemp;
}

//get the list of all sites
function ReadAllSites(){
	var sTemp = ReturnDropFile('catsAllSites');
	return sTemp;
}

//get the list of all sites
function ReadReportedBy(){
	var sTemp = ReturnDropFile('catsReportedBy');
	return sTemp;
}

//get the list of all active text names
function ReadActiveTextNames(){
	var sTemp = ReturnDropFile('catsActiveTextNames');
	return sTemp;
}

//get the list of all text names
function ReadAllTextNames(){
	var sTemp = ReturnDropFile('catsAllTextNames');
	return sTemp;
}

//get the list of all text names
function ReadRefDescriptions(){
	var sTemp = ReturnDropFile('catsRefDescriptions');
	return sTemp;
}

//get the list of all text names
function ReadRefTypes(){
	var sTemp = ReturnDropFile('catsRefTypes');
	return sTemp;
}

//get the list of all injured employees
function ReadInjuredEmps(){
	var sTemp = ReturnDropFile('catsInjuredEmps');
	return sTemp;
}

//get the list of all injured employees
function ReadInjuredIncIDs(){
	var sTemp = ReturnDropFile('catsInjuredIncIDs');
	return sTemp;
}

//get the list of incident checkbox types
function ReadCheckboxTypes(){
	var sTemp = ReturnDropFile('catsCheckboxTypes');
	return sTemp;
}

//get the list of incident checkbox descriptions
function ReadCheckboxDesc(){
	var sTemp = ReturnDropFile('catsCheckboxDesc');
	return sTemp;
}

//get the list of active companies
function ReadActiveCompanies(){
	var sTemp = ReturnDropFile('catsActiveCompanies');
	return sTemp;
}

//get the list of all companies
function ReadAllCompanies(){
	var sTemp = ReturnDropFile('catsAllCompanies');
	return sTemp;
}

//get the list of all employee positions
function ReadEmpPositions(){
	var sTemp = ReturnDropFile('catsEmpPositions');
	return sTemp;
}
 
function ReadAccessGroups(){
	var sTemp = ReturnDropFile('catsAccessGroups');
	return sTemp;
}
 
//get the list of all employee positions
function ReadIncidentNumbers(){
	var sTemp = ReturnDropFile('catsIncidentNumbers');
	return sTemp;
}