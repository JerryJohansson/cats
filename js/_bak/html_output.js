function CATS_getTable_IncidentCheckbox(){
/*************************************************
	create incident checkboxes table string
	Params:
		1. array(array(array(),array(),...),array())
		2. Maximum Columns to generate for the table
	Returns:[string]
		HTML string containing incident checkbox table
*************************************************/
	var args=CATS_getTable_IncidentCheckbox.arguments;
	var maxColumns = (args.length>1)?args[1]:3;
	maxColumns = maxColumns * 2;
	var i=cnt=0;
	var a=args[0][0];
	var b=args[0][1];
	var sHTML = "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	sHTML += "<tr>";
	
	for (i=0;i<a.length;i++){ 
		cnt++;
		sHTML += "<td class=\"normal\" valign=\"top\">"; 
		sHTML += "<input class=\"inputTextBox\" type=\"checkbox\" name=\"" + a[i][0] + "\" value=\"" + a[i][1] + "\">";
		sHTML += "</td><td class=\"normal\" valign=\"top\">";
		sHTML += "<span class=\"normal\" id=\"" + a[i][2] + "\" onMouseOver=\"ShowDescription('" + a[i][2] + "','" + a[i][3] + "');\">" + a[i][4] + "</span>";
		sHTML += "</td>";
		if ((cnt % maxColumns) == 0) {
			sHTML += "</tr><tr>"; 
		}
	} // end for
	sHTML += "<tr><td colspan=\""+ maxColumns +"\"><input type=\"hidden\" name=\"" + b[0] + "\" value=\"" + b[1] + "\"></td><tr>";
	sHTML += "</tr>";
	sHTML += "</table>";
	return sHTML;
}
function CATS_getSelect(){
/*************************************************
	create dropdown from array
	Params:
		1. array(array(type,length),array(item1,item2))
			array[0]
				type=dd[dropdown]
				length=1 or 2
			array[1]
				if length=1
					value=item1
					text=item1
				else length=2
					value=item1
					text=item2
		2. 
	Returns:[string]
		HTML string containing select element
*************************************************/
	var args = CATS_getSelect.arguments;
	var sHTML = "";
	var type=args[0][0][0];
	var offset=args[0][0][1];
	var a=args[0][1];
	var on=(args.length>1)?args[1]:false;
	//alert("type="+type+"\noffset="+offset+"\na="+a);
	for (i=0;i<a.length;i+=offset){
			sHTML += "<option value='" + a[i] + "' " + ((on&&on==a[i])?" selected ":"") + ">" + a[i+(offset-1)] + "</option>";
	}
	return sHTML;
}
//select any checkboxes that need to be checked
function CheckTheBoxes(sValues, sCheckbox, sHidden){
	if(sValues != ""){
		var objHidden = document.getElementById(sHidden);
		var r = objHidden.value;
		var arValues = sValues.split("|");
		var w = arValues.length;
		for(q=1;q<=r;q++){
			var objCheckbox = document.getElementById(sCheckbox + q);
			var iValue = objCheckbox.value;
			for(k=0;k<w;k++){
				if(iValue == arValues[k]){
					objCheckbox.checked = true;
					break;
				}
			}
		}
	}//end if(sValues != "")
}
//select the appropriate value from the file generated drop downs
function SelectDropDown(sField, iValue){
	try{
		var o = document.getElementById(sField);
		var f = o.options.length;
		var i = 0;
		for(i=0;i<f;i++){
			if(o.options[i].value == iValue){
				o.options[i].selected = true;
				break;
			}
		}
	}catch(e){
		// dropdown doesn't exist ;(
	}
}
//go and get the values from the chosen file
function ReturnDropFile(sFileName){
	var sFileNameStrVar = "txt_"+sFileName;
	var ret = "";
	try{
		try{
			ret = eval(sFileNameStrVar);
			return ret;
		}catch(e){
			ret = CATS_getSelect(eval(sFileName));
			eval(sFileNameStrVar + '= ret;');
			return ret;
		}
	}catch(e){
		alert(sFileName + " does not seem to be loaded");
		return "";
	}
}
//go and get the values from the chosen file
function ReturnCheckFile(sFileName){
	var sFileNameStrVar = "txt_"+sFileName;
	var ret = "";
	try{
		try{
			ret = eval(sFileNameStrVar);
			return ret;
		}catch(e){
			ret = CATS_getTable_IncidentCheckbox(eval(sFileName));
			eval(sFileNameStrVar + '= ret;');
			return ret;
		}
	}catch(e){
		alert(sFileName + " does not seem to be loaded");
		return "";
	}
}