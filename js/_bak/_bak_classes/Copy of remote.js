
// retrieve XML document (reusable generic function);
// parameter is URL string (relative or complete) to
// an .xml file whose Content-Type is a valid XML
// type, such as text/xml; XML source must be from
// same domain as HTML file
/***************************************************
	properties=
	{
		onreadystatechange:	'Event handler for an event that fires at every state change',
		readyState:	{Object_status_integer:[
		'0 = uninitialized',
		'1 = loading',
		'2 = loaded',
		'3 = interactive',
		'4 = complete']},
		responseText:	'String version of data returned from server process',
		responseXML:	'DOM-compatible document object of data returned from server process',
		status:	'Numeric code returned by server, such as 404 for "Not Found" or 200 for "OK"',
		statusText:	'String message accompanying the status code'
	}
	***********************************************/
	/***********************************************
	methods
	{
		abort:function(){//	Stops the current request
			this.response.abort();
		},
		getAllResponseHeaders: function(){//	Returns complete set of headers (labels and values) as a string
			this.request.getAllResponseHeaders();
		},
		getResponseHeader: function("headerLabel"){//	Returns the string value of a single header label
			this.request.getResponseHeader(name);
		},
		open: function("method", "URL"[, asyncFlag[, "userName"[, "password"]]]){//	Assigns destination URL, method, and other optional attributes of a pending request
			this.request.open("post",url,true);
		},
		send: function(content){//	Transmits the request, optionally with postable string or DOM object data
			this.request.send(content);
		},
		setRequestHeader: function("label", "value"){//	Assigns a label/value pair to the header to be sent with a request
			this.request.setRequestHeader(name,value);
		}
	}
	***************************************************/
	// global request and XML document objects
	
	//--------------------------------------------------------------------------
	var _show_message=function(s){
		window.status=s;
	}
	var _XMLHttp_messages={
		readyState:['Waiting for response from the server','Connected: Transfering data...','Please wait while initialising','Complete','Remote load - '],
		errors:['There was an error while processing response']
	};
	
	//experiment
	var experimental=true;
	var ezw_XMLHttpRequests=new Object();
	var ezw_XMLHttpRequests_index=0;
	var ezw_XMLHttpResponses=new Object();
	//------------------------
	var ezw_XMLHttpRequest;
	var ezw_XMLHttpResponse;
	var ezw_XMLHttpResponseType;
	var ezw_XMLHttpRequestType;
	function _HTTPRequest() {
		var args=arguments;
		var moz=(window.XMLHttpRequest)?1:0;
		var sendObj=null;
		var async=true;
		ezw_XMLHttpRequest=(moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));
		//experiement
		var _ret=ezw_XMLHttpRequests_index;
		ezw_XMLHttpResponses[_ret]=new _HTTPResponse(args);
		eval("var r=(typeof(ezw_XMLHttpRequests_"+_ret+")!='undefined')?ezw_XMLHttpRequests_"+_ret+":null;");
		var url=args[0];
		var method=(args.length>1 && typeof(args[1])=="string")?args[1].toUpperCase():"POST";
		if(args.length>2 && typeof(args[2])=="function") ezw_XMLHttpResponse = args[2];
		if(args.length>3 && typeof(args[3])=="string"){
			sendObj=args[3];
			url=(url.indexOf(".php?")!=-1)?(url+args[3]):(url+"?"+args[3]);
		}
		if(args.length>4 && typeof(args[4])=="string"){
			ezw_XMLHttpResponseType=args[4];
		}
		if(args.length>5 && typeof(args[5])=="string"){
			ezw_XMLHttpRequestType=args[5];
			if(ezw_XMLHttpRequestType=="refresh"){
				window.document._screen.show("loading");// show message box and disable screen by covering it with semi-transparent div
			}
		}
		if(args.length>6 && typeof(args[6])=="boolean"){
			async=args[6];
		}
		
		if(experimental){
			r.onreadystatechange = _HTTPResponseFunction(_ret);
			r.open(method, url, async);
			r.send(sendObj);
			return _ret;
		}
		//alert("not experimental")
		ezw_XMLHttpRequest.onreadystatechange = _HTTPResponse;
		ezw_XMLHttpRequest.open(method, url, async);
		if(moz) ezw_XMLHttpRequest.send(sendObj);
		else ezw_XMLHttpRequest.send(sendObj);
		return ezw_XMLHttpRequest;
	}
	function _HTTPRequestGet(name) {
		try{ezw_XMLHttpRequest.getResponseHeader(name);
		}catch(e){alert("_HTTPRequestGet:"+e);}
	}
	function _HTTPRequestGetAll() {
		try{return ezw_XMLHttpRequest.getAllResponseHeaders();
		}catch(e){alert("_HTTPRequestGetAll:"+e);}
	}
	function _HTTPResponseFunction(id){
		var s="o=function(){ezw_XMLHttpResponses["+id+"].response("+id+");}";
		var o=null;
		eval(s);
		if(typeof(o)=="function") return o;
		else return null;
	}
	function _HTTPResponse(args){
		var _ret=ezw_XMLHttpRequests_index++;
		this.id="ezw_XMLHttpRequests_"+_ret;
		this.moz=(window.XMLHttpRequest)?1:0;
		this.index=_ret;
		this.func=(args.length>2 && typeof(args[2])=="function")?args[2]:null;
		this.type=(args.length>4 && typeof(args[4])=="string")?args[4]:"";
		eval(this.id+'=(this.moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));var r='+this.id+';');
		//ezw_XMLHttpRequests[_ret]=(this.moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));
		
		var res=function(id) {
			var parent=ezw_XMLHttpResponses[id];
			eval('var r=ezw_XMLHttpRequests_'+id+';');
			eval("var m=function(s){window.status=_XMLHttp_messages.readyState[ezw_XMLHttpRequests_"+id+".readyState]+((typeof(s)!='undefined'&&s!='') ? '\\n'+s:'');}");
			var p=function(id) {
			// function to process the response
			// Returns the response to the callback function with object formated in set format
				var _return='';
				var parent=ezw_XMLHttpResponses[id];
				eval('var r=ezw_XMLHttpRequests_'+id+';');
				switch(String(parent.type).toLowerCase()){
					case 'xmlobject':
						_return=r.responseXML;
					case 'xml':
						_return=r.responseXML.documentElement;
						break;
					case 'text':
						_return=r.responseText;
					default:
						_return=r.responseText;
						break;
				}
				if(typeof(parent.func)=="function"){
					parent.func(_return);
				}else{
					alert("No function specified:"+_return);
				}
			};
			
			this.process=p;
			this.show_message=(m!=null)?m:_show_message;
			try{switch(r.readyState){
				case 0:
					this.show_message();
					break;
				case 1:
					this.show_message();
					break;
				case 2:
					this.show_message();
					break;
				case 3:
					this.show_message();
					break;
				case 4:
					switch(r.status){
						case 200:// do callback
							this.process(id);
							break;
						case 500:// server error
							this.show_message(r.statusText);
							break;
						case 404:// url not found
							this.show_message(r.statusText);
							break;
						default:
							//this.show_message(r.statusText);
							break;
					}
					this.show_message(r.statusText);
					break;
				default:
					this.show_message(_XMLHttp_messages.errors[0]+"\n"+r.statusText);
					break;
			}}catch(e){alert("_HTTPResponse:"+e+"\n"+r)}
		};
		this.response=res;
		return this;
	}
	
/***************************************************************************
END _HTTPRequest Object
***************************************************************************/


/***************************************************************************
HELPERS FOR _HTTPRequest Object
***************************************************************************/
function return_xml(e){
	//var e=eval(o);
	var s="";
	if(e){
		for(i=0;i<e.length;i++)s+="<row><name>"+e[i][0]+"</name><value>"+e[i][1]+"</value></row>";
	}
	if(s!='') s="<rows>"+s+"</rows>";
	return s;
}
function return_query(e){
	var s=parse_array(e);
	if(s!='') s=s.replace(/&$/,"");
	return s;
}
function parse_array(e){
	var args=arguments;
	var s="";
	var i=0;
	if(e){
		for(i=0;i<e.length;i++)
			if(typeof(e[i][1])=="object" && e[i][1][0]){s+=parse_array(e[i][1],e[i][0]);}
			else if(args.length>1) s+=""+args[1]+"["+e[i][0]+"]="+escape(e[i][1])+"&";
			else s+=""+e[i][0]+"="+escape(e[i][1])+"&";
	}
	return s;
}
function return_params(e){
	//var e=eval(o);
	var s="";
	if(e){
		for(i=0;i<e.length;i++)s+=""+e[i][0]+"="+e[i][1]+"&";
	}
	if(s!='') s=s.replace(/&$/,"");
	return s;
}
function return_function(o){
	if(_cats_dbg==true){
		var w=window.open("","new");
		w.document.open();
		w.document.write(o);
		w.document.close();
	}
	eval('var e='+o);
	var s="";
	for(a in e){
		s+="Name: "+a+"\nValue: "+unescape(e[a])+"\n";
	}
	alert("Array Parsed:\n"+s);
	return e;
}
function return_login(o){
	try{
		eval('var e='+o);
		if(e){
			for(a in e) e[a]=unescape(e[a]);
		}
		if(e['redirect']!="") top.location.href=e['redirect'];
	}catch(e){alert(e+"\n"+e.number+"\n"+e.description);}
}
function return_window(o){
	var w=window.open("","new");
		w.document.open();
		w.document.write(o);
		w.document.close();
}
//===================================================================================//





function show_message(s){
	window.status=global_messages.readyState[ezw_XMLHttpRequest.readyState]+((s!='')?'\n'+s:'');
}