function ReadExReportedTo(){
	return ReturnCheckFile('catsExReportedTo');
}

function ReadSystemicCause(){
	return ReturnCheckFile('catsSystemicCause');
}

function ReadBehaviourCause(){
	return ReturnCheckFile('catsBehaviourCause');
}

function ReadInjClass(){
	return ReturnCheckFile('catsInjClass');
}

function ReadInjLocation(){
	return ReturnCheckFile('catsInjLocation');
}

function ReadInjMechanism(){
	return ReturnCheckFile('catsInjMechanism');
}

function ReadInjType(){
	return ReturnCheckFile('catsInjType');
}

function ReadInjAgency(){
	return ReturnCheckFile('catsInjAgency');
}

function ReadEnvClass(){
	return ReturnCheckFile('catsEnvClass');
}

function ReadEnvImpactTo(){
	return ReturnCheckFile('catsEnvImpactTo');
}

function ReadEnvImpactBy(){
	return ReturnCheckFile('catsEnvImpactBy');
}

function ReadCommClass(){
	return ReturnCheckFile('catsCommClass');
}

function ReadEconomicClass(){
	return ReturnCheckFile('catsEconomicClass');
}

function ReadQualClass(){
	return ReturnCheckFile('catsQualClass');
}

function ReadRiskLevel(){
	var sTemp = ReturnDropFile('catsRiskLevel');
	return sTemp;
}

function ReadRiskCategory(){
	var sTemp = ReturnDropFile('catsRiskCategory');
	return sTemp;
}

function ReadIncReportTypes(){
	var sTemp = ReturnDropFile('catsIncReportTypes');
	return sTemp;
}

function ReadIncCategories(){
	//var sTemp = ReturnDropFile('catsIncidentCategories');
	var sTemp = ReturnDropFile('catsIncCategories');
	return sTemp;
}