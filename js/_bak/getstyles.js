// displays a dropdown list of the current styles available
function getStyles(id) {
	try{
		var theStyle = new Array();
    var theStyleText = new Array();
    var styleExists,cboStyles;
    var styleValue, styleText, styleLength;
    var sObj='None:""';
    var oObj;
    data = window;
    noOfSheets = data.document.styleSheets.length;
    if (noOfSheets > 0) {
        for (i=1;i<=noOfSheets;i++) {
            //alert(data.document.styleSheets[i-1].id)
            if(data.document.styleSheets[i-1].title!=id) continue;
            noOfStyles = (document.all)?data.document.styleSheets[i-1].rules.length:data.document.styleSheets[i-1].cssRules.length;

						for (x=0;x<noOfStyles;x++){
                styleValue = (document.all)?data.document.styleSheets[i-1].rules[x].selectorText:data.document.styleSheets[i-1].cssRules[x].selectorText;

                // stylesheet rule contains a . (ignore any styles that dont contain a . they are NOT user styles)
                if (styleValue.indexOf(".") >= 0) {

                    // stylesheet rule doesnt contain :
                    if (styleValue.indexOf(":") < 0) {

                        styleLength = (styleValue.indexOf(",")>0)?styleValue.indexOf(","):styleValue.length;

                        // style contains a . at beginning
                        if (styleValue.indexOf(".") == 0) {
                            styleText = styleValue.substring(1,styleLength);
                            theStyle[theStyle.length] = styleValue;
                            theStyleText[theStyleText.length] = styleText;

                        } else {
                            // style contains a . not at beginning
                            if (styleValue.indexOf(".") > 0) {
                                styleText = styleValue.substring(styleValue.indexOf(".")+1,styleLength);
                                styleValue = styleValue.substring(styleValue.indexOf("."),styleValue.length);

                                theStyleText[theStyleText.length] = styleText;
                                theStyle[theStyle.length] = styleValue;
                            }
                        }

                    // contains BOTH a . and a :
                    } else {
                        styleValue = styleValue.substring(styleValue.indexOf("."),styleValue.indexOf(":"))

                        for (i=0;i<theStyle.length;i++) {
                            if (styleValue == theStyle[i]) {
                                styleExists = true;
                            }
                        }

                        if (styleExists != true) {
                            theStyle[theStyle.length] = styleValue;

                            styleText = styleValue.substring(styleValue.indexOf(".")+1,styleValue.length);
                            theStyleText[theStyleText.length] = styleText;
                        }

                        styleExists = false;
                    }

                }

            } // End for

            for (z=0; z <= theStyle.length-1; z++) {
                sObj+=',\"'+theStyleText[z]+'\":\"'+theStyleText[z]+'\"';
            }
        } // End For
    } // End if
    eval("oObj = {"+sObj+"}");
    return oObj;
	}catch(e){}
} // End function
