function showhide(itm){
	if(document.all)document.body.focus();
	tab_onclick(itm.getAttribute('id'));
}
function tab_onclick(itm){
	if(typeof(itm)=="string") itm=element(itm);
	if(itm.blur) itm.blur();
	var b = element("tab_buttons");
	var a = b.getElementsByTagName("A");
	var id = 0;
	for(i=0;i<a.length;i++){
		if(a[i]==itm) id=i;
		a[i].className='';
		element('tab_panel['+(i)+']').style.display='none';
	}
	element('tab_panel['+id+']').style.display='block';
	itm.className='indent';
	return false;
}
function initTabs(){
	tab_onclick("Editor");
}

// Functions
function string_to_id(s){
	return s.replace(/\s|&|#|\?|@|\||!|\^|%|\+|\*/gi,"_");
}
function alertMe(s){alert(s);return s;}
function element(s){return document.getElementById(s);}
function getAvailableHeight(sobj,offset){
	var el=element(sobj);
	var h=h1=0;
	var bh=(!document.all)?window.innerHeight:document.body.offsetHeight;
	while(((el=el.parentNode)!=null) && el.nodeName != "BODY" ){
		h1+=parseInt(el.offsetTop);
	}
	h=(bh-h1);
	return h-((typeof(offset)=="number")?offset:0);
}
function getAvailableWidth(sobj,offset){
	var w=(!document.all)?window.innerWidth:document.body.offsetWidth+2;
	return w-((typeof(offset)=="number")?offset:0);
}

function _row_over(o){
	o.className='tr_over';
}
function _row_out(o){
	if(!o.selected)
		o.className='';
}
function _row_down(o){
	var tmp=o.parentNode.getElementsByTagName("TR");
	for(i=0;i<tmp.length;i++){
		tmp[i].className = '';
		tmp[i].selected=false;
	}
	o.selected=true;
	o.className='tr_over';
}
// returns the target element for the current event(e)
function _target(e){
	return (document.all)?event.srcElement:e.target;
}
// returns true if o is in container
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}
// disables elements of a given tag
// if a 3rd arguments is supplied it will only disable
// those elements with an attribute equal to the argument
// e.g. _elements_disable('IMG',true,'name')
// This will only disable images with the name attribute
function _elements_disable(tag, disable){
	var attrib = (arguments[2]) ? arguments[2] : false ;
	var els = document.getElementsByTagName(tag);
	for(i=0;i<els.length;i++){
		if(attrib){
			//if(tag=="IMG") alert(els[i].getAttribute(attrib))
			if(els[i].getAttribute(attrib)!=null){
				els[i].disabled = disable;
			}
		}else els[i].disabled = disable;
	}
}