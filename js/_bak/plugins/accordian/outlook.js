// ---------------------------------------------------------------------------
// this script is copyright (c) 2001 by Michael Wallner!
// http://www.wallner-software.com
// mailto:dhtml@wallner-software.com
//
// you may use this script on web pages of your own
// you must not remove this copyright note!
//
// This script featured on Dynamic Drive (http://www.dynamicdrive.com)
// Visit http://www.dynamicdrive.com for full source to this script and more
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
//                   Outlook like navigation bar version 1.2
//
// supported browsers:  IE4, IE5, NS4, NS6, MOZ, OP5
// needed script files: crossbrowser.js
//
// History:
// 1.0: initial version
// 1.1: no Reload in IE and NS6
// 1.2: no Reload in OP5 if width is not changed
// ---------------------------------------------------------------------------

//add one button to a panel
//einen Button zu einem Panel hinzufügen
//img:    image name - Name der Bilddatei
//label:  button caption - Beschriftung des Buttons
//action: javascript on MouseUp event - Javascript beim onMouseUp event
function b_addButton(type, img, label, action) {
	var args=arguments;
	this.type[this.type.length]=type;
  this.img[this.img.length]=img;
  this.lbl[this.lbl.length]=label;
  this.act[this.act.length]=action;
	this.html[this.html.length]=(args.length>4)?args[4]:"";
  this.sta[this.sta.length]=0;

  return this
}

//reset all panel buttons  (ns4, op5)
//alle Panel Buttons zurücksetzten (ns4, op5)
function b_clear() {
var i
  for (i=0;i<this.sta.length;i++) {
    if (this.sta[i] != 0)
      this.mOut(i);
  }
}

// ---------------------------------------------------------------------------
// Panel functions for ie4, ie5, ns5, op5
// ---------------------------------------------------------------------------

//test if scroll buttons should be visible
//teste ob Scroll-Buttons sichtbar sein sollen
function b_testScroll() {
	if(!this.scroll_on){
		this.objm1.style.visibility='hidden';
		this.objm2.style.visibility='hidden';
		return;
	}
	
	var i=parseInt(this.obj.style.height);
	var j=parseInt(this.objf.style.height);
  var k=parseInt(this.objf.style.top);
	
  if (k==38)
    this.objm1.style.visibility='hidden';
  else
    this.objm1.style.visibility='visible';

  if ((j+k)<i) {
    this.objm2.style.visibility='hidden';
  }
  else
    this.objm2.style.visibility='visible';
}

//scroll the panel content up
//scrolle den Panel Inhalt nach Oben
function b_up(nr) {
    this.ftop = this.ftop - 5;
    this.objf.style.top=this.ftop;
    nr--
    if (nr>0)
      setTimeout(this.v+'.up('+nr+');',10);
    else
      this.testScroll();
}

//scroll the panel content down
//scrolle den Panel Inhalt nach Unten
function b_down(nr) {
    this.ftop = this.ftop + 5;
    if (this.ftop>=38) {
      this.ftop=38;
      nr=0;
    }
    this.objf.style.top=this.ftop;
    nr--

    if (nr>0)
      setTimeout(this.v+'.down('+nr+');',10);
    else
      this.testScroll();
}

// ---------------------------------------------------------------------------
// Panel object
// ---------------------------------------------------------------------------

//create one panel
function createPanel(name,caption) {
	var args=arguments;
  this.name=name;                  // panel layer ID
	this.ftop=38;                    // actual panel scroll position
  this.obj=null;                   // panel layer object
  this.objc=null;                  // caption layer object
  this.objf=null;                  // panel field layer object
  this.objm1=null;                 // scroll button up
  this.objm2=null;                 // scroll button down
  this.caption=caption;            // panel caption
	this.type=new Array();					// items type for different display options etc: ob_button_1, ob_button_l_sub, imgB (so far...)
  this.img=new Array();            // button images
  this.lbl=new Array();            // button labels
  this.act=new Array();            // button actions
	this.html=new Array();           // HTML array for content inside accordian
  this.sta=new Array();            // button status (internal)
  this.addButton=b_addButton;      // add one button to panel
  this.clear=b_clear;              // reset all buttons
  this.content_type=(args.length>2)?args[2]:'';
  this.action=(args.length>3)?args[3]:'';
	this.scroll_on=(this.content_type!='');
	this.testScroll=b_testScroll;    // test if scroll buttons should be visible
	this.up=b_up;                    // scroll panel buttons up
	this.down=b_down;                // scroll panel buttons down
	
//alert(this.content_type)
  this.v = this.name + "var";   // global var of 'this'
  eval(this.v + "=this");

  return this
}

//add one panel to the outlookbar
function b_addPanel(panel) {
  panel.name=this.name+'_panel'+this.panels.length
  this.panels[this.panels.length] = panel;
}

// Draw the Outlook Bar
function b_draw() {
	var i;
	var j;
	var t=0;
	var h;
	var c=(this.moz)?0:0;
	var s="";
	var new_h;
	//OutlookBar layer..
	s+='<DIV id='+this.name+' style="position:absolute; left:';
	s+=this.xpos+'; top:'+this.ypos+'; width:'+this.width;
	s+='; height:'+this.height+'; background-color:'+this.bgcolor;
	s+='; clip:rect(0,'+this.width+','+this.height+',0)">';
	h=this.height-((this.panels.length-1)*28);

	//one layer for every panel...
	for (i=0;i<this.panels.length;i++) {
		s+='<DIV id='+this.name+'_panel'+i;
		if(this.panels[i].content_type!='') s+=' custom="'+this.panels[i].content_type+'"';
		s+=' style="padding:0;position:absolute; left:0; top:'+t;
		s+='; width:'+this.width+'; height:'+h+'; clip:rect(0px, ';
		s+=this.width+'px, '+h+'px, 0px); background-color:';
		s+=this.bgcolor+';">';
		t=t+this.buttonheight;
		new_h=(this.panels[i].img.length>1)?''+(this.panels[i].img.length*this.buttonspace):this.height-((this.panels.length)*(this.buttonheight+4));
		 //one layer to host the panel buttons
		s+='<div id='+this.name+'_panel'+i;
		s+='_f style="padding:0;position:absolute; left:0; top:38; width:';
		s+=this.width+'; height:'+new_h;
		s+='; background-color:'+this.bgcolor+';" >';
		mtop=0;
		button_space=this.buttonspace;

		//one (ie4, ie5, ns5) for every button
		for (j=0;j<this.panels[i].img.length;j++) {
			switch(this.panels[i].type[j]){
				case 'ob_button_l':case 'ob_button_l_sub':
					button_space=(this.panels[i].type[j]=='ob_button_l')?50:30;
					s+='<a href=\'javascript:void('+this.panels[i].act[j]+');\' class="'+this.panels[i].type[j]+'" style="background-image:url('+this.images_path+this.panels[i].img[j]+');">'+this.panels[i].lbl[j]+'</a>'
					break;
				case 'imgB':
					button_space=60;
					s+='<DIV id='+this.name+'_panel'+i+'_b'+j+' class=imgB style="position:absolute; left:0; width:'+this.width+'; top:'+mtop+'; text-align:center;">'+
					'<img src='+this.images_path+this.panels[i].img[j]+' class=imgnob onmouseOver="this.className=\'imgbout\';" onmouseDown="this.className=\'imgbin\';" onmouseUp=\'this.className="imgbout";'+this.panels[i].act[j]+';\' onmouseOut="this.className=\'imgnob\';">'+
					'<BR>'+this.panels[i].lbl[j]+'</DIV>';
					break;
				default:
					s+='<DIV id='+this.name+'_panel'+i+'_b'+j+' class=imgB ';
					s+='style="position:absolute;top:-10px;overflow: auto; left:0; width:'+this.width;
					s+='; top:'+mtop+';height:'+this.height-(this.panels.length*this.buttonheight)+'px;border:1px solid">';
					s+=this.panels[i].lbl[j];
					if(this.panels[i].html[j]!='')s+=this.panels[i].html[j];
					s+='</DIV>';
					break;
			}
			mtop=mtop+button_space;
		}
		s+='</DIV>';

		//one layer for the panels caption
		s+='<DIV id='+this.name+'_panel'+i+'_c class=button ';
		s+='onClick="javascript:'+this.v+'.showPanel('+i;
		s+=');" style="padding:0;position:absolute; left:0; top:0; width:';
		s+=(this.width-c)+'; height:'+(this.buttonheight-c)+';"><A href="#" ';
		s+='onClick="'+this.v+'.showPanel('+i+');this.blur();';
		s+='return false;" class=noLine>'+this.panels[i].caption;
		s+='</A></DIV>';
		//two layers for scroll-up -down buttons
		s+='<DIV id='+this.name+'_panel'+i;
		s+='_m1 style="position:absolute; top:40; left:';
		s+=(this.width-20)+';"><A href="#" onClick="';
		s+=this.panels[i].v+'.down(16);this.blur();return false;" ';
		s+='onmouseOver="'+this.panels[i].v+'.clear();">';
		s+='<img width=16 height=16 src='+this.images_path+'arrowup.gif border=0>';
		s+='</A></DIV>';
		s+='<DIV id='+this.name+'_panel'+i;
		s+='_m2 style="position:absolute;  top:';
		s+=(this.height-(this.panels.length)*this.buttonheight)+'; left:';
		s+=(this.width-20)+';"><A href="#" onClick="';
		s+=this.panels[i].v+'.up(16);this.blur();return false" ';
		s+='onmouseOver="'+this.panels[i].v+'.clear();">';
		s+='<img width=16 height=16 src='+this.images_path+'arrowdown.gif border=0>';
		s+='</A></DIV>';


		s+='</DIV>';

	}
  s+='</DIV>';
	document.write(s);s="";
  for (i=0;i<this.panels.length;i++) {
    try {
			this.panels[i].obj=getObj(this.name+'_panel'+i);
			this.panels[i].objc=getObj(this.name+'_panel'+i+'_c');
			this.panels[i].objf=getObj(this.name+'_panel'+i+'_f');
			this.panels[i].objm1=getObj(this.name+'_panel'+i+'_m1');
			this.panels[i].objm2=getObj(this.name+'_panel'+i+'_m2');
			if(this.panels[i].scroll_on) this.panels[i].testScroll();
		}catch(e){}
  }
	myOnResize();
	//actual panel is saved in a cookie
	if (this.cookies.enabled && this.cookies.get("panel"))
		this.showPanel(this.cookies.get("panel"));
	else
		if (document.location.search=='')  {
			this.showPanel(0);
		}
		else
			this.showPanel(document.location.search.substr(1,1));
}

// ---------------------------------------------------------------------------
// outlookbar function for ie4, ie5, ns5, op5
// ---------------------------------------------------------------------------

function b_showPanel(nr) {
	var i;
	var l;
	var o;
	var o1;
	this.cookies.set("panel",nr);
  this.aktPanel=nr;
  l = this.panels.length;
  for (i=0;i<l;i++) {
		p1=this.panels[i].obj;
		try{
			o1=p1.firstChild.firstChild;
			if(o1.id!='')
				if(parseInt(nr)==i)
					o1.style.visibility='';
				else
					o1.style.visibility='hidden';
		}catch(e){}
		if(i>nr) {
      p1.style.top=this.height-((l-i)*this.buttonheight);
    }
    else {
      p1.style.top=i*this.buttonheight;
    }
  }
}

//resize the Outlook Like Bar
//IE4/5 & NS6 -> resize all layers (width & height)
//op5         -> resize only height - reload on width change
//ns4         -> reload on any change!
//
//if you change the width of a layer (style="text-align:center;") then
//the content will not be moved!
function b_resize(x,y,width,height) {
	var o;
	var i;
	var j;
	var h;
	var ch;
	var c=0;//(this.moz)?6:0;

	this.xpos=x;
	this.yPos=y;
	this.width=width;
	this.height=height;
	
	o=getObj(this.name);
	
	o.style.left=x;
	o.style.top=y;
	o.style.width=width;
	o.style.height=height;
	o.style.clip='rect(0px '+this.width+'px '+this.height+'px 0px)';
	h=this.height-((this.panels.length-1)*this.buttonheight);
	ch=(this.height-(this.panels.length)*this.buttonheight)-5;
	for (i=0; i<this.panels.length; i++) {
	 try{o=getObj(this.name+'_panel'+i+'_c');
	 o.style.width=(this.width-c);
	 }catch(e){}
	 try{for (j=0;j<this.panels[i].img.length;j++) {
		 o=getObj(this.name+'_panel'+i+'_b'+j);
		 o.style.width=this.width;
	 }}catch(e){}
	
	 this.panels[i].objm1.style.left=(this.width-20);
	 this.panels[i].objm2.style.top=(this.height-(this.panels.length)*this.buttonheight);
	 this.panels[i].objm2.style.left=(this.width-20);
	 this.panels[i].objf.style.width=this.width;
	 this.panels[i].obj.style.width=this.width
	 this.panels[i].obj.style.height=h;
	 this.panels[i].obj.style.pixelHeight=h;
	 this.panels[i].obj.style.clip='rect(0px '+this.width+'px '+h+'px 0px)';
	 try{switch(this.panels[i].obj.getAttribute("custom")){
		 case 'tree':
		 	this.panels[i].objf.style.height=this.panels[i].obj.firstChild.firstChild.firstChild.style.height=ch;
			this.panels[i].scroll_on=false;
			break;
		 default:
		 	
		 	break;
	 }}catch(e){}
	 //if(this.scroll_on) 
	 this.panels[i].testScroll();
	}
	this.showPanel(this.aktPanel);
}

// ---------------------------------------------------------------------------
// OutlookBar object for ie4, ie5, ns5, op5
// ---------------------------------------------------------------------------

function createOutlookBar(name,path,x,y,width,height,bgcolor,pagecolor) {
  this.aktPanel=0;                        // last open panel
  this.name=name;                         // OutlookBar name
	this.images_path=path;                  // Global Outlook bar images path
  this.xpos=x;                            // bar x-pos
  this.ypos=y;                            // bar y-pos
  this.width=width;                       // bar width
  this.height=height;                     // bar height
  this.bgcolor=bgcolor;                   // bar background color
  this.pagecolor=pagecolor;               // page bgcolor (op5!)
  this.buttonspace=50;                    // distance of panel buttons
	this.buttonheight=28;
  this.panels=new Array()                 // OutlookBar panels
  this.addPanel=b_addPanel;               // add new panel to bar
  this.draw=b_draw;                       // write HTML code of bar
  this.showPanel=b_showPanel;             // make a panel visible (!=ns4)
  this.moz=(document.getElementById && navigator.appVersion.indexOf("MSIE ") == -1)?1:0;
  this.resize=b_resize;                   // resize Outlook Like Bar
	this.cookies=new _cookies();

  this.v = name + "var";                  // global var of 'this'
  eval(this.v + "=this");

  return this
}

