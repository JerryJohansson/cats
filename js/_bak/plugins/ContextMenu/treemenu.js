var aMenus = new Array();
var controlWindow = null;
var controlWindowName = "treeControl";
// MenuSystem
var vcoMenuSystemCollection=new Array(),vcoMenuSystemIndex=0;
function MenuSystem(id,sArrayName,oImages,vContainer,iWidth,btOptions){
	var args = MenuSystem.arguments;
	
	// System Name
	this.SystemIndex = vcoMenuSystemIndex;
	this.SystemName = "vcoMenuSystemCollection";
	this.SystemArrayName = this.SystemName + "[" + this.SystemIndex + "]";
	
	this.pos = (args.length>6)?args[6]:0;
	this.menus = new Array();
	this.x = this.pos.x;
	this.y = this.pos.y;
	this.offsetX = -15;
	this.offsetY = 3;
	this.w = iWidth;
	this.name = this.SystemName + id;
	this.id = id;
	this.aRef = eval(sArrayName);
	this.images = oMenuImages;
	this.count = 0;
	this.imageLocation = "images/";
	this.floating = (btOptions & 1) //== 0 ? 0 : 1 ;
	this.menuTimeout = 500;
	
	// get container
	this.container = (typeof(vContainer)=="object") ? vContainer : ((getElement(vContainer)!=null)?getElement(vContainer):createContainer(vContainer));
	
	this.HTML = "";
	
	this.build = function(){
		this.construct(this.aRef,0);
		this.display();
	}
	this.construct = function(a,iShift){
		var i=0;
		iShift++;
		this.HTML += (this.floating)?"<DIV id=\"MENU_" + this.id + "_OWNER\" style=\"border:2px outset;position:absolute;left:" + this.pos.x + "px;top:" + this.pos.y + "px;\">" : "" ;  
		for(i=0;i<a.length;i++){
			this.count++;
			this.OpenMenu(a[i],this.count);
			if(a[i].menu) {
				this.construct(a[i].menu,iShift);
			}
			this.CloseMenu();
		}
		this.HTML += (this.floating)?"</DIV>" : "" ;  
		
	}
	this.OpenMenu = function(oItem,iItemID){
		oItem.func = "";
		var id = oItem.id?oItem.id:iItemID;
		var loc = this.imageLocation;
		var ico = oItem.icon ?  oItem.icon : 0 ;
		var img = loc + (oItem.menu ? this.images.folders[ico][1]  : this.images.icons[ico]) + ".gif" ;
		var style = this.floating ? "cursor:default;font:11px verdana;border:1px dotted #009999;color:#990099;background:#CCCCCC;width:" + this.w + "px;" : "cursor:default;font:11px verdana;color:#990099;border:1px outset;background:#CCCCCC;width:" + this.w + "px;";
		var sMenuStyle = this.floating ? "position:absolute;" : "" ;
		var sContStyle = this.floating ? "border:2px outset;" : "margin-left:16px;" ;
		var mOver = "this.style.backgroundColor='#9999CC';this.style.color='#FFFFCC';" + (this.floating ? this.SystemArrayName + ".MenuSystemOver(this);" : "" );
		var mOut = "this.style.backgroundColor='#CCCCCC';this.style.color='#990099';" + (this.floating ? this.SystemArrayName + ".MenuSystemOut(this);" : "" );
		var mClick = this.SystemArrayName + ".MenuSystemClick(this);" + oItem.func + ";";
		var mType = oItem.menu ? 1 : 0 ;
		var mIcons = mType ? this.images.folders[ico].join() : this.images.icons[ico] ;
		
		if(oItem.seperator==1){
			this.HTML +="<DIV id=sep style=\"" + style + "height:2px;border:none;margin:0px;\"><HR>";
		}else{
			this.HTML	+="<DIV id=\"MENU_" + this.id + "_" + id + "\" " +
									"MENUID=\"" + this.id + "\" " +
									"style=\"" + style + "\" " +
									"onmouseover=\"" + mOver + "\" " +
									"onmouseout=\"" + mOut + "\" " +
									"onclick=\"" + mClick + "\" " +
									"type=\"" + mType + "\" " +
									"icon=\"" + mIcons + "\" " +
									"oncontextmenu=\"alert('hello');return false;\" " +
									"imgloc=\"" + this.imageLocation + "\">" + 
									"<IMG id=\"MENU_" + this.id + "_IMG_" + id + "\" src=\"" + img + "\" style=\"width:32px;height:16px;margin-right:5px;\" align=\"absmiddle\">" +
									"<SPAN onclick='alert(this.parentElement.outerHTML);'>" +
									this.encode_nbsp(oItem.desc + " - MENU_" + this.id + "_" + id) + "</SPAN>" +
								"</DIV>" +
								"<DIV id=\"MENU_" + this.id + "_CONTAINER_" + id + "\" " +
									"MENUID=\"" + this.id + "\" " +
									"style=\"display:none;" + sContStyle + sMenuStyle + "\">";
		}
	}
	this.CloseMenu = function(){
		this.HTML +="</DIV>";
	}
	this.MenuItem = function(oItem,id){
		
	}
	this.encode_nbsp = function(s){
		return s.replace(/\s/g,"&nbsp;");
	}
	this.display = function(){
		//this.container.insertAdjacentHTML("BeforeEnd",this.HTML);
		this.container.innerHTML += this.HTML;
	}
		
		
		
	this.MenuSystemClick = function(item){
		var id = item.getAttribute("id").substring(item.getAttribute("id").lastIndexOf("_")+1);
		var mID = item.getAttribute("MENUID");
		var oCont = getElement("MENU_" + mID + "_CONTAINER_" + id);
		var oImg = getElement("MENU_" + mID + "_IMG_" + id);
		var aImgs = item.getAttribute("icon").split(",");
		if(item.type=="1"){
			oImg.src = item.getAttribute("imgloc") + ((oCont.style.display == "none") ? aImgs[0] + ".gif" : aImgs[1] + ".gif" );
		}
		oCont.style.display = (oCont.style.display == "none") ? "" : "none" ;
	}
	this.MenuSystemOver = function(item){
		var id = item.getAttribute("id").substring(item.getAttribute("id").lastIndexOf("_")+1);
		var mID = this.getAttribute("id");
		var oCont = getElement("MENU_" + mID + "_CONTAINER_" + id);
		var oImg = getElement("MENU_" + mID + "_IMG_" + id);
		var aImgs = item.getAttribute("icon").split(",");
		if(item.type=="1")
			oImg.src = item.getAttribute("imgloc") + aImgs[0] + ".gif";
		oCont.MENUON=1;
		item.parentElement.parentElement.setAttribute("MENUON", 1);
		oCont.style.pixelTop = oCont.previousSibling.offsetTop + this.offsetY - this.y;
		oCont.style.pixelLeft = oCont.previousSibling.offsetLeft + this.w + this.offsetX - this.x;
		oCont.style.display = "block";
	}
	this.MenuSystemOut = function(item){
		var id = item.id.substring(item.id.lastIndexOf("_")+1);
		var mID = this.id; //item.MENUID;
		var sCont = "getElement('MENU_" + mID + "_CONTAINER_" + id + "')";
		var sItem = "getElement('" + item.id + "')";
		var oCont = eval(sCont);
		var oImg = getElement("MENU_" + mID + "_IMG_" + id);
		var aImgs = item.icon.split(",");
		var childOn = (eval(sCont).style.display=="none") ? false : true ;
		
		if(item.type=="1")
			oImg.src = item.imgloc + aImgs[1] + ".gif";

		item.parentElement.parentElement.setAttribute("MENUON", 0);
		oCont.setAttribute("MENUON", 0);
		setTimeout("MenuSystemHideItem(" + sCont + "," + this.menuTimeout + ");",this.menuTimeout);
		
	}
	
	vcoMenuSystemCollection[vcoMenuSystemIndex++] = this;
	
	return this;
}
function MenuSystemHideItem(item,iTimeOut){
	var on=parseInt(item.MENUON);
	dbg(item.id)
	if(!(on==1)){
		item.style.display = "none";
		//dbg("HIDE::"+item.id);
	}else setTimeout("MenuSystemHideItem(getElement('" + item.id + "'));",iTimeOut);
}
function getElement(id){
	return document.getElementById(id);
}
function createContainer(sID){
	var o=document.createElement("DIV");
	o.id = sID;
	document.body.appendChild(o);
	return getElement(sID);
}
//dbg
function alertMe(s){
	alert(s);return s;
}
function dbg(s){source.value += s + "\n";}