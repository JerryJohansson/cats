<?php
/**
 * Show a list of images in a long horizontal table.
 * @author $Author: Wei Zhuo $
 * @version $Id: images.php 27 2004-04-01 08:31:57Z Wei Zhuo $
 * @package ImageManager
 */

require_once('config.inc.php');
require_once('Classes/AssetManager.php');

//default path is /
$relative = '/';
$manager = new AssetManager($ASConfig);

//process any file uploads
$manager->processUploads();

$manager->deleteFiles();

$refreshDir = false;
//process any directory functions
if($manager->deleteDirs() || $manager->processNewDir())
	$refreshDir = true;

//check for any sub-directory request
//check that the requested sub-directory exists
//and valid
if(isset($_REQUEST['dir']))
{
	$path = rawurldecode($_REQUEST['dir']);
	if($manager->validRelativePath($path))
		$relative = $path;
}


$manager = new AssetManager($ASConfig);


//get the list of files and directories
$list = $manager->getFiles($relative);
$view = (isset($_REQUEST['view']))?$_REQUEST['view']:'';
$asset_manager_post=($view=='image')?"images.php":"assets.php";
/* ================= OUTPUT/DRAW FUNCTIONS ======================= */
function getFileType($extension='unknown'){
	$exts=array(
		"doc"=>"Word Document",
		"xls"=>"Exel Spreadsheet",
		"csv"=>"Comma Seperated File",
		"mp3"=>"MP3 Audio File",
		"avi"=>"AVI Movie",
		"txt"=>"Text Document",
		"xsl"=>"XSL Stylesheet",
		"xml"=>"XML Document",
		"htm"=>"HTML Document (htm)",
		"html"=>"HTML Document (html)",
		"unknown"=>"Unknown document type"
	);
	return $exts[$extension];
}
/**
 * Draw the files in an table.
 */
function drawFiles($list, &$manager)
{
	global $relative;
?><tr><?php
	foreach($list as $entry => $file) 
	{ ?>
		<td><table width="100" cellpadding="0" cellspacing="0"><tr><td class="block">
		<a href="javascript:;" onclick="selectImage('<? echo $file['relative'];?>', '<? echo $entry; ?>', <? echo $file['image'][0];?>, <? echo $file['image'][1]; ?>);"title="<? echo $entry; ?> - <? echo Files::formatSize($file['stat']['size']); ?>"><img src="<? echo $manager->getThumbnail($file['relative']); ?>" alt="<? echo $entry; ?> - <? echo Files::formatSize($file['stat']['size']); ?>"/></a>
		</td></tr><tr><td class="edit">
			<a href="<?php echo $asset_manager_post;?>?dir=<? echo $relative; ?>&amp;delf=<? echo rawurlencode($file['relative']);?>" title="Trash" onclick="return confirmDeleteFile('<? echo $entry; ?>');"><img src="img/edit_trash.gif" height="15" width="15" alt="Trash"/></a><a href="javascript:;" title="Edit" onclick="editImage('<? echo rawurlencode($file['relative']);?>');"><img src="img/edit_pencil.gif" height="15" width="15" alt="Edit"/></a>
		<? if($file['image']){ echo $file['image'][0].'x'.$file['image'][1]; } else echo $entry;?>
		</td></tr></table></td> 
	  <? 
	}//foreach
?></tr><?php
}//function drawFiles

/**
 * Draw the files list in an table.
 */
function drawFilesList($list, &$manager)
{
	global $relative;

	foreach($list as $entry => $file) 
	{ ?>
		<tr>
		<td width="20"><img src="img/files/<? echo $file['pathinfo']['extension']; ?>.gif" 
		<? if($file['image']){ ?> 
			onclick="selectImage('<? echo $file['relative'];?>', '<? echo $entry; ?>', <? echo $file['image'][0];?>, <? echo $file['image'][1]; ?>,'<? echo Files::formatSize($file['stat']['size']);?>');" 
		<? } else { ?>
			onclick="selectDocument('<? echo $file['relative'];?>', '<? echo $entry; ?>','<? echo Files::formatSize($file['stat']['size']);?>');" 
		<? } ?>
			alt="<? echo $entry; ?> - <? echo Files::formatSize($file['stat']['size']); ?>"/></td>
		<td width="50%">
		<?php if(is_array($file['image'])){ ?>
		
		<a href="javascript:;" onclick="selectImage('<? echo $file['relative'];?>', '<? echo $entry; ?>', <? echo $file['image'][0];?>, <? echo $file['image'][1]; ?>,'<? echo Files::formatSize($file['stat']['size']);?>');" title="<? echo $entry; ?> - <? echo Files::formatSize($file['stat']['size']); ?>"><? echo $entry; ?></a>

		<?php } else { ?>
		
		<a href="javascript:;" onclick="selectDocument('<? echo $file['relative'];?>', '<? echo $entry; ?>','<? echo Files::formatSize($file['stat']['size']);?>');" title="<? echo $entry; ?> - <? echo Files::formatSize($file['stat']['size']); ?>"><? echo $entry; ?></a>
		
		<?php } ?>
		</td>
		
		<?php if($file['image']){ ?>
			<td><? echo $file['image'][0].'x'.$file['image'][1];?>&nbsp;</td>
			<td class="edit" align="right" width="20">
			<a href="javascript:;" title="View" onclick="viewImage('<? echo $file['url'];?>');"><img src="img/edit_active.gif" height="15" width="15" alt="Edit"/></a>
			</td>
			<td class="edit" width="20">
			<a href="javascript:;" title="Edit" onclick="editImage('<? echo rawurlencode($file['relative']);?>');"><img src="img/edit_pencil.gif" height="15" width="15" alt="Edit"/></a>
			</td>
		<?php } else { ?>
			<td colspan="2"><? echo getFileType($file['pathinfo']['extension']);?>&nbsp;</td>
			<td class="edit" align="right" width="20">
			<a href="javascript:;" title="Edit" onclick="editDocument('<? echo $file['url'];?>');"><img src="img/edit_pencil.gif" height="15" width="15" alt="Edit"/></a>
			</td>
		<?php } ?>
			<td class="edit" style="border-left:1px solid #999;" width="20"><a href="<?php echo $asset_manager_post;?>?dir=<? echo $relative; ?>&amp;delf=<? echo rawurlencode($file['relative']);?>" title="Trash" onclick="return confirmDeleteFile('<? echo $entry; ?>');"><img src="img/edit_trash.gif" height="15" width="15" alt="Trash"/></a></td>
		</td></tr>
	  <? 
	}//foreach
}//function drawFilesList


/**
 * Draw the directory.
 */
function drawDirs($list, &$manager) 
{
	global $relative;
?><tr><?php
	foreach($list as $path => $dir) 
	{ ?>
		<td><table width="100" cellpadding="0" cellspacing="0"><tr><td class="block">
		<a href="<?php echo $asset_manager_post;?>?dir=<? echo rawurlencode($path); ?>" onclick="updateDir('<? echo $path; ?>')" title="<? echo $dir['entry']; ?>"><img src="img/folder.gif" height="80" width="80" alt="<? echo $dir['entry']; ?>" /></a>
		</td></tr><tr>
		<td class="edit">
			<a href="<?php echo $asset_manager_post;?>?dir=<? echo $relative; ?>&amp;deld=<? echo rawurlencode($path); ?>" title="Trash" onclick="return confirmDeleteDir('<? echo $dir['entry']; ?>', <? echo $dir['count']; ?>);"><img src="img/edit_trash.gif" height="15" width="15" alt="Trash"/></a>
			<? echo $dir['entry']; ?>
		</td>
		</tr></table></td>
	  <? 
	} //foreach
?></tr><?php
}//function drawDirs

/**
 * Draw the directory.
 */
function drawDirsList($list, &$manager) 
{
	global $relative;

	foreach($list as $path => $dir) 
	{ ?>
		<tr>
		<td>
		<img src="img/files/folder.gif" onclick="location.href='<?php echo $asset_manager_post;?>?dir=<? echo rawurlencode($path); ?>';updateDir('<? echo $path; ?>')" alt="<? echo $dir['entry']; ?>" />
		</td>
		<td width="50%">
		<a href="<?php echo $asset_manager_post;?>?dir=<? echo rawurlencode($path); ?>" onclick="updateDir('<? echo $path; ?>')" title="<? echo $dir['entry']; ?>">
		<? echo $dir['entry']; ?></a>
		</td>
		<td colspan="3">[folder]</td>
		<td class="edit" style="border-left:1px solid #999;" width="20"><a href="<?php echo $asset_manager_post;?>?dir=<? echo $relative; ?>&amp;deld=<? echo rawurlencode($path); ?>" title="Trash" onclick="return confirmDeleteDir('<? echo $dir['entry']; ?>', <? echo $dir['count']; ?>);"><img src="img/edit_trash.gif" height="15" width="15" alt="Trash"/></a></td>
		</tr>
	  <? 
	} //foreach
}//function drawDirsList

/**
 * No directories and no files.
 */
function drawNoResults() 
{
?>
<table width="100%">
  <tr>
    <td class="noResult">No Images Found</td>
  </tr>
</table>
<?	
}

/**
 * No directories and no files.
 */
function drawErrorBase(&$manager) 
{
?>
<table width="100%">
  <tr>
    <td class="error">Invalid base directory: <? echo $manager->config['base_dir']; ?></td>
  </tr>
</table>
<?	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>Image List</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="assets/imagelist.css" rel="stylesheet" type="text/css" />
	<style>
	a, a:link, a:visited {
	padding: 2px;
	color: #666;
	text-decoration:none;
	}
	a:hover {
	color:#333333;
	padding: 1px;
	border: 1px solid #999;
	text-decoration:underline;
	}
	img{
	border:0;
	padding-right:3px;
	}
	.file_list{
	font-family: Verdana, Arial;
	font-size: 11px;
	}
	.file_list a{
	display:block;
	}
	</style>
<script type="text/javascript" src="assets/dialog.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
var ASSETMANAGER_ON=true;

	if(window.top)
		I18N = window.top.I18N;

	function hideMessage()
	{
		var topDoc = window.top.document;
		var messages = topDoc.getElementById('messages');
		if(messages)
			messages.style.display = "none";
	}

	init = function()
	{
		hideMessage();
		var topDoc = window.top.document;

<? 
	//we need to refesh the drop directory list
	//save the current dir, delete all select options
	//add the new list, re-select the saved dir.
	if($refreshDir) 
	{ 
		$dirs = $manager->getDirs();
?>
		var selection = topDoc.getElementById('dirPath');
		var currentDir = selection.options[selection.selectedIndex].text;

		while(selection.length > 0)
		{	selection.remove(0); }
		
		selection.options[selection.length] = new Option("/","<? echo rawurlencode('/'); ?>");	
		<? foreach($dirs as $relative=>$fullpath) { ?>
		selection.options[selection.length] = new Option("<? echo $relative; ?>","<? echo rawurlencode($relative); ?>");		
		<? } ?>
		
		for(var i = 0; i < selection.length; i++)
		{
			var thisDir = selection.options[i].text;
			if(thisDir == currentDir)
			{
				selection.selectedIndex = i;
				break;
			}
		}		
<? } ?>
	}	

	function editImage(image) 
	{
		var url = "editor.php?img="+image;
		Dialog(url, function(param) 
		{
			if (!param) // user must have pressed Cancel
				return false;
			else
			{
				return true;
			}
		}, null);		
	}
	
	function editDocument(url) 
	{
		if(confirm("If you edit the document you will not be able to save it to the server directly.\nYou must save to your local hard drive and unload the file again.\n\nAre you sure you want to continue?")){
			var win=window.open(url,"new");
			win.focus();
		}
	}
	
	function viewImage(url)
	{
		var win=window.open("","new_preview","width=100,height=100,resizable=1");
		win.blur();
		win.document.open();
		win.document.write("<img src='"+url+"' onload='window.resizeTo(this.width+20,this.height+70);window.focus();' />");
		win.document.close();
	}
/*]]>*/
</script>
<script type="text/javascript" src="assets/assets.js"></script>
</head>

<body>
<? if ($manager->isValidBase() == false) { drawErrorBase($manager); } 
	elseif(count($list[0]) > 0 || count($list[1]) > 0) { ?>
<table width="100%" class="file_list" cellpadding="0" cellspacing="0">
<? 
if($view!='image') {
	drawDirsList($list[0], $manager); 
	drawFilesList($list[1], $manager); 
}else{
	drawDirs($list[0], $manager);
	drawFiles($list[1], $manager); 
}
?>
</table>
<? } else { drawNoResults(); } ?>
</body>
</html>
