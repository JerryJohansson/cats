<?php
/**
 * Image Manager configuration file.
 * @author $Author: Wei Zhuo $
 * @version $Id: config.inc.php 27 2004-04-01 08:31:57Z Wei Zhuo $
 * @package ImageManager
 */
/* 
 File system path to the directory you want to manage the images
 for multiple user systems, set it dynamically.

 NOTE: This directory requires write access by PHP. That is, 
       PHP must be able to create files in this directory.
	   Able to create directories is nice, but not necessary.
*/
/*
 The URL to the above path, the web browser needs to be able to see it.
 It can be protected via .htaccess on apache or directory permissions on IIS,
 check you web server documentation for futher information on directory protection
 If this directory needs to be publicly accessiable, remove scripting capabilities
 for this directory (i.e. disable PHP, Perl, CGI). We only want to store assets
 in this directory and its subdirectories.
*/
$domain_split=explode("/",$_SERVER['SCRIPT_NAME']);
$site_domain=$domain_split[1];
$site_domain_name=explode(".",$site_domain);
$theme=$site_domain_name[0];
$IMConfig['base_dir'] = 'D:/Projects/'.$theme.'/www_source/assets/';
$ASConfig['base_dir'] = 'D:/Projects/'.$theme.'/www_source/assets/';

$IMConfig['base_url'] = 'http://localhost:8080/'.$site_domain.'/assets/';
$ASConfig['base_url'] = 'http://localhost:8080/'.$site_domain.'/assets/';
?>