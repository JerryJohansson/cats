/**
 * The ImageManager plugin javascript.
 * @author $Author: Wei Zhuo $
 * @version $Id: image-manager.js 26 2004-03-31 02:35:21Z Wei Zhuo $
 * @package ImageManager
 */

/**
 * To Enable the plug-in add the following line before HTMLArea is initialised.
 *
 * HTMLArea.loadPlugin("ImageManager");
 *
 * Then configure the config.inc.php file, that is all.
 * For up-to-date documentation, please visit http://www.zhuo.org/htmlarea/
 */

/**
 * It is pretty simple, this file over rides the HTMLArea.prototype._insertImage
 * function with our own, only difference is the popupDialog url
 * point that to the php script.
 */
function ImageManager(editor)
{
	var tt = ImageManager.I18N;	

};
ImageManager.I18N = {
	"Image Manager"									: "Image Manager",
	"Crop"											: "Crop"
};
ImageManager._pluginInfo = {
	name          : "ImageManager",
	version       : "1.0",
	developer     : "Xiang Wei Zhuo",
	developer_url : "http://www.zhuo.org/htmlarea/",
	license       : "htmlArea"
};


// Over ride the _insertImage function in htmlarea.js.
// Open up the ImageManger script instead.
HTMLArea.prototype._insertImage = function(image) {

	var editor = this;	// for nested functions
	var outparam = null;
	if (typeof image == "undefined") {
		image = this.getParentElement();
		if (image && !/^img$/i.test(image.tagName))
			image = null;
	}
	if (image) outparam = {
		f_url    : HTMLArea.is_ie ? image.src : image.getAttribute("src"),
		f_alt    : image.alt,
		f_border : image.border,
		f_align  : image.align,
		f_vert   : image.vspace,
		f_horiz  : image.hspace,
		f_width  : image.width,
		f_height  : image.height
	};

	var manager = _editor_url + 'plugins/ImageManager/manager.php';

	Dialog(manager, function(param) {
		if (!param) {	// user must have pressed Cancel
			return false;
		}
		var img = image;
		if (!img) {
			var sel = editor._getSelection();
			var range = editor._createRange(sel);			
			editor._doc.execCommand("insertimage", false, param.f_url);
			if (HTMLArea.is_ie) {
				img = range.parentElement();
				// wonder if this works...
				if (img.tagName.toLowerCase() != "img") {
					img = img.previousSibling;
				}
			} else {
				img = range.startContainer.previousSibling;
			}
		} else {			
			img.src = param.f_url;
		}
		
		for (field in param) {
			var value = param[field];
			switch (field) {
			    case "f_alt"    : img.alt	 = value; break;
			    case "f_border" : img.border = parseInt(value || "0"); break;
			    case "f_align"  : img.align	 = value; break;
			    case "f_vert"   : img.vspace = parseInt(value || "0"); break;
			    case "f_horiz"  : img.hspace = parseInt(value || "0"); break;
					case "f_width"  : img.width = parseInt(value || "0"); break;
					case "f_height"  : img.height = parseInt(value || "0"); break;
			}
		}
		
		
	}, outparam);
};

// Over ride the _insertImage function in htmlarea.js.
// Open up the ImageManger script instead.
HTMLArea.prototype._insertDocument = function(doc,obj1,images_only) {
	var args=arguments;
	var editor = this;	// for nested functions
	var outparam = null;
	var image = null;
	var extra_param = query = "";
	if (typeof doc == "undefined") {
		image = this.getParentElement();
		if (image && !/^img$/i.test(image.tagName))
			image = null;
	}
	
	switch(typeof(images_only)){
		case 'undefined':
			images_only = false;
			break;
		case 'string':
			extra_param="selectfiles";
			query = ((query.match(/^\?/)==null)?"?":"&")+"view="+images_only;
			images_only = false;
			break;
		default:
			extra_param="image_button";
			// do nothing here
			break;
	}
	
	var return_function=function(obj){
		var s="";
		for(i in obj)s+=i+":"+obj[i]+";\n";
		//alert(s);
		if(typeof doc == "object"){
			var tname=doc.tagName.toLowerCase();
			switch(tname){
				case "input": case "button":
					switch(doc.type.toLowerCase()){
						case "button": case 'hidden':
							//alert(extra_param);
							switch(extra_param){
								case 'selectfiles': case 'multi':
									doc.value=obj.selected;
									args[1].innerHTML=obj.selected_urls
									break;
								case "image_button":
									doc.innerHTML = '<img name="Ichange_user_image" src ="'+obj.url+'" width="100" />';
									args[1].value = obj.url;
									break;
								default:
									doc.value=obj.url+":"+obj.size;
									break;
							}
							break;
						case 'unknown':
							alert("Unexpected Error occured:\n\nDon't know how we got here but this is the chosend URL:"+obj.url);
							break;
						default:
						
							doc.value=obj.url;
							break;
					};
					break;
				case "div":case "span":
					doc.innerHTML = '<img name="Ichange_user_image" src ="'+obj.url+'" width="100" />';
					args[1].value = obj.url;
					break;
				
			};
		}
	};
	
	if (image){ 
		outparam = {
			f_url    : HTMLArea.is_ie ? image.src : image.getAttribute("src"),
			f_alt    : image.alt,
			f_border : image.border,
			f_align  : image.align,
			f_vert   : image.vspace,
			f_horiz  : image.hspace,
			f_width  : image.width,
			f_height  : image.height,
			return_type : 'image'
		};
	}else{ 
		var return_type = extra_param;
		switch(typeof(args[1])){
			case "object": case "string":
				f_sel = doc.value;//args[1]
				f_u = "";
				break;
			default:
				f_sel = '';
				f_u = doc.value;
				break;
		}

		outparam = {
			f_file   : '',
			f_url    : f_u,
			f_alt    : '',
			f_border : '',
			f_align  : '',
			f_vert   : '',
			f_horiz  : '',
			f_width  : '',
			f_height  : '',
			f_selected : f_sel
		};
	}
	
	// set the path and query for the manager
	if(images_only)	{
		query+=((query!='')?"&":"?")+"images_only=1";
		var manager = _editor_url + 'plugins/ImageManager/manager.php'+query;
	}else{
		var manager = _editor_url + 'plugins/ImageManager/asset_manager.php'+query;
	}
	//alert(manager)
	Dialog(manager, function(param) {
		if (!param) {	// user must have pressed Cancel
			return false;
		}
		
		var img = image;
		if (img!=null && editor!=null) {
			var sel = editor._getSelection();
			var range = editor._createRange(sel);			
			editor._doc.execCommand("insertimage", false, param.f_url);
			if (HTMLArea.is_ie) {
				img = range.parentElement();
				// wonder if this works...
				if (img.tagName.toLowerCase() != "img") {
					img = img.previousSibling;
				}
			} else {
				img = range.startContainer.previousSibling;
			}
		}else img=new Object();
		
		for (field in param) {
			var value = param[field];
			switch (field) {
			    case "f_alt"    : img.alt	 = value; break;
			    case "f_border" : img.border = parseInt(value || "0"); break;
			    case "f_align"  : img.align	 = value; break;
			    case "f_vert"   : img.vspace = parseInt(value || "0"); break;
			    case "f_horiz"  : img.hspace = parseInt(value || "0"); break;
					case "f_width"  : img.width = parseInt(value || "0"); break;
					case "f_height" : img.height = parseInt(value || "0"); break;
					case "f_size"   : img.size = value; break;
					case "f_url"    : img.url = value; break;
					case "f_file"   : img.file = value.replace(/^\//,""); break;
					case "f_selected_files" : img.selected = value; break;
					case "relative"	: img.relative = value; break;
					case "f_selected_urls"	: img.selected_urls = value; break;
			}
		}
		
		if(typeof return_function == "function") return_function(img,extra_param);
		
		
	}, outparam);
};
