
function initEditor() {
    // create an editor for the "ta" textbox
    editor = new HTMLArea("content");
    // add a contextual menu
    editor.registerPlugin("ContextMenu");

    var config = editor.config;
    config.pageStyle = "<style type='text/css'>@import url(templates/"+arguments[0]+"/style.css);</style>\n" +
								"<style type='text/css'>@import url(templates/"+arguments[0]+"/classes.css);</style>\n";
								//alert(config.pageStyle);
		//config.pageStyle = "<link ref='stylesheet' type='text/css' href='templates/style.css' />\n" +
				//							"<link ref='stylesheet' type='text/css' href='templates/classes.css' />\n";
    config.width = '100%';
    config.height = ''+getAvailabelHeight('content')+'';
		config.toolbar = [
        [ "fontname", "space",
          "fontsize", "space",
          "formatblock", "space",
          "bold", "italic", "underline", "strikethrough", "separator",
          "subscript", "superscript", "separator",
          "copy", "cut", "paste", "space", "undo", "redo" ],

        [ "justifyleft", "justifycenter", "justifyright", "justifyfull", "separator",
          "insertorderedlist", "insertunorderedlist", "outdent", "indent", "separator",
          "forecolor", "hilitecolor", "separator",
          "inserthorizontalrule", "createlink", "insertimage", "inserttable", "htmlmode"]
    ];
		editor.config.baseURL = _editor_base_url;
    
    // register the SpellChecker plugin
    editor.registerPlugin(TableOperations);
		
    /***** CSS STYLES ********/
    /*editor.registerPlugin(CSS,
        {combos : [
            { label: "Styles:",
              options: getStyles("ed"),
              context: ""
            }
            ]
        });*/
    /***** CSS STYLES ********/

  setTimeout(function() {
    editor.generate();
  }, 500);
  return false;
}
function getAvailabelHeight(sobj){
	var obj=el=element(sobj);
	var h=h1=0;
	var bh=(!document.all)?window.innerHeight:document.body.offsetHeight-40;
	while(((el=el.parentNode)!=null) && el.nodeName != "BODY" ){
		h1+=parseInt(el.offsetTop);
	}
	h=(bh-h1);
	return h;
}
function insertHTML() {
  var html = prompt("Enter some HTML code here");
  if (html) {
    editor.insertHTML(html);
  }
}
function highlight() {
  editor.surroundHTML('<span style="background-color: yellow">', '</span>');
}
function addDocs(item, cont, name){
	var f=item.form;
	var oCont = document.getElementById(cont);
	var o = document.createElement("INPUT");
	var br = document.createElement("BR");
	o.type = "file";
	o.name = name;
	oCont.appendChild(br);
	oCont.appendChild(o);
}
var HYPERLINKS_CONT = null;
var HYPERLINKS_ITEM = null;
var HYPERLINKS_ITEM_URL = null;
function addHyperlinks(item_id,cont){
	HYPERLINKS_CONT=document.getElementById(cont);
	HYPERLINKS_ITEM = item_id;
	var args=arguments;
	var multi=(args.length>2)?args[2]:'';
	if(args.length>3) HYPERLINKS_ITEM_URL = args[3];
	if(multi!='') multi="&multi=1";
	else multi = '';
	var query_index = item_id.value.indexOf("?id=");
	var ids_value=(query_index!=-1)?item_id.value.substring(query_index+4):item_id.value;
	var win=window.open("popup_hyperlink.php?ids="+ids_value+multi,"links","width=600, height=600, top=0, left=0");
	win.focus();
}
function addHyperLinksToList(s,list,url){
	//alert(arguments[0]+":"+arguments[1]+":"+arguments[2])
	if(HYPERLINKS_CONT) HYPERLINKS_CONT.innerHTML=list;
	if(HYPERLINKS_ITEM) HYPERLINKS_ITEM.value=s;
	if(HYPERLINKS_ITEM_URL) HYPERLINKS_ITEM_URL.value=url;
}
function showhide(itm){
	if(document.all)document.body.focus();
	tab_onclick(itm.getAttribute('id'));
}
function tab_onclick(itm){
	if(typeof(itm)=="string") itm=element(itm);
	if(itm.blur) itm.blur();
	var b = element("tab_buttons");
	var a = b.getElementsByTagName("A");
	var id = 0;
	for(i=0;i<a.length;i++){
		if(a[i]==itm) id=i;
		a[i].className='';
		element('tab_panel['+(i)+']').style.display='none';
	}
	element('tab_panel['+id+']').style.display='block';
	itm.className='indent';
	return false;
}
function initTabs(){
	tab_onclick("Editor");
}
function unloadTabs(){
	
}
function setFocus(){
	
}
function validateForm(itm,sValidate){
	var b = element("tab_buttons");
	var a = b.getElementsByTagName("A");
	var i=0;
	var x=a.length;
	var form=itm.form;
	try{
		element('loading_message').style.visibility='';
		for(i=0;i<x;i++){
			element('tab_panel['+(i)+']').style.display='block';
		}
		if(typeof(EDITOR_VALIDATE)=='undefined') EDITOR_VALIDATE="";
		if(EDITOR_VALIDATE=="") return true;
		var valid=new _validation("title;empty;Title;"+EDITOR_VALIDATE.replace(/^;()/,""));
		valid.setForm(itm.form);
		var validated=valid.validate();
		if(validated)
			return true;
		else {
			for(i=0;i<x;i++) if(i!=0) element('tab_panel['+(i)+']').style.display='none';
			element('loading_message').style.visibility='hidden';
			return false;
		}
	}catch(e){
		element('loading_message').style.visibility='hidden';
		alert("Error while validating form:\nProbably a field in the EDITOR_VALIDATE list does not exist.\n\nError:\n"+e);
		return false;
	}
}