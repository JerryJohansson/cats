
// retrieve XML document (reusable generic function);
// parameter is URL string (relative or complete) to
// an .xml file whose Content-Type is a valid XML
// type, such as text/xml; XML source must be from
// same domain as HTML file
/***************************************************
	properties=
	{
		onreadystatechange:	'Event handler for an event that fires at every state change',
		readyState:	{Object_status_integer:[
		'0 = uninitialized',
		'1 = loading',
		'2 = loaded',
		'3 = interactive',
		'4 = complete']},
		responseText:	'String version of data returned from server process',
		responseXML:	'DOM-compatible document object of data returned from server process',
		status:	'Numeric code returned by server, such as 404 for "Not Found" or 200 for "OK"',
		statusText:	'String message accompanying the status code'
	}
	***********************************************/
	/***********************************************
	methods
	{
		abort:function(){//	Stops the current request
			this.response.abort();
		},
		getAllResponseHeaders: function(){//	Returns complete set of headers (labels and values) as a string
			this.request.getAllResponseHeaders();
		},
		getResponseHeader: function("headerLabel"){//	Returns the string value of a single header label
			this.request.getResponseHeader(name);
		},
		open: function("method", "URL"[, asyncFlag[, "userName"[, "password"]]]){//	Assigns destination URL, method, and other optional attributes of a pending request
			this.request.open("post",url,true);
		},
		send: function(content){//	Transmits the request, optionally with postable string or DOM object data
			this.request.send(content);
		},
		setRequestHeader: function("label", "value"){//	Assigns a label/value pair to the header to be sent with a request
			this.request.setRequestHeader(name,value);
		}
	}
	***************************************************/
	// global request and XML document objects
	
	//--------------------------------------------------------------------------
	var _show_message=function(s){
		//window.status=s;
		element(_status_message).innerHTML=s;
	}
	var _XMLHttp_messages={
		readyState:['Waiting for response from the server','Connected: Transfering data...','Please wait while initialising','Complete','Remote load - '],
		errors:['There was an error while processing response']
	};
	
	//experiment
	var experimental=true;
	var ezw_XMLHttpRequests=new Object();
	var ezw_XMLHttpRequests_index=0;
	var ezw_XMLHttpResponses=new Object();
	//------------------------
	var ezw_XMLHttpRequest;
	var ezw_XMLHttpResponse;
	var ezw_XMLHttpResponseType;
	var ezw_XMLHttpRequestType;
	function _HTTPRequest() {
		var args=arguments;
		var moz=(window.XMLHttpRequest)?1:0;
		var sendObj=null;
		var async=true;
		ezw_XMLHttpRequest=(moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));
		//experiement
		var _ret=ezw_XMLHttpRequests_index;
		ezw_XMLHttpResponses[_ret]=new _HTTPResponse(args);
		eval("var r=(typeof(ezw_XMLHttpRequests_"+_ret+")!='undefined')?ezw_XMLHttpRequests_"+_ret+":null;");
		var url=args[0];
		var method=(args.length>1 && typeof(args[1])=="string")?args[1].toUpperCase():"POST";
		if(args.length>2 && typeof(args[2])=="function") ezw_XMLHttpResponse = args[2];
		if(args.length>3 && typeof(args[3])=="string"){
			sendObj=args[3];
			url=(url.indexOf(".php?")!=-1)?(url+args[3]):(url+"?"+args[3]);
		}
		if(args.length>4 && typeof(args[4])=="string"){ //
			ezw_XMLHttpResponseType=args[4];
		}
		if(args.length>5 && typeof(args[5])=="string"){
			ezw_XMLHttpRequestType=args[5];
			if(ezw_XMLHttpRequestType=="refresh"){
				window.document._screen.show("loading");// show message box and disable screen by covering it with semi-transparent div
			}
		}
		if(args.length>6 && typeof(args[6])=="boolean"){
			async=args[6];
		}
		//dalert(experimental+":"+url+":"+async);
		if(experimental){
			r.onreadystatechange = _HTTPResponseFunction(_ret);
			r.open(method, url, async);
			r.send(sendObj);
			return _ret;
		}
		//dalert("not experimental")
		ezw_XMLHttpRequest.onreadystatechange = _HTTPResponse;
		ezw_XMLHttpRequest.open(method, url, async);
		if(moz) ezw_XMLHttpRequest.send(sendObj);
		else ezw_XMLHttpRequest.send(sendObj);
		return ezw_XMLHttpRequest;
	}
	function _HTTPRequestGet(name) {
		try{ezw_XMLHttpRequest.getResponseHeader(name);
		}catch(e){_error_message("_HTTPRequestGet:"+e);}
	}
	function _HTTPRequestGetAll() {
		try{return ezw_XMLHttpRequest.getAllResponseHeaders();
		}catch(e){_error_message("_HTTPRequestGetAll:"+e);}
	}
	function _HTTPResponseFunction(id){
		var s="o=function(){ezw_XMLHttpResponses["+id+"].response("+id+");}";
		var o=null;
		eval(s);
		if(typeof(o)=="function") return o;
		else return null;
	}
	function _HTTPResponse(args){
		var _ret=ezw_XMLHttpRequests_index++;
		this.id="ezw_XMLHttpRequests_"+_ret;
		this.moz=(window.XMLHttpRequest)?1:0;
		this.index=_ret;
		this.func=(args.length>2 && typeof(args[2])=="function")?args[2]:null;
		this.type=(args.length>4 && typeof(args[4])=="string")?args[4]:"";
		eval(this.id+'=(this.moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));var r='+this.id+';');
		//ezw_XMLHttpRequests[_ret]=(this.moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));
		
		var res=function(id) {
			var parent=ezw_XMLHttpResponses[id];
			eval('var r=ezw_XMLHttpRequests_'+id+';');
			eval("var m=function(s){window.status=_XMLHttp_messages.readyState[ezw_XMLHttpRequests_"+id+".readyState]+((typeof(s)!='undefined'&&s!='') ? '\\n'+s:'');}");
			var p=function(id) {
			// function to process the response
			// Returns the response to the callback function with object formated in set format
				var _return='';
				var parent=ezw_XMLHttpResponses[id];
				eval('var r=ezw_XMLHttpRequests_'+id+';');
				switch(String(parent.type).toLowerCase()){
					case 'xmlobject':
						_return=r.responseXML;
					case 'xml':
						_return=r.responseXML.documentElement;
						break;
					case 'text':
						_return=r.responseText;
					default:
						_return=r.responseText;
						break;
				}
				if(typeof(parent.func)=="function"){
					parent.func(_return);
				}else{
					_error_message("No function specified:"+_return);
				}
			};
			
			this.process=p;
			this.show_message=(m!=null)?m:_show_message;
			try{switch(r.readyState){
				case 0:
					this.show_message();
					break;
				case 1:
					this.show_message();
					break;
				case 2:
					this.show_message();
					break;
				case 3:
					this.show_message();
					break;
				case 4:
					switch(r.status){
						case 200:// do callback
							this.process(id);
							break;
						case 500:// server error
							this.show_message(r.statusText);
							break;
						case 404:// url not found
							this.show_message(r.statusText);
							break;
						default:
							//this.show_message(r.statusText);
							break;
					}
					this.show_message(r.statusText);
					break;
				default:
					this.show_message(_XMLHttp_messages.errors[0]+"\n"+r.statusText);
					break;
			}}catch(e){_error_message("_HTTPResponse:"+e+"\n"+r)}
		};
		this.response=res;
		return this;
	}
	
/***************************************************************************
END _HTTPRequest Object
***************************************************************************/


/***************************************************************************
HELPERS FOR _HTTPRequest Object
***************************************************************************/
function return_xml(e){
	//var e=eval(o);
	var s="";
	if(e){
		for(i=0;i<e.length;i++)s+="<row><name>"+e[i][0]+"</name><value>"+e[i][1]+"</value></row>";
	}
	if(s!='') s="<rows>"+s+"</rows>";
	return s;
}
function return_query(e){
	var s=parse_array(e);
	if(s!='') s=s.replace(/&$/,"");
	return s;
}
function parse_array(e){
	var args=arguments;
	var s="";
	var i=0;
	if(e){
		for(i=0;i<e.length;i++)
			if(typeof(e[i][1])=="object" && e[i][1][0]){s+=parse_array(e[i][1],e[i][0]);}
			else if(args.length>1) s+=""+args[1]+"["+e[i][0]+"]="+escape(e[i][1])+"&";
			else s+=""+e[i][0]+"="+escape(e[i][1])+"&";
	}
	return s;
}
function return_params(e){
	//var e=eval(o);
	var s="";
	if(e){
		for(i=0;i<e.length;i++)s+=""+e[i][0]+"="+e[i][1]+"&";
	}
	if(s!='') s=s.replace(/&$/,"");
	return s;
}
function return_function(o){
	if(_cats_dbg==true){
		var w=window.open("","new");
		w.document.open();
		w.document.write(o);
		w.document.close();
	}
	eval('var e='+o);
	var s="";
	for(a in e){
		s+="Name: "+a+"\nValue: "+unescape(e[a])+"\n";
	}
	dalert("Array Parsed:\n"+s);
	return e;
}
function return_login(o){
	try{
		eval('var e='+o);
		if(e){
			for(a in e) e[a]=unescape(e[a]);
		}
		if(e.success=='true'){
			if(e.url && e.url!=""){
				var url=e.url;
				if(e.redirect && e.redirect!="")
					url+="&rdir="+escape(e.redirect);
				var win=show_cats_main_window(url);
				
				//var win=window.open(url,_cats_main_window);//top.location.href=url;
			}else{
				_error_message("An unspecified error occured: Please note the steps you took to get to this message and report it to catsadmin@tiwest.com.au");
			}
		}else{
			if(e.message) _error_message(e.message);
			else _error_message("Username or Password was incorrect. Please try again.");
			var f=document.forms[0];
			for(i=0;i<f.length;i++){
				f[i].disabled=false;
			}
		}
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);}
}
function return_html(o){
	//alert(o);
	//alert(top.CATS_HELPER);
	if(top.CATS_HELPER!=null)
		top.IFrameHelpers[top.CATS_HELPER].contentWindow.window.document.body.innerHTML=o;
	//top.element(top.CATS_HELPER).contentWindow.window.document.body.innerHTML=o;
//	return o;
}
function return_window(o){
	var w=window.open("","new");
		w.document.open();
		w.document.write(o);
		w.document.close();
}
//===================================================================================//
function show_message(s){
	element(_status_message).innerHTML = global_messages.readyState[ezw_XMLHttpRequest.readyState]+((s!='')?'\n'+s:'');
	//window.status=global_messages.readyState[ezw_XMLHttpRequest.readyState]+((s!='')?'\n'+s:'');
}

function _remote(){
	this.set_relation_options = function(el){
		try{
			//alert(el);
			if(this.options_to_element){
				eval('var e='+el);
				_options_replace(options_to_element,e);
			}
		}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\nRetrun Value:\n"+el);}
	}
	this.get_relation_options = function(el,to_el,func){
		options_from_element=el;
		options_to_element=to_el;
		var action_func=(typeof(func)!="undefined") ? func : 'get_columns_array';
		//var f=el.form;
		var el_value=el.options[el.selectedIndex].value;
		var sendObj=[
			['m',action_func],
			['p',el_value]
		];
		//alert(sendObj[0]+":"+sendObj[1]);
		oXML=_HTTPRequest(cats_get_path(CATS_REMOTE_PATH,"admin"),"post",this.set_relation_options,return_query(sendObj));
	}
	
	this.set_options = function(return_obj){
		try{
			dalert(return_obj);
			eval('var e='+return_obj);
			dalert(e);
			var name = e.name;
			
			if(this.options_to_element[name]){
				_options_replace(this.options_to_element[name],e.options);
			}
		}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\nRetrun Value:\n"+return_obj);}
	}

	this.set_options_add = function(return_obj){
		try{
			//alert(return_obj);
			eval('var e='+return_obj);
			//alert(e);
			var name = e.name;
			//alert(name);
			//alert("try alert this options to element["+name+"]")
			//var s="";
			//alert("this.options[name]="+remote.options[name])
			//for(e in remote.options[name]) s+="\ne="+e+":"+remote.options[name][e];
			//alert(s)
			//alert(remote.options[name].to_element)
			if(remote.options[name].to_element){
				//alert(typeof(e.options)+":"+e.options);
				var id = (e.id)?e.id:null;
				var top_options_name = remote.options[name]._params['m']+"__"+remote.options[name]._params['a'];
				if(typeof(top.gui.options[top_options_name])!="object") {
					top.gui.options.add(top_options_name);
				}
				//alert(e.options.length)
				var selected_value=top.gui.options[top_options_name].replace(name, remote.options[name].to_element,e.options,id);
				//remote.filter(name,remote.options[name]._params['p'],remote.options[name].to_element);//, remote.options[name]._params['relative']);
			}
		}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\nset_options_add.Retrun Value:\n"+return_obj);}
	}
	
	this.get_options_add1 = function(from_el,to_el,func){
		var el=(typeof(from_el)=="string")?{'name':'_no_name_','value':from_el,'type':'text'}:from_el;
		var name=el.name;
		var el_to = (typeof(to_el)!="object")?el:((to_el!=null)?to_el:el);
		
		var action_func=(typeof(func)!="undefined") ? func : '';
		var module=(typeof(arguments[3])!="undefined") ? arguments[3] : 'admin';
		var id=(typeof(arguments[4])!="undefined") ? arguments[4] : '';
		var el_value=(typeof(arguments[5])!="undefined") ? arguments[5] : ((el.type.match(/(text|textarea|hidden)/gi))?el.value:el.options[el.selectedIndex].value) ; 
		this.get_options(name,el,el_to,action_func,module,id,el_value);
	}
	this.get_options_add = function(func,module,id){
		
		var el=(typeof(arguments[3])=="string")?{'name':'_no_name_','value':arguments[3],'type':'text'}:arguments[3];
		var el_to=(typeof(arguments[4])=="string")?{'name':'_no_name_','value':arguments[4],'type':'text'}:arguments[4];
		var name=el.name;
		var action_func=(typeof(func)!="undefined") ? func : '';
		var el_value=(typeof(arguments[5])!="undefined") ? arguments[5] : ((el.type.match(/(text|textarea|hidden)/gi))?el.value:el.options[el.selectedIndex].value) ; 
		this.get_options(name,el,el_to,action_func,module,id,el_value);
	}
	
	this.get_options_replace = function(from_el,to_el,func){
		var el=(typeof(from_el)=="string")?{'name':'_no_name_','value':from_el,'type':'text'}:from_el;
		var name=el.name;
		var el_to = (typeof(to_el)!="object")?el:((to_el!=null)?to_el:el);
		
		var action_func=(typeof(func)!="undefined") ? func : '';
		var module=(typeof(arguments[3])!="undefined") ? arguments[3] : 'admin';
		var id=(typeof(arguments[4])!="undefined") ? arguments[4] : '';
		var el_value=(typeof(arguments[5])!="undefined") ? arguments[5] : ((el.type.match(/(text|textarea|hidden)/gi))?el.value:el.options[el.selectedIndex].value) ; 
		this.get_options(name,el,el_to,action_func,module,id,el_value);
	}
	this.get_options = function(name_obj,el,el_to,action_func,module,id,el_value){
		//caller_name = call.replace(/\s/g,"").replace(/function([a-zA-Z0-9]+)\((.*)/,"function $1()");
		dalert(this.get_options.caller);
		if(typeof(name_obj)=="object"){
			name = name_obj.name;
		}else{
			name = name_obj;
		}
		//name = module+"__"+action_func;
		// arguments:
		// 	el:element holding value
		// 	el_to: element where options are to be added
		// 	action_func: action of the module
		// 	module: module where action exists
		// 	id: do id lookup if this argument exists
		// 	el_value: set this to override value
		
		// create options object if it doesn't already exist
		if(typeof(remote.options[name])!="object") remote.options[name] = new Object();
		
		remote.options[name].from_element=el;
		remote.options[name].to_element=el_to;
		remote.options[name].selected_id=id;
		/*var sendObj=[
			['a',action_func],
			['m',m],
			['n',name],
			['p',el_value],
			['id',id]
		];*/
		var obj={
			'a':action_func,
			'm':module,
			'n':name,
			'p':el_value,
			'id':id
		};
		var url = cats_get_path(CATS_REMOTE_PATH,module);
		//alert(obj);
		var q = this._query(obj);
		
		//alert(url+"?"+q)
		//obj.relative=el;
		remote.options[name]._query = q;
		remote.options[name]._params = obj;
		remote.options[name]._url = url;
		remote.options[name]._xml = _HTTPRequest(url,"post",this.set_options_add,q);//,null,null,true);
		
	}
	
	this.get_html = function(name_obj,el,el_to,action_func,module,id,el_value){
		//caller_name = call.replace(/\s/g,"").replace(/function([a-zA-Z0-9]+)\((.*)/,"function $1()");
		dalert(this.get_options.caller);
		if(typeof(name_obj)=="object"){
			name = name_obj.name;
		}else{
			name = name_obj;
		}
		//name = module+"__"+action_func;
		// arguments:
		// 	el:element holding value
		// 	el_to: element where options are to be added
		// 	action_func: action of the module
		// 	module: module where action exists
		// 	id: do id lookup if this argument exists
		// 	el_value: set this to override value
		
		// create options object if it doesn't already exist
		if(typeof(remote.options[name])!="object") remote.options[name] = new Object();
		
		remote.options[name].from_element=el;
		remote.options[name].to_element=el_to;
		remote.options[name].selected_id=id;
		var obj={
			'a':name,
			'm':module,
			'n':name,
			'p':el_value,
			'id':id
		};
		//alert("obj from get_html="+obj);
		var url = cats_get_path(CATS_REMOTE_PATH,module);
		//alert(obj);
		var q = this._query(obj);
		//alert(url+"\n"+q);
		remote.options[name]._query = q;
		remote.options[name]._params = obj;
		remote.options[name]._url = url;
		remote.options[name]._xml = _HTTPRequest(url,"post",action_func,q);//,null,null,true);
		
	}
	
	this._query = function (o){
		var s=this.parse_object(o);
		if(s!='') s=s.replace(/&$/,"");
		return s;
	}
	this.parse_array = function(a){
		var args=arguments;
		var s="";
		var i=0;
		if(typeof(a)=="object"){
			for(i=0;i<a.length;i++)
				if(typeof(a[i][1])=="object" && a[i][1][0]){s+=this.parse_array(a[i][1],a[i][0]);}
				else if(args.length>1) s+=""+args[1]+"["+a[i][0]+"]="+escape(a[i][1])+"&";
				else s+=""+a[i][0]+"="+escape(a[i][1])+"&";
		}
		return s;
	}
	this.parse_object = function(o){
		var args=arguments;
		var s="";
		var i=0;
		if(typeof(o)=="object"){
			//for(i=0;i<a.length;i++)
			for(i in o){
				//alert(i+":"+o[i]);
				if(typeof(o[i])=="object"){s+=this.parse_object(o[i],i);}
				else if(args.length>1) s+=""+args[1]+"["+i+"]="+escape(o[i])+"&";
				else s+=""+i+"="+escape(o[i])+"&";
			}
		}
		return s.replace(/&$/,"");
	}
	
	this.filter = function (name, filter, list){
		if(filter=='') return;
		if(!this.filter_options(filter, list)){
			//function(el,el_to,action_func,module,id,el_value)
			//remote.get_options({'name':name,'type':'text','value':''},el,'get_employee_array','users',id);
			dalert("Inside filter:name="+name+":filter="+filter);
			this.get_options_add(name, list, remote.options[name]._params['a'], remote.options[name]._params['m']);
		}
	}
	this.filter_options = function (filter, list){
		//var s = (typeof(filter)=="object")?filter.value:filter;
		var s = (typeof(filter)=="string")?filter:((filter.type) && (el.type.match(/(text|textarea|hidden)/gi))?filter.value:filter.options[filter.selectedIndex].value);
		var _ret=false;
		if(s != ""){
			if(typeof(list)=="string") list=element(list);
			var x = list.options.length;
			s = s.toLowerCase();
			var ilen = s.length;
			var sdat = s.substring(0,ilen);
			var val,dat;
			for(i=0;i<x; i++){
				val = list.options[i].text;
				dat = val.substring(0,ilen);
				dat = dat.toLowerCase();
				if(s == dat){
					list.options[i].selected = true; 
					_ret=true;
					break;
				}
			}
			if(_ret) list.focus();
		}
		return _ret;
	}
}
var remote = new _remote();
remote.options = new Object();
/*
remote.options_from_element=new Object();
remote.options_to_element=new Object();//null;
remote.options_to_selected_id=new Object();//null;
remote.options_params=new Object();
remote.options_XML=new Object();
*/
function _cats_get_path(path,page){
	if(path)
		return path+page+".php";
	else
		return CATS_REMOTE_PATH+page+".php";
}
if(typeof(cats_get_path)!="function"){
	cats_get_path = _cats_get_path;
}

// dbg function
var gDEBUG_ON=false;
function dalert(s){
	if(gDEBUG_ON && gDEBUG_ON==true) alert(s);
}

function _error_message(s){
	alert("Error Message: "+s);
	// TODO: Create nice window for error messages
}









/*
function _options_clear(el){
	var i=0;
	var len=el.options.length;
	for(i=len;i>0;i--){
		el.options[i]=null;
	}
}
function _options_add(el,arr){
	var value=0;
	var text=1;
	var i=0;
	var len=arr.length;
	for(i=0;i<len;i++){
		var olen=el.options.length;
		var add = true;
		if(olen==0) add = true;
		//alert(el.name);
		JLOOP:for(j=0;j<olen;j++){
			dalert(typeof(el.options[j].value)+":"+typeof(arr[i][value])+"\noVal="+el.options[j].value+"\naVal="+arr[i][value]+":"+arr[i][text]);
			if(String(el.options[j].value)==String(arr[i][value])){
				add=false;
				break JLOOP;
			}
		}
		dalert(add);
		if(add)
			el.options[el.options.length]=new Option(arr[i][text],arr[i][value]);
	}
	
	if(typeof(arguments[2])!="undefined" && arguments[2]!=null){
		var id=arguments[2];
		alert(id)
		if(typeof(id)=="object")
			id=id.value;
		_options_select_by_id(el,id);
	}
}
function _add_options(name,el,arr,selected_id){
	//alert(name);
	var value=0;
	var text=1;
	var i=0;
	var len=arr.length;
	alert(name);
	if(typeof(top.gui.options[name]._array)!="object") top.gui.options[name]._array = new Array();
	var atop=top.gui.options[name]._array;
	for(i=0;i<len;i++){
		var olen=atop.length;//el.options.length;
		var add = true;
		if(olen==0) add = true;
		//alert(el.name);
		JLOOP:for(j=0;j<olen;j++){
			//dalert(typeof(el.options[j].value)+":"+typeof(arr[i][value])+"\noVal="+el.options[j].value+"\naVal="+arr[i][value]+":"+arr[i][text]);
			if(String(atop[j][value])==String(arr[i][value])){
			//if(String(el.options[j].value)==String(arr[i][value])){
				add=false;
				break JLOOP;
			}
		}
		dalert(add);
		if(add)
			el.options[el.options.length]=new Option(arr[i][text],arr[i][value]);
	}
	
	if(typeof(arguments[2])!="undefined" && arguments[2]!=null){
		var id=arguments[2];
		alert(id)
		if(typeof(id)=="object")
			id=id.value;
		_options_select_by_id(el,id);
	}
}
function _options_select_by_id(el,id){
	var i=0;
	var opts=el.options;
	var len=opts.length;
	for(i=0;i<len;i++){
		if(String(id)==String(opts[i].value)){
			opts[i].selected=true;
			break;
		}
	}
}
function _options_replace(el,arr){
	_options_clear(el);
	_options_add(el,arr);
}*/
/*
var options_from_element=new Object();//null;
var options_to_element=new Object();//null;
var options_to_selected_id=new Object();//null;
var options_XML=new Object();
*/