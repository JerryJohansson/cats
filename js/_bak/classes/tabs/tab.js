//--------------------------------------------------------------------------------------------------------------------------
//	TAB OBJECT
//--------------------------------------------------------------------------------------------------------------------------
top._CURRENT_TAB_NAME="";
top._CURRENT_PAGE_NAME="";
top._tab = function(){
	this.toolbar=new top._toolbar();
	this.screen=new top._screen();
	this.type=0;
	
	this.add = function(name){
		if(typeof(this[name])=="object"&&typeof(this[name].initialised)=="boolean"){
			// Do nothing we already exist
		}else{
			this[name] = {'initialised':false};
			this[name].data = this.fields(name,"search");
			this[name].toolbar = new top._toolbar(name);
			this[name].toolbar.parent = this;
			this[name].screen = new top._screen(name);
			this[name].screen.parent = this;
			if(arguments.length>1){
				this.show(name);
			}
		}
		return this[name];
	}
	this.initialise = function(name){
			this[name].initialised = true;
	}
	this.get = function(what){
		if(what=="toolbar")
			return this[_CURRENT_TAB_NAME].toolbar;
		else if(what=="screen")
			return this[_CURRENT_TAB_NAME].screen;
	}
	this.show = function(id){
			alert(this[id].data.search)
		var m = element(_menu);
		if(m.innerHTML!="") tabs[_menu_id].html = m.innerHTML;
		//try{alert(tabs[_menu_id].html);}catch(e){}
		//alert(id)
		var c = element(_cont);
		var t = element(_title);
		var exit = false;
		var i=0;
		var menu = "";
		alert(c.innerHTML);
		_menu_id = id;
		if(arguments[2]){
			menu=element(_menu_prefix+id).innerHTML;
			tabs[id]={};
		}else if(tabs[id]&&typeof(tabs[id]['html'])=="string"){
			menu=tabs[id].html;
		}else{
			menu=element(_menu_prefix+id).innerHTML;
			tabs[id]={};
		}
		tabs[id]['html'] = menu;
		
		m.setAttribute("name",id);
		m.innerHTML = menu;
		
		
		c.style.display = 'block';
		if(arguments[1])
			t.innerHTML = arguments[1];
		
		//alert(c.firstChild.firstChild.offsetWidth)
		f=element("menu_frame");
		f.style.backgroundColor = "white";
		f.style.width=c.firstChild.offsetWidth;
		f.style.height=c.offsetHeight;
		f.style.top=c.offsetTop;
		f.style.left=c.firstChild.offsetLeft;
		//alert(f.style.top+":"+f.style.left+":"+f.style.width+":"+f.style.height+":")
		f.style.display = 'block';
	}
	this.click = function(){
	//alert('hey hey')
		//try{
			var m = element(_menu);
			var c = element(_cont);
			var f = element("menu_frame");
			f.style.display = 'none';
			c.style.display = 'none';
			tabs[m.getAttribute('name')].html=m.innerHTML;
			m.innerHTML = "";
			if(arguments[0]){
				name=arguments[0].replace(/\.php/,"").replace(/\s/gi,"_");
				//alert(typeof(tabs[name]))
				if(typeof(tabs[name])=="object"){// && tabs.toolbar[name]){
					var s ="";
					for(e in tabs[name]){s+=e+"="+tabs[name][e]+'\n'}
					alert(s);
					
				}else{
					//element(_iframe).contentWindow.window.get_fields(name);
					var a=get_fields(name);
					var o=null;
					var obj=(typeof(a['array'])=="object")?a['array']:a['search'].split("|");
					var interval=a['int'];
					var type=a['type'];
					//alert(obj+":"+interval+":"+type)
					if(typeof(top.tabs[name])!="object") top.tabs[name] = a;
					top.tabs[name]._html=new _html( obj, interval, "", type );
					var s=top.tabs[name]._html.gen();
					try{
						o=parent.element(_menu_prefix+name);
						menu=o.innerHTML;
					}catch(e){
						var tmp=top.document.createElement("DIV");
						tmp.setAttribute("id",_menu_prefix+name);
						o=top.document.body.appendChild(tmp);
						tmp=null;
						tmp=top.document.createElement("A");
						tmp.className = "def";
						tmp.setAttribute("href","javascript:showMenu('"+name+"','"+obj_string+"');");
						tmp=top.element("menu_history_list").appendChild(tmp);
						tmp.innerHTML = obj_string;
					}
					o.innerHTML = s;
					o.style.display = 'none';
					top.showMenu(obj_string_name);
					element("page_title").innerHTML = title;
				}
			}
			/*
			if(arguments[0])
				element(_iframe).contentWindow.window.location.href = arguments[0];*/
			if(arguments[1])
				element(_title).innerHTML = arguments[1];
		//}catch(e){}
	}
	
	this.fields = function(tab_name, page_name){
		var url = top._get_class_path("forms.php");
		var function_name = "get_fields";
		var data=null;
		if(this[tab_name]&&(typeof(page_name)=="string"&&typeof(this[tab_name][page_name])=="string")){
			data=this[tab_name][page_name];
		}else if(typeof(this[tab_name].initialised)=="boolean"){
			data=this[tab_name];
		}else{
			try{
				// [url get fileds],[function_name],[name of tab],[button name or page name]
				var return_value = RSExecute(url, function_name, tab_name, page_name); 
			}catch(e){
				var return_value = {'return_value':"{'"+page_name+"':'"+this[tab_name][page_name]+"','int':"+this[tab_name]['int']+"}"};
			}
			eval("data="+return_value.return_value+";");
			
			//this[tab_name]=data;
			//*
			var s = "";
			for(e in return_value) if(typeof return_value[e] != "function") s+=e+":"+return_value[e]+"\n";
			alert("get_fields()\n\n"+page_name+"\n\n"+data+"\n\n"+return_value.return_value);
			//alert(data.string)
			//*/
		}
		return data;
	}
}

top._toolbar = function(){
/*
	This class holds all buttons added to the toolbar.
	It holds the html string
*/
	this.buttons = {};
	this.button = {};
	this.parent = (arguments.length>0)?arguments[0]:_tab_toolbar;
	this.html=(arguments.length>1)?arguments[1]:"";
	this.add = function(title){
		var name = (arguments.length>1)?arguments[1]:title.toLowerCase().replace(/\s/gi, "_");
		if(typeof(this.buttons[name])=="undefined") this.append(name,title);
	}
	this.append = function(name,title){
		var tmp=top.document.createElement("A");
		tmp.title=title;
		tmp.setAttribute("href","javascript:_"+name+"();");
		tmp.style.cssText = "background-image: url(icons/btn_"+name+"_bg.gif);float:left;";
		this.button[name]=top.element(this.parent).appendChild(tmp);
		this.button[name].innerHTML=title;
		this.buttons[name]=this.button[name].outerHTML;
		this.html+=this.buttons[name];
	}
	this.getHTML = function(){
		var s = "";
		for(e in this.buttons){
			s += this.buttons[e];
		}
		return s;
	}
}
//var toolbar = new _toolbar();
// [tab].[screen].[name]
top._SCREEN_INCREMENT = 0;
top._screen = function(){
	var args=arguments;
	this.type=(args.length>0)?args[0]:"frame";//frame, window, dialog, panel
	this.name=(args.length>1)?args[1]:this.type+"_screen_"+(top._SCREEN_INCREMENT++);
	this.html=(args.length>2)?args[2]:"<p>No html was passed in arg[1]</p>";
	this.id=this.name.replace(/\s/gi,"_");
	top.tabs_current_id = this.id;
	this.pages = {};
	//alert(this.parent)
	this.set = function(val){
		this.html=val;
	}
	this.get = function(){
		return this.html;
	}
	this.add = function(title){
		var name = (arguments.length>1)?arguments[1]:title.toLowerCase().replace(/\s/gi, "_");
		if(typeof(this.pages[name])=="undefined") this.append(name,title);
	}
	this.append = function(name, title){
		var args=arguments;
		this.pages[name]={};
		this.pages[name]['html']=(args.length>2)?args[2]:"";
		if((this.pages[name]['screen']=top.element(this.id))==null){
			switch(this.type){//frame, window, dialog, panel
				case 'frame': case 'window':
					var o=top.document.createElement("IFRAME");
					o.className="iframe_menu";
					o.id=this.id;
					o.contentWindow.window.location.href="edit.php?cb=top.tabs[top._CURRENT_TAB_NAME]._render();";
					break;
				case 'dialog': case 'panel':
					var o=top.document.createElement("DIV");
					o.className="iframe_menu";
					o.id=this.id;
					var screen_name = ""+this.type+"_"+name;
					var c = element(_cont);
					o.innerHTML = c.innerHTML.replace(/[ id="menu| id="screen]/gi,' id="'+screen_name);
					break;
			}
			this.pages[name]['screen']=top.document.body.appendChild(o);
		}
		if(arguments[1])
			element(screen_name+"_title").innerHTML = arguments[1];
		this.pages[name]['screen'].setAttribute("name",this.id);
		// top.tabs.actions.toolbar.edit.page.show();
		// top.tabs['actions'].toolbar['edit'].page.show();
		// top.tabs['actions'].screen.show('edit');
		this.hide(name);
	}
	this.show = function(name){
		this.pages[name]['screen'].style.display = 'block';
	}
	this.hide = function(name){
		this.pages[name]['screen'].style.display = 'none';
	}
}
top.tabs=new top._tab();//Array();
		//tabs['config']=new Object();
		//alert(top.tabs);
