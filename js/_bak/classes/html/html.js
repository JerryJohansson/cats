/*
	CREATES AN HTML OBJECT
		has built in functions to perform the generation of html
*/
function _html(){
/*
	ARGS:
		fields[array]=array of items - Name|{type[s]}|{length[n]}|{value[s]}|{columns[n]}|{mandatory[n]}
		interval[n]=Default is 6 - Interval for loops e.g. i+=this.interval
		html[string]=html starter string is optional
*/
	var args=_html.arguments;
	this.fields = (args.length>0)?args[0]:IDX;
	this.interval = (args.length>1)?args[1]:6; // default to 6 hard code ala jta style
	this.action = "search.php";
	this.target = "main_frame";
	this.type = (args.length>3)?args[3]:0; // Default type = form - 0=form, 1=Recordset
	//alert("<br>this.type="+this.type+args[0]+args[1]+args[2])
	if(this.fields=="")return false;
	this.html = (args.length>2)?args[2]:"";
	this.init = function(){
		this.html = this.getHeader("", "", this.action, this.target);
	}
	this.getHeader = function(){
		var s=caption=func=action=target=form="";
		var args=arguments;
		if(args.length>1) func=args[1];
		if(args.length>0 && args[0]!="") caption="<caption id=\"tog\"><a name=\"todo\" onclick=\""+(func!=''?func:"this.blur()")+";\">"+args[0].replace(/_/gi," ")+"</a></caption>";
		if(args.length>2) action="<form action=\""+args[2]+"\" ";
		if(args.length>3) target= " target=\""+args[3]+"\" ";
		script=(args.length>4)?" onsubmit=\"return "+args[4]+"\" ":" onsubmit=\"return form_search_submit(this);\" ";
		if(action!="") form = action + target + script + ">";
		form += "<fieldset class=\"tbar\">" +
			"<table class=\"admin\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\" style=\"background:buttonface;\">" +
			caption;
		return form;
	}
	this.getLabel = function(name,label){
		var args=arguments;
		var start = "	<td class=\"label\">";
		var middle = "<label for=\"id_"+ name +"\">"+ label.replace(/_/gi," ") +":</label>";
		var end = "</td>";
		if(args.length>2)
			if(!args[2]) start=end="";
		if(args.length>3){
			switch(args[3]){
				case 'g':
					start=middle=end="";
					break;
				default:
					
					break;
			}
		}
		return start+middle+end;
	}
	this.getField = function(name, value, type, length){
		var args=arguments;
		var s=(args.length>5)?(args[5]?"<td>":""):"<td>";
		var helper = "";
		var rows="3";
		var cols="2";
		var input_type = (type=='c'||type=='r')?((type=='c')?"checkbox":"radio"):"text";
		switch(type){
			case 'c': case 'r': 
				s+="<input class=\"sfield\" id=\"id_"+ name +"\" name=\""+ name +"\" value=\""+ value +"\" type=\""+input_type+"\" />";
				helper = "";//"<img src=\"icons/btn_date_bg.gif\" id=\"helper\" onclick=\"calendar_onclick();\" />";
				break;
			case 'd':
				s+="<input class=\"dfield\" id=\"id_"+ name +"\" name=\""+ name +"\" value=\""+ value +"\" />";
				helper = "<img src=\"icons/btn_date_bg.gif\" id=\"helper\" onclick=\"calendar_onclick();\" />";
				break;
			case 'n':
				s+="<input class=\"nfield\" id=\"id_"+ name +"\" name=\""+ name +"\" value=\""+ value +"\" onclick=\"popup_onclick('"+name+"');\" onkeypress=\"check({'type':'n','length':'"+length+"'});\" />";
				helper = "<img src=\"icons/btn_show_bg.gif\" id=\"helper\" onclick=\"popup_onclick('"+name+"');\" />";
				break;
			case 'g':
				this.html = this.html.substr(0,this.html.length-100);
				//alert(this.html)
				s="";
				s+="<td rowspan=\""+rows+"\" colspan=\""+cols+"\"><fieldset id=\"tbar\">";//<legend>"+ name +"</legend>";
				s+=this.getGroup(name, value);
				s+="</fieldset>";
				this.html+=s;
				//alert(this.html)
				s=""; 
				break;
			default:
				s+="<input class=\"sfield\" id=\"id_"+ name +"\" name=\""+ name +"\" value=\""+ value +"\" onclick=\"popup_onclick('"+name+"');\" onkeypress=\"check({'type':'n','length':'"+length+"'});\" />";
				helper = "<img src=\"icons/btn_show_bg.gif\" id=\"helper\" onclick=\"popup_onclick('"+name+"');\" />";
				break;
		}
		helper=args.length>4?(args[4]?helper:""):helper;
		return s+=helper+((args.length>5)?(args[5]?"</td>":"<br />"):"</td>");
	}
	this.getGroup = function(name, values){
		var s="<legend>"+name+"</legend>";
		if(typeof(values)=="string") values=values.split(",");
		for(i=0;i<values.length;i+=this.interval){
			value=(this.interval>IDX_VALUE)?values[i+IDX_VALUE]:"";
			s += this.getLabel(values[i],values[i+IDX_NAME],false,values[i+IDX_TYPE])+this.getField(values[i], value, values[i+IDX_TYPE], values[i+IDX_LENGTH],false,false);
		}
		return s;
	}
	this.build = function(){
		var tmp=s="";
		var i=j=x=0;
		var exit=false;
		for(i=0;i<this.fields.length;i+=this.interval){
			this.html += "\n<tr onclick=\"_row_down(this);\" onmouseover=\"_row_over(this);\" onmouseout=\"_row_out(this);\" >\n";
			tmp="show";
			show=(this.fields[i+IDX_TYPE]=='g')?false:true;
			try{
				
				tmp=this.fields[i];
				s="";
				value = (this.interval>IDX_VALUE)?this.fields[i+IDX_VALUE]:"";
				if(this.type==1)
					s += this.getRow(i);
				else
					s += this.getLabel(tmp,this.fields[i],show,this.fields[i+IDX_TYPE])+this.getField(tmp, value, this.fields[i+IDX_TYPE], this.fields[i+IDX_LENGTH]);
				x=(this.fields[i+IDX_COLUMNS])?this.fields[i+IDX_COLUMNS]:1;

				// Write extra columns/rows
				if(x>1){
					xlen=(x-1)*this.interval;
					for(j=1;j<xlen;j+=this.interval){
						i+=this.interval;
						value = (this.interval>IDX_VALUE)?this.fields[i+IDX_VALUE]:"";
						if(this.type==1)
							s += this.getRow(i);
						else
							s += this.getLabel(this.fields[i],this.fields[i],show,this.fields[i+IDX_TYPE])+this.getField(this.fields[i], value, this.fields[i+IDX_TYPE], this.fields[i+IDX_LENGTH]);
					}
					i--;
				}
				this.html+=s;
				//alert(this.html);
			}catch(e){}
			if(x>1) i=i+(x-1);
			this.html += "\n</tr>\n";
			if(i>=this.fields.length) {
				i=this.fields.length-1;
				exit=true;
			}
			if(exit==true) break;
		}
		//alert(this.html);
	}
	this.close = function(){
		this.html += "</table>" +
					"</fieldset>";
					//alert(typeof(this.type))
		if(this.type==0){
			this.html += "<fieldset class=\"tbar\">" +
					"<div align=\"right\">" +
					"	<input type=\"submit\" name=\"Submit\" value=\"Search\"><input type=\"button\" name=\"New\" value=\"New...\" onClick=\"_new();\"><input type=\"button\" name=\"Cancel\" value=\"Cancel\" onClick=\"_cancel();\">" +
					"</div>" +
					"</fieldset>" +
					"</form>";
		}
	}
	this.out = function(){
		return this.html;
	}
	this.set = function(html){
		this.html = html;
	}
	this.get = function(){
			return this.html;
	}
	this.getRow =	function(iRow){
		var s="";
		for(i=iRow;i<(this.interval+iRow);i++){
				s+="<td>"+this.fields[i]+"</td>";
		}
		return s;
	}
	this.gen = function(){
		if(arguments[0])this.interval = arguments[0];
		this.init();
		this.build();
		this.close();
		return this.html;
	}
}
/*
_html.prototype.getRow = function(iRow){
	var s="";
	for(i=iRow;i<(this.interval+iRow);i++){
			s+="<td>"+this.fields[i]+"</td>";
	}
	return s;
}
*/