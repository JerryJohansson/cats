// RC stuff
var buttonElement = null;
var _base_editor_frame = "frmMain";
//var _base_url = "<!-- base_url -->";
function getTree(){
	return document.getElementById("MENU_1_CONTAINER_0");
}
function incontext(el,target){
	while(((el=el.parentNode)!=null) && el.nodeName != "BODY" ){
		if(arguments.length>2&&el.nodeName==arguments[2]) return el;
		if(el.id==target) return el;
	}
	return false;
}
function popupMenu(event) {
	var doc=getTree();
	buttonElement =document.all?event.srcElement:event.target;
	if(!incontext(buttonElement,"MENU_1_CONTAINER_0")) return;
	return ContextMenu.popupMenu(event);
	//return false;
}
function alertMe(s){alert(s);return s;}

/*
+-------------------------------------------
|		Context menu constructor and prototyping
+-------------------------------------------
*/
function ContextMenu(tree) {
	this.tree = tree;
};
// Menus clipboard stores copied objects
ContextMenu.prototype.clipboard = {
	"value":{},
	"add":function (val){this.value=val;},
	"isempty":function (){return !(typeof(this.value["Node"])=="object");}
};

ContextMenu._pluginInfo = {
	name          : "ContextMenu",
	version       : "1.0",
	developer     : "Vernon Laskey",
	developer_url : "http://www.ezwebmaker.com/",
	c_owner       : "ezwebmaker.com",
	sponsor       : "Buzmedia",
	sponsor_url   : "http://www.buzmedia.com.au",
	license       : "ContextMenu"
};

ContextMenu.prototype.getContextMenu = function(target) {
	var self = this;
	var tree = this.tree;
	var menu = [];

	var currentTarget = target;
	
	//alert(target.parentNode.parentNode.parentNode.innerHTML)
	target = target.parentNode.parentNode;
	var type = target.getAttribute("type");
	function canCutCopy(tgt){
		return true;
	}
	function canPaste(tgt){
		return !ContextMenu.clipboard.isempty();
	}
	function canDelete(id){
		return !(id > 1);
	}
	function canMove(dir){
		if(dir=="up"){
			return !(target.previousSibling&&target.previousSibling.nodeName=="DIV");
		}else if(dir=="down"){
			return !(target.nextSibling.nextSibling&&target.nextSibling.nextSibling.nodeName=="DIV");
		}
	}
	menu.push(
		  [ "Edit Item...",
		    function() {
			    open(_base_url+"editor.php?action=edit&id="+target.id,_base_editor_frame);
		    },
		    "Edit currently selected item",
				_base_url+"images/m-edit.gif" ]);
	menu.push(null,
		  [ "Add Item...",
		    function() {
			    open(_base_url+"editor.php?action=add&pid="+target.id,_base_editor_frame);
		    },
		    "Add a new item to currently selected item",
				_base_url+"images/m-add.gif" ]);
	if(CHILD_CREATION_DISABLED_LIST.indexOf(type)==-1){
	menu.push(null,
			[ "Cut", function() { ContextMenu.clipboard.add({"Node":{"id":target.id,"action":"cut"}}); }, "Cut this item and place into the clipboard", _base_url+"images/ed_cut.gif", !canCutCopy(target) ],
			[ "Copy", function() { ContextMenu.clipboard.add({"Node":{"id":target.id,"action":"copy"}}); }, "Copy this item to the clipboard", _base_url+"images/ed_copy.gif", !canCutCopy(target) ],
			[ "Paste", function() { open("explore.php?action=paste&mode="+window.ContextMenu.clipboard.value["Node"].action+"&id_list="+window.ContextMenu.clipboard.value["Node"].id+"&id="+target.id,"_self"); }, "Paste items from the clipboard into this item", _base_url+"images/ed_paste.gif", !canPaste(target) ]);
	}
	if(DELETE_DISABLED_LIST.indexOf(type)==-1){
	menu.push(null,
			[ "Delete",
		    function() {
					if(confirm("Are you sure you want to delete this page?\nWarning: Any children of this page will no longer be accessible!"))
						open(_base_url+"editor.php?action=del&id="+target.id,_base_editor_frame);
		    },
		    "Delete selected item",
				_base_url+"images/ed_delete.gif", canDelete(target.id) ]);
	}
	menu.push(null,
			[ "Move to Top",
		    function() {
					window.move_item("top",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item to the top of the branch",
				_base_url+"images/m-movetop.gif", canMove("up") ],
		  [ "Move Item Up",
		    function() {
					window.move_item("up",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item up one space",
				_base_url+"images/m-moveup.gif", canMove("up") ],
			[ "Move Item Down",
		    function() {
			    window.move_item("down",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item down one space",
				_base_url+"images/m-movedown.gif", canMove("down") ],
			[ "Move to Bottom",
		    function() {
					window.move_item("bot",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item to the bottom of the branch",
				_base_url+"images/m-movebot.gif", canMove("down") ]
	);
	menu.push(null,
		  [ "Refresh Node",
		    function() {
					window.action(_base_url+"explore_actions.php?pid="+target.id);
		    },
		    "Refresh children items" ]);
	menu.push(null,
		  [ "Properties...",
		    function() {
			    alert("base_url="+_base_url+"\nid="+target.id+"\nbase_frame="+_base_editor_frame+"\nnode="+target.outerHTML+"\n"+target.nextSibling.outerHTML);
		    },
		    "Display currently selected items extended attributes" ]);
	
	return menu;
};

ContextMenu.prototype.popupMenu = function(ev) {
	var self = this;
	if (this.currentMenu)
		this.currentMenu.parentNode.removeChild(this.currentMenu);
	function getPos(el) {
		var r = { x: el.offsetLeft, y: el.offsetTop };
		if (el.offsetParent) {
			var tmp = getPos(el.offsetParent);
			r.x += tmp.x;
			r.y += tmp.y;
		}
		return r;
	};
	function documentClick(ev) {
		ev || (ev = window.event);
		if (!self.currentMenu) {
			alert("How did you get here? (Please report!)");
			return false;
		}
		var el = ContextMenu.is_ie ? ev.srcElement : ev.target;
		for (; el != null && el != self.currentMenu; el = el.parentNode);
		if (el == null)
			self.closeMenu();
	};
	self.closeMenu = function() {
		self.currentMenu.parentNode.removeChild(self.currentMenu);
		self.currentMenu = null;
		if (ContextMenu.is_ie)
			self.iePopup.hide();
	};
	
	var target = ContextMenu.is_ie ? ev.srcElement : ev.target;
	var ifpos = {x:0,y:0};//getPos(document);
	var x = ev.clientX + ifpos.x;
	var y = ev.clientY + ifpos.y;
	
	if(target.nodeName=="IMG") return true;
	
	var div;
	var doc;
	if (!ContextMenu.is_ie) {
		doc = document.getElementById("ezecmenu").contentWindow.document;
	} else {
		// IE stinks
		var popup = this.iePopup = window.createPopup();
		doc = popup.document;
		doc.open();
		doc.write("<html><head><style type='text/css'>@import url(" + _base_url + "plugins/ContextMenu/menu.css); html, body { padding: 0px; margin: 0px; overflow: hidden; border: 0px; }</style></head><body unselectable='yes'></body></html>");
		doc.close();
	}
	div = doc.createElement("div");
	cont = doc.createElement("div");
	if (ContextMenu.is_ie)
		div.unselectable = "on";
	div.oncontextmenu = function() { return false; };
	div.className = "htmlarea-context-menu";
	if (!ContextMenu.is_ie)
		div.style.left = div.style.top = "0px";

	cont.appendChild(div);
	doc.body.innerHTML = cont.innerHTML;
	div = doc.body.firstChild;
	
	var table = doc.createElement("table");
	div.appendChild(table);
	table.cellSpacing = 0;
	table.cellPadding = 0;
	var parent = doc.createElement("tbody");
	table.appendChild(parent);

	var options = this.getContextMenu(target);
	for (var i = 0; i < options.length; ++i) {
		var option = options[i];
		var item = doc.createElement("tr");
		parent.appendChild(item);
		if (ContextMenu.is_ie)
			item.unselectable = "on";
		else item.onmousedown = function(ev) {
			ContextMenu._stopEvent(ev);
			return false;
		};
		if (!option) {
			item.className = "separator";
			var td = doc.createElement("td");
			td.className = "icon";
			var IE_IS_A_FUCKING_SHIT = '>';
			if (ContextMenu.is_ie) {
				td.unselectable = "on";
				IE_IS_A_FUCKING_SHIT = " unselectable='on' style='height=1px'>&nbsp;";
			}
			td.innerHTML = "<div" + IE_IS_A_FUCKING_SHIT + "</div>";
			var td1 = td.cloneNode(true);
			td1.className = "label";
			item.appendChild(td);
			item.appendChild(td1);
		} else {
			var label = option[0];
			item.className = "item";
			item.__msh = {
				item: item,
				label: label,
				action: option[1],
				tooltip: option[2] || null,
				icon: option[3] || null,
				disabled: option[4] || false,
				activate: function() {
					self.closeMenu();
					this.action();
				}
			};
			item.disabled = (option[4])?option[4]:false;
			label = label.replace(/_([a-zA-Z0-9])/, "<u>$1</u>");
			if (label != option[0])
				keys.push([ RegExp.$1, item ]);
			label = label.replace(/__/, "_");
			var td1 = doc.createElement("td");
			if (ContextMenu.is_ie)
				td1.unselectable = "on";
			item.appendChild(td1);
			td1.className = "icon";
			if (item.__msh.icon)
				td1.innerHTML = "<img align='middle' src='" + item.__msh.icon + "' />";
			var td2 = doc.createElement("td");
			if (ContextMenu.is_ie)
				td2.unselectable = "on";
			item.appendChild(td2);
			td2.className = "label";
			td2.innerHTML = label;
			item.onmouseover = function() {
				this.className += " hover";
				window.status=this.__msh.tooltip||"";
			};
			item.onmouseout = function() { this.className = "item";window.status=""; };
			item.oncontextmenu = function(ev) {
				this.__msh.activate();
				if (!ContextMenu.is_ie)
					ContextMenu._stopEvent(ev);
				return false;
			};
			item.onclick = function(ev) {
				//alert(ev.srcElement.id);
			};
			item.onmouseup = function(ev) {
				var timeStamp = (new Date()).getTime();
				if (timeStamp - self.timeStamp > 500)
					this.__msh.activate();
				if (!ContextMenu.is_ie)
					ContextMenu._stopEvent(ev);
				return false;
			};
			//if (typeof option[2] == "string")
			//item.title = option[2];
		}
	}
	buffer = document.getElementById("ezecbuffer").contentWindow.document;
	buffer.body.innerHTML = doc.body.innerHTML;
	w = buffer.body.firstChild.offsetWidth;
	h = buffer.body.firstChild.offsetHeight;
	if (ContextMenu.is_ie) {
		this.iePopup.show(ev.screenX, ev.screenY, w, h);
	}else{
		cmenu = document.getElementById('ezecmenu');
		cmenu.style.left = ev.clientX;
		cmenu.style.top = ev.clientY;
		cmenu.style.width = w+"px";
		cmenu.style.height = h+"px";
		if (cmenu.style.visibility == "hidden")
			cmenu.style.visibility="visible";
		else {
			cmenu.style.visibility="hidden";
		}
	}
	
	this.currentMenu = (div);
	this.timeStamp = (new Date()).getTime();
	return false;
};

ContextMenu = new ContextMenu("MENU_1_CONTAINER_0");
//--------------------------------------------------------------------------//
ContextMenu.agt		= navigator.userAgent.toLowerCase();
ContextMenu.is_ie	= ((ContextMenu.agt.indexOf("msie") != -1) && (ContextMenu.agt.indexOf("opera") == -1));
ContextMenu._addEvent = function(el, evname, func) {
    if (ContextMenu.is_ie) {
        el.attachEvent("on" + evname, func);
    } else {
        el.addEventListener(evname, func, true);
    }
};

ContextMenu._addEvents = function(el, evs, func) {
    for (var i in evs) {
        ContextMenu._addEvent(el, evs[i], func);
    }
};

ContextMenu._removeEvent = function(el, evname, func) {
    if (ContextMenu.is_ie) {
        el.detachEvent("on" + evname, func);
    } else {
        el.removeEventListener(evname, func, true);
    }
};

ContextMenu._removeEvents = function(el, evs, func) {
    for (var i in evs) {
        ContextMenu._removeEvent(el, evs[i], func);
    }
};

ContextMenu._stopEvent = function(ev) {
    if (ContextMenu.is_ie) {
        ev.cancelBubble = true;
        ev.returnValue = false;
    } else {
        ev.preventDefault();
        ev.stopPropagation();
    }
};
//--------------------------------------------------------------------------//



function get_ezeIFRAME(name, file, show){
	var hide = (show)?"":"hidden";
	return '<iframe id="eze'+name+'" height="100" src="'+file+'" marginwidth="0" marginheight="0" border="0" frameborder="no" scrolling="no" style="visibility:'+hide+'; position: absolute;"></iframe>';
}
document.write(get_ezeIFRAME("cmenu", "plugins/ContextMenu/explore.htm"));
document.write(get_ezeIFRAME("cbuffer", "plugins/ContextMenu/explore.htm"));
document.write(get_ezeIFRAME("remote", "plugins/ContextMenu/explore.htm"));
document.onmousedown=hidemenu;
function hidemenu(){
	document.getElementById('ezecmenu').style.visibility="hidden";
}
function move_item(dir,id,pid){
	pid=(isNaN(pid))?0:pid;
	if(isNaN(id)) {
		alert("Error: This action requires a valid ID\nThe ID passed in was "+id+"\nPlease contact your administrator for support");
		return false;
	}
	action(_base_url+"explore_actions.php?action=move&dir="+dir+"&id="+id+"&pid="+pid);
}
function action(url){
	document.getElementById("ezeremote").contentWindow.location.href=url;
}
function go(url){
	void(open(url,_base_editor_frame));
}