function popup_menu(offElName,offX,offY,setTrigger){
	var offEl=document.getElementById(offElName);
	//alert("offEl="+offEl)
	var element=new Object();
	if(document.all){
		element.popup=window.createPopup();
	}else{
		function createPopup(){
			var o=document.createElement("IFRAME");
			e=document.body.appendChild(o);
			e.style.position="absolute";
			e.style.display='none';
			e.show=function(x,y,w,h){
				var obj=arguments.length>4?arguments[4]:null;
				if(obj){
					e.style.display='block';
					e.style.top=y;
					e.style.left=x;
					e.style.width=w;
					e.style.height=h;
				}
			};
			return e.contentWindow;
		};
		element.popup=createPopup();
	}
	//alert(element.popup)
	element.popup.document.body.appendChild(element.popup.document.createElement("<div style='cursor: default; background-color: buttonface; border:2px outset buttonface'>"));
	element.offElement=new Object();
	element.offX=new Object();
	element.offY=new Object();
	element.offElement[offEl.id]=offEl;
	element.offX[offEl.id]=offX;
	element.offY[offEl.id]=offY;
	element.width=0;
	element.height=0;
	element.addItem=function(name,action){
		var div=element.popup.document.createElement("<span style='cursor: default; height: 20px; background-color: #ccc; font: 12px verdana; color: #111133'>");
		var div2=document.createElement("<span style='cursor: default; height: 20px; background-color: #ccc; font: 12px verdana;'>");
		div.innerText=name;
		div2.innerText=name;
		document.body.appendChild(div2);
		div.action=action;
		div.attachEvent("onmousedown",action);
		div.attachEvent("onmousedown",element.close);
		div.attachEvent("onmouseover",element.mouseOver);
		div.attachEvent("onmouseout",element.mouseOut);
		element.popup.document.body.firstChild.appendChild(div);
		element.popup.document.body.firstChild.appendChild(element.popup.document.createElement("<br>"));
		var width=div2.offsetWidth;
		var height=div2.offsetHeight;div2.removeNode(true);
		if(width>element.width)element.width=width+20;
		element.height+=height;
		//alert(element.height);
	};
	element.replace=function(html){
		//alert(html)
		var div=element.popup.document.createElement("<span style='cursor: default; background-color: #ccc; font: 12px verdana; color: #111133'>");
		var div2=document.createElement("<span style='cursor: default; background-color: #ccc; font: 12px verdana;'>");
		div.innerHTML=html;
		div2.innerHTML=html;
		
		element.popup.document.body.appendChild(div);

		ezw_buffer = top.document.getElementById("eze_popup_buffer");
		buffer = ezw_buffer.contentWindow.document;
		
		//alert(buffer.body.innerHTML)
		buffer.body.innerHTML = html;
		//alert(buffer.body.firstChild.nodeName)
		var width = buffer.body.firstChild.offsetWidth;
		var height = buffer.body.firstChild.offsetHeight;
		
		if(arguments.length>1){
			element.width=arguments[1];
		}else
			if(width>element.width)element.width=width+20;
		if(arguments.length>2){
			element.height+=arguments[2];
		}else
			element.height+=height;
		//alert(element.height+":"+element.width)
	};
	element.dim=function(){
		//
	}
	element.close=function(){
		element.popup.hide();
	};
	element.open=function(){
		if(element.noShow){
			element.noShow=false;
			return;
		}
		element.noShow=true;
		var spans=element.popup.document.getElementsByTagName("span");
		var length=spans.length;
		for(var i=0;i<length;i++){
			spans[i].style.backgroundColor="buttonface";
			spans[i].style.width=element.width;
		}
		var id = event.srcElement.id;
		if(!(element.width>0)) element.width=200;
		if(!(element.height>0)) element.height=200;
		//alert(element.offY[id]+':'+element.offElement[id])
		element.popup.show(element.offX[id],element.offY[id],element.width,element.height,element.offElement[id]);
	};
	element.setTrigger=function(trigger){
		try{
		element.offElement[trigger.id]=trigger;
		if(arguments.length>1){
			element.offX[trigger.id]=arguments[1];
			element.offY[trigger.id]=arguments[2];
		}
		trigger.attachEvent("onmousedown",element.open);
		trigger.attachEvent("onmouseenter",element.mouseEnter);
		}catch(e){}
	};
	element.mouseEnter=function(){
		if(element.popup.isOpen)element.noShow=true;
		else element.noShow=false;
	};
	element.mouseOver=function(){
		var event=element.popup.document.parentWindow.event;
		src=event.srcElement;
		src.style.backgroundColor="#ccc";
	};
	element.mouseOut=function(){
		var event=element.popup.document.parentWindow.event;
		src=event.srcElement;
		src.style.backgroundColor="buttonface";
	};
	if(typeof(setTrigger)=="boolean" && setTrigger==true) element.setTrigger(offEl);
	return element;
}
var eze_IFRAME_idx=0;
function eze_IFRAME(name, file, show){
	var hide = (show)?"":"hidden";
	return '<iframe id="eze_'+name+'" height="100" src="'+file+'" marginwidth="0" marginheight="0" border="0" frameborder="no" scrolling="no" style="visibility:'+hide+'; position: absolute;z-index:1000'+(eze_IFRAME_idx++)+';"></iframe>';
}
//document.write(eze_IFRAME("popup_buffer", "includes/blank.php",false));