var _helpers=new Array();
if(top.tabs && typeof(top.tabs)=="object" && typeof(top.tabs[_HELPERS])!="object"){
	top.tabs[_HELPERS]=new Object();
} else {
	top.tabs=new Object();
	top.tabs[_HELPERS]=new Object();
}
function _helper(args){
	this.id=_helpers.length;
	var name = (args.length>0)?args[0]:'_no_name_for_helper_';
	var id = (args.length>1)?args[1]:0;
	var text_value="";
	if(typeof(id)=="object") {
		id=args[1][1];
		text_value=args[1][0];
	}
	var real_value = (args.length>2)?args[2]:id;
	var display_value = (args.length>3)?args[3]:text_value;
	var icon = "btn_get_employee_array_bg";
	var w = 200;
	var wtype = "px";
	
	var dd_name = 'h_'+name+'_dd';
	var hist_name = 'h_'+name+'_hist_dd';
	var img='I_'+name;
	var size=10;
	var options_name = 'get_employee_array';
	if(typeof(top.gui.options[options_name])!='object') top.gui.options.add(options_name);
	this.options = top.gui.options[options_name]._html;
	//alert(this.options)
	this.html =	'<div id="h_'+name+'" class="helper">'+
					'<input type="hidden" name="'+name+'" id="'+name+'" value="'+real_value+'" />'+
					'<input type="text" style="width:'+w+wtype+'" name="'+name+'_text" id="'+name+'_text" value="'+display_value+'" '+
					'	onfocus="this.className=\'active\';this.select();" '+
					'	onblur="this.className=\'\';" '+
					' onchange="helper_filter(\''+name+'\');" '+
					' />'+
					'<img src="'+CATS_ICONS_PATH+icon+'.gif" align="absmiddle" id="'+img+'" onclick="helper_show(\''+name+'\');" />\n';
	document.write(this.html);
	//dalert(this.html);
	this.popup_html='<div><div id="h_'+name+'_dd_cont" style="display:none;position:absolute;">'+
					'<div class="h_tab_btns"><a id="h_'+name+'_a_hist" href="javascript:helper_switch(\''+name+'\',\'_hist\',\'\');">History</a><a id="h_'+name+'_a" href="javascript:helper_switch(\''+name+'\',\'\',\'_hist\');">Results</a></div>'+
					'<div class="h_tabs"><select style="width:'+(w+25)+wtype+'" size="'+size+'" id="'+dd_name+'" onclick="helper_results_click(this,\''+name+'\');">\n<option></option>\n'+ //'			' + _get_dd_options(_get_data('get_employee_array'),2) +'\n'+
					this.options +'\n'+
					'</select>\n'+
					'<select style="width:'+(w+25)+wtype+'" size="5" id="'+hist_name+'" onclick="helper_history_click(this,\''+name+'\')">\n<option></option>\n'+
					_get_dd_helper_history(name)+
					'</select>\n'+
					'</div>\n'+
					'</div>\n';
	document.write(this.popup_html);
	//dalert(this.popup_html);
	var el=element(dd_name);
	//dalert(el);
	remote.get_options_add({'name':name,'type':'text','value':''},el,options_name,'users',id);
	
	//var img=args.length>0?args[0]:'popup_helper_image';
	//dalert(typeof(popup_menu));
	_helpers[this.id]=new popup_menu(img,-200,20,true);
	//dalert(_helpers[this.id] +":"+ this.id);
	_helpers[this.id].replace(this.popup_html,182,176);
	//dalert(this.html);
	//dalert(_helpers[this.id]);
	this.doc=window._helpers[this.id];
	return this.doc;
/*	if(args[1]==true) return this.html;//document.write(this.html);
	else return this.html*/
}

function get_helper_dd(){
	help=new _helper(arguments);
}

function _get_dd_helper_history(name){
	var id='h_'+name+'_hist_dd';
	var his=element(id);
	//dalert(top.session.history)
	if(top.session && top.session.history[id]) return top.session.history[id].innerHTML;
}




/*this function is used to select an employee from a drop down box by
by using text entered in the text box next to the drop down as the search criteria*/
function helper_filter(helper){
	var el=element(helper);
	var dd=element('h_'+helper+'_dd');
	var text=element(''+helper+'_text');
	helper_show(helper);
	helper_switch(helper,'','_hist');
	remote.get_options_add(text, dd, 'get_employee_array', 'users');
}
function helper_show(helper){
	var el=element(helper);
	remote.get_options_add(text, dd, 'get_employee_list', 'users');
}
function show_helper_menu(e,d,name,func,module){
	top.helper_show_menu(e,d,name,func,module);
}


function helper_show2(name){
	dalert(name);
	var sid='h_'+name+'_dd_cont';
	var hst=element('h_'+name+'_hist_dd');
	hst={options:[]};
	var rst=element('h_'+name+'_dd');
	var on=(hst.options.length>HISTORY_MIN_LENGTH)?'_hist':'';
	var off=(on)?'':'_hist';
	ACTIVE_HELPER=element(sid);
	helper_switch(name,on,off);
	show(sid);
}
function helper_hide(){
	ACTIVE_HELPER.style.display='none';
	ACTIVE_HELPER=null;
}

function helper_add_history(itm,name){
	var id="h_"+name+"_hist_dd";
	var his=element(id);
	var i=0;
	var a=[];
	var len=his.options.length
	for(i=0;i<len;i++){
		a[i]=[his.options[i].value,his.options[i].text,his.options[i].selected];
		if(his.options[i].value==itm.options[itm.selectedIndex].value) return;
	}
	for(i=0;i<len;i++) his.options[i] = null;
	if(itm.options[itm.selectedIndex].value!='') a[a.length]=[itm.options[itm.selectedIndex].value,itm.options[itm.selectedIndex].text,true];
	for(i=0;i<a.length;i++){
		his.options[i]=new Option(a[i][1],a[i][0],a[i][2]);
	}
	top.session.add_history(his);
}
function helper_results_click(itm,name){
	element(name).value=itm.options[itm.selectedIndex].value;
	element(name+'_text').value=itm.options[itm.selectedIndex].text;
	helper_add_history(itm,name);
	helper_hide('h_'+name+'_dd_cont');
}
function helper_history_click(itm,name){
	element(name).value=itm.options[itm.selectedIndex].value;
	element(name+'_text').value=itm.options[itm.selectedIndex].text;
	helper_hide('h_'+name+'_dd_cont');
}

function helper_switch(name,on,off){
	show('h_'+name+on+'_dd');
	hide('h_'+name+off+'_dd');
	element('h_'+name+'_a'+on).className='selected';
	element('h_'+name+'_a'+off).className='';
}

function _get_dd_helper_history(name){
	var id='h_'+name+'_hist_dd';
	var his=element(id);
	//alert(top.session.history)
	if(top.session && top.session.history[id]) return top.session.history[id].innerHTML;
	else return "";
}

/*****************************************

Right Click Menu below 
 TODO: Complete a contect menu for general use

********************************************/

function ContextMenu(editor) {
	this.editor = editor;
};

// RC Context menu
// Get

ContextMenu.prototype.getContextMenu = function(target) {
	var self = this;
	var tree = this.tree;
	var menu = [];

	var currentTarget = target;
	
	//alert(target.parentNode.parentNode.parentNode.innerHTML)
	target = target.parentNode.parentNode;
	var type = target.getAttribute("type");
	function canCutCopy(tgt){
		return true;
	}
	function canPaste(tgt){
		return !ContextMenu.clipboard.isempty();
	}
	function canDelete(id){
		return !(id > 1);
	}
	function canMove(dir){
		if(dir=="up"){
			return !(target.previousSibling&&target.previousSibling.nodeName=="DIV");
		}else if(dir=="down"){
			return !(target.nextSibling.nextSibling&&target.nextSibling.nextSibling.nodeName=="DIV");
		}
	}
	menu.push(
		  [ "Edit Item...",
		    function() {
			    open(_base_url+"editor.php?action=edit&id="+target.id,_base_editor_frame);
		    },
		    "Edit currently selected item",
				_base_url+"images/m-edit.gif" ]);
	menu.push(null,
		  [ "Add Item...",
		    function() {
			    open(_base_url+"editor.php?action=add&pid="+target.id,_base_editor_frame);
		    },
		    "Add a new item to currently selected item",
				_base_url+"images/m-add.gif" ]);
	if(CHILD_CREATION_DISABLED_LIST.indexOf(type)==-1){
	menu.push(null,
			[ "Cut", function() { ContextMenu.clipboard.add({"Node":{"id":target.id,"action":"cut"}}); }, "Cut this item and place into the clipboard", _base_url+"images/ed_cut.gif", !canCutCopy(target) ],
			[ "Copy", function() { ContextMenu.clipboard.add({"Node":{"id":target.id,"action":"copy"}}); }, "Copy this item to the clipboard", _base_url+"images/ed_copy.gif", !canCutCopy(target) ],
			[ "Paste", function() { open("explore.php?action=paste&mode="+window.ContextMenu.clipboard.value["Node"].action+"&id_list="+window.ContextMenu.clipboard.value["Node"].id+"&id="+target.id,"_self"); }, "Paste items from the clipboard into this item", _base_url+"images/ed_paste.gif", !canPaste(target) ]);
	}
	if(DELETE_DISABLED_LIST.indexOf(type)==-1){
	menu.push(null,
			[ "Delete",
		    function() {
					if(confirm("Are you sure you want to delete this page?\nWarning: Any children of this page will no longer be accessible!"))
						open(_base_url+"editor.php?action=del&id="+target.id,_base_editor_frame);
		    },
		    "Delete selected item",
				_base_url+"images/ed_delete.gif", canDelete(target.id) ]);
	}
	menu.push(null,
			[ "Move to Top",
		    function() {
					window.move_item("top",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item to the top of the branch",
				_base_url+"images/m-movetop.gif", canMove("up") ],
		  [ "Move Item Up",
		    function() {
					window.move_item("up",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item up one space",
				_base_url+"images/m-moveup.gif", canMove("up") ],
			[ "Move Item Down",
		    function() {
			    window.move_item("down",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item down one space",
				_base_url+"images/m-movedown.gif", canMove("down") ],
			[ "Move to Bottom",
		    function() {
					window.move_item("bot",target.id,target.parentNode.previousSibling.id);
		    },
		    "Move selected item to the bottom of the branch",
				_base_url+"images/m-movebot.gif", canMove("down") ]
	);
	menu.push(null,
		  [ "Refresh Node",
		    function() {
					window.action(_base_url+"explore_actions.php?pid="+target.id);
		    },
		    "Refresh children items" ]);
	menu.push(null,
		  [ "Properties...",
		    function() {
			    alert("base_url="+_base_url+"\nid="+target.id+"\nbase_frame="+_base_editor_frame+"\nnode="+target.outerHTML+"\n"+target.nextSibling.outerHTML);
		    },
		    "Display currently selected items extended attributes" ]);
	
	return menu;
};

ContextMenu.prototype.popupMenu = function(ev) {
	var self = this;
	if (this.currentMenu)
		this.currentMenu.parentNode.removeChild(this.currentMenu);
	function getPos(el) {
		var r = { x: el.offsetLeft, y: el.offsetTop };
		if (el.offsetParent) {
			var tmp = getPos(el.offsetParent);
			r.x += tmp.x;
			r.y += tmp.y;
		}
		return r;
	};
	function documentClick(ev) {
		ev || (ev = window.event);
		if (!self.currentMenu) {
			alert("How did you get here? (Please report!)");
			return false;
		}
		var el = ContextMenu.is_ie ? ev.srcElement : ev.target;
		for (; el != null && el != self.currentMenu; el = el.parentNode);
		if (el == null)
			self.closeMenu();
	};
	self.closeMenu = function() {
		self.currentMenu.parentNode.removeChild(self.currentMenu);
		self.currentMenu = null;
		if (ContextMenu.is_ie)
			self.iePopup.hide();
	};
	
	var target = ContextMenu.is_ie ? ev.srcElement : ev.target;
	var ifpos = {x:0,y:0};//getPos(document);
	var x = ev.clientX + ifpos.x;
	var y = ev.clientY + ifpos.y;
	
	if(target.nodeName=="IMG") return true;
	
	var div;
	var doc;
	if (!ContextMenu.is_ie) {
		doc = document.getElementById("ezecmenu").contentWindow.document;
	} else {
		// IE stinks
		var popup = this.iePopup = window.createPopup();
		doc = popup.document;
		doc.open();
		doc.write("<html><head><style type='text/css'>@import url(" + _base_url + "plugins/ContextMenu/menu.css); html, body { padding: 0px; margin: 0px; overflow: hidden; border: 0px; }</style></head><body unselectable='yes'></body></html>");
		doc.close();
	}
	div = doc.createElement("div");
	cont = doc.createElement("div");
	if (ContextMenu.is_ie)
		div.unselectable = "on";
	div.oncontextmenu = function() { return false; };
	div.className = "htmlarea-context-menu";
	if (!ContextMenu.is_ie)
		div.style.left = div.style.top = "0px";

	cont.appendChild(div);
	doc.body.innerHTML = cont.innerHTML;
	div = doc.body.firstChild;
	
	var table = doc.createElement("table");
	div.appendChild(table);
	table.cellSpacing = 0;
	table.cellPadding = 0;
	var parent = doc.createElement("tbody");
	table.appendChild(parent);

	var options = this.getContextMenu(target);
	for (var i = 0; i < options.length; ++i) {
		var option = options[i];
		var item = doc.createElement("tr");
		parent.appendChild(item);
		if (ContextMenu.is_ie)
			item.unselectable = "on";
		else item.onmousedown = function(ev) {
			ContextMenu._stopEvent(ev);
			return false;
		};
		if (!option) {
			item.className = "separator";
			var td = doc.createElement("td");
			td.className = "icon";
			var IE_IS_A_FUCKING_SHIT = '>';
			if (ContextMenu.is_ie) {
				td.unselectable = "on";
				IE_IS_A_FUCKING_SHIT = " unselectable='on' style='height=1px'>&nbsp;";
			}
			td.innerHTML = "<div" + IE_IS_A_FUCKING_SHIT + "</div>";
			var td1 = td.cloneNode(true);
			td1.className = "label";
			item.appendChild(td);
			item.appendChild(td1);
		} else {
			var label = option[0];
			item.className = "item";
			item.__msh = {
				item: item,
				label: label,
				action: option[1],
				tooltip: option[2] || null,
				icon: option[3] || null,
				disabled: option[4] || false,
				activate: function() {
					self.closeMenu();
					this.action();
				}
			};
			item.disabled = (option[4])?option[4]:false;
			label = label.replace(/_([a-zA-Z0-9])/, "<u>$1</u>");
			if (label != option[0])
				keys.push([ RegExp.$1, item ]);
			label = label.replace(/__/, "_");
			var td1 = doc.createElement("td");
			if (ContextMenu.is_ie)
				td1.unselectable = "on";
			item.appendChild(td1);
			td1.className = "icon";
			if (item.__msh.icon)
				td1.innerHTML = "<img align='middle' src='" + item.__msh.icon + "' />";
			var td2 = doc.createElement("td");
			if (ContextMenu.is_ie)
				td2.unselectable = "on";
			item.appendChild(td2);
			td2.className = "label";
			td2.innerHTML = label;
			item.onmouseover = function() {
				this.className += " hover";
				window.status=this.__msh.tooltip||"";
			};
			item.onmouseout = function() { this.className = "item";window.status=""; };
			item.oncontextmenu = function(ev) {
				this.__msh.activate();
				if (!ContextMenu.is_ie)
					ContextMenu._stopEvent(ev);
				return false;
			};
			item.onclick = function(ev) {
				//alert(ev.srcElement.id);
			};
			item.onmouseup = function(ev) {
				var timeStamp = (new Date()).getTime();
				if (timeStamp - self.timeStamp > 500)
					this.__msh.activate();
				if (!ContextMenu.is_ie)
					ContextMenu._stopEvent(ev);
				return false;
			};
			//if (typeof option[2] == "string")
			//item.title = option[2];
		}
	}
	buffer = document.getElementById("ezecbuffer").contentWindow.document;
	buffer.body.innerHTML = doc.body.innerHTML;
	w = buffer.body.firstChild.offsetWidth;
	h = buffer.body.firstChild.offsetHeight;
	if (ContextMenu.is_ie) {
		this.iePopup.show(ev.screenX, ev.screenY, w, h);
	}else{
		cmenu = document.getElementById('ezecmenu');
		cmenu.style.left = ev.clientX;
		cmenu.style.top = ev.clientY;
		cmenu.style.width = w+"px";
		cmenu.style.height = h+"px";
		if (cmenu.style.visibility == "hidden")
			cmenu.style.visibility="visible";
		else {
			cmenu.style.visibility="hidden";
		}
	}
	
	this.currentMenu = (div);
	this.timeStamp = (new Date()).getTime();
	return false;
};