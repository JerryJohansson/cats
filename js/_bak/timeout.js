var oCheckTimeoutInterval="";
var iSessionTimeout = <%=(Session.Timeout * 60 * 1000)%>;
function ezw_checkSessionTimedOut(){
	if(typeof(top.oCheckTimeoutInterval)=="undefined") top.oCheckTimeoutInterval = "";
	if(top.oCheckTimeoutInterval==""){
		 
		// show dialog 1 minute before session timeout
		//alert(iSessionTimeout-(1000*60))
		//top.oCheckTimeoutInterval = top.setInterval("ezw_showLogonDialog()", iSessionTimeout-(1000*60)); 
		top.oCheckTimeoutInterval = top.setInterval("ezw_showLogonDialog()", iSessionTimeout); 
		
		 
	}	else{
		top.ezw_resetSessionTimedOut();
	}
}
//ezw_checkSessionTimedOut();
function ezw_resetSessionTimedOut(){
	if(oCheckTimeoutInterval != ""){
		top.clearInterval(top.oCheckTimeoutInterval);
		top.oCheckTimeoutInterval="";
		top.ezw_checkSessionTimedOut();
	}
}
// Dialog stuff
function ezw_showLogonDialog(){
//alert("ezw_showLogonDialog");
	var url = "<%=Application("ezw_AppRoot")%>Common/Dialogs/Logon.asp?"
	var oSend = new Object();
	oSend.msg = "Your session has timed out. You must re-submit your \"Account Name\" and \"Password\" before you can continue working.";
	oSend.iRole = (top.nROLE_MASK !== undefined) ? top.nROLE_MASK : 0 ;
	url += "msg=" + oSend.msg + "&rol=" + oSend.iRole;
	//var oLogon = showModalDialog(sAppRoot + "scripts/LogonDialog.asp", oSend, getDialogFeatures(430,230));
	var oLogon = showModalDialog(url, "", getDialogFeatures(430,230));
	
	if(typeof(oLogon)=="object" && oLogon.bError) top.location = "Login.asp";
	else if(typeof(oLogon)=="object" && oLogon.bRoleChanged) top.location.reload();
	else top.ezw_resetSessionTimedOut();
}