//--Browser sniffer---------------
var IE	= document.all;
var DOM	= document.getElementById;
var NS	= document.layers;
var NS6	= DOM && !IE;
//--------------------------------
function _error(e){
	this.submit_url="errors_log.php";
	this.err=new Array();
	this.log_error=function(e){
		var args=arguments;
		this.err[this.err.length]=e;
	}
	this.post=function(){
		alert(this.err.join())
	}
}
err=new _error();
function _cookies(){
	this.names=[];
	this.values=[];
	this.cookies=[];
	if(document.cookie){
		this.enabled=true;
		this.init = function(){
			this.cookies=document.cookie.split(";");
			for(i=0;i<this.cookies.length;i++){
				var a=this.cookies[i].split("=");
				this.names[i]=a[0];
				this.values[i]=a[1];
			}
		}
		this.search = function(name){
			var i=0;found=0;for(i=0;i<this.cookies.length;i++)if(name==this.names[i]){found=1;break;}
			return (found)?this.values[i]:'';
		}
		this.get = function(name){
			return this.search(name);
		}
		this.set = function(name,value){
			document.cookie = name + "=" + value;
		}
		this.init();
	}else{
		this.get=function(){};this.set=function(){};
		this.enabled=false;
	}
	return this;
}

// BEGIN::PROTOTYPE DATE OBJECTS-------------------------------------
Date.prototype.MONTHNUM = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
Date.prototype.MONTHNAME = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
Date.prototype.DAYNAME = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
Date.prototype.getDaysInMonth = function(){
	var year = this.getFullYear();
	var month = this.getMonth();
	var isLeap = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
	return (month + 1 == 2) && isLeap ? 29 : this.MONTHNUM[month];
}
Date.prototype.getFirstDay = function(){
	var dateTmp = new Date(this.getFullYear(), this.getMonth(), 1);
	return dateTmp.getDay();
}
Date.prototype.getIntegerDate = function(){
	return (this.getTime()-(new Date(1900,0,1)).getTime())/1000/60/60/24;
}
Date.prototype.getDateInteger = function (dayno){
	return new Date(1900,0,dayno);
}
Date.prototype.getMonthName = function(){return this.MONTHNAME[this.getMonth()];}
Date.prototype.getDayName = function(){return this.DAYNAME[this.getDay()];}
Date.prototype.padZero = function(s){var pad=("00"+s);return pad.substring(pad.length-2);}
Date.prototype.getMonthByName=function(n){
	var i=0;
	var a=this.MONTHNAME;
	for(i=0;i<a.length;i++){
		if(a[i].indexOf(n)==0){
			trace(i);
			break;
		}
	}
	return i;
}
Date.prototype.Format = function(){
	var dthis=new Date();
	var args=arguments;
	var format = (args.length>0 && args[0]!=undefined)?args[0]:"d-mm-yyyy";
	var date_string=(args.length>1)?args[1]:null;
	var d,dd,ddd,dddd,m,mm,mmm,mmmm,y,yy,yyyy,h,n,s,HH,MM;
	var dt=(args.length>1)?args[1]:this;//dthis;
	
	d=dt.getDate();
	dddd=dthis.getDayName();
	ddd=dddd.substr(0,3);
	dd=dthis.padZero(d);
	m=dt.getMonth()+1;
	mmmm=dthis.getMonthName();
	mmm=mmmm.substr(0,3);
	mm=dthis.padZero(m);
	y=dt.getYear();
	yy=y;
	yyyy=dt.getFullYear();
	h=dt.getHours();
	n=dt.getMinutes();
	s=dt.getSeconds();
	HH=dthis.padZero(h);
	MM=dthis.padZero(n);
	var ret="";
	var dps = format.split(" ");
	var partLen = dps.length;
	var i = j = 0;
	// loop thru parts of date string and try to find matches 
	// that wil be replaced by the corresponding date part
	for(i=0;i<partLen;i++){
		var dp=dps[i];
		delim = (dp.indexOf("-")!=-1)?"-":((dp.indexOf("/")!=-1)?"/":((dp.indexOf(":")!=-1)?":":" "));
		var dss=dp.split(delim);
		var dsLen=dss.length;
		for(j=0;j<dsLen;j++){
			ds = dss[j];
			if((dsLen-1)==j) delim = " ";
			switch(ds){
				case "d":
					ret += d+delim;
					break;
				case "dd":
					ret += dd+delim;
					break;
				case "ddd":
					ret += ddd+delim;
					break;
				case "dddd":
					ret += dddd+delim;
					break;
				case "m":
					ret += m+delim;
					break;
				case "mm":
					ret += mm+delim;
					break;
				case "mmm":
					ret += mmm+delim;
					break;
				case "mmmm":
					ret += mmmm+delim;
					break;
				case "y":
					ret += y+delim;
					break;
				case "yy":
					ret += yy+delim;
					break;
				case "yyyy":
					ret += yyyy+delim;
					break;
				case "HH":
					ret += HH+delim;
					break;
				case "MM":
					ret += MM+delim;
					break;
				default:
					ret += ds+" ";
			}
		}
	}
	return ret;
}
//alert(TheDateFormat("ddd-mmmm-yyyy HH:MM"));
// END::PROTOTYPE DATE OBJECTS-------------------------------------
// STRING PROTOTYPE
function toPropperCase(){
	var x=this.length;
	var i=0;
	var s="";
	var tmp=this.split("");
	for(i=0;i<x;i++){
		if(i==0){
			s+=tmp[i].toUpperCase();
		}else{
			if(tmp[i-1]==" "){
				s+=tmp[i].toUpperCase();
			}else{
				s+=tmp[i].toLowerCase();
			}
		}
	}
	return s;
}
String.prototype.toPropperCase = toPropperCase;
String.prototype.trim = function()
{
	return this.replace( /(^\s*)|(\s*$)/g, '' ) ;
}
// END:: PROTOTYPE STRING OBJECT

// BEGIN:: PROTOTYPE DOCUMENT
document.getElementsByClassName = function(className) {
  var children = document.getElementsByTagName('*') || document.all;
  var elements = new Array();
  
  for (var i = 0; i < children.length; i++) {
    var child = children[i];
    var classNames = child.className.split(' ');
    for (var j = 0; j < classNames.length; j++) {
      if (classNames[j] == className) {
        elements.push(child);
        break;
      }
    }
  }
  
  return elements;
}
// END:: PROTOTYPE DOCUMENT


// BEGIN::TEXTAREA MAXLENGTH
function set_textarea_maxlength()
{
	var x = document.getElementsByTagName('textarea');
	var counter = document.createElement('div');
	counter.className = 'counter';
	for (var i=0;i<x.length;i++)
	{
		if (x[i].getAttribute('maxlength'))
		{
			var counterClone = counter.cloneNode(true);
			counterClone.relatedElement = x[i];
			counterClone.innerHTML = '<span>0</span> of '+x[i].getAttribute('maxlength')+' allowed';
			x[i].parentNode.insertBefore(counterClone,x[i].nextSibling);
			x[i].relatedElement = counterClone.getElementsByTagName('span')[0];
			x[i].state = "open";
			//x[i].onkeyup = x[i].onchange = checkMaxLength;
			x[i].onkeydown = x[i].onchange = function(evt){return check_maxlength(this,evt);};
			x[i].onkeydown(x[i]);
		}
	}
}
function check_maxlength(itm,evt){
	var maxLen = itm.getAttribute('maxlength');
	var curLen = itm.value.length;
	var ret=true;
	if (curLen > maxLen){
		itm.relatedElement.className = 'toomuch';
		if(itm.getAttribute("state")!="editing"){
			ret=_deny_keypress(itm, '', evt);
			var rem=curLen-maxLen;
			if(rem>2){
				if(confirm('You have entered too many characters.\nClick OK to reduce the amount of text used (Warning: This will delete the last '+rem+' characters in the text field).\nClick Cancel if you wish to edit the text yourself.')){
					itm.value=itm.value.substring(0,maxLen-1);
					curLen=maxLen;
				}else{
					itm.state = "editing";
					itm.onblur = function(evt){return check_text_length(this, evt.target)};
				}
			}
		}
	}else{
		itm.onblur = function(){};
		itm.state = "open";
		itm.relatedElement.className = '';
	}
	itm.relatedElement.firstChild.nodeValue = curLen;
	return ret;
}
function _deny_keypress (item, exception, evt){
	var keyCode = document.all ? evt.keyCode : evt.which;
	if(keyCode<20) alert(keyCode==8);
	if (keyCode === exception)
		return true;
	else
		return (keyCode == exception);
}
function check_text_length(itm, evt){
	var ret=true;
	if(itm.maxlength && itm.maxlength < itm.value.length){
		alert('There are still too many characters in this field.\nThis field will be truncated to '+itm.maxlength+' characters in length.');
		itm.value.substring(0,itm.maxlength);
		ret = false;
	}
	return ret;
}
function checkMaxLength()
{
	var maxLength = this.getAttribute('maxlength');
	var currentLength = this.value.length;
	if (currentLength > maxLength){
		this.relatedElement.className = 'toomuch';
		this.value=this.value.substring(0,maxLength);
		currentLength=maxLength;
	}else
		this.relatedElement.className = '';
	this.relatedElement.firstChild.nodeValue = currentLength;
	// not innerHTML
}