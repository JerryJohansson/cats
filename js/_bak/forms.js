<!-- Hide
// define variables for toc
var defaultEmptyText="<Click here to edit this field>";
var activeCSS="border:2px inset; cursor: text;";
var inactiveCSS="border:1px solid #666666; cursor: default;";
var validTextColor="black";
var invalidTextColor="#666666";
//alert("forms")
function initEntryField(e,sValue){
	e.value = sValue;
	e.setAttribute("emptytext",sValue);
}
function alertMe(s){
	//alert(s);
	return s;
}
function doInput(el){
	var emptyText=el.getAttribute("emptytext");
	var w="";
	if(el.editMode==null)
		el.editMode=false;
	if(!el.editMode){
		el.editMode=true;
		/*
		if((el.value==emptyText)||(emptyText==null)){
			emptyText=el.value;
			el.value="";
			el.setAttribute("emptytext",emptyText);
		}
		*/
		el.title = el.value;
		if(el.tagName != "TEXTAREA"){
			w = (el.style.width!="")?"width="+el.style.width+";":"";
			el.style.cssText=activeCSS + w;
		}
	}else{
		el.editMode=false;
		if(el.tagName != "TEXTAREA"){
			w = (el.style.width!="")?"width="+el.style.width+";":"";
			el.style.cssText=inactiveCSS + w;
		}
		if(el.value.replace(/\s/g,"")!=""){
			el.style.color=validTextColor;
		/*
		}else{
			if(emptyText==null){
				emptyText=defaultEmptyText;
				el.setAttribute("emptytext",emptyText);
			}
			if(el.name!="Value")
				el.value=emptyText;
			el.style.color=invalidTextColor;
		*/
		}
	}
}

function btnClass(eBtn){
	var type = event.type;
	var sClass = eBtn.className;
	var on = /mouseover|focus/;
	var off = /mouseout|blur/;
	if(typeof(eBtn.originalclass)=="undefined"){
		eBtn.originalclass = eBtn.className;
	}
	if(type.match(on)) eBtn.className = sClass + "on";
	else if(type.match(off)) eBtn.className = eBtn.originalclass;
}

var frmSubmitOnce = false;
function checkSubmit(frm){
		if(frmSubmitOnce) return false
		frmSubmitOnce = true;
		return frmSubmitOnce;
}

function setFormElement(){
	var args = setFormElement.arguments;
	var value = (args.length>1)?args[1]:"";
	if( typeof( args[0] ) == "object" ){
		if( args[0].nodeName.match(/INPUT|TEXTAREA|SELECT|BUTTON/gi) == null ) {
			alert("The object is not a form element");
			return false;
		}
		switch( args[0].type ){
			case "text": case "textarea": case "hidden":
				args[0].value = value;
				break;
			case "radio": case "checkbox" :
				if(typeof(args[0][0])=="object"){
					for(i=0;i<args[0].options.length;i++){
						if( args[0][i].value == value ) {args[0][i].selected = true;break;}
					}
				}else{
					args[0].checked;
				}
				break;
			case "button": case "submit": case "reset":
				args[0].value = value;
				break;
			case "select":
				for(i=0;i<args[0].options.length;i++){
					if( args[0][i].value == value ) {args[0][i].selected = true;break;}
				}
				break;
			default:
				if( args[0].nodeName.toLowerCase() == "select" ){
					for(i=0;i<args[0].options.length;i++){
						if( args[0][i].value == value ) {args[0][i].selected = true;break;}
					}
				}else if( args[0].type.match(/[button|submit|reset]/) ){
					if(confirm("are u sure u want to change the value of the button")) args[0].value = value;
				}
				break;
		}
	}
}






function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}

function checkFileUpload(form,list,requireUpload,sizeLimit,minWidth,minHeight,maxWidth,maxHeight,saveWidth,saveHeight) { //v2.09
  document.MM_returnValue = true;
  for (var i = 0; i<form.elements.length; i++) {
    item = form.elements[i];
		pass = item.nodeName.toUpperCase()!="FIELDSET";
    if (pass&&item.type.toUpperCase() != 'FILE') continue;
    if(pass)checkOneFileUpload(item,list,requireUpload,sizeLimit,minWidth,minHeight,maxWidth,maxHeight,saveWidth,saveHeight);
} }

function checkOneFileUpload(item,list,requireUpload,sizeLimit,minWidth,minHeight,maxWidth,maxHeight,saveWidth,saveHeight) { //v2.09
  document.MM_returnValue = true;
  if (list != '') var re = new RegExp("\.(" + list.replace(/,/gi,"|").replace(/\s/gi,"") + ")$","i");
    if (item.value == '') {
      if (requireUpload) {alert('File is required!');document.MM_returnValue = false;setupFocus(item,true);return;}
    } else {
      if(list != '' && !re.test(item.value)) {
        alert('This file type is not allowed for uploading.\nOnly the following file extensions are allowed: ' + list + '.\nPlease select another file and try again.');
        document.MM_returnValue = false;setupFocus(item,true);return;
      }
    document.PU_uploadForm = item.form;
    re = new RegExp(".(gif|jpg|png|bmp|jpeg)$","i");
    if(re.test(item.value) && (sizeLimit != '' || minWidth != '' || minHeight != '' || maxWidth != '' || maxHeight != '' || saveWidth != '' || saveHeight != '')) {
      checkImageDimensions(item,sizeLimit,minWidth,minHeight,maxWidth,maxHeight,saveWidth,saveHeight);
    } }
}

// ----------------------------
//	Validates file types using the files extension
// ----------------------------
function checkFileType(item, list){
	if (list != ""){
		var re = new RegExp("\.(" + list.replace(/,/gi,"|").replace(/\s/gi,"") + ")$","i");
    if (item.value != "") {
      if(list != "" && !re.test(item.value)) {
        alert("This file type is not allowed for uploading.\nOnly the following file extensions are allowed: " + list + ".\nPlease select another file and try again.");
				item.value = "";
				setupFocus(item,true);
				return;
      }
    }
	}
}

function showImageDimensions(fieldImg) { //v2.09
  var isNS6 = (!document.all && document.getElementById ? true : false);
  var img = (fieldImg && !isNS6 ? fieldImg : this);
  if (img.width > 0 && img.height > 0) {
  if ((img.minWidth != '' && img.minWidth > img.width) || (img.minHeight != '' && img.minHeight > img.height)) {
    alert('Uploaded Image is too small!\nShould be at least ' + img.minWidth + ' x ' + img.minHeight); return;}
  if ((img.maxWidth != '' && img.width > img.maxWidth) || (img.maxHeight != '' && img.height > img.maxHeight)) {
    alert('Uploaded Image is too big!\nShould be max ' + img.maxWidth + ' x ' + img.maxHeight); return;}
  if (img.sizeLimit != '' && img.fileSize > img.sizeLimit) {
    alert('Uploaded Image File Size is too big!\nShould be max ' + (img.sizeLimit/1024) + ' KBytes'); return;}
  if (img.saveWidth != '') document.PU_uploadForm[img.saveWidth].value = img.width;
  if (img.saveHeight != '') document.PU_uploadForm[img.saveHeight].value = img.height;
  document.MM_returnValue = true;
} }

function checkImageDimensions(item,sizeL,minW,minH,maxW,maxH,saveW,saveH) { //v2.09
  if (!document.layers) {
    var isNS6 = (!document.all && document.getElementById ? true : false);
    document.MM_returnValue = false; var imgURL = 'file:///' + item.value.replace(/\\/gi,'/').replace(/:/gi,'|').replace(/"/gi,'').replace(/^\//,'');
    if (!item.gp_img || (item.gp_img && item.gp_img.src != imgURL) || isNS6) {item.gp_img = new Image();
		   with (item) {gp_img.sizeLimit = sizeL*1024; gp_img.minWidth = minW; gp_img.minHeight = minH; gp_img.maxWidth = maxW; gp_img.maxHeight = maxH;
  	   gp_img.saveWidth = saveW; gp_img.saveHeight = saveH; gp_img.onload = showImageDimensions; gp_img.src = imgURL; }
	 } else showImageDimensions(field.gp_img);}
}



// ----------------------------
//	Validate an email address to
//	have all the correct components
// ----------------------------
function isEmailAddress (string) {
  var addressPattern = 
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  return addressPattern.test(string);
}
function checkemail (eMail) {
  if (!isEmailAddress(eMail.value)) {
    alert("Please enter correct email address!");
		setupFocus(eMail);
  }
}
// ----------------------------
//	Check date entered into date field
//	and format date to the prefered style
// ----------------------------
function checkDate(eDate) {
	var args = checkDate.arguments;
	var sDateStyle = (args.length>1)?args[1]:((typeof(sGLOBAL_DATE_STYLE)!="undefined")?sGLOBAL_DATE_STYLE:"AU");  // AU = Australian date style : US = United States date style
	var bShortDateFormat = (args.length>2)?args[2]:true; // true = "01/01/2003" : false = "1 Jan 2003";
	var bReturnValue = (args.length>3)?args[3]:false;
	var datePat = /^(\d{1,2})(\/|-|\s|\.)(\d{1,2})\2(\d{1,4})$/;
	var dateStr = eDate.value;
	var aMatch = dateStr.match(datePat); // is the format ok?
	var aMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var sTmp, sYear, sMonth, sDay, sReturnDate = "";
	// 
	var name = (args.length>4)?args[4]:"Date";
	var msg = "The following errors occured in the '" + name + "' field:\n";
	var iMsg = 0;
	var retValue = true;
	if (trim(dateStr)=="") return false;
	if (aMatch == null) {
		msg += (++iMsg) + ". Date is not in a valid format\n";
		retValue = false;
	}else{
		// parse date into variables
		sDay = aMatch[1];
		sMonth = aMatch[3];
		sYear = aMatch[4];
	}
	
	if ((typeof(sYear)!="undefined") && sYear.length < 4) {
		sTmp = "000"+sYear;
		sYear = "2" + sTmp.substring(sTmp.length-3);
	}
	
	if ((typeof(sYear)!="undefined") && sYear.length !=4) {
		msg += (++iMsg) + ". Please enter a 4 digit year.\n";
		retValue = false;
	}
	
	if (sMonth < 1 || sMonth > 12) { // check month range
		msg += (++iMsg) + ". Month must be between 1 and 12\n";
		retValue = false;
	}

	if (sDay < 1 || sDay > 31) {
		msg += (++iMsg) + ". Day must be between 1 and 31\n";
		retValue = false;
	}

	if ((sMonth==4 || sMonth==6 || sMonth==9 || sMonth==11) && sDay==31) {
		msg += (++iMsg) + ". " + aMonths[sMonth-1] + " does not have 31 days\n";
		retValue = false;
	}
	    
	if (sMonth == 2) { // check for february 29th
		var isleap = (sYear % 4 == 0 && (sYear % 100 != 0 || sYear % 400 == 0));
		if (sDay>29 || (sDay==29 && !isleap)) {
			msg += (++iMsg) + ". " + aMonths[sMonth-1] + " " + sYear + " doesn't have " + sDay + " days\n";
			retValue = false;
		}
	}
	if(retValue){
		if (bShortDateFormat){
			sDay = dtFixEl(sDay);sMonth = dtFixEl(sMonth);
			sReturnDate = (sDateStyle == "US")?
												(sMonth+"/"+sDay+"/"+sYear):
												(sDay+"/"+sMonth+"/"+sYear);
		}else{
			sReturnDate = (sDateStyle == "US")?
												(aMonths[parseInt(sMonth)-1].substr(0,3) + " " + sDay + " " + sYear):
												(sDay + " " + aMonths[parseInt(sMonth)-1].substr(0,3) + " " + sYear);
		}
		if(bReturnValue) return sReturnDate;
		eDate.value = sReturnDate;
	}else{
		alert(msg);
		setupFocus(eDate);
	}
	return retValue;
}
// ----------------------------
//	Check to and from dates
//		to date must be greater than from date
// ----------------------------
function doDateCheck(from, to) {
	// Retreive extra[optional] params
	var args = doDateCheck.arguments;
	var sFromName = (args.length>2)?"'"+args[2]+"'":"'From Date'";
	var sToName = (args.length>3)?"'"+args[3]+"'":"'To Date'";
	var bMand = (args.length>4)?args[4]:false;
	var bReturnValue = (args.length>5)?args[5]:false;
	
	var retValue = true;
	var iCount = 0;
	var msg = "The following errors occured for date range:\n";
	var aFrom = from.value.split("/");
	var aTo = to.value.split("/");
	var obj = null; // Element to pass back to calling method
	// return true if not mandatory and both fields are empty
	if(!bMand && trim(to.value)=="" && trim(from.value)=="") return true;
	// return true if not mandatory and to field is empty
	if(!bMand && trim(to.value)=="") return true;
	
	if (aTo.length!=3){
		msg += (++iCount) + ". " + sToName + " is not in a valid format.\n";
		obj = to;
		retValue = false;
	}
	if(aFrom.length!=3){
		msg += (++iCount) + ". " + sFromName + " is not in a valid format.\n";
		obj = from;
		retValue = false;
	}
	if (Date.parse(new Date(aFrom[2],aFrom[1],aFrom[0]).toGMTString()) <= Date.parse(new Date(aTo[2],aTo[1],aTo[0]).toGMTString())) {
		retValue = true;
	}	else {
		if (from.value == "" || to.value == "") {
			msg += (++iCount) + ". Both dates must be entered.\n";
			obj = from;
			retValue = false;
		}else{
			msg += (++iCount) + ". " + sToName + " date must occur after the " + sFromName + " date.\n";
			obj = to;
			retValue = false;
		}
	}
	if(bReturnValue){ // return the msg back to caller
		return (retValue) ? "" : {msg:msg,obj:obj} ;
	}
	if(!retValue){
		alert(msg.replace(/\n$/,""));
		if(obj)
			obj.focus();
	}
	return retValue;
}
// ----------------------------
//	Check time entered into element
//	and format time to hh:mm
// ----------------------------
function checktime(item) {
	var args = checktime.arguments;
	var name = (args.length>1)?args[1]:"Time";
	var bReturnValue = (args.length>2)?args[2]:false;
	var timePat = /^(\d{1,2})(\/|-|\s|\.|:)(\d{1,2})$/;
	var sTime = item.value;
	var aMatch = sTime.match(timePat); // is the format ok?
	var sTmp, sYear, sMonth, sDay, sReturnDate = "";
	var sep = ":";
	var msg = "The following errors occured in the '" + name + "' field:\n";
	var iMsg = 0;
	var retValue = true;
	var empty = false;
	if (trim(sTime)=="") return false;
	if (aMatch == null) {
		msg += (++iMsg) + ". Time is not in a valid format\n";
		empty = true;
		retValue = false;
	}else{
		// parse time into variables
		sHH = aMatch[1];
		sMM = aMatch[3];
	}
	if(trim(sTime).length>0 && !isNaN(sTime)){
		var len = trim(sTime).length;
		if(len<=2){
			sHH = sTime;
			sMM = "00";
		}
		if(len>2){
			sHH = sTime.substring(0,2);
			sMM = sTime.substring(2,4);
		}
		retValue = true;
	}
	if ((typeof(sHH)!="undefined") && sHH.length < 2) {
		sTmp = "00"+sHH;
		sHH = sTmp.substring(sTmp.length-2);
	}
	if ((typeof(sMM)!="undefined") && sMM.length < 2) {
		sTmp = "00"+sMM;
		sMM = sTmp.substring(sTmp.length-2);
	}
	
	if((typeof(sHH)!="undefined") && parseInt(sHH)>23){
		msg += (++iMsg) + ". Hours part cannot exceed 23 hrs\n";
		retValue = false;
	}
	
	if((typeof(sMM)!="undefined") && parseInt(sMM)>59){
		msg += (++iMsg) + ". Minutes part cannot exceed 59 min\n";
		retValue = false;
	}
	
	if(retValue){
		sReturnTime = sHH + sep + sMM;
		if(bReturnValue) return sReturnTime;
		item.value = sReturnTime;
	}else{
		if(empty) item.value = "";
		alert(msg);
		setupFocus(item);
	}
	return retValue;
}
// ----------------------------
//	Convert digit{1} to digit{2}
// ----------------------------
function dtFixEl(i){
	var s = ""+i;
	return (s.length>1)?((s.length>2)?s.substring(0,2):s):"0"+s;
}
// ----------------------------
//	Validates a number entered in a field
// ----------------------------
function validateNumber(eField,iLength,sLabelName) {
	var args = validateNumber.arguments;
	var bError = false;
	if(args.length>3 && args[3])
		bMand = true;
	else
		if(trim(eField.value)=="") return true;
	if (isNaN(eField.value)) {
		bError = true;
		alert ("Please enter a valid number");
	}else{
		if(args.length>1){
			if(eField.value.length!=iLength){
				bError = true;
				sLabelName = (args.length>2)?sLabelName:"The field " + eField.name;
				alert(sLabelName + " requires a " + iLength + " digit number");
			}
		}
	}
	
	if(bError){
		setupFocus(eField);
	}
	return !bError;
}
// ----------------------------
//	Uses timeout to set the focus to a given field
//	This is to get over the tab key bug
// ----------------------------
function setupFocus(item){
	// set a timeout for tab key bug
	var args = setupFocus.arguments;
	var bSelect = (args.length>1)?args[1]:false;
	if(item != null && typeof(item) == "object")
		setTimeout("setFocusToField(document.forms['" + item.form.name + "'].elements['" + item.name + "']," + bSelect + ");",10);
}
function setFocusToField(item,bSelect){
	if(item != null && typeof(item) == "object"){
		if(bSelect) item.select();
		item.focus();
	}
}
// ----------------------------
//	Validates a given fields value[item.value] against a given RegExp[pat]
// ----------------------------
function checkBadCharacters(item,pat){
	//*******************************
	// BEGIN::Special Character Matching
	// Author: Vern - 20030203
	// Parameters:
	//		item	= the form element to check
	//		pat		= re pattern. e.g. "\||:|;|a|z|~"; This function will 
	//		return false if any of the characters in this list are present in the items value
	//
	// Escape the following Special Characters by a preceeding backslash (\)
	// Special Characters:
	//		$()*+.[?\^{|
	//		To match one of these characters itself, use \c (where c=Special Character)
	
	// Create match pattern
	if(pat.replace(/\s/g,"")=="") return true;
	var re = new RegExp("[" + pat + "]","gi");
	// use the String objects match to return an array
	var aMatch = item.value.match(re);
	var sMsg = "";
	if(aMatch!=null && typeof(aMatch)=="object"){
		for(i=0;i<aMatch.length;i++) sMsg += (sMsg.indexOf(aMatch[i])==-1)?"\"" + aMatch[i] + "\", ":"";
		alert("The following illegal characters were found in the Subject field:\n"+ sMsg +"\nPlease try again.");
		setupFocus(item);
		return false;
	}
	return true;
	// END::Special character match
	//*****************************
}


//******************************
// BEGIN::Character Length Check
// Checks the length of a given field
// Returns boolean
// Suppresses key entry after maxLength has been reached
//
function checkMaxLength (item, evt, maxLength){
	if (item.selected && evt.shiftKey) return true;
	var allowKey = false;
	if (item.selected && item.selectedLength > 0)
		allowKey = true;
	else {
		var keyCode = document.all ? evt.keyCode : evt.which;
		if (keyCode < 32 && keyCode != 13)
			allowKey = true;
		else
			allowKey = (item.value.length < maxLength) && (keyCode != 13);
	}
	item.selected = false;
	return allowKey;
}

// checkLength
// Truncate the value to the max length
// USAGE:checkLength(item(form element),name[label of element][,maxLength(number)][,nolinebreaks(boolean)][,returnMsgNoAlert(boolean)]);
//
function checkLength(item, name){
	var args = checkLength.arguments;
	var maxLength = (args.length>2) ? args[2] : 80 ;
	var noLineBreaks = (args.length>3) ? args[3] : false ;
	var bReturnValue = (args.length>4) ? args[4] : false ;
	var valOut = valIn = item.value;
	var msg = "The following errors occured for the field " + name + ":\n";
	var retValue = true;
	var iCount = 0;
	if(noLineBreaks){
		if(valIn.match(/\n|\r/g)){
			msg += (++iCount) + ". Linebreaks were detected. All linebreaks will be removed\n";
			valOut = valOut.replace(/\n|\r/g,"");
			retValue = false;
		}
	}
 	if (valIn.length > maxLength){
		msg += (++iCount) + ". Max Length exceeded, anything after " + maxLength + " characters will be truncated\n";
		valOut = valOut.substring(0, maxLength);
		retValue = false;
	}
	if(bReturnValue){ // return the msg back to caller
		return (retValue) ? "" : {msg:msg,value:valOut} ;
	}
	if(!retValue){
		alert(msg);
		item.value = valOut;
		setupFocus(item);
	}
	return retValue;
}

// END::Character Length Check
//******************************


// ----------------------------
//	Returns a string without white space
// ----------------------------
function trim(s){return s.replace(/\s/g,"");}

// ----------------------------
//	Clears all elements values
// ----------------------------
function clearForm(item){
	var form = item.form;
	form.reset();
	for(var i=0;i<form.length;i++) {
		var type = form[i].type.toLowerCase();
		switch(type){
			case "radio"	: form[i].checked=false;break;
			case "checkbox"	: form[i].checked=false;break;
			case "select-one":form[i].options[0].selected=true;break;
			default:if(type!="button"&&type!="hidden"&&type!="submit"&&type!="reset") form[i].value="";break;
		}
	}
}
/*****************************************
*		Validate Fields from list
**	List = "sFieldName[,sFieldName2[,n..]];validateType[,param1[,param2[,n..]]];sFriendlyName";
***	validateTypes: 
			empty = empty field
			alertonly = no focus
			number = check number
			email = email address
			length = check length of a number
			date = check date
			daterange = check to and from dates
******************************************/
function validateFields(form,sFieldsList){
	var args=validateFields.arguments;	
	if (sFieldsList != ""){
		var pval = sFieldsList.split(";");
		var obj = null;
		var validate = null;
		var i = j = 0;
		var oStrings =	(args.length>2&&!args[2]) ? {empty:"",alertonly:"",number:"",email:""}:
										{empty:"Please enter a value into the ",alertonly:"Please enter/select a value into/from the ",number:"Please enter a valid number into the ",email:"Please enter a valid email address into the "};
		var sFieldName = "selected ";
		var bFocus = true;
		var bAlert = true;
		var retValue = true;
		var msg = "";
		var a,o,aNames;
		for(i=0; i<pval.length; i+=3){
			a = pval[i].split(",");
			obj = form.elements[a[0]];
			validate = pval[i+1].split(",");
			if(pval[i+2]!="") sFieldName = "'" + pval[i+2] + "'";
			switch(validate[0]){
				case "empty":
					if(trim(obj.value).length==0){
						msg += oStrings.empty + sFieldName + " field\n";
						retValue = false;
    			}
					break;
				case "alertonly":
	    		if(trim(obj.value).length==0){
						msg += oStrings.alertonly + sFieldName + " field\n";
						retValue = false;
						bFocus = false;
	    		}
					break;
				case "number":
					if((trim(obj.value).length==0) || isNaN(obj.value)){
						msg += oStrings.number + sFieldName + " field\n";
						retValue = false;
					}
					break;
				case "email":  
					if (!isEmailAddress (trim(obj.value))){
						msg += oStrings.email + sFieldName + " field\n";
						retValue = false;
					}
					break;
				case "length":
					o = checkLength( obj, sFieldName, validate[1], ((validate[2]=="true")?true:false), true );
					if(	typeof(o)=="object" ){
						msg += o.msg;
						if(validate[3]=="true") obj.value = o.value;
						retValue = true;
					}
					if(validate.length>4&&validate[4]=="empty"){
						if(trim(obj.value).length==0){
							msg += oStrings.empty + sFieldName + " field\n";
							retValue = false;
	    			}
					}
					break;
				case "date":
					retValue = checkDate(obj);
					bAlert = retValue;
					break;
				case "daterange":
					a = pval[i].split(",");
					o = doDateCheck( form.elements[a[0]], form.elements[a[1]], validate[1], validate[2], true, true );
					if(	typeof(o)=="object" ){
						msg += o.msg;
						obj = o.obj;
						retValue = false;
					}
					break;
			}
			if(!retValue) break;
		}
	}
	if(!retValue){
		if(bAlert) alert(msg.replace(/\n$/,""));
		if(bFocus)
			obj.focus();
	}else if(args.length>3) execute(args[3]);
	return retValue;
}

// Execute function evaluates
//
function execute(s){
	try{
		eval(s);
	}catch(e){
		var caller = "" + execute.caller;
		caller = caller.replace(/\s/g,"").replace(/function([a-zA-Z0-9]+)\((.*)/,"function $1()");
		alert("ERROR:\nAn error occured while executing the script block:\n- "+ s +"\nThe calling method is:\n- "+ caller );
	}
}

	//toggle row control for tables
	// USE:hides all rows but caption, as in <CAPTION>sCaption</CAPTION>
	function toggleRows(e){
		while(e.nodeName.toLowerCase()!="caption"){
			e = e.parentNode;
		}
		while(e.nodeName.toLowerCase()!="table"){
			e = e.parentNode;
		}
		if(e.rows.length){
			for(i=0;i<e.rows.length;i++){
				e.rows[i].style.display = (e.rows[i].style.display=="none")?"block":"none";
			}
			if(e.caption!="") {
				if(e.rows[0].style.display=="none")
					e.caption.style.cssText = "background:#b4b4b4;color:white;";
				else
					e.caption.style.cssText = "background:buttonface;";
			}
		}
	}
	//******************************
	// BEGIN::executeAnchor
	//	Used on a table rows click event to trigger 
	//	the loading of the href of the first anchor tag within the row
	//
	function executeAnchor(evt){
		var target = typeof(evt.target)!="undefined" ? evt.target : event.srcElement;
		var a=e=null;
		if(target.nodeName.toLowerCase()=="input") return;
		e=getParentByNodeName(target,"TR");
		try{
			if(e.nodeName=="TR"){
				if((a=e.getElementsByTagName('A').item(0))!=null)
					location.href = a.href;
			}
		}catch(e){}
	}
	function getParentByNodeName(eTarget,sName){
		var el = eTarget;
		try{
			while(el.nodeName!=sName){
				if(el.nodeName=="BODY"){el=null;break;}
				el=el.parentNode;
			}
		}catch(e){}
		return el;
	}
	// END::executeAnchor
	//******************************
// -->
