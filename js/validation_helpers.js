

// Global values
var messageString, errorFlag


/*
|--------------------------------------
| checkNumeric
|--------------------------------------
| Parameters: 
|           :- strString: string value to check if numeric
|           :- errorMessage: error message to append to the global message string
|--------------------------------------
*/
function checkNumeric(strString, errorMessage)
{
   var strValidChars = "0123456789.-";
   var strChar;
   var blnResult = true;

   //  test strString consists of valid characters listed above
   for (i = 0; i < strString.length && blnResult == true; i++)
   {
      strChar = strString.charAt(i);
      if (strValidChars.indexOf(strChar) == -1)
      {
         blnResult = false; 
      }
   }

   if (blnResult == false || strString.length == 0)
   {
       errorFlag = false;
	   messageString += errorMessage;		
   }
}


/*
|--------------------------------------
| checkRequiredField
|--------------------------------------
| Parameters: 
|           :- fieldValue: string value to check
|           :- fieldLength: minimum allowable length of the string
|           :- errorMessage: error message to append to the global message string
|--------------------------------------
*/
function checkRequiredField(fieldValue, fieldLength, errorMessage)
{
   if (fieldValue.length < fieldLength)
   {
      errorFlag = false;
      messageString += errorMessage;
   }
}

function checkFilePath(fieldValue, fieldLength, errorMessage)
{
	if(fieldValue.charAt(0) != '\\' || fieldValue.charAt(1) != '\\')
   {
      errorFlag = false;
      messageString += errorMessage;
   }
}