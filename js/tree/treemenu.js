var aMenus = new Array();
var controlWindow = null;
var controlWindowName = "treeControl";
// MenuSystem
var vcoMenuSystemCollection=new Array(),vcoMenuSystemIndex=0;
function MenuSystem(id,sArrayName,oMenuImages,vContainer,iWidth,btOptions){
	var args = MenuSystem.arguments;
	aMenus[1]="#ccc";
	aMenus[2]="#333";
	// System Name
	this.SystemIndex = vcoMenuSystemIndex;
	this.SystemName = "vcoMenuSystemCollection";
	this.SystemArrayName = this.SystemName + "[" + this.SystemIndex + "]";
	
	this.pos = (args.length>6)?args[6]:0;
	this.menus = new Array();
	this.x = this.pos.x;
	this.y = this.pos.y;
	this.offsetX = -15;
	this.offsetY = 3;
	this.w = iWidth;
	this.name = this.SystemName + id;
	this.id = id;
	this.aRef = eval(sArrayName);
	this.images = oMenuImages;
	this.count = 0;
	this.floating = (btOptions & 1) //== 0 ? 0 : 1 ;
	this.menuTimeout = 500;
	this.config = {
		contextMenuOn:false,
		contextMenuFunction:""
	}
	this.constants = {
		MENU_:"MENU_",
		_IMG_:"_IMG_",
		_CONTAINER_:"_CONTAINER_",
		menu_array_all:false
	}
	
	// get container
	this.container = (typeof(vContainer)=="object") ? vContainer : ((getElement(vContainer)!=null)?getElement(vContainer):createContainer(vContainer));
	
	this.HTML = "";
	
	this.build = function(){
		this.construct(this.aRef,0);
		this.display();
	}
	this.refresh = function(id,a){
		this.HTML = "";
		this.construct(a,0);
		this.get_container(id).innerHTML = this.HTML;
	}
	this.get_container=function(id){
			try{
				return getElement(this.constants.MENU_ + this.id + this.constants._CONTAINER_ + id);
			}catch(e){
				log_errors()
			}
	}
	this.construct = function(a,iShift){
		var i=0;
		iShift++;
		for(i=0;i<a.length;i++){
			this.count++;
			this.OpenMenu(a[i],this.count);
			if(this.constants.menu_array_all && a[i].menu) {
				this.construct(a[i].menu,iShift);
			}
			this.CloseMenu();
		}
	}
	this.OpenMenu = function(oItem,iItemID){
		var id = oItem.id?oItem.id:iItemID;
		var pid = oItem.pid?oItem.pid:"_"+id;
		var loc = this.images.path;
		var ico = oItem.icon ?  oItem.icon : 0 ;
		var type = oItem.type;
		var img = loc + (oItem.menu>0 ? this.images.icons[ico][1]  : this.images.icons[ico][0]);
		var style = "padding-bottom:2px;cursor:default;font:11px verdana;color:#000;width:100% !important;";
		var sContStyle = "margin-left:16px;" ;
		var mOver = this.SystemArrayName + ".MenuSystemOver(this)";
		var mOut = this.SystemArrayName + ".MenuSystemOut(this);";
		var mClick = this.SystemArrayName + ".MenuSystemClick(this);";
		var mMenu = oItem.menu>0 ? 1 : 0 ;
		//alert(oItem.menu+":"+mMenu);
		if(oItem.seperator==1){
			this.HTML +="<div id=sep style=\"" + style + "height:2px;border:none;margin:0px;\"><hr>";
		}else{
			var contextMenu = this.config.contextOn ? "oncontextmenu=\""+ this.config.contextMenuFunction +"\" ":"";
			this.HTML	+="<div id=\"" + id + "\" " +
									"MENUID=\"" + this.id + "\" " +
									"type=\"" + type + "\" " +
									"style=\"" + style + "\" " +
									"menu=\"" + mMenu + "\" " +
									"icon=\"" + ico + "\" " +
									contextMenu +
									" title=\"ID:"+id+" - URL:"+type+".php?id="+id+"\"><nobr>" + 
									"<img " +
									"onclick=\"" + mClick + "\" id=\""+this.constants.MENU_ + this.id + this.constants._IMG_ + id + "\" src=\"" + img + "\" style=\"width:32px;height:16px;margin-right:5px;\" align=\"absmiddle\">" +
									"<span folder=\""+mMenu+"\" pid=\""+pid+"\" node_id=\""+id+"\" type=\""+type+"\" "+
									"onmouseover=\"" + mOver + "\" " +
									"onmouseout=\"" + mOut + "\" " +
									"onclick=\"javascript:"+ mClick +";"+ oItem.func +";\">" +
									this.encode_nbsp(oItem.desc) + "</span></nobr>" +
								"</div>" +
								"<div id=\""+this.constants.MENU_ + this.id + this.constants._CONTAINER_ + id + "\" " +
									"MENUID=\"" + this.id + "\" " +
									"style=\"display:none;" + sContStyle + "\">";
		}
	}
	this.CloseMenu = function(){
		this.HTML +="</div>";
	}
	this.MenuItem = function(oItem,id){
		
	}
	this.encode_nbsp = function(s){
		return s.replace(/\s/g,"&nbsp;");
	}
	this.display = function(){
		this.container.innerHTML += this.HTML;
	}
		
	this.MenuSystemClick = function(item){
		if(item.nodeName=="IMG")
		{
			var el = item.parentNode.parentNode;
			var id = el.getAttribute("id").substring(el.getAttribute("id").lastIndexOf("_")+1);
			var mID = el.getAttribute("MENUID");
			var oCont = getElement(this.constants.MENU_ + mID + this.constants._CONTAINER_ + id);
			var oImg = getElement(this.constants.MENU_ + mID + this.constants._IMG_ + id);
			var aImgs = this.images.icons;
			var idx = parseInt(el.getAttribute("icon"));
			if(el.getAttribute("menu")=="1"){
				oImg.src = this.images.path+((oCont.style.display == "none") ? aImgs[idx][2] : aImgs[idx][1] );
				if(oCont.firstChild==null){// || (oCont.firstChild!=null && parseInt(oCont.firstChild.getAttributes("loaded"))!=0)){
					oCont.innerHTML='<img loaded="0" src="'+this.images.path+'loading.gif" style="margin-left:16px;">';
					window.action(_base_url+"actions.php?pid="+id);
				}
			}
			oCont.style.display = (oCont.style.display == "none") ? "" : "none" ;
		}else{
			if(aMenus[0]) {
				aMenus[0].style.backgroundColor = "";
				aMenus[0].style.color = "#000";
			}
			aMenus[0] = item;
			aMenus[0].style.backgroundColor = aMenus[1];
			aMenus[0].style.color = aMenus[2];
		}
	}
	this.MenuSystemOver = function(item){
		if(aMenus[0] != item){
			item.style.backgroundColor='#9999CC';
			item.style.color='#FFFFCC';
		}
	}
	this.MenuSystemOut = function(item){
		if(aMenus[0] != item){
			item.style.backgroundColor='';
			item.style.color='#000';
		}
	}
	this.openNode = function(el){
		el.parentNode.style.display = 'block';
	}
	
	vcoMenuSystemCollection[vcoMenuSystemIndex++] = this;
	
	return this;
}

function go(){
	location.href = arguments[0];
}
function MenuSystemHideItem(item,iTimeOut){
	var on=parseInt(item.MENUON);
	dbg(item.id)
	if(!(on==1)){
		item.style.display = "none";
	}else setTimeout("MenuSystemHideItem(getElement('" + item.id + "'));",iTimeOut);
}
function getElement(id){
	return document.getElementById(id);
}
function createContainer(sID){
	var o=document.createElement("DIV");
	o.id = sID;
	document.body.appendChild(o);
	return getElement(sID);
}
//dbg
function alertMe(s){
	alert(s);return s;
}
function dbg(s){source.value += s + "\n";}