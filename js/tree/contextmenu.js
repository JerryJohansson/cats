// Context Menu for explorer tree in CMS mode
var DELETE_DISABLED_LIST="Menu|Tabs|";
var CHILD_CREATION_DISABLED_LIST="Number|Text|Combo|Date|DateTime|Radio|Checkbox|Helper|";
ContextMenu.prototype.getContextMenu = function (target) {
	var self = this;
	var tree = this.tree;
	var menu = [];

	var currentTarget = target;
	target = target.parentNode.parentNode;
	var type = target.getAttribute("type");
	function canCutCopy(tgt){
		return true;
	}
	function canPaste(tgt){
		return !ContextMenu.clipboard.isempty();
	}
	function canDelete(id){
		return !(id > 1);
	}
	function canMove(dir){
		if(dir=="up"){
			return !(target.previousSibling&&target.previousSibling.nodeName=="DIV");
		}else if(dir=="down"){
			return !(target.nextSibling.nextSibling&&target.nextSibling.nextSibling.nodeName=="DIV");
		}
	}
	menu.push(
		  [ "Edit Item...",
		    function() {
			    go(_base_public_url+"index.php?m=form&p=edit&a=edit&id="+target.id,_base_editor_frame);
		    }, 
		    "Edit currently selected item",
				_base_url+"images/m-edit.gif" ]);
	menu.push(null,
		  [ "Add Item...",
		    function() {
			    go(_base_public_url+"index.php?m=form&p=edit&a=add&pid="+target.id,_base_editor_frame);
		    },
		    "Add a new item to currently selected item",
				_base_url+"images/m-add.gif" ]);
	if(CHILD_CREATION_DISABLED_LIST.indexOf((type+"|"))==-1){
	menu.push(null,
			[ "Cut", function() { ContextMenu.clipboard.add({"Node":{"id":target.id,"action":"cut"}}); }, "Cut this item and place into the clipboard", _base_url+"images/ed_cut.gif", !canCutCopy(target) ],
			[ "Copy", function() { ContextMenu.clipboard.add({"Node":{"id":target.id,"action":"copy"}}); }, "Copy this item to the clipboard", _base_url+"images/ed_copy.gif", !canCutCopy(target) ],
			[ "Paste", function() { location.href=_base_public_url+"index.php?m=form&p=explore&a=paste&mode="+window.ContextMenu.clipboard.value["Node"].action+"&id_list="+window.ContextMenu.clipboard.value["Node"].id+"&id="+target.id; }, "Paste items from the clipboard into this item", _base_url+"images/ed_paste.gif", !canPaste(target) ]);
	}
	if(DELETE_DISABLED_LIST.indexOf((type+"|"))==-1){
	menu.push(null,
			[ "Delete",
		    function() {
					if(confirm("Are you sure you want to delete this page?\nWarning: Any children of this page will no longer be accessible!"))
						go(_base_public_url+"index.php?m=form&p=post&a=del&id="+target.id,_base_editor_frame);
		    },
		    "Delete selected item",
				_base_url+"images/ed_delete.gif", canDelete(target.id) ]);
	}
	menu.push(null,
			[ "Move to Top",
		    function() {
					var pid=(target.parentNode && target.parentNode.previousSibling)?target.parentNode.previousSibling.id:0;
					window.move_item("top",target.id,pid);
		    },
		    "Move selected item to the top of the branch",
				_base_url+"images/m-movetop.gif", canMove("up") ],
		  [ "Move Item Up",
		    function() {
					var pid=(target.parentNode && target.parentNode.previousSibling)?target.parentNode.previousSibling.id:0;
					window.move_item("up",target.id,pid);
		    },
		    "Move selected item up one space",
				_base_url+"images/m-moveup.gif", canMove("up") ],
			[ "Move Item Down",
		    function() {
					var pid=(target.parentNode && target.parentNode.previousSibling)?target.parentNode.previousSibling.id:0;
			    window.move_item("down",target.id,pid);
		    },
		    "Move selected item down one space",
				_base_url+"images/m-movedown.gif", canMove("down") ],
			[ "Move to Bottom",
		    function() {
					var pid=(target.parentNode && target.parentNode.previousSibling)?target.parentNode.previousSibling.id:0;
					window.move_item("bot",target.id,pid);
		    },
		    "Move selected item to the bottom of the branch",
				_base_url+"images/m-movebot.gif", canMove("down") ]
	);
	menu.push(null,
		  [ "Refresh Node",
		    function() {
					var q="";
					if(currentTarget.getAttribute("folder")=="1") q=target.id;
					else q=currentTarget.getAttribute("pid");
					window.action(_base_url+"actions.php?pid="+q);
		    },
		    "Refresh children items",
				_base_url+"images/m-refresh.gif"]);
	menu.push(null,
		  [ "Properties...",
		    function() {
					var s="\n<style>\nbody{font:11px verdana;}\ndiv,b{font-weight:bold;}</style><div><b>Base URL</b></div>"+_base_public_url+
						"\n<div><b>ID</b></div>"+currentTarget.getAttribute("node_id")+
						"\n<div><b>Title</b></div>"+currentTarget.innerHTML+
						"\n<div><b>URL</b></div>"+currentTarget.getAttribute("type")+".php?id="+target.id+
						"\n<div><b>Full URL</b></div>"+_base_public_url+currentTarget.getAttribute("type")+".php?id="+target.id;
					var win=window.open("","properties","width=400, height=200");
					win.document.open();
					win.document.write(s);
					win.document.close();
					win.focus();
		    },
		    "Display currently selected items extended attributes",
				_base_url+"images/m-properties.gif"]);
	
	return menu;
};