// RC stuff
var buttonElement = null;
var _base_editor_frame = "frmMain";
//var _base_url = "<!-- base_url -->";
function getTree(){
	return document.getElementById("MENU_1_CONTAINER_0");
}
function incontext(el,target){
	while(((el=el.parentNode)!=null) && el.nodeName != "BODY" ){
		if(arguments.length>2&&el.nodeName==arguments[2]) return el;
		if(el.id==target) return el;
	}
	return false;
}
function popupMenu(event) {
	var doc=getTree();
	buttonElement =document.all?event.srcElement:event.target;
	if(!incontext(buttonElement,"MENU_1_CONTAINER_0")) return;
	return ContextMenu.popupMenu(event);
	//return false;
}
function alertMe(s){alert(s);return s;}

/*
+-------------------------------------------
|		Context menu constructor and prototyping
+-------------------------------------------
*/
function ContextMenu(tree) {
	this.tree = tree;
	this.agt		= navigator.userAgent.toLowerCase();
	this.is_ie	= ((this.agt.indexOf("msie") != -1) && (this.agt.indexOf("opera") == -1));
};
// Menus clipboard stores copied objects
ContextMenu.prototype.clipboard = {
	"value":{},
	"add":function (val){this.value=val;},
	"isempty":function (){return !(typeof(this.value["Node"])=="object");}
};

ContextMenu._pluginInfo = {
	name          : "ContextMenu",
	version       : "1.0",
	developer     : "Vernon Laskey",
	developer_url : "http://www.ezwebmaker.com/",
	c_owner       : "ezwebmaker.com",
	sponsor       : "Buzmedia",
	sponsor_url   : "http://www.buzmedia.com.au",
	license       : "ContextMenu"
};

ContextMenu.prototype.popupMenu = function(ev) {
	var self = this;
	if (this.currentMenu)
		this.currentMenu.parentNode.removeChild(this.currentMenu);
	function getPos(el) {
		var r = { x: el.offsetLeft, y: el.offsetTop };
		if (el.offsetParent) {
			var tmp = getPos(el.offsetParent);
			r.x += tmp.x;
			r.y += tmp.y;
		}
		return r;
	};
	function documentClick(ev) {
		ev || (ev = window.event);
		if (!self.currentMenu) {
			alert("How did you get here? (Please report!)");
			return false;
		}
		var el = ContextMenu.is_ie ? ev.srcElement : ev.target;
		for (; el != null && el != self.currentMenu; el = el.parentNode);
		if (el == null)
			self.closeMenu();
	};
	self.closeMenu = function() {
		self.currentMenu.parentNode.removeChild(self.currentMenu);
		self.currentMenu = null;
		if (ContextMenu.is_ie)
			self.iePopup.hide();
	};
	
	var target = ContextMenu.is_ie ? ev.srcElement : ev.target;
	var ifpos = {x:0,y:0};//getPos(document);
	var x = ev.clientX + ifpos.x;
	var y = ev.clientY + ifpos.y;
	if(target.nodeName=="IMG") return true;
	
	var div;
	var doc;
	
	if (!ContextMenu.is_ie) {
		//alert(ContextMenu.is_ie);
		doc = document.getElementById("ezecmenu").contentWindow.document;
	} else {
		// IE stinks
		var popup = this.iePopup = window.createPopup();
		doc = popup.document;
		doc.open();
		doc.write("<html><head><style type='text/css'>@import url(js/tree/menu.css); html, body { padding: 0px; margin: 0px; overflow: hidden; border: 0px; }</style></head><body unselectable='yes'></body></html>");
		doc.close();
	}
	div = doc.createElement("div");
	cont = doc.createElement("div");
	if (ContextMenu.is_ie)
		div.unselectable = "on";
	div.oncontextmenu = function() { return false; };
	div.className = "htmlarea-context-menu";
	if (!ContextMenu.is_ie)
		div.style.left = div.style.top = "0px";

	cont.appendChild(div);
	doc.body.innerHTML = cont.innerHTML;
	div = doc.body.firstChild;
	
	var table = doc.createElement("table");
	div.appendChild(table);
	table.cellSpacing = 0;
	table.cellPadding = 0;
	var parent = doc.createElement("tbody");
	table.appendChild(parent);

	var options = this.getContextMenu(target);
	for (var i = 0; i < options.length; ++i) {
		var option = options[i];
		var item = doc.createElement("tr");
		parent.appendChild(item);
		if (ContextMenu.is_ie)
			item.unselectable = "on";
		else item.onmousedown = function(ev) {
			ContextMenu._stopEvent(ev);
			return false;
		};
		if (!option) {
			item.className = "separator";
			var td = doc.createElement("td");
			td.className = "icon";
			var IE_IS_A_FUCKING_SHIT = '>';
			if (ContextMenu.is_ie) {
				td.unselectable = "on";
				IE_IS_A_FUCKING_SHIT = " unselectable='on' style='height=1px'>&nbsp;";
			}
			td.innerHTML = "<div" + IE_IS_A_FUCKING_SHIT + "</div>";
			var td1 = td.cloneNode(true);
			td1.className = "label";
			item.appendChild(td);
			item.appendChild(td1);
		} else {
			var label = option[0];
			item.className = "item";
			//alert(option[3]);
			item.__msh = {
				item: item,
				label: label,
				action: option[1],
				tooltip: option[2] || null,
				icon: option[3] || null,
				disabled: option[4] || false,
				activate: function() {
					self.closeMenu();
					this.action();
				}
			};
			item.disabled = (option[4])?option[4]:false;
			label = label.replace(/_([a-zA-Z0-9])/, "<u>$1</u>");
			if (label != option[0])
				keys.push([ RegExp.$1, item ]);
			label = label.replace(/__/, "_");
			var td1 = doc.createElement("td");
			if (ContextMenu.is_ie)
				td1.unselectable = "on";
			item.appendChild(td1);
			td1.className = "icon";
			if (item.__msh.icon)
				td1.innerHTML = "<img align='middle' src='" + item.__msh.icon + "' />";
			var td2 = doc.createElement("td");
			if (ContextMenu.is_ie)
				td2.unselectable = "on";
			item.appendChild(td2);
			td2.className = "label";
			td2.innerHTML = label;
			item.onmouseover = function() {
				this.className += " hover";
				window.status=this.__msh.tooltip||"";
			};
			item.onmouseout = function() { this.className = "item";window.status=""; };
			item.oncontextmenu = function(ev) {
				this.__msh.activate();
				if (!ContextMenu.is_ie)
					ContextMenu._stopEvent(ev);
				return false;
			};
			item.onclick = function(ev) {
				//alert(ev.srcElement.id);
			};
			item.onmouseup = function(ev) {
				var timeStamp = (new Date()).getTime();
				if (timeStamp - self.timeStamp > 500)
					this.__msh.activate();
				if (!ContextMenu.is_ie)
					ContextMenu._stopEvent(ev);
				return false;
			};
			//if (typeof option[2] == "string")
			//item.title = option[2];
		}
	}
	buffer = document.getElementById("ezecbuffer").contentWindow.document;
	buffer.body.innerHTML = doc.body.innerHTML;
	w = buffer.body.firstChild.offsetWidth;
	h = buffer.body.firstChild.offsetHeight;
	if (ContextMenu.is_ie) {
		this.iePopup.show(ev.screenX, ev.screenY, w, h);
	}else{
		
		cmenu = document.getElementById('ezecmenu');
		cmenu.style.left = ev.clientX;
		cmenu.style.top = ev.clientY;
		cmenu.style.width = w+"px";
		cmenu.style.height = h+"px";
		if (cmenu.style.visibility == "hidden")
			cmenu.style.visibility="visible";
		else {
			cmenu.style.visibility="hidden";
		}
		//alert(cmenu.style.visibility+":"+ev.clientX+":"+ev.clientY)
	}
	
	this.currentMenu = (div);
	this.timeStamp = (new Date()).getTime();
	return false;
};

//ContextMenu = new ContextMenu("MENU_1_CONTAINER_0");
//--------------------------------------------------------------------------//
//ContextMenu.agt		= navigator.userAgent.toLowerCase();
//ContextMenu.is_ie	= ((ContextMenu.agt.indexOf("msie") != -1) && (ContextMenu.agt.indexOf("opera") == -1));
ContextMenu.prototype._addEvent = function(el, evname, func) {
    if (this.is_ie) {
        el.attachEvent("on" + evname, func);
    } else {
        el.addEventListener(evname, func, true);
    }
};

ContextMenu.prototype._addEvents = function(el, evs, func) {
    for (var i in evs) {
        this._addEvent(el, evs[i], func);
    }
};

ContextMenu.prototype._removeEvent = function(el, evname, func) {
    if (this.is_ie) {
        el.detachEvent("on" + evname, func);
    } else {
        el.removeEventListener(evname, func, true);
    }
};

ContextMenu.prototype._removeEvents = function(el, evs, func) {
    for (var i in evs) {
        this._removeEvent(el, evs[i], func);
    }
};

ContextMenu.prototype._stopEvent = function(ev) {
    if (this.is_ie) {
        ev.cancelBubble = true;
        ev.returnValue = false;
    } else {
        ev.preventDefault();
        ev.stopPropagation();
    }
};
//--------------------------------------------------------------------------//


var eze_IFRAME_idx=0;
function get_ezeIFRAME(name, file, show){
	var hide = (show)?"":"hidden";
	return '<iframe id="eze'+name+'" height="100" src="'+file+'" marginwidth="0" marginheight="0" border="0" frameborder="no" scrolling="no" style="visibility:'+hide+'; position: absolute;z-index:1000'+(eze_IFRAME_idx++)+';"></iframe>';
}
//document.write(get_ezeIFRAME("cmenu", _base_public_url+"js/tree/rc_menu.htm"));
//document.write(get_ezeIFRAME("cbuffer", _base_public_url+"js/tree/rc_menu.htm"));
//document.write(get_ezeIFRAME("remote", _base_public_url+"js/tree/rc_menu.htm"));
document.write(get_ezeIFRAME("cmenu", "js/tree/rc_menu.htm"));
document.write(get_ezeIFRAME("cbuffer", "js/tree/rc_menu.htm"));
document.write(get_ezeIFRAME("remote", "js/tree/rc_menu.htm"));
document.onmousedown=hidemenu;
function hidemenu(){
	document.getElementById('ezecmenu').style.visibility="hidden";
}
function move_item(dir,id,pid){
	pid=(isNaN(pid))?0:pid;
	if(isNaN(id)) {
		alert("Error: This action requires a valid ID\nThe ID passed in was "+id+"\nPlease contact your administrator for support");
		return false;
	}
	action(_base_url+"actions.php?action=move&dir="+dir+"&id="+id+"&pid="+pid);
}
function action(url){
	document.getElementById("ezeremote").contentWindow.location.href=url;
}
function go_rc(url){
	//void(open(url,_base_editor_frame));
	//alert(url)
	//top.frames[1].frames[1].location=url;
	parent.window.frames[_base_editor_frame].location.href = url;
}