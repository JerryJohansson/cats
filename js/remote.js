
// retrieve XML document (reusable generic function);
// parameter is URL string (relative or complete) to
// an .xml file whose Content-Type is a valid XML
// type, such as text/xml; XML source must be from
// same domain as HTML file
/***************************************************
	properties=
	{
		onreadystatechange:	'Event handler for an event that fires at every state change',
		readyState:	{Object_status_integer:[
		'0 = uninitialized',
		'1 = loading',
		'2 = loaded',
		'3 = interactive',
		'4 = complete']},
		responseText:	'String version of data returned from server process',
		responseXML:	'DOM-compatible document object of data returned from server process',
		status:	'Numeric code returned by server, such as 404 for "Not Found" or 200 for "OK"',
		statusText:	'String message accompanying the status code'
	}
	***********************************************/
	/***********************************************
	methods
	{
		abort:function(){//	Stops the current request
			this.response.abort();
		},
		getAllResponseHeaders: function(){//	Returns complete set of headers (labels and values) as a string
			this.request.getAllResponseHeaders();
		},
		getResponseHeader: function("headerLabel"){//	Returns the string value of a single header label
			this.request.getResponseHeader(name);
		},
		open: function("method", "URL"[, asyncFlag[, "userName"[, "password"]]]){//	Assigns destination URL, method, and other optional attributes of a pending request
			this.request.open("post",url,true);
		},
		send: function(content){//	Transmits the request, optionally with postable string or DOM object data
			this.request.send(content);
		},
		setRequestHeader: function("label", "value"){//	Assigns a label/value pair to the header to be sent with a request
			this.request.setRequestHeader(name,value);
		}
	}
	***************************************************/
	// global request and XML document objects
	
	//--------------------------------------------------------------------------
	var _XMLHttp_messages={
		readyState:['Waiting for response from the server','Connected: Transfering data...','Please wait while initialising','Complete','Remote load - '],
		errors:['There was an error while processing response']
	};
	
	//experiment
	var experimental=true;
	var ezw_XMLHttpRequests=new Object();
	var ezw_XMLHttpRequests_index=0;
	var ezw_XMLHttpResponses=new Object();
	//------------------------
	var ezw_XMLHttpRequest;
	var ezw_XMLHttpResponse;
	var ezw_XMLHttpResponseType;
	var ezw_XMLHttpRequestType;
	function _HTTPRequest() {
		var args=arguments;
		var moz=(window.XMLHttpRequest)?1:0;
		var sendObj=null;
		var async=true;
		ezw_XMLHttpRequest=(moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));
		//experiement
		var _ret=ezw_XMLHttpRequests_index;
		ezw_XMLHttpResponses[_ret]=new _HTTPResponse(args);
		eval("var r=(typeof(ezw_XMLHttpRequests_"+_ret+")!='undefined')?ezw_XMLHttpRequests_"+_ret+":null;");
		var url=args[0];
		var method=(args.length>1 && typeof(args[1])=="string")?args[1].toUpperCase():"POST";
		if(args.length>2 && typeof(args[2])=="function") ezw_XMLHttpResponse = args[2];
		if(args.length>3 && typeof(args[3])=="string"){
			sendObj=args[3];
			url=(url.indexOf(".php?")!=-1)?(url+args[3]):(url+"?"+args[3]);
		}
		if(args.length>4 && typeof(args[4])=="string"){ //
			ezw_XMLHttpResponseType=args[4];
		}
		if(args.length>5 && typeof(args[5])=="string"){
			ezw_XMLHttpRequestType=args[5];
			if(ezw_XMLHttpRequestType=="refresh"){
				//window.document._screen.show("loading");// show message box and disable screen by covering it with semi-transparent div
				top._show_message("Loading remote content...");
			}
		}
		if(args.length>6 && typeof(args[6])=="boolean"){
			async=args[6];
		}
		//dalert(experimental+":"+url+":"+async);
		//experimental=false;
		if(experimental){
			r.onreadystatechange = _HTTPResponseFunction(_ret);
			r.open(method, url, async);
			r.send(sendObj);
			return _ret;
		}else{
			ezw_XMLHttpRequest.onreadystatechange = _HTTPResponse;
			ezw_XMLHttpRequest.open(method, url, async);
			if(moz) ezw_XMLHttpRequest.send(sendObj);
			else ezw_XMLHttpRequest.send(sendObj);
			return ezw_XMLHttpRequest;
		}
	}
	function _HTTPRequestGet(name) {
		try{ezw_XMLHttpRequest.getResponseHeader(name);
		}catch(e){_error_message("_HTTPRequestGet:"+e);}
	}
	function _HTTPRequestGetAll() {
		try{return ezw_XMLHttpRequest.getAllResponseHeaders();
		}catch(e){_error_message("_HTTPRequestGetAll:"+e);}
	}
	function _HTTPResponseFunction(id){
		var s="o=function(){ezw_XMLHttpResponses["+id+"].response("+id+");}";
		var o=null;
		eval(s);
		if(typeof(o)=="function") return o;
		else return null;
	}
	function _HTTPResponse(args){
		var _ret=ezw_XMLHttpRequests_index++;
		this.id="ezw_XMLHttpRequests_"+_ret;
		this.moz=(window.XMLHttpRequest)?1:0;
		this.index=_ret;
		this.func=(args.length>2 && typeof(args[2])=="function")?args[2]:null;
		this.type=(args.length>4 && typeof(args[4])=="string")?args[4]:"";
		eval(this.id+'=(this.moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));var r='+this.id+';');
		//ezw_XMLHttpRequests[_ret]=(this.moz)?(new XMLHttpRequest()):(new ActiveXObject("Microsoft.XMLHTTP"));
		
		var res=function(id) {
			var Parent=ezw_XMLHttpResponses[id];
			eval('var r=ezw_XMLHttpRequests_'+id+';');
			eval("var m=function(s){var x=ezw_XMLHttpRequests_"+id+";var s=(typeof(s)!='undefined'&&s!='')?'::'+s:'';try{element('status_message').innerHTML=s;}catch(e){window.status=_XMLHttp_messages.readyState[x.readyState]+s;}}");
			var p=function(id) {
			// function to process the response
			// Returns the response to the callback function with object formated in set format
				var _return='';
				var Parent=ezw_XMLHttpResponses[id];
				eval('var r=ezw_XMLHttpRequests_'+id+';');
				switch(String(Parent.type).toLowerCase()){
					case 'xmlobject':
						_return=r.responseXML;
					case 'xml':
						_return=r.responseXML.documentElement;
						break;
					case 'text':
						_return=r.responseText;
					default:
						_return=r.responseText;
						break;
				}
				if(typeof(Parent.func)=="function"){
					Parent.func(_return);
				}else{
					_error_message("No function specified:"+_return);
				}
				//top._hide_message();
			};
			
			this.process=p;
			this.show_message=(m!=null)?m:_show_message;
			try{switch(r.readyState){
				case 0: case 1: case 2: case 3:
					this.show_message();
					break;
				case 4:
					switch(r.status){
						case 200:// do callback
							this.process(id);
							break;
						case 500:// server error
							alert(r.statusText);
							break;
						case 404:// url not found
							alert(r.statusText);
							break;
					}
					this.show_message(r.statusText);
					break;
				default:
					this.show_message(_XMLHttp_messages.errors[0]+"\n"+r.statusText);
					break;
			}}catch(e){_error_message("_HTTPResponse:"+e.description+"\n"+r);}
		};
		this.response=res;
		return this;
	}
	
/***************************************************************************
END _HTTPRequest Object
***************************************************************************/


//===================================================================================//
/*
+------------------------------------------------------------------------
|  BEGIN::Helper Object functions
+------------------------------------------------------------------------
*/
function _helper_show_employee(func,module){
	var a=arguments;
	var f=document.forms[0];
	var e=(f!=null)?f.elements:{}; 
	//create it once only
	//alert(typeof(top.HelperObjects[func]));
	//alert(top.HelperObjects[func])
	//if(typeof(top.HelperObjects[func])=='undefined')
	if(!top.HelperObjects[func])
		top.HelperObjects[func]=new Object();
	
	if(a.length>3){
		top.HelperObjects[func].emp_id=a[2];
		top.HelperObjects[func].emp_val=a[3];
	}
	
	if(a.length>5){
		top.HelperObjects[func].site_id=e[a[4]];
		top.HelperObjects[func].site_val=e[a[5]];
	}
	
	if(a.length>7){
		top.HelperObjects[func].dep_id=e[a[6]];
		top.HelperObjects[func].dep_val=e[a[7]];
	}
	
	if(a.length>9){
		top.HelperObjects[func].sec_id=e[a[8]];
		top.HelperObjects[func].sec_val=e[a[9]];
	}
	
	if(a.length>11){
		top.HelperObjects[func].pos_id=e[a[10]];
		top.HelperObjects[func].pos_val=e[a[11]];
	}
	top.helper_show_menu(func, module, null, e[a[3]]);
}
/*
+------------------------------------------------------------------------
|  END::Helper Object functions
+------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------
|  BEGIN:: Helper functions for remoting
+------------------------------------------------------------------------
*/
function get_site_department(itm,dept,sect){
	remote.get_options_add('get_departments_array','admin',itm.options[itm.selectedIndex].value,itm,dept);
}
function get_department_section(itm,sect){
	remote.get_options_add('get_sections_array','admin',itm.options[itm.selectedIndex].value,itm,sect);
}
function get_section_shift(itm,shif){
	remote.get_options_add('get_shifts_array','admin',itm.options[itm.selectedIndex].value,itm,shif);
}


function get_config_value_control(itm){
	var e = itm.form.elements;
	var els = e['CONFIGURATION_KEY[]'];
	var i=0;
	for(i=0;i<els.length;i++) if(itm==els[i]) break;
	var cfg = e['CONFIGURATION_VALUE[]'][i];
	var val = '';
	
	switch(cfg.nodeName){
		case 'SELECT':
			val = cfg.options[cfg.selectedIndex].value
			break;
		default:
			val = cfg.value;
			break;
	}
	var obj={
		'a':'get_config_control',
		'p':itm.options[itm.selectedIndex].value,
		'id':val
	};
	var e = remote.get_sync('admin',obj);
	//alert(cfg.parentNode)
	if(e!=null && e.html!='')
		cfg.parentNode.innerHTML = e.html;
}
function load_config(img,idx){
	if(img.getAttribute('loaded')==null){
		img.setAttribute('loaded','true');
		var e=document.forms[0].elements;
		var cfg = e['CONFIGURATION_KEY[]'][idx];
		var val = img.getAttribute('value');
		var p = cfg.options[cfg.selectedIndex].value;
		var a = 'get_config_control';
		if(empty(p)) return;
		remote.get_html(a+"_"+idx,img.parentNode,a,'admin',val,p);
	}
}
/*
+------------------------------------------------------------------------
|  END:: Helper functions for remoting
+------------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------
|  BEGIN::Remote Object
+------------------------------------------------------------------------
*/
function _remote(){
	this.module='';
	this.set_options = function(return_obj){
		try{
			dalert(return_obj);
			eval('var e='+return_obj);
			dalert(e);
			var name = e.name;
			
			if(this.options_to_element[name]){
				_options_replace(this.options_to_element[name],e.options);
			}
		}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\nRetrun Value:\n"+return_obj);}
	}

	this.set_options_add = function(return_obj){
		try{
			eval('var e='+return_obj);
			var name = e.name;
			//alert(return_obj)
			if(remote.options[name].to_element){
				var id = (e.id)?e.id:null;
				var top_options_name = remote.options[name]._params['m']+"__"+remote.options[name]._params['a'];
				if(typeof(top.gui.options[top_options_name])!="object") {
					top.gui.options.add(top_options_name);
				}
				var selected_value=top.gui.options[top_options_name].replace(name, remote.options[name].to_element,e.options);
				//remote.filter(name,remote.options[name]._params['p'],remote.options[name].to_element);//, remote.options[name]._params['relative']);
			}
		}catch(e){
			window.open("","test").document.write(return_obj);
			_error_message(e+"\n"+e.number+"\n"+e.description+"\nset_options_add.Retrun Value:\n"+return_obj);
		}
	}
	
	this.get_options_add = function(func,module,id){
		var el=(typeof(arguments[3])=="string")?{'name':'_no_name_','value':arguments[3],'type':'text'}:arguments[3];
		var el_to=(typeof(arguments[4])=="string")?{'name':'_no_name_','value':arguments[4],'type':'text'}:arguments[4];
		var name=el.name;
		var action_func=(typeof(func)!="undefined") ? func : '';
		var el_value=(typeof(arguments[5])!="undefined") ? arguments[5] : ((el.type.match(/(text|textarea|hidden)/gi))?el.value:el.options[el.selectedIndex].value) ; 
		this.get_options(name,el,el_to,action_func,module,id,el_value);
	}
	
	this.get_options = function(name_obj,el,el_to,action_func,module,id,el_value){
		//caller_name = call.replace(/\s/g,"").replace(/function([a-zA-Z0-9]+)\((.*)/,"function $1()");
		dalert(this.get_options.caller);
		if(typeof(name_obj)=="object"){
			name = name_obj.name;
		}else{
			name = name_obj;
		}
		//name = module+"__"+action_func;
		// arguments:
		// 	el:element holding value
		// 	el_to: element where options are to be added
		// 	action_func: action of the module
		// 	module: module where action exists
		// 	id: do id lookup if this argument exists
		// 	el_value: set this to override value
		
		// create options object if it doesn't already exist
		if(typeof(remote.options[name])!="object") remote.options[name] = new Object();
		
		remote.options[name].from_element=el;
		remote.options[name].to_element=el_to;
		remote.options[name].selected_id=id;
		//alert(el_to.name);
		var obj={
			'a':action_func,
			'm':module,
			'n':name,
			'p':el_value,
			'id':id,
			'rm':this.module
		};
		var url = cats_get_path(CATS_REMOTE_PATH,module);
		var q = this._query(obj);
		remote.options[name]._query = q;
		remote.options[name]._params = obj;
		remote.options[name]._url = url;
		remote.options[name]._xml = _HTTPRequest(url,"post",this.set_options_add,q);//,null,null,true);
		
	}
	this.get_html = function(name_obj,el_to,action_func,module,id,el_value){
		dalert(this.get_options.caller);
		if(typeof(name_obj)=="object"){
			name = name_obj.name;
		}else{
			name = name_obj;
		}
		// create options object if it doesn't already exist
		if(typeof(remote.options[name])!="object") remote.options[name] = new Object();
		
		remote.options[name].to_element=el_to;
		//alert(el_to.name);
		var obj={
			'a':action_func,
			'm':module,
			'n':name,
			'p':el_value,
			'id':id
		};
		var url = cats_get_path(CATS_REMOTE_PATH,module);
		var q = this._query(obj);
		//alert(url+"?"+q)
		remote.options[name]._query = q;
		remote.options[name]._params = obj;
		remote.options[name]._url = url;
		remote.options[name]._xml = _HTTPRequest(url,"post",this.set_html,q);//,null,null,true);
		
	}
	this.set_html = function(return_obj){
		try{
			dalert(return_obj);
			eval('var e='+return_obj);
			dalert(e);
			var name = e.name;
			
			if(remote.options[name].to_element){
				remote.options[name].to_element.innerHTML = e.html;
			}
		}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\nRetrun Value:\n"+return_obj);}
	}
	this.get_sync = function(module, send_obj){
		try{
			var url = cats_get_path(CATS_REMOTE_PATH,module);
			var q = this._query(send_obj);
			var moz=(window.XMLHttpRequest)?1:0;
			var r = (moz)?new XMLHttpRequest():new ActiveXObject("MSXML2.XMLHTTP.3.0");
			r.open("GET", url+"?"+q, false, null, null);
			r.send(q);
			//alert(r.responseText+":"+q)
			eval("var o="+r.responseText);
		}catch(e){alert(e+"\n"+r.responseText);}
		return o;
	}
	this._query = function (o){
		var s=this.parse_object(o);
		if(s!='') s=s.replace(/&$/,"");
		return s;
	}
	this.parse_array = function(a){
		var s="";
		var i=0;
		if(typeof(a)=="object"){
			for(i=0;i<a.length;i++)
				if(typeof(a[i][1])=="object" && (!a[i][1].nodeName) && a[i][1][0]){s+=this.parse_array(a[i][1],a[i][0]);}
				else if(args.length>1) s+=""+args[1]+"["+a[i][0]+"]="+escape(a[i][1])+"&";
				else s+=""+a[i][0]+"="+escape(a[i][1])+"&";
		}
		return s;
	}
	this.parse_object = function(o){
		var args=arguments;
		var s="";
		var i=null;
		if(typeof(o)=="object"){
			for(i in o){
				if(typeof(o[i])=="object" && (!o[i].nodeName)){s+=this.parse_object(o[i],i);}
				else if(args.length>1) s+=""+args[1]+"["+i+"]="+escape(o[i])+"&";
				else s+=""+i+"="+escape(o[i])+"&";
			}
		}
		return s.replace(/&$/,"");
	}
	
	this.filter = function (name, filter, list){
		if(filter=='') return;
		if(!this.filter_options(filter, list)){
			dalert("Inside filter:name="+name+":filter="+filter);
			this.get_options_add(remote.options[name]._params['a'], remote.options[name]._params['m'], remote.options[name]._params['id'], list, remote.options[name].to_element);
		}
	}
	this.filter_options = function (filter, list){
		var s = (typeof(filter)=="string")?filter:((filter.type) && (filter.type.match(/(text|textarea|hidden)/gi))?filter.value:filter.options[filter.selectedIndex].value);
		var _ret=false;
		if(s != ""){
			if(typeof(list)=="string") list=element(list);
			var x = list.options.length;
			s = s.toLowerCase();
			var ilen = s.length;
			var sdat = s.substring(0,ilen);
			var val,dat;
			for(i=0;i<x; i++){
				val = list.options[i].text;
				dat = val.substring(0,ilen);
				dat = dat.toLowerCase();
				if(s == dat){
					list.options[i].selected = true; 
					_ret=true;
					break;
				}
			}
			if(_ret) list.focus();
		}
		return _ret;
	}
}
var remote = new _remote();
remote.options=new Object();
/*
+------------------------------------------------------------------------
|  END::Remote Object
+------------------------------------------------------------------------
*/

function _cats_get_path(path,page){
	if(path)
		return path+page+".php";
	else
		return CATS_REMOTE_PATH+page+".php";
}
if(typeof(cats_get_path)!="function"){
	cats_get_path = _cats_get_path;
}

// dbg function
var gDEBUG_ON=false;
function dalert(s){
	if(gDEBUG_ON && gDEBUG_ON==true) alert(s);
}

function _error_message(s){
	alert("Error Message: "+s);
	// TODO: Create nice window for error messages
}
