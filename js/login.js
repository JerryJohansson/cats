<!--
//================================================================================//
//	XML Requests/Responses
//================================================================================//
function alertMe(s){alert(s);return s;}
var oXML=oXML2=null;
function cats_get_path(path,module){
	return path+module+".php";
}
function _login(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	var s="";
	var i=0;
	for(i=0;i<f.length;i++){
		s+=""+f[i].name+"="+escape(f[i].value)+"&";
	}
	//alert(s+"\n"+url)
	oXML=_HTTPRequest(url,"post",return_login,s.replace(/&$/,""));
	for(i=0;i<f.length;i++){
		f[i].disabled=true;
	}
	//oXML2=_HTTPRequest(url,"post",return_window,"action=dash");
	return false;
}
var CATS_MAIN_WINDOW=null;

function return_login(o){
	try{
		eval('var e='+o);
		if(e){
			for(a in e) 
				if(typeof(e[a])=="string") e[a]=unescape(e[a]);
		}
		if(e.success){
			if(e.url && e.url!=""){
				var url=e.url;
				if(e.redirect && e.redirect!="")
					url+="&rdir="+escape(e.redirect);
				CATS_MAIN_WINDOW = open_cats_main_window(url);
				CATS_MAIN_WINDOW.resizeTo(screen.availWidth,screen.availHeight);
				CATS_MAIN_WINDOW.moveTo(screen.availLeft,screen.availTop);
			}else{
				_error_message("An unspecified error occured: Please note the steps you took to get to this message and report it to catsadmin@tronox.com");
			}
		}else{
			var s = "";
			if((dup=e.duplicates)!=null) {
				var x = dup.length;
				for(i=0;i<x;i++){
					s+="\n"+dup[i][0] + " : " + dup[i][1];
				}
				s+="\nA message has been sent to the IT department but you should confirm that they got it.";
			}
			if(e.message) _error_message(e.message+s);
			else _error_message("Username or Password was incorrect. Please try again.");
			var f=document.forms[0];
			for(i=0;i<f.length;i++){
				f[i].disabled=false;
			}
		}
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);
	var w=window.open("","dbg");
	var d=w.document;
	d.open();
	d.write(e+"\n"+e.number+"\n"+e.description+"\n"+o);
	d.close();
	}
}

onload = function(){
	try{
		var f=document.forms[0];
		var i=(f[0].value!='')?1:0;
		f[i].focus();
	}catch(e){}
}
window.onfocus=function(){
	if(CATS_MAIN_WINDOW!=null){
		CATS_MAIN_WINDOW.focus();
	}
}
//-->