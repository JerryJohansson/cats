// global vars
var SHOW_ANCHOR=true;

function CATS_getTable_Checkboxes(){
/*************************************************
	create incident checkboxes table string
	Params:
		1. array(array(array(),array(),...),array())
		2. Maximum Columns to generate for the table
	Returns:[string]
		HTML string containing incident checkbox table
*************************************************/
	var args=arguments;
	var name=args[0];//(args[0]).toUpperCase();
	var maxColumns = (args.length>2)?args[2]:3;
	var i=cnt=0;
	var a=args[1];
	var sHTML = "<table style=\"border-bottom: 1px dotted #999;\" width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">";
	
	sHTML += "<tr>";
	for (i=0;i<a.length;i++){ 
		cnt++;
	  if(typeof(PAGE)!='undefined' && PAGE == "search"){
	  	checked = " ";
	  }
	  else{
			checked = (parseInt(a[i][3])>0)?" checked ":"";
		}
		sHTML += "<td valign=\"top\" width=\"3%\">"; 
		sHTML += "<input class=\"bfield\" type=\"checkbox\" "+checked+" id=\"" + name + i + "\" name=\"" + name + "[]\" value=\"" + a[i][0] + "\" />";
		sHTML += "</td><td valign=\"top\" width=\"30%\">";
		sHTML += "<label class=\"chk\" for=\"" + name + i + "\" title=\"" + a[i][1] + "\">" + a[i][2] + "</label>";
		sHTML += "</td>";
		if ((cnt % maxColumns) == 0) sHTML += "</tr><tr>"; 
	} // end for
	sHTML += "</tr>";
	sHTML += "</table>";
	return sHTML;
}

function tab_toggle(itm,type){
	var args=arguments;	var state = (itm.className=="tab_off");
	itm.className = state ? "tab_on" : "tab_off" ;
	
	var parent_name=itm.id.substr(4);
	var tab=element(parent_name);
	tab.className = state ? "div_on" : "div_off" ;
	// create checkboxes for privileged search
	if(typeof(top.gui[type])=='undefined'){
		top.gui[type] = new Object();
	}

	var i=0;
	var s='';
	var append=true;
	var name='';
	var id='';
	/*var str1 ;
	var str2;
	var str3;
	var str4;
	var str5;
	var divLoad1=document.getElementById("div"+args[2]);
	var divLoad2 = document.getElementById("div"+args[3]);
	var divLoad3 = document.getElementById("div"+args[4]);
	var divLoad4 = document.getElementById("div"+args[5]);
	var divLoad5 = document.getElementById("div"+args[6]);
	str1 = divLoad1.innerHTML;
	if(divLoad2 != null)
		str2 = divLoad2.innerHTML;
	if(divLoad3 != null)
		{
		str3 = divLoad3.innerHTML;
		}
	if(divLoad4 != null)
		str4 = divLoad4.innerHTML;
	if(divLoad5 != null)
		str5 = divLoad5.innerHTML;*/
		/*if(parent_name == "all_incidents") 
		{
			var serverity1=	document.getElementById("Serverity_1").checked;
			var serverity2=	document.getElementById("Serverity_2").checked;
			var serverity3=	document.getElementById("Serverity_3").checked;
		}*/
	
	
	//quan.style.display = (quan.style.display=="block" ? "none" : "block");
	
	
	if(state && args.length>2){
		for(i=2;i<args.length;i++){
			append=true;
			name=args[i];
			id="div"+name;
			// if it exists then use it otherwise get it remotely
			if(typeof(top.gui[type][name])!='undefined'){
				 var to=element(id);
				if(to){
					append=false;
					/*if(to.getAttribute("chks_loaded")){
						// nothing to do here yet
						// ... except to ensure the checkbox values don't disappear when the tab is collapsed. - MTG 04Mar2008
						to.innerHTML = top.gui[type][name].html;   // Added MTG 04Mar2008
						
					}else{
						to.setAttribute("chks_loaded","yes");
						to.innerHTML = top.gui[type][name].html;
						
					}*/
					
				}
				location.href="#anc_"+parent_name;
				
			}
			if(append) s+=args[i]+"|";
			
		}
		if(s!=""){
			func = 'get_'+type+"_array";
			s=s.replace(/\|$/,"");
			get_remote_checkboxes(parent_name,type,func,'incidents',s);
			}
		
	}
	else{
		//if(typeof(PAGE)=='undefined' || (typeof(PAGE)!='undefined' && PAGE != "search")) return false;
		if(!state){
			var f=document.forms[0];
			var chk_name='';
					
			i=0
			switch(parent_name){//all_incidents,injuries,environment,community,quality
				case "all_incidents":
					//var r=f.elements["SEVERITY_RATING"];
					//for(i=0;i<r.length;i++) r[i].checked=false;
					break;
				case "injuries":
					//var e=f.elements["INJURED_PERSON_ID"];
					//var d=f.elements["INJURED_PERSON_ID_text"];
					var c=f.elements["INJUREDCOMPANYID"];
					//e.value="";
					//d.value="";
					if(c) c.options[0].selected=true;
					break;
				case "community":
					//var r=f.elements["SUBSTANTIATED"];
					//for(i=0;i<r.length;i++) r[i].checked=false;
					break;
			}
			 
			/*for(i=2;i<args.length;i++){
				name=args[i];
				chk_name=name+"[]";
				var chks = f.elements[chk_name];
				if(chks.length>0){
					for(j=0;j<chks.length;j++)
						chks[j].checked=false;
				}else chks.checked=false;
			}*/
		}
		
	}
	/* divLoad1.innerHTML= str1;
	 if(divLoad2 != null)
		 divLoad2.innerHTML=str2;
	if(divLoad3 != null)
		{
		 divLoad3.innerHTML= str3;
		}
	if(divLoad4 != null)
		divLoad4.innerHTML=str4;
	if(divLoad5 != null)
		divLoad5.innerHTML=str5;*/
		
	
			
		}
	 


function get_remote_checkboxes(name,type,func, module, names){
	var obj={
		'a':func,
		'm':module,
		'p':names,
		'n':name,
		'type':type
	};
	if(typeof(INCIDENT_ID)=="number") obj.id = INCIDENT_ID;
	var url = cats_get_path(CATS_REMOTE_PATH,module);
	var q = remote._query(obj);
	if(typeof(top.gui[type][name])=='undefined') top.gui[type][name] = new Object();
	top.gui[type][name]._xml = _HTTPRequest(url,"post",set_remote_checkboxes,q);
}
function set_remote_checkboxes(return_obj){
	try{
		eval('var e='+return_obj);
		if(typeof(e.options)=="object"){
			var type = e.type;
			for(name in e.options){
				top.gui[type][name] = new Object();
				top.gui[type][name]._array = e.options[name];
				top.gui[type][name].html = CATS_getTable_Checkboxes(name, e.options[name]);
				var o=element("div"+name);
				if(o){
					o.innerHTML = top.gui[type][name].html;
				}
			}
			if(SHOW_ANCHOR){
				var parent_name=e.name;
				location.href="#anc_"+parent_name;
			}
		}
	}catch(e){format_error("Error setting checkbox values",e,"set_remote_checkboxes.Retrun Value:\n"+return_obj);}
}