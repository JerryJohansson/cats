<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>EmailTest Cronjob</title>
</head>
<body>

<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

define('CATS_ROOT_PATH','/apps2/CATS/dev/');
define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php
define('CATS_REMOTE_PATH',CATS_INCLUDE_PATH.'remote/');

// load db stuff
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/cats_admin.ini");
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}

// Include the DB Functions
require_once(CATS_FUNCTIONS_PATH . 'db_functions.php');

// Include the Min functions
require_once(CATS_FUNCTIONS_PATH . 'main_functions.php');

// Include the Mail Class
require_once(CATS_CLASSES_PATH . 'mail.class.php');

// Set Debugging to True
$db->debug = true;

// connect to the database
db_connect();

// Load the Email Config
EmailConfig();
echo "<p>".CATS_MAIL_FROM_ADDRESS."</p>";
echo "<p>".CATS_MAIL_TEST_ADDRESS."</p>";

if(CATS_MAIL_STATUS == 1) {
	// Send Single Test Email
	$to = "testcats@tronox.com";
	$from = "catshelpdesk.v3@tronox.com";
	$subject = "Test Email from PHP";
	$body = "This is a test email sent from a php job.";
	
	SendEmail($to, $from, $subject, $body);
}

// Close the database
$db = db_close();


/*
+---------------------------------------------------------------------------
|	Load the Email Configuration
+---------------------------------------------------------------------------
*/
function EmailConfig() {
	$sql = "SELECT * FROM tblemailoptions";
	$rs = db_query($sql);
	// build the array of fields
	$arr=array();
	$row = $rs->FetchRow();
	$fcnt=$rs->FieldCount();
	for($i=0;$i<$fcnt;$i++){
		// get field object so we know what we are dealing with
		$fld=$rs->FetchField($i);
		// get the field type
		$type = $rs->MetaType($fld->type);
		switch($type){
			case 'D':case 'T': // format the date value
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'N':case 'I': // don't really need this one as this is the same as the default case
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'C':case 'X': // format text value
				$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
				break;
			default: // default value is simply the value of the field
				$arr[$fld->name] = $row[$fld->name];
				break;
		}
	}
	define('CATS_MAIL_STATUS',1); //$arr['EMAIL_STATUS']);
	define('CATS_MAIL_HOST',$arr['EMAIL_SERVER']);
	define('CATS_MAIL_TEST_ADDRESS',$arr['EMAIL_TESTADDRESS']);
	define('CATS_MAIL_TEST_NAME',$arr['EMAIL_TESTNAME']);
	define('CATS_MAIL_FROM_ADDRESS',$arr['EMAIL_FROMADDRESS']);
	define('CATS_MAIL_FROM_NAME',$arr['EMAIL_FROMNAME']);
	//define('CATS_MAIL_TRANSPORT','smtp');
}

/*
+---------------------------------------------------------------------------
|	Send the email function
+---------------------------------------------------------------------------
*/
function SendEmail($to,$from,$subject,$body) { 

	// Create the E-Mail
	$m= new Mail; 				// create the mail object
	$m->From( $from );			// set the from
	$m->To( $to );				// set the to
	$m->Subject( $subject );	// set the subject
	$m->Body( $body );			// set the body
	$m->Priority(4) ;			// set the priority (default Low(4))
	$ret = $m->Send();			// send the mail
	if ((bool)$ret)
		echo "The mail below has been sent:<br><pre>", $m->Get(), "</pre>";
	else
		echo "The mail below has failed:<br><pre>", $m->Get(), "</pre>";

	return (bool)$ret;
}

?>

</body>
</html>
