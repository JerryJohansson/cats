<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>EmailReminder Cronjob</title>
</head>
<body>
<h1> EmailReminder Cronjob Started</h1>

<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

switch($host_name) {
	case 'catsdev' :
		echo "<h2> Running Script over DEV </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catsdev31' :
		echo "<h2> Running Script over DEV 3.1</h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev31/');
		break;	
	case 'catstst' :
		echo "<h2> Running Script over TST </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	case 'catsprd' :
		echo "<h2> Running Script over PRD </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/prd/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;	
}

define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php
define('CATS_REMOTE_PATH',CATS_INCLUDE_PATH.'remote/');

// load db stuff
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/cats_admin.ini");
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}

// Include the DB Functions
require_once(CATS_FUNCTIONS_PATH . 'db_functions.php');

// Include the Min functions
require_once(CATS_FUNCTIONS_PATH . 'main_functions.php');

// Include the Mail Class
require_once(CATS_CLASSES_PATH . 'mail.class.php');

// Set Debugging to True
$db->debug = true;

// connect to the database
db_connect();

// Load the Email Config
EmailConfig();

if(CATS_MAIL_STATUS >= 1) {
	// Send the Reminders
	//SendReminders();

	// Send the Allocated Reminders
	SendAllocatedReminders();
}

// Close the database
$db = db_close();


/*
+---------------------------------------------------------------------------
|	Load the Email Configuration
+---------------------------------------------------------------------------
*/
function EmailConfig() {
	$sql = "SELECT * FROM tblemailoptions";
	$rs = db_query($sql);
	// build the array of fields
	$arr=array();
	$row = $rs->FetchRow();
	$fcnt=$rs->FieldCount();
	for($i=0;$i<$fcnt;$i++){
		// get field object so we know what we are dealing with
		$fld=$rs->FetchField($i);
		// get the field type
		$type = $rs->MetaType($fld->type);
		switch($type){
			case 'D':case 'T': // format the date value
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'N':case 'I': // don't really need this one as this is the same as the default case
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'C':case 'X': // format text value
				$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
				break;
			default: // default value is simply the value of the field
				$arr[$fld->name] = $row[$fld->name];
				break;
		}
	}
	define('CATS_MAIL_STATUS',$arr['EMAIL_STATUS']);
	define('CATS_MAIL_HOST',$arr['EMAIL_SERVER']);
	define('CATS_MAIL_TEST_ADDRESS',$arr['EMAIL_TESTADDRESS']);
	define('CATS_MAIL_TEST_NAME',$arr['EMAIL_TESTNAME']);
	define('CATS_MAIL_FROM_ADDRESS',$arr['EMAIL_FROMADDRESS']);
	define('CATS_MAIL_FROM_NAME',$arr['EMAIL_FROMNAME']);
	//define('CATS_MAIL_TRANSPORT','smtp');
}

/*
+---------------------------------------------------------------------------
|	Send the email function
+---------------------------------------------------------------------------
*/
function SendEmail($Employee_Name,$Employee_Email,$Action_Arr,$Message_Type,$Site) { 

	// Set the Subject and Body 
	if($Message_Type == "ManagedBy") {
		$subject = 'Reminder of Your Managed Actions to complete as of: '.date("d/m/y");
		$body = 'This is a reminder that you have been assigned to manage the following action/s:

	';
	} else {
		$subject = 'Reminder of Your Allocated Actions to complete as of: '.date("d/m/y");
		$body = 'This is a reminder that you have been allocated to carry out the following action/s (Note: This is a final reminder, to receive another reminder please change the reminder date on the action):

	';
	}
	
	// Build the List of Actions
	foreach($Action_Arr as $key => $value) {
		$body .= 'Action Title:  '.$value['ACTION_TITLE'].'
	';
		$body .= 'Action Number: '.$value['ACTION_ID'].'
	';
		$body .= 'Date Due:      '.$value['SCHEDULED_DATE'].'
	';
		$body .= 'Description:   '.$value['ACTION_DESCRIPTION'].'

';
	}
	
	// Set Addresses
	if(CATS_MAIL_STATUS == 1) {
		$to = "testcats@tronox.com";
		$from = "catshelpdesk.v3@tronox.com";
						
		// Add Original Recipients and Sender to Test Message
		$sql = "SELECT email_from_address FROM tblsite WHERE site_id=".$Site;
		$body .= "
		
		
		Original Recipient : ".$Employee_Email."
		Original Sender     : ".db_get_one($sql);
		
	} else {
		$to = $Employee_Email;
		
		// Get the email address from the site
		$sql = "SELECT email_from_address FROM tblsite WHERE site_id=".$Site;
		$from = db_get_one($sql);
	}
	
	// Create the E-Mail
	$m= new Mail; 				// create the mail object
	$m->From( $from );			// set the from
	$m->To( $to );				// set the to
	$m->Subject( $subject );	// set the subject
	$m->Body( $body );			// set the body
	$m->Priority(4) ;			// set the priority (default Low(4))
	$ret = $m->Send();			// send the mail
	if ((bool)$ret)
		echo "The mail below has been sent:<br><pre>", $m->Get(), "</pre>";
	else
		echo "The mail below has failed:<br><pre>", $m->Get(), "</pre>";

	return (bool)$ret;
}

/*
+---------------------------------------------------------------------------
|	Send the Reminders
+---------------------------------------------------------------------------
*/
function SendReminders() {
	$manager_arr = array();

//	$sql = "SELECT DISTINCT managed_by_id FROM tblaction_details WHERE SYSDATE > reminder_date AND status='Open' AND register_origin NOT LIKE 'PCR%'";
//  Task 15367 - Now include PCRs as well
	$sql = "SELECT DISTINCT managed_by_id FROM tblaction_details WHERE SYSDATE > reminder_date AND status='Open' ";
	$rs = db_query($sql);
	$rc = $rs->RowCount();
	
	// Build the array of managers
	$arr=array();
	while($row = $rs->FetchRow()){
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		// Add the Manager ID to the Array
		$manager_arr[] = array('EMPLOYEE_NUMBER' => $arr['MANAGED_BY_ID']);
	}
	
	// Now get the Manager Details
	foreach($manager_arr as $key => $value) {
		$sql = "SELECT emp_name,email_address,employee_number,site_id FROM view_employee_details WHERE employee_number=".$value['EMPLOYEE_NUMBER'];
		$rs = db_query($sql);
		$rc = $rs->RowCount();
		
		// Extend the array of managers
		$arr=array();
		while($row = $rs->FetchRow()){
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
		}
		$value['EMP_NAME'] = $arr['EMP_NAME'];
		$value['EMAIL_ADDRESS'] = $arr['EMAIL_ADDRESS'];
		$value['SITE_ID'] = $arr['SITE_ID'];		
	}
	
	// Now Process the emails
	foreach($manager_arr as $key => $value) {
//		$sql = "SELECT action_id, action_title, action_description, allocated_to_id, TO_CHAR(scheduled_date, 'YYYY-MM-DD') as scheduled_date, TO_CHAR(reminder_date, 'YYYY-MM-DD') as reminder_date FROM tblaction_details WHERE SYSDATE > reminder_date AND status='Open' AND register_origin NOT LIKE 'PCR%' AND managed_by_id=".$value['EMPLOYEE_NUMBER'];
//  Task 15367 - Now include PCRs as well
		$sql = "SELECT action_id, action_title, action_description, allocated_to_id, TO_CHAR(scheduled_date, 'YYYY-MM-DD') as scheduled_date, TO_CHAR(reminder_date, 'YYYY-MM-DD') as reminder_date FROM tblaction_details WHERE SYSDATE > reminder_date AND status='Open' AND managed_by_id=".$value['EMPLOYEE_NUMBER'];
		$rs = db_query($sql);
		$rc = $rs->RowCount();
		
		// For Each Reminder Email
		$Action_Arr=array();
		$arr=array();
		while($row = $rs->FetchRow()){
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
			// Build Array of Actions
			$Action_Arr[] = array(
				'ACTION_ID' => $arr['ACTION_ID'],
				'ACTION_TITLE' => $arr['ACTION_TITLE'],
				'SCHEDULED_DATE' => $arr['SCHEDULED_DATE'],
				'REMINDER_DATE' => $arr['REMINDER_DATE'],
				'ACTION_DESCRIPTION' => $arr['ACTION_DESCRIPTION']
				);
		}
		SendEmail($value['EMP_NAME'], $value['EMAIL_ADDRESS'], $Action_Arr, 'Managed By', $value['SITE_ID']);
	}
}

/*
+---------------------------------------------------------------------------
|	Send the Allocated Reminders
+---------------------------------------------------------------------------
*/
function SendAllocatedReminders() {
	$allocated_arr = array();
	
	$sql = "SELECT DISTINCT allocated_to_id FROM tblaction_details WHERE TO_CHAR(SYSDATE, 'YYYY-MM-DD') = TO_CHAR(reminder_date, 'YYYY-MM-DD') AND status='Open' AND allocated_to_id <> 0";
//	$sql = "SELECT DISTINCT allocated_to_id FROM tblaction_details WHERE TO_CHAR(SYSDATE, 'YYYY-MM-DD') = TO_CHAR(reminder_date, 'YYYY-MM-DD') AND status='Open' AND register_origin NOT LIKE 'PCR%' AND allocated_to_id <> 0";
//  Task 15367 - Now include PCRs as well
		//$sql = "SELECT DISTINCT allocated_to_id FROM tblaction_details WHERE SYSDATE = reminder_date AND status='Open' AND register_origin NOT LIKE 'PCR%' AND allocated_to_id <> 0";

	$rs = db_query($sql);
	$rc = $rs->RowCount();
	
	// Build the array of managers
	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		// Add the Manager ID to the Array
		$allocated_arr[] = array('EMPLOYEE_NUMBER' => $arr['ALLOCATED_TO_ID']);
	}
	
	// Now get the Manager Details
	foreach($allocated_arr as $key => $value) {
		$sql = "SELECT emp_name,email_address,employee_number,site_id FROM view_employee_details WHERE employee_number=".$value['EMPLOYEE_NUMBER'];
		$rs = db_query($sql);
		$rc = $rs->RowCount();
		
		// Extend the array of managers
		while($row = $rs->FetchRow()){
			$arr=array();
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
		}
		$allocated_arr[$key]['EMP_NAME'] = $arr['EMP_NAME'];
		$allocated_arr[$key]['EMAIL_ADDRESS'] = $arr['EMAIL_ADDRESS'];
		$allocated_arr[$key]['SITE_ID'] = $arr['SITE_ID'];		
	}
	
	// Now Process the emails
	foreach($allocated_arr as $key => $value) {
//		$sql = "SELECT action_id, action_title, action_description, allocated_to_id, TO_CHAR(scheduled_date, 'YYYY-MM-DD') as scheduled_date, TO_CHAR(reminder_date, 'YYYY-MM-DD') as reminder_date FROM tblaction_details WHERE TO_CHAR(SYSDATE, 'YYYY-MM-DD') = TO_CHAR(reminder_date, 'YYYY-MM-DD') AND status='Open' AND register_origin NOT LIKE 'PCR%' AND allocated_to_id=".$value['EMPLOYEE_NUMBER'];
//  Task 15367 - Now include PCRs as well
		$sql = "SELECT action_id, action_title, action_description, allocated_to_id, TO_CHAR(scheduled_date, 'YYYY-MM-DD') as scheduled_date, TO_CHAR(reminder_date, 'YYYY-MM-DD') as reminder_date FROM tblaction_details WHERE TO_CHAR(SYSDATE, 'YYYY-MM-DD') = TO_CHAR(reminder_date, 'YYYY-MM-DD') AND status='Open' AND allocated_to_id=".$value['EMPLOYEE_NUMBER'];
		$rs = db_query($sql);
		$rc = $rs->RowCount();
		
		// For Each Reminder Email
		$Action_Arr=array();
		while($row = $rs->FetchRow()){
			$arr=array();
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
			// Build Array of Actions
			$Action_Arr[] = array(
				'ACTION_ID' => $arr['ACTION_ID'],
				'ACTION_TITLE' => $arr['ACTION_TITLE'],
				'SCHEDULED_DATE' => $arr['SCHEDULED_DATE'],
				'REMINDER_DATE' => $arr['REMINDER_DATE'],
				'ACTION_DESCRIPTION' => $arr['ACTION_DESCRIPTION']
				);
		}
		SendEmail($value['EMP_NAME'], $value['EMAIL_ADDRESS'], $Action_Arr, 'Allocated To', $value['SITE_ID']);
	}
}
?>

<h1> EmailReminder Cronjob Completed</h1>
</body>
</html>
