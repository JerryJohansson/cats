//************************************************ Configuration Section ********************************************************
  // ############################################# Database connection ################################################
  //CATSDEV - development database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSDEV; UID=catsdba; PWD=takerealcare;';

  //CATSPROD - production database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSPRD_DS1; UID=catsdba; PWD=takecare;';
  
  //CATSTST
  var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSTST_DS1; UID=catsdba; PWD=catstst;';
  
  
  // ############################################ End Database connection ##############################################
 
 
 //########################## Email Confuguration ##########################
  /*
  var strRSString = "SELECT * FROM tblEmailOptions";
  var rsEmail = createRecordset(strRSString);
  var MM_EmailStatus = parseInt(rsEmail.Fields.Item("Email_Status").Value);
  var MM_EmailServer = rsEmail.Fields.Item("Email_Server").Value;
  var MM_EmailTestAddress = rsEmail.Fields.Item("Email_TestAddress").Value;
  var MM_EmailTestName = rsEmail.Fields.Item("Email_TestName").Value;
  var MM_EmailFromAddress = rsEmail.Fields.Item("Email_FromAddress").Value;
  var MM_EmailFromName = rsEmail.Fields.Item("Email_FromName").Value;
  rsEmail.Close();
  //*/
  //######################## End Email Confuguration #########################
	//MM_EmailStatus = 0

  //*
  var MM_EmailServer = 'perxs1'; // Email server name
  var MM_EmailFromName = 'CATS Agent Creator'; //From name to appear in any emails sent
  var MM_EmailFromAddress = 'serviceM@tiwest.com'; //address any reply emails will be sent to
  var MM_EmailTestAddress = 'kevinl@tronox.com'; //address to send test emails
  var MM_EmailTestName = "Kevin Lilje"; //name to put in test emails
  var MM_EmailStatus = 1; //0 = email turned off; 1 = send emails to MM_EmailTestAddress; 2 = send emails to proper person
  //*/
  
//******************************************** End Configuration Section ***********************************************************

function formatDate(ufDate){
	var DD = ufDate.getDate();
	var MM = ufDate.getMonth()+1;
	var YYYY = ufDate.getYear();
	var fDate = DD + '/' + MM + '/' + YYYY;
	return fDate;
}

function dateAdd(start, days){
    // get the milliseconds for this Date object.
    var buffer = Date.parse( start ) ;
    days *= 24 ; // days to hours
	days *= 60 ; // hours to minutes
	days *= 60 ; // minutes to seconds
	days *= 1000 ; // seconds to milliseconds
    var newDate = new Date( buffer + days ) ;
    return newDate;
}

function createRecordset(strSource){
	//var MM_cats_STRING = 'dsn=CATS;uid=catsdba;pwd=takecare';
	var MM_cats_STRING = connString;
	var rs = new ActiveXObject('ADODB.Recordset');
	rs.ActiveConnection = MM_cats_STRING;
	rs.Source = strSource;
	rs.CursorType = 3;
	rs.CursorLocation = 2;
	rs.LockType = 3;
	rs.Open();
	return rs;
}

function updateAction(Action_Id, ReminderDate){
	var rsUpdateAction = createRecordset("SELECT * FROM TBLACTION_DETAILS WHERE ACTION_ID = " + Action_Id);
	rsUpdateAction.Edit;
	rsUpdateAction.Fields("REMINDER_DATE") = formatDate(ReminderDate);
	rsUpdateAction.Update;
	rsUpdateAction.Close();
}

function sendEmail(strEmp_Name, strEmail, arAction_Id, arAction_Title, arDue_Date, intSite){
  	//--------------------------------------------------------------------------
	var strRemoteHost = MM_EmailServer;
	var strFromName = MM_FromName;
	var strFromAddress = MM_FromAddress;
	//-----------------------------------------------------------------------

	//Send email to Action Manager
	// get the form variables to insert into the email message
	// create and send the managed by email
	var today = new Date();
	var fToday = formatDate(today);
	strSubject = 'Reminder of Overdue Actions to complete as of: ' + fToday;
	strBody = 'You are the assigned manager of the following overdue action/s:\n';
	for(var i=1;i<arAction_Id.length;i++){
		strBody += '\nAction Title:  ' + arAction_Title[i] + '\n';
		strBody += 'Action Number:   ' + arAction_Id[i] + '\n';
		strBody += 'This Action was due on: ' + formatDate(arDue_Date[i]) + '\n\n';
	}

	//get the email from address for this persons site
	var strRSString = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + intSite;
	var rsSite_Details = createRecordset(strRSString);
	if(!rsSite_Details.EOF || !rsSite_Details.BOF){
		var TempAddress = rsSite_Details.Fields.Item("Email_From_Address").Value;
		if(TempAddress != ""){strFromAddress = TempAddress;}
	}
	rsSite_Details.Close();

    if(MM_EmailStatus == 1){// if MM_EmailStatus = 1 this means all emails are getting sent to the person in MM_TestAddress
		strFromAddress = MM_FromAddress;
		strEmail = MM_TestAddress;
		strEmp_Name = MM_TestName;
	}

	//check that we have valid email addresses
	var tempAddress_1 = String(strEmail);
	var tempAddress_2 = String(strFromAddress);
	var IsEmail_1 = tempAddress_2.indexOf("@");
	var IsEmail_2 = tempAddress_1.indexOf("@");

	if(IsEmail_2 > 0 && IsEmail_1 > 0){
		var Mailer = new ActiveXObject('SMTPsvg.Mailer');
		Mailer.FromName = strFromName;
		Mailer.FromAddress = strFromAddress;
		Mailer.RemoteHost = strRemoteHost;
		Mailer.AddRecipient(strEmp_Name, strEmail);
		Mailer.Subject	= strSubject;
		Mailer.BodyText   = strBody;
		Mailer.SendMail();
	}
}

function SendReminders(){
	var sAndWhere = " AND Register_Origin Not Like 'PCR%' ";
	var strSQL = "SELECT * FROM TBLACTION_DETAILS WHERE SYSDATE > Scheduled_Date AND STATUS = 'Open'" + sAndWhere;
	var rsActionDocs = createRecordset(strSQL);
	var ActionCount = rsActionDocs.RecordCount;
	rsActionDocs.Close();
	if(ActionCount>0){ // there are actions that need reminders to be sent
		strSQL = "SELECT DISTINCT MANAGED_BY_ID FROM TBLACTION_DETAILS WHERE SYSDATE > Scheduled_Date AND STATUS = 'Open'" + sAndWhere;
		var rsActionDocs = createRecordset(strSQL);
		ActionCount = rsActionDocs.RecordCount;
		var arManagers = new Array(ActionCount);//set up an array for all the action managers
		for(var i=1;i<=ActionCount;i++){
			arManagers[i] = rsActionDocs.Fields.Item('MANAGED_BY_ID').Value;
			rsActionDocs.MoveNext();
		}
		rsActionDocs.Close();
		// we now have a list of the managers
		//loop thorough each manager, select thier records, create their actions, send the emails
		for(var i=1;i<=ActionCount;i++){
		//create the managers Actiond documents
			strSQL = "SELECT * FROM TBLACTION_DETAILS WHERE SYSDATE > Scheduled_Date AND STATUS = 'Open' AND  MANAGED_BY_ID = " + arManagers[i] + sAndWhere;
			var rsActionDocs = createRecordset(strSQL);
		    // we need an array for all the email variables
			var arAction_Id = new Array(rsActionDocs.RecordCount);
			var arAction_Title = new Array(rsActionDocs.RecordCount);
			var arDue_Date = new Array(rsActionDocs.RecordCount);
			var Managed_By_Id = arManagers[i];
			var x = 1;
			while(!rsActionDocs.EOF){
				arAction_Title[x] = rsActionDocs.Fields.Item('ACTION_TITLE').Value;
				var due_date = new Date(rsActionDocs.Fields.Item('SCHEDULED_DATE').Value);
				var reminder_date = new Date(rsActionDocs.Fields.Item('REMINDER_DATE').Value);
				arDue_Date[x] = due_date;
				var ReminderDate = dateAdd(reminder_date, 7);
				var Action_Id = rsActionDocs.Fields.Item('ACTION_ID').Value;
				arAction_Id[x] = Action_Id;
				updateAction(Action_Id, ReminderDate);
				rsActionDocs.MoveNext();
				x+=1;
			}
			rsActionDocs.Close();
			strSQL = 'SELECT * FROM tblEmployee_Details WHERE Employee_Number=' + Managed_By_Id;
			var rsActionDocs = createRecordset(strSQL);
			if(!rsActionDocs.BOF || !rsActionDocs.EOF){
				var strEmp_Name = rsActionDocs.Fields.Item('Last_Name').Value + ', ' + rsActionDocs.Fields.Item('First_Name').Value;
				var strEmail = rsActionDocs.Fields.Item('Email_Address').Value;
				var intSite = rsActionDocs.Fields.Item('Site_Id').Value;
				sendEmail(strEmp_Name, strEmail, arAction_Id, arAction_Title, arDue_Date, intSite);
			}
			rsActionDocs.Close();
		}
	}
}

//***** Send the Reminders ******
if(MM_EmailStatus != 0){ // if MM_EmailStatus = 0 this means emailing is turned off
	SendReminders();
}
