//************************************************ Configuration Section ********************************************************
  // ############################################# Database connection string ################################################
  //CATSDEV - development database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSDEV; UID=catsdba; PWD=catsdba;';

  //CATSTEST - Testing database
  var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSTST_DS1; UID=catsdba; PWD=catstst;';
  
  //CATSPROD - production database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSPRD_DS1; UID=catsdba; PWD=takecare;';
  var CRLF = "\r\n";
  var Email_OK = 0;
	var bErrors = 0;
  var MM_cats_STRING = connString;
  // ############################################ End Database connection string ##############################################

  //########################## Email Confuguration ##########################
  /*
  var strRSString = "SELECT * FROM tblEmailOptions";
  var rsEmail = createRecordset(strRSString);  
  var MM_EmailStatus = parseInt(rsEmail.Fields.Item("Email_Status").Value);
  var MM_EmailServer = rsEmail.Fields.Item("Email_Server").Value;
  var MM_EmailTestAddress = rsEmail.Fields.Item("Email_TestAddress").Value;
  var MM_EmailTestName = rsEmail.Fields.Item("Email_TestName").Value;
  var MM_EmailFromAddress = rsEmail.Fields.Item("Email_FromAddress").Value;
  var MM_EmailFromName = rsEmail.Fields.Item("Email_FromName").Value;
  rsEmail.Close();
  //*/
  //######################## End Email Confuguration #########################
	//MM_EmailStatus = 0
  //*
  var MM_EmailServer = 'perxs1'; // Email server name
  var MM_EmailFromName = 'CATS Agent Creator'; //From name to appear in any emails sent
  var MM_EmailFromAddress = 'serviceM@tiwest.com'; //address any reply emails will be sent to
  var MM_EmailTestAddress = 'kevinl@tronox.com'; //address to send test emails
  var MM_EmailTestName = "Kevin Lilje"; //name to put in test emails
  var MM_EmailStatus = 1; //0 = email turned off; 1 = send emails to MM_EmailTestAddress; 2 = send emails to proper person
  //*/
  var strErrors = "";
  var MM_ServerPath = "catstst/";
  var gbRunAtServerSide = false;
  var gbTestRun=false;
//******************************************** End Configuration Section ***********************************************************

function FormatDateOut(ufDate){
	var aMonth = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	var DD = ufDate.getDate();
	var MMM = aMonth[ufDate.getMonth()];
	var YYYY = ufDate.getFullYear();
	var fDate = DD + '-' + MMM + '-' + YYYY;
	return fDate;
}

function formatDate(ufDate){
	var DD = ufDate.getDate();
	var MM = ufDate.getMonth()+1;
	var YYYY = ufDate.getYear();
	var fDate = DD + '/' + MM + '/' + YYYY;
	return fDate;
}

function dateAdd(start, days){
	// get the milliseconds for this Date object.
	var buffer = Date.parse(start);
	days *= 24; // days to hours
	days *= 60; // hours to minutes
	days *= 60; // minutes to seconds
	days *= 1000; // seconds to milliseconds
	var newDate = new Date(buffer + days);
	return newDate;
}

function dateMinus(start, days){
	// get the milliseconds for this Date object.
	var buffer = Date.parse(start);
	days *= 24; // days to hours
	days *= 60; // hours to minutes
	days *= 60; // minutes to seconds
	days *= 1000; // seconds to milliseconds
	var newDate = new Date(buffer - days);
	return newDate;
}

function getObject(id){
	if(gbRunAtServerSide){
		if(gbTestRun && id.toLowerCase()=='smtpsvg.mailer') return new objSendMailToOutput();
		return Server.CreateObject(id);
	}else{
		return new ActiveXObject(id);
	}
}
function createRecordset(strSource){
	//var MM_cats_STRING = 'dsn=CATS;uid=catsdba;pwd=takecare';
	//var MM_cats_STRING = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATS; UID=catsdba; PWD=takecare;';
	var MM_cats_STRING = connString;
	var rs = getObject('ADODB.Recordset');
	rs.ActiveConnection = MM_cats_STRING;
	rs.Source = strSource;
//	out(strSource);
//	out('=======');
	rs.CursorType = 3;
	rs.CursorLocation = 2;
	rs.LockType = 3;
	rs.Open();
	return rs;
}
function generateNewId(){
//  	var strRSString = "SELECT NVL(MAX(ACTION_ID),1) NEW_ID FROM TBLACTION_DETAILS";
  	var strRSString = "SELECT new_action_id NEW_ID FROM DUAL";
  	var rsNew_Id = createRecordset(strRSString);
	var new_ID = 1;
	if(rsNew_Id.Fields.Item('NEW_ID').Value!=null){ new_ID = rsNew_Id.Fields.Item('NEW_ID').value + 1; }
	rsNew_Id.Close();
	return new_ID;
}
function CreateActions(){
	var strSQL = "SELECT * FROM tblScheduler WHERE Action_Raised_Date Is Not Null And Action_Raised_Date <= SYSDATE And Status != 'Off' ";
	var rsSchedDocs = createRecordset(strSQL);
	var ScheduleCount = rsSchedDocs.RecordCount;
	rsSchedDocs.Close();
	out('We have '+ScheduleCount+' actions to create');
	if(ScheduleCount>0){ //there are actions to be created
		strSQL = "SELECT DISTINCT MANAGED_BY_ID FROM tblScheduler WHERE Action_Raised_Date Is Not Null And Action_Raised_Date <= SYSDATE And Status != 'Off' ";
		var rsSchedDocs = createRecordset(strSQL);
		ScheduleCount = rsSchedDocs.RecordCount;
		var arManagers = new Array(ScheduleCount);
		for(var i=1;i<=ScheduleCount;i++){
			arManagers[i] = rsSchedDocs.Fields.Item('MANAGED_BY_ID').Value;
			rsSchedDocs.MoveNext();
		}
		rsSchedDocs.Close();
		// we now have a list of the managers
		//loop thorough each manager, select thier records, create their actions, send the emails
		var New_ID = generateNewId();
		out('NewID = '+New_ID);
		for(var i=1;i<=ScheduleCount;i++){
		//create the managers scheduled documents
			strSQL = "SELECT * FROM tblScheduler WHERE Action_Raised_Date Is Not Null And Action_Raised_Date <= SYSDATE And Status != 'Off' And MANAGED_BY_ID = " + arManagers[i] + " ";
			var rsSchedDocs = createRecordset(strSQL);
		// we need an array for all the email variables
			var arAction_Id = new Array(rsSchedDocs.RecordCount);
			var arAction_Title = new Array(rsSchedDocs.RecordCount);
			var arAction_Desc = new Array(rsSchedDocs.RecordCount);
			var arDue_Date = new Array(rsSchedDocs.RecordCount);
			var Managed_By_Id = arManagers[i];
			var x = 1;
			while(!rsSchedDocs.EOF){
				arAction_Id[x] = New_ID;
				arAction_Title[x] = rsSchedDocs.Fields.Item('ACTION_TITLE').Value;
				arAction_Desc[x] = rsSchedDocs.Fields.Item('SCHEDULER_DESCRIPTION').Value;
				var due_date = new Date(rsSchedDocs.Fields.Item('COMPLETION_DATE').Value);
				arDue_Date[x] = due_date;
				var ReminderDate = null;
				var No_Days_Notice = rsSchedDocs.Fields.Item('NO_OF_DAYS_NOTICE').Value;
				if(No_Days_Notice>0){
					ReminderDate = dateMinus(due_date, No_Days_Notice);
				}

				var Action_Id = New_ID;
				var Register_Origin = rsSchedDocs.Fields.Item('REGISTER_ORIGIN').Value;
				var Action_Type = 'Planned';
				var Action_Title = rsSchedDocs.Fields.Item('ACTION_TITLE').Value;
				var Action_Description = rsSchedDocs.Fields.Item('SCHEDULER_DESCRIPTION').Value;
				var Scheduled_Date = new Date(rsSchedDocs.Fields.Item('COMPLETION_DATE').Value);
				var Managed_By_Id = rsSchedDocs.Fields.Item('MANAGED_BY_ID').Value;
				var Site_Id = rsSchedDocs.Fields.Item('SITE_ID').Value;
				var Department_Id = rsSchedDocs.Fields.Item('DEPARTMENT_ID').Value;
				var Reminder_Date = ReminderDate;
				var Status = 'Open';
				var Origin_Table = 'Originating_Schedule';
				var Report_Id = rsSchedDocs.Fields.Item('SCHEDULER_ID').Value;
				var Section_Id = rsSchedDocs.Fields.Item('Section_Id').Value;
				out('CreateAction for '+Action_Id);
				if(createAction(Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description, Scheduled_Date, Managed_By_Id, null, Site_Id, Department_Id, Reminder_Date, Status, Origin_Table, Report_Id, Section_Id)){
					out('UpdateSchedule for '+Action_Id);
					updateSchedule(rsSchedDocs.Fields.Item('Scheduler_Id').Value);
					New_ID+=1;
				}
				rsSchedDocs.MoveNext();
				x+=1;
			}
			rsSchedDocs.Close();
			strSQL = "SELECT * FROM tblEmployee_Details WHERE Employee_Number=" + Managed_By_Id;
			var rsSchedDocs = createRecordset(strSQL);
			if(!rsSchedDocs.EOF || !rsSchedDocs.BOF){
				var strEmp_Name = rsSchedDocs.Fields.Item('First_Name').Value + ' ' + rsSchedDocs.Fields.Item('Last_Name').Value;
				var strEmail = rsSchedDocs.Fields.Item('Email_Address').Value;
				var intSite = rsSchedDocs.Fields.Item('Site_Id').Value;
				sendEmail(strEmp_Name, strEmail, intSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc);
			}
			rsSchedDocs.Close();
		}
	}
}
function createAction(Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description, Scheduled_Date, Managed_By_Id, Allocated_To_Id, Site_Id, Department_Id, Reminder_Date, Status, Origin_Table, Report_Id, Section_Id){
	try{
		var rsNewAction = createRecordset("Select * from tblAction_Details");
		rsNewAction.AddNew();
		rsNewAction.Fields("Action_Id") = Action_Id;
		rsNewAction.Fields("Register_Origin") = Register_Origin;
		rsNewAction.Fields("Action_Type") = Action_Type;
		rsNewAction.Fields("Action_Title") = Action_Title;
		rsNewAction.Fields("Action_Description") = Action_Description;
		rsNewAction.Fields("Scheduled_Date") = formatDate(Scheduled_Date);
		rsNewAction.Fields("Managed_By_Id") = Managed_By_Id;
		rsNewAction.Fields("Allocated_To_Id") = Allocated_To_Id;
		rsNewAction.Fields("Site_Id") = Site_Id;
		rsNewAction.Fields("Department_Id") = Department_Id;
		if ( typeof(Reminder_Date)=="object" ) if (Reminder_Date != null) rsNewAction.Fields("Reminder_Date") = formatDate(Reminder_Date);
		rsNewAction.Fields("Status") = Status;
		rsNewAction.Fields("Origin_Table") = Origin_Table;
		rsNewAction.Fields("Report_Id") = Report_Id;
		if (Section_Id != null) rsNewAction.Fields("Section_Id") = Section_Id;
		rsNewAction.Update;
		rsNewAction.Close();
		out("Action Creation Success:Action_Id="+Action_Id+":Register_Origin="+Register_Origin+":Action_Type="+Action_Type+":Action_Title="+Action_Title+":Action_Description="+Action_Description+":Scheduled_Date="+Scheduled_Date+":Managed_By_Id="+Managed_By_Id+":Allocated_To_Id="+Allocated_To_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Reminder_Date="+Reminder_Date+":Status="+Status+":Origin_Table="+Origin_Table+":Report_Id="+Report_Id+":Section_Id="+Section_Id);
		return true;
	}catch(e){
		strErrors += 'Errors::Function::createAction:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+
			"Action_Id="+Action_Id+":Register_Origin="+Register_Origin+":Action_Type="+Action_Type+":Action_Title="+Action_Title+":Action_Description="+Action_Description+":Scheduled_Date="+Scheduled_Date+":Managed_By_Id="+Managed_By_Id+":Allocated_To_Id="+Allocated_To_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Reminder_Date="+Reminder_Date+":Status="+Status+":Origin_Table="+Origin_Table+":Report_Id="+Report_Id+":Section_Id="+Section_Id+
			'\n------------------------------------------------------------\n\n';
		return false;
	}
}

function createSchedule(Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status){
	try{
		var rsNewSchedule = createRecordset("Select * from tblScheduler");
		rsNewSchedule.AddNew();
		rsNewSchedule.Fields("Scheduler_Id") = Scheduler_Id;
		rsNewSchedule.Fields("Register_Origin") = Register_Origin;
		rsNewSchedule.Fields("No_Of_Days_Notice") = No_Of_Days_Notice;
		rsNewSchedule.Fields("Action_Title") = Action_Title;
		rsNewSchedule.Fields("Scheduler_Description") = Scheduler_Description;
		rsNewSchedule.Fields("Completion_Date") = formatDate(Completion_Date);
		rsNewSchedule.Fields("Managed_By_Id") = Managed_By_Id;
		rsNewSchedule.Fields("Frequency") = Frequency;
		rsNewSchedule.Fields("Site_Id") = Site_Id;
		rsNewSchedule.Fields("Department_Id") = Department_Id;
		rsNewSchedule.Fields("Action_Raised_Date") = (typeof(Action_Raised_Date)=="object")?formatDate(Action_Raised_Date):null;
		rsNewSchedule.Fields("Status") = Status;
		rsNewSchedule.Fields("Origin_Table") = Origin_Table;
		rsNewSchedule.Fields("Report_Id") = Report_Id;
		rsNewSchedule.Fields("Section_Id") = Section_Id;
		rsNewSchedule.Update;
		rsNewSchedule.Close();
		out("Schedule creation success:Scheduler_Id="+Scheduler_Id+":Register_Origin="+Register_Origin+":Action_Title="+Action_Title+":Scheduler_Description="+Scheduler_Description+":Completed_Date="+Completed_Date+":Managed_By_Id="+Managed_By_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Action_Raised_Date="+Action_Raised_Date+":Frequency="+Frequency+":No_Of_Days_Notice="+No_Of_Days_Notice+":Report_Id="+Report_Id+":Section_Id="+Section_Id+":Status="+Status);
		return true;
	}catch(e){
		strErrors += 'Errors::Function::createSchedule:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+
			"Scheduler_Id="+Scheduler_Id+":Register_Origin="+Register_Origin+":Action_Title="+Action_Title+":Scheduler_Description="+Scheduler_Description+":Completed_Date="+Completed_Date+":Managed_By_Id="+Managed_By_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Action_Raised_Date="+Action_Raised_Date+":Frequency="+Frequency+":No_Of_Days_Notice="+No_Of_Days_Notice+":Report_Id="+Report_Id+":Section_Id="+Section_Id+":Status="+Status+
			'\n------------------------------------------------------------\n\n';
		return false;
	}
}
function updateSchedule(Scheduler_Id){
	var rsUpdateSchedule = createRecordset("SELECT * FROM tblScheduler WHERE Scheduler_Id = " + Scheduler_Id);
	var freq = rsUpdateSchedule.Fields.Item("Frequency").Value;
	var actRaisedDate = rsUpdateSchedule.Fields.Item("Action_Raised_Date").Value;
	var actCompDate = rsUpdateSchedule.Fields.Item("Completion_Date").Value;
	var noDaysNotice = rsUpdateSchedule.Fields.Item("No_Of_Days_Notice").Value;
	if(freq >0){
		actCompDate = dateAdd(actCompDate, (freq*30));
		actRaisedDate = dateMinus(actCompDate, noDaysNotice);
	} else {
		actRaisedDate = null;
	}

	rsUpdateSchedule.Edit;
	if(actRaisedDate != null){
		rsUpdateSchedule.Fields("Action_Raised_Date") = formatDate(actRaisedDate);
		rsUpdateSchedule.Fields("Completion_Date") = formatDate(actCompDate);
	}else{
		rsUpdateSchedule.Fields("Action_Raised_Date") = actRaisedDate;
		rsUpdateSchedule.Fields("Completion_Date") = actCompDate;
	}
	rsUpdateSchedule.Update;
	rsUpdateSchedule.Close();
}

function sendEmail(strEmp_Name, strEmail, intSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc){
	if(MM_EmailStatus != 0){// if MM_EmailStatus = 0 this means emailing is turned off
		//--------------------------------------------------------------------------
		var strRemoteHost = MM_EmailServer;
		var strFromName = MM_EmailFromName;
		var strFromAddress = MM_EmailFromAddress;
		//-----------------------------------------------------------------------

		//Send email to Action Manager
		// get the form variables to insert into the email message
		// create and send the managed by email
		var today = new Date();
		var fToday = FormatDateOut(today);
		strSubject = 'Action/s to Manage generated by Scheduler on: ' + fToday;
		strBody = 'You have been assigned to manage the following action/s:\n\n';
		for(var i=1;i<arAction_Id.length;i++){
			strBody += 'Action Title:       ' + arAction_Title[i] + '\n';
			strBody += 'Action Number:      ' + arAction_Id[i] + '\n';
			strBody += 'Action Description: ' + arAction_Desc[i] + '\n';
			strBody += 'Action Date Due:    ' + formatDate(arDue_Date[i]) + '\n';
		}

		//get the email from address for this persons site
		var strRSString = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + intSite;
		var rsSite_Details = createRecordset(strRSString);
		if(!rsSite_Details.EOF || !rsSite_Details.BOF){
			strFromAddress = rsSite_Details.Fields.Item("Email_From_Address").Value;
		}
		rsSite_Details.Close();

		if(MM_EmailStatus == 1){// if MM_EmailStatus = 1 this means all emails are getting sent to the person in MM_EmailTestAddress
			strFromAddress = MM_EmailFromAddress;
			strEmail = MM_EmailTestAddress;
			strEmp_Name = MM_EmailTestName;
		}

		//check that we have valid email addresses
		var tempAddress_1 = String(strFromAddress);
		var tempAddress_2 = String(strEmail);
		var IsEmail_1 = tempAddress_1.indexOf("@");
		var IsEmail_2 = tempAddress_2.indexOf("@");

		if(IsEmail_2 > 0 && IsEmail_1 > 0){
			try{
				var Mailer = getObject('SMTPsvg.Mailer');
			}catch(e){
				var Mailer = new objSendMailToOutput();
			}
			out('Email to '+strFromName+"~~~"+strFromAddress+"~~~"+strEmp_Name+"~~~"+strEmail+"~~~"+strSubject+"~~~"+strBody);

			Mailer.FromName = strFromName;
			Mailer.FromAddress = strFromAddress;
			Mailer.RemoteHost = strRemoteHost;
			Mailer.AddRecipient(strEmp_Name, strEmail);
			//if(!gbRunAtServerSide){
				Mailer.Subject	= strSubject;
				Mailer.BodyText   = strBody;
				Mailer.SendMail();
			//}
		}
	}//end if(MM_EmailStatus != 0)
}



/*****************************************************************/
// DB Functions below
/*****************************************************************/
function dbExecute(sql){
	try{
		var cmd = getObject('ADODB.Recordset');
		cmd.ActiveConnection = MM_cats_STRING;
		cmd.CommandText = sql;
		cmd.Execute();
		cmd.ActiveConnection.Close();
		return true;
	}catch(e){
		strErrors += 'Errors::Function::dbExecute:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+sql+'\n------------------------------------------------------------\n\n';
	}
}
function getRecordset(sql){
	var args = getRecordset.arguments;
	if(args.length>1){
		cursor_type=args[1];
	}else{
		cursor_type = 2;
	}
	
	if(args.length>2){
		lock_type=args[2];
	}else{
		lock_type = 3;
	}
	
	try{
		var rs = getObject('ADODB.Recordset');
		rs.ActiveConnection = connString;
		rs.Source = sql;
		rs.CursorType = cursor_type;
		rs.CursorLocation = 2;
		rs.LockType = 3;
		rs.Open();
		out("SQL Success:"+sql);
	}catch(e){
		strErrors += 'Errors::Function::getRecordset:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+sql+'\n------------------------------------------------------------\n\n';
		out(strErrors);//rs=rsError('Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+sql+'');
	}
	return rs;
}
function rsError(s){
	var rs=new Object();
		rs.EOF=true;
		rs.errors = s;
	return rs;
}
function getFieldValue(sql,field){
	var rs = getRecordset(sql);
	var ret = "";
	var id = (typeof(field)=="string")?field:0;
	if(!rs.EOF){
		ret = rs.Fields.Item(id).Value;
	}
	rs.Close();
	return ret;
}

function getNewId(id,tbl){
	if      ( tbl == 'tblAction_Details' )  var sql = "SELECT new_action_id new_id from dual";
	else if ( tbl == 'tblScheduler' )       var sql = "SELECT new_schedule_id new_id from dual";
	else var sql = "SELECT Unsupported_table new_id from dual";
	
	return getFieldValue(sql);
}

function isEmailAddress (string) {
  var addressPattern =
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  return addressPattern.test(string);
}
// Check the PCR actions for actions that are due
function CheckPCRActions(){
//	var sql = "select count(*) as cnt from tblPCR_Actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null ";
	var sql = "select pcr_action_cnt as cnt from dual ";
	var rsPCRActions = getRecordset(sql);
	var cnt = rsPCRActions.Fields.Item("cnt").Value;
	var i=0;
	rsPCRActions.Close();

	if(cnt>0){
		//sql = "select originator_id, pcr_date from tblpcr_details where pcr_date > '1-jan-2004' and pcr_date < (sysdate) and (originator_notified < (sysdate) or originator_notified is null) and overall_status = 'Open'";
		sql = "select distinct managed_by_id from tblPCR_actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null ";
		rsPCRActions = getRecordset(sql);
		cnt = rsPCRActions.RecordCount;
		var arManagers = new Array();//(cnt);
		while(!rsPCRActions.EOF){
			arManagers[i++] = rsPCRActions.Fields.Item('Managed_By_Id').Value;
			rsPCRActions.MoveNext();
		}

		rsPCRActions.Close();
		var Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description;
		var Scheduled_Date, Managed_By_Id, Site_Id, Department_Id, Reminder_Date, Status;
		var Origin_Table, Report_Id, Section_Id, EmailStatus, Stage_Id
		var arAction_Id, arAction_Title, arAction_Desc, arDue_Date;
		var execute_result;
		for(i=0;i<arManagers.length;i++){
			sql = "select * from tblPCR_actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null and Managed_By_Id = " + arManagers[i];
			rsPCRActions = getRecordset(sql);
			sql2 = "select pcr_action_notify_cnt("+arManagers[i]+") as cnt from dual";//"select count(*) as cnt from tblPCR_actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null and Managed_By_Id = " + arManagers[i];
			rsPCRActionsCnt = getRecordset(sql2);
			cnt = rsPCRActionsCnt.Fields.Item("cnt").Value;
			arAction_Id = new Array();//(cnt);
			arAction_Title = new Array();//(cnt);
			arAction_Desc = new Array();//(cnt);
			arDue_Date = new Array();//(cnt);
			var Managed_By_Id = arManagers[i];
			var j = 0;
			while(!rsPCRActions.EOF){
				Report_Id = rsPCRActions.Fields.Item("PCR_ID").Value;
				Action_Id = getNewId("Action_Id","tblAction_Details");
				Register_Origin = "PCR Action";
				Action_Type = "Planned";
				Stage_Id = rsPCRActions.Fields.Item("Stage_Id").Value;
				//"PCR Action:" + Action_Id + ";Stage:"+ Stage_Id + ";Item:"+ rsPCRActions.Fields.Item("Detail_Id").Value +" ";
				Action_Title = Action_Description = rsPCRActions.Fields.Item("Action_Description").Value;
				Scheduled_Date = new Date(rsPCRActions.Fields.Item("Due_Date").Value);
				Managed_By_Id = rsPCRActions.Fields.Item("Managed_By_Id").Value;
				Allocated_To_Id = rsPCRActions.Fields.Item("Allocated_To_Id").Value;
				Site_Id = rsPCRActions.Fields.Item("Site_Id").Value;
				Department_Id = getFieldValue("select Department_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
				Section_Id = getFieldValue("select Section_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
				//Status = rsPCRActions.Fields.Item("Status").Value;
				Status = 'Open';
				Origin_Table = "PCR_Actions";

				arAction_Id[j] = Action_Id;
				arAction_Title[j] = "PCR Action:" + Action_Id + ";Stage:"+ Stage_Id + ";Item:"+ rsPCRActions.Fields.Item("Detail_Id").Value +" ";
				arAction_Desc[j] = Action_Description;
				arDue_Date[j] = Scheduled_Date;

				if(createAction(Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description, Scheduled_Date, Managed_By_Id, Allocated_To_Id, Site_Id, Department_Id, Scheduled_Date, Status, Origin_Table, Report_Id, Section_Id)){
					rsPCRActions.Edit;
					//rsPCRActions.Fields("Status") = "Processed";
					rsPCRActions.Fields("Status") = "Open";
					rsPCRActions.Fields("action_register_id") = Action_Id;
					rsPCRActions.Fields("manager_notified") = formatDate(new Date());
					rsPCRActions.Update;
					//execute_result=dbExecute("insert into tblaction_checkboxvalues (ActionId, CheckBoxId, CheckBoxType) values ("+Action_Id+",(select checkbox_id from tblIncidentForm_Checkboxes WHERE Checkbox_Type = 'Incident_Category' and Checkbox_Desc = 'Other'),'ActionCategory') ");
				}
				j++;
				rsPCRActions.MoveNext();
			}
			rsPCRActions.Close();
			var rsEmail = getRecordset("select distinct employee_number, last_name, first_name, email_address, site_id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
			if(!rsEmail.EOF){
				var sEmp_Name = rsEmail.Fields.Item('First_Name').Value + ' ' + rsEmail.Fields.Item('Last_Name').Value;
				var sEmail = rsEmail.Fields.Item('Email_Address').Value;
				var iSite = rsEmail.Fields.Item('Site_Id').Value;
				sendPCRActionEmail(sEmp_Name, sEmail, iSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc);
			}else{
				strErrors += "No Employee details for ID:" + Managed_By_Id + "\n\n";
			}
			if(Allocated_To_Id>=0 && Allocated_To_Id!=Managed_By_Id){
				rsEmail = getRecordset("select distinct employee_number, last_name, first_name, email_address, site_id from tblEmployee_Details where Employee_Number = "+Allocated_To_Id);
				if(!rsEmail.EOF){
					var sEmp_Name = rsEmail.Fields.Item('First_Name').Value + ' ' + rsEmail.Fields.Item('Last_Name').Value;
					var sEmail = rsEmail.Fields.Item('Email_Address').Value;
					var iSite = rsEmail.Fields.Item('Site_Id').Value;
					sendPCRActionEmail(sEmp_Name, sEmail, iSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc);
				}else{
					strErrors += "No Employee details for ID:" + Allocated_To_Id + "\n\n";
				}
			}
			print_errors();
			rsEmail.Close();
		}
	}
}


function sendPCRActionEmail(strEmp_Name, strEmail, intSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc){
	if(MM_EmailStatus != 0){//only come in here if we are sending emails
		//Send email to Action Manager
		var today = new Date();
		var fToday = FormatDateOut(today);
		var i=0;
		var oMessages = getRecordset("select * from tblpcr_email where screen_id = (select screen_id from tblpcr_tabs where screen_name='Actions') and person_description = 'Managed By' and Site_id="+intSite);
		while(!oMessages.EOF){
			strSubject = oMessages.Fields.Item("Subject").Value;//'PCR Action/s to Manage generated by CATS-Pro Scheduler on: ' + fToday;
			strBody = oMessages.Fields.Item("Message").Value +'\n\n';//'You have been assigned to manage the following action/s:\n\n';
			for(var i=1;i<arAction_Id.length;i++){
				strBody += 'Action Title:\n	' + arAction_Title[i] + '\n';
				strBody += 'Action Number:\n	' + arAction_Id[i] + '\n';
				strBody += 'Action Description:\n	' + arAction_Desc[i] + '\n';
				strBody += 'Action Date Due:\n	' + formatDate(arDue_Date[i]) + '\n';

				var sLink = "Link: http://";
				sLink += MM_ServerPath;
				sLink += "Goto.asp?DEST=ActionRegisterEdit.asp?Action_Id=" + arAction_Id[i] + "\n\n";
				strBody += sLink;
			}

			//get the email from address for this persons site
			MM_EmailFromAddress = getFieldValue("SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + intSite);

			//if we are sending test emails
			if (MM_EmailStatus == 1) {
				strEmail = MM_EmailTestAddress;
				strEmp_Name = MM_EmailTestName;
				MM_EmailFromAddress = MM_EmailTestAddress;
			}

			//check that we have valid email addresses
			var tempAddress_1 = String(MM_EmailFromAddress);
			var tempAddress_2 = String(strEmail);
			var IsEmail_1 = tempAddress_1.indexOf("@");
			var IsEmail_2 = tempAddress_2.indexOf("@");

			if(IsEmail_2 > 0 && IsEmail_1 > 0){
				try{
					var Mailer = getObject('SMTPsvg.Mailer');
				}catch(e){
					var Mailer = new objSendMailToOutput();
				}
				Mailer.FromName = MM_EmailFromName;
				Mailer.FromAddress = String(MM_EmailFromAddress);
				Mailer.RemoteHost = MM_EmailServer;
				Mailer.AddRecipient(strEmp_Name, String(strEmail));
				//if(!gbRunAtServerSide){
					Mailer.Subject	= strSubject;
					Mailer.BodyText   = strBody;
					Mailer.SendMail();
				//}
			} else {
				//we don't have valid email addresses so we send an email with the error details
				if (Email_Errors) {//only if we are required to send the error email
					if (IsEmail_1 <= 0) {
						strErrors += 'Invalid Email Address (Employee Name:'+MM_EmailFromName+" - Email Address:"+tempAddress_1+')\n\n';
					}
					if(IsEmail_2 <= 0) {
						strErrors += 'Invalid Email Address (Employee Name:'+strEmp_Name+" - Email Address:"+tempAddress_2+')\n\n';
					}
					strErrors += 'For Message : '+strBody;
				} //fi Email_Errors
				bErrors = 1;
			} //fi invalid email addresses
			oMessages.MoveNext();
		}//
		print_errors();
	}//end if(MM_EmailStatus != 0)
}



function PostErrors() {
	if(MM_EmailStatus != 0){
		var SendIt = 0;
		var strSubject = 'CATS-Pro Action Creation Report';
		try{
			var Mailer = getObject('SMTPsvg.Mailer');
		}catch(e){
			var Mailer = new objSendMailToOutput();
		}
		if (strErrors != '') {
			Mailer.BodyText   = 'ERRORS\n\n'+strErrors;
			SendIt = Email_Errors;
		} else {
			var tmp = '';
			if (bErrors) {
				tmp = '(Email address errors exist but ignored)';
			}
			Mailer.BodyText   = 'Ran OK\n\n'+tmp;
			SendIt = Email_OK;
		}

		if (SendIt) {
			Mailer.FromName = MM_EmailFromName;
			Mailer.FromAddress = MM_EmailFromAddress;
			Mailer.RemoteHost = MM_EmailServer;
			Mailer.AddRecipient('CATS Admin', strAdminAddress);
			//if(!gbRunAtServerSide){
				Mailer.Subject	= strSubject;
				Mailer.SendMail();
			//}
		}
	}//end if(MM_EmailStatus != 0)
}

function objSendMailToOutput(){
	this.to=new Array();
	this.AddRecipient=function(name,address){this.to.push({"name":name,"email":address})}
	this.SendMail=function(){
		var s="<table border=1><caption align=left><b>Send Mail</b></caption>";
		var sTo="";
		for(i=0;i<this.to.length;i++){
			sTo+=''+this.to[i].name+" {"+this.to[i].email+"},";
		}
		if(this.FromName) s+="<tr><th align=left>From:</th><td>"+this.FromName+" "+"- "+this.FromAddress+"</td></tr>";
		if(sTo!="") s+="<tr><th align=left>To:</th><td>"+sTo+"</td></tr>";
		s+="<tr><th align=left>Subject:</th><td>"+this.Subject+"</td></tr>";
		s+="<tr><th align=left>Message:</th></tr><tr><td colspan=2>"+this.BodyText+"<hr /></td></tr></table>";
		this.to=new Array();
		out(s);
	}
}

function SendPCREmails(sql){
	var EmailFromAddress=MM_EmailFromAddress;
	var EmailFromName=MM_EmailFromName;
	var EmailToAddress=EmailToName="";
	var EmailRecipients = new Array();
	var Subject, BodyText;
	var update_table="";
	var update_field="";
	var update_sql="";
	var update_where="";
	var update_detail_where="";
	var update_detail_id="";
	var sErrors;
	var Current_Date = FormatDateOut(new Date());
	var rse,rsr,i_sql=idx=0;
	try{
		var Mailer = getObject('SMTPsvg.Mailer');
	}catch(e){
		var Mailer = new objSendMailToOutput();
	}

	for(i_sql=0;i_sql<sql.length;i_sql++){
		out("SQL["+i_sql+"]");
		rs=getRecordset(sql[i_sql]);
		if(rs["errors"]) {out(rs["errors"]);}
		//out(rs.EOF+":"+rs.BOF)
		if(!rs.EOF||!rs.BOF){
			//out("SQL["+i_sql+"]:"+sql[i_sql]);
			out("screen_name='"+rs.Fields.Item("Screen_Name").Value+"' and person_description = '"+rs.Fields.Item("Email_To").Value+"' and Site_id="+rs.Fields.Item("Site_Id").Value);
			while(!rs.EOF){
				rse=getRecordset("select Subject, Message from tblpcr_email where screen_id = (select screen_id from tblpcr_tabs where screen_name='"+rs.Fields.Item("Screen_Name").Value+"') and person_description = '"+rs.Fields.Item("Email_To").Value+"' and Site_id="+rs.Fields.Item("Site_Id").Value);
				if(!rse.EOF){
					update_table="";
					update_field="";
					update_sql="";
					update_detail_where="";
					update_detail_id="";
					Subject = rse.Fields.Item("Subject").Value;//'PCR Action/s to Manage generated by CATS-Pro Scheduler on: ' + fToday;
					BodyText = rse.Fields.Item("Message").Value +'\n\n';//'You have been assigned to manage the following action/s:\n\n';
					switch(rs.Fields.Item("Screen_Name").Value){
						case 'Details':
							update_table="tblPCR_details";
							update_field="originator_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value+" ";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRDetailsEdit.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Initial Reviews':
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRInitialReview.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Evaluation':
							//update_sql="update tblPCR_Evaluation set superintendent_notified = '"+Current_Date+"', engineer_notified = '"+Current_Date+"', originator_notified = '"+Current_Date+"' where pcr_id = "+rs.Fields.Item("PCR_ID").Value+" ";
							update_table="tblPCR_Evaluation";
							update_field="manager_notified";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCREvaluation.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Area Review':
							update_table="tblPCR_AreaReview_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRAreaReview.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'PCRT Review':
							update_table="tblPCR_PCRTReview_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRPCRTReview.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Extension':
							update_table="tblPCR_extension_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRExtension.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Actions':
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRActions.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Sign-Off':
							switch(rs.Fields.Item("Email_To").Value){
								case 'Originator':
									update_table="tblPCR_PostAudit";
									update_field="postauditor_notifed";
									break;
								case 'Post Auditor':
									update_table="tblPCR_PostAudit";
									update_field="postauditor_notifed";
									break;
								default:
									break;
							}
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRSignOff.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						case 'Post Audit':
							update_table="tblPCR_PostAudit";
							update_field="postauditor_notified";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRPostAudit.asp?Report_Id=" + rs.Fields.Item("PCR_ID").Value + "\n\n";
							break;
						default:
							BodyText += 'Error Occurred while compiling email headers:\nNo Screen selected of screen name does not exist!';
							break;
					}


					EmailRecipients=new Array();
					idx=0;
					// get email recipient addresses
					switch(rs.Fields.Item("Email_To").Value){
						case 'Originator':
							update_table="tblPCR_Details";
							update_field="originator_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select originator_id from tblPCR_Details where PCR_ID="+rs.Fields.Item("PCR_ID").Value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'PCRT Team':
							update_table="tblPCR_InitialReview_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							
							updateNotified("tblPCR_InitialReview", "pcrt_notified", update_where, "", "");
							
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where PCRT_Team is not null");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Evaluator':
							if(rs.Fields.Item("Screen_Name").Value=='Evaluation'){
								rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where (Engineer is not null) or (Superintendent is not null) or Employee_Number in (select originator_id from tblPCR_Details where PCR_ID="+rs.Fields.Item("PCR_ID").Value+") ");
								update_table="tblPCR_Evaluation";
								update_field="manager_notified";
								update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value
							}else{
								rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select evaluation_id from tblPCR_Details where PCR_ID="+rs.Fields.Item("PCR_ID").Value+") ");
								update_table="tblPCR_Details";
								update_field="evaluator_notified";
								update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							}
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Area Reviewer':
							update_table="tblPCR_AreaReview_Details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							
							updateNotified("tblPCR_AreaReview", update_field, update_where, "", "");
							
							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+rs.Fields.Item("PCR_ID").Value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select reviewer_id from tblPCR_AreaReview_Details where Required = 1 and PCR_ID = "+rs.Fields.Item("PCR_ID").Value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'PCRT Reviewer':
							update_table="tblPCR_PCRTReview_Details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							
							updateNotified("tblPCR_PCRTReview", update_field, update_where, "", "");
							
							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+rs.Fields.Item("PCR_ID").Value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select reviewer_id from tblPCR_PCRTReview_Details where Required = 1 and PCR_ID = "+rs.Fields.Item("PCR_ID").Value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Exention Reviewer':
							update_table="tblPCR_Extension_Details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+rs.Fields.Item("PCR_ID").Value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							
							updateNotified("tblPCR_Extension", update_field, update_where, "", "");
							
							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+rs.Fields.Item("PCR_ID").Value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select reviewer_id from tblPCR_Extension_Details where Required = 1 and PCR_ID = "+rs.Fields.Item("PCR_ID").Value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Managed By':
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where PCRT_Team is not null");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Engineer, Superintendent, Originator':
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where (Engineer is not null) or (Superintendent is not null) or Employee_Number in (select originator_id from tblPCR_Details where PCR_ID="+rs.Fields.Item("PCR_ID").Value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Engineer, Superintendent':
							//update_sql = "update tblPCR_PostAudit set postauditor_notifed = '"+Current_Date+"' where PCR_ID="+rs.Fields.Item("PCR_ID").Value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where (Engineer is not null) or (Superintendent is not null) ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Post Auditor':// This should happen only once...Create scheduled actions for the post audit
							var MM_editTable  = "tblScheduler";
						  var MM_fieldsStr = "Scheduler_Id|value|Register_Origin|value|Action_Title|value|Scheduler_Description|value|Completion_Date|value|Managed_By_Id|value|Site_Id_No|value|Dept_Id_No|value|Action_Raised_Date|value|Frequency|value|No_Of_Days_Notice|value|Report_Id|value|Section_Id|value|Status|value";
						  var MM_columnsStr = "Scheduler_Id|none,none,NULL|Register_Origin|',none,''|Action_Title|',none,''|Scheduler_Description|',none,''|Completion_Date|',none,NULL|Managed_By_Id|none,none,NULL|Site_Id|none,none,NULL|Department_Id|none,none,NULL|Action_Raised_Date|',none,NULL|Frequency|none,none,NULL|No_Of_Days_Notice|none,none,NULL|Report_Id|none,none,NULL|Section_Id|none,none,NULL|Status|',none,''";
	  					var Scheduler_Id=getNewId("Scheduler_Id","tblScheduler");
							var Register_Origin = "PCR Action";
							var Action_Title = "A PCR has been completed and signed off and a Post Audit is required";
							var Scheduler_Description = Action_Title;
							var Completion_Date = FormatDateOut(new Date(rs.Fields.Item("AUDIT_DATE").Value));
							var Managed_By_Id = rs.Fields.Item("AUDIT_COMPLETED_BY_ID").Value;
							var Site_Id = rs.Fields.Item("Site_Id").Value;
							var Department_Id = getFieldValue("select Department_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
							var Action_Raised_Date = dateMinus(new Date(rs.Fields.Item("AUDIT_DATE").Value),10);
							Action_Raised_Date = FormatDateOut(new Date(Action_Raised_Date));
							var Frequency = 1
							var No_Of_Days_Notice = 10
							var Report_Id = 0;
							var Section_Id = getFieldValue("select Section_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
							var Status = 'On';

							sErrors = createSchedule(Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status);
							/*var s="insert into tblScheduler "+
								"	(Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status) "+
								"	values "+
								"	("+Scheduler_Id+",'"+Register_Origin+"','"+Action_Title+"','"+Scheduler_Description+"','"+Completion_Date+"',"+Managed_By_Id+","+Site_Id+","+Department_Id+",'"+Action_Raised_Date+"',"+Frequency+","+No_Of_Days_Notice+","+Report_Id+","+Section_Id+",'"+Status+"') ";
							sErrors = dbExecute(s);*/
							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+rs.Fields.Item("PCR_ID").Value+" ";
							table_where = " where PCR_ID="+rs.Fields.Item("PCR_ID").Value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select audit_completed_by_id from tblPCR_PostAudit where audit_completed_by_id is not null and postauditor_notified is not null and PCR_ID="+rs.Fields.Item("PCR_ID").Value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						default:
							break;
					}
					out("update_table="+update_table+":update_field="+update_field+":update_where="+update_where+":update_detail_where="+update_detail_where+":update_detail_id="+update_detail_id);
					if(update_table!="" && update_field!="" && update_where!=""){
						
						// Update notified field in specified table
						updateNotified(update_table, update_field, update_where, update_detail_id, update_detail_where);
						//
						
					}
					//get the email from address for this persons site
					EmailFromAddress = getFieldValue("SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + rs.Fields.Item("SITE_ID").Value);
					//out(BodyText);

					//check that we have valid email addresses
					if(isEmailAddress(EmailFromAddress)){
						if(MM_EmailStatus != 0){
							Mailer.FromName = EmailFromName;
							Mailer.FromAddress = EmailFromAddress;
							Mailer.RemoteHost = MM_EmailServer;
							if(MM_EmailStatus == 1){
								Mailer.AddRecipient(MM_EmailTestName,MM_EmailTestAddress);
							}else{
								out("To:");
								for(iRecipients=0;iRecipients<EmailRecipients.length;iRecipients++){
									if(isEmailAddress(EmailRecipients[iRecipients].Value)){
										Mailer.AddRecipient(EmailRecipients[iRecipients].Name, EmailRecipients[iRecipients].Value);
										out(""+iRecipients+":"+EmailRecipients[iRecipients].Name + " " + EmailRecipients[iRecipients].Value);
									}else{
										strErrors += 'Invalid Email Address ('+EmailRecipients[iRecipients].Value+') for employee ('+EmailRecipients[iRecipients].Name+')\n\n';
									}
								}
							}
							//out(Subject);
							//out(BodyText);
							//if(!gbRunAtServerSide){
								Mailer.Subject	= Subject;
								Mailer.BodyText   = BodyText;
								Mailer.SendMail();
							//}
						}
					} else {
						//we don't have valid email addresses so we send an email with the error details
						if (Email_Errors) {//only if we are required to send the error email
							strErrors += 'Invalid Email From Address ('+EmailFromAddress+')\n\n';
							strErrors += 'For Message : '+BodyText;
						} //fi Email_Errors
					} //fi invalid email addresses
				}
				rs.MoveNext();
			}//END:while(!rs.EOF)
			print_errors();
		}
	}
}
function updateNotified(update_table, update_field, update_where, update_detail_id, update_detail_where){
	try{
		var rsUpdate = createRecordset("select * from "+update_table+update_where);
		if(rsUpdate.RecordCount>1){
			while(!rsUpdate.EOF){
				if(typeof(update_detail_where)!="undefined" && update_detail_where!="" && typeof(update_detail_id)!="undefined" && update_detail_id!=""){
					var sql_update_details="select * from "+update_table+update_detail_where+rsUpdate.Fields.Item(update_detail_id).Value;
					var rsUpdateDetail=createRecordset(sql_update_details);
					rsUpdateDetail.Edit;
					rsUpdateDetail.Fields(update_field) = formatDate(new Date());
					rsUpdateDetail.Update;
					rsUpdateDetail.Close();
					out("updated "+update_field+":"+update_detail_id+" from sql: "+sql_update_details+"");
					rsUpdate.MoveNext();
				}
			}
		}else{
			rsUpdate.Edit;
			rsUpdate.Fields(update_field) = formatDate(new Date());
			rsUpdate.Update;
			out("updated "+update_field+" from table: "+update_table+" where:"+update_where);
		}
		rsUpdate.Close();
	}catch(e){
		strErrors += 'Errors::Function::SendPCREmails:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+
			"Update Table="+update_table+"\nUpdate Field="+update_field+
			'\n------------------------------------------------------------\n\n';
	}
}
function CheckPCRDetails(){
	var sql;

	// get originator email messages
	sql=new Array(
		"select 'Details' as Screen_Name,'Originator' as Email_To, d.*, t.originator_notified from VIEW_PCR_DETAILS d, TBLPCR_DETAILS t where t.PCR_ID=d.PCR_ID and ((d.authoritystatus_1 is not null and d.authoritystatus_2 is not null) or (d.site_id=3)) and t.originator_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Details' as Screen_Name,'Evaluator' as Email_To, d.*, t.evaluator_notified from VIEW_PCR_DETAILS d, TBLPCR_DETAILS t where d.PCR_ID=t.PCR_ID and t.evaluator_notified is null and lower(d.overall_status) not like 'close%' and d.evaluation_id is not null and t.modified_date is not null ",
		"select 'Initial Reviews' as Screen_Name,'PCRT Team' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_InitialReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.pcrt_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Evaluation' as Screen_Name,'Evaluator' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_Evaluation t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.manager_notified is null and lower(d.overall_status) not like 'close%' and t.evaluation_name_id is not null and t.modified_date is not null ",
		"select 'Area Review' as Screen_Name,'Area Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_AreaReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'PCRT Review' as Screen_Name,'PCRT Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_PCRTReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Extension' as Screen_Name,'Exention Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_Extension t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Sign-Off' as Screen_Name,'Originator' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_SignOff t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and  t.originator_notified is null and lower(d.overall_status) not like 'close%' and t.sign_off is not null and t.modified_date is not null ",
		"select 'Sign-Off' as Screen_Name,'Post Auditor' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_PostAudit t, tblPCR_SignOff s, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and d.pcr_id=s.pcr_id and s.sign_off='Approve' and t.AUDIT_COMPLETED_BY_ID is not null and t.AUDIT_DATE is not null and t.postauditor_notified is null and lower(d.overall_status) not like 'close%' "
	);
	SendPCREmails(sql);
}

function DailyPCRCronJob(){
	out("BEGIN::CreateActions()");
		CreateActions();
	out("END::CreateActions()");

// 7/09/2005 12:22PM KLL - commented out while Vernon fixes errors	
//	out("BEGIN::CheckPCRDetails()");
//		CheckPCRDetails();
//	out("END::CheckPCRDetails()");
	
//	out("BEGIN::CheckPCRActions()");	
//		CheckPCRActions();
//	out("END::CheckPCRActions()");
	
	out("BEGIN::PostErrs()");
		PostErrors();
	out("END::PostErrs()");
}

if(!gbRunAtServerSide){
	DailyPCRCronJob();
}


function out(s){
	//AddToLog(s);
	if(gbRunAtServerSide){
		Response.Write(s+"<br>");
		Response.Flush();
	}else{
		WScript.StdErr.Write(s+CRLF);
	}
	//WScript.Echo(s);
}
function print_errors(){
	if(strErrors!=""){
		out(strErrors);
		strErrors = "";
	}
}