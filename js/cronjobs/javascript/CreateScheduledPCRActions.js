
//************************************************ Configuration Section ********************************************************
  // ############################################# Database connection string ################################################
  //CATSDEV - development database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSDEV; UID=catsdba; PWD=takerealcare;';

  //CATSTEST - Testing database
  var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSTST_DS1; UID=catsdba; PWD=catstst;';

  //CATSPROD - production database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSPRD_DS1; UID=catsdba; PWD=takecare;';
  var CRLF = "\r\n";
  var Email_OK = 0;
  var Email_Errors = 1;
	var bErrors = 0;
  var MM_cats_STRING = connString;
  // ############################################ End Database connection string ##############################################

  //########################## Email Confuguration ##########################
  /*
  var strRSString = "SELECT * FROM tblEmailOptions";
  var rsEmail = createRecordset(strRSString);
  var MM_EmailStatus = parseInt(rsEmail.Fields.Item("Email_Status").Value);
  var MM_EmailServer = rsEmail.Fields.Item("Email_Server").Value;
  var MM_EmailTestAddress = rsEmail.Fields.Item("Email_TestAddress").Value;
  var MM_EmailTestName = rsEmail.Fields.Item("Email_TestName").Value;
  var MM_EmailFromAddress = rsEmail.Fields.Item("Email_FromAddress").Value;
  var MM_EmailFromName = rsEmail.Fields.Item("Email_FromName").Value;
  rsEmail.Close();
  //*/
  //######################## End Email Confuguration #########################
	//MM_EmailStatus = 0

  //*
  var MM_EmailServer = 'perxs1'; // Email server name
  var MM_EmailFromName = 'CATS Agent Creator'; //From name to appear in any emails sent
  var MM_EmailFromAddress = 'serviceM@tiwest.com'; //address any reply emails will be sent to
  var MM_EmailTestAddress = 'kevinl@tronox.com'; //address to send test emails
  var MM_EmailTestName = "Kevin Lilje"; //name to put in test emails
  var MM_EmailStatus = 1; //0 = email turned off; 1 = send emails to MM_EmailTestAddress; 2 = send emails to proper person
  //*/
  
  var strErrors = "";
  var MM_ServerPath = "catstst/";
  var gbRunAtServerSide = false;
  var gbTestRun = false;
//******************************************** End Configuration Section ***********************************************************

function FormatDateOut(ufDate){
	if(typeof(ufDate)=="string"){
		//if(ufDate.indexOf("-"))
		return String(ufDate);

	}
	var aMonth = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	var DD = ufDate.getDate();
	var MMM = aMonth[ufDate.getMonth()];
	var YYYY = ufDate.getFullYear();
	var fDate = DD + '-' + MMM + '-' + YYYY;
	return fDate;
}

function formatDate(ufDate){
	try{
		var DD = ufDate.getDate();
		var MM = ufDate.getMonth()+1;
		var YYYY = ufDate.getYear();
		var fDate = DD + '/' + MM + '/' + YYYY;
		return fDate;
	}catch(e){
		var s='';s += 'Errors::Function::formatDate('+ufDate+'):returning date string:\n\n';
		s += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\n------------------------------------------------------------\n\n';
		out(s);
		strErrors+=s;
		return ufDate;
	}
}

function dateAdd(start, days){
	// get the milliseconds for this Date object.
	try{
		var buffer = Date.parse(start);
		days *= 24; // days to hours
		days *= 60; // hours to minutes
		days *= 60; // minutes to seconds
		days *= 1000; // seconds to milliseconds
		var newDate = new Date(buffer + days);
		return newDate;
	}catch(e){
		var s='';s += 'Errors::Function::dateAdd('+start+','+days+'):\n\n';
		s += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\n------------------------------------------------------------\n\n';
		out(s);
		strErrors+=s;
		return null;
	}
}

function dateMinus(start, days){
	// get the milliseconds for this Date object.
	var buffer = Date.parse(start);
	days *= 24; // days to hours
	days *= 60; // hours to minutes
	days *= 60; // minutes to seconds
	days *= 1000; // seconds to milliseconds
	var newDate = new Date(buffer - days);
	return newDate;
}

function getObject(id){
	if(gbRunAtServerSide){
		if(gbTestRun && id.toLowerCase()=='smtpsvg.mailer') return new objSendMailToOutput();
		return Server.CreateObject(id);
	}else{
		return new ActiveXObject(id);
	}
}
function createRecordset(strSource,type){
	//var MM_cats_STRING = 'dsn=CATS;uid=catsdba;pwd=takecare';
	//var MM_cats_STRING = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATS; UID=catsdba; PWD=takecare;';
	var MM_cats_STRING = connString;
	var rs = getObject('ADODB.Recordset');
	rs.ActiveConnection = MM_cats_STRING;
	rs.Source = strSource;
	if (!isNaN(type)) rs.CursorType = type;
	rs.CursorLocation = 2;
	rs.LockType = 3;
	try{
		rs.Open();
	}catch(e){
		out(strSource);
	}
	return rs;
}

function generateNewId(){
	return getNewId("new_action_id");
}

function createAction(Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description, Scheduled_Date, Managed_By_Id, Allocated_To_Id, Site_Id, Department_Id, Reminder_Date, Status, Origin_Table, Report_Id, Section_Id){
	out("TRACE-->createAction->("+print_arguments(createAction.arguments,"Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description, Scheduled_Date, Managed_By_Id, Allocated_To_Id, Site_Id, Department_Id, Reminder_Date, Status, Origin_Table, Report_Id, Section_Id")+")\n");
	try{
		var rsNewAction = createRecordset("Select * from tblAction_Details",3);
		rsNewAction.AddNew();
		rsNewAction.Fields("Action_Id") = Action_Id;
		rsNewAction.Fields("Register_Origin") = Register_Origin;
		rsNewAction.Fields("Action_Type") = Action_Type;
		rsNewAction.Fields("Action_Title") = Action_Title;
		rsNewAction.Fields("Action_Description") = Action_Description;
		rsNewAction.Fields("Scheduled_Date") = formatDate(Scheduled_Date);
		rsNewAction.Fields("Managed_By_Id") = Managed_By_Id;
		rsNewAction.Fields("Allocated_To_Id") = Allocated_To_Id;
		rsNewAction.Fields("Site_Id") = Site_Id;
		rsNewAction.Fields("Department_Id") = Department_Id;
		if ( typeof(Reminder_Date)=="object" ) if (Reminder_Date != null) rsNewAction.Fields("Reminder_Date") = formatDate(Reminder_Date);
		rsNewAction.Fields("Status") = Status;
		rsNewAction.Fields("Origin_Table") = Origin_Table;
		rsNewAction.Fields("Report_Id") = Report_Id;
		if (Section_Id != null) rsNewAction.Fields("Section_Id") = Section_Id;
		rsNewAction.Update;
		rsNewAction.Close();
		out("Action Creation Success:Action_Id="+Action_Id+":Register_Origin="+Register_Origin+":Action_Type="+Action_Type+":Action_Title="+Action_Title+":Action_Description="+Action_Description+":Scheduled_Date="+Scheduled_Date+":Managed_By_Id="+Managed_By_Id+":Allocated_To_Id="+Allocated_To_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Reminder_Date="+Reminder_Date+":Status="+Status+":Origin_Table="+Origin_Table+":Report_Id="+Report_Id+":Section_Id="+Section_Id);
		return true;
	}catch(e){
		strErrors += 'Errors::Function::createAction:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+
			"Action_Id="+Action_Id+":Register_Origin="+Register_Origin+":Action_Type="+Action_Type+":Action_Title="+Action_Title+":Action_Description="+Action_Description+":Scheduled_Date="+Scheduled_Date+":Managed_By_Id="+Managed_By_Id+":Allocated_To_Id="+Allocated_To_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Reminder_Date="+Reminder_Date+":Status="+Status+":Origin_Table="+Origin_Table+":Report_Id="+Report_Id+":Section_Id="+Section_Id+
			'\n------------------------------------------------------------\n\n';
		return false;
	}
}

function createSchedule(Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status){
/*
SCHEDULER_ID
REGISTER_ORIGIN
SCHEDULER_DESCRIPTION
COMPLETION_DATE
MANAGED_BY_ID
SITE_ID
DEPARTMENT_ID
ACTION_RAISED_DATE
FREQUENCY
NO_OF_DAYS_NOTICE
REPORT_ID
ACTION_TITLE
SECTION_ID 
STATUS
*/
	out("TRACE-->createSchedule->("+print_arguments(createSchedule.arguments,"Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status")+")\n");
	try{
		out("createSchedule::"+Scheduler_Id+"::Completion_Date="+Completion_Date+":Action_Raised_Date="+Action_Raised_Date);
        var junk = (typeof(Action_Raised_Date)=="object")?formatDate(Action_Raised_Date):null;
        out('--KLKL-->'+junk+'<--KLKL--');
		var rsNewSchedule = createRecordset("Select * from tblScheduler");
		rsNewSchedule.AddNew();
		rsNewSchedule.Fields("Scheduler_Id") = Scheduler_Id;
		rsNewSchedule.Fields("Register_Origin") = Register_Origin;
		rsNewSchedule.Fields("No_Of_Days_Notice") = No_Of_Days_Notice;
		rsNewSchedule.Fields("Action_Title") = Action_Title;
		rsNewSchedule.Fields("Scheduler_Description") = Scheduler_Description;
		rsNewSchedule.Fields("Completion_Date") = Completion_Date
		rsNewSchedule.Fields("Managed_By_Id") = Managed_By_Id;
		rsNewSchedule.Fields("Frequency") = Frequency;
		rsNewSchedule.Fields("Site_Id") = Site_Id;
		rsNewSchedule.Fields("Department_Id") = Department_Id;
		rsNewSchedule.Fields("Action_Raised_Date") = (typeof(Action_Raised_Date)=="object")?formatDate(Action_Raised_Date):null;
		rsNewSchedule.Fields("Status") = Status;
		rsNewSchedule.Fields("Report_Id") = Report_Id;
		rsNewSchedule.Fields("Section_Id") = Section_Id;
		rsNewSchedule.Update;
		rsNewSchedule.Close();
		out("Schedule creation success:Scheduler_Id="+Scheduler_Id+":Register_Origin="+Register_Origin+":Action_Title="+Action_Title+":Scheduler_Description="+Scheduler_Description+":Completion_Date="+Completion_Date+":Managed_By_Id="+Managed_By_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Action_Raised_Date="+Action_Raised_Date+":Frequency="+Frequency+":No_Of_Days_Notice="+No_Of_Days_Notice+":Report_Id="+Report_Id+":Section_Id="+Section_Id+":Status="+Status);
		return true;
	}catch(e){
		strErrors += 'Errors::Function::createSchedule:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+
			"Scheduler_Id="+Scheduler_Id+":Register_Origin="+Register_Origin+":Action_Title="+Action_Title+":Scheduler_Description="+Scheduler_Description+":Completion_Date="+Completion_Date+":Managed_By_Id="+Managed_By_Id+":Site_Id="+Site_Id+":Department_Id="+Department_Id+":Action_Raised_Date="+Action_Raised_Date+":Frequency="+Frequency+":No_Of_Days_Notice="+No_Of_Days_Notice+":Report_Id="+Report_Id+":Section_Id="+Section_Id+":Status="+Status+
			'\n------------------------------------------------------------\n\n';
		return false;
	}
}

function updateSchedule(Scheduler_Id){
	var rsUpdateSchedule = createRecordset("SELECT * FROM tblScheduler WHERE Scheduler_Id = " + Scheduler_Id);
	var freq = rsUpdateSchedule.Fields.Item("Frequency").Value;
	var actRaisedDate = rsUpdateSchedule.Fields.Item("Action_Raised_Date").Value;
	var actCompDate = rsUpdateSchedule.Fields.Item("Completion_Date").Value;
	var noDaysNotice = rsUpdateSchedule.Fields.Item("No_Of_Days_Notice").Value;
	if(freq >0){
		actCompDate = dateAdd(actCompDate, (freq*30));
		actRaisedDate = dateMinus(actCompDate, noDaysNotice);
	} else {
		actRaisedDate = null;
	}

	rsUpdateSchedule.Edit;
	if(actRaisedDate != null){
		rsUpdateSchedule.Fields("Action_Raised_Date") = formatDate(actRaisedDate);
		rsUpdateSchedule.Fields("Completion_Date") = formatDate(actCompDate);
	}else{
		rsUpdateSchedule.Fields("Action_Raised_Date") = actRaisedDate;
		rsUpdateSchedule.Fields("Completion_Date") = actCompDate;
	}
	rsUpdateSchedule.Update;
	rsUpdateSchedule.Close();
}



/*****************************************************************/
// DB Functions below
/*****************************************************************/
function dbExecute(sql){
	try{
		var cmd = getObject('ADODB.Recordset');
		cmd.ActiveConnection = MM_cats_STRING;
		cmd.CommandText = sql;
		cmd.Execute();
		cmd.ActiveConnection.Close();
		return true;
	}catch(e){
		strErrors += 'Errors::Function::dbExecute:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+sql+'\n------------------------------------------------------------\n\n';
	}
}

function getRecordCount(rs){
	if(rs.EOF && rs.BOF) return 0;
	var cnt=rs.RecordCount;
	if(cnt==-1) for(cnt=0; rs.EOF; rs.MoveNext()) cnt++;
	// reset the cursor to the beginning
  if (rs.CursorType > 0) {
    if (!rs.BOF) rs.MoveFirst();
  } else {
    rs.Requery();
  }
  return cnt;
}

function getRecordset(sql,type){

	try{
		var rs = getObject('ADODB.Recordset');
		rs.ActiveConnection = connString;
		rs.Source = sql;
		rs.CursorType = (!isNaN(type))?parseInt(type):0;
		rs.CursorLocation = 2;
		rs.LockType = 3;
		rs.Open();
		out("SQL Success:"+sql);
	}catch(e){
		strErrors += 'Errors::Function::getRecordset:\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+sql+'\n------------------------------------------------------------\n\n';
		out(strErrors);//rs=rsError('Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+sql+'');
	}
	return rs;
}
function rsError(s){
	var rs=new Object();
		rs.EOF=true;
		rs.errors = s;
	return rs;
}
function getFieldValue(sql,field){
	var rs = getRecordset(sql);
	var ret = "";
	var id = (typeof(field)=="string")?field:0;
	if(!rs.EOF){
		ret = rs.Fields.Item(id).Value;
	}
	rs.Close();
	return ret;
}
function getNewId(id_function){
	var sql = "SELECT "+id_function+" NEW_ID FROM DUAL";
	return getFieldValue(sql);
}

function isEmailAddress (string) {
  var addressPattern =
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  return addressPattern.test(string);
}

// uses action_confirmed!='Y'
function stage_not_complete_sql(){
	var args=arguments;
	return "select "+args[0]+" from tblPCR_Actions a1 where (select count(a3.detail_id) from tblPCR_Actions a3 where a3.pcr_id=a1.pcr_id and a3.stage_id=a1.stage_id) = (select count(a2.detail_id) from tblPCR_Actions a2 where a2.pcr_id=a1.pcr_id and a2.stage_id=a1.stage_id and a2.action_confirmed!='Y' and a2.action_register_id > 0) ";
}

// uses action_confirmed='Y'
function stage_complete_sql(){
	var args=arguments;
	return "select "+args[0]+" from tblPCR_Actions a1 where (select count(a3.detail_id) from tblPCR_Actions a3 where a3.pcr_id=a1.pcr_id and a3.stage_id=a1.stage_id) = (select count(a2.detail_id) from tblPCR_Actions a2 where a2.pcr_id=a1.pcr_id and a2.stage_id=a1.stage_id and a2.action_confirmed='Y' and a2.action_register_id > 0) ";
}

function get_stages_complete(){
	// return recordset containing distinct pcr_id of complete actions
	var args=arguments;
	var sql=stage_complete_sql("distinct a1.pcr_id");
	sql+=(args.length>0)?" and a1.pcr_id = " + args[0] + " ":"";
	sql+=(args.length>1)?" and a1.stage_id in(" + args[1] + ") ":"";
	trace("get_stages_complete::sql",sql);
	return getRecordset(sql);
}

function is_stage_complete(){
	// return the amount of actions complete
	var args=arguments;
	var sql=stage_complete_sql("count(*) as c");
	sql+=(args.length>0)?" and a1.pcr_id = " + args[0] + " ":"";
	sql+=(args.length>1)?" and a1.stage_id in(" + args[1] + ") ":"";
	trace("is_stage_complete::sql",sql);
	return (getFieldValue(sql,"c")>0);
	//return (getFieldValue("select count(*) as c from tblPCR_Actions where pcr_id="+pcr_id+" and stage_id in("+stage_ids+") and (action_confirmed='N' or action_confirmed is null) ","c")==0);
}

function CheckPCRActionsComplete(){
	var rs=get_stages_complete();
	var sql=new Array();
	var stage_id,pcr_id;
	while(!rs.EOF){
		for(stage_id=1; stage_id<=3;stage_id++){
			pcr_id=rs.Fields.Item("pcr_id").Value;
			if(is_stage_complete(pcr_id, stage_id)){
				//get the email from address for this persons site
				sql[sql.length]="select 'Actions' as Screen_Name,'Engineer, Superintendent, Originator' as Email_To, t.*, d.* from tblPCR_Actions t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and d.PCR_ID = "+pcr_id+" and t.stage_id="+stage_id+" and t.stage"+stage_id+"_notified is null ";
			}
		}
		rs.MoveNext();
	}
	if(sql.length>0) SendPCREmails(sql);
	else out("No complete stages to report");
}

// Check the PCR actions for actions that are due
function CheckPCRActions(){
	//var sql = "select count(*) as cnt from tblPCR_Actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null ";
	var sql = "select pcr_action_cnt as cnt from dual ";
	var rsPCRActions = getRecordset(sql);
	var cnt = rsPCRActions.Fields.Item("cnt").Value;
	var i=0;
	rsPCRActions.Close();

	if(cnt>0){
		//sql = "select originator_id, pcr_date from tblpcr_details where pcr_date > '1-jan-2004' and pcr_date < (sysdate) and (originator_notified < (sysdate) or originator_notified is null) and overall_status = 'Open'";
		//sql = "select distinct managed_by_id from tblPCR_actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null ";
		// 20051206 - removed manager_notified is null to allow stage(n)_notified records thru
		sql = "select distinct managed_by_id from tblPCR_actions where (status is null or status like 'pend%') and action_register_id is null ";
		rsPCRActions = getRecordset(sql);
		cnt = getRecordCount(rsPCRActions);
		var arManagers = new Array();//(cnt);
		while(!rsPCRActions.EOF){
			arManagers[i++] = rsPCRActions.Fields.Item('Managed_By_Id').Value;
			rsPCRActions.MoveNext();
		}

		rsPCRActions.Close();
		var Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description;
		var Scheduled_Date, Managed_By_Id, Site_Id, Department_Id, Reminder_Date, Status;
		var Origin_Table, Report_Id, Section_Id, EmailStatus, Stage_Id, days_after, raise_now
		var arAction_Id, arAction_Title, arAction_Desc, arDue_Date;
		var execute_result;
		var d_today = new Date();
		for(i=0;i<arManagers.length;i++){
			//sql = "select * from tblPCR_actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null and Managed_By_Id = " + arManagers[i] + " order by pcr_id, stage_id ";
			// 20051206 - removed manager_notified is null to allow for stage(n)_notified records to come through
			sql = "select * from tblPCR_actions where (status is null or status like 'pend%') and action_register_id is null and Managed_By_Id = " + arManagers[i] + " order by pcr_id, stage_id ";
			rsPCRActions = getRecordset(sql,2);
			var sql2 = "select pcr_action_notify_cnt("+arManagers[i]+") as cnt from dual";//"select count(*) as cnt from tblPCR_actions where manager_notified is null and (status is null or status like 'pend%') and action_register_id is null and Managed_By_Id = " + arManagers[i];
			rsPCRActionsCnt = getRecordset(sql2);
			cnt = rsPCRActionsCnt.Fields.Item("cnt").Value;

			//cnt = getRecordCount(rsPCRActions);
			trace("CheckPCRACtions::array_count",cnt);
			/*
			arAction_Id = new Array(cnt);
			arAction_Title = new Array(cnt);
			arAction_Desc = new Array(cnt);
			arDue_Date = new Array(cnt);
			*/
			arAction_Id = new Array();
			arAction_Title = new Array();
			arAction_Desc = new Array();
			arDue_Date = new Array();
			var Managed_By_Id = arManagers[i];
			var j = 0;
			var b_skip=false;
			while(!rsPCRActions.EOF){
				b_skip=false; // create an action by default::false=create action or true to skip action creation
				Report_Id = rsPCRActions.Fields.Item("PCR_ID").Value;
				Action_Id = getNewId("new_action_id");
				Register_Origin = "PCR Action";
				Action_Type = "Planned";
				Stage_Id = rsPCRActions.Fields.Item("Stage_Id").Value;
				days_after = rsPCRActions.Fields.Item("Days_After_Prev_Stage").Value;
				raise_now = rsPCRActions.Fields.Item("Raise_Now").Value == 'Y';
				switch(Stage_Id){
					case 1:
						b_skip=false;
						Scheduled_Date = new Date(rsPCRActions.Fields.Item("Due_Date").Value);
						break;
					case 2:
						// if raise now is set then create the action regardless
						// else only create actions if all actions are complete for the previous stage
						if(raise_now==false){
							b_skip=!is_stage_complete(Report_Id, (Stage_Id-1)); // check stage 1 before continue
						}
						Scheduled_Date = dateAdd(d_today, days_after);
						trace("CheckPCRActions::Switch::Stage 2","Scheduled_Date after dateAdd("+d_today+","+days_after+"): Stage 2");
						break;
					case 3:
						// if raise now is set then create the action regardless
						// else only create actions if all actions are complete for the previous stage
						if(raise_now==false){
							b_skip=!is_stage_complete(Report_Id, (Stage_Id-1)); // check stages 1 and 2 before continue
						}
						Scheduled_Date = dateAdd(d_today, days_after);
						trace("CheckPCRActions::Switch::Stage 3","Scheduled_Date after dateAdd("+d_today+","+days_after+"): Stage 3");
						break;
					default:
						strErrors+="No stage id for the pcr action:"+Action_Id;
						b_skip=true;
						break;
				}
				if(b_skip){
					// skip this record
					trace("CheckPCRActions::Skipping Record","PCR:"+Report_Id+";\nAction:" + Action_Id + ";\nTitle:"+rsPCRActions.Fields.Item("Action_Description").Value+";\nStage:"+ Stage_Id + ";\nItem:"+ rsPCRActions.Fields.Item("Detail_Id").Value +";");
				}else{
					//"PCR Action:" + Action_Id + ";Stage:"+ Stage_Id + ";Item:"+ rsPCRActions.Fields.Item("Detail_Id").Value +" ";
					Action_Title = Action_Description = rsPCRActions.Fields.Item("Action_Description").Value;
					//Scheduled_Date = new Date(rsPCRActions.Fields.Item("Due_Date").Value);
					Managed_By_Id = rsPCRActions.Fields.Item("Managed_By_Id").Value;
					Allocated_To_Id = rsPCRActions.Fields.Item("Allocated_To_Id").Value;
					Site_Id = rsPCRActions.Fields.Item("Site_Id").Value;
					Department_Id = getFieldValue("select Department_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
					Section_Id = getFieldValue("select Section_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
					//Status = rsPCRActions.Fields.Item("Status").Value;
					Status = 'Open';
					Origin_Table = "PCR_Actions";

					arAction_Id[j] = Action_Id;
					arAction_Title[j] = getFieldValue("select title from tblpcr_details where pcr_id="+rsPCRActions.Fields.Item("PCR_ID").Value);//"PCR Action:" + Action_Id + ";Stage:"+ Stage_Id + ";Item:"+ rsPCRActions.Fields.Item("Detail_Id").Value +" ";
					arAction_Desc[j] = Action_Description;
					arDue_Date[j] = Scheduled_Date;

					trace("CheckPCRACtions::arActionTitle",arAction_Title[j]);

					if(String(Action_Id)=="undefined") {
						out("Errors:sendPCREmails::CheckPCRActions:Action_id is undefined:arAction_Title[]="+arAction_Title.join());
					}
					out("Creating an action in sendPCREmails::CheckPCRActions")
					if(createAction(Action_Id, Register_Origin, Action_Type, Action_Title, Action_Description, Scheduled_Date, Managed_By_Id, Allocated_To_Id, Site_Id, Department_Id, d_today, Status, Origin_Table, Report_Id, Section_Id)){
						rsPCRActions.Edit;
						//rsPCRActions.Fields("Status") = "Processed";
						rsPCRActions.Fields("Status") = "Open";
						rsPCRActions.Fields("action_register_id") = Action_Id;
						rsPCRActions.Fields("manager_notified") = formatDate(d_today);
						rsPCRActions.Fields("stage"+Stage_Id+"_notified") = formatDate(d_today);
						rsPCRActions.Fields("due_date") = formatDate(Scheduled_Date);
						rsPCRActions.Update;
						//execute_result=dbExecute("insert into tblaction_checkboxvalues (ActionId, CheckBoxId, CheckBoxType) values ("+Action_Id+",(select checkbox_id from tblIncidentForm_Checkboxes WHERE Checkbox_Type = 'Incident_Category' and Checkbox_Desc = 'Other'),'ActionCategory') ");
					}
					j++;
				}
				rsPCRActions.MoveNext();
			}
			rsPCRActions.Close();
			if(j > 0){
				var rsManagedByEmail = getRecordset("select LAST_NAME||', '||FIRST_NAME as Name, Email_Address, Site_id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
				var sEmp_Name = sEmail = "";
				var iSite=0;
				if(!rsManagedByEmail.EOF){
					var sEmp_Name = rsManagedByEmail.Fields.Item('Name').Value;
					var sEmail = rsManagedByEmail.Fields.Item('Email_Address').Value;
					var iSite = rsManagedByEmail.Fields.Item('Site_Id').Value;
				}else{
					strErrors += "No Employee details for ID:" + Manager_By_Id + "\n\n";
				}

				var rsAllocatedToEmail = getRecordset("select LAST_NAME||', '||FIRST_NAME as Name, Email_Address from tblEmployee_Details where Employee_Number = "+Allocated_To_Id);
				var sAll_Name = sAllEmail = "";

				if(!rsAllocatedToEmail.EOF){
					sAll_Name = rsAllocatedToEmail.Fields.Item('Name').Value;
					sAllEmail = rsAllocatedToEmail.Fields.Item('Email_Address').Value;
				}else{
					strErrors += "No Employee details for ID:" + Manager_By_Id + "\n\n";
				}


				sendPCRActionEmail(sEmp_Name, sEmail, sAll_Name, sAllEmail, iSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc);

				print_errors();
				rsManagedByEmail.Close();
				rsAllocatedToEmail.Close();
			}
		}
	}
}
function get_email_messages(screen_name,email_group,site_id){
	if(site_id)
		return getRecordset("select * from tblpcr_email where screen_id = (select screen_id from tblpcr_tabs where screen_name='"+screen_name+"') and person_description = '"+email_group+"' and Site_id="+site_id);
}
function print_arguments(args,names){
	names=(typeof(names)=="string")?names.split(","):false;
	var s="";
	for(i=0;i<args.length;i++){
		s+=((names)?names[i]:""+i)+'='+args[i]+'\n';
	}
	return s;
}
function sendPCRActionEmail(ManagedByName, ManagedByEmail, AllocatedToName, AllocatedToEmail, intSite, arAction_Id, arAction_Title, arDue_Date, arAction_Desc){
	trace("sendPCRActionEmail",print_arguments(sendPCRActionEmail.arguments,"ManagedByName,ManagedByEmail,AllocatedToName,AllocatedToEmail,intSite,arAction_Id,arAction_Title,arDue_Date,arAction_Desc"));
	if(MM_EmailStatus != 0){//only come in here if we are sending emails
		//Send email to Action Manager
		var today = new Date();
		//var fToday = FormatDateOut(today);
		var i=0;
		var rsCount = 1;
		var oMessages = get_email_messages("Actions","Managed By",intSite);
		while(!oMessages.EOF){
			strSubject = oMessages.Fields.Item("Subject").Value;//'PCR Action/s to Manage generated by CATS-Pro Scheduler on: ' + fToday;
			strBody = oMessages.Fields.Item("Message").Value +'\n\n';//'You have been assigned to manage the following action/s:\n\n';
			//trace("rsCount",rsCount++);
			trace("sendPCRActionEmail::ID Array[Length]",arAction_Id.length+":"+arAction_Id.join(",")+":"+arAction_Title.join(",")+"::rsCount="+(rsCount++)+"\n<br>");
			var sLink="";

			trace("arAction.typeof",typeof(arAction_Title)+":"+arAction_Title.length)

			for(var i=0;i<arAction_Id.length;i++){
				strBody += 'PCR Title:\n	' + arAction_Title[i] + '\n';
				strBody += 'Action Title:\n	' + arAction_Desc[i] + '\n';
				//strBody += 'Action Title:\n	' + arAction_Title[i] + '\n';
				strBody += 'Action Number:\n	' + arAction_Id[i] + '\n';
				strBody += 'Action Date Due:\n	' + formatDate(arDue_Date[i]) + '\n';

				sLink = "Link: http://";
				sLink += MM_ServerPath;
				sLink += "Goto.asp?DEST=ActionRegisterEdit.asp?Action_Id=" + arAction_Id[i] + "\n\n";
				strBody += sLink;
			}


			if(sLink!=''){
				//get the email from address for this persons site
				MM_EmailFromAddress = getFieldValue("SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + intSite);

				//if we are sending test emails
				if (MM_EmailStatus == 1) {
					ManagedByEmail = MM_EmailTestAddress;
					ManagedByName = MM_EmailTestName;
					MM_EmailFromAddress = MM_EmailTestAddress;
				}

				try{
					var Mailer = getObject('SMTPsvg.Mailer');
				}catch(e){
					var Mailer = new objSendMailToOutput();
				}

				var b_FromEmail = isEmailAddress(String(MM_EmailFromAddress));
				var b_ManagedByEmail = isEmailAddress(String(ManagedByEmail));
				var b_AllocatedToEmail = isEmailAddress(String(AllocatedToEmail));

				out("b_FromEmail="+b_FromEmail+":b_AllocatedToEmail="+b_AllocatedToEmail+":ManagedByEmail="+b_ManagedByEmail);

				if(!b_ManagedByEmail){
					ManagedByEmail = MM_EmailFromAddress;
				}
				if(!b_AllocatedToEmail){
					AllocatedToEmail = MM_EmailFromAddress;
				}

				if(b_FromEmail){
					Mailer.FromName = MM_EmailFromName;
					Mailer.FromAddress = String(MM_EmailFromAddress);
					Mailer.RemoteHost = MM_EmailServer;

					if(b_ManagedByEmail){
						Mailer.AddRecipient(ManagedByName, String(ManagedByEmail));
					}

					if(b_AllocatedToEmail){
						Mailer.AddRecipient(AllocatedToName, String(AllocatedToEmail));
					}
					printSendMail(AllocatedToName+" "+AllocatedToEmail+";"+ManagedByName+" "+ManagedByEmail+";",MM_EmailFromAddress,strSubject,strBody);
					//if(!gbRunAtServerSide){
						Mailer.Subject	= strSubject;
						Mailer.BodyText   = strBody;
						Mailer.SendMail();
					//}

				} else {

					//we don't have valid email addresses so we send an email with the error details
					if (Email_Errors) {//only if we are required to send the error email
						if (!b_FromEmail) {
							strErrors += 'Invalid Email Address (Employee Name:'+MM_EmailFromName+" - Email Address:"+tempAddress_1+')\n\n';
						}
						if(!b_ManagedByEmail) {
							strErrors += 'Invalid Email Address (Employee Name:'+strEmp_Name+" - Email Address:"+tempAddress_2+')\n\n';
						}

						if(!b_AllocatedToEmail) {
							strErrors += 'Invalid Email Address (Employee Name:'+strEmp_Name+" - Email Address:"+tempAddress_2+')\n\n';
						}

						strErrors += 'For Message : '+strBody;
					} //fi Email_Errors
					bErrors = 1;
				} //fi invalid email addresses
			}//fi sLink is empty
			oMessages.MoveNext();
		}//
		print_errors();
	}//end if(MM_EmailStatus != 0)
}



function PostErrors() {
	if(MM_EmailStatus != 0){
		var SendIt = 0;
		var strSubject = 'CATS-Pro Action Creation Report';
		try{
			var Mailer = getObject('SMTPsvg.Mailer');
		}catch(e){
			var Mailer = new objSendMailToOutput();
		}
		if (strErrors != '') {
			Mailer.BodyText   = 'ERRORS\n\n'+strErrors;
			SendIt = Email_Errors;
		} else {
			var tmp = '';
			if (bErrors) {
				tmp = '(Email address errors exist but ignored)';
			}
			Mailer.BodyText   = 'Ran OK\n\n'+tmp;
			SendIt = Email_OK;
		}

		if (SendIt) {
			Mailer.FromName = MM_EmailFromName;
			Mailer.FromAddress = MM_EmailFromAddress;
			Mailer.RemoteHost = MM_EmailServer;
			Mailer.AddRecipient('CATS Admin', strAdminAddress);
			printSendMail(strAdminAddress,MM_EmailFromAddress,strSubject,Mailer.BodyText);
			//if(!gbRunAtServerSide){
				Mailer.Subject	= strSubject;
				Mailer.SendMail();
			//}
		}
	}//end if(MM_EmailStatus != 0)
}


function printSendMail(to,from,Subject,BodyText){
	var s="<table border=1><caption align=left><b>Send Mail</b></caption>";
	var sTo=to;
	var i=0;
	/*
	for(i=0;i<to.length;i++){
		sTo+=''+to+" {"+to[i].email+"},";
	}*/
	if(from) s+="<tr><th align=left>From:</th><td>"+from+"</td></tr>";
	if(sTo!="") s+="<tr><th align=left>To:</th><td>"+sTo+"</td></tr>";
	s+="<tr><th align=left>Subject:</th><td>"+Subject+"</td></tr>";
	s+="<tr><th align=left>Message:</th></tr><tr><td colspan=2>"+BodyText+"<hr /></td></tr></table>";
	out(s);
}



function objSendMailToOutput(){
	this.to=new Array();
	this.AddRecipient=function(name,address){this.to.push({"name":name,"email":address})}
	this.SendMail=function(){
		var s="<table border=1><caption align=left><b>Send Mail</b></caption>";
		var sTo="";
		for(i=0;i<this.to.length;i++){
			sTo+=''+this.to[i].name+" {"+this.to[i].email+"},";
		}
		if(this.FromName) s+="<tr><th align=left>From:</th><td>"+this.FromName+" "+"- "+this.FromAddress+"</td></tr>";
		if(sTo!="") s+="<tr><th align=left>To:</th><td>"+sTo+"</td></tr>";
		s+="<tr><th align=left>Subject:</th><td>"+this.Subject+"</td></tr>";
		s+="<tr><th align=left>Message:</th></tr><tr><td colspan=2>"+this.BodyText+"<hr /></td></tr></table>";
		this.to=new Array();
		out(s);
	}
}

function SendPCREmails(sql){
	var EmailFromAddress=MM_EmailFromAddress;
	var EmailFromName=MM_EmailFromName;
	var EmailToAddress=EmailToName="";
	var EmailRecipients = new Array();
	var Subject, BodyText;
	var update_table="";
	var update_field="";
	var update_sql="";
	var update_where="";
	var update_detail_where="";
	var update_detail_id="";
	var sErrors;
	var Current_Date = FormatDateOut(new Date());
	var rse,rsr,i_sql=idx=0;
	var Scheduler_Id=getNewId("new_schedule_id");

	for(i_sql=0;i_sql<sql.length;i_sql++){
		out("SQL["+i_sql+"]");
		rs=getRecordset(sql[i_sql]);
		if(rs["errors"]) {out(rs["errors"]);}
		//out(rs.EOF+":"+rs.BOF)
		if(!rs.EOF||!rs.BOF){
			var email_to_value = rs.Fields.Item("Email_To").Value;
			var screen_name_value = rs.Fields.Item("Screen_Name").Value;
			var pcr_id_value = rs.Fields.Item("PCR_ID").Value;
			var site_id_value = rs.Fields.Item("SITE_ID").Value;
			out("screen_name='"+screen_name_value+"' and person_description = '"+email_to_value+"' and Site_id="+rs.Fields.Item("Site_Id").Value);
			while(!rs.EOF){
				rse=get_email_messages(screen_name_value, email_to_value, rs.Fields.Item("Site_Id").Value);
				//rse=getRecordset("select Subject, Message from tblpcr_email where screen_id = (select screen_id from tblpcr_tabs where screen_name='"+rs.Fields.Item("Screen_Name").Value+"') and person_description = '"+rs.Fields.Item("Email_To").Value+"' and Site_id="+rs.Fields.Item("Site_Id").Value);
				pcr_id_value = rs.Fields.Item("PCR_ID").Value;
				site_id_value = rs.Fields.Item("SITE_ID").Value;
				if(!rse.EOF){
					update_table="";
					update_field="";
					update_sql="";
					update_detail_where="";
					update_detail_id="";
					Subject = rse.Fields.Item("Subject").Value;//'PCR Action/s to Manage generated by CATS-Pro Scheduler on: ' + fToday;
					BodyText = rse.Fields.Item("Message").Value +'\n\n';//'You have been assigned to manage the following action/s:\n\n';

					out("SendPCREmails::Get message body for pcr:"+pcr_id_value);

					switch(screen_name_value){
						case 'Details':
							update_table="tblPCR_details";
							update_field="originator_notified";
							update_where=" where pcr_id = "+pcr_id_value+" ";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRDetailsEdit.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'Initial Reviews':
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRInitialReview.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'Evaluation':
							//update_sql="update tblPCR_Evaluation set superintendent_notified = '"+Current_Date+"', engineer_notified = '"+Current_Date+"', originator_notified = '"+Current_Date+"' where pcr_id = "+rs.Fields.Item("PCR_ID").Value+" ";
							update_table="tblPCR_Evaluation";
							update_field="manager_notified";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCREvaluation.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'Area Review':
							update_table="tblPCR_AreaReview_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRAreaReview.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'PCRT Review':
							update_table="tblPCR_PCRTReview_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRPCRTReview.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'Extension':
							update_table="tblPCR_extension_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRExtension.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'Actions':
							out("SendPCREmails::Begin: message construction");

							update_table="tblPCR_Actions";
							update_field="stage"+rs.Fields.Item("stage_id").Value+"_notified";
							update_where=" where pcr_id = "+pcr_id_value+" and stage_id = "+ rs.Fields.Item("stage_id").Value ;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+' - Stage '+rs.Fields.Item("stage_id").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRActions.asp?Report_Id=" + pcr_id_value + "\n\n";
							BodyText += '---------------------------------------------\n';
							BodyText += '- Below is a list of Actions completed for this stage -\n\n';
							out("SendPCREmails::Begin: action details messages construction");
							while(!rs.EOF){
								BodyText += '---------------------------------------------\n';
								BodyText += '- ACTION COMPLETE -\n';
								BodyText += '---------------------------------------------\n';
								BodyText += 'Action Title:\n	' + rs.Fields.Item("action_description").Value + '\n';
								BodyText += 'Action Number:\n	' + rs.Fields.Item("detail_id").Value + '\n';
								BodyText += 'Action Date Due:\n	' + rs.Fields.Item("due_date").Value + '\n';
								BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=ActionRegisterEdit.asp?Action_Id=" + rs.Fields.Item("DETAIL_ID").Value + "\n\n";
								rs.MoveNext();
							}
							out("SendPCREmails::End: action details messages construction");
							//if(rs.EOF && !rs.BOF) rs.MovePrevious();
							break;
						case 'Sign-Off':
							switch(email_to_value){
								case 'Originator':
									update_table="tblPCR_PostAudit";
									update_field="postauditor_notifed";
									break;
								case 'Post Auditor':
									update_table="tblPCR_PostAudit";
									update_field="postauditor_notifed";
									break;
								default:
									break;
							}
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRSignOff.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						case 'Post Audit':
							update_table="tblPCR_PostAudit";
							update_field="postauditor_notified";
							BodyText += 'PCR No.:\n'+rs.Fields.Item("PCR_DisplayId").Value+'\n';
							BodyText += 'Type of PCR:\n'+rs.Fields.Item("PCR_Type").Value+'\n';
							BodyText += 'Criticality:\n'+rs.Fields.Item("ImpactLevel").Value+'\n';
							BodyText += 'Title:\n'+rs.Fields.Item("Title").Value+'\n';
							BodyText += 'Status:\n'+rs.Fields.Item("Overall_Status").Value+'\n';
							BodyText += "Link: http://" + MM_ServerPath + "Goto.asp?DEST=PCR/PCRPostAudit.asp?Report_Id=" + pcr_id_value + "\n\n";
							break;
						default:
							BodyText += 'Error Occurred while compiling email headers:\nNo Screen selected of screen name does not exist!';
							break;
					}
					out("SendPCREmails::Complete: message body for pcr:"+pcr_id_value);

					out("SendPCREmails::Get email recipients for pcr:"+pcr_id_value);

					EmailRecipients=new Array();
					idx=0;
					// get email recipient addresses
					switch(email_to_value){
						case 'Originator':

							if(screen_name_value=='Sign-Off'){
								update_table="tblPCR_SignOff";
								update_field="originator_notified";
							}else{
								update_table="tblPCR_Details";
								update_field="originator_notified";
							}
							update_where=" where pcr_id = "+pcr_id_value
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select originator_id from tblPCR_Details where PCR_ID="+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'PCRT Team':
							update_table="tblPCR_InitialReview_details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;

							updateNotified("tblPCR_InitialReview", "pcrt_notified", update_where, "", "");

							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where PCRT_Team = 1");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Evaluator':
							if(screen_name_value == 'Evaluation'){
								rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where (Engineer = 1 ) or (Superintendent = 1) or Employee_Number in (select originator_id from tblPCR_Details where PCR_ID="+pcr_id_value+") ");
								update_table="tblPCR_Evaluation";
								update_field="manager_notified";
								update_where=" where pcr_id = "+pcr_id_value
							}else{
								rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select evaluation_id from tblPCR_Details where PCR_ID="+pcr_id_value+") ");
								update_table="tblPCR_Details";
								update_field="evaluator_notified";
								update_where=" where pcr_id = "+pcr_id_value;
							}
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Area Reviewer':
							update_table="tblPCR_AreaReview_Details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";

							updateNotified("tblPCR_AreaReview", update_field, update_where, "", "");

							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+pcr_id_value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select reviewer_id from tblPCR_AreaReview_Details where Required = 1 and PCR_ID = "+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'PCRT Reviewer':
							update_table="tblPCR_PCRTReview_Details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";

							updateNotified("tblPCR_PCRTReview", update_field, update_where, "", "");

							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+pcr_id_value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select reviewer_id from tblPCR_PCRTReview_Details where Required = 1 and PCR_ID = "+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Exention Reviewer':
							update_table="tblPCR_Extension_Details";
							update_field="reviewer_notified";
							update_where=" where pcr_id = "+pcr_id_value;
							update_detail_where=update_where+" and detail_id=";
							update_detail_id="detail_id";

							updateNotified("tblPCR_Extension", update_field, update_where, "", "");

							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+pcr_id_value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select reviewer_id from tblPCR_Extension_Details where Required = 1 and PCR_ID = "+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Managed By':
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where PCRT_Team = 1");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							break;
						case 'Engineer, Superintendent, Originator': // pcr action stage complete email to send
							out("SendPCREmails::Begin: email recipients construction");

							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where ((Engineer = 1) or (Superintendent = 1)) and site_id = (select Site_id from tblPCR_Details where PCR_ID="+pcr_id_value+") or Employee_Number in (select originator_id from tblPCR_Details where PCR_ID="+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							// Send extra emails to admin assistant to notify them of actions
							// they need to confirm
							if(site_id_value == 3){ // if PI
								idx=EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = "Admin Assistant";
								EmailRecipients[idx].Value = "BIAA@tronox.com";
							}
							//
							out("SendPCREmails::End: email recipients construction");
							break;
						case 'Engineer, Superintendent':
							//update_sql = "update tblPCR_PostAudit set postauditor_notifed = '"+Current_Date+"' where PCR_ID="+pcr_id_value+" ";
							//rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where (Engineer = 1) or (Superintendent = 1) ");
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where ((Engineer = 1) or (Superintendent = 1)) and site_id = (select Site_id from tblPCR_Details where PCR_ID="+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							// Send extra emails to admin assistant to notify them of actions
							// they need to confirm
							if(site_id_value == 3){ // if PI
								idx=EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = "Admin Assistant";
								EmailRecipients[idx].Value = "BIAA@tronox.com";
							}
							break;
						case 'Post Auditor':// This should happen only once...Create scheduled actions for the post audit
							var MM_editTable  = "tblScheduler";
						  var MM_fieldsStr = "Scheduler_Id|value|Register_Origin|value|Action_Title|value|Scheduler_Description|value|Completion_Date|value|Managed_By_Id|value|Site_Id_No|value|Dept_Id_No|value|Action_Raised_Date|value|Frequency|value|No_Of_Days_Notice|value|Report_Id|value|Section_Id|value|Status|value";
						  var MM_columnsStr = "Scheduler_Id|none,none,NULL|Register_Origin|',none,''|Action_Title|',none,''|Scheduler_Description|',none,''|Completion_Date|',none,NULL|Managed_By_Id|none,none,NULL|Site_Id|none,none,NULL|Department_Id|none,none,NULL|Action_Raised_Date|',none,NULL|Frequency|none,none,NULL|No_Of_Days_Notice|none,none,NULL|Report_Id|none,none,NULL|Section_Id|none,none,NULL|Status|',none,''";
	  					var Scheduler_Id=getNewId("new_schedule_id");
							var Register_Origin = "PCR Action";
							var Action_Title = "A PCR has been completed and signed off and a Post Audit is required";
							var Scheduler_Description = Action_Title;
							var Completion_Date = FormatDateOut(new Date(rs.Fields.Item("AUDIT_DATE").Value));
							//var Completion_Date = new Date(rs.Fields.Item("AUDIT_DATE").Value);
							var Managed_By_Id = rs.Fields.Item("AUDIT_COMPLETED_BY_ID").Value;
							var Site_Id = rs.Fields.Item("Site_Id").Value;
							var Department_Id = getFieldValue("select Department_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
							var Action_Raised_Date = dateMinus(new Date(rs.Fields.Item("AUDIT_DATE").Value),10);
							//Action_Raised_Date = FormatDateOut(new Date(Action_Raised_Date));
							var Frequency = 1
							var No_Of_Days_Notice = 10
							var Report_Id = 0;
							var Section_Id = getFieldValue("select Section_Id from tblEmployee_Details where Employee_Number = "+Managed_By_Id);
							var Status = 'On';

							sErrors = createSchedule(Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status);
							/*var s="insert into tblScheduler "+
								"	(Scheduler_Id,Register_Origin,Action_Title,Scheduler_Description,Completion_Date,Managed_By_Id,Site_Id,Department_Id,Action_Raised_Date,Frequency,No_Of_Days_Notice,Report_Id,Section_Id,Status) "+
								"	values "+
								"	("+Scheduler_Id+",'"+Register_Origin+"','"+Action_Title+"','"+Scheduler_Description+"','"+Completion_Date+"',"+Managed_By_Id+","+Site_Id+","+Department_Id+",'"+Action_Raised_Date+"',"+Frequency+","+No_Of_Days_Notice+","+Report_Id+","+Section_Id+",'"+Status+"') ";
							sErrors = dbExecute(s);*/
							update_sql = "update "+update_table+" set "+update_field+" = '"+Current_Date+"' where PCR_ID="+pcr_id_value+" ";
							table_where = " where PCR_ID="+pcr_id_value+" ";
							rsr = getRecordset("select  distinct Employee_number, FIRST_NAME||' '||LAST_NAME as Name, EMAIL_ADDRESS as Email from tblEmployee_Details where Employee_Number in (select audit_completed_by_id from tblPCR_PostAudit where audit_completed_by_id is not null and postauditor_notified is not null and PCR_ID="+pcr_id_value+") ");
							while(!rsr.EOF){
								idx = EmailRecipients.length;
								EmailRecipients[idx] = new Object();
								EmailRecipients[idx].Name = rsr.Fields.Item("Name").Value;
								EmailRecipients[idx].Value = rsr.Fields.Item("Email").Value;
								rsr.MoveNext();
							}
							//Scheduler_Id++;
							break;
						default:
							break;
					}

					// Debug info: so we know exactly which case this comes from
					BodyText += "[Message Info]\nScreen:"+screen_name_value+"\nNotify:"+email_to_value+"\n\n";// Let us know the screen name value and email to value

					if(EmailRecipients.length>0){
						out("SendPCREmails::Complete:Email recipients for pcr:"+pcr_id_value);

						out("update_table="+update_table+":update_field="+update_field+":update_where="+update_where+":update_detail_where="+update_detail_where+":update_detail_id="+update_detail_id);
						if(update_table!="" && update_field!="" && update_where!=""){

							// Update notified field in specified table
							updateNotified(update_table, update_field, update_where, update_detail_id, update_detail_where);
							//

						}
						//get the email from address for this persons site
						EmailFromAddress = getFieldValue("SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + site_id_value);
						//out(BodyText);

						//check that we have valid email addresses
						if(isEmailAddress(EmailFromAddress)){
							if(MM_EmailStatus != 0){

								try{
									var Mailer = getObject('SMTPsvg.Mailer');
								}catch(e){
									var Mailer = new objSendMailToOutput();
								}

								Mailer.FromName = EmailFromName;
								Mailer.FromAddress = EmailFromAddress;
								Mailer.RemoteHost = MM_EmailServer;

								sEmailRecipients = "";

								if(MM_EmailStatus == 1){
									Mailer.AddRecipient(MM_EmailTestName,MM_EmailTestAddress);
								}else{
									out("To:"+EmailRecipients.length+EmailRecipients[0]);

									for(iRecipients=0;iRecipients<EmailRecipients.length;iRecipients++){
										if(isEmailAddress(EmailRecipients[iRecipients].Value)){
											Mailer.AddRecipient(EmailRecipients[iRecipients].Name, EmailRecipients[iRecipients].Value);
											sEmailRecipients += EmailRecipients[iRecipients].Name + " " + EmailRecipients[iRecipients].Value +";";
											out(""+iRecipients+":"+EmailRecipients[iRecipients].Name + " " + EmailRecipients[iRecipients].Value);
										}else{
											strErrors += 'Invalid Email Address ('+EmailRecipients[iRecipients].Value+') for employee ('+EmailRecipients[iRecipients].Name+')\n\n';
										}
									}
								}
								//out(Subject);
								//out(BodyText);
								//if(!gbRunAtServerSide){
								printSendMail(sEmailRecipients,EmailFromAddress,Subject,BodyText)
								Mailer.Subject	= Subject;
								Mailer.BodyText   = BodyText;
								Mailer.SendMail();
								//}
							}
						}
					} else {
						//we don't have valid email addresses so we send an email with the error details
						if (Email_Errors) {//only if we are required to send the error email
							strErrors += 'Invalid Email From Address ('+EmailFromAddress+')\n\n';
							strErrors += 'For Message : '+BodyText;
						} //fi Email_Errors
					} //fi invalid email addresses
				}
				if(!rs.EOF) rs.MoveNext();
			}//END:while(!rs.EOF)
			print_errors();
		}
	}
}
function updateNotified(update_table, update_field, update_where, update_detail_id, update_detail_where){
	try{
		var sql = "select * from "+update_table+update_where;
		out("updateNotified::SQL="+sql);
		var rsUpdate = getRecordset(sql,1);
		var cnt=getRecordCount(rsUpdate);

		out("updateNotified::cnt="+cnt);

		if(cnt>1){
			while(!rsUpdate.EOF){
				if(typeof(update_detail_where)!="undefined" && update_detail_where!="" && typeof(update_detail_id)!="undefined" && update_detail_id!=""){
					var sql_update_details="select * from "+update_table+update_detail_where+rsUpdate.Fields.Item(update_detail_id).Value;
					var rsUpdateDetail=getRecordset(sql_update_details,2);
					rsUpdateDetail.Edit;
					rsUpdateDetail.Fields(update_field) = formatDate(new Date());
					rsUpdateDetail.Update;
					rsUpdateDetail.Close();
					out("updated "+update_field+":"+update_detail_id+" from sql: "+sql_update_details+"");
					rsUpdate.MoveNext();
				}
			}
		}else{
			rsUpdate.Edit;
			rsUpdate.Fields(update_field) = formatDate(new Date());
			rsUpdate.Update;
			out("updated "+update_field+" from table: "+update_table+" where:"+update_where);
		}
		rsUpdate.Close();
	}catch(e){
		strErrors += 'Errors::Function::SendPCREmails::updateNotified\n\n';
		strErrors += 'Error Number:\n'+e.number+'\nError Description:\n'+e.description+'\nSQL:\n'+
			"Update Table="+update_table+"\nUpdate Field="+update_field+"\nupdate_where="+update_where+"\nupdate_detail_id="+update_detail_id+"\nupdate_detail_where="+update_detail_where+
			'\n------------------------------------------------------------\n\n';
	}
}

function CheckPCRDetails(){
	var sql;

	// get originator email messages
	sql=new Array(
		"select 'Details' as Screen_Name,'Originator' as Email_To, d.*, t.originator_notified from VIEW_PCR_DETAILS d, TBLPCR_DETAILS t where t.PCR_ID=d.PCR_ID and ((d.authoritystatus_1 is not null and d.authoritystatus_2 is not null) or (d.site_id=3)) and t.originator_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Details' as Screen_Name,'Evaluator' as Email_To, d.*, t.evaluator_notified from VIEW_PCR_DETAILS d, TBLPCR_DETAILS t where d.PCR_ID=t.PCR_ID and t.evaluator_notified is null and lower(d.overall_status) not like 'close%' and d.evaluation_id is not null and t.modified_date is not null ",
		"select 'Initial Reviews' as Screen_Name,'PCRT Team' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_InitialReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.pcrt_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Evaluation' as Screen_Name,'Evaluator' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_Evaluation t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.manager_notified is null and lower(d.overall_status) not like 'close%' and t.evaluation_name_id is not null and t.modified_date is not null ",
//		"select 'Area Review' as Screen_Name,'Area Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_AreaReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
//		"select 'PCRT Review' as Screen_Name,'PCRT Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_PCRTReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null and t.type_of_review <> 'PCRT Meeting'",
		"select 'Extension' as Screen_Name,'Exention Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_Extension t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null and t.type_of_review <> 'PCRT'",
		"select 'Sign-Off' as Screen_Name,'Originator' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_SignOff t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and  t.originator_notified is null and lower(d.overall_status) not like 'close%' and t.sign_off is not null and t.modified_date is not null ",
		"select 'Sign-Off' as Screen_Name,'Post Auditor' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_PostAudit t, tblPCR_SignOff s, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and d.pcr_id=s.pcr_id and s.sign_off='Approve' and t.AUDIT_COMPLETED_BY_ID is not null and t.AUDIT_DATE is not null and t.postauditor_notified is null and lower(d.overall_status) not like 'close%' "
	);
	SendPCREmails(sql);
}

function DailyPCRCronJob(){
	out("BEGIN::CheckPCRDetails()");
		CheckPCRDetails();
	out("END::CheckPCRDetails()");

	out("BEGIN::CheckPCRActions()");
		CheckPCRActions();
	out("END::CheckPCRActions()");

	out('CheckPCRActionsComplete begin');
		CheckPCRActionsComplete();
	out('CheckPCRActionsComplete complete');

	out("BEGIN::PostErrs()");
		PostErrors();
	out("END::PostErrs()");
}


function out(s){
	//AddToLog(s);
	if(gbRunAtServerSide){
		Response.Write(s+"<br>");
		Response.Flush();
	}else{
		WScript.StdErr.Write(s+CRLF);
	}
	//WScript.Echo(s);
}
function trace(name,s){
	out("\nTRACE-->"+name+"->("+s+")<br>\n");
}
function print_errors(){
	if(strErrors!=""){
		out(strErrors);
		strErrors = "";
	}
}



// Start the cron
DailyPCRCronJob();

