//************************************************ Configuration Section ********************************************************
  // ############################################# Database connection ################################################
  //CATSDEV - development database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSDEV; UID=catsdba; PWD=takerealcare;';

  //CATSPROD - production database
  //var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSTST_DS1; UID=catsdba; PWD=takecare;';
  
  //CATSTST
  var connString = 'Provider=MSDASQL; DRIVER={Microsoft ODBC for Oracle}; SERVER=CATSTST_DS1; UID=catsdba; PWD=catstst;';

  
  // ############################################ End Database connection ##############################################

  //########################## Email Confuguration ##########################
  /*
  var strRSString = "SELECT * FROM tblEmailOptions";
  var rsEmail = createRecordset(strRSString);
  var MM_EmailStatus = parseInt(rsEmail.Fields.Item("Email_Status").Value);
  var MM_EmailServer = rsEmail.Fields.Item("Email_Server").Value;
  var MM_EmailTestAddress = rsEmail.Fields.Item("Email_TestAddress").Value;
  var MM_EmailTestName = rsEmail.Fields.Item("Email_TestName").Value;
  var MM_EmailFromAddress = rsEmail.Fields.Item("Email_FromAddress").Value;
  var MM_EmailFromName = rsEmail.Fields.Item("Email_FromName").Value;
  rsEmail.Close();
  //*/
  //######################## End Email Confuguration #########################
	//MM_EmailStatus = 0

  //*
  var MM_EmailServer = 'perxs1'; // Email server name
  var MM_EmailFromName = 'CATS Agent Creator'; //From name to appear in any emails sent
  var MM_EmailFromAddress = 'serviceM@tiwest.com'; //address any reply emails will be sent to
  var MM_EmailTestAddress = 'kevinl@tronox.com'; //address to send test emails
  var MM_EmailTestName = "Kevin Lilje"; //name to put in test emails
  var MM_EmailStatus = 1; //0 = email turned off; 1 = send emails to MM_EmailTestAddress; 2 = send emails to proper person
  //*/
  
//******************************************** End Configuration Section ***********************************************************

function formatDate(ufDate){
	var DD = ufDate.getDate();
	var MM = ufDate.getMonth()+1;
	var YYYY = ufDate.getYear();
	var fDate = DD + '/' + MM + '/' + YYYY;
	return fDate;
}

function createRecordset(strSource){
	var MM_cats_STRING = connString;
	var rs = new ActiveXObject('ADODB.Recordset');
	rs.ActiveConnection = MM_cats_STRING;
	rs.Source = strSource;
	rs.CursorType = 3;
	rs.CursorLocation = 2;
	rs.LockType = 3;
	rs.Open();
	return rs;
}

function sendEmail(strEmp_Name, strEmail, arAction_Id, arAction_Title, arDue_Date, MessageType, intSite){
  	//--------------------------------------------------------------------------
	var strRemoteHost = MM_EmailServer;
	var strFromName = MM_FromName;
	var strFromAddress = MM_FromAddress;
	//-----------------------------------------------------------------------

    //Create the email message
	var today = new Date();
	var fToday = formatDate(today);
	if(MessageType == "ManagedBy"){
		strSubject = 'Reminder of Your Managed Actions to complete as of: ' + fToday;
		strBody = 'This is a reminder that you have been assigned to manage the following action/s:\n\n';
	}else{
		strSubject = 'Reminder of Your Allocated Actions to complete as of: ' + fToday;
		strBody = 'This is a reminder that you have been allocated to carry out the following action/s:\n\n';
	}
	for(var i=1;i<arAction_Id.length;i++){
		strBody += 'Action Title:  ' + arAction_Title[i] + '\n';
		strBody += 'Action Number: ' + arAction_Id[i] + '\n';
		strBody += 'Date Due:      ' + formatDate(arDue_Date[i]) + '\n\n';
	}

	//get the email from address for this persons site
	var strRSString = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " + intSite;
	var rsSite_Details = createRecordset(strRSString);
	if(!rsSite_Details.EOF || !rsSite_Details.BOF){
		strFromAddress = rsSite_Details.Fields.Item("Email_From_Address").Value;
	}
	rsSite_Details.Close();

    if(MM_EmailStatus == 1){// if MM_EmailStatus = 1 this means all emails are getting sent to the person in MM_TestAddress
		strFromAddress = MM_FromAddress;
		strEmail = MM_TestAddress;
		strEmp_Name = MM_TestName;
	}

	var tempAddress_1 = String(strEmail);
	var tempAddress_2 = String(strFromAddress);
	IsEmail_1 = tempAddress_1.indexOf("@");
	IsEmail_2 = tempAddress_2.indexOf("@");

	if(IsEmail_1 > 0 && IsEmail_2 > 0){
		//Send the email to the action manager
		var Mailer = new ActiveXObject('SMTPsvg.Mailer');
		Mailer.FromName = strFromName;
		Mailer.FromAddress = strFromAddress;
		Mailer.RemoteHost = strRemoteHost;
		Mailer.AddRecipient(strEmp_Name, strEmail);
		Mailer.Subject    = strSubject;
		Mailer.BodyText   = strBody;
		Mailer.SendMail();
	}
}

function SendReminders(){
	var strSQL = "SELECT * FROM VIEW_REMINDER_EMAIL";
	var rsActionManagers = createRecordset(strSQL);
	var ActionCount = rsActionManagers.RecordCount;
	rsActionManagers.Close();
	if(ActionCount>0){ // there are reminders that need to be sent
		strSQL = "SELECT DISTINCT Managed_By_Id FROM VIEW_REMINDER_EMAIL";
		var rsActionManagers = createRecordset(strSQL);
		ActionCount = rsActionManagers.RecordCount;
		var arManagers = new Array(ActionCount);
		//set up an array for all the action managers
		for(var i=1;i<=ActionCount;i++){
			arManagers[i] = rsActionManagers.Fields.Item('Managed_By_Id').Value;
			rsActionManagers.MoveNext();
		}
		rsActionManagers.Close();

		/* now that we have a list of the managers we
		loop through each manager, select thier records, create their actions, send the emails */
		for(var i=1;i<=ActionCount;i++){
		    //create the managers Actiond documents
			strSQL = "SELECT * FROM VIEW_REMINDER_EMAIL WHERE Managed_By_Id =" + arManagers[i];
			var rsActionDocs = createRecordset(strSQL);
		    // we need an array for all the email variables
			var arAction_Id = new Array(rsActionDocs.RecordCount);
			var arAction_Title = new Array(rsActionDocs.RecordCount);
			var arDue_Date = new Array(rsActionDocs.RecordCount);
			var Managed_By_Id = arManagers[i];
			var x = 1;

			while(!rsActionDocs.EOF){
				arAction_Title[x] = rsActionDocs.Fields.Item('Action_Title').Value;
				var due_date = new Date(rsActionDocs.Fields.Item('Scheduled_Date').Value);
				arDue_Date[x] = due_date;
				var Action_Id = rsActionDocs.Fields.Item('Action_Id').Value;
				arAction_Id[x] = Action_Id;
				rsActionDocs.MoveNext();
				x+=1;
			}//end while(!rsActionDocs.EOF)

			rsActionDocs.Close();
			strSQL = 'SELECT Emp_Name, Email_Address, Employee_Number, Site_Id FROM View_Employee_Details WHERE Employee_Number =' + Managed_By_Id;
			//Collect the name, email address, username and password for the manager
			var rsActionDocs = createRecordset(strSQL);
			if(!rsActionDocs.BOF || !rsActionDocs.EOF){
				var strEmp_Name = rsActionDocs.Fields.Item('Emp_Name').Value;
				var strEmail = rsActionDocs.Fields.Item('Email_Address').Value;
				var iSiteId = rsActionDocs.Fields.Item('Site_Id').Value;
				rsActionDocs.Close();
				sendEmail(strEmp_Name, strEmail, arAction_Id, arAction_Title, arDue_Date, 'ManagedBy', iSiteId);
			}
		}//end for(var i=1;i<=ActionCount;i++)
	}//end if(ActionCount>0)
}//end function

function SendAllocatedReminders(){
	var strSQL = "SELECT * FROM VIEW_REMINDER_EMAIL";
	var rsActionManagers = createRecordset(strSQL);
	var ActionCount = rsActionManagers.RecordCount;
	rsActionManagers.Close();
	if(ActionCount>0){ // there are reminders that need to be sent
		strSQL = "SELECT DISTINCT Allocated_To_Id FROM VIEW_REMINDER_EMAIL";
		var rsActionManagers = createRecordset(strSQL);
		ActionCount = rsActionManagers.RecordCount;
		var arManagers = new Array(ActionCount);
		//set up an array for all the action managers
		for(var i=1;i<=ActionCount;i++){
			arManagers[i] = rsActionManagers.Fields.Item('Allocated_To_Id').Value;
			rsActionManagers.MoveNext();
		}
		rsActionManagers.Close();

		/* now that we have a list of the managers we
		loop through each manager, select thier records, create their actions, send the emails */
		for(var i=1;i<=ActionCount;i++){
		    //create the managers Actiond documents
			strSQL = "SELECT * FROM VIEW_REMINDER_EMAIL WHERE Allocated_To_Id =" + arManagers[i];
			var rsActionDocs = createRecordset(strSQL);
		    // we need an array for all the email variables
			var arAction_Id = new Array(rsActionDocs.RecordCount);
			var arAction_Title = new Array(rsActionDocs.RecordCount);
			var arDue_Date = new Array(rsActionDocs.RecordCount);
			var Managed_By_Id = arManagers[i];
			var x = 1;

			while(!rsActionDocs.EOF){
				arAction_Title[x] = rsActionDocs.Fields.Item('Action_Title').Value;
				var due_date = new Date(rsActionDocs.Fields.Item('Scheduled_Date').Value);
				arDue_Date[x] = due_date;
				var Action_Id = rsActionDocs.Fields.Item('Action_Id').Value;
				arAction_Id[x] = Action_Id;
				rsActionDocs.MoveNext();
				x+=1;
			}//end while(!rsActionDocs.EOF)

			rsActionDocs.Close();
			strSQL = 'SELECT Emp_Name, Email_Address, Employee_Number, Site_Id FROM View_Employee_Details WHERE Employee_Number =' + Managed_By_Id;
			//Collect the name, email address, username and password for the manager
			var rsActionDocs = createRecordset(strSQL);
			if(!rsActionDocs.BOF || !rsActionDocs.EOF){
				var strEmp_Name = rsActionDocs.Fields.Item('Emp_Name').Value;
				var strEmail = rsActionDocs.Fields.Item('Email_Address').Value;
				var iSiteId = rsActionDocs.Fields.Item('Site_Id').Value;
				rsActionDocs.Close();
				sendEmail(strEmp_Name, strEmail, arAction_Id, arAction_Title, arDue_Date, 'AllocatedTo', iSiteId);
		 	}
		}//end for(var i=1;i<=ActionCount;i++)
	}//end if(ActionCount>0)
}//end function

//***** Send the Reminders ******
if(MM_EmailStatus != 0){ // if MM_EmailStatus = 0 this means emailing is turned off
	SendReminders();
	SendAllocatedReminders();
}
