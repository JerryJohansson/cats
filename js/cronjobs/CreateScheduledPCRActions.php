<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>CreateScheduledPCRActions Cronjob</title>
</head>
<body>
<h1> CreateScheduledPCRActions Cronjob Started</h1>

<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

switch($host_name) {
	case 'catsdev' :
		echo "<h2> Running Script over DEV </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catsdev31' :
		echo "<h2> Running Script over DEV31 </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev31/');
		break;
	case 'catstst' :
		echo "<h2> Running Script over TST </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	case 'catsprd' :
		echo "<h2> Running Script over PRD </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/prd/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;
}

define('CATS_SERVER',$host_name);
define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php
define('CATS_REMOTE_PATH',CATS_INCLUDE_PATH.'remote/');

// load db stuff
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/cats_admin.ini");
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}

// Include the DB Functions
require_once(CATS_FUNCTIONS_PATH . 'db_functions.php');

// Include the Min functions
require_once(CATS_FUNCTIONS_PATH . 'main_functions.php');

// Include the Mail Class
require_once(CATS_CLASSES_PATH . 'mail.class.php');

// Set Debugging to True
$db->debug = true;

// connect to the database
db_connect();

// Load the Email Config
EmailConfig();

if(CATS_MAIL_STATUS >= 1) {
	// Process the actions
	CheckPCRDetails();
	CheckPCRActions();
	CheckPCRActionsComplete();
}

// Close the database
$db = db_close();


/*
+---------------------------------------------------------------------------
|	Load the Email Configuration
+---------------------------------------------------------------------------
*/
function EmailConfig() {
	$sql = "SELECT * FROM tblemailoptions";
	$rs = db_query($sql);
	// build the array of fields
	$arr=array();
	$row = $rs->FetchRow();
	$fcnt=$rs->FieldCount();
	for($i=0;$i<$fcnt;$i++){
		// get field object so we know what we are dealing with
		$fld=$rs->FetchField($i);
		// get the field type
		$type = $rs->MetaType($fld->type);
		switch($type){
			case 'D':case 'T': // format the date value
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'N':case 'I': // don't really need this one as this is the same as the default case
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'C':case 'X': // format text value
				$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
				break;
			default: // default value is simply the value of the field
				$arr[$fld->name] = $row[$fld->name];
				break;
		}
	}
	define('CATS_MAIL_STATUS',$arr['EMAIL_STATUS']);
	define('CATS_MAIL_HOST',$arr['EMAIL_SERVER']);
	define('CATS_MAIL_TEST_ADDRESS',$arr['EMAIL_TESTADDRESS']);
	define('CATS_MAIL_TEST_NAME',$arr['EMAIL_TESTNAME']);
	define('CATS_MAIL_FROM_ADDRESS',$arr['EMAIL_FROMADDRESS']);
	define('CATS_MAIL_FROM_NAME',$arr['EMAIL_FROMNAME']);
	//define('CATS_MAIL_TRANSPORT','smtp');
}

/*
+---------------------------------------------------------------------------
|	Check PCR Details ??
+---------------------------------------------------------------------------
*/
function CheckPCRDetails(){
	// get originator email messages
	$sql_arr=array(
		"select 'Details' as Screen_Name,'Originator' as Email_To, d.*, t.originator_notified from VIEW_PCR_DETAILS d, TBLPCR_DETAILS t where t.PCR_ID=d.PCR_ID and ((d.authoritystatus_1 is not null and d.authoritystatus_2 is not null) or (d.site_id=3)) and t.originator_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Details' as Screen_Name,'Evaluator' as Email_To, d.*, t.evaluator_notified from VIEW_PCR_DETAILS d, TBLPCR_DETAILS t where d.PCR_ID=t.PCR_ID and t.evaluator_notified is null and lower(d.overall_status) not like 'close%' and d.evaluation_id is not null and t.modified_date is not null ",
		"select 'Initial Reviews' as Screen_Name,'PCRT Team' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_InitialReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.pcrt_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
		"select 'Evaluation' as Screen_Name,'Evaluator' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_Evaluation t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.manager_notified is null and lower(d.overall_status) not like 'close%' and t.evaluation_name_id is not null and t.modified_date is not null ",
//		"select 'Area Review' as Screen_Name,'Area Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_AreaReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null ",
//		"select 'PCRT Review' as Screen_Name,'PCRT Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_PCRTReview t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null and t.type_of_review <> 'PCRT Meeting'",
		"select 'Extension' as Screen_Name,'Exention Reviewer' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_Extension t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and t.reviewer_notified is null and lower(d.overall_status) not like 'close%' and t.modified_date is not null and t.type_of_review <> 'PCRT'",
		"select 'Sign-Off' as Screen_Name,'Originator' as Email_To, t.*, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_SignOff t, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and  t.originator_notified is null and lower(d.overall_status) not like 'close%' and t.sign_off is not null and t.modified_date is not null ",
		"select 'Sign-Off' as Screen_Name,'Post Auditor' as Email_To, t.PCR_ID, t.AUDIT_COMMENTS, t.AUDIT_NAME_ID, TO_CHAR(t.AUDIT_DATE,'YYYY-MM-DD') as AUDIT_DATE, t.AUDIT_COMPLETED_BY, TO_CHAR(t.POSTAUDITOR_NOTIFIED,'YYYY-MM-DD') as POSTAUDITOR_NOTIFIED, t.AUDIT_COMPLETED_BY_ID, TO_CHAR(t.MODIFIED_DATE,'YYYY-MM-DD') as MODIFIED_DATE, d.PCR_DisplayId, d.PCR_Type, d.ImpactLevel, d.Title, d.Overall_Status, d.Site_Id from tblPCR_PostAudit t, tblPCR_SignOff s, VIEW_PCR_DETAILS d where t.PCR_ID=d.PCR_ID and d.pcr_id=s.pcr_id and s.sign_off='Approve' and t.AUDIT_COMPLETED_BY_ID is not null and t.AUDIT_DATE is not null and t.postauditor_notified is null and lower(d.overall_status) not like 'close%' "
	);
	SendPCREmails($sql_arr);
}

/*
+---------------------------------------------------------------------------
|	Check PCR Actions ??
+---------------------------------------------------------------------------
*/
function CheckPCRActions(){
	$Manager_Arr = array();

	$sql = "SELECT DISTINCT managed_by_id FROM tblpcr_actions WHERE (status IS NULL OR status LIKE 'pend%') AND action_register_id IS NULL";
	$rs = db_query($sql);
	$rc = $rs->RowCount();

	// Build the array of managers
	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		// Add the Manager ID to the Array
		if ($arr['MANAGED_BY_ID'] != '')
			$Manager_Arr[] = $arr['MANAGED_BY_ID'];
	}

	print_r($Manager_Arr);

	$last_allocated = "";

	foreach($Manager_Arr as $manager) {
		$fields = "PCR_ID,DETAIL_ID,SITE_ID,STAGE_ID,ACTION_ID,ACTION_DESCRIPTION,MANAGED_BY_ID,ALLOCATED_TO_ID,STATUS,TO_CHAR(DUE_DATE,'YYYY-MM-DD') as DUE_DATE,DAYS_AFTER_PREV_STAGE,ACTION_CONFIRMED,RAISE_NOW,SUPERVISOR_ID,GO_TO_ACTIONS,ACTION_REGISTER_ID,TO_CHAR(MANAGER_NOTIFIED,'YYYY-MM-DD') as MANAGER_NOTIFIED,TO_CHAR(SUPER_ENG_NOTIFIED,'YYYY-MM-DD') as SUPER_ENG_NOTIFIED,TO_CHAR(STAGE1_NOTIFIED,'YYYY-MM-DD') as STAGE1_NOTIFIED,TO_CHAR(STAGE2_NOTIFIED,'YYYY-MM-DD') as STAGE2_NOTIFIED,TO_CHAR(STAGE3_NOTIFIED,'YYYY-MM-DD') as STAGE3_NOTIFIED,TO_CHAR(DATE_CREATED,'YYYY-MM-DD') as DATE_CREATED";
		$sql = "SELECT ".$fields." FROM tblpcr_actions WHERE (status IS NULL OR status LIKE 'pend%') AND action_register_id IS NULL AND managed_by_id=".$manager." ORDER BY allocated_to_id, pcr_id, stage_id";
		$rs = db_query($sql);
		$rc = $rs->RowCount();

		// For Each Reminder Email
		$PCRAction_Arr=array();

		while($row = $rs->FetchRow()) {
			$arr=array();
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}

			// Print Out the Selected Record
			print_r($arr);

			$b_skip=false;

			switch($arr['STAGE_ID']) {
				case 1:
					$scheduled_date = date('Y-m-d',strtotime($arr['DUE_DATE']));
					break;
				case 2:
					if ($arr['RAISE_NOW'] != 'Y')
						$b_skip = !is_stage_complete($arr['PCR_ID'], 1);
					$scheduled_date = date('Y-m-d',strtotime("+".$arr['DAYS_AFTER_PREV_STAGE']." days"));
					break;
				case 3:
					if ($arr['RAISE_NOW'] != 'Y')
						$b_skip = !is_stage_complete($arr['PCR_ID'], 2);
					$scheduled_date = date('Y-m-d',strtotime("+".$arr['DAYS_AFTER_PREV_STAGE']." days"));
					break;
				default:
					$b_skip=true;
					break;
			}

			if ($b_skip) {
				// Skip it
			} else {
				// KLL 28/03/2007 Change from 7days to 14 days
				// Set the Reminder Date
				if (time() > strtotime("-14 days",strtotime($scheduled_date))) {

					// less than 14 days ago, so set to due date
					$reminder_date = date('Y-m-d',strtotime($scheduled_date));
				} else {
					// set to 14 days before due date
					$reminder_date = date('Y-m-d',strtotime("-14 days",strtotime($scheduled_date)));
				}

				$Action = array(
					'ACTION_ID' => 0,
					'REGISTER_ORIGIN' => 'PCR Action',
					'ACTION_TYPE' => 'Planned',
					'ACTION_DESCRIPTION' => $arr['ACTION_DESCRIPTION'],
					'SCHEDULED_DATE' => $scheduled_date,
					'REMINDER_DATE' => $reminder_date,
					'CHANGE_COMMENTS' => '',
					'MANAGED_BY_ID' => $arr['MANAGED_BY_ID'],
					'ALLOCATED_TO_ID' => $arr['ALLOCATED_TO_ID'],
					'NOTIFY_ALLOCATED_EMAIL' => 0,
					'NOTIFY_ALLOCATED_PRINT' => 0,
					'STATUS' => 'Open',
					'CLOSING_DATE' => '',
					'CLOSED_BY' => 0,
					'DATE_DUE_CHANGED_HISTORY' => '',
					'ORIGIN_TABLE' => 'ORIGINATING_PCR_ACTION',
					'REPORT_ID' => $arr['PCR_ID'],
					'RECORD_COMMENTS' => '',
					//'SITE_ID' => $arr['SITE_ID'],
					'SITE_ID' => db_get_one("SELECT site_id FROM tblemployee_details WHERE employee_number=".$arr['ALLOCATED_TO_ID']),
					//'DEPARTMENT_ID' => db_get_one("SELECT department_id FROM tblemployee_details WHERE employee_number=".$arr['MANAGED_BY_ID']),
					'DEPARTMENT_ID' => db_get_one("SELECT department_id FROM tblemployee_details WHERE employee_number=".$arr['ALLOCATED_TO_ID']),
					'AT' => '',
					'RL' => '',
					'ACTION_TITLE' => $arr['ACTION_DESCRIPTION'],
					'RECORD_LOCATION' => '',
					//'SECTION_ID' => db_get_one("SELECT section_id FROM tblemployee_details WHERE employee_number=".$arr['MANAGED_BY_ID']),
					'SECTION_ID' => db_get_one("SELECT section_id FROM tblemployee_details WHERE employee_number=".$arr['ALLOCATED_TO_ID']),
					'DATE_CREATED' => 'SYSDATE',
					'CRITICAL' => ''
					);

				$Action['ACTION_ID'] = createAction($Action);
				if ($Action['ACTION_ID'] > 0) {
					$arr['ACTION_REGISTER_ID'] = $Action['ACTION_ID'];
					//$arr['STATUS'] = "Processed";
					$arr['STATUS'] = "Open";
					$arr['DUE_DATE'] = $scheduled_date;
					updatePCRAction($arr);
				}

				$PCRAction_Arr[] = array(
					'PCR_DISPLAYID' => db_get_one("SELECT pcr_displayid FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID']),
					'ACTION_ID' => $Action['ACTION_ID'],
					'ACTION_TITLE' => db_get_one("SELECT title FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID']),
					'ACTION_DESCRIPTION' => $Action['ACTION_DESCRIPTION'],
					'DUE_DATE' => $scheduled_date
					);

			} // End of if ($b_skip)

			// If the allocated to person has changed
			//if ($last_allocated != $arr['ALLOCATED_TO_ID']) {
				//if ($last_allocated != "") {
					print_r($PCRAction_Arr);

					if (sizeof($PCRAction_Arr) > 0) {
						$sql = "SELECT last_name||', '||first_name as name, email_address, site_id FROM tblEmployee_Details WHERE Employee_Number=".$arr['MANAGED_BY_ID'];
						$managed_recipient = array();
						if ($arr['MANAGED_BY_ID'] != "")
							$managed_recipient = get_email_recipient($sql);

						//$sql = "SELECT last_name||', '||first_name as name, email_address, site_id FROM tblEmployee_Details WHERE Employee_Number=".$last_allocated;
						$sql = "SELECT last_name||', '||first_name as name, email_address, site_id FROM tblEmployee_Details WHERE Employee_Number=".$arr['ALLOCATED_TO_ID'];
						$allocated_recipient = array();
						$allocated_recipient = get_email_recipient($sql);

						// Send the Emails for this manager and allocated to
						sendPCRActionEmail($managed_recipient, $allocated_recipient, $PCRAction_Arr);

					} // End of if (sizeof($PCRAction_Arr) > 0)

					// Destroy the Array for this manager and allocated to
					$PCRAction_Arr=array();

				//} // End of if ($last_allocated != "")

				// Set the new last allocated to
				//$last_allocated = $arr['ALLOCATED_TO_ID'];

			//} // End of if ($last_allocated != $arr['ALLOCATED_TO_ID'])
		} // End of while($row = $rs->FetchRow())

		print_r($PCRAction_Arr);

		if (sizeof($PCRAction_Arr) > 0) {
			$sql = "SELECT last_name||', '||first_name as name, email_address, site_id FROM tblEmployee_Details WHERE Employee_Number=".$arr['MANAGED_BY_ID'];
			$managed_recipient = array();
			if ($arr['MANAGED_BY_ID'] != "")
				$managed_recipient = get_email_recipient($sql);

			$sql = "SELECT last_name||', '||first_name as name, email_address, site_id FROM tblEmployee_Details WHERE Employee_Number=".$arr['ALLOCATED_TO_ID'];
			$allocated_recipient = array();
			if ($arr['ALLOCATED_TO_ID'] != "")
				$allocated_recipient = get_email_recipient($sql);

			sendPCRActionEmail($managed_recipient, $allocated_recipient, $PCRAction_Arr);

		} // End of if (sizeof($PCRAction_Arr) > 0)
	} // End of foreach($manager_arr as $manager)
}

/*
+---------------------------------------------------------------------------
|	Check PCR Actions ??
+---------------------------------------------------------------------------
*/
function CheckPCRActionsComplete(){
	$sql_arr = array();

	echo "CheckPCRActionsComplete - Collecting Completed Stages";

	$complete_arr = get_stages_complete();

	// For Each Complete PCR
	foreach($complete_arr as $pcr) {
		// For Each Stage
		for($stage=1; $stage<=3; $stage++) {
			// Are there any stages that haven't been notified ?
			if (db_get_one("SELECT COUNT(pcr_id) FROM tblpcr_actions WHERE pcr_id=".$pcr." AND stage_id=".$stage." AND stage".$stage."_notified IS NULL") > 0) {
				// Is the Stage Complete ?
				if (is_stage_complete($pcr, $stage) > 0) {
					// Fetch the Site
					$site = db_get_one("SELECT site_id FROM tblpcr_details WHERE pcr_id=".$pcr);
					// Build the Array
					$sql_arr[] = array(
						'SCREEN_NAME'=>'Actions',
						'EMAIL_TO'=>'Engineer, Superintendent, Originator',
						'PCR_ID'=>$pcr,
						'STAGE_ID'=>$stage,
						'SITE_ID'=>$site,
						'DETAIL_SQL'=>"SELECT * FROM view_pcr_details WHERE pcr_id=".$pcr,
						'ACTION_SQL'=>"SELECT * FROM tblpcr_actions WHERE pcr_id=".$pcr." AND stage_id=".$stage." AND stage".$stage."_notified IS NULL"
						);
				} // End of if (is_stage_complete($pcr, $stage) > 0)
			} // End of if (db_get_one("SELECT COUNT(action_id) FROM tblpcr_actions WHERE pcr_id=".$pcr." AND stage_id=".$stage." AND stage".$stage."_notified IS NULL") > 0)
		} // End of for($stage=1; $stage<=3; $stage++)
	} // End of foreach($complete_arr as $pcr)

	if (sizeof($sql_arr) > 0) {
		echo "CheckPCRActionsComplete - Processing Emails";
		SendPCREmails_Actions($sql_arr);
	} else
		echo "No complete stages to report";
}

/*
+---------------------------------------------------------------------------
|	Stage InComplete SQL ??
+---------------------------------------------------------------------------
*/
function stage_not_complete_sql($fields){
	return "SELECT ".$fields." FROM tblpcr_actions a1 WHERE (SELECT COUNT(a3.detail_id) FROM tblpcr_actions a3 WHERE a3.pcr_id=a1.pcr_id AND a3.stage_id=a1.stage_id) = (SELECT COUNT(a2.detail_id) FROM tblpcr_actions a2 WHERE a2.pcr_id=a1.pcr_id AND a2.stage_id=a1.stage_id AND a2.action_confirmed!='Y' AND a2.action_register_id > 0) ";
}

/*
+---------------------------------------------------------------------------
|	Stage Complete SQL ??
+---------------------------------------------------------------------------
*/
function stage_complete_sql($fields){
	return "SELECT ".$fields." FROM tblpcr_actions a1 WHERE (SELECT COUNT(a3.detail_id) FROM tblpcr_actions a3 WHERE a3.pcr_id=a1.pcr_id AND a3.stage_id=a1.stage_id) = (SELECT COUNT(a2.detail_id) FROM tblpcr_actions a2 WHERE a2.pcr_id=a1.pcr_id AND a2.stage_id=a1.stage_id AND a2.action_confirmed='Y' AND a2.action_register_id > 0) ";
}

/*
+---------------------------------------------------------------------------
|	Stage Complete SQL ??
+---------------------------------------------------------------------------
*/
function get_stages_complete(){
	$complete_arr = array();
	$sql=stage_complete_sql("distinct a1.pcr_id");

	$rs = db_query($sql);
	$rc = $rs->RowCount();

	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		$complete_arr[] = $arr['PCR_ID'];
	}

	return $complete_arr;
}

/*
+---------------------------------------------------------------------------
|	Stage Complete SQL ??
+---------------------------------------------------------------------------
*/
function is_stage_complete($pcr, $stage){
	$sql=stage_complete_sql("COUNT(*) AS c");
	$sql .= " AND a1.pcr_id=".$pcr;
	$sql .= " AND a1.stage_id=".$stage;
	return db_get_one($sql);
}

/*
+---------------------------------------------------------------------------
|	Send PCR Emails
+---------------------------------------------------------------------------
*/
function SendPCREmails($sql_arr) {
	foreach($sql_arr as $sql) {
		$rs = db_query($sql);
		$rc = $rs->RowCount();

		// Boolean to process actions differently

		echo "<p>".$rc." record(s) found</p>"; //PPD Debug

		while($row = $rs->FetchRow()){
			$arr=array();
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}

			$email_arr = get_email_messages($arr['SCREEN_NAME'], $arr['EMAIL_TO'], $arr['SITE_ID']);
			$update_arr = array();

			// Build the email message
			foreach($email_arr as $key => $email) {
				switch($arr['SCREEN_NAME']) {
					case 'Details':
						$update_arr['TABLE'] = "tblpcr_details";
						$update_arr['FIELD'] = "originator_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID']." ";

						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					case 'Initial Reviews':
						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					case 'Evaluation':
						$update_arr['TABLE'] = "tblpcr_evaluation";
						$update_arr['FIELD'] = "manager_notified";

						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					case 'Area Review':
						$update_arr['TABLE'] = "tblpcr_areareview_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID']." ";
						$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']."AND detail_id=";
						$update_arr['DETAIL_ID'] = "detail_id";

						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."
	";

						break;

					case 'PCRT Review':
						$update_arr['TABLE'] = "tblpcr_pcrtreview_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID']." ";
						$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']."AND detail_id=";
						$update_arr['DETAIL_ID'] = "detail_id";

						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					case 'Extension':
						$update_arr['TABLE'] = "tblpcr_extension_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID']." ";
						$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']."AND detail_id=";
						$update_arr['DETAIL_ID'] = "detail_id";

						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					case 'Actions':
						// Should Never Happen !!
						break;

					case 'Sign-Off':
						switch($arr['EMAIL_TO']) {
							case 'Originator':
							case 'Post Auditor':
								$update_arr['TABLE'] = "tblpcr_postaudit";
								$update_arr['FIELD'] = "postauditor_notified";
								break;
							default:
								break;
						}
						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					case 'Post Audit':
						$update_arr['TABLE'] = "tblpcr_postaudit";
						$update_arr['FIELD'] = "postauditor_notified";

						$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
						$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
						$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
						$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
						$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
						$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";

						break;

					default:
						$email['MESSAGE'] .= 'Error occured while compiling email headers:\nNo Screen selected or screen name does not exist!';
						break;

				} // End of switch($arr['SCREEN_NAME'])

				// get email recipient adresses
				$email['RECIPIENT'] = array();
				switch($arr['EMAIL_TO']) {
					case 'Originator':
						if ($arr['SCREEN_NAME'] == 'Sign-Off') {
							$update_arr['TABLE'] = "tblpcr_signoff";
							$update_arr['FIELD'] = "postauditor_notified";
						} else {
							$update_arr['TABLE'] = "tblpcr_details";
							$update_arr['FIELD'] = "originator_notified";
						}
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];

						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status FROM tblEmployee_Details WHERE employee_number IN (SELECT originator_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'PCRT Team':
						$update_arr['TABLE'] = "tblpcr_initialreview_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];

						updateNotified("tblpcr_initialreview", "pcrt_notified", $update_arr['WHERE'], "", "");

						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status FROM tblemployee_Details WHERE (engineer = 'Yes') OR (superintendent = 'Yes') OR employee_number IN (SELECT originator_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'Evaluator':
						if ($arr['SCREEN_NAME'] == 'Evaluation') {
							$update_arr['TABLE'] = "tblpcr_evaluation";
							$update_arr['FIELD'] = "manager_notified";
							$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];

							$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE (engineer = 'Yes') OR (superintendent = 'Yes') OR employee_number IN (SELECT originator_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") ";
						} else {
							$update_arr['TABLE'] = "tblpcr_details";
							$update_arr['FIELD'] = "evaluator_notified";
							$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];

							$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE employee_number IN (SELECT evaluation_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") ";
						}

						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'Area Reviewer':
						$update_arr['TABLE'] = "tblpcr_areareview_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];
						$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']." and detail_id=";
						$update_arr['DETAIL_ID'] = "detail_id";

						updateNotified("tblpcr_areareview", $update_arr['FIELD'], $update_arr['WHERE'], "", "");

						$update_arr['SQL'] = "UPDATE ".$update_arr['TABLE']." SET ".$update_arr['FIELD']."=SYSDATE WHERE pcr_id=".$arr['PCR_ID']." ";

						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE employee_number IN (SELECT reviewer_id FROM tblpcr_areareview_details WHERE required=1 AND pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'PCRT Reviewer':
						$update_arr['TABLE'] = "tblpcr_pcrtreview_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];
						$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']." and detail_id=";
						$update_arr['DETAIL_ID'] = "detail_id";

						updateNotified("tblpcr_pcrtreview", $update_arr['FIELD'], $update_arr['WHERE'], "", "");

						$update_arr['SQL'] = "UPDATE ".$update_arr['TABLE']." SET ".$update_arr['FIELD']."=SYSDATE WHERE pcr_id=".$arr['PCR_ID']." ";

						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE employee_number IN (SELECT reviewer_id FROM tblpcr_pcrtreview_details WHERE required=1 AND pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'Exention Reviewer':
						$update_arr['TABLE'] = "tblpcr_extension_details";
						$update_arr['FIELD'] = "reviewer_notified";
						$update_arr['WHERE'] = " where pcr_id = ".$arr['PCR_ID'];
						$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']." and detail_id=";
						$update_arr['DETAIL_ID'] = "detail_id";

						updateNotified("tblpcr_extension", $update_arr['FIELD'], $update_arr['WHERE'], "", "");

						$update_arr['SQL'] = "UPDATE ".$update_arr['TABLE']." SET ".$update_arr['FIELD']."=SYSDATE WHERE pcr_id=".$arr['PCR_ID']." ";

						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE employee_number IN (SELECT reviewer_id FROM tblpcr_extension_details WHERE required=1 AND pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'Managed By':
						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE pcrt_team=1";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						break;

					case 'Engineer, Superintendent, Originator':
						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE ((engineer='Yes') OR (superintendent='Yes')) AND site_id=(SELECT site_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") OR employee_number IN (SELECT originator_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						// Send extra emails to admin assistant to notify them of actions they need to confirm
// Changes for Work Item #12887
						//if ($arr['SITE_ID'] == 3) {
							$email['RECIPIENT'][] = array(
								'NAME' => "Admin Assistant",
								//'EMAIL' => "BIAA@tronox.com"
								'EMAIL' => db_get_one("SELECT pcr_admin_email FROM tblsite WHERE site_id=".$arr['SITE_ID'])
								);
						//}

						break;

					case 'Engineer, Superintendent':
						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE ((engineer='Yes') OR (superintendent='Yes')) AND site_id=(SELECT site_id FROM tblpcr_details WHERE pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);

						// Send extra emails to admin assistant to notify them of actions they need to confirm
// Changes for Work Item #12887
						//if ($arr['SITE_ID'] == 3) {
							$email['RECIPIENT'][] = array(
								'NAME' => "Admin Assistant",
								//'EMAIL' => "BIAA@tronox.com"
								'EMAIL' => db_get_one("SELECT pcr_admin_email FROM tblsite WHERE site_id=".$arr['SITE_ID'])
								);
						//}

						break;

					case 'Post Auditor': // This should happen only once...Create scheduled actions for the post audit
					    $email['RECIPIENT'] = array();
						// Commented out as per Altiris Helpdesk Incident 12332.
/*						$schedule_arr = array(
							'REGISTER_ORIGIN' => "PCR Action",
							'SCHEDULER_DESCRIPTION' => "A PCR has been completed and signed off and a Post Audit is required",
							'COMPLETION_DATE' => date('Y-m-d',strtotime($arr['AUDIT_DATE'])),
							'MANAGED_BY_ID' => $arr['AUDIT_COMPLETED_BY_ID'],
							'SITE_ID' => $arr['SITE_ID'],
							'DEPARTMENT_ID' => db_get_one("SELECT department_id FROM tblemployee_details WHERE employee_number=".$arr['AUDIT_COMPLETED_BY_ID']),
							'ACTION_RAISED_DATE' => date('Y-m-d',strtotime("-10 days",strtotime($completion_date))),
							'FREQUENCY' => 1,
							'NO_OF_DAYS_NOTICE' => 10,
							'REPORT_ID' => 0,
							'ACTION_TITLE' => "A PCR has been completed and signed off and a Post Audit is required",
							'SECTION_ID' => db_get_one("SELECT section_id FROM tblemployee_details WHERE employee_number=".$arr['AUDIT_COMPLETED_BY_ID']),
							'STATUS' => 'On'
							);
						$schedule_arr['SCHEDULER_ID'] = createSchedule($schedule_arr);

						$update_arr['SQL'] = "UPDATE ".$update_arr['TABLE']." SET ".$arr['FIELD']."=SYSDATE WHERE pcr_id=".$arr['PCR_ID']." ";

						$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE employee_number IN (SELECT audit_completed_by_id FROM tblpcr_postaudit WHERE audit_completed_by_id IS NOT NULL AND postauditor_notified IS NOT NULL AND pcr_id=".$arr['PCR_ID'].") ";
						$email['RECIPIENT'] = get_email_recipients($recipient_sql);
*/
						break;

					default:
						break;
				} // End of switch($arr['EMAIL_TO'])

				// Debug info: so we know exactly which case this comes from
				$email['MESSAGE'] .= "[Message Info]
	Screen:".$arr['SCREEN_NAME']."
	Notify:".$arr['EMAIL_TO']."

	";

				if (sizeof($email['RECIPIENT']) > 0) {
					if($update_arr['TABLE']!="" && $update_arr['FIELD']!="" && $update_arr['WHERE']!="")
						updateNotified($update_arr['TABLE'], $update_arr['FIELD'], $update_arr['WHERE'], $update_arr['DETAIL_ID'], $update_arr['DETAIL_WHERE']);

					// get the email from address for this persons site
					$email['FROM'] = db_get_one("SELECT email_from_address FROM tblsite WHERE site_id=".$arr['SITE_ID']);

					SendEmail($email);

					echo "<h3>email:</h3>"; //PPD Debug
					print_r($email); //PPD Debug
					echo "<h3>update_arr:</h3>"; //PPD Debug
					print_r($update_arr); //PPD Debug

				} // End of if(sizeof($email['RECIPIENT']) > 0)
			} // End of foreach($email_arr as $key => $email)
		} // End of while($row = $rs->FetchRow())
	} // End of foreach($sql_arr as $sql)
}

/*
+---------------------------------------------------------------------------
|	Send PCR Emails
+---------------------------------------------------------------------------
*/
function SendPCREmails_Actions($sql_arr) {
	// For Each Completed Stage
	foreach($sql_arr as $sql_obj) {

		//print_r($sql_obj);

		// Get the Email Messages
		$email_arr = get_email_messages($sql_obj['SCREEN_NAME'], $sql_obj['EMAIL_TO'], $sql_obj['SITE_ID']);

		// For each Email Message
		foreach($email_arr as $key => $email) {

			// Set the Update Array
			$update_arr = array();
			$update_arr['TABLE'] = "tblpcr_actions";
			$update_arr['FIELD'] = "stage".$sql_obj['STAGE_ID']."_notified";
			$update_arr['WHERE'] = " WHERE pcr_id=".$sql_obj['PCR_ID']." AND stage_id=".$sql_obj['STAGE_ID'];
			$update_arr['DETAIL_WHERE'] = $update_arr['WHERE']." AND detail_id=";
			$update_arr['DETAIL'] = "detail_id";

			// Get the PCR Details
			$rs = db_query($sql_obj['DETAIL_SQL']);
			$arr=array();
			while($row = $rs->FetchRow()){
				$fcnt=$rs->FieldCount();
				for($i=0;$i<$fcnt;$i++){
					// get field object so we know what we are dealing with
					$fld=$rs->FetchField($i);
					// get the field type
					$type = $rs->MetaType($fld->type);
					switch($type){
						case 'D':case 'T': // format the date value
							$arr[$fld->name] = $row[$fld->name];
							break;
						case 'N':case 'I': // don't really need this one as this is the same as the default case
							$arr[$fld->name] = $row[$fld->name];
							break;
						case 'C':case 'X': // format text value
							$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
							break;
						default: // default value is simply the value of the field
							$arr[$fld->name] = $row[$fld->name];
							break;
					}
				}
			}

			// Add Extra Details to the Email Message
			$email['MESSAGE'] .= 'PCR No.:
	'.$arr['PCR_DISPLAYID'].'
	';
			$email['MESSAGE'] .= 'Type of PCR:
	'.$arr['PCR_TYPE'].'
	';
			$email['MESSAGE'] .= 'Criticality:
	'.$arr['IMPACTLEVEL'].'
	';
			$email['MESSAGE'] .= 'Title:
	'.$arr['TITLE'].'
	';
			$email['MESSAGE'] .= 'Status:
	'.$arr['OVERALL_STATUS'].'
	';
			$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=pcr_search&p=view&id=".$arr['PCR_ID']."

	";
			$email['MESSAGE'] .= "---------------------------------------------
	";
			$email['MESSAGE'] .= "- Below is a list of Actions completed for this stage -

	";

			// get email recipient adresses
			$email['RECIPIENT'] = array();
			$recipient_sql = "SELECT DISTINCT employee_number, first_name||' '||last_name as name, email_address as email, user_status  FROM tblemployee_Details WHERE ((engineer='Yes') OR (superintendent='Yes')) AND site_id=(SELECT site_id FROM tblpcr_details WHERE pcr_id=".$sql_obj['PCR_ID'].") OR employee_number IN (SELECT originator_id FROM tblpcr_details WHERE pcr_id=".$sql_obj['PCR_ID'].") ";
			$email['RECIPIENT'] = get_email_recipients($recipient_sql);

			// get the email from address for this persons site
			$email['FROM'] = db_get_one("SELECT email_from_address FROM tblsite WHERE site_id=".$sql_obj['SITE_ID']);

			// Send extra emails to admin assistant to notify them of actions they need to confirm
// Changes for Work Item #12887
			//if ($sql_obj['SITE_ID'] == 3) {
				$email['RECIPIENT'][] = array(
					'NAME' => "Admin Assistant",
					//'EMAIL' => "BIAA@tronox.com"
					'EMAIL' => db_get_one("SELECT pcr_admin_email FROM tblsite WHERE site_id=".$sql_obj['SITE_ID'])
					);
			//}

			// Now Process the Completed Actions
			$rs = db_query($sql_obj['ACTION_SQL']);
			$rc = $rs->RowCount();

			echo "<p>".$rc." record(s) found</p>"; //PPD Debug

			// For Each Action in the Stage
			while($row = $rs->FetchRow()){
				$arr=array();
				$fcnt=$rs->FieldCount();
				for($i=0;$i<$fcnt;$i++){
					// get field object so we know what we are dealing with
					$fld=$rs->FetchField($i);
					// get the field type
					$type = $rs->MetaType($fld->type);
					switch($type){
						case 'D':case 'T': // format the date value
							$arr[$fld->name] = $row[$fld->name];
							break;
						case 'N':case 'I': // don't really need this one as this is the same as the default case
							$arr[$fld->name] = $row[$fld->name];
							break;
						case 'C':case 'X': // format text value
							$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
							break;
						default: // default value is simply the value of the field
							$arr[$fld->name] = $row[$fld->name];
							break;
					}
				}

				// Append the Actions on to the end of the Email Message
				$email['MESSAGE'] .= "---------------------------------------------
	";
				$email['MESSAGE'] .= "- ACTION COMPLETE -
	";
				$email['MESSAGE'] .= "---------------------------------------------
	";
				$email['MESSAGE'] .= "Action Title:
	".$arr['ACTION_DESCRIPTION']."
	";
				$email['MESSAGE'] .= "Action Number:
	".$arr['DETAIL_ID']."
	";
				$email['MESSAGE'] .= "Action Date Due:
	".$arr['DUE_DATE']."
	";
				$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=actions&p=edit&id=".$arr['ACTION_REGISTER_ID']."

	";

			} // End of while($row = $rs->FetchRow())

			// Debug info: so we know exactly which case this comes from
			$email['MESSAGE'] .= "[Message Info]
	Screen:".$sql_obj['SCREEN_NAME']."
	Notify:".$sql_obj['EMAIL_TO']."

	";

			if (sizeof($email['RECIPIENT']) > 0) {
				if($update_arr['TABLE']!="" && $update_arr['FIELD']!="" && $update_arr['WHERE']!="")
					updateNotified($update_arr['TABLE'], $update_arr['FIELD'], $update_arr['WHERE'], $update_arr['DETAIL_ID'], $update_arr['DETAIL_WHERE']);

				SendEmail($email);

				echo "<h3>email:</h3>"; //PPD Debug
				print_r($email); //PPD Debug
				echo "<h3>update_arr:</h3>"; //PPD Debug
				print_r($update_arr); //PPD Debug

			} // End of if(sizeof($email['RECIPIENT']) > 0)
		} // End of foreach($email_arr as $key => $email)
	} // End of foreach($sql_arr as $sql_obj)
}



/*
+---------------------------------------------------------------------------
|	Get PCR Email Messages (returns array)
+---------------------------------------------------------------------------
*/
function get_email_messages($screen_name,$email_group,$site_id){
	$email_arr = array();

	if($site_id <= 0)
		return $email_arr;

	$sql = "SELECT * FROM tblpcr_email WHERE screen_id = (SELECT screen_id FROM tblpcr_tabs WHERE screen_name='".$screen_name."') and person_description = '".$email_group."' and Site_id=".$site_id;
	$rs = db_query($sql);
	$rc = $rs->RowCount();

	echo "<p>".$rc." record(s) found</p>"; //PPD Debug

	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		$email_arr[] = array(
			'EMAIL_ID' => $arr['EMAIL_ID'],
			'SITE_ID' => $arr['SITE_ID'],
			'SCREEN_ID' => $arr['SCREEN_ID'],
			'PERSON_DESCRIPTION' => $arr['PERSON_DESCRIPTION'],
			'SUBJECT' => $arr['SUBJECT'],
			'MESSAGE' => $arr['MESSAGE'].'
	'
			);
	}

	return $email_arr;
}

/*
+---------------------------------------------------------------------------
|	Get PCR Email receipients (returns array)
+---------------------------------------------------------------------------
*/
function get_email_recipients($sql) {
	$recipient_arr = array();

	$rs = db_query($sql);
	$rc = $rs->RowCount();

	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		if ($arr['USER_STATUS'] != 'Active') {
			$recipient_arr[] = array(
				'NAME' => $arr['NAME'],
				'EMAIL' => $email['FROM']
				);
		} else {
			$recipient_arr[] = array(
				'NAME' => $arr['NAME'],
				'EMAIL' => $arr['EMAIL']
				);
		}
	}

	return $recipient_arr;
}

/*
+---------------------------------------------------------------------------
|	Get PCR Email recipients (returns array)
+---------------------------------------------------------------------------
*/
function get_email_recipient($sql) {
	$rs = db_query($sql);
	$rc = $rs->RowCount();

	$arr=array();
	while($row = $rs->FetchRow()){
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
	}

	return $arr;
}

/*
+---------------------------------------------------------------------------
|	Update Notified Record
+---------------------------------------------------------------------------
*/
function updateNotified($update_table, $update_field, $update_where, $update_detail_id, $update_detail_where) {
	global $db;

	// Dont Understand the need for $update_detail_id and $update_detail_where so I am ignoring them
	$sql = "UPDATE ".$update_table." SET ".$update_field."=SYSDATE".$update_where;

	$db->Execute($sql);

	//echo "<p><h3>updateNotified:</h3>".$sql."</p>";
}

/*
+---------------------------------------------------------------------------
|	Create Schedule Record
+---------------------------------------------------------------------------
*/
function createSchedule($value) {
	global $db;
	$sqlok = true;
	$ret = 0;

	$fields = "REGISTER_ORIGIN,SCHEDULER_DESCRIPTION,COMPLETION_DATE,MANAGED_BY_ID,SITE_ID,DEPARTMENT_ID,ACTION_RAISED_DATE,FREQUENCY,NO_OF_DAYS_NOTICE,REPORT_ID,ACTION_TITLE,STATUS";
	$values = "'".$value['REGISTER_ORIGIN']."','".$value['SCHEDULER_DESCRIPTION']."','".$value['COMPLETION_DATE']."',".$value['MANAGED_BY_ID'].",".$value['SITE_ID'].",".$value['DEPARTMENT_ID'].",'".$value['ACTION_RAISED_DATE']."',".$value['FREQUENCY'].",".$value['NO_OF_DAYS_NOTICE'].",".$value['REPORT_ID'].",'".$value['ACTION_TITLE']."','".$value['STATUS']."'";

	if ($value['SECTION_ID'] != '') {
		$fields = $fields.",SECTION_ID";
		$values = $values.",".$value['SECTION_ID'];
	}

	$sql = "INSERT INTO tblscheduler (".$fields.") VALUES (".$values.")";
	$sql = str_replace("\'","''",$sql);

	$sqlok = $db->Execute($sql);

	if ((bool)$sqlok)
		$ret = $db->Insert_ID("schedules_seq");

	//echo "<p><h3>createSchedule:</h3>".$sql."</p>";
	echo "<h3>New Schedule ID is ".$ret."</h3>";

	return $ret;
}

/*
+---------------------------------------------------------------------------
|	Create Action Record
+---------------------------------------------------------------------------
*/
function createAction($value) {
	global $db;
	$sqlok = true;
	$ret = 0;

	if ($value['SECTION_ID'] == "")
		$value['SECTION_ID'] = 'NULL';

	if ($value['ALLOCATED_TO_ID'] == "")
		$value['ALLOCATED_TO_ID'] = 'NULL';

	$fields = "ACTION_ID,REGISTER_ORIGIN,ACTION_TYPE,ACTION_DESCRIPTION,SCHEDULED_DATE,REMINDER_DATE,MANAGED_BY_ID,ALLOCATED_TO_ID,STATUS,ORIGIN_TABLE,REPORT_ID,SITE_ID,DEPARTMENT_ID,ACTION_TITLE,SECTION_ID";
	$values = $value['ACTION_ID'].",'".$value['REGISTER_ORIGIN']."','".$value['ACTION_TYPE']."','".$value['ACTION_DESCRIPTION']."','".$value['SCHEDULED_DATE']."','".$value['REMINDER_DATE']."',".$value['MANAGED_BY_ID'].",".$value['ALLOCATED_TO_ID'].",'".$value['STATUS']."','".$value['ORIGIN_TABLE']."',".$value['REPORT_ID'].",".$value['SITE_ID'].",".$value['DEPARTMENT_ID'].",'".$value['ACTION_TITLE']."',".$value['SECTION_ID'];

	$sql = "INSERT INTO tblaction_details (".$fields.") VALUES (".$values.")";
	$sql = str_replace("\'","''",$sql);

	$sqlok = $db->Execute($sql);

	if ((bool)$sqlok) {
		$ret = $db->Insert_ID("tblaction_details_seq");

		// now add the action category
		$fields = "ACTIONID,CHECKBOXID,CHECKBOXTYPE";
		$values = $ret.",6,'ActionCategory'";
		$sql = "INSERT INTO tblaction_checkboxvalues (".$fields.") VALUES (".$values.")";
		$sql = str_replace("\'","''",$sql);

		$sqlok = $db->Execute($sql);
	}

	//echo "<p><h3>createAction:</h3>".$sql."</p>";
	echo "<h3>New Action ID is ".$ret."</h3>";

	return $ret;
}

/*
+---------------------------------------------------------------------------
|	Update Action Record
+---------------------------------------------------------------------------
*/
function updatePCRAction($value) {
	global $db;
	$ret = true;

	$sql = "UPDATE tblpcr_actions SET status='".$value['STATUS']."', action_register_id=".$value['ACTION_REGISTER_ID'].", manager_notified=SYSDATE, stage".$value['STAGE_ID']."_notified=SYSDATE, due_date='".$value['DUE_DATE']."' WHERE detail_id=".$value['DETAIL_ID'];

	$ret = $db->Execute($sql);

	//echo "<p><h3>updatepcrAction:</h3>".$sql."</p>";

	return (bool)$ret;
}

/*
+---------------------------------------------------------------------------
|	Send PCR Action Email
+---------------------------------------------------------------------------
*/
function sendPCRActionEmail($managed_recipient, $allocated_recipient, $PCRAction_Arr) {

	print_r($managed_recipient);
	print_r($allocated_recipient);
	print_r($PCRAction_Arr);

	$email_arr = get_email_messages("Actions","Managed By", $managed_recipient['SITE_ID']);

	// Build the email message
	foreach($email_arr as $key => $email) {
		$email['MESSAGE'] .= "

	";

		foreach($PCRAction_Arr as $key => $action) {
			$email['MESSAGE'] .= 'PCR No.:
	'.$action['PCR_DISPLAYID'].'
	';
			$email['MESSAGE'] .= 'PCR Title:
	'.$action['ACTION_TITLE'].'
	';
			$email['MESSAGE'] .= 'Action Title:
	'.$action['ACTION_DESCRIPTION'].'
	';
			$email['MESSAGE'] .= 'Action Number:
	'.$action['ACTION_ID'].'
	';
			$email['MESSAGE'] .= 'Action Due Date:
	'.$action['DUE_DATE'].'
	';
			$email['MESSAGE'] .= "Link: http://".CATS_SERVER."/index.php?m=actions&p=edit&id=".$action['ACTION_ID']."

	";
		}
		// Set the From Address
		$email['FROM'] = db_get_one("SELECT email_from_address FROM tblsite WHERE site_id=".$managed_recipient['SITE_ID']);

		// Set the To Addresses
		$email['RECIPIENT'] = array(
			array('NAME' => $managed_recipient['NAME'],'EMAIL' => $managed_recipient['EMAIL_ADDRESS']),
			array('NAME' => $allocated_recipient['NAME'],'EMAIL' => $allocated_recipient['EMAIL_ADDRESS'])
		);

		SendEmail($email);
	}
}

/*
+---------------------------------------------------------------------------
|	Send Email
+---------------------------------------------------------------------------
*/
function SendEmail($email) {

	// Decompose the email addresses from the names
	$email_to = "";
	foreach($email['RECIPIENT'] as $ekey => $evalue) {
		$email_to = $evalue['EMAIL'];

		// Get the Site Administrator's Email Address
		$email_admin = $email['FROM'];

		// Set the Subject and Body
		$subject = $email['SUBJECT'];
		$body = $email['MESSAGE'];

		// Are the Recipients addresses valid non-blank
		if(strlen($email_to) == 0) {
			echo "-->The mail below has not been sent as the intended recipients address could not be resolved. :<br><pre>", $subject,"<BR>",$body, "</pre>";
//			$email_to = $email_admin;
//			$body .= "
//			You are receiving this email as the intended recipients address could not be resolved.
//			";
		} else {

			// Add a link to CATS
			$body .= "
			Link: http://".CATS_SERVER."
			";

			// Set Addresses
			if(CATS_MAIL_STATUS == 1) {
				$to = "testcats@tronox.com";
				$from = "catshelpdesk.v3@tronox.com";

				// Add Original Receipients and Sender to Test Message
				$body .= "


				Original Recipients : ".$email_to."
				Original Sender      : ".$email['FROM'];

			} else {
				$to = $email_to;
				$from = $email['FROM'];
			}

			// Create the E-Mail
			$m= new Mail; 				// create the mail object
			$m->From( $from );			// set the from
			$m->To( $to );				// set the to
			$m->Subject( $subject );	// set the subject
			$m->Body( $body );			// set the body
			$m->Priority(4) ;			// set the priority (default Low(4))
			$ret = $m->Send();			// send the mail
			if ((bool)$ret) {
				echo "The mail below has been sent:<br><pre>", $m->Get(), "</pre>";
			} else {
				echo "The mail below has failed:<br><pre>", $m->Get(), "</pre>";
			}
		}

	}

	return (bool)$ret;
}
?>

<h1> CreateScheduledPCRActions Cronjob Completed</h1>
</body>
</html>