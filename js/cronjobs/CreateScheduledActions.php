<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>CreateScheduledActions Cronjob</title>
</head>
<body>
<h1> CreateScheduledActions Cronjob Started</h1>

<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

switch($host_name) {
	case 'catsdev' :
		echo "<h2> Running Script over DEV </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catsdev31' :
		echo "<h2> Running Script over DEV31 </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev31/');
		break;
	case 'catstst' :
		echo "<h2> Running Script over TST </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	case 'catsprd' :
		echo "<h2> Running Script over PRD </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/prd/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;
}

define('CATS_SERVER',$host_name);
define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php
define('CATS_REMOTE_PATH',CATS_INCLUDE_PATH.'remote/');

// load db stuff
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/cats_admin.ini");
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}

// Include the DB Functions
require_once(CATS_FUNCTIONS_PATH . 'db_functions.php');

// Include the Min functions
require_once(CATS_FUNCTIONS_PATH . 'main_functions.php');

// Include the Mail Class
require_once(CATS_CLASSES_PATH . 'mail.class.php');

// Set Debugging to True
$db->debug = true;

// connect to the database
db_connect();

// Load the Email Config
EmailConfig();

if(CATS_MAIL_STATUS >= 1) {
	// Process the schedule
	CreateActions();
}

// Close the database
$db = db_close();


/*
+---------------------------------------------------------------------------
|	Load the Email Configuration
+---------------------------------------------------------------------------
*/
function EmailConfig() {
	$sql = "SELECT * FROM tblemailoptions";
	$rs = db_query($sql);
	// build the array of fields
	$arr=array();
	$row = $rs->FetchRow();
	$fcnt=$rs->FieldCount();
	for($i=0;$i<$fcnt;$i++){
		// get field object so we know what we are dealing with
		$fld=$rs->FetchField($i);
		// get the field type
		$type = $rs->MetaType($fld->type);
		switch($type){
			case 'D':case 'T': // format the date value
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'N':case 'I': // don't really need this one as this is the same as the default case
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'C':case 'X': // format text value
				$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
				break;
			default: // default value is simply the value of the field
				$arr[$fld->name] = $row[$fld->name];
				break;
		}
	}
	define('CATS_MAIL_STATUS',$arr['EMAIL_STATUS']);
	define('CATS_MAIL_HOST',$arr['EMAIL_SERVER']);
	define('CATS_MAIL_TEST_ADDRESS',$arr['EMAIL_TESTADDRESS']);
	define('CATS_MAIL_TEST_NAME',$arr['EMAIL_TESTNAME']);
	define('CATS_MAIL_FROM_ADDRESS',$arr['EMAIL_FROMADDRESS']);
	define('CATS_MAIL_FROM_NAME',$arr['EMAIL_FROMNAME']);
	//define('CATS_MAIL_TRANSPORT','smtp');
}

/*
+---------------------------------------------------------------------------
|	Create the Actions
+---------------------------------------------------------------------------
*/
function CreateActions() {
	$manager_arr = array();

	$sql = "SELECT DISTINCT managed_by_id FROM tblscheduler WHERE action_raised_date IS NOT NULL AND action_raised_date<=SYSDATE AND status!='Off'";
	$rs = db_query($sql);
	$rc = $rs->RowCount();

	// Build the array of managers
	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			}
		}
		// Add the Manager ID to the Array
		$manager_arr[] = array('EMPLOYEE_NUMBER' => $arr['MANAGED_BY_ID']);
	}

	// Now get the Manager Details
	foreach($manager_arr as $key => $value) {
		$sql = "SELECT * FROM tblemployee_details WHERE employee_number=".$value['EMPLOYEE_NUMBER'];
		$rs = db_query($sql);
		$rc = $rs->RowCount();

		// Extend the array of managers
		while($row = $rs->FetchRow()){
			$arr=array();
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
		}
		$manager_arr[$key]['EMP_NAME'] = $arr['FIRST_NAME'].' '.$arr['LAST_NAME'];
		$manager_arr[$key]['EMAIL_ADDRESS'] = $arr['EMAIL_ADDRESS'];
		$manager_arr[$key]['SITE_ID'] = $arr['SITE_ID'];
	}

	// Now Process the emails
	foreach($manager_arr as $key => $value) {
		$fields = "SCHEDULER_ID, SCHEDULER_DESCRIPTION, TO_CHAR(COMPLETION_DATE, 'YYYY-MM-DD') as Completion_Date, MANAGED_BY_ID, ALLOCATED_TO_ID, SITE_ID, DEPARTMENT_ID, TO_CHAR(ACTION_RAISED_DATE, 'YYYY-MM-DD') as action_raised_date, FREQUENCY, NO_OF_DAYS_NOTICE, REPORT_ID, ACTION_TITLE, SECTION_ID, STATUS, REGISTER_ORIGIN";
		$sql = "SELECT ".$fields." FROM tblscheduler WHERE action_raised_date IS NOT NULL AND action_raised_date<=SYSDATE AND status!='Off' AND managed_by_id=".$value['EMPLOYEE_NUMBER'];
		$rs = db_query($sql);
		$rc = $rs->RowCount();

		// For Each Reminder Email
		$Scheduler_Arr=array();
		$Action_Arr=array();
		while($row = $rs->FetchRow()){
			$arr=array();
			$fcnt=$rs->FieldCount();
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
			// Set the Array Key
			$key = $arr['SCHEDULER_ID'];
			
			// If allocated to is blank set to 0
			if ($arr['ALLOCATED_TO_ID'] == '') {
				$arr['ALLOCATED_TO_ID'] = 0;
			}


			// Build Array of Schedules
			$Scheduler_Arr[$key] = array(
				'SCHEDULER_ID' => $arr['SCHEDULER_ID'],
				//'REGISTER_ORIGIN' => $arr['SCHEDULER_DESCRIPTION'],
				'COMPLETION_DATE' => $arr['COMPLETION_DATE'],
				'MANAGED_BY_ID' => $arr['MANAGED_BY_ID'],
				'ALLOCATED_TO_ID' => $arr['ALLOCATED_TO_ID'],
				'SITE_ID' => $arr['SITE_ID'],
				'DEPARTMENT_ID' => $arr['DEPARTMENT_ID'],
				'ACTION_RAISED_DATE' => $arr['ACTION_RAISED_DATE'],
				'FREQUENCY' => $arr['FREQUENCY'],
				'NO_OF_DAYS_NOTICE' => $arr['NO_OF_DAYS_NOTICE'],
				'REPORT_ID' => $arr['REPORT_ID'],
				'ACTION_TITLE' => $arr['ACTION_TITLE'],
				'SECTION_ID' => $arr['SECTION_ID'],
				'STATUS' => $arr['STATUS'],
				'REGISTER_ORIGIN' => $arr['REGISTER_ORIGIN'],
				);

			if ($arr['NO_OF_DAYS_NOTICE'] > 0)
				$reminder_date = date('Y-m-d',strtotime("-".$arr['NO_OF_DAYS_NOTICE']." days",strtotime($arr['COMPLETION_DATE'])));
			else
				$reminder_date = '';
				
				// If no allocated to person then set site, department and section from managed by person
					echo "<h3>Allocated To ID is ".$arr['ALLOCATED_TO_ID']."</h3>";
				if ($arr['ALLOCATED_TO_ID'] == 0) {
					$wk_site_id = db_get_one("SELECT site_id FROM tblemployee_details WHERE employee_number=".$arr['MANAGED_BY_ID']);
					$wk_department_id = db_get_one("SELECT department_id FROM tblemployee_details WHERE employee_number=".$arr['MANAGED_BY_ID']);
					$wk_section_id = db_get_one("SELECT section_id FROM tblemployee_details WHERE employee_number=".$arr['MANAGED_BY_ID']);
				} else {	
					$wk_site_id = $arr['SITE_ID'];
					$wk_department_id = $arr['DEPARTMENT_ID'];
					$wk_section_id = $arr['SECTION_ID'];
				}						

			// Build Array of Actions
			$Action_Arr[$key] = array(
				'ACTION_ID' => 0,
				'REGISTER_ORIGIN' => $arr['REGISTER_ORIGIN'],
				'ACTION_TYPE' => 'Planned',
				'ACTION_DESCRIPTION' => $arr['SCHEDULER_DESCRIPTION'],
				'SCHEDULED_DATE' => date('Y-m-d',strtotime($arr['COMPLETION_DATE'])),
				'REMINDER_DATE' => $reminder_date,
				'CHANGE_COMMENTS' => '',
				'MANAGED_BY_ID' => $arr['MANAGED_BY_ID'],
				'ALLOCATED_TO_ID' => $arr['ALLOCATED_TO_ID'],
				'NOTIFY_ALLOCATED_EMAIL' => 0,
				'NOTIFY_ALLOCATED_PRINT' => 0,
				'STATUS' => 'Open',
				'CLOSING_DATE' => '',
				'CLOSED_BY' => 0,
				'DATE_DUE_CHANGED_HISTORY' => '',
				'ORIGIN_TABLE' => 'Originating_Schedule',
				'REPORT_ID' => $arr['SCHEDULER_ID'],
				'RECORD_COMMENTS' => '',
				'SITE_ID' => $wk_site_id,
				'DEPARTMENT_ID' => $wk_department_id,
				'AT' => '',
				'RL' => '',
				'ACTION_TITLE' => $arr['ACTION_TITLE'],
				'RECORD_LOCATION' => '',
				'SECTION_ID' => $wk_section_id,
				'DATE_CREATED' => 'SYSDATE',
				'CRITICAL' => ''
				);
		}

		// Create the Action & Update the schedule
		foreach($Scheduler_Arr as $key1 => $value1) {
			$Action_Arr[$key1]['ACTION_ID'] = CreateAction($Action_Arr[$key1]);
			if ($Action_Arr[$key1]['ACTION_ID'] > 0)
				UpdateSchedule($value1);
		}
		print_r($value);
		SendEmail($value['EMP_NAME'], $value['EMAIL_ADDRESS'], $value['SITE_ID'], $Action_Arr);
		
		// If we have an allocated to person send them an e-mail
		if ($arr['ALLOCATED_TO_ID'] != 0) {
			SendEmail_AllocatedTo($arr['ALLOCATED_TO_ID'], $Action_Arr);
		}
	}
}

/*
+---------------------------------------------------------------------------
|	Create the Action
+---------------------------------------------------------------------------
*/
function CreateAction($value) {
	global $db;
	$sqlok = true;
	$ret = 0;

	if ($value['SECTION_ID'] == "")
		$value['SECTION_ID'] = 'NULL';

	$fields = "ACTION_ID,REGISTER_ORIGIN,ACTION_TYPE,ACTION_DESCRIPTION,SCHEDULED_DATE,REMINDER_DATE,MANAGED_BY_ID,ALLOCATED_TO_ID,STATUS,ORIGIN_TABLE,REPORT_ID,SITE_ID,DEPARTMENT_ID,ACTION_TITLE,SECTION_ID";
	$values = $value['ACTION_ID'].",'".$value['REGISTER_ORIGIN']."','".$value['ACTION_TYPE']."',q'#".$value['ACTION_DESCRIPTION']."#','".$value['SCHEDULED_DATE']."','".$value['REMINDER_DATE']."',".$value['MANAGED_BY_ID'].",".$value['ALLOCATED_TO_ID'].",'".$value['STATUS']."','".$value['ORIGIN_TABLE']."',".$value['REPORT_ID'].",".$value['SITE_ID'].",".$value['DEPARTMENT_ID'].",'".$value['ACTION_TITLE']."',".$value['SECTION_ID'];
	$sql = "INSERT INTO tblaction_details (".$fields.") VALUES (".$values.")";

	$sqlok = $db->Execute($sql);

	if ((bool)$sqlok) {
		$ret = $db->Insert_ID("tblaction_details_seq");

		// now add the action category
		$fields = "ACTIONID,CHECKBOXID,CHECKBOXTYPE";
		$values = $ret.",6,'ActionCategory'";
		$sql = "INSERT INTO tblaction_checkboxvalues (".$fields.") VALUES (".$values.")";

		$sqlok = $db->Execute($sql);
	}

	//echo "<p>".$sql."</p>";
	echo "<h3>New Action ID is ".$ret."</h3>";

	return $ret;
}

/*
+---------------------------------------------------------------------------
|	Update the Schedule
+---------------------------------------------------------------------------
*/
function UpdateSchedule($value) {
	global $db;

	if ($value['FREQUENCY'] > 0) {
		$completion_date = date('Y-m-d',strtotime("+".($value['FREQUENCY']*30)." days",strtotime($value['COMPLETION_DATE'])));
		$action_raised_date = date('Y-m-d',strtotime("-".$value['NO_OF_DAYS_NOTICE']." days",strtotime($completion_date)));
	} else {
		$action_raised_date = '';
		$completion_date = date('Y-m-d',strtotime($value['COMPLETION_DATE']));
	}
	$sql = "UPDATE tblscheduler SET action_raised_date='".$action_raised_date."', completion_date='".$completion_date."' WHERE scheduler_id=".$value['SCHEDULER_ID'];

	$db->Execute($sql);

	//echo "<p>".$sql."</p>";
}

/*
+---------------------------------------------------------------------------
|	Send the email function
+---------------------------------------------------------------------------
*/
function SendEmail($Employee_Name,$Employee_Email,$Site,$Action_Arr) {

	// Set the Subject and Body
	$subject = 'Action/s to Manage generated by Scheduler on: '.date("d/m/y");
	$body = 'You have been assigned to manage the following action/s:
(NOTE:  Please verify each action and ensure it has been allocated to the correct person.)

	';

	// Build the List of Actions
	foreach($Action_Arr as $key => $value) {
		$body .= 'Action Title:       '.$value['ACTION_TITLE'].'
	';
		$body .= 'Action Number:      '.$value['ACTION_ID'].'
	';
		$body .= 'Action Description: '.$value['ACTION_DESCRIPTION'].'
	';
		$body .= 'Date Due:           '.$value['SCHEDULED_DATE'].'

	';
	}

	// Get the Site Administrator's Email Address
	$sql = "SELECT email_from_address FROM tblsite WHERE site_id=".$Site;
	$email_admin = db_get_one($sql);

	// Are the Recipients addresses valid non-blank
	if(strlen($Employee_Email) == 0) {
		echo "-->The mail below has not been sent as the intended recipients address could not be resolved. :<br><pre>", $subject,"<BR>",$body, "</pre>";
		$Employee_Email = $email_admin;
		$body .= "
		You are receiving this email as the intended recipients address could not be resolved.
		";
	} else {

		// Add a link to CATS
		$body .= "
		Link: http://".CATS_SERVER."
		";

		// Set Addresses
		if(CATS_MAIL_STATUS == 1) {
			$to = "testcats@tronox.com";
			$from = "catshelpdesk.v3@tronox.com";

			// Add Original Recipients and Sender to Test Message

			$body .= "


			Original Recipient : ".$Employee_Email."
			Original Sender     : ".$email_admin;

		} else {
			$to = $Employee_Email;
			$from = $email_admin;
		}

		// Create the E-Mail
		$m= new Mail; 				// create the mail object
		$m->From( $from );			// set the from
		$m->To( $to );				// set the to
		$m->Subject( $subject );	// set the subject
		$m->Body( $body );			// set the body
		$m->Priority(4) ;			// set the priority (default Low(4))
		$ret = $m->Send();			// send the mail
		if ((bool)$ret) {
			echo "The mail below has been sent:<br><pre>", $m->Get(), "</pre>";
		} else {
			echo "The mail below has failed:<br><pre>", $m->Get(), "</pre>";
		}
	}
	return (bool)$ret;
}

/*
+---------------------------------------------------------------------------
|	Send the email to allocated to person 
+---------------------------------------------------------------------------
*/
function SendEmail_AllocatedTo($id,$Action_Arr) {
	
	$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
	$rs_emp = db_get_array($sql);
	$Employee_Name = $rs_emp["EMP_NAME"];
	$Employee_Email = $rs_emp["EMAIL_ADDRESS"];
	$Site = $rs_emp["SITE_ID"];

	// Set the Subject and Body
	$subject = 'Allocated Action generated by Scheduler on: '.date("d/m/y");
	//$body = 'You have been assigned to complete the following action:\n\n';
	$body = 'You have been assigned to complete the following action:


	';

	// Build the List of Actions
	foreach($Action_Arr as $key => $value) {
		$body .= 'Action Number:      '.$value['ACTION_ID'].'
	';
		$body .= 'Action Title:       '.$value['ACTION_TITLE'].'
	';
		$body .= 'Action Description: '.$value['ACTION_DESCRIPTION'].'
	';
		$body .= 'Date Due:           '.$value['SCHEDULED_DATE'].'

	';
	}

	// Get the Site Administrator's Email Address
	$sql = "SELECT email_from_address FROM tblsite WHERE site_id=".$Site;
	$email_admin = db_get_one($sql);

	// Are the Recipients addresses valid non-blank
	if(strlen($Employee_Email) == 0) {
		echo "-->The mail below has not been sent as the intended recipients address could not be resolved. :<br><pre>", $subject,"<BR>",$body, "</pre>";
		$Employee_Email = $email_admin;
		$body .= "
		You are receiving this email as the intended recipients address could not be resolved.
		";
	} else {

		// Add a link to CATS
		//$body .= "
		//Link: http://".CATS_SERVER."
		//";

		$body .= "Click the link to go to the Action: http://".CATS_SERVER."/index.php?m=actions&p=edit&id=".$value['ACTION_ID']."
		";
		
		//$message .= "Click the link below to go to the Action\n";
		//$message .= WS_PATH . 'index.php?m=actions&p=edit&id='.$action_id;

		// Set Addresses
		if(CATS_MAIL_STATUS == 1) {
			$to = "testcats@tronox.com";
			$from = "catshelpdesk.v3@tronox.com";

			// Add Original Recipients and Sender to Test Message

			$body .= "


			Original Recipient : ".$Employee_Email."
			Original Sender     : ".$email_admin;

		} else {
			$to = $Employee_Email;
			$from = $email_admin;
		}

		// Create the E-Mail
		$m= new Mail; 				// create the mail object
		$m->From( $from );			// set the from
		$m->To( $to );				// set the to
		$m->Subject( $subject );	// set the subject
		$m->Body( $body );			// set the body
		$m->Priority(4) ;			// set the priority (default Low(4))
		$ret = $m->Send();			// send the mail
		if ((bool)$ret) {
			echo "The mail below has been sent:<br><pre>", $m->Get(), "</pre>";
		} else {
			echo "The mail below has failed:<br><pre>", $m->Get(), "</pre>";
		}
	}
	return (bool)$ret;
}

?>

<h1> CreateScheduledActions Cronjob Completed</h1>
</body>
</html>