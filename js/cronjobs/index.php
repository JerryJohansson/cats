<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cronjobs</title>
</head>

<body>
<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

switch($host_name) {
	case 'catsdev' :
		echo "<h1> Cronjobs in DEV </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catsdev31' :
		echo "<h1> Cronjobs in DEV 3.1</h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev31/');
		break;
	case 'catstst' :
		echo "<h1> Cronjobs in TST </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;	
	}
?>

<h2>Output To Browser</h2>
<p><a href="EmailOverdueActions.php" target="_blank">Email Overdue Actions</a></p>
<p><a href="CreateScheduledActions.php" target="_blank">Create Scheduled Actions</a></p>
<p><a href="CreateScheduledPCRActions.php" target="_blank">Create Scheduled PCR Actions</a></p>

<h2>Output To Log File</h2>
<p><a href="Log.php?cronjob=EmailOverdueActions" target="_blank">Email Overdue Actions</a></p>
<p><a href="Log.php?cronjob=CreateScheduledActions" target="_blank">Create Scheduled Actions</a></p>
<p><a href="Log.php?cronjob=CreateScheduledPCRActions" target="_blank">Create Scheduled PCR Actions</a></p>

<h2>Log Files</h2>
<table border="5" align="left" cellpadding="5" cellspacing="5">
<thead>
<th>File</th>
<th>Size (bytes)</th>
<th>Changed Date</th>
</thead>
<?php
$dir = CATS_ROOT_PATH."/js/cronjobs/logs/";

// Open a known directory, and proceed to read its contents
if (is_dir($dir)) {
	if ($dh = opendir($dir)) {
		while (($file = readdir($dh)) !== false) {
			//echo "<p>filename: $file : filetype: " . filetype($dir . $file) . "</p>";
			if (filetype($dir.$file) == 'file') {
				echo '<tr>';
				echo '<td><a href="logs/'.$file.'" target="_blank">'.$file.'</a></td>';
				echo '<td>'.filesize($dir.$file).'</td>';
				echo '<td>'.date("d F Y H:i:s.", filectime($dir.$file)).'</td>';
				echo '<tr>';
			}
		}
	closedir($dh);
	}
}
?>
</table>
</body>
</html>