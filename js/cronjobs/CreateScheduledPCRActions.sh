#!/bin/sh
# Program:	CreateScheduledPCRActions.sh
# Date:		27/09/2006
# Author:	Kevin Lilje
#
# Purpose:
#	This script is designed to run php scripts under the web (using curl)
#
# *** Revision History ***
#
#

BASE_DIR="/apps/CATS/prd/js/cronjobs/logs"


###. $HOME/.profile

LOGFILE="${BASE_DIR}/CreateScheduledPCRActions_sh.`date +"%d"`.log"	    # Log file of this script

#################################################################################
# Re-direct standard output (and any errors) to a log file                    	#
#################################################################################
exec > $LOGFILE 2>&1

#################################################################################
# Timestamp...																	#
#################################################################################
echo "*******************************************************"
echo "                      CreateScheduledPCRActions Script     "
echo "                      `date`  "
echo "*******************************************************"
echo "\n"
echo "Logfile is $LOGFILE\n\n "
echo "Base Dir is ${BASE_DIR}\n\n "

#################################################################################
# First, check Base Dir is valid.  If it's not, quit out						#
#################################################################################

if (  ( [ ! -d "$BASE_DIR" ] )  )
then
	echo " ERROR!  - The Base directory specified:"
	echo "           $BASE_DIR"
	echo "           Does not exist.  Either this or the"
	echo "           admin subdirectory is not there!"
	echo "           Please check the above location(s)"
	echo " PROGRAM ABORTED. "
	exit 1
fi

rsh csprd -l oracle -n "cjlog -replicate `hostname` -write cats PCRscheduled START 'CreateScheduledPCRActions' $$"

curl http://catsprd/js/cronjobs/Log.php?cronjob=CreateScheduledPCRActions

rsh csprd -l oracle -n "cjlog -replicate `hostname` -write cats PCRscheduled FINISH 'CreateScheduledPCRActions' "

echo "\n\n\n"
echo "CreateScheduledPCRActions Finished"
date
echo "*******************************************************"

