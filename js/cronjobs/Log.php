<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Log Cronjob</title>
</head>

<body>
<h1>Log Cronjob Started</h1>

<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

switch($host_name) {
	case 'catsdev' :
		echo "<h2> Running Script over DEV </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catstst' :
		echo "<h2> Running Script over TST </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	case 'catsprd' :
		echo "<h1> Cronjobs in PRD </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/prd/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;	
	}
	
// Get the Cronjob to call
if (isset($_GET['cronjob'])) {
	$cronjob = $_GET['cronjob'];
} else {
		echo "<p> Cronjob to run not specified !</p>";
		echo "</body></html>";
		die;	
}


// Now define the Cronjob Log Path
define('CATS_CRON_PATH','http://'.$host_name.'/js/cronjobs/');
define('CATS_CRON_LOG_PATH',CATS_ROOT_PATH.'js/cronjobs/logs/');
	
// First generate the file name
$filename = CATS_CRON_LOG_PATH.$cronjob.'_'.date('d').'.html';
echo "<p>Filename is :".$filename."</p>";

// Open the Log File
if($logfile = fopen($filename,"w")) {
	echo "<p>Opened Log File</p>";
		
	// Get the Page Data
	$fp = fsockopen($host_name, 80, $errno, $errstr, 900);
	if (!fp) {
		fwrite($logfile,$errno.":".errstr);
		echo "<p>Failed to Open TCP Connection</p>";
		echo "<p>".$errno." : ".$errstr."</p>";
	} else {
		echo "<p>Opened TCP Connection to ".$host_name."</p>";
		
		$out = "GET /js/cronjobs/".$cronjob.".php HTTP/1.0\r\n";
		$out .= "Host: ".$host_name."\r\n";
		$out .= "Connection: Close\r\n\r\n";
		
		// Send the HTTP Request
		fwrite($fp, $out);
		echo "<p>Sent Request for Page /js/cronjobs/".$cronjob.".php</p>";
		echo "<p>".$out."</p>";
		
		while (!feof($fp)) {
			$pagecontents = fgets($fp, 4096);
			fwrite($logfile,$pagecontents);
			
			echo "<p>Processing Response in 4096 Byte Chunks</p>";
		}
		// Close the TCP Connection
		fclose($fp);
		echo "<p>Closed TCP Connection</p>";
	}
	
	// Close the file
	fclose($logfile);
	echo "<p>Closed Log File</p>";
	}
	
?>
<h1>Log Cronjob Finished</h1>

</body>
</html>