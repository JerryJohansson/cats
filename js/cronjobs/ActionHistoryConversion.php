<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>ActionHistoryConversion Cronjob</title>
</head>
<body>
<h1> ActionHistoryConversion Cronjob Started</h1>

<?php
// Define the Common Contants
$baseDir = dirname(__FILE__);

// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

switch($host_name) {
	case 'catsdev' :
		echo "<h2> Running Script over DEV </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catstst' :
		echo "<h2> Running Script over TST </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	case 'catsprd' :
		echo "<h2> Running Script over PRD </h2>";
		define('CATS_ROOT_PATH','/apps2/CATS/prd/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;	
}

define('CATS_SERVER',$host_name);
define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php
define('CATS_REMOTE_PATH',CATS_INCLUDE_PATH.'remote/');

// load db stuff
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/cats_admin.ini");
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}

// Include the DB Functions
require_once(CATS_FUNCTIONS_PATH . 'db_functions.php');

// Include the Min functions
require_once(CATS_FUNCTIONS_PATH . 'main_functions.php');

// Include the Mail Class
require_once(CATS_CLASSES_PATH . 'mail.class.php');

// Set Debugging to True
$db->debug = true;

// connect to the database
db_connect();

// Read the Action History
ReadHistory();

// Close the database
$db = db_close();



/*
+---------------------------------------------------------------------------
|	Read the Action History
+---------------------------------------------------------------------------
*/
function ReadHistory() {
	global $db;
	
	$sql = "SELECT action_id, date_due_changed_history, status FROM tblaction_details WHERE date_due_changed_history IS NOT NULL";
	//$sql = "SELECT action_id, date_due_changed_history, status FROM tblaction_details WHERE action_id=175";
	$rs = db_query($sql);
	$rc = $rs->RowCount();
	
	// Build the array of managers
	while($row = $rs->FetchRow()){
		$arr=array();
		$fcnt=$rs->FieldCount();
		for($i=0;$i<$fcnt;$i++){
			// get field object so we know what we are dealing with
			$fld=$rs->FetchField($i);
			// get the field type
			$type = $rs->MetaType($fld->type);
			switch($type){
				case 'D':case 'T': // format the date value
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'N':case 'I': // don't really need this one as this is the same as the default case
					$arr[$fld->name] = $row[$fld->name];
					break;
				case 'C':case 'X': // format text value
					$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
					break;
				default: // default value is simply the value of the field
					$arr[$fld->name] = $row[$fld->name];
					break;
			} // End of switch($type)
		} // End of for($i=0;$i<$fcnt;$i++)
		
		$history = $arr['DATE_DUE_CHANGED_HISTORY'];
		echo "<p>".$history."</p>";
		
		$action_history = array();
		
		// now try and break it down by line
		$lines = explode("\n",$history);
		
		foreach($lines as $key => $value) {
			// break each line down by tabs
			$fields = explode("\t",$value);
			
			// Test to See if in correct format
			if (count($fields) == 3) {
				// Get Date in correct format
				$date_array = explode("/",$fields[0]);
				$us_date = $date_array[1]."/".$date_array[0]."/".$date_array[2];
				$db_date = date('Y-m-d',strtotime($us_date));
				
				// Get Employee
				$db_employee = "";
				$db_employee = db_get_one("SELECT employee_number FROM tblemployee_details WHERE access_id = '".strtoupper($fields[1])."'");
				
				// Only Add if employee was found in database
				if($db_employee != "")
					$action_history[] = array(
						'ACTION_ID' => $arr['ACTION_ID'],
						'EMPLOYEE_NUMBER' => $db_employee,
						'DATE_TIME' => $db_date,
						'COMMENTS' => $fields[2],
						'STATUS' => 'Unknown',
						'DATE_CHANGED' => 1
					);
			} else {
				// Add it on to the end of the last one
				$action_history_count = count($action_history);
				if ($action_history_count > 0) {
					$action_history[$action_history_count - 1]['COMMENTS'] .= $value;
				} // End of if if ($action_history_count > 0)
			} // End of if (count($fields) == 3)
		} // End of foreach($lines as $key => $value)
		
		print_r($action_history);
		
		// do the insert
		foreach($action_history as $key => $value) {
			$sql = "INSERT INTO action_history";
			$sql .= " (action_id, employee_number, date_time, comments, status, date_changed)";
			$sql .= " VALUES(".$value['ACTION_ID'].",".$value['EMPLOYEE_NUMBER'].",TO_TIMESTAMP('".$value['DATE_TIME']."','YYYY-MM-DD'),q'{".$value['COMMENTS']."}','".$value['STATUS']."',".$value['DATE_CHANGED'].")";
			$db->Execute($sql);
		} // End of foreach($record as $key => $value)
		
	} // End of while($row = $rs->FetchRow())
}
?>

<h1> ActionHistoryConversion Cronjob Completed</h1>
</body>
</html>