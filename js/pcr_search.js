/*
+------------------------------------------------------------------------
|  BEGIN::Helper Objects
+------------------------------------------------------------------------
*/
function _helper_show_pcr_employee(func,module){
	var a=arguments;
	var f=document.forms[0];
	var e=(f!=null)?f.elements:{}; 
	if(!top.HelperObjects[func])
		top.HelperObjects[func]=new Object();
	
	if(a.length>3){
		top.HelperObjects[func].emp_id=a[2];
		top.HelperObjects[func].emp_val=a[3];
	}
	
	if(a.length>5){
		top.HelperObjects[func].site_id=e[a[4]];
		top.HelperObjects[func].site_val=e[a[5]];
	}
	
	if(a.length>7){
		top.HelperObjects[func].dep_id=e[a[6]];
		top.HelperObjects[func].dep_val=e[a[7]];
	}
	
	if(a.length>9){
		top.HelperObjects[func].sec_id=e[a[8]];
		top.HelperObjects[func].sec_val=e[a[9]];
	}
	
	if(a.length>11){
		top.HelperObjects[func].pos_id=e[a[10]];
		top.HelperObjects[func].pos_val=e[a[11]];
	}
	
	top.HelperObjects[func].trigger_function=show_area_reason;
	
	top.helper_show_menu(func, module, null, e[a[3]]);
}
function _helper_show_site_employee(func,module){
	var a=arguments;
	var f=document.forms[0];
	var e=(f!=null)?f.elements:{}; 
	if(!top.HelperObjects[func])
		top.HelperObjects[func]=new Object();
	//alert(a[3].id+":"+a[4].id)
	if(a.length>4){
		top.HelperObjects[func].emp_id=a[3];
		top.HelperObjects[func].emp_val=a[4];
	}
	var o=new Object();
	o.value=String(a[2]);
	var force_refresh=(top.HelperObjects[func].initial_search_site_id!=a[2]);
	top.HelperObjects[func].initial_search_site_id=a[2];
	top.helper_show_menu(func, module, o, a[4], force_refresh);
}

function _helper_show_pcr_actions(func,module){
	var a=arguments;
	var f=document.forms[0];
	var e=(f!=null)?f.elements:{}; 
	if(!top.HelperObjects[func])
		top.HelperObjects[func]=new Object();
	var act=a[4];
	var mid=act.parentNode.nextSibling.firstChild; // manager id
	var man=mid.nextSibling; // manager name
	if(a[3]>1){
		var days=mid.parentNode.nextSibling.nextSibling.nextSibling.nextSibling.firstChild;
		//alert(days.name)
		top.HelperObjects[func].days=days; // how many days after stage
	}
//alert("site_id="+a[2] + ":stage=" + a[3] + " : first el=" +a[4])
	top.HelperObjects[func].act=act;
	top.HelperObjects[func].mid=mid;
	top.HelperObjects[func].man=man;

	var o=new Object();
	o.value=String(a[2]);
	top.helper_show_menu(func, module, null, o);
}
// END:: Helper Objects
//--------------------------------------------

function date_add_month(itm, to_id, months){
	var e=itm.form.elements;
	var to=e[to_id];
	var to_d=e[to_id+"_d"];
	var v=to.value;
	var d='';
	if(empty(v)){
		var a=new Date();
		d=(new Date(a.getFullYear(), (a.getMonth()+months), a.getDate()));
	}else{
		var a=v.trim().split('-');//.match(/^(\d{4})-(\d{1,2})-(\d{1,2})$/g);
		if(a) d=(new Date(parseInt(a[0]),(parseInt(a[1])+months),parseInt(a[2])));
	}
	to_d.value=d.Format('dd-mmm-yyyy');
	to.value=d.Format('yyyy-mm-dd');
}


function show_approvers(itm, row, required){
	var objRow = document.getElementById(row);
	if(itm.checked == true && itm.value=='No'){
		switch(required){
			case "Mandatory" :
				itm.checked = false;
				alert("This Reviewer is Mandatory, you cannot change this selection");
				break;
			case "Recommended" :
				alert("You can change this selection, but this Reviewer is Recommended.");
				objRow.style.display = "none";
				break;
			case "Optional" :
				objRow.style.display = "none";
				break;
		}
	}else{
		objRow.style.display = "";
	}
}

//enable all the form fields so we can save their values
function enable_pcr_form_fields(){
	var e = document.forms[0].elements.length;
	var x = e.length;
	for(var i=0;i<x;i++) if(e[i] && (e[i].type != "file")) e[i].disabled = false;
}

//show or hide the reviewers section and the details section depending on what type of review it is
function toggle_reviewer_team(){
	var e = document.forms[0].elements;
	var show = (e['TYPE_OF_REVIEW'][0].checked == true);
	var rows = element("row_reviewers");
	display = (show)?'none':'';
	//alert(rows.nextSibling.id)
	rows.style.display = display;
	rows.nextSibling.style.display = display
	//rows[1].style.display = display;
	display = (show)?'':'none';
	element('row_MEETING_DATE').style.display = display;
	element('row_MEETING_COMMENTS').style.display = display;
	var docs=element('row_Document');
	docs.style.display = display;
	docs.nextSibling.style.display = display
	//docs[1].style.display = display;
}

function get_remote_dd(id, to, module, action, selected){
	var top_options_name = module+"__"+action;
	var option_id = top_options_name+"__"+id
	var obj={
		'a':action,
		'p':id,
		'id':id
	};
	var e = null;
	var cached = false;
	if(typeof(top.gui.options[option_id])!="object") {
		top.gui.options.add(option_id);
		e = remote.get_sync(module,obj);
	} else {
		e = new Object();
		e.options = top.gui.options[option_id]._options;
		cached = true;
	}
		var selected_value=top.gui.options[option_id].replace(cached, to, e.options, selected);
}