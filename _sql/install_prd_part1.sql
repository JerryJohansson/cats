spool install_prd_part1.log

-- Create New tables
@/apps/CATS/prd/_sql/tables.sql

-- Insert New Reference Data
@/apps/CATS/prd/_sql/_insert/configuration.sql
@/apps/CATS/prd/_sql/_insert/cfg_group.sql
@/apps/CATS/prd/_sql/_insert/form_fields.sql
@/apps/CATS/prd/_sql/_insert/form_types.sql
@/apps/CATS/prd/_sql/_insert/help_fields.sql
@/apps/CATS/prd/_sql/_insert/status_values.sql
@/apps/CATS/prd/_sql/_insert/user_group.sql
@/apps/CATS/prd/_sql/_insert/user_role.sql

-- Alter / Update Existing Tables
@/apps/CATS/prd/_sql/alter_update.sql

-- Create New Sequences
@/apps/CATS/prd/_sql/sequences.sql

-- Create / Alter Views
@/apps/CATS/prd/_sql/view_funcs.sql


--in TST or DEV - log in and set email to 'Test'
prompt in TST or DEV - log in and set email to 'Test'

exit
