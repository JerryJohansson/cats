spool install_dev_part1.log

-- Create New tables
@/apps/CATS/dev/_sql/tables.sql

-- Insert New Reference Data
@/apps/CATS/dev/_sql/_insert/configuration.sql
@/apps/CATS/dev/_sql/_insert/cfg_group.sql
@/apps/CATS/dev/_sql/_insert/form_fields.sql
@/apps/CATS/dev/_sql/_insert/form_types.sql
@/apps/CATS/dev/_sql/_insert/help_fields.sql
@/apps/CATS/dev/_sql/_insert/status_values.sql
@/apps/CATS/dev/_sql/_insert/user_group.sql
@/apps/CATS/dev/_sql/_insert/user_role.sql

-- Alter / Update Existing Tables
@/apps/CATS/dev/_sql/alter_update.sql

-- Create New Sequences
@/apps/CATS/dev/_sql/sequences.sql

-- Create / Alter Views
@/apps/CATS/dev/_sql/view_funcs.sql


--in TST or DEV - log in and set email to 'Test'
prompt in TST or DEV - log in and set email to 'Test'
exit
