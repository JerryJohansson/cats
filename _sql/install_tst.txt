login to wyatt
catsadmin
precarious
[catsadmin@wyatt]$...
cd /apps/CATS/tst/_sql
sqlplus catsdba/catstst@catstst_ds1

-- now run the following sql scripts

-- Create New tables
@/apps/CATS/tst/_sql/tables.sql

-- Insert New Reference Data
@/apps/CATS/tst/_sql/_insert/configuration.sql
@/apps/CATS/tst/_sql/_insert/cfg_group.sql
@/apps/CATS/tst/_sql/_insert/form_fields.sql
@/apps/CATS/tst/_sql/_insert/form_types.sql
@/apps/CATS/tst/_sql/_insert/help_fields.sql
@/apps/CATS/tst/_sql/_insert/status_values.sql
@/apps/CATS/tst/_sql/_insert/user_group.sql
@/apps/CATS/tst/_sql/_insert/user_role.sql

-- Alter / Update Existing Tables
@/apps/CATS/tst/_sql/alter_update.sql

-- Create New Sequences
@/apps/CATS/tst/_sql/sequences.sql

-- Create / Alter Views
@/apps/CATS/tst/_sql/view_funcs.sql

-- go to a browser and run the install script using the following URL:
http://catstst/install.php

-- login and let the script run...this may take a while.
-- NOTE: DO NOT CLOSE THE WINDOW OR CLICK ANY BUTTONS ON THE SCREEN
-- the script will print out the sql used to update each user
-- when the script is finished go back to your terminal and run the following sql script

@/apps/CATS/tst/_sql/update_user_permissions.sql

--in TST or DEV - log in and set email to 'Test'
--in sqlplus on the databse server
@$ORACLE_HOME/rdbms/admin/utlrp.sql

exit
