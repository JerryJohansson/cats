-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool cfg_group.txt;

-- INSERTING into CFG_GROUP
Insert into "CFG_GROUP" ("CFG_GROUP_ID","CFG_GROUP_TITLE","CFG_GROUP_DESCRIPTION","SORT_ORDER","VISIBLE") values (1,'Site Administration','This is where we set all default application configuration settings',1,1);
Insert into "CFG_GROUP" ("CFG_GROUP_ID","CFG_GROUP_TITLE","CFG_GROUP_DESCRIPTION","SORT_ORDER","VISIBLE") values (2,'Module Options','Configuration settings specific to default and plugin modules',2,1);
Insert into "CFG_GROUP" ("CFG_GROUP_ID","CFG_GROUP_TITLE","CFG_GROUP_DESCRIPTION","SORT_ORDER","VISIBLE") values (3,'Sessions','Session options',3,1);
Insert into "CFG_GROUP" ("CFG_GROUP_ID","CFG_GROUP_TITLE","CFG_GROUP_DESCRIPTION","SORT_ORDER","VISIBLE") values (4,'Email Configuration','Email Configuration settings such as Transport Method and Linefeed type',4,1);
Insert into "CFG_GROUP" ("CFG_GROUP_ID","CFG_GROUP_TITLE","CFG_GROUP_DESCRIPTION","SORT_ORDER","VISIBLE") values (5,'User Options','User Configuration settings such as result count, history count etc',5,1);
commit;
-- turn spooling off
spool off;