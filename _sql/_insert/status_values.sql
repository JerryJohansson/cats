-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool status_values.txt;

-- INSERTING into STATUS_VALUES
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (1,'Active',1,'TBLEMPLOYEE_DETAILS',null,0,1);
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (2,'Inactive',0,'TBLEMPLOYEE_DETAILS',null,0,1);
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (3,'Open',0,'TBLACTION_DETAILS',null,0,1);
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (4,'Closed - Performed',1,'TBLACTION_DETAILS',null,0,1);
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (5,'Closed - Not Performed',2,'TBLACTION_DETAILS',null,0,1);
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (6,'Open',0,'TBLINCIDENT_DETAILS',null,0,1);
Insert into "STATUS_VALUES" ("ID","STATUS_TITLE","STATUS_MASK","STATUS_TYPE","STATUS_SUB_TYPE","GROUP_MASK","STATUS") values (7,'Closed',1,'TBLINCIDENT_DETAILS',null,0,1);
commit;
-- turn spooling off
spool off;