-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool user_role.txt;

-- INSERTING into USER_ROLE
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (1,'Super Administrator',2147483648,2);
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (2,'Administrator',128,0);
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (3,'Admin Assistant',64,0);
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (4,'Editor',32,0);
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (5,'Trainer',16,0);
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (6,'Viewer',2,0);
Insert into "USER_ROLE" ("USER_ROLE_ID","USER_ROLE_NAME","USER_ROLE_MASK","STATUS") values (7,'No Access',0,0);
commit;
-- turn spooling off
spool off;