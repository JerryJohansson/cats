-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool user_group.txt;

-- INSERTING into USER_GROUP
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (1,'Master Administrator',8388608,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (2,'Root Administrators',1048576,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (3,'Super Administrators',524288,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (4,'PCR Administrator',16384,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (5,'Incidents Privileged User',8192,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (6,'Actions Administrator',4096,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (7,'Actions Privileged User',2048,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (8,'Administrators',1024,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (9,'Admin Assistant',512,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (10,'PCR Team Member',256,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (11,'Incident Analysis',32,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (12,'Editor (Privileged)',16,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (13,'Editor (Restricted)',8,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (14,'Reader (Privileged)',4,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (15,'Reader (Restricted)',2,0);
Insert into "USER_GROUP" ("USER_GROUP_ID","USER_GROUP_NAME","USER_GROUP_MASK","STATUS") values (16,'No Access',0,0);
commit;

-- turn spooling off
spool off;