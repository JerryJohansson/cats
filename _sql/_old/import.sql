--DROP ROLE cats_admin; 
--CREATE ROLE cats_admin; 
--GRANT
--	ALL PRIVILEGES
--TO CATS_ADMIN;
--GRANT cats_admin TO catsdba;

DROP INDEX catsdba.pk_form_types_id;
DROP TABLE catsdba.form_types;
DROP SEQUENCE catsdba.form_types_seq;
commit;

CREATE TABLE catsdba.form_types (
  id NUMBER NOT NULL,
  name varchar2(128) default NULL,
  description varchar2(255) default NULL,
  icon varchar2(255) default NULL,
  icon_plus varchar2(255) default NULL,
  icon_minus varchar2(255) default NULL,
  thumbnail varchar2(255) default NULL,
  allowed_child_types varchar2(255) default NULL,
	display_function varchar2(255) default '',
	set_function varchar2(255) default '',
	get_function varchar2(255) default '',
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_types_id ON catsdba.form_types
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_types_seq;
CREATE OR REPLACE TRIGGER catsdba.form_types_trg
	BEFORE INSERT ON catsdba.form_types
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_types_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Menu', 'Standard Top Level Menu', 'menu.gif', 'menu-plus.gif', 'menu-minus.gif', 'menu.gif', '1,2,3,4,5,9,10', 'html_menu_element(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Number', 'Standard Input Field with number formatter', 'number.gif', 'number-plus.gif', 'number-minus.gif', 'number.gif', '2', 'html_number_field(', 'db_number', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Text', 'Standard Input field', 'text.gif', 'text-plus.gif', 'text-minus.gif', 'text.gif', '3', 'html_draw_input_field(', 'db_sanitize(', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Combo', 'Standard Combo List', 'combo.gif', 'combo-plus.gif', 'combo-minus.gif', 'combo.gif', '4', 'html_draw_combo_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Group', 'Group of Items/Collection', 'group.gif', 'group-plus.gif', 'group-minus.gif', 'group.gif', '5,9', 'html_group_collection(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Date', 'Standard Input field with date widget', 'date.gif', 'date-plus.gif', 'date-minus.gif', 'date.gif', '6', 'html_date_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('DateTime', 'Standard Input field with datetime widget', 'dtime.gif', 'dtime-plus.gif', 'dtime-minus.gif', 'dtime.gif', '7', 'html_date_time_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Radio', 'Standard Radio button', 'radio.gif', 'radio-plus.gif', 'radio-minus.gif', 'radio.gif', '1,2,3,4,5,8,9,10', 'html_draw_input_field(', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Checkbox', 'Standard Checkbox', 'checkbox.gif', 'checkbox-plus.gif', 'checkbox-minus.gif', 'checkbox.gif', '1,2,3,4,5,9,10', 'html_draw_checkbox_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Helper', 'Helper', 'helper.gif', 'helper-plus.gif', 'helper-minus.gif', 'helper.gif', '10', 'html_draw_help_field(', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Tabs','Tab Container', 'tab.gif', 'tab-plus.gif', 'tab-minus.gif', 'tab.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormTabs', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Edit','Form Editor', 'edit.gif', 'edit-plus.gif', 'edit-minus.gif', 'edit.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormEdit', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Search','Search Filter Form', 'search.gif', 'search-plus.gif', 'search-minus.gif', 'search.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormSearch', '', '', 0);

commit;

DROP INDEX catsdba.pk_form_fields_pid;
DROP INDEX catsdba.pk_form_fields_id;
DROP TABLE catsdba.form_fields;
DROP SEQUENCE catsdba.form_fields_seq;
commit;


CREATE TABLE catsdba.form_fields (
  field_id NUMBER NOT NULL,
  field_pid NUMBER default NULL,
  field_group_type varchar2(50) default NULL,
  field_name varchar2(128) default NULL,
  field_label varchar2(128) default NULL,
  field_type varchar2(50) default 'Menu',
	field_type_id NUMBER default 1,
  field_length NUMBER default NULL,
  field_value varchar2(1000),
  field_attributes varchar2(255) default NULL,
	field_table_name varchar2(50) default NULL,
  field_column_name varchar2(50) default NULL,
  field_display_func varchar2(255) default NULL,
	field_display_params varchar2(255) default NULL,
  field_set_func varchar2(255) default NULL,
	field_set_params varchar2(255) default NULL,
	col_num SMALLINT default NULL,
  col_span SMALLINT default NULL,
  row_span SMALLINT default NULL,
  col_count SMALLINT default NULL,
  mandatory SMALLINT default NULL,
  options varchar2(1000),
  sort_order NUMBER default 1,
  role_mask NUMBER default NULL,
  group_mask NUMBER default NULL,
  auth_site_id varchar2(255) default NULL,
	status SMALLINT default NULL,
	page_name varchar2(255) default NULL,
	origin_table varchar2(255) default NULL,
	register_origin varchar2(255) default NULL
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_fields_id ON catsdba.form_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_form_fields_pid ON catsdba.form_fields
(
    field_pid
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_fields_seq;
CREATE OR REPLACE TRIGGER catsdba.form_fields_trg
	BEFORE INSERT ON catsdba.form_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_fields_seq.nextval INTO :new.field_id FROM dual;
	END;
/
commit;

-- Top level or Main menu items
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'CATS', 'CATS - Main Menu', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'ADMIN', 'CATS - Administration', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, 128, 128, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'PCR', 'CATS - Plant Change Request', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'CFG', 'Configuration Settings', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 4, 2048, 2048, 0);

-- Menu items that link to pages
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'actions', 'Actions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'audits_and_inspections', 'Audits &'||' Inspections', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'government_requirements', 'Government Inspections', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'incidents', 'Incidents', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'major_hazards', 'Major Hazard Register', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'meeting_minutes', 'Meeting Minutes', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'other_reports', 'Other Records', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'schedules', 'Schedules', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'site_specific', 'Site Specific Obligations', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'dashboard', 'Workload Review', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 0, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'organisational_structure', 'Organisation Structure', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'reallocate', 'Re-Allocate responsibility', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'employees', 'Employees', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'incident_checkboxes', 'Incident Form Checkboxes', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'references', 'Reference Table', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'risk_definitions', 'Risk Definitions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'work_hours', 'Work Hours', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'motd', 'Message of the Day', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 1024+512+256+128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'lost_days', 'Lost Days', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'email_configuration', 'Email Configuration', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_search', 'PCR Search', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_maintain_actions', 'Maintain Actions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_maintain_reviewers', 'Maintain Reviewers', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_maintain_email', 'Maintain Email Messages', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);

--next field_id=29
-- insert config menus 
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 'menu', NULL, 'configuration', 'Configuration Settings', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2048, 2048, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 'menu', NULL, 'forms', 'Forms Settings', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, 2048, 2048, 0);
commit;

--next field_id=31
-- insert action main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 'search', NULL, 'search_actions', 'Search Actions', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128+64+32+16+8+4+2, 128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 'edit', 'Action_Register_Edit', 'edit_actions', 'Edit Action', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 'edit', 'Action_Register_View', 'view_actions', 'View Action', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
commit;

--next field_id=34
-- insert audits and inspections main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 'search', NULL, 'search_audits', 'Search Audits and Inspections', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 'edit', 'Audit_Edit', 'edit_audits', 'Edit Audits and Inspections', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 'edit', 'Audit_View', 'view_audits', 'View Audits and Inspections', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
commit;

--next field_id=37
-- insert government requirements main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 'search', NULL, 'search_government_requirements', 'Search Government Inspections', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 'edit', 'Government_Requirement_Edit', 'edit_government_requirements', 'Edit Government Inspections', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 'edit', 'Government_Requirement_View', 'view_government_requirements', 'View Government Inspections', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
commit;

--next field_id=39
-- insert incidents main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 'search', NULL, 'search_incidents', 'Search Incidents', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 'edit', 'Incident_Report_Edit', 'edit_incidents', 'Edit Incidents', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 'edit', 'Incident_Report_View', 'view_incidents', 'View Incidents', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=41
-- insert major_hazards, Major Hazard Register
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 'search', NULL, 'search_major_hazards', 'Search Major Hazard Register', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 'edit', 'Major_Hazard_Register_Edit', 'edit_major_hazards', 'Edit Major Hazard Register', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 'edit', 'Major_Hazard_Register_View', 'view_major_hazards', 'View Major Hazard Register', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=43
-- insert meeting_minutes, Meeting Minutes
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 'search', NULL, 'search_meeting_minutes', 'Search Meeting Minutes', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 'edit', 'MeetingMinutes_Edit', 'edit_meeting_minutes', 'Edit Meeting Minutes', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 'edit', 'MeetingMinutes_View', 'view_meeting_minutes', 'View Meeting Minutes', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=46
-- insert other_reports, Other Records
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 'search', NULL, 'search_other_reports', 'Search Other Records', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 'edit', 'OtherRecords_Edit', 'edit_other_reports', 'Edit Other Records', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 'edit', 'OtherRecords_View', 'view_other_reports', 'View Other Records', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=49
-- insert schedules, Schedules
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 'search', NULL, 'search_schedules', 'Search Schedules', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 'edit', 'Schedule_Edit', 'edit_schedules', 'Edit Schedules', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 'edit', 'Schedule_View', 'view_schedules', 'View Schedules', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=52
-- insert site_specific, Site Specific Obligations
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 'search', NULL, 'search_site_specific', 'Search Site Specific Obligations', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 'edit', 'Site_Specific_Obligation_Edit', 'edit_site_specific', 'Edit Site Specific Obligations', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 'edit', 'Site_Specific_Obligation_View', 'view_site_specific', 'View Site Specific Obligations', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=55
-- insert dashboard, Workload Review (this page does not have search/edit/new)
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (14, 'search', NULL, 'search_dashboard', 'Search Workload Review', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
commit;

--next field_id=56
-- insert organisational_structure, Organisation Structure
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 'search', NULL, 'search_organisational_structure', 'Search Organisation Structure', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 'edit', 'Organisational_Structure_Edit', 'edit_organisational_structure', 'Edit Organisation Structure', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 'edit', 'Organisational_Structure_View', 'view_organisational_structure', 'View Organisation Structure', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=59
-- insert reallocate, Re-Allocate responsibility
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (16, 'search', NULL, 'search_reallocate', 'Search Re-Allocate Responsibility', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
commit;

--next field_id=60
-- insert employees, Employees
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 'search', NULL, 'search_employees', 'Search Employees', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 'edit', 'Employee_Register_Edit', 'edit_employees', 'Edit Employee Details', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 'edit', 'Employee_Register_View', 'view_employees', 'View Employee Details', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=63
-- insert incident_checkboxes, Incident Form Checkboxes
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 'search', 'Incident_Checkbox_New', 'search_incident_checkboxes', 'Search Incident Checkboxes', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 'edit', 'Incident_Checkbox_Edit', 'edit_incident_checkboxes', 'Edit Incident Checkboxes', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 'edit', 'Incident_Checkbox_View', 'view_incident_checkboxes', 'View Incident Checkboxes', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=66
-- insert references, Reference Table
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 'search', NULL, 'search_references', 'Search Reference Table', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 'edit', 'Reference_Table_Edit', 'edit_references', 'Edit Reference Table', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 'edit', 'Reference_Table_View', 'view_references', 'View Reference Table', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=69
-- insert risk_definitions, Risk Definitions
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 'search', NULL, 'search_risk_definitions', 'Search Risk Definitions', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 'edit', 'Administration', 'edit_risk_definitions', 'Edit Risk Definitions', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 'edit', 'Administration', 'view_risk_definitions', 'View Risk Definitions', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=72
-- insert work_hours, Work Hours
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 'search', NULL, 'search_work_hours', 'Search Work Hours', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 'edit', 'Work_Hours_Edit', 'edit_work_hours', 'Edit Work Hours', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 'edit', 'Work_Hours_View', 'view_work_hours', 'View Work Hours', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=75
-- insert motd, Message of the Day
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 'search', NULL, 'search_motd', 'Search Message of the Day', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 'edit', 'MOTD_Edit', 'edit_motd', 'Edit Message of the Day', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 'edit', 'MOTD_View', 'view_motd', 'View Message of the Day', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=78
-- insert lost_days, Lost Days
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 'search', NULL, 'search_lost_days', 'Search Lost Days', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 'edit', 'Lost_Days_Edit', 'edit_lost_days', 'Edit Lost Days', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 'edit', 'Lost_Days_View', 'view_lost_days', 'View Lost Days', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=81
-- insert email_configuration, Email Configuration
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (24, 'edit', 'Administration', 'edit_email_configuration', 'Edit Email Configuration', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (24, 'edit', 'Administration', 'view_email_configuration', 'View Email Configuration', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=83
-- insert pcr_search, PCR Search
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 'search', 'PCR_Details_Edit', 'search_pcr_details', 'Search PCR Details', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 'edit', 'PCR_Details_Edit', 'edit_pcr_details', 'Edit PCR Details', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 'edit', 'PCR_Details_New', 'view_pcr_details', 'View PCR Details', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=86
-- insert pcr_maintain_actions, Maintain Actions
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 'search', 'PCR_Admin_Actions_List', 'search_pcr_maintain_actions', 'Search Maintain Actions', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 'edit', 'PCR_Admin_Actions_List', 'edit_pcr_maintain_actions', 'Edit Maintain Actions', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 'edit', 'PCR_Admin_Actions_List', 'view_pcr_maintain_actions', 'View Maintain Actions', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=89
-- insert pcr_maintain_reviewers, Maintain Reviewers
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 'search', 'PCR_Admin_Maintain_Reviewers', 'search_pcr_maintain_reviewers', 'Search Maintain Reviewers', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 'edit', 'PCR_Admin_Maintain_Reviewers', 'edit_pcr_maintain_reviewers', 'Edit Maintain Reviewers', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 'edit', 'PCR_Admin_Maintain_Reviewers', 'view_pcr_maintain_reviewers', 'View Maintain Reviewers', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=92
-- insert pcr_maintain_email, Maintain Email Messages
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'PCR_Admin_Email_Messages', 'search_pcr_maintain_email', 'Search Maintain Email Messages', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'edit', 'PCR_Admin_Email_Messages', 'edit_pcr_maintain_email', 'Edit Maintain Email Messages', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'edit', 'PCR_Admin_Email_Messages', 'view_pcr_maintain_email', 'View Maintain Email Messages', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=95

-- END FORM_FIELDS ################################################################################


-- update field_type_id
update catsdba.form_fields f set field_type_id = (select t.id from catsdba.form_types t where t.name = f.field_type);
-- field_display_params
--update catsdba.form_fields set field_display_params = '''search''' where field_pid in (select field_id from catsdba.form_fields where field_pid=0);
commit;



DROP INDEX catsdba.pk_help_fields_field_id;
DROP INDEX catsdba.pk_help_fields_id;
DROP TABLE catsdba.help_fields;
DROP SEQUENCE catsdba.help_fields_seq;
commit;

CREATE TABLE catsdba.help_fields (
	id NUMBER NOT NULL,
  field_id NUMBER default 0,
	help_tip varchar2(500) default '<h2>Help Page Under Construction</h2>',
	help_user_guide varchar2(2000) default '<h2>User Guide Page Under Construction</h2>'
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_help_fields_id ON catsdba.help_fields
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_help_fields_field_id ON catsdba.help_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.help_fields_seq;
CREATE OR REPLACE TRIGGER catsdba.help_fields_trg
	BEFORE INSERT ON catsdba.help_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.help_fields_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

insert into catsdba.help_fields (select 0 as id, field_id, '<h1>'||field_label||'</h1><h4>Help Page Under Construction</h4>' as help_tip, '<h1>'||field_label||'</h1><h3>Table:'||field_table_name||'</h3><h3>Column:'||field_column_name||'</h3><h4>User Guide Page Under Construction</h4>' as help_user_guide from catsdba.form_fields);

-- pcr details tab
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_details', 'PCR Details', 11, 0, '', 1, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'PCR Details tab', '<h1>PCR Details</h1><h3>Table:</h3><h3>Column:</h3><h4>User Guide Page Under Construction</h4>' );
-- initial review tab
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_initial_review', 'Initial Review', 11, 0, '', 2, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Initial Review tab', '<h1>PCR Initial Review</h1><h3>Table:</h3><h3>Column:</h3><h4>User Guide Page Under Construction</h4>' );
-- evaluation tab
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_evaluation', 'Evaluation', 11, 0, '', 3, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Evaluation tab', '<h1>PCR Evaluation</h1><h3>Table:</h3><h3>Column:</h3><h4>User Guide Page Under Construction</h4>' );
-- area review tab
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_area_review', 'Area Review', 11, 0, '', 4, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Area Review tab', '<h1>PCR Area Review<br /></h1><h3>Table:</h3><h3>Column:</h3><h4>User Guide Page Under Construction</h4>' );
-- PCRT Review
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_pcrt_review', 'PCRT Review', 11, 0, '', 5, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'PCRT Review tab', '<h1>PCR PCRT Review<br /></h1> <h3>Table:</h3> <h3>Column:</h3> <h4>User Guide Page Under Construction</h4>' );
-- Extensions
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_extension', 'Extension', 11, 0, '', 6, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Extensions tab', '<h1>PCR Extensions</h1> <h3>Table:</h3> <h3>Column:</h3> <h4>User Guide Page Under Construction</h4>' );
-- Actions
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_actions', 'Actions', 11, 0, '', 7, 242, 236, 128, 'ORIGINATING_PCR_ACTION', 'PCR Action' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Actions tab', '<h1>PCR Actions</h1> <h3>Table:</h3> <h3>Column:</h3> <h4>User Guide Page Under Construction</h4>' );
-- Sign-off
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_signoff', 'Sign Off', 11, 0, '', 8, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Sign Off tab', '<h1>PCR Sign Off<br /></h1> <h3>Table:</h3> <h3>Column:</h3> <h4>User Guide Page Under Construction</h4>' );
-- Post Audit
INSERT INTO form_fields ( FIELD_PID, FIELD_GROUP_TYPE, FIELD_NAME, FIELD_LABEL, FIELD_TYPE_ID, FIELD_LENGTH, FIELD_VALUE, SORT_ORDER, ROLE_MASK, GROUP_MASK, STATUS, ORIGIN_TABLE, REGISTER_ORIGIN ) VALUES ( 25, 'edit', 'pcr_post_audit', 'Post Audit', 11, 0, '', 9, 242, 236, 128, '', '' );
INSERT INTO HELP_FIELDS ( FIELD_ID, HELP_TIP, HELP_USER_GUIDE ) VALUES ( form_fields_seq.currval, 'Post Audit tab', '<h1>PCR Post Audit<br /></h1> <h3>Table:</h3> <h3>Column:</h3> <h4>User Guide Page Under Construction</h4>' );


commit;



DROP INDEX catsdba.pk_user_group_id;
DROP TABLE catsdba.user_group;
DROP SEQUENCE catsdba.user_group_seq;
commit;

CREATE TABLE catsdba.user_group (
  user_group_id NUMBER NOT NULL,
  user_group_name varchar2(64) default NULL,
  user_group_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_group_id on catsdba.user_group 
(
  user_group_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_group_seq;
CREATE OR REPLACE TRIGGER catsdba.user_group_trg
	BEFORE INSERT ON catsdba.user_group
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_group_seq.nextval INTO :new.user_group_id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Master Administrator', 8388608, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Root Administrators', 1048576, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Super Administrators', 524288, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Incidents Privileged User', 8192, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Actions Administrator', 4096, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Actions Privileged User', 2048, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Administrators', 1024, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Admin Assistant', 512, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('PCR Team Member', 256, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('PCR Super Intendant', 128, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('PCR Engineer', 64, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Incident Analysis', 32, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Editor (Privileged)', 16, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Editor (Restricted)', 8, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Reader (Privileged)', 4, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Reader (Restricted)', 2, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('No Access', 0, 0);

commit;
-- available mask values
--1048576+524288+262144+131072+65536+32786+16384+8192+4096+2048+1024+512+256+128+64+32+16+8+4+2


DROP INDEX catsdba.pk_user_role_id;
DROP TABLE catsdba.user_role;
DROP SEQUENCE catsdba.user_role_seq;
commit;

CREATE TABLE catsdba.user_role (
  user_role_id NUMBER NOT NULL,
  user_role_name varchar2(64) default NULL,
  user_role_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_role_id on catsdba.user_role 
(
  user_role_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_role_seq;
CREATE OR REPLACE TRIGGER catsdba.user_role_trg
	BEFORE INSERT ON catsdba.user_role
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_role_seq.nextval INTO :new.user_role_id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Super Administrator', 2147483648, 2);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Administrator', 128, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Admin Assistant', 64, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Editor', 32, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Trainer', 16, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Viewer', 2, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('No Access', 0, 0);

commit;




DROP INDEX catsdba.pk_user_history_log_id;
DROP TABLE catsdba.user_history_log;
DROP SEQUENCE catsdba.user_history_log_seq;
commit;

CREATE TABLE catsdba.user_history_log (
  id NUMBER NOT NULL,
  log_type varchar2(50) default NULL,
	session_id varchar2(50) default NULL,
  employee_number NUMBER default 0,
  form_id NUMBER default 0,
	form_name varchar2(128) default NULL,
	msg varchar2(255) default NULL,
  ip varchar2(15) default NULL,
  request varchar2(255) default NULL,
  date_time date default sysdate
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_history_log_id on catsdba.user_history_log 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_history_log_seq;
CREATE OR REPLACE TRIGGER catsdba.user_history_log_trg
	BEFORE INSERT ON catsdba.user_history_log
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_history_log_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

DROP INDEX catsdba.pk_error_log_id;
DROP TABLE catsdba.error_log;
DROP SEQUENCE catsdba.error_log_seq;
commit;

CREATE TABLE catsdba.error_log (
  id NUMBER NOT NULL,
	file_name varchar2(500) default NULL,
	line_number NUMBER(4) default NULL,
	error_type varchar2(50) default 'error',
	error_message varchar2(500) default NULL,
	session_id varchar2(50) default NULL,
  employee_number NUMBER default 0,
  ip varchar2(15) default NULL,
	post_info varchar2(2000) default NULL,
  date_time date default sysdate
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_error_log_id on catsdba.error_log 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.error_log_seq;
CREATE OR REPLACE TRIGGER catsdba.error_log_trg
	BEFORE INSERT ON catsdba.error_log
	FOR EACH ROW
	BEGIN
		SELECT catsdba.error_log_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;


DROP INDEX catsdba.pk_status_values_id;
DROP TABLE catsdba.status_values;
DROP SEQUENCE catsdba.status_values_seq;
commit;

CREATE TABLE catsdba.status_values (
  id NUMBER NOT NULL,
  status_title varchar2(50) default NULL,
  status_mask NUMBER default NULL,
  status_type varchar2(50) default NULL,
  status_sub_type varchar2(50) default NULL,
  group_mask NUMBER default 0,
  status SMALLINT default 1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_status_values_id on catsdba.status_values 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.status_values_seq;
CREATE OR REPLACE TRIGGER catsdba.status_values_trg
	BEFORE INSERT ON catsdba.status_values
	FOR EACH ROW
	BEGIN
		SELECT catsdba.status_values_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Active', 1, 'TBLEMPLOYEE_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Inactive', 0, 'TBLEMPLOYEE_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Open', 0, 'TBLACTION_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Closed - Performed', 1, 'TBLACTION_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Closed - Not Performed', 2, 'TBLACTION_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Open', 0, 'TBLINCIDENT_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Closed', 1, 'TBLINCIDENT_DETAILS', NULL, 0, 1);
commit;

