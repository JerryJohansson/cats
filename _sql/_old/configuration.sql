-- CREATE configuration options table
-- Stores configuration details and also user configuration details that override the default system configuration values
-- For this[employee_number] user of group/s of users[user_group_mask]
DROP INDEX catsdba.pk_configuration_id;
DROP TABLE catsdba.configuration;
DROP SEQUENCE catsdba.configuration_seq;
commit;

CREATE TABLE catsdba.configuration (
  configuration_id number NOT NULL,
	employee_number NUMBER default NULL,
	user_group_mask NUMBER default 0,
  configuration_title varchar2(64) default NULL,
  configuration_key varchar2(64) NOT NULL,
  configuration_value varchar2(255) NOT NULL,
  configuration_description varchar2(255) default NULL,
  cfg_group_id number default 5,
  sort_order number default NULL,
  last_modified date default NULL,
  date_added date default SYSDATE,
  use_function varchar2(255) default NULL,
  set_function varchar2(255) default NULL
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_configuration_id ON catsdba.configuration
(
    configuration_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.configuration_seq;
CREATE OR REPLACE TRIGGER catsdba.configuration_trg
	BEFORE INSERT ON catsdba.configuration
	FOR EACH ROW
	BEGIN
		SELECT catsdba.configuration_seq.nextval INTO :new.configuration_id FROM dual;
	END;
/
commit;

-- Insert config data
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Site Name', 'SITE_NAME', 'CATS', 'Set the website name. This value will be displayed globally throughout the site', 1, 1, sysdate, sysdate, NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Site Owner', 'SITE_OWNER', 'CATS Admin', 'Owner of the website or main administrator i.e. webmaster', 1, 2, sysdate, sysdate, NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Version', 'CATS_VERSION', '2', 'Version Release Number of the application', 1, 3, sysdate, sysdate, NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Build', 'CATS_BUILD', '1', 'Release Build Number of the application', 1, 4, sysdate, sysdate, NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Site Owners Email Address', 'SITE_OWNER_EMAIL', 'catsadmin@tronox.com', 'Email address of the site owner', 4, 1, sysdate, sysdate, NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Default From Email Address', 'FROM_EMAIL', 'catsadmin@tronox.com', 'The email address used in the from field when sending emails', 4, 2, sysdate, sysdate, NULL, NULL);
commit;
-- insert user specific configuration details
-- insert override details for VIANET
INSERT INTO catsdba.configuration (employee_number, configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES (90999,'Set default result count per page', 'CATS_DEFAULT_ROW_COUNT', '40', 'Default result count per page. The amount of search results to display per page.', 5, 1, SYSDATE, SYSDATE, NULL, NULL);
INSERT INTO catsdba.configuration (employee_number, configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES (90999,'Set auto login value', 'CATS_REUSE_LOGIN', 'true', 'Sets a flag notifying the application to fill the login field with your login after logout.', 5, 2, SYSDATE, SYSDATE, NULL, NULL);
INSERT INTO catsdba.configuration (employee_number, configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES (90999,'Set default style', 'DEFAULT_STYLE', 'verns', 'Sets the default style folders name i.e. cats = www_root/style/cats/.', 5, 3, SYSDATE, SYSDATE, NULL, NULL);
INSERT INTO catsdba.configuration (employee_number, configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES (90999,'Override setting for maintenance', 'CATS_MAINTENANCE', 'false', 'Sets an flag to override maintenance screen if system is closed.', 5, 4, SYSDATE, SYSDATE, NULL, NULL);
commit;
-- insert override details for K_LILJE
INSERT INTO catsdba.configuration (employee_number, configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES (10562,'Set default result count per page', 'CATS_DEFAULT_ROW_COUNT', '20', 'Default result count per page. The amount of search results to display per page.', 5, 1, SYSDATE, SYSDATE, NULL, NULL);
INSERT INTO catsdba.configuration (employee_number, configuration_title, configuration_key, configuration_value, configuration_description, cfg_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES (10562,'Set auto login value', 'CATS_REUSE_LOGIN', 'true', 'Sets a flag notifying the application to fill the login field with your login after logout.', 5, 2, SYSDATE, SYSDATE, NULL, NULL);
commit;


DROP INDEX catsdba.pk_cfg_group_id;
DROP TABLE catsdba.cfg_group;
DROP SEQUENCE catsdba.cfg_group_seq;
commit;

CREATE TABLE catsdba.cfg_group (
  cfg_group_id number NOT NULL,
  cfg_group_title varchar2(64) NOT NULL,
  cfg_group_description varchar2(255) NOT NULL,
  sort_order number default NULL,
  visible SMALLINT default 1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_cfg_group_id ON catsdba.cfg_group
(
    cfg_group_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.cfg_group_seq;
CREATE OR REPLACE TRIGGER catsdba.cfg_group_trg
	BEFORE INSERT ON catsdba.cfg_group
	FOR EACH ROW
	BEGIN
		SELECT catsdba.cfg_group_seq.nextval INTO :new.cfg_group_id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Site Administration', 'This is where we set all default application configuration settings', 1, 1);
INSERT INTO catsdba.cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Module Options', 'Configuration settings specific to default and plugin modules', 2, 1);
INSERT INTO catsdba.cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Sessions', 'Session options', 3, 1);
INSERT INTO catsdba.cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Email Configuration', 'Email Configuration settings such as Transport Method and Linefeed type', 4, 1);
INSERT INTO catsdba.cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('User Options', 'User Configuration settings such as result count, history count etc', 5, 1);

commit;


DROP INDEX catsdba.pk_filters_id;
DROP TABLE catsdba.filters;
DROP SEQUENCE catsdba.filters_seq;
commit;

CREATE TABLE catsdba.filters (
	id number NOT NULL,
	filter_name varchar2(64) NOT NULL,
	filter_description varchar2(255) NOT NULL,
	filter_object varchar2(4000),
	filter_type varchar2(50) default 'search',
	module varchar2(128) NOT NULL,
	employee_number NUMBER NOT NULL,
	group_mask NUMBER,
  status NUMBER default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_filters_id ON catsdba.filters
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.filters_seq;
CREATE OR REPLACE TRIGGER catsdba.filters_trg
	BEFORE INSERT ON catsdba.filters
	FOR EACH ROW
	BEGIN
		SELECT catsdba.filters_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

DROP INDEX catsdba.pk_templates_id;
DROP TABLE catsdba.templates;
DROP SEQUENCE catsdba.templates_seq;
commit;

CREATE TABLE catsdba.templates (
	id number NOT NULL,
	template_name varchar2(64) NOT NULL,
	template_description varchar2(255) NOT NULL,
	template_object varchar2(4000),
	template_type varchar2(50) default 'search',
	module varchar2(128) NOT NULL,
	employee_number NUMBER NOT NULL,
	group_mask NUMBER,
  status NUMBER default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_templates_id ON catsdba.templates
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.templates_seq;
CREATE OR REPLACE TRIGGER catsdba.templates_trg
	BEFORE INSERT ON catsdba.templates
	FOR EACH ROW
	BEGIN
		SELECT catsdba.templates_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

