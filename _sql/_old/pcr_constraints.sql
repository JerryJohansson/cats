-- add constraints and cascade delete
-- initial review table cascade delete on primary key
ALTER TABLE tblpcr_initialreview
drop CONSTRAINT PKPCR_INITIALREVIEW;
ALTER TABLE TBLPCR_INITIALREVIEW
add CONSTRAINT PKPCR_INITIALREVIEW
  PRIMARY KEY (pcr_id)
  REFERENCES TBLPCR_DETAILS(pcr_id)
  ON DELETE CASCADE;
-- evaluation table cascade delete on primary key
ALTER TABLE tblpcr_evaluation
drop CONSTRAINT PKPCR_EVALUATION;
ALTER TABLE TBLPCR_EVALUATION
add CONSTRAINT PKPCR_EVALUATION
  PRIMARY KEY (pcr_id)
  REFERENCES TBLPCR_DETAILS(pcr_id)
  ON DELETE CASCADE;
