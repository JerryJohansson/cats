-- setup spooling so we can output the results to a file
set term on;
set heading off;
set linesize 2000;
spool view_funcs.txt;

-- FIRST DO FUNCTIONS

-- ACTIONS_OPENED
prompt "ACTIONS_OPENED";
create or replace FUNCTION           "ACTIONS_OPENED" (p_reportid number,p_origin varchar2) return number
AS
cursor inctype is
SELECT
	count(A.Action_Id) Actions_Opened
FROM
	tblAction_Details A
WHERE
	A.Origin_Table = p_origin
	AND A.Report_Id = p_reportid
	AND lower(A.Status) = 'open'
;
b inctype%ROWTYPE;
iValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	iValue := b.Actions_Opened;
end loop;
close inctype;

return iValue;
END;
/
commit;


-- PCR_ACTIONS_CLOSED
prompt "PCR_ACTIONS_CLOSED";
create or replace FUNCTION           "PCR_ACTIONS_CLOSED" (p_pcrid number) return number
AS
cursor inctype is
SELECT
	count(C.action_register_id) PCR_Actions_Closed
FROM
	tblPCR_Actions C,
	tblAction_Details A
WHERE
	C.pcr_id = p_pcrid
	AND C.action_confirmed='Y'
	AND C.action_register_id = A.action_id
	AND A.origin_table = 'ORIGINATING_PCR_ACTION'
	AND A.status not in('Open')
	
;
b inctype%ROWTYPE;
iValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	iValue := b.PCR_Actions_Closed;
end loop;
close inctype;

return iValue;
END;
/
commit;


-- COMMUNITY_CHECKED
prompt "COMMUNITY_CHECKED";
create or replace FUNCTION           "COMMUNITY_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT
	count(I.IncidentId) as CHECKED
FROM
	tblIncident_Checkboxvalues I
WHERE
	I.IncidentId = p_id
	AND I.CheckboxType in ('CommunityClassification')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END;
/
commit;


-- ECONOMIC_CHECKED
prompt "ECONOMIC_CHECKED";
create or replace FUNCTION           "ECONOMIC_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT
	count(I.IncidentId) as CHECKED
FROM
	tblIncident_Checkboxvalues I
WHERE
	I.IncidentId = p_id
	AND I.CheckboxType in ('Economic')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END;
/
commit;


-- ENVIRONMENT_CHECKED
prompt "ENVIRONMENT_CHECKED";
create or replace FUNCTION           "ENVIRONMENT_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT
	count(I.IncidentId) as CHECKED
FROM
	tblIncident_Checkboxvalues I
WHERE
	I.IncidentId = p_id
	AND I.CheckboxType in ('EnviroClassification','EnviroImpactTo','EnviroImpactBy')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END;
/
commit;


-- INCIDENT_CHECKED
prompt "INCIDENT_CHECKED";
create or replace FUNCTION           "INCIDENT_CHECKED" (p_id number,p_type varchar2) return number
AS
cursor inctype is
SELECT
	count(I.IncidentId) as CHECKED
FROM
	tblIncident_Checkboxvalues I
WHERE
	I.IncidentId = p_id
	AND I.CheckboxType = p_type
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END;
/
commit;


-- INJURIES_CHECKED
prompt "INJURIES_CHECKED";
create or replace FUNCTION           "INJURIES_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT
	count(I.IncidentId) as CHECKED
FROM
	tblIncident_Checkboxvalues I
WHERE
	I.IncidentId = p_id
	AND I.CheckboxType in ('InjuryClassification','InjuryLocation','InjuryMechanism','InjuryType','InjuryAgency')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END;
/
commit;


-- PCR_REASONFORCHANGE
prompt "PCR_REASONFORCHANGE";
create or replace FUNCTION           "PCR_REASONFORCHANGE" (p_pcrid number, p_category varchar2) return varchar2
AS
cursor inctype is
SELECT
	distinct C.checkbox_desc
FROM
	tblincidentform_checkboxes C,
	tblpcr_checkboxvalues CV
WHERE
	CV.checkboxid = C.checkbox_id
	and CV.checkboxtype = p_category
	and CV.pcrid = p_pcrid
;

a inctype%ROWTYPE;
mystring varchar2(200) DEFAULT NULL;
BEGIN
open inctype;
loop
	fetch inctype into a;
	exit when inctype%NOTFOUND;
	if (mystring is NULL) then
	mystring := a.checkbox_desc;
	else
	mystring := mystring || ', ' || a.checkbox_desc;
	end if;
end loop;
close inctype;

return mystring;
END;
/
commit;


-- QUALITY_CHECKED
prompt "QUALITY_CHECKED";
create or replace FUNCTION           "QUALITY_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT
	count(I.IncidentId) as CHECKED
FROM
	tblIncident_Checkboxvalues I
WHERE
	I.IncidentId = p_id
	AND I.CheckboxType in ('QualityClassification')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END;
/
commit;


-- SECOND DO VIEWS

-- VIEW_ACTION_DETAILS
prompt "VIEW_ACTION_DETAILS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_ACTION_DETAILS" ("ACTION_ID", "REGISTER_ORIGIN", "ACTION_TYPE", "ACTION_TITLE", "ACTION_DESCRIPTION", "SCHEDULED_DATE", "REMINDER_DATE", "CHANGE_COMMENTS", "MANAGED_BY_ID", "ALLOCATED_TO_ID", "NOTIFY_ALLOCATED_EMAIL", "NOTIFY_ALLOCATED_PRINT", "STATUS", "CLOSING_DATE", "CLOSED_BY", "DATE_DUE_CHANGED_HISTORY", "RECORD_LOCATION", "RECORD_COMMENTS", "ORIGIN_TABLE", "REPORT_ID", "SITE_ID", "DEPARTMENT_ID", "SECTION_ID", "DATE_CREATED", "CRITICAL", "MANAGED_BY", "M_SITE_ID", "ALLOCATED_TO", "A_SITE_ID", "CLOSED_BY_NAME", "C_SITE_ID", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "SECTION_DESCRIPTION", "ACTIONCATEGORY") AS 
  (
SELECT
	a.Action_Id,
	a.Register_Origin,
	a.Action_Type,
	a.Action_Title,
	a.Action_Description,
	a.Scheduled_Date,
	a.Reminder_Date,
	a.Change_Comments,
	a.Managed_By_Id,
	a.Allocated_To_Id,
	a.Notify_Allocated_Email,
	a.Notify_Allocated_Print,
	a.Status,
	a.Closing_Date,
	a.Closed_By,
	a.Date_Due_Changed_History,
	a.Record_Location,
	a.Record_Comments,
	a.Origin_Table,
	a.Report_Id,
	a.Site_Id,
	a.Department_Id,
	a.Section_Id,
	a.date_created,
	a.critical,
	e1.Last_Name || ', ' || e1.First_Name Managed_By,
	e1.Site_ID M_Site_ID,
	e2.Last_Name || ', ' || e2.First_Name Allocated_To,
	e2.Site_ID A_Site_ID,
	e3.Last_Name || ', ' || e3.First_Name Closed_By_Name,
	e3.Site_ID C_Site_ID,
	s.Site_Description,
	d.Department_Description,
	sec.Section_Description,
	F_ActionString(a.Action_Id, 'ActionCategory') ActionCategory
FROM
	tblAction_Details a,
	tblSite s,
	tblDepartment d,
	tblSection sec,
	tblEmployee_Details e1,
	tblEmployee_Details e2,
	tblEmployee_Details e3
WHERE
	a.Site_Id = s.Site_Id(+)
	AND a.Department_Id = d.Department_Id(+)
	AND a.Section_Id = sec.Section_Id(+)
	AND a.Managed_By_Id = e1.Employee_Number(+)
	AND a.Allocated_To_Id = e2.Employee_Number(+)
	AND a.Closed_By = e3.Employee_Number(+)
);
commit;


-- VIEW_AUDITS
prompt "VIEW_AUDITS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_AUDITS" ("RECORD_TYPE", "DATE_INSPECTION", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "LOCATION_DOCS", "REPORT_ID", "REGISTER_ORIGIN", "AUDIT_NOTES", "SITE_ID", "DEPARTMENT_ID", "STRDATE", "ACTIONS_CREATED", "ACTIONS_CLOSED") AS 
  (
SELECT
	a.Record_Type,
	a.Date_Inspection,
	s.Site_Description,
	d.Department_Description,
	a.Location_Docs,
	a.Report_Id,
	a.Register_Origin,
	a.Audit_Notes,
	a.Site_Id,
	a.Department_Id,
	TO_CHAR(a.Date_Inspection) StrDate,
	actions_created(a.Report_Id,'Originating_Audit') actions_created,
	actions_closed(a.Report_Id,'Originating_Audit') actions_closed
FROM
	tblDepartment d,
	tblSite s,
	tblAudits a
WHERE
	s.Site_Id = a.Site_Id
	AND d.Department_Id = a.Department_Id
);
commit;


-- VIEW_EMPLOYEE_DETAILS
prompt "VIEW_EMPLOYEE_DETAILS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_EMPLOYEE_DETAILS" ("EMPLOYEE_NUMBER", "INDUCTION_NUMBER", "FIRST_NAME", "LAST_NAME", "SITE_ID", "DEPARTMENT_ID", "POSITION", "INDUCTION_DATE", "CAN_CHANGE_DUE_DATE", "INCIDENT_PRIVILEGED_USER", "ACTION_PRIVILEGED_USER", "TYPE_OF_EMPLOYEE", "CONTRACTOR_COMPANY", "EMAIL_ADDRESS", "TERMINATION_DATE", "ACCESS_ID", "GROUP_NAME", "GROUP_MASK", "PASSWORD", "USER_STATUS", "VIEWABLE_SITES", "SITE_ACCESS", "EMP_NAME", "COMPANYID", "SECTION_ID", "VERSION", "SUPERINTENDENT", "ENGINEER", "TERMINATION_DATE_STRING", "INDUCTION_DATE_STRING", "COMPANYNAME", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "POSITION_NAME", "SECTION_DESCRIPTION") AS 
  (
SELECT
	e.Employee_Number,
	e.INDUCTION_NUMBER,
	e.First_Name,
	e.Last_Name,
	e.Site_Id,
	e.Department_Id,
	e.Position,
	e.Induction_Date,
	e.Can_Change_Due_Date,
	e.Incident_Privileged_User,
	e.Action_Privileged_User,
	e.Type_Of_Employee,
	e.Contractor_Company,
	e.Email_Address,
	e.Termination_Date,
	e.Access_Id,
	e.Group_Name,
	e.GROUP_MASK,
	e.Password,
	e.User_Status,
	e.Viewable_Sites,
	e.SITE_ACCESS,
	e.Last_Name || ', ' || e.First_Name Emp_Name,
	e.CompanyId,
	e.Section_Id,
	e.Version,
	e.Superintendent,
	e.Engineer,
	To_Char(e.Termination_Date) Termination_Date_String,
	To_Char(e.Induction_Date) Induction_Date_String,
	c.CompanyName,
	s.Site_Description,
	d.Department_Description,
	p.Position_Description Position_Name,
	a.Section_Description
FROM
	tblEmployee_Details e,
	tblSite s,
	tblDepartment d,
	tblPosition p,
	tblCompanies c,
	tblSection a
WHERE
	e.Site_Id = s.Site_Id
	AND e.Department_Id = d.Department_Id
	AND e.Position = p.Position_Id(+)
	AND e.CompanyId = c.CompanyId(+)
	AND e.Section_Id = a.Section_Id(+)
);
commit;


-- VIEW_GOVERNMENT_DETAILS
prompt "VIEW_GOVERNMENT_DETAILS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_GOVERNMENT_DETAILS" ("DATE_COMPLETED", "DATE_STRING", "SITE_DESCRIPTION", "GOVT_DEPARTMENT", "LOCATION", "LOCATION_OF_DOCUMENTS", "COMMENTS", "EMP_NAME", "CONDUCTED_BY_ID", "GOVERNMENT_ID", "REQUIREMENT_TYPE", "REFTABLE_DESCRIPTION", "SHIFT_ID", "TIWEST_CONTACT_ID", "SITE_ID", "SHIFT") AS 
  (
SELECT
	G.Date_Completed,
	TO_CHAR(G.Date_Completed) Date_String,
	S.Site_Description,
	G.Govt_Department,
	G.Location,
	G.Location_Of_Documents,
	G.Comments,
	E.Last_Name || ', ' || E.First_Name Emp_Name,
	G.Conducted_By_Id,
	G.Government_Id,
	G.Requirement_Type,
	R1.RefTable_Description,
	G.Shift_Id,
	G.Tiwest_Contact_Id,
	G.Site_Id,
	R2.RefTable_Description Shift
FROM
	tblRefTable R1,
	tblRefTable R2,
	tblEmployee_Details E,
	tblSite S,
	tblGovernment_Details G
WHERE
	S.Site_Id = G.Site_Id
	AND E.Employee_Number = G.Tiwest_Contact_Id
	AND R1.RefTable_Id = G.Requirement_Type
	AND R2.RefTable_Id = G.Shift_Id
);
commit;


-- VIEW_INCIDENT_DETAILS_STRING
prompt "VIEW_INCIDENT_DETAILS_STRING";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_INCIDENT_DETAILS_STRING" ("INCIDENT_NUMBER", "INCIDENT_DATE", "INCIDENT_TYPE_VALUE", "INCIDENT_TYPE_STRING", "INCIDENT_DESCRIPTION", "INCIDENT_TYPE", "ENV_INCIDENT_DETAILS", "BODY_PART", "MECHANISM_OF_INJURY", "INJURY_STATUS_VALUE", "INJURED_PERSON_ID", "INJURED_PERSON_COMPANY", "INJURY_STATUS", "EMP_NAME", "TYPE_OF_EMPLOYEE", "INJURED_SITE_ID", "COMPANY", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "SUPER_SITE_ID", "INIT_SITE_ID", "DEPARTMENT_ID") AS 
  (
SELECT
	DISTINCT I.Incident_Number,
	To_Char(I.Incident_Date,'DD/MM/YYYY'),
	I.Incident_Type,
	A.Incident_Type_String,
	I.Incident_Description,
	I.Incident_Type,
	I.Env_Incident_Details,
	I.Body_Part,
	I.Mechanism_of_Injury,
	I.Injury_Status,
	I.Injured_Person_Id,
	I.Injured_Person_Company,
	AI.Injury_Status_Value,
	E3.Emp_Name,
	E3.Type_Of_Employee,
	E3.Site_ID Injured_Site_ID,
	E3.Contractor_Company Company,
	E3.Site_Description,
	E3.Department_Description,
	E2.Site_ID Super_Site_ID,
	E.Site_ID Init_Site_ID,
	E3.Department_Id
FROM
	tblIncident_Details I,
	tblIncident_Type T,
	All_Incident_Types A,
	View_Employee_Details E,
	View_Employee_Details E2,
	View_Employee_Details E3,
	All_Injury_Status AI
WHERE
	I.Incident_Type=A.Incident_Type
	AND (BITAND(I.Incident_Type,T.Incident_Type)+0) > 0
	AND E2.Employee_Number = I.Superintendent_Id
	AND E.Employee_Number = I.Initiated_By_Id
	AND I.Injured_Person_Id = E3.Employee_Number(+)
	AND I.Injury_Status=AI.Injury_Status(+)
);
commit;


-- VIEW_INCIDENT_PRINTVIEW
prompt "VIEW_INCIDENT_PRINTVIEW";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_INCIDENT_PRINTVIEW" ("INCDISPLYID", "PRIMARYPERSONID", "PRIMARYCOMPANYID", "REPORTEDCOMPANYID", "INJUREDCOMPANYID", "INCIDENTSTATUS", "AREA", "BODY_PART", "CAUSE_OF_INJURY", "COMPANY", "CURRENT_LOCATION", "DEPARTMENT_ID", "ESTIMATED_COST", "IMMEDIATE_CORRECTIVE_ACTIONS", "INCIDENT_DESCRIPTION", "INCIDENT_NUMBER", "INCIDENT_REPORT_STATUS_ID", "INCIDENT_TIME", "INCIDENT_TITLE", "INCIDENT_TYPE", "INITIALRISKCATEGORY", "INITIALRISKLEVEL", "INITIATED_BY_ID", "INJURED_PERSON_COMPANY", "INJURED_PERSON_GENDER", "INJURED_PERSON_ID", "INJURY_STATUS", "INVESTIGATION_COMPLETION_DATE", "INVESTIGATION_REQUIRED", "LOCATION", "MECHANISM_OF_INJURY", "PERSONS_INVOLVED_DESCRIPTION", "RECALCRISKCATEGORY", "RECALCRISKLEVEL", "RECALC_RISK_RESULT", "REPORTED_TO", "INCIDENT_DATE", "REPORT_DATE", "REPORT_TIME", "SEVERITY_RATING", "SITE_ID", "SUBSTANTIATED", "SUPERINTENDENT_ID", "INCIDENT_CATEGORIES", "INCIDENT_REPORTTYPE", "INCIDENT_IMPACTBY", "INCIDENT_IMPACTTO", "INCIDENT_INJURYTYPE", "INCIDENT_INJURYMECHANISM", "INCIDENT_INJURYAGENCY", "INCIDENT_INJURYLOCATION", "INCIDENT_INJURYCLASS", "INCIDENT_ENVIROCLASS", "INCIDENT_COMMUNITYCLASS", "INCIDENT_QUALITYCLASS", "INCIDENT_REPORTEDTO", "INCIDENT_SYSTEMICCAUSE", "INCIDENT_BEHAVIOURCAUSE", "INCIDENT_ECONOMIC", "DEPARTMENT", "SITE", "INJURED_PERSON", "SUPERINTENDENT", "INITIATED_BY", "TYPE_OF_EMPLOYEE", "REPORTEDTO", "PRIMARYPERSON", "RECALC_RISKCAT", "RECALC_RISKLEVEL", "INIT_RISKCAT", "INIT_RISKLEVEL", "REPORTEDBYCOMPANY", "INJUREDCOMPANY", "PRIMARYCOMPANY", "CREATEDACTIONS", "CLOSEDACTIONS") AS 
  (
SELECT
	I.INCDISPLYID,
	I.PRIMARYPERSONID,
	I.PRIMARYCOMPANYID,
	I.REPORTEDCOMPANYID,
	I.INJUREDCOMPANYID,
	I.INCIDENTSTATUS,
	I.AREA,
	I.BODY_PART,
	I.CAUSE_OF_INJURY,
	I.COMPANY,
	I.CURRENT_LOCATION,
	I.DEPARTMENT_ID,
	I.ESTIMATED_COST,
	I.IMMEDIATE_CORRECTIVE_ACTIONS,
	I.INCIDENT_DESCRIPTION,
	I.INCIDENT_NUMBER,
	I.INCIDENT_REPORT_STATUS_ID,
	to_char(I.INCIDENT_DATE,'HH24:MI') INCIDENT_TIME,
	I.INCIDENT_TITLE,
	I.INCIDENT_TYPE,
	I.INITIALRISKCATEGORY,
	I.INITIALRISKLEVEL,
	I.INITIATED_BY_ID,
	I.INJURED_PERSON_COMPANY,
	I.INJURED_PERSON_GENDER,
	I.INJURED_PERSON_ID,
	I.INJURY_STATUS,
	I.INVESTIGATION_COMPLETION_DATE,
	I.INVESTIGATION_REQUIRED,
	I.LOCATION,
	I.MECHANISM_OF_INJURY,
	I.PERSONS_INVOLVED_DESCRIPTION,
	I.RECALCRISKCATEGORY,
	I.RECALCRISKLEVEL,
	I.RECALC_RISK_RESULT,
	I.REPORTED_TO,
	To_Char (I.INCIDENT_DATE,'DD/MM/YYYY') INCIDENT_DATE,
	To_Char (I.REPORT_DATE,'DD/MM/YYYY') REPORT_DATE,
	to_char(I.REPORT_DATE,'HH24:MI') REPORT_TIME,
	I.SEVERITY_RATING,
	I.SITE_ID,
	I.SUBSTANTIATED,
	I.SUPERINTENDENT_ID,
	getstring(I.INCIDENT_NUMBER,'IncidentCategory') incident_categories,
	getstring(I.INCIDENT_NUMBER,'IncidentReportType') incident_reporttype,
	getstring(I.INCIDENT_NUMBER,'EnviroImpactBy') incident_impactby,
	getstring(I.INCIDENT_NUMBER,'EnviroImpactTo') incident_impactto,
	getstring(I.INCIDENT_NUMBER,'InjuryType') incident_injurytype,
	getstring(I.INCIDENT_NUMBER,'InjuryMechanism') incident_injurymechanism,
	getstring(I.INCIDENT_NUMBER,'InjuryAgency') incident_injuryagency,
	getstring(I.INCIDENT_NUMBER,'InjuryLocation') incident_injurylocation,
	getstring(I.INCIDENT_NUMBER,'InjuryClassification') incident_injuryclass,
	getstring(I.INCIDENT_NUMBER,'EnviroClassification') incident_enviroclass,
	getstring(I.INCIDENT_NUMBER,'CommunityClassification') incident_communityclass,
	getstring(I.INCIDENT_NUMBER,'QualityClassification') incident_qualityclass,
	getstring(I.INCIDENT_NUMBER,'ReportedTo') incident_reportedto,
	getstring(I.INCIDENT_NUMBER,'SystemicCause') incident_SystemicCause,
	getstring(I.INCIDENT_NUMBER,'BehaviourCause') incident_BehaviourCause,
	getstring(I.INCIDENT_NUMBER,'Economic') incident_Economic,
	D.DEPARTMENT_DESCRIPTION DEPARTMENT,
	S.SITE_DESCRIPTION SITE,
	E1.LAST_NAME || ', ' || E1.FIRST_NAME INJURED_PERSON,
	E2.LAST_NAME || ', ' || E2.FIRST_NAME SUPERINTENDENT,
	E3.LAST_NAME || ', ' || E3.FIRST_NAME INITIATED_BY,
	E3.TYPE_OF_EMPLOYEE,
	E4.LAST_NAME || ', ' || E4.FIRST_NAME REPORTEDTO,
	E5.LAST_NAME || ', ' || E5.FIRST_NAME PRIMARYPERSON,
	R1.REFTABLE_DESCRIPTION RECALC_RISKCAT,
	R2.REFTABLE_DESCRIPTION RECALC_RISKLEVEL,
	R3.REFTABLE_DESCRIPTION INIT_RISKCAT,
	R4.REFTABLE_DESCRIPTION INIT_RISKLEVEL,
	C1.COMPANYNAME REPORTEDBYCOMPANY,
	C2.COMPANYNAME INJUREDCOMPANY,
	C3.COMPANYNAME PRIMARYCOMPANY,
	ACTIONS_CREATED(I.INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS,
	ACTIONS_CLOSED(I.INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS
FROM
	TBLINCIDENT_DETAILS I,
	TBLSITE S,
	TBLDEPARTMENT D,
	TBLEMPLOYEE_DETAILS E1,
	TBLEMPLOYEE_DETAILS E2,
	TBLEMPLOYEE_DETAILS E3,
	TBLEMPLOYEE_DETAILS E4,
	TBLEMPLOYEE_DETAILS E5,
	TBLREFTABLE R1,
	TBLREFTABLE R2,
	TBLREFTABLE R3,
	TBLREFTABLE R4,
	TBLCOMPANIES C1,
	TBLCOMPANIES C2,
	TBLCOMPANIES C3
WHERE
	I.INJURED_PERSON_ID = E1.EMPLOYEE_NUMBER(+)
	AND I.SUPERINTENDENT_ID = E2.EMPLOYEE_NUMBER
	AND I.INITIATED_BY_ID = E3.EMPLOYEE_NUMBER
	AND I.REPORTED_TO = E4.EMPLOYEE_NUMBER
	AND I.PRIMARYPERSONID = E5.EMPLOYEE_NUMBER
	AND I.SITE_ID = S.SITE_ID
	AND D.DEPARTMENT_ID = I.DEPARTMENT_ID
	AND I.RECALCRISKCATEGORY = R1.REFTABLE_ID(+)
	AND I.RECALCRISKLEVEL = R2.REFTABLE_ID(+)
	AND R3.REFTABLE_ID(+) = I.INITIALRISKCATEGORY
	AND R4.REFTABLE_ID(+) = I.INITIALRISKLEVEL
	AND C1.COMPANYID(+) = I.REPORTEDCOMPANYID
	AND C2.COMPANYID(+) = I.INJUREDCOMPANYID
	AND C3.COMPANYID(+) = I.PRIMARYCOMPANYID
);
commit;


-- VIEW_ORGANISATIONAL_STRUCTURE
prompt "VIEW_ORGANISATIONAL_STRUCTURE";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_ORGANISATIONAL_STRUCTURE" ("SITE_ID", "SITE_DESCRIPTION", "SITE_CODE", "MIMS_DISTRICT", "SITE_FOLDER", "DEPARTMENT_ID", "DEPARTMENT_DESCRIPTION", "SECTION_ID", "SECTION_DESCRIPTION", "EMAIL_FROM_ADDRESS") AS 
  (
SELECT
	s.Site_Id,
	s.Site_Description,
	s.site_code,
	s.mims_district,
	s.site_folder,
	d.Department_Id,
	d.Department_Description,
	a.Section_Id,
	a.Section_Description,
	s.email_from_address
FROM
	tblSite s,
	tblDepartment d,
	tblSection a
WHERE
	s.Site_Id = d.Site_Id
	and d.Department_Id = a.Department_Id(+)
	)
ORDER BY
	s.Site_Description ASC;
commit;	


-- VIEW_PCR_REVIEWER_SUBSET
prompt "VIEW_PCR_REVIEWER_SUBSET";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_REVIEWER_SUBSET" ("REVIEWER_DETAIL_ID", "SITE_ID", "REVIEW_ID", "REVIEWER_TYPE", "CHECKLIST_LOC", "FOLDER_LOC", "REQUIREMENT", "PASSWORD_FLAG", "PCR_ID", "DEF_REVIEWER_ID", "AREA_ID", "REVIEW_TYPE") AS
  ( 
SELECT 
 	rd.detail_id reviewer_detail_id,
	rd.site_id,
	rd.review_id,
	rd.reviewer_type,
	rd.checklist_loc,
	rd.folder_loc,
	rd.requirement,
	rd.password_flag,
	pd.PCR_Id,
	rn.reviewer_id def_reviewer_id,
	rn.area_id,
	ref.reftable_description review_type
FROM
	tblPCR_Reviewer_Detail rd,
	tblPCR_Reviewer_Name rn,
	tblPCR_details pd,
	tblRefTable ref
WHERE
	pd.area_id = rn.area_id
	AND rd.detail_id = rn.detail_id
	AND rd.review_id = ref.reftable_id
);
commit;


-- VIEW_REFTABLE_RISK_LEVEL
prompt "VIEW_REFTABLE_RISK_LEVEL";
CREATE OR REPLACE VIEW catsdba.view_reftable_risk_level
(
    reftable_id
  , reftable_description
)
AS
(
SELECT
	tblRefTable.REFTABLE_ID,
	tblRefTable.REFTABLE_DESCRIPTION
FROM
	tblRefTable
WHERE
	tblRefTable.REFTABLE_TYPE = 'Risk Level'
);
commit;

-- VIEW_REFTABLE_RISK_CATEGORY
prompt "VIEW_REFTABLE_RISK_CATEGORY";
CREATE OR REPLACE VIEW catsdba.view_reftable_risk_category
(
    reftable_id
  , reftable_description
)
AS
(
SELECT
	tblRefTable.REFTABLE_ID,
	tblRefTable.REFTABLE_DESCRIPTION
FROM
	tblRefTable
WHERE
	tblRefTable.REFTABLE_TYPE = 'Risk Category'
);

commit;


-- VIEW_INCIDENT_SEARCHVIEW
prompt "VIEW_INCIDENT_SEARCHVIEW";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_INCIDENT_SEARCHVIEW" ("COMPANY", "REALDATE", "PRIMARYPERSONID", "PRIMARYPERSON_NAME", "PRIMARYCOMPANYID", "PRIMARYCOMPANY_NAME", "REPORTEDCOMPANYID", "REPORTEDCOMPANY_NAME", "INJUREDCOMPANYID", "INJUREDCOMPANY_NAME", "INCDISPLYID", "INCIDENTSTATUS", "DEPARTMENT_ID", "DEPARTMENT_DESCRIPTION", "ESTIMATED_COST", "INCIDENT_TITLE", "INCIDENT_NUMBER", "INCIDENT_REPORT_STATUS_ID", "INITIALRISKCATEGORY", "INITIALRISKCATEGORY_DESC", "INITIALRISKLEVEL", "INITIALRISKLEVEL_DESC", "INITIATED_BY_ID", "INITIATED_BY_NAME", "TYPE_OF_EMPLOYEE", "INJURED_PERSON_COMPANY", "INJURED_PERSON_ID", "INJURED_PERSON_NAME", "INJURY_STATUS", "RECALCRISKCATEGORY", "RECALCRISKCATEGORY_DESC", "RECALCRISKLEVEL", "RECALCRISKLEVEL_DESC", "RECALC_RISK_RESULT", "REPORTED_TO", "REPORTED_TO_NAME", "REPORT_TIME", "SEVERITY_RATING", "SITE_ID", "SITE_DESCRIPTION", "SUBSTANTIATED", "SUPERINTENDENT_ID", "SUPERINTENDENT_NAME", "INCIDENT_DATE", "REPORT_DATE", "INCIDENT_CATEGORIES", "CREATEDACTIONS", "CLOSEDACTIONS","PERSONS_INVOLVED_DESCRIPTION") AS 
  (
SELECT
	I.COMPANY,
	I.INCIDENT_DATE RealDate,
	I.PRIMARYPERSONID,
	E1.Last_Name || ', ' || E1.First_Name PRIMARYPERSON_NAME,
	I.PRIMARYCOMPANYID,
	C1.COMPANYNAME PRIMARYCOMPANY_NAME,
	I.REPORTEDCOMPANYID,
	C2.COMPANYNAME REPORTEDCOMPANY_NAME,
	I.INJUREDCOMPANYID,
	C3.COMPANYNAME INJUREDCOMPANY_NAME,
	I.INCDISPLYID,
	I.INCIDENTSTATUS,
	I.DEPARTMENT_ID,
	D.DEPARTMENT_DESCRIPTION,
	I.ESTIMATED_COST,
	I.INCIDENT_TITLE,
	I.INCIDENT_NUMBER,
	I.INCIDENT_REPORT_STATUS_ID,
	I.INITIALRISKCATEGORY,
	R1.REFTABLE_DESCRIPTION INITIALRISKCATEGORY_DESC,
	I.INITIALRISKLEVEL,
	R2.REFTABLE_DESCRIPTION INITIALRISKLEVEL_DESC,
	I.INITIATED_BY_ID,
	E2.Last_Name || ', ' || E2.First_Name INITIATED_BY_NAME,
	E2.TYPE_OF_EMPLOYEE,
	I.INJURED_PERSON_COMPANY,
	I.INJURED_PERSON_ID,
	E3.Last_Name || ', ' || E3.First_Name INJURED_PERSON_NAME,
	I.INJURY_STATUS,
	I.RECALCRISKCATEGORY,
	R3.REFTABLE_DESCRIPTION RECALCRISKCATEGORY_DESC,
	I.RECALCRISKLEVEL,
	R4.REFTABLE_DESCRIPTION RECALCRISKLEVEL_DESC,
	I.RECALC_RISK_RESULT,
	I.REPORTED_TO,
	E4.Last_Name || ', ' || E4.First_Name REPORTED_TO_NAME,
	to_char(I.REPORT_DATE,'HH24:MI') REPORT_TIME,
	I.SEVERITY_RATING,
	I.SITE_ID,
	S.SITE_DESCRIPTION,
	I.SUBSTANTIATED,
	I.SUPERINTENDENT_ID,
	E5.Last_Name || ', ' || E5.First_Name SUPERINTENDENT_NAME,
	To_Char (I.INCIDENT_DATE,'DD/MM/YYYY') INCIDENT_DATE,
	To_Char (I.REPORT_DATE,'DD/MM/YYYY') REPORT_DATE,
	getstring(I.INCIDENT_NUMBER,'IncidentCategory') incident_categories,
	ACTIONS_CREATED(I.INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS,
	ACTIONS_CLOSED(I.INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS,
	I.PERSONS_INVOLVED_DESCRIPTION
FROM
	TBLINCIDENT_DETAILS I,
	TBLDEPARTMENT D,
	TBLEMPLOYEE_DETAILS E1,
	TBLEMPLOYEE_DETAILS E2,
	TBLEMPLOYEE_DETAILS E3,
	TBLEMPLOYEE_DETAILS E4,
	TBLEMPLOYEE_DETAILS E5,
	TBLCOMPANIES C1,
	TBLCOMPANIES C2,
	TBLCOMPANIES C3,
	VIEW_REFTABLE_RISK_CATEGORY R1,
	VIEW_REFTABLE_RISK_LEVEL R2,
	VIEW_REFTABLE_RISK_CATEGORY R3,
	VIEW_REFTABLE_RISK_LEVEL R4,
	TBLSITE S
WHERE
	I.DEPARTMENT_ID = D.DEPARTMENT_ID
	AND I.PRIMARYPERSONID = E1.EMPLOYEE_NUMBER(+)
	AND I.INITIATED_BY_ID = E2.EMPLOYEE_NUMBER(+)
	AND I.INJURED_PERSON_ID = E3.EMPLOYEE_NUMBER(+)
	AND I.REPORTED_TO = E4.EMPLOYEE_NUMBER(+)
	AND I.SUPERINTENDENT_ID = E5.EMPLOYEE_NUMBER(+)
	AND I.PRIMARYCOMPANYID = C1.COMPANYID(+)
	AND I.REPORTEDCOMPANYID = C2.COMPANYID(+)
	AND I.INJUREDCOMPANYID = C3.COMPANYID(+)
	AND I.INITIALRISKCATEGORY = R1.REFTABLE_ID(+)
	AND I.INITIALRISKLEVEL = R2.REFTABLE_ID(+)
	AND I.RECALCRISKCATEGORY = R3.REFTABLE_ID(+)
	AND I.RECALCRISKLEVEL = R4.REFTABLE_ID(+)
	AND I.SITE_ID = S.SITE_ID
);
commit;


-- VIEW_LOST_DAYS
prompt "VIEW_LOST_DAYS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_LOST_DAYS" ("LOST_DAYS_ID", "YEARS", "MONTHS", "YEAR_MONTH", "LOST_DAYS", "RESTRICTED_DAYS", "ROSTERED_DAYS", "MONTH_NAME", "INCIDENT_NUMBER", "LOCATION", "AREA", "INCIDENT_DATE", "INJURED_PERSON_ID", "INCDISPLYID", "SITE_ID", "EMPLOYEE_NUMBER", "EMP_NAME", "SITE_DESCRIPTION") AS 
  (
SELECT
	L.Lost_Days_Id,
	L.Years,
	L.Months,
	L.Year_Month,
	L.Lost_Days,
	L.Restricted_Days,
	L.ROSTERED_DAYS,
	initcap(to_char(to_date('2000-' || cast(L.Months as varchar2(2)) || '-1','YYYY-MM-DD'),'MONTH')) Month_Name,
	I.Incident_Number,
	I.Location,
	I.Area,
	I.Incident_Date,
	I.Injured_Person_Id,
	I.IncDisplyId,
	E.Site_Id,
	E.Employee_Number,
	E.Emp_Name,
	E.Site_Description
FROM
	tblIncident_Details I,
	tblLost_Days L,
	VIEW_EMPLOYEE_DETAILS E
WHERE
	L.Incident_Number = I.Incident_Number
	AND E.Employee_Number = I.Injured_Person_Id
);
commit;


-- VIEW_OTHER_REPORTS
prompt "VIEW_OTHER_REPORTS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_OTHER_REPORTS" ("RECORD_TYPE", "DATE_COMPLETED", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "REPORTED_BY", "REPORTED_BY_ID", "LOCATION_DOCS", "REPORT_ID", "REGISTER_ORIGIN", "REPORT_NOTES", "COMMENTS", "DEPARTMENT_ID", "STRDATE", "SITE_ID") AS 
  (
SELECT
	o.Record_Type,
	o.Date_Completed,
	s.Site_Description,
	d.Department_Description,
	e.last_name||', '||e.first_name Reported_by,
	o.Reported_by_id,
	o.Location_Docs,
	o.Report_Id,
	o.Register_Origin,
	o.Report_Notes,
	o.Comments,
	o.Department_Id,
	TO_CHAR(o.Date_Completed) StrDate,
	s.Site_ID
FROM
	tblDepartment d,
	tblSite s,
	tblOther_Reports o,
	tblEmployee_Details e
WHERE
	s.Site_Id = o.Site_Id AND
	d.Department_Id = o.Department_Id AND
	e.employee_number = o.reported_by_id
);
commit;


-- VIEW_PCR_ACTIONS
prompt "VIEW_PCR_ACTIONS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_ACTIONS" ("PCR_ID", "SITE_ID", "AREA_ID", "SUPERVISOR_ID", "CRITICALITY") AS 
  (
SELECT
	p.PCR_Id,
	p.site_id,
	p.area_id,
	e.SUPERVISOR_ID,
	r1.RefTable_Description AS Criticality
FROM
	tblPCR_Details p,
	tblPCR_Evaluation e,
	tblRefTable r1
WHERE
  p.PCR_ID = e.PCR_ID
	and r1.RefTable_Id(+) = p.Criticality_Id
);
commit;


-- VIEW_PCR_ACTIONS_DETAILS
prompt "VIEW_PCR_ACTIONS_DETAILS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_ACTIONS_DETAILS" ("PCR_ID", "DETAIL_ID", "SITE_ID", "STAGE_ID", "ACTION_DESCRIPTION", "ACTION_REGISTER_ID", "MANAGED_BY_ID", "MANAGED_BY", "ALLOCATED_TO_ID", "ALLOCATED_TO", "STATUS", "DUE_DATE", "DAYS_AFTER_PREV_STAGE", "ACTION_CONFIRMED", "RAISE_NOW", "MANAGER_NOTIFIED", "SUPER_ENG_NOTIFIED", "STAGE1_NOTIFIED", "STAGE2_NOTIFIED", "STAGE3_NOTIFIED", "SITE") AS 
  (
SELECT
	A.PCR_ID,
	A.DETAIL_ID,
	A.SITE_ID,
	A.STAGE_ID,
	A.ACTION_DESCRIPTION,
	A.ACTION_REGISTER_ID,
	A.MANAGED_BY_ID,
	E1.LAST_NAME||', '||E1.FIRST_NAME AS MANAGED_BY,
	A.ALLOCATED_TO_ID,
	E2.LAST_NAME||', '||E2.FIRST_NAME AS ALLOCATED_TO,
	A.STATUS,
	A.DUE_DATE,
	A.DAYS_AFTER_PREV_STAGE,
	A.ACTION_CONFIRMED,
	A.RAISE_NOW,
	A.MANAGER_NOTIFIED,
	A.SUPER_ENG_NOTIFIED,
	A.STAGE1_NOTIFIED,
	A.STAGE2_NOTIFIED,
	A.STAGE3_NOTIFIED,
	S.SITE_DESCRIPTION AS SITE
FROM
	tblPCR_Actions A,
	tblEmployee_details E1,
	tblEmployee_details E2,
	tblsite S
WHERE
	A.managed_by_id = E1.employee_number(+)
	and A.allocated_to_id = E2.employee_number(+)
	and A.site_id = S.site_id
);
commit;


-- VIEW_PCR_AREA
prompt "VIEW_PCR_AREA";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_AREA" ("AREA_ID", "SITE_ID", "SITE_DESCRIPTION", "AREA_DESCRIPTION") AS
  (
SELECT
   a.area_id,
   s.site_id,
   s.site_description,
   a.area_description
FROM
   tblpcr_area a,
   tblsite s
WHERE
   a.site_id = s.site_id
);
commit;


-- VIEW_PCR_AREAREVIEW
prompt "VIEW_PCR_AREAREVIEW";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_AREAREVIEW" ("PCR_ID", "PCR_TYPE", "MAKE_PERMANENT", "DATE_MAKE_PERMANENT", "EXTEND_PERMANENT", "DATE_EXTEND_PERMANENT", "ACTION", "COMMENTS", "DUE_BY", "DUE_DATE", "SUPERVISOR_ID", "AFE_NUMBER", "AUTHORITYID_1", "AUTHORITYID_2", "AUTHORITYSTATUS_1", "AUTHORITYSTATUS_2", "AUTHORITYDATE_1", "AUTHORITYDATE_2", "AUTHORITYCOMMENT_1", "AUTHORITYCOMMENT_2", "SUPERVISOR_NAME", "AUTHORITIYNAME_1", "AUTHORITIYNAME_2", "SITE_ID", "AREA_ID", "ASSESSMENT_SCORE", "ASSESSMENT_LEVEL", "CONFIRM_LEVEL", "REVISED_SCORE", "CRITICALITY") AS 
  (
SELECT
	p.PCR_Id,
	p.PCR_Type,
	p.Make_Permanent,
	p.Date_Make_Permanent,
	p.Extend_Permanent,
	p.Date_Extend_Permanent,
	a.ACTION,
	a.COMMENTS,
	a.DUE_BY,
	a.DUE_DATE,
	a.SUPERVISOR_ID,
	a.AFE_NUMBER,
	a.AuthorityId_1,
	a.AuthorityId_2,
	a.AuthorityStatus_1,
	a.AuthorityStatus_2,
	a.AuthorityDate_1,
	a.AuthorityDate_2,
	a.authoritycomment_1,
	a.authoritycomment_2,
	e1.Last_Name || ', ' || e1.First_Name AS SUPERVISOR_NAME,
	a1.Last_Name || ', ' || a1.First_Name AS AuthoritiyName_1,
	a2.Last_Name || ', ' || a2.First_Name AS AuthoritiyName_2,
	p.site_id,
	p.area_id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.CONFIRM_LEVEL,
	e.REVISED_SCORE,
	r1.RefTable_Description AS Criticality
FROM
	tblPCR_AreaReview a,
	tblPCR_Details p,
  tblEmployee_Details e1,
  tblEmployee_Details a1,
  tblEmployee_Details a2,
	tblPCR_Evaluation e,
	tblRefTable r1
WHERE
  e1.Employee_Number(+) = a.SUPERVISOR_ID
  and a1.Employee_Number(+) = a.AuthorityId_1
  and a2.Employee_Number(+) = a.AuthorityId_2
  and p.PCR_ID = a.PCR_ID
	and e.PCR_ID = a.PCR_ID
	and r1.RefTable_Id(+) = p.Criticality_Id
);
commit;


-- VIEW_PCR_DEFAULT_ACTIONS
prompt "VIEW_PCR_DEFAULT_ACTIONS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_DEFAULT_ACTIONS" ("ACTION_ID", "SITE_ID", "SITE_DESCRIPTION", "MANAGED_BY_ID", "MANAGED_BY", "ACTION_DESCRIPTION", "DAYS_AFTER_PREV_STAGE") AS 
  (
SELECT
	a.Action_id,
	a.Site_Id,
	s.site_description,
	a.managed_by_id,
	(e.last_name || ', ' || e.first_name) as managed_by,
	a.action_description,
	a.days_after_prev_stage
FROM
	tblPCR_Action a,
	tblEmployee_details e,
	tblsite s
WHERE
	a.managed_by_id = e.employee_number
	and a.site_id = s.site_id
);
commit;


-- VIEW_PCR_DEFAULTNAMES
prompt "VIEW_PCR_DEFAULTNAMES";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_DEFAULTNAMES" ("DETAIL_ID", "AREA_ID", "REVIEWER_ID", "AREA_DESCRIPTION", "EMP_NAME", "SITE_ID") AS 
  (
SELECT
	r.detail_id,
	r.area_id,
	r.reviewer_id,
	a.area_description,
	e.last_name || ', ' || e.first_name emp_name,
	a.site_id
FROM
	tblPCR_Reviewer_Name r,
	tblPCR_Area a,
	tblEmployee_Details e
WHERE
	r.area_id = a.area_id
	and r.reviewer_id = e.employee_number
);
commit;


-- VIEW_PCR_DETAILS
prompt "VIEW_PCR_DETAILS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_DETAILS" ("PCR_ID", "PCR_DISPLAYID", "CRITICALITY_ID", "PCR_TYPE", "CHANGETYPE_ID", "ORIGINATOR_ID", "EMPLOYEE_NO", "SITE_ID", "DEPARTMENT_ID", "SECTION_ID", "POSITION_ID", "CREW_ID", "PCR_DATE", "OPERATION_ID", "TITLE", "DESCRIPTION", "AREA_ID", "AREA_DESCRIPTION", "EQUIPMENT_ID", "AUTHORITYID_1", "AUTHORITYID_2", "AUTHORITYSTATUS_1", "AUTHORITYSTATUS_2", "AUTHORITYDATE_1", "AUTHORITYDATE_2", "AUTHORITYCOMMENT_1", "AUTHORITYCOMMENT_2", "WO_NUMBER", "EVALUATION_ID", "INITIALREVIEW_REQUIRED", "MEETING_DATE", "PROCESS_STEP", "PROCESSSTEP_STATUS", "OVERALL_STATUS", "STATUS", "UNIT", "REASSIGNEDTO_ID", "COMMENTS", "ORIGINATORNAME", "AUTHORITIYNAME_1", "AUTHORITIYNAME_2", "EVALUATIONNAME", "REASSIGNEDTO", "IMPACTLEVEL", "TYPEOFCHANGE", "OPERATION", "CRITICALITY", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "POSITION_DESCRIPTION", "SECTION_DESCRIPTION", "TCP", "ACTIONS_OPEN", "ACTIONS_CLOSED", "REASON_FOR_CHANGE", "MAKE_PERMANENT", "DATE_MAKE_PERMANENT", "EXTEND_PERMANENT", "DATE_EXTEND_PERMANENT", "DUE_DATE") AS 
  (
SELECT
	p.PCR_Id,
	p.PCR_DisplayId,
	p.Criticality_Id,
	p.PCR_Type,
	p.ChangeType_Id,
	p.Originator_Id,
	p.Employee_No,
	p.Site_Id,
	p.Department_Id,
	p.Section_Id,
	p.Position_Id,
	p.Crew_Id,
	p.PCR_Date,
	p.Operation_Id,
	p.Title,
	p.Description,
	p.Area_Id,
	ar.area_description,
	p.Equipment_Id,
	p.AuthorityId_1,
	p.AuthorityId_2,
	p.AuthorityStatus_1,
	p.AuthorityStatus_2,
	p.AuthorityDate_1,
	p.AuthorityDate_2,
	p.authoritycomment_1,
	p.authoritycomment_2,
	p.WO_Number,
	p.Evaluation_Id,
	p.InitialReview_Required,
	p.Meeting_Date,
	p.Process_Step,
	p.ProcessStep_Status,
	p.Overall_Status,
	p.Overall_Status||'-'||p.Process_Step||'-'||p.ProcessStep_Status Status,
	p.Unit,
	p.ReAssignedTo_Id,
	p.Comments,
	e1.Last_Name || ', ' || e1.First_Name AS OriginatorName,
	e2.Last_Name || ', ' || e2.First_Name AS AuthoritiyName_1,
	e3.Last_Name || ', ' || e3.First_Name AS AuthoritiyName_2,
	e4.Last_Name || ', ' || e4.First_Name AS EvaluationName,
	e5.Last_Name || ', ' || e5.First_Name AS ReassignedTo,
	r1.RefTable_Description AS ImpactLevel,
	r2.RefTable_Description AS TypeOfChange,
	r3.RefTable_Description AS Operation,
	r4.RefTable_Comment AS Criticality,
	s.Site_Description,
	d.Department_Description,
	o.Position_Description,
	s2.Section_Description,
	p.tcp,
	ACTIONS_OPENED(p.pcr_id, 'ORIGINATING_PCR_ACTION') ACTIONS_OPEN,
	ACTIONS_CLOSED(p.pcr_id, 'ORIGINATING_PCR_ACTION') ACTIONS_CLOSED,
	PCR_REASONFORCHANGE(p.pcr_id, 'Reason_For_Request') Reason_For_Change,
  p.MAKE_PERMANENT,
  p.DATE_MAKE_PERMANENT,
  p.EXTEND_PERMANENT,
  p.DATE_EXTEND_PERMANENT,
	so.due_date
FROM
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblEmployee_Details e4,
  tblEmployee_Details e5,
  tblRefTable r1,
  tblRefTable r2,
  tblRefTable r3,
	tblRefTable r4,
  tblSite s,
  tblDepartment d,
  tblPosition o,
  tblSection s2,
  tblPCR_Details p,
	tblPCR_Area ar,
	tblPCR_Signoff so
WHERE
  e1.Employee_Number = p.Originator_Id
  and e2.Employee_Number(+) = p.AuthorityId_1
  and e3.Employee_Number(+) = p.AuthorityId_2
  and e4.Employee_Number(+) = p.Evaluation_Id
  and e5.Employee_Number(+) = p.ReAssignedTo_Id
  and r1.RefTable_Id(+) = p.Criticality_Id
  and r2.RefTable_Id(+) = p.ChangeType_Id
  and r3.RefTable_Id(+) = p.Operation_Id
	and r4.RefTable_Id(+) = p.Criticality_Id
  and s.Site_Id = p.Site_Id
  and d.Department_Id = p.Department_Id
  and s2.Section_Id(+) = p.Section_Id
  and o.Position_Id(+) = p.Position_Id
	and ar.Area_Id(+) = p.Area_Id
	and so.PCR_ID = p.PCR_ID
);
commit;


-- ORGINATING_PCR_ACTION
prompt "ORGINATING_PCR_ACTION";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."ORIGINATING_PCR_ACTION" ("TYPE", "REPORT_ID", "DISPLAY_ID", "SITE", "DEPARTMENT", "REPORT_DATE", "EMPLOYEE", "DETAILS") AS 
  (
SELECT
	'PCR Action' Type,
	p.PCR_ID Report_Id,
	p.PCR_DISPLAYID Display_Id,
	p.SITE_DESCRIPTION Site,
	p.DEPARTMENT_DESCRIPTION Department,
	p.PCR_DATE Report_Date,
	p.ORIGINATORNAME Employee,
	p.TITLE Details
FROM
	View_PCR_Details p
);
commit;


-- VIEW_PCR_EMAIL
prompt "VIEW_PCR_EMAIL";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_EMAIL" ("EMAIL_ID", "SITE_ID", "SITE_DESCRIPTION", "SCREEN_ID", "SCREEN_NAME", "PERSON_DESCRIPTION", "SUBJECT", "MESSAGE") AS 
  (
SELECT
	e.email_id,
	e.site_id,
	s.site_description,
	e.screen_id,
	t.screen_name,
	e.person_description,
	e.subject,
	e.message
FROM
	tblpcr_email e,
	tblsite s,
	tblpcr_tabs t
WHERE
	s.site_id = e.site_id
	and t.screen_id = e.screen_id
);
commit;


-- VIEW_PCR_EVALUATION
prompt "VIEW_PCR_EVALUATION";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_EVALUATION" ("PCR_ID", "EVALUATION_ID", "ASSESSMENT_SCORE", "ASSESSMENT_LEVEL", "EVALUATION_NAME_ID", "EVALUATION_DATE", "EVALUATION_COMMENTS", "AREA_REVIEW_REQ", "TEAM_REVIEW_REQ", "EXT_REVIEW_REQ", "AUTHORITYID_1", "AUTHORITYID_2", "AUTHORITYSTATUS_1", "AUTHORITYSTATUS_2", "AUTHORITYDATE_1", "AUTHORITYDATE_2", "AUTHORITYCOMMENT_1", "AUTHORITYCOMMENT_2", "EVALUATION_NAME", "AUTHORITIYNAME_1", "AUTHORITIYNAME_2", "PCR_EVALUATION_NAME", "ASSESSMENT_SCORE_VALUE", "SITE_ID") AS 
  (
SELECT
	p.PCR_Id,
	p.Evaluation_Id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.EVALUATION_NAME_ID,
	e.EVALUATION_DATE,
	e.EVALUATION_COMMENTS,
	e.AREA_REVIEW_REQ,
	e.TEAM_REVIEW_REQ,
	e.EXT_REVIEW_REQ,
	e.AuthorityId_1,
	e.AuthorityId_2,
	e.AuthorityStatus_1,
	e.AuthorityStatus_2,
	e.AuthorityDate_1,
	e.AuthorityDate_2,
	e.authoritycomment_1,
	e.authoritycomment_2,
	e1.Last_Name || ', ' || e1.First_Name AS EVALUATION_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS AuthoritiyName_1,
	e3.Last_Name || ', ' || e3.First_Name AS AuthoritiyName_2,
	e4.Last_Name || ', ' || e4.First_Name AS PCR_EVALUATION_NAME,
	r1.RefTable_Description AS ASSESSMENT_SCORE_VALUE,
	p.site_id
FROM
	tblPCR_Evaluation e,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblEmployee_Details e4,
  tblRefTable r1,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = e.EVALUATION_NAME_ID
  and e2.Employee_Number(+) = e.AuthorityId_1
  and e3.Employee_Number(+) = e.AuthorityId_2
  and e4.Employee_Number(+) = p.Evaluation_Id
  and r1.RefTable_Id(+) = e.ASSESSMENT_SCORE
  and p.PCR_ID = e.PCR_ID
);
commit;

-- VIEW_PCR_EXTENSION
prompt "VIEW_PCR_EXTENSION";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_EXTENSION" ("PCR_ID", "PCR_TYPE", "MAKE_PERMANENT", "DATE_MAKE_PERMANENT", "EXTEND_PERMANENT", "DATE_EXTEND_PERMANENT", "TYPE_OF_REVIEW", "MEETING_DATE", "MEETING_COMMENTS", "ACTION", "COMMENTS", "DUE_BY", "DUE_DATE", "AFE_NUMBER", "SITE_ID", "AREA_ID", "ASSESSMENT_SCORE", "ASSESSMENT_LEVEL", "CONFIRM_LEVEL", "REVISED_SCORE", "CRITICALITY") AS 
  (
SELECT
	p.PCR_Id,
	p.PCR_Type,
	p.Make_Permanent,
	p.Date_Make_Permanent,
	p.Extend_Permanent,
	p.Date_Extend_Permanent,
	a.TYPE_OF_REVIEW,
	a.MEETING_DATE,
	a.MEETING_COMMENTS,
	a.ACTION,
	a.COMMENTS,
	a.DUE_BY,
	a.DUE_DATE,
	a.AFE_NUMBER,
	p.site_id,
	p.area_id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.CONFIRM_LEVEL,
	e.REVISED_SCORE,
	r1.RefTable_Description AS Criticality
FROM
	tblPCR_Extension a,
	tblPCR_Details p,
	tblPCR_Evaluation e,
	tblRefTable r1
WHERE
  p.PCR_ID = a.PCR_ID
	and e.PCR_ID = a.PCR_ID
	and r1.RefTable_Id(+) = p.Criticality_Id
);
commit;


-- VIEW_PCR_INITIALREVIEW
prompt "VIEW_PCR_INITIALREVIEW";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_INITIALREVIEW" ("PCR_ID", "AUTHORITYID_1", "AUTHORITYID_2", "AUTHORITYSTATUS_1", "AUTHORITYSTATUS_2", "AUTHORITYDATE_1", "AUTHORITYDATE_2", "AUTHORITYCOMMENT_1", "AUTHORITYCOMMENT_2", "AUTHORITIYNAME_1", "AUTHORITIYNAME_2", "SITE_ID", "AREA_ID", "CRITICALITY") AS 
  (
SELECT
	p.PCR_Id,
	a.AuthorityId_1,
	a.AuthorityId_2,
	a.AuthorityStatus_1,
	a.AuthorityStatus_2,
	a.AuthorityDate_1,
	a.AuthorityDate_2,
	a.authoritycomment_1,
	a.authoritycomment_2,
	a1.Last_Name || ', ' || a1.First_Name AS AuthoritiyName_1,
	a2.Last_Name || ', ' || a2.First_Name AS AuthoritiyName_2,
	p.site_id,
	p.area_id,
	r1.RefTable_Description AS Criticality
FROM
	tblPCR_InitialReview a,
	tblPCR_Details p,
  tblEmployee_Details a1,
  tblEmployee_Details a2,
	tblRefTable r1
WHERE
  a1.Employee_Number(+) = a.AuthorityId_1
  and a2.Employee_Number(+) = a.AuthorityId_2
  and p.PCR_ID = a.PCR_ID
	and r1.RefTable_Id(+) = p.Criticality_Id
);
commit;


-- VIEW_PCR_MEETINGS
prompt "VIEW_PCR_MEETINGS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_MEETINGS" ("MEETING_ID",  "SITE_ID", "SITE_DESCRIPTION", "OTHER_INVITEES", "ADDITIONAL_COMMENTS", "MINUTES", "STATUS", "MEETING_DATE", "STRDATE") AS 
  (
SELECT
	tblPCR_Meetings.Meeting_Id,
	tblPCR_Meetings.Site_Id,
	tblSite.Site_Description,
	tblPCR_Meetings.Other_Invitees,
	tblPCR_Meetings.Additional_Comments,
	tblPCR_Meetings.Minutes,
	tblPCR_Meetings.Status,
	tblPCR_Meetings.Meeting_Date,
	TO_CHAR(Meeting_Date) StrDate
FROM
	tblPCR_Meetings,
	tblSite
WHERE
	tblPCR_Meetings.Site_Id = tblSite.Site_Id
);
commit;


-- VIEW_PCR_MEETINGS_PCR_DETAILS
prompt "VIEW_PCR_MEETINGS_PCR_DETAILS";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_MEETINGS_PCR_DETAILS" ("MEETING_ID",  "PCR_ID", "PCR_DISPLAYID") AS 
  (
SELECT
	M.Meeting_Id,
	P.PCR_ID,
	P.PCR_DisplayID
FROM
	tblPCR_Meetings_PCR_Details M,
	tblPCR_Details P
WHERE
	M.PCR = P.PCR_ID
);
commit;


-- VIEW_PCR_PCRTREVIEW
prompt "VIEW_PCR_PCRTREVIEW";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_PCRTREVIEW" ("PCR_ID", "PCR_TYPE", "MAKE_PERMANENT", "DATE_MAKE_PERMANENT", "EXTEND_PERMANENT", "DATE_EXTEND_PERMANENT", "TYPE_OF_REVIEW", "MEETING_DATE", "MEETING_COMMENTS", "ACTION", "COMMENTS", "DUE_BY", "DUE_DATE", "AFE_NUMBER", "SITE_ID", "AREA_ID", "ASSESSMENT_SCORE", "ASSESSMENT_LEVEL", "CONFIRM_LEVEL", "REVISED_SCORE", "CRITICALITY") AS 
  (
SELECT
	p.PCR_Id,
	p.PCR_Type,
	p.Make_Permanent,
	p.Date_Make_Permanent,
	p.Extend_Permanent,
	p.Date_Extend_Permanent,
	a.TYPE_OF_REVIEW,
	a.MEETING_DATE,
	a.MEETING_COMMENTS,
	a.ACTION,
	a.COMMENTS,
	a.DUE_BY,
	a.DUE_DATE,
	a.AFE_NUMBER,
	p.site_id,
	p.area_id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.CONFIRM_LEVEL,
	e.REVISED_SCORE,
	r1.RefTable_Description AS Criticality
FROM
	tblPCR_PCRTReview a,
	tblPCR_Details p,
	tblPCR_Evaluation e,
	tblRefTable r1
WHERE
  p.PCR_ID = a.PCR_ID
	and e.PCR_ID = a.PCR_ID
	and r1.RefTable_Id(+) = p.Criticality_Id
);
commit;


-- VIEW_PCR_POSTAUDIT
prompt "VIEW_PCR_POSTAUDIT";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_POSTAUDIT" ("PCR_ID", "AUDIT_COMMENTS", "AUDIT_NAME_ID", "AUDIT_DATE", "AUDIT_COMPLETED_BY_ID", "AUDIT_NAME", "AUDIT_COMPLETED_BY", "SITE_ID") AS 
  (
SELECT
	p.PCR_Id,
	a.AUDIT_COMMENTS,
	a.AUDIT_NAME_ID,
	a.AUDIT_DATE,
	a.AUDIT_COMPLETED_BY_ID,
	e1.Last_Name || ', ' || e1.First_Name AS AUDIT_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS AUDIT_COMPLETED_BY,
	p.site_id
FROM
	tblPCR_PostAudit a,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = a.AUDIT_NAME_ID
	and e2.Employee_Number(+) = a.AUDIT_COMPLETED_BY_ID
  and p.PCR_ID = a.PCR_ID
);
commit;


-- VIEW_PCR_REVIEWER_DETAIL
prompt "VIEW_PCR_REVIEWER_DETAIL";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_REVIEWER_DETAIL" ("DETAIL_ID", "SITE_ID", "SITE_DESCRIPTION", "REVIEW_ID", "REVIEW_TYPE", "REVIEWER_TYPE", "CHECKLIST_LOC", "FOLDER_LOC", "REQUIREMENT", "PASSWORD_FLAG") AS 
  (
SELECT
	r.detail_id,
	r.site_id,
	s.site_description,
	r.review_id,
	ref.reftable_description review_type,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag
FROM
	tblpcr_reviewer_detail r,
	tblsite s,
	tblreftable ref
WHERE
	s.site_id = r.site_id
	and ref.reftable_id = r.review_id
);
commit;


-- VIEW_PCR_SEARCH
prompt "VIEW_PCR_SEARCH";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_SEARCH" ("PCR_ID", "PCR_DISPLAYID", "CRITICALITY_ID", "PCR_TYPE", "CHANGETYPE_ID", "ORIGINATOR_ID", "SITE_ID", "DEPARTMENT_ID", "SECTION_ID", "PCR_DATE", "TITLE", "DESCRIPTION", "AREA_ID", "AREA_DESCRIPTION", "EQUIPMENT_ID", "OVERALL_STATUS", "ORIGINATORNAME", "IMPACTLEVEL", "TYPEOFCHANGE", "CRITICALITY", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "SECTION_DESCRIPTION", "TCP", "ACTIONS_OPEN", "ACTIONS_CLOSED", "DUE_DATE") AS 
  (
SELECT
	p.PCR_Id,
	p.PCR_DisplayId,
	p.Criticality_Id,
	p.PCR_Type,
	p.ChangeType_Id,
	p.Originator_Id,
	p.Site_Id,
	p.Department_Id,
	p.Section_Id,
	p.PCR_Date,
	p.Title,
	p.Description,
	p.Area_Id,
	ar.area_description,
	p.Equipment_Id,
	p.Overall_Status,
	e1.Last_Name || ', ' || e1.First_Name AS OriginatorName,
	r1.RefTable_Description AS ImpactLevel,
	r2.RefTable_Description AS TypeOfChange,
	r4.RefTable_Comment AS Criticality,
	s.Site_Description,
	d.Department_Description,
	s2.Section_Description,
	p.tcp,
	ACTIONS_OPENED(p.pcr_id, 'ORIGINATING_PCR_ACTION') ACTIONS_OPEN,
	PCR_ACTIONS_CLOSED(p.pcr_id) ACTIONS_CLOSED,
	so.due_date
FROM
  tblEmployee_Details e1,
  tblRefTable r1,
  tblRefTable r2,
  tblRefTable r4,
  tblSite s,
  tblDepartment d,
  tblSection s2,
  tblPCR_Details p,
	tblPCR_Area ar,
	tblPCR_Signoff so
WHERE
  e1.Employee_Number = p.Originator_Id
  and r1.RefTable_Id(+) = p.Criticality_Id
  and r2.RefTable_Id(+) = p.ChangeType_Id
  and r4.RefTable_Id(+) = p.Criticality_Id
  and s.Site_Id = p.Site_Id
  and d.Department_Id = p.Department_Id
  and s2.Section_Id(+) = p.Section_Id
  and ar.Area_Id(+) = p.Area_Id
	and so.PCR_ID = p.PCR_ID
);
commit;


-- VIEW_PCR_SIGNOFF
prompt "VIEW_PCR_SIGNOFF";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_PCR_SIGNOFF" ("PCR_ID", "TYPE_OF_REVIEW", "MEETING_DATE", "MEETING_COMMENTS", "DUE_DATE", "AUDITBY_ID", "SIGN_OFF", "SIGNED_OFF_DATE", "ENGINEER_ID", "ENGINEER_APPROVE", "ENGINEER_DATE", "SUPER_ID", "SUPER_APPROVE", "SUPER_DATE", "AUTHORITYID_1", "AUTHORITYID_2", "AUTHORITYSTATUS_1", "AUTHORITYSTATUS_2", "AUTHORITYDATE_1", "AUTHORITYDATE_2", "AUTHORITYCOMMENT_1", "AUTHORITYCOMMENT_2", "AUTHORITIYNAME_1", "AUTHORITIYNAME_2", "ENGINEER_NAME", "SUPER_NAME", "AUDITBY_NAME", "SITE_ID", "AREA_ID") AS 
  (
SELECT
	p.PCR_Id,
	s.TYPE_OF_REVIEW,
	s.MEETING_DATE,
	s.MEETING_COMMENTS,
	s.DUE_DATE,
	s.AUDITBY_ID,
	s.SIGN_OFF,
	s.SIGNED_OFF_DATE,
	s.ENGINEER_ID,
	s.ENGINEER_APPROVE,
	s.ENGINEER_DATE,
	s.SUPER_ID,
	s.SUPER_APPROVE,
	s.SUPER_DATE,
	s.AuthorityId_1,
	s.AuthorityId_2,
	s.AuthorityStatus_1,
	s.AuthorityStatus_2,
	s.AuthorityDate_1,
	s.AuthorityDate_2,
	s.authoritycomment_1,
	s.authoritycomment_2,
	a1.Last_Name || ', ' || a1.First_Name AS AuthoritiyName_1,
	a2.Last_Name || ', ' || a2.First_Name AS AuthoritiyName_2,
	e1.Last_Name || ', ' || e1.First_Name AS ENGINEER_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS SUPER_NAME,
	e3.Last_Name || ', ' || e3.First_Name AS AUDITBY_NAME,
	p.site_id,
	p.area_id
FROM
	tblPCR_Signoff s,
  tblEmployee_Details a1,
  tblEmployee_Details a2,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = s.ENGINEER_ID
	and e2.Employee_Number(+) = s.SUPER_ID
  and a1.Employee_Number(+) = s.AuthorityId_1
  and a2.Employee_Number(+) = s.AuthorityId_2
  and e3.Employee_Number(+) = s.AUDITBY_ID
  and p.PCR_ID = s.PCR_ID
);
commit;

prompt "I am here 5";

-- VIEW_SCHEDULE
prompt "VIEW_SCHEDULE";
CREATE OR REPLACE FORCE VIEW "CATSDBA"."VIEW_SCHEDULE" ("SCHEDULER_ID", "REGISTER_ORIGIN", "ACTION_TITLE", "SCHEDULER_DESCRIPTION", "COMPLETION_DATE", "ACTION_RAISED_DATE", "FREQUENCY", "NO_OF_DAYS_NOTICE", "MANAGED_BY_ID", "REPORT_ID", "DEPARTMENT_ID", "SECTION_ID", "STATUS", "COMPLETION_DATE_STRING", "MANAGED_BY", "POSITION", "POSITIONID", "SITE_ID", "SITE_DESCRIPTION", "DEPARTMENT_DESCRIPTION", "SECTION_DESCRIPTION", "ACTIONS_CREATED", "ACTIONS_CLOSED") AS 
  (
SELECT
	sc.Scheduler_Id,
	sc.Register_Origin,
	sc.Action_Title,
	sc.Scheduler_Description,
	sc.Completion_Date,
	sc.Action_Raised_Date,
	sc.Frequency,
	sc.No_Of_Days_Notice,
	sc.Managed_By_Id,
	sc.Report_Id,
	sc.department_Id,
	sc.Section_Id,
	sc.Status,
	TO_CHAR(sc.Completion_Date) Completion_Date_String,
	e.Emp_Name Managed_By,
	e.Position_Name Position,
	e.Position PositionID,
	s.Site_Id,
	s.Site_Description,
	d.Department_Description,
	sec.Section_Description,
	actions_created(sc.scheduler_id,'Originating_Schedule') actions_created,
	actions_closed(sc.scheduler_id,'Originating_Schedule') actions_closed
FROM
	tblScheduler sc,
	tblSite s,
	tblDepartment d,
	tblSection sec,
	VIEW_EMPLOYEE_DETAILS e
WHERE
	sc.Managed_By_Id = e.Employee_Number
	AND sc.Site_Id = s.Site_Id
	AND sc.Department_Id = d.Department_Id
	AND sc.Section_Id = sec.Section_Id(+)
);
commit;


-- turn spooling off
spool off;