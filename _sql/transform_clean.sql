-- EMPLOYEE STUFF HERE
--
-- Remove the temp table for employee access mapping
-- This table may be deleted after the import.php script is run
-- But lets leave it until we are all happy with the testing results
DROP INDEX catsdba.pk_ug_id;
DROP TABLE catsdba.ug;
DROP SEQUENCE catsdba.ug_seq;
commit;


-- Remove viewable_sites from tblemployee_details table
-- as this column has been replaced by site_access
DECLARE col_name VARCHAR2(30);
select column_name into col_name from user_tab_columns where table_name = 'TBLEMPLOYEE_DETAILS' and column_name = 'VIEWABLE_SITES';
if col_name = 'VIEWABLE_SITES' then
	alter table tblemployee_details drop column viewable_sites;
end if;
commit;

-- update incident_details by removing incident_time and report_time columns
alter table tblincident_details drop column incident_time;
alter table tblincident_details drop column report_time;
commit;

-- remove reported_by column from tblother_reports
alter table TBLOTHER_REPORTS drop column reported_by;
commit;