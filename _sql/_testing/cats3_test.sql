# DBTools DBMYSQL - MySQL Database Dump
#

CREATE DATABASE `cats3_test`;
USE `cats3_test`;

# Dumping Table Structure for _form_element

#
CREATE TABLE `_form_element` (
  `form_element_id` int(11) NOT NULL auto_increment,
  `form_element_name` varchar(100) default NULL,
  `item_name` varchar(100) default NULL,
  `sort_order` int(11) default NULL,
  `status` int(11) default '0',
  PRIMARY KEY  (`form_element_id`)
) TYPE=MyISAM;
#
# Dumping Data for _form_element
#
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (1, 'origin', 'Action', 1, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (2, 'origin', 'Schedule', 2, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (3, 'origin', 'Hazard', 3, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (4, 'origin', 'Lost Days', 4, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (5, 'origin', 'Obligation', 5, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (6, 'origin', 'Audit', 6, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (7, 'origin', 'Inspection', 7, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (8, 'origin_type', 'Record', 1, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (9, 'origin_type', 'Meeting', 2, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (10, 'origin_type', 'Audit or Inspection', 3, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (11, 'origin_type', 'Government Inspection', 4, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (12, 'origin_type', 'Incident Severity Rating', 5, 0);
INSERT INTO `_form_element` (form_element_id, form_element_name, item_name, sort_order, status) VALUES (13, 'origin_type', 'Hazard Register Process', 6, 0);
# Dumping Table Structure for _form_fields

#
CREATE TABLE `_form_fields` (
  `id` int(11) NOT NULL auto_increment,
  `pid` int(11) default NULL,
  `page_type` varchar(128) default NULL,
  `field_name` varchar(128) default NULL,
  `field_table_name` varchar(128) default NULL,
  `field_table_fname` varchar(128) default NULL,
  `field_title` varchar(128) default NULL,
  `field_type` varchar(50) default NULL,
  `field_length` int(11) default NULL,
  `field_value` text,
  `field_help_tip` varchar(255) default NULL,
  `field_columns` smallint(6) default NULL,
  `field_mandatory` smallint(6) default NULL,
  `field_options` text,
  `user_group_mask` int(11) default '0',
  `sort_order` int(11) default '1',
  `system_status_id` int(11) default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;
#
# Dumping Data for _form_fields
#
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (1, 0, 'menu', 'cats', NULL, NULL, 'CATS', 'm', 0, '', NULL, 1, 0, '', 8190, 1, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (2, 0, 'menu', 'admin', NULL, NULL, 'ADMIN', 'm', 0, '', NULL, 1, 0, '', 8176, 2, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (3, 0, 'menu', 'pcr', NULL, NULL, 'PCR', 'm', 0, '', NULL, 1, 0, '', 8190, 3, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (4, 1, 'item', 'actions', NULL, NULL, 'Actions', 'm', 0, '', NULL, 1, 0, '', 8190, 1, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (5, 1, 'item', 'audits_and_inspections', NULL, NULL, 'Audits & Inspections', 'm', 0, '', NULL, 1, 0, '', 8190, 2, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (6, 1, 'item', 'government_inspections', NULL, NULL, 'Government Inspections', 'm', 0, '', NULL, 1, 0, '', 8190, 3, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (7, 1, 'item', 'incidents', NULL, NULL, 'Incidents', 'm', 0, '', NULL, 1, 0, '', 8190, 4, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (8, 1, 'item', 'major_hazard_register', NULL, NULL, 'Major Hazard Register', 'm', 0, '', NULL, 1, 0, '', 8190, 5, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (9, 1, 'item', 'meeting_minutes', NULL, NULL, 'Meeting Minutes', 'm', 0, '', NULL, 1, 0, '', 8190, 6, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (10, 1, 'item', 'other_records', NULL, NULL, 'Other Records', 'm', 0, '', NULL, 1, 0, '', 8190, 7, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (11, 1, 'item', 'schedules', NULL, NULL, 'Schedules', 'm', 0, '', NULL, 1, 0, '', 8190, 8, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (12, 1, 'item', 'site_specific_obligations', NULL, NULL, 'Site Specific Obligations', 'm', 0, '', NULL, 1, 0, '', 8190, 9, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (13, 1, 'item', 'workload_review', NULL, NULL, 'Workload Review', 'm', 0, '', NULL, 1, 0, '', 8190, 10, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (14, 2, 'item', 'organisation_structure', NULL, NULL, 'Organisation Structure', 'm', 0, '', NULL, 1, 0, '', 8176, 1, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (15, 2, 'item', 'reallocate_responsibility', NULL, NULL, 'Re-Allocate responsibility', 'm', 0, '', NULL, 1, 0, '', 8176, 2, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (16, 2, 'item', 'employees', NULL, NULL, 'Employees', 'm', 0, '', NULL, 1, 0, '', 8176, 3, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (17, 2, 'item', 'incident_form_checkboxes', NULL, NULL, 'Incident Form Checkboxes', 'm', 0, '', NULL, 1, 0, '', 8176, 4, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (18, 2, 'item', 'reference_table', NULL, NULL, 'Reference Table', 'm', 0, '', NULL, 1, 0, '', 8176, 5, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (19, 2, 'item', 'risk_definitions', NULL, NULL, 'Risk Definitions', 'm', 0, '', NULL, 1, 0, '', 8176, 6, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (20, 2, 'item', 'work_hours', NULL, NULL, 'Work Hours', 'm', 0, '', NULL, 1, 0, '', 8176, 7, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (21, 2, 'item', 'motd', NULL, NULL, 'Message of the Day', 'm', 0, '', NULL, 1, 0, '', 8176, 8, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (22, 2, 'item', 'lost_days', NULL, NULL, 'Lost Days', 'm', 0, '', NULL, 1, 0, '', 8176, 9, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (23, 2, 'item', 'email_configuration', NULL, NULL, 'Email Configuration', 'm', 0, '', NULL, 1, 0, '', 8176, 10, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (24, 3, 'item', 'pcr_search', NULL, NULL, 'PCR Search', 'm', 0, '', NULL, 1, 0, '', 8190, 1, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (25, 3, 'item', 'maintain_actions', NULL, NULL, 'Maintain Actions', 'm', 0, '', NULL, 1, 0, '', 8176, 2, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (26, 3, 'item', 'maintain_reviewers', NULL, NULL, 'Maintain Reviewers', 'm', 0, '', NULL, 1, 0, '', 8176, 3, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (27, 3, 'item', 'email_messages', NULL, NULL, 'Email Messages', 'm', 0, '', NULL, 1, 0, '', 8176, 4, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (28, 4, 'search', 'site_id', NULL, NULL, 'Site', 's', 64, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'site_id\\\')\'}}', 8190, 1, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (29, 4, 'search', 'department_id', NULL, NULL, 'Department', 's', 64, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'department_id\\\')\'}}', 8190, 2, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (30, 4, 'search', 'section_id', NULL, NULL, 'Section', 's', 64, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'section_id\\\')\'}}', 8190, 3, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (31, 4, 'search', 'action_managed_by', NULL, NULL, 'Action Managed By', 'n', 16, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_helper(\\\'employee_id\\\')\'}}', 8190, 4, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (32, 4, 'search', 'allocated_to', NULL, NULL, 'Allocated To', 'n', 16, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_helper(\\\'employee_id\\\')\'}}', 8190, 5, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (33, 4, 'search', 'managed_by_or_allocated_to', NULL, NULL, 'Managed By Or Allocated To', 'n', 16, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_helper(\\\'employee_id\\\')\'}}', 8190, 6, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (34, 4, 'search', 'action_type', NULL, NULL, 'Action Type', 's', 50, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'action_type_id\\\')\'}}', 8190, 7, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (35, 4, 'search', 'origin', NULL, NULL, 'Origin', 's', 50, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'origin\\\')\'}}', 8190, 8, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (36, 4, 'search', 'action_category_id', NULL, NULL, 'Category', 's', 50, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'action_category_id\\\')\'}}', 8190, 9, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (37, 4, 'search', 'system_status_id', NULL, NULL, 'Status', 's', 50, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_status_dropdown(\\\'actions\\\')\'}}', 8190, 10, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (38, 4, 'search', 'action_number', NULL, NULL, 'Action Number', 'n', 16, '', NULL, 1, 0, '', 8190, 11, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (39, 4, 'search', 'due_date_from', NULL, NULL, 'Due Date From', 'd', 10, '', NULL, 1, 0, '', 8190, 12, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (40, 4, 'search', 'due_date_to', NULL, NULL, 'Due Date To', 'd', 10, '', NULL, 1, 0, '', 8190, 13, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (41, 4, 'search', 'closed_date_from', NULL, NULL, 'Closed Date From', 'd', 10, '', NULL, 1, 0, '', 8190, 14, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (42, 4, 'search', 'closed_date_to', NULL, NULL, 'Closed Date To', 'd', 10, '', NULL, 1, 0, '', 8190, 15, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (43, 4, 'edit', 'origin', NULL, NULL, 'Origin', 's', 50, 'Action', NULL, 1, 0, '{\'functions\':{\'field\':\'get_helper(\\\'origin\\\')\'}}', 8190, 1, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (44, 4, 'edit', 'action_category_id', NULL, NULL, 'Action Category', 'g', 6, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_checkboxes(\\\'action_category_id\\\')\'}}', 8190, 2, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (45, 4, 'edit', 'action_title', NULL, NULL, 'Action Title', 's', 128, '', NULL, 1, 0, '', 8190, 3, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (46, 4, 'edit', 'description', NULL, NULL, 'Description', 's', 500, '', NULL, 1, 0, '', 8190, 4, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (47, 4, 'edit', 'scheduled_date', NULL, NULL, 'Scheduled Completion Date', 'd', 10, '', NULL, 1, 0, '', 8190, 5, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (48, 4, 'edit', 'reminder_date', NULL, NULL, 'Date Next Reminder to be Sent to Manager', 'd', 10, '', NULL, 1, 0, '', 8190, 6, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (49, 4, 'edit', 'managed_by', NULL, NULL, 'Action Managed By', 'n', 16, '', NULL, 2, 0, '{\'functions\':{\'field\':\'get_helper(\\\'employee_id\\\')\'}}', 8190, 7, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (50, 4, 'edit', 'notify_managed_by', NULL, NULL, 'Notify Managed By Person By Email', 'c', 3, '', NULL, 2, 0, '', 8190, 8, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (51, 4, 'edit', 'site_id', NULL, NULL, 'Site', 's', 3, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'site_id\\\')\'}}', 8190, 9, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (52, 4, 'edit', 'department_id', NULL, NULL, 'Department', 's', 64, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'department_id\\\')\'}}', 8190, 10, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (53, 4, 'edit', 'section_id', NULL, NULL, 'Section', 's', 64, '', NULL, 1, 0, '{\'functions\':{\'field\':\'get_dropdown(\\\'section_id\\\')\'}}', 8190, 11, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (54, 4, 'edit', 'allocated_to', NULL, NULL, 'Allocated To', 'n', 16, '', NULL, 2, 0, '{\'functions\':{\'field\':\'get_helper(\\\'employee_id\\\')\'}}', 8190, 12, 0);
INSERT INTO `_form_fields` (id, pid, page_type, field_name, field_table_name, field_table_fname, field_title, field_type, field_length, field_value, field_help_tip, field_columns, field_mandatory, field_options, user_group_mask, sort_order, system_status_id) VALUES (55, 4, 'edit', 'notify_allocated_to', NULL, NULL, 'Notify Allocated to Person By Email', 'c', 3, 'Yes', NULL, 2, 0, '', 8190, 13, 0);
# Dumping Table Structure for _menu

#
CREATE TABLE `_menu` (
  `menu_id` int(11) NOT NULL auto_increment,
  `menu_pid` int(11) default NULL,
  `menu_name` varchar(100) default NULL,
  `menu_title` varchar(100) default NULL,
  `user_group_mask` int(11) default NULL,
  `system_status_id` int(11) default NULL,
  PRIMARY KEY  (`menu_id`)
) TYPE=MyISAM;
#
# Dumping Data for _menu
#
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (1, 0, 'CATS', 'CATS', 0, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (2, 0, 'ADMIN', 'ADMIN', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (3, 0, 'PCR', 'PCR', 0, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (4, 1, 'actions', 'Actions', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (5, 1, 'audits_and_inspections', 'Audits & Inspections', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (6, 1, 'incidents', 'Incidents', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (7, 1, 'major_hazards_register', 'Major Hazards Register', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (8, 1, 'meeting_minutes', 'Meeting Minutes', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (9, 1, 'other_records', 'Other Records', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (10, 1, 'schedules', 'Schedules', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (11, 1, 'site_specific_obligations', 'Site Specific Obligations', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (12, 1, 'workload_review', 'Workload Review', 0, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (13, 2, 'oranisation_structure', 'Organisation Structure', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (14, 2, 'reallocate_responsibility', 'Re-Allocate Responsiblity', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (15, 2, 'employees', 'Employess', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (16, 2, 'incident_form_checkboxes', 'Incident Form Checkboxes', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (17, 2, 'reference_table', 'Reference Table', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (18, 2, 'risk_definitions', 'Risk Definitions', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (19, 2, 'work_hours', 'Work Hours', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (20, 2, 'message_of_the_day', 'Message of the Day', 8160, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (21, 2, 'lost_days', 'Lost Days', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (22, 2, 'email_configuration', 'Email Configuration', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (23, 3, 'pcr_search', 'PCR Search', 8190, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (24, 3, 'maintain_actions', 'Maintain Actions', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (25, 3, 'maintain_reviewers', 'Maintain Reviewers', 8176, NULL);
INSERT INTO `_menu` (menu_id, menu_pid, menu_name, menu_title, user_group_mask, system_status_id) VALUES (26, 3, 'email_messages', 'Email Messages', 8176, NULL);
# Dumping Table Structure for _sql_history

#
CREATE TABLE `_sql_history` (
  `sql_id` int(11) NOT NULL auto_increment,
  `sql_source` text,
  `sql_object` text,
  `sql_date_executed` datetime default NULL,
  `sql_type` varchar(50) default NULL,
  `sql_rows_effected` varchar(50) default NULL,
  `sql_error` text,
  PRIMARY KEY  (`sql_id`)
) TYPE=MyISAM;
#
# Dumping Data for _sql_history
#
# Dumping Table Structure for company

#
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL auto_increment,
  `company_description` varchar(100) default NULL,
  `comapny_name` varchar(100) default NULL,
  `system_status_id` int(11) default NULL,
  PRIMARY KEY  (`company_id`)
) TYPE=MyISAM;
#
# Dumping Data for company
#
# Dumping Table Structure for employee

#
CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL default '0',
  `site_id` int(11) default NULL,
  `department_id` int(11) default NULL,
  `position_id` int(11) default NULL,
  `induction_date` date default NULL,
  `employee_type_id` int(11) default NULL,
  `termination_date` date default NULL,
  `username` varchar(20) default NULL,
  `password` varchar(16) default NULL,
  `user_group_mask` int(11) default NULL,
  `first_name` varchar(64) default NULL,
  `last_name` varchar(64) default NULL,
  `contractor_company` varchar(100) default NULL,
  `email_address` varchar(255) default NULL,
  `viewable_sites` varchar(255) default NULL,
  `action_privileged_user` int(11) default NULL,
  `incident_priviledged_user` int(11) default NULL,
  `company_id` int(11) default NULL,
  `system_status_id` int(11) default NULL,
  PRIMARY KEY  (`employee_id`)
) TYPE=MyISAM;
#
# Dumping Data for employee
#
# Dumping Table Structure for incident

#
CREATE TABLE `incident` (
  `incident_id` int(11) NOT NULL auto_increment,
  `initiated_by_id` int(11) default NULL,
  `incident_date` date default NULL,
  `reported_date` date default NULL,
  `supperintendent_id` int(11) default NULL,
  `persons_involved_description` blob,
  `incident_type_id` int(11) default NULL,
  `injured_person_id` int(11) default NULL,
  `injured_person_gender` varchar(10) default NULL,
  `injured_status_id` int(11) default NULL,
  `body_part` int(11) default NULL,
  `incident_description` blob,
  `immediate_corrective_actions` blob,
  `env_incident_details` int(11) default NULL,
  `env_area_affected` varchar(50) default NULL,
  `investigation_required` int(11) default NULL,
  `mechanism_of_injury` int(11) default NULL,
  `cause_of_injury` int(11) default NULL,
  `incident_report_status_id` int(11) default NULL,
  `investigation_completion_date` date default NULL,
  `lostprod_safe` int(11) default NULL,
  `material_safe` int(11) default NULL,
  `other_safe` int(11) default NULL,
  `lostprod_pd` int(11) default NULL,
  `labour_pl` int(11) default NULL,
  `material_pl` int(11) default NULL,
  `other_pl` int(11) default NULL,
  `lostprod_env` int(11) default NULL,
  `labour_env` int(11) default NULL,
  `material_env` int(11) default NULL,
  `other_env` int(11) default NULL,
  `admin_checks_1` varchar(50) default NULL,
  `admin_checks_2` varchar(50) default NULL,
  `admin_checks_3` varchar(50) default NULL,
  `max_risk_result` varchar(50) default NULL,
  `recalc_risk_result` varchar(50) default NULL,
  `current_location` varchar(50) default NULL,
  `incident_time` varchar(50) default NULL,
  `report_time` varchar(50) default NULL,
  `company` varchar(100) default NULL,
  `location` varchar(100) default NULL,
  `area` varchar(100) default NULL,
  `equipment_no` varchar(100) default NULL,
  `injured_person_company` varchar(100) default NULL,
  `env_volume_of_spill` varchar(100) default NULL,
  `env_other` varchar(100) default NULL,
  `site_id` int(11) default NULL,
  `department_id` int(11) default NULL,
  `substatiated` varchar(20) default NULL,
  `estimated_cost` varchar(30) default NULL,
  `reported_to_id` int(11) default NULL,
  `incident_title` varchar(255) default NULL,
  `severity_rating` varchar(10) default NULL,
  `initial_risk_category` varchar(100) default NULL,
  `initial_risk_level` varchar(100) default NULL,
  `recalc_risk_category` varchar(100) default NULL,
  `recalc_risk_level` varchar(100) default NULL,
  `incident_display_id` varchar(15) default NULL,
  `primary_person_id` int(11) default NULL,
  `primary_company_id` int(11) default NULL,
  `injured_company_id` int(11) default NULL,
  `reported_company_id` int(11) default NULL,
  `system_status_id` int(11) default NULL,
  `system_type_id` varchar(50) default NULL,
  PRIMARY KEY  (`incident_id`)
) TYPE=MyISAM;
#
# Dumping Data for incident
#
# Dumping Table Structure for position

#
CREATE TABLE `position` (
  `position_id` int(11) NOT NULL auto_increment,
  `position_description` varchar(100) default NULL,
  `position_name` varchar(100) default NULL,
  `system_status_id` int(11) default NULL,
  PRIMARY KEY  (`position_id`)
) TYPE=MyISAM;
#
# Dumping Data for position
#
# Dumping Table Structure for user_group

#
CREATE TABLE `user_group` (
  `user_group_id` int(11) NOT NULL auto_increment,
  `user_group_name` varchar(100) default NULL,
  `user_group_mask` int(11) default NULL,
  `system_status_id` int(11) default NULL,
  PRIMARY KEY  (`user_group_id`)
) TYPE=MyISAM;
#
# Dumping Data for user_group
#
