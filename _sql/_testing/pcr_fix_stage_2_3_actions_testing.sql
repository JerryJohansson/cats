
select p.action_register_id from tblpcr_actions p, tblaction_details a where a.action_id = p.action_register_id and p.stage_id in (2,3)
select count(p.action_register_id) from tblpcr_actions p, tblaction_details a where a.action_id = p.action_register_id and p.stage_id in (2,3)

select count(s.report_id) from tblpcr_actions p, tblscheduler s where s.report_id = p.action_register_id and p.stage_id in (2,3)

-- delete schedules assocc. with stage 2,3 actions
delete from tblscheduler where action_id in (select p.action_register_id from tblpcr_actions p, tblaction_details a where a.action_id = p.action_register_id and p.stage_id in (2,3))
-- delete stage 2,3 actions
delete from tblaction_details where action_id in (select p.action_register_id from tblpcr_actions p, tblaction_details a where a.action_id = p.action_register_id and p.stage_id in (2,3))
-- delete stage 2,3 pcr actions
delete from tblpcr_actions where stage_id in (2,3)

update tblpcr_actions set action_register_id = NULL where stage_id in (2,3)
update tblpcr_actions set action_register_id = NULL, status = 'pending' where stage_id in (2,3)

-- delete schedules assocc. with stage 2,3 actions
delete from tblscheduler where action_id in (select action_register_id from tblpcr_actions where stage_id in (2,3))
-- delete stage 2,3 actions
delete from tblaction_details where action_id in (select action_register_id from tblpcr_actions where stage_id in (2,3))
-- delete stage 2,3 pcr actions
delete from tblpcr_actions where stage_id in (2,3)