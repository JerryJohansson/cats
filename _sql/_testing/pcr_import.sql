

update tblaction_details set allocated_to_id = managed_by_id where register_origin = 'PCR Action';
update tblaction_details set managed_by_id = 0 where register_origin = 'PCR Action';

update tblpcr_actions set allocated_to_id = managed_by_id;
update tblpcr_actions set managed_by_id = NULL;

update tblpcr_actions set managed_by_id = NULL where managed_by_id = 0 or managed_by_id is null

select * from tblpcr_actions where managed_by_id is null and rownum < 100
select * from tblpcr_actions where allocated_to_id is null and rownum < 100

--desc tblpcr_actions
--select * from tblpcr_actions
--select allocated_to_id from tblpcr_actions where allocated_to_id is not null
--select managed_by_id from tblpcr_actions where managed_by_id is not null
-- UPDATE allocated_to_id to managed_by_id and then UPDATE managed_by_id to null for both actions and pcr_actions
select count(allocated_to_id) from tblaction_details where register_origin = 'PCR Action' and allocated_to_id is not null;
select count(managed_by_id) from tblaction_details where register_origin = 'PCR Action' and managed_by_id is not null;

update tblaction_details set allocated_to_id = managed_by_id where register_origin = 'PCR Action';
update tblaction_details set managed_by_id = 0 where register_origin = 'PCR Action';

update tblpcr_actions set allocated_to_id = managed_by_id;
update tblpcr_actions set managed_by_id = NULL;



CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_ACTIONS_DETAILS" ("PCR_ID","DETAIL_ID","SITE_ID","STAGE_ID","ACTION_DESCRIPTION","ACTION_REGISTER_ID",
"MANAGED_BY_ID","MANAGED_BY","ALLOCATED_TO_ID","ALLOCATED_TO","STATUS","DUE_DATE","DAYS_AFTER_PREV_STAGE","ACTION_CONFIRMED","RAISE_NOW",
"MANAGER_NOTIFIED","SUPER_ENG_NOTIFIED","STAGE1_NOTIFIED","STAGE2_NOTIFIED","STAGE3_NOTIFIED","SITE",
"A_MANAGED_BY_ID","A_MANAGED_BY","A_ALLOCATED_TO_ID","A_ALLOCATED_TO","A_STATUS") AS (
SELECT 
	A.PCR_ID, 
	A.DETAIL_ID,
	A.SITE_ID,
	A.STAGE_ID,
	A.ACTION_DESCRIPTION,
	A.ACTION_REGISTER_ID,
	A.MANAGED_BY_ID,
	E1.LAST_NAME||', '||E1.FIRST_NAME AS MANAGED_BY, 
	A.ALLOCATED_TO_ID,
	E2.LAST_NAME||', '||E2.FIRST_NAME AS ALLOCATED_TO,
	A.STATUS,
	A.DUE_DATE,
	A.DAYS_AFTER_PREV_STAGE,
	A.ACTION_CONFIRMED,
	A.RAISE_NOW,
	A.MANAGER_NOTIFIED,
	A.SUPER_ENG_NOTIFIED,
	A.STAGE1_NOTIFIED,
	A.STAGE2_NOTIFIED,
	A.STAGE3_NOTIFIED,
	S.SITE_DESCRIPTION AS SITE,
	A2.MANAGED_BY_ID AS A_MANAGED_BY_ID,
	E3.LAST_NAME||', '||E3.FIRST_NAME AS A_MANAGED_BY, 
	A2.ALLOCATED_TO_ID AS A_ALLOCATED_TO_ID,
	E4.LAST_NAME||', '||E4.FIRST_NAME AS A_ALLOCATED_TO,
	A2.STATUS AS A_STATUS
FROM
	tblPCR_Actions A, 
	tblaction_details A2,
	tblEmployee_details E1,
	tblEmployee_details E2,
	tblEmployee_details E3,
	tblEmployee_details E4,
	tblsite S
WHERE
	A2.ACTION_ID = A.ACTION_REGISTER_ID(+)
	and A2.managed_by_id = E3.employee_number(+)
	and A2.allocated_to_id = E4.employee_number(+)
	and A.managed_by_id = E1.employee_number(+)
	and A.allocated_to_id = E2.employee_number(+)
	and A.site_id = S.site_id
);
commit;



