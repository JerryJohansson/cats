-- setup spooling so we can output the results to a file
set term on;
set heading off;
set linesize 2000;
spool migration.txt;

SELECT instance_name FROM v$instance;


-- turn spooling off
spool off;