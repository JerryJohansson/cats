-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool sequences.txt;


-- ACTION HISTORY SEQ
prompt "action_history_seq";
BEGIN
	IF ObjectExists('action_history_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.action_history_seq';
	END IF;	
END;
/

CREATE SEQUENCE catsdba.action_history_seq;
CREATE OR REPLACE TRIGGER catsdba.action_history_trg
	BEFORE INSERT ON catsdba.action_history
	FOR EACH ROW
	BEGIN
		SELECT catsdba.action_history_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

-- CFG_GROUP SEQ
prompt "CFG_GROUP_SEQ";
BEGIN
	IF ObjectExists('CFG_GROUP_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.CFG_GROUP_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(CFG_GROUP_ID) + 1 INTO seq_start FROM catsdba.CFG_GROUP;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.CFG_GROUP_SEQ START WITH ' || seq_start;
END;
/

CREATE OR REPLACE TRIGGER catsdba.CFG_GROUP_TRG
	BEFORE INSERT ON catsdba.CFG_GROUP
	FOR EACH ROW
	BEGIN
		SELECT catsdba.CFG_GROUP_SEQ.nextval INTO :new.CFG_GROUP_ID FROM dual;
	END;
/
commit;


-- CHECKBOXES SEQ - the table used to be TBLINCIDENTFORM_CHECKBOXES it is now CHECKBOXES
prompt "CHECKBOXES_SEQ";
BEGIN
	IF ObjectExists('CHECKBOXES_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.CHECKBOXES_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(CHECKBOX_ID) + 1 INTO seq_start FROM catsdba.TBLINCIDENTFORM_CHECKBOXES;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.CHECKBOXES_SEQ START WITH ' || seq_start;
END;
/

CREATE OR REPLACE TRIGGER catsdba.CHECKBOXES_TRG
	BEFORE INSERT ON catsdba.TBLINCIDENTFORM_CHECKBOXES
	FOR EACH ROW
	BEGIN
		SELECT catsdba.CHECKBOXES_SEQ.nextval INTO :new.CHECKBOX_ID FROM dual;
	END;
/
commit;


-- COMPANIES SEQ
prompt "COMPANIES_SEQ";
BEGIN
	IF ObjectExists('COMPANIES_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.COMPANIES_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(COMPANYID) + 1 INTO seq_start FROM catsdba.TBLCOMPANIES;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.COMPANIES_SEQ START WITH ' || seq_start;
END;
/

CREATE OR REPLACE TRIGGER catsdba.COMPANIES_TRG
	BEFORE INSERT ON catsdba.TBLCOMPANIES
	FOR EACH ROW
	BEGIN
		SELECT catsdba.COMPANIES_SEQ.nextval INTO :new.COMPANYID FROM dual;
	END;
/
commit;


-- CONFIGURATION SEQ
prompt "CONFIGURATION_SEQ";
BEGIN
	IF ObjectExists('CONFIGURATION_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.CONFIGURATION_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(CONFIGURATION_ID) + 1 INTO seq_start FROM catsdba.CONFIGURATION;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.CONFIGURATION_SEQ START WITH ' || seq_start;
END;
/

CREATE OR REPLACE TRIGGER catsdba.CONFIGURATION_TRG
	BEFORE INSERT ON catsdba.CONFIGURATION
	FOR EACH ROW
	BEGIN
		SELECT catsdba.CONFIGURATION_SEQ.nextval INTO :new.CONFIGURATION_ID FROM dual;
	END;
/
commit;


-- ERROR_LOG SEQ
prompt "ERROR_LOG_SEQ";
BEGIN
	IF ObjectExists('ERROR_LOG_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.ERROR_LOG_SEQ';
	END IF;	
END;
/

CREATE SEQUENCE catsdba.ERROR_LOG_SEQ;
CREATE OR REPLACE TRIGGER catsdba.ERROR_LOG_TRG
	BEFORE INSERT ON catsdba.ERROR_LOG
	FOR EACH ROW
	BEGIN
		SELECT catsdba.ERROR_LOG_SEQ.nextval INTO :new.ID FROM dual;
	END;
/
commit;


-- FILTERS SEQ
prompt "FILTERS_SEQ";
BEGIN
	IF ObjectExists('FILTERS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.FILTERS_SEQ';
	END IF;	
END;
/

CREATE SEQUENCE catsdba.FILTERS_SEQ;
CREATE OR REPLACE TRIGGER catsdba.FILTERS_TRG
	BEFORE INSERT ON catsdba.FILTERS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.FILTERS_SEQ.nextval INTO :new.ID FROM dual;
	END;
/
commit;


-- FORM_FIELDS SEQ
prompt "form_fields_seq";
BEGIN
	IF ObjectExists('form_fields_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.form_fields_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(field_id) + 1 INTO seq_start FROM catsdba.form_fields;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.form_fields_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.form_fields_trg
	BEFORE INSERT ON catsdba.form_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_fields_seq.nextval INTO :new.field_id FROM dual;
	END;
/
commit;


-- FORM_TYPES SEQ
prompt "form_types_seq";
BEGIN
	IF ObjectExists('form_types_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.form_types_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(id) + 1 INTO seq_start FROM catsdba.form_types;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.form_types_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.form_types_trg
	BEFORE INSERT ON catsdba.form_types
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_types_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;


-- HAZARD REGISTER SEQ - 
prompt "HAZARD_REGISTER_SEQ";
BEGIN
	IF ObjectExists('HAZARD_REGISTER_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.HAZARD_REGISTER_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(HAZARD_ID) + 1 INTO seq_start FROM catsdba.TBLHAZARDREGISTER;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.HAZARD_REGISTER_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.HAZARD_REGISTER_TRG
	BEFORE INSERT ON catsdba.TBLHAZARDREGISTER
	FOR EACH ROW
	BEGIN
		SELECT catsdba.HAZARD_REGISTER_SEQ.nextval INTO :new.HAZARD_ID FROM dual;
	END;
/
commit;


-- HELP_FIELDS SEQ
prompt "help_fields_seq";
BEGIN
	IF ObjectExists('help_fields_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.help_fields_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(id) + 1 INTO seq_start FROM catsdba.help_fields;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.help_fields_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.help_fields_trg
	BEFORE INSERT ON catsdba.help_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.help_fields_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;


-- INCIDENT_DETAILS SEQ
prompt "incident_details_seq";
BEGIN
	IF ObjectExists('incident_details_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.incident_details_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(incident_number) + 1 INTO seq_start FROM catsdba.TBLINCIDENT_DETAILS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.incident_details_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.incident_details_trg
	BEFORE INSERT ON catsdba.TBLINCIDENT_DETAILS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.incident_details_seq.nextval INTO :new.incident_number FROM dual;
	END;
/
commit;


-- MEETING_MINUTES SEQ - 
prompt "MEETING_MINUTES_SEQ";
BEGIN
	IF ObjectExists('MEETING_MINUTES_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.MEETING_MINUTES_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(REPORT_ID) + 1 INTO seq_start FROM catsdba.TBLMEETING_MINUTES;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.MEETING_MINUTES_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.MEETING_MINUTES_TRG
	BEFORE INSERT ON catsdba.TBLMEETING_MINUTES
	FOR EACH ROW
	BEGIN
		SELECT catsdba.MEETING_MINUTES_SEQ.nextval INTO :new.REPORT_ID FROM dual;
	END;
/
commit;


-- MOTD SEQ - 
prompt "MOTD_SEQ";
BEGIN
	IF ObjectExists('MOTD_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.MOTD_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(MOTDID) + 1 INTO seq_start FROM catsdba.TBLMOTD;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.MOTD_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.MOTD_TRG
	BEFORE INSERT ON catsdba.TBLMOTD
	FOR EACH ROW
	BEGIN
		SELECT catsdba.MOTD_SEQ.nextval INTO :new.MOTDID FROM dual;
	END;
/
commit;


-- SCHEDULES SEQ - 
prompt "SCHEDULES_SEQ";
BEGIN
	IF ObjectExists('SCHEDULES_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.SCHEDULES_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(SCHEDULER_ID) + 1 INTO seq_start FROM catsdba.TBLSCHEDULER;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.SCHEDULES_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.SCHEDULES_TRG
	BEFORE INSERT ON catsdba.TBLSCHEDULER
	FOR EACH ROW
	BEGIN
		SELECT catsdba.SCHEDULES_SEQ.nextval INTO :new.SCHEDULER_ID FROM dual;
	END;
/
commit;


-- STATUS_VALUES SEQ
prompt "STATUS_VALUES_SEQ";
BEGIN
	IF ObjectExists('STATUS_VALUES_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.STATUS_VALUES_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(ID) + 1 INTO seq_start FROM catsdba.STATUS_VALUES;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.STATUS_VALUES_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.STATUS_VALUES_TRG
	BEFORE INSERT ON catsdba.STATUS_VALUES
	FOR EACH ROW
	BEGIN
		SELECT catsdba.STATUS_VALUES_SEQ.nextval INTO :new.ID FROM dual;
	END;
/
commit;


-- TBLACTION_DETAILS SEQ
prompt "tblaction_details_seq";
BEGIN
	IF ObjectExists('tblaction_details_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblaction_details_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(action_id) + 1 INTO seq_start FROM catsdba.tblaction_details;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblaction_details_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblaction_details_trg
	BEFORE INSERT ON catsdba.tblaction_details
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblaction_details_seq.nextval INTO :new.action_id FROM dual;
	END;
/
commit;


-- TBLAUDITS SEQ - 
prompt "TBLAUDITS_SEQ";
BEGIN
	IF ObjectExists('TBLAUDITS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLAUDITS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(REPORT_ID) + 1 INTO seq_start FROM catsdba.TBLAUDITS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLAUDITS_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLAUDITS_TRG
	BEFORE INSERT ON catsdba.TBLAUDITS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLAUDITS_SEQ.nextval INTO :new.REPORT_ID FROM dual;
	END;
/
commit;


-- TBLEMPLOYEE_DETAILS SEQ
prompt "tblemployee_details_seq";
BEGIN
	IF ObjectExists('tblemployee_details_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblemployee_details_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(employee_number) + 1 INTO seq_start FROM catsdba.tblemployee_details;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblemployee_details_seq START WITH ' || seq_start;
END;
.
RUN;
commit;


-- TBLGOV_DETAILS SEQ
prompt "TBLGOV_DETAILS_SEQ";
BEGIN
	IF ObjectExists('TBLGOV_DETAILS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLGOV_DETAILS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start1 NUMBER;
BEGIN
	SELECT max(GOVERNMENT_ID) + 1 INTO seq_start FROM catsdba.TBLGOVERNMENT_DETAILS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLGOV_DETAILS_SEQ START WITH ' || seq_start1;
END;
.
RUN;
commit;


-- TBLLOST_DAYS SEQ - 
prompt "TBLLOST_DAYS_SEQ";
BEGIN
	IF ObjectExists('TBLLOST_DAYS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLLOST_DAYS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(LOST_DAYS_ID) + 1 INTO seq_start FROM catsdba.TBLLOST_DAYS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLLOST_DAYS_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLLOST_DAYS_TRG
	BEFORE INSERT ON catsdba.TBLLOST_DAYS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLLOST_DAYS_SEQ.nextval INTO :new.LOST_DAYS_ID FROM dual;
	END;
/
commit;


-- TBLMULTIDOCUMENTS_SEQ SEQ - 
prompt "TBLMULTIDOCUMENTS_SEQ";
BEGIN
	IF ObjectExists('TBLMULTIDOCUMENTS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLMULTIDOCUMENTS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(ID) + 1 INTO seq_start FROM catsdba.TBLMULTIDOCUMENTS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLMULTIDOCUMENTS_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLMULTIDOCUMENTS_TRG
	BEFORE INSERT ON catsdba.TBLMULTIDOCUMENTS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLMULTIDOCUMENTS_SEQ.nextval INTO :new.ID FROM dual;
	END;
/
commit;


-- TBLOBLIGATIONS SEQ - 
prompt "TBLOBLIGATIONS_SEQ";
BEGIN
	IF ObjectExists('TBLOBLIGATIONS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLOBLIGATIONS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(OBLIGATION_ID) + 1 INTO seq_start FROM catsdba.TBLOBLIGATIONS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLOBLIGATIONS_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLOBLIGATIONS_TRG
	BEFORE INSERT ON catsdba.TBLOBLIGATIONS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLOBLIGATIONS_SEQ.nextval INTO :new.OBLIGATION_ID FROM dual;
	END;
/
commit;


-- TBLOTHER_REPORTS_SEQ SEQ - 
prompt "TBLOTHER_REPORTS_SEQ";
BEGIN
	IF ObjectExists('TBLOTHER_REPORTS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLOTHER_REPORTS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(REPORT_ID) + 1 INTO seq_start FROM catsdba.TBLOTHER_REPORTS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLOTHER_REPORTS_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLOTHER_REPORTS_TRG
	BEFORE INSERT ON catsdba.TBLOTHER_REPORTS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLOTHER_REPORTS_SEQ.nextval INTO :new.REPORT_ID FROM dual;
	END;
/
commit;


-- TBLPCR_ACTION SEQ
prompt "tblpcr_action_seq";
BEGIN
	IF ObjectExists('tblpcr_action_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_action_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(action_id) + 1 INTO seq_start FROM catsdba.tblpcr_action;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_action_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_action_trg
	BEFORE INSERT ON catsdba.tblpcr_action
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_action_seq.nextval INTO :new.action_id FROM dual;
	END;
/
commit;


-- TBLPCR_ACTIONS SEQ for the action details - only detail_id need be incremented
prompt "tblpcr_actions_seq";
BEGIN
	IF ObjectExists('tblpcr_actions_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_actions_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(detail_id) + 1 INTO seq_start FROM catsdba.tblpcr_actions;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_actions_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_actions_trg
	BEFORE INSERT ON catsdba.tblpcr_actions
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_actions_seq.nextval INTO :new.detail_id FROM dual;
	END;
/
commit;


-- TBLPCR_AREA SEQ
prompt "tblpcr_area_seq";
BEGIN
	IF ObjectExists('tblpcr_area_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_area_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(area_id) + 1 INTO seq_start FROM catsdba.tblpcr_area;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_area_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_area_trg
	BEFORE INSERT ON catsdba.tblpcr_area
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_area_seq.nextval INTO :new.area_id FROM dual;
	END;
/
commit;


-- TBLPCR_DETAILS SEQ
prompt "tblpcr_details_seq";
BEGIN
	IF ObjectExists('tblpcr_details_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_details_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT SUBSTR(MAX(pcr_displayid),3) + 1 INTO seq_start FROM catsdba.tblpcr_details;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_details_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_details_trg
	BEFORE INSERT ON catsdba.tblpcr_details
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_details_seq.nextval INTO :new.pcr_id FROM dual;
		SELECT catsdba.tblsite.site_code || :new.pcr_id into :new.pcr_displayid FROM catsdba.tblsite WHERE :new.site_id = tblsite.site_id;
	END;
/
commit;


-- TBLPCR_EMAIL SEQ
prompt "tblpcr_email_seq";
BEGIN
	IF ObjectExists('tblpcr_email_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_email_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(email_id) + 1 INTO seq_start FROM catsdba.tblpcr_email;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_email_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_email_trg
	BEFORE INSERT ON catsdba.tblpcr_email
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_email_seq.nextval INTO :new.email_id FROM dual;
	END;
/
commit;


-- TBLPCR_MEETINGS SEQ
prompt "tblpcr_meetings_seq";
BEGIN
	IF ObjectExists('tblpcr_meetings_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_meetings_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(meeting_id) + 1 INTO seq_start FROM catsdba.tblpcr_meetings;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_meetings_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_meetings_trg
	BEFORE INSERT ON catsdba.tblpcr_meetings
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_meetings_seq.nextval INTO :new.meeting_id FROM dual;
	END;
/
commit;


-- TBLPCR_REVIEWER_DETAILS SEQ
prompt "tblpcr_reviewer_detail_seq";
BEGIN
	IF ObjectExists('tblpcr_reviewer_detail_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.tblpcr_reviewer_detail_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(detail_id) + 1 INTO seq_start FROM catsdba.tblpcr_reviewer_detail;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.tblpcr_reviewer_detail_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.tblpcr_reviewer_detail_trg
	BEFORE INSERT ON catsdba.tblpcr_reviewer_detail
	FOR EACH ROW
	BEGIN
		SELECT catsdba.tblpcr_reviewer_detail_seq.nextval INTO :new.detail_id FROM dual;
	END;
/
commit;


-- TBLREFTABLE_SEQ SEQ - 
prompt "TBLREFTABLE_SEQ";
BEGIN
	IF ObjectExists('TBLREFTABLE_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLREFTABLE_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(REFTABLE_ID) + 1 INTO seq_start FROM catsdba.TBLREFTABLE;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLREFTABLE_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLREFTABLE_TRG
	BEFORE INSERT ON catsdba.TBLREFTABLE
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLREFTABLE_SEQ.nextval INTO :new.REFTABLE_ID FROM dual;
	END;
/
commit;


-- TBLSITE SEQ - 
prompt "TBLSITE_SEQ";
BEGIN
	IF ObjectExists('TBLSITE_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.TBLSITE_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(SITE_ID) + 1 INTO seq_start FROM catsdba.TBLSITE;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.TBLSITE_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.TBLSITE_TRG
	BEFORE INSERT ON catsdba.TBLSITE
	FOR EACH ROW
	BEGIN
		SELECT catsdba.TBLSITE_SEQ.nextval INTO :new.SITE_ID FROM dual;
	END;
/
commit;


-- USER_GROUP SEQ
prompt "user_group_seq";
BEGIN
	IF ObjectExists('user_group_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.user_group_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(user_group_id) + 1 INTO seq_start FROM catsdba.user_group;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.user_group_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.user_group_trg
	BEFORE INSERT ON catsdba.user_group
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_group_seq.nextval INTO :new.user_group_id FROM dual;
	END;
/
commit;


-- USER_HISTORY_LOG SEQ
prompt "user_history_log_seq";
BEGIN
	IF ObjectExists('user_history_log_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.user_history_log_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(id) + 1 INTO seq_start FROM catsdba.user_history_log;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.user_history_log_seq START WITH ' || seq_start;
	EXCEPTION
		WHEN No_Data_Found THEN
			EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.user_history_log_seq START WITH 1';
		WHEN Others THEN
			EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.user_history_log_seq START WITH 1';
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.user_history_log_trg
	BEFORE INSERT ON catsdba.user_history_log
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_history_log_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;


-- USER_ROLE SEQ
prompt "user_role_seq";
BEGIN
	IF ObjectExists('user_role_seq') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.user_role_seq';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(user_role_id) + 1 INTO seq_start FROM catsdba.user_role;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.user_role_seq START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.user_role_trg
	BEFORE INSERT ON catsdba.user_role
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_role_seq.nextval INTO :new.user_role_id FROM dual;
	END;
/
commit;


-- WORK_HOURS SEQ - the table used to be TBLWORK_HOURS it is now CHECKBOXES
prompt "WORK_HOURS_SEQ";
BEGIN
	IF ObjectExists('WORK_HOURS_SEQ') > 0 THEN
		EXECUTE IMMEDIATE 'DROP SEQUENCE catsdba.WORK_HOURS_SEQ';
	END IF;	
END;
/

DECLARE
	seq_start NUMBER;
BEGIN
	SELECT max(WORK_HOURS_ID) + 1 INTO seq_start FROM catsdba.TBLWORK_HOURS;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE catsdba.WORK_HOURS_SEQ START WITH ' || seq_start;
END;
.
RUN;

CREATE OR REPLACE TRIGGER catsdba.WORK_HOURS_TRG
	BEFORE INSERT ON catsdba.TBLWORK_HOURS
	FOR EACH ROW
	BEGIN
		SELECT catsdba.WORK_HOURS_SEQ.nextval INTO :new.WORK_HOURS_ID FROM dual;
	END;
/
commit;


-- turn spooling off
spool off;