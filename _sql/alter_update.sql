-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool alter_update.txt;


-- ACTION STUFF
prompt "ACTION STUFF";
-- add critical and date created to action details
alter table tblaction_details add(critical varchar2(3));
-- set created date to a default of the start of the century
update tblaction_details set date_created = TO_DATE('1900-01-01','YYYY-MM-DD') where date_created is null;
commit;
-- update action details with correct views
update tblaction_details set origin_table = 'ORIGINATING_PCR_ACTION' where origin_table in ('PCR_Actions');
commit;
-- set default reminder date for PCR-actions
UPDATE tblaction_details SET reminder_date = scheduled_date - 7 WHERE register_origin = 'PCR Action' AND reminder_date is null AND scheduled_date - 7 > SYSDATE;
UPDATE tblaction_details SET reminder_date = scheduled_date WHERE register_origin = 'PCR Action' AND reminder_date is null AND scheduled_date - 7 < SYSDATE;
commit;

-- EMAIL STUFF
prompt "EMAIL STUFF";
-- add email_id to email options
alter table tblemailoptions add(email_id number );
update tblemailoptions set email_id=1;
alter table tblemailoptions modify(email_id number not null);
commit;


-- EMPLOYEE STUFF
prompt "EMPLOYEE STUFF";
-- add site_access column to eventually replace viewable_sites column
-- add induction_number for user defined employee_id ? sad but true
alter table tblemployee_details add(SITE_ACCESS VARCHAR2(50), INDUCTION_NUMBER NUMBER);
commit;

-- update induction_number to a default value of employee_number
update tblemployee_details set induction_number = employee_number;
commit;

-- add group mask and role mask to tblemployee_details
alter table catsdba.tblemployee_details add(group_mask number, role_mask number);
commit;

-- update role_mask for multiple roles access
update catsdba.tblemployee_details e set e.role_mask = 0;
commit;

-- RUN INSTALL SCRIPT INSTEAD update group_mask for multiple groups access
--update catsdba.tblemployee_details e set group_mask = (select u.ug_mask from catsdba.ug u where u.ug_name=e.group_name);
--commit;

-- update kevin lilje to root admin and everything below
update catsdba.tblemployee_details e set group_mask = (1588478) where upper(access_id) = 'K_LILJE';
commit;

-- update actions privileged user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+2048) where e.action_privileged_user = 1;
commit;

-- update can change action due date user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+4096) where e.CAN_CHANGE_DUE_DATE = 1;
commit;

-- update incidents privileged user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+8192) where e.incident_privileged_user = 1;
commit;

-- add new Superintendent, Engineer Columns
ALTER TABLE catsdba.tblemployee_details ADD(tmpsuperintendent varchar2(3), tmpengineer varchar2(3));

-- update tmpsuperintendent user setting
UPDATE catsdba.tblemployee_details e SET e.tmpsuperintendent = 'Yes' WHERE e.superintendent = 1;
UPDATE catsdba.tblemployee_details e SET e.tmpsuperintendent = 'No' WHERE e.superintendent = 0 OR e.superintendent IS NULL;

-- update tmpengineer user setting
UPDATE catsdba.tblemployee_details e SET e.tmpengineer = 'Yes' WHERE e.engineer = 1;
UPDATE catsdba.tblemployee_details e SET e.tmpengineer = 'No' WHERE e.engineer = 0 OR e.engineer IS NULL;

-- drop old columns
ALTER TABLE catsdba.tblemployee_details DROP COLUMN superintendent;
ALTER TABLE catsdba.tblemployee_details DROP COLUMN engineer;

-- rename Temporary to Real
ALTER TABLE catsdba.tblemployee_details RENAME COLUMN tmpsuperintendent TO superintendent;
ALTER TABLE catsdba.tblemployee_details RENAME COLUMN tmpengineer TO engineer;
commit;

-- INCIDENT STUFF
prompt "INCIDENT STUFF";
-- add sort order to incident checkboxes
alter table tblIncidentForm_Checkboxes add(sort_order number);
commit;

-- update incident_details 
-- convert incident_date and report_date to timestamp datatype
alter table tblincident_details modify(incident_date timestamp, report_date timestamp);
commit;

-- incident_time to incident_date etc...
update tblincident_details set report_time = '00:00' where report_time like '24%';
update tblincident_details set incident_time = '00:00' where incident_time like '24%';
-- update incident_date, report_date to include time from respective time fields
update tblincident_details set 
	incident_date = to_date(to_char(incident_date,'YYYY-MM-DD')||' '||incident_time,'YYYY-MM-DD HH24:MI'),
	report_date = to_date(to_char(report_date,'YYYY-MM-DD')||' '||report_time,'YYYY-MM-DD HH24:MI')
	;
commit;

-- rename MPR to DOIR in incident checkboxes(tblIncidentform_checkboxes)
update tblincidentform_checkboxes set checkbox_desc = 'DOIR', checkbox_comment = 'Department of Industry and Resources' where checkbox_id = 157;
commit;

-- move pcr checkboxes to the main checkboxes table
--insert into TBLINCIDENTFORM_CHECKBOXES (checkbox_type, checkbox_desc, checkbox_comment, dropdown_display) (select checkbox_type, checkbox_desc, checkbox_comment, dropdown_display from TBLPCR_CHECKBOXES);
--commit;


-- LOST DAYS STUFF
prompt "LOST DAYS STUFF";
-- lost days modifications
alter table tbllost_days add(tmpyears number(4), tmpmonths number(2));
update tbllost_days set tmpyears=years, tmpmonths=months;
alter table tbllost_days drop column years;
alter table tbllost_days drop column months;
alter table tbllost_days rename column tmpyears to years;
alter table tbllost_days rename column tmpmonths to months;
commit;

-- add rostered days to lost days(tblLost_Days) table
alter table tbllost_days add(rostered_days number);
commit;


-- OTHER REPORTS STUFF
prompt "OTHER REPORTS STUFF";
-- add reported_by_id for employee number instead of employee name
alter table TBLOTHER_REPORTS add(reported_by_id number);
commit;

-- update the table with employee_numbers
update TBLOTHER_REPORTS o set o.reported_by_id = (select e.employee_number from view_employee_details e where e.emp_name = o.reported_by and e.user_status='Active');
commit;


-- PCR STUFF
prompt "PCR STUFF";
-- modify pcr make permanent, extend permanent
alter table tblpcr_details add(tmpmake varchar2(3), tmpextend varchar2(3));
update tblpcr_details set tmpmake='Yes' where make_permanent = 1;
update tblpcr_details set tmpextend='Yes' where extend_permanent = 1;
update tblpcr_details set tmpmake='No' where make_permanent = 0;
update tblpcr_details set tmpextend='No' where extend_permanent = 0;
alter table tblpcr_details drop column make_permanent;
alter table tblpcr_details drop column extend_permanent;
alter table tblpcr_details rename column tmpmake to make_permanent;
alter table tblpcr_details rename column tmpextend to extend_permanent;
commit;

-- make the status column in the tblpcr_areareview_details table longer
ALTER TABLE tblpcr_areareview_details MODIFY status varchar2(40);
ALTER TABLE tblpcr_extension_details MODIFY status varchar2(40);
ALTER TABLE tblpcr_initialreview_details MODIFY status varchar2(40);
ALTER TABLE tblpcr_pcrtreview_details MODIFY status varchar2(40);
ALTER TABLE tblpcr_signoff_details MODIFY status varchar2(40);
commit;

-- rename 'Proceed with change' to 'Proceed with extension' for the Action in PCR Extensions
UPDATE tblpcr_extension SET action='Proceed with extension' WHERE action='Proceed with change';
commit;

-- TBLPCR_SIGNOFF STUFF
prompt "TBLPCR_SIGNOFF STUFF";
-- Fix the Errornous Records where SIGN_OFF is 'Approve' instead of 'Approved'
UPDATE tblpcr_signoff SET sign_off = 'Approved' where sign_off = 'Approve';
commit;

-- PCR_AREA STUFF
prompt "PCR_AREA STUFF";
ALTER TABLE tblpcr_area MODIFY area_id NUMBER NOT NULL ENABLE;
commit;

-- create index
CREATE UNIQUE INDEX "CATSDBA"."PK_TBLPCR_AREA_ID" ON "CATSDBA"."TBLPCR_AREA" 
(
	"AREA_ID"
);
commit;

-- SITE STUFF
prompt "SITE STUFF";
-- add site folder to sites table - stores the default network directory for user browsing 
alter table tblsite add(site_folder varchar2(255) default 'C:\');


-- REFTABLE STUFF
prompt "REFTABLE STUFF";
UPDATE catsdba.tblreftable SET reftable_description='Closed' WHERE reftable_id=27;
DELETE FROM catsdba.tblreftable WHERE reftable_id=28;
commit;


-- turn spooling off
spool off;