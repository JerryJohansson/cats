-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool update_user_permissions.txt;


-- update kevin lilje to root admin and everything below
update catsdba.tblemployee_details e set group_mask = (1588478) where upper(access_id) = 'K_LILJE';
commit;

-- update actions privileged user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+2048) where e.action_privileged_user = 1;
commit;

-- update can change action due date user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+4096) where e.CAN_CHANGE_DUE_DATE = 1;
commit;

-- update incidents privileged user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+8192) where e.incident_privileged_user = 1;
commit;


-- turn spooling off
spool off;