CREATE TABLESPACE catstst_ts
  logging
  datafile 'c:/oracle/product/10.1.0/oradata/catstst/catstst_ts.dbf' 
  size 32m 
  autoextend on 
  next 32m maxsize 2048m
  extent management local;

CREATE TABLESPACE catstst_temp
  logging
  datafile 'c:/oracle/product/10.1.0/oradata/catstst/catstst_temp.dbf' 
  size 32m 
  autoextend on 
  next 32m maxsize 2048m
  extent management local;

DROP USER catsdba CASCADE; 
 
CREATE USER catsdba IDENTIFIED BY takeextracare 
	DEFAULT TABLESPACE catstst_ts   
	TEMPORARY TABLESPACE temp 
	QUOTA UNLIMITED ON catstst_ts; 
 
CREATE ROLE cats_admin; 
 
GRANT 
	CREATE session,
	CREATE table,
	CREATE view,
	CREATE procedure,
	CREATE synonym
TO CATS_ADMIN; 

GRANT
	ALL PRIVILEGES
TO CATS_ADMIN;

GRANT cats_admin TO catsdba;

DROP TABLE IF EXISTS catsdba.form_fields;
CREATE TABLE catsdba.form_fields (
  field_id NUMBER NOT NULL,
  field_pid NUMBER default NULL,
  field_group_type varchar2(50) default NULL,
  field_name varchar2(128) default NULL,
  field_label varchar2(128) default NULL,
  field_type varchar2(10) default NULL,
  field_length NUMBER default NULL,
  field_value varchar2(1000),
  field_attributes varchar2(255) default NULL,
  col_num SMALLINT default NULL,
  col_span SMALLINT default NULL,
  row_span SMALLINT default NULL,
  field_column_name varchar2(50) default NULL,
  field_display_func varchar2(255) default NULL,
  field_set_func varchar2(255) default NULL,
  col_count SMALLINT default NULL,
  mandatory SMALLINT default NULL,
  options varchar2(1000),
  sort_order NUMBER default 1,
  role_mask NUMBER default NULL,
  group_mask NUMBER default NULL,
  status SMALLINT default NULL
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catstst_ts
;

CREATE UNIQUE INDEX catsdba.pk_form_fields_id ON catsdba.form_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catstst_ts
;

commit;

INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 0, 'menu', 'cats', 'CATS', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 0, 'menu', 'admin', 'ADMIN', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 0, 'menu', 'pcr', 'PCR', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 1, 'menu', 'actions', 'Actions', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 1, 'menu', 'audits_and_inspections', 'Audits & Inspections', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 1, 'menu', 'government_inspections', 'Government Inspections', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 1, 'menu', 'incidents', 'Incidents', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 1, 'menu', 'major_hazards', 'Major Hazard Register', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 1, 'menu', 'meeting_minutes', 'Meeting Minutes', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 1, 'menu', 'other_records', 'Other Records', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 1, 'menu', 'schedules', 'Schedules', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 1, 'menu', 'site_specific', 'Site Specific Obligations', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 1, 'menu', 'workload_review', 'Workload Review', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (14, 2, 'menu', 'organisation_structure', 'Organisation Structure', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 2, 'menu', 'reallocate', 'Re-Allocate responsibility', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (16, 2, 'menu', 'employees', 'Employees', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 2, 'menu', 'checkboxes', 'Incident Form Checkboxes', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 2, 'menu', 'references', 'Reference Table', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 2, 'menu', 'risk_definitions', 'Risk Definitions', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 2, 'menu', 'work_hours', 'Work Hours', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 2, 'menu', 'motd', 'Message of the Day', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 2, 'menu', 'lost_days', 'Lost Days', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 2, 'menu', 'email_configuration', 'Email Configuration', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (24, 3, 'menu', 'pcr_search', 'PCR Search', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 3, 'menu', 'pcr_maintain_actions', 'Maintain Actions', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 3, 'menu', 'pcr_maintain_reviewers', 'Maintain Reviewers', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 3, 'menu', 'pcr_maintain_email', '', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 4, 'search', 'site_id', 'Site', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (29, 4, 'search', 'department_id', 'Department', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (30, 4, 'search', 'section_id', 'Section', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (31, 4, 'search', 'action_managed_by', 'Action Managed By', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (32, 4, 'search', 'allocated_to', 'Allocated To', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (33, 4, 'search', 'managed_by_or_allocated_to', 'Managed By Or Allocated To', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (34, 4, 'search', 'action_type', 'Action Type', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (35, 4, 'search', 'origin', 'Origin', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'get_dropdown(\'origin\')', 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (36, 4, 'search', 'action_category_id', 'Category', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '{\'function\':\'get_dropdown(\\\'action_category_id\\\')\'}', 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (37, 4, 'search', 'status', 'Status', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (38, 4, 'search', 'action_number', 'Action Number', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (39, 4, 'search', 'due_date_from', 'Due Date From', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (40, 4, 'search', 'due_date_to', 'Due Date To', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (41, 4, 'search', 'closed_date_from', 'Closed Date From', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (42, 4, 'search', 'closed_date_to', 'Closed Date To', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (43, 4, 'edit', 'origin', 'Origin', 's', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 4, 'edit', 'action_category_id', 'Action Category', 'g', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '{\'function\':\'get_checkboxes(\\\'action_category_id\\\')\'}', 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (45, 4, 'edit', 'action_title', 'Action Title', 's', 128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (46, 4, 'edit', 'description', 'Description', 's', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (47, 4, 'edit', 'scheduled_date', 'Scheduled Completion Date', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (48, 4, 'edit', 'reminder_date', 'Date Next Reminder to be Sent to Manager', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (49, 4, 'edit', 'managed_by', 'Action Managed By', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '{\'function\':\'get_dropdown(\\\'employee_id\\\')\'}', 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (50, 4, 'edit', 'notify_managed_by', 'Notify Managed By Person By Email', 'c', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (51, 4, 'edit', 'site_id', 'Site', 's', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (52, 4, 'edit', 'department_id', 'Department', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (53, 4, 'edit', 'section_id', 'Section', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (54, 4, 'edit', 'allocated_to', 'Allocated To', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);
INSERT INTO form_fields (field_id, field_pid, field_group_type, field_name, field_label, field_type, field_length, field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (55, 4, 'edit', 'notify_allocated_to', 'Notify Allocated to Person By Email', 'c', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, NULL);


