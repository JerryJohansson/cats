# DBTools DBMYSQL - MySQL Database Dump
#

SET FOREIGN_KEY_CHECKS=0;

# Dumping Table Structure for user_pref

#
CREATE TABLE `user_pref` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `pname` varchar(128) default NULL,
  `pvalue` varchar(255) default NULL,
  `palt_value` varchar(255) default NULL,
  `modified_date` date default NULL,
  `status` int(11) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#
# Dumping Data for user_pref
#
SET FOREIGN_KEY_CHECKS=1

