DROP INDEX catsdba.pk_configuration_id;
DROP TABLE catsdba.configuration;
DROP SEQUENCE catsdba.configuration_seq;
commit;

CREATE TABLE catsdba.configuration (
  configuration_id number NOT NULL,
  configuration_title varchar2(64) NOT NULL,
  configuration_key varchar2(64) NOT NULL,
  configuration_value varchar2(255) NOT NULL,
  configuration_description varchar2(255) NOT NULL,
  configuration_group_id number NOT NULL,
  sort_order number default NULL,
  last_modified date default NULL,
  date_added date NOT NULL,
  use_function varchar2(255) default NULL,
  set_function varchar2(255) default NULL
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_configuration_id ON catsdba.configuration
(
    configuration_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.configuration_seq;
CREATE OR REPLACE TRIGGER catsdba.configuration_trg
	BEFORE INSERT ON catsdba.configuration
	FOR EACH ROW
	BEGIN
		SELECT catsdba.configuration_seq.nextval INTO :new.configuration_id FROM dual;
	END;
/
commit;

-- Insert config data
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Site Name', 'SITE_NAME', 'CATS', 'Set the website name. This value will be displayed globally throughout the site', 1, 1, to_date('2004-09-05 01:23:19', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:30:04', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Site Owner', 'SITE_OWNER', 'CATS Admin', 'Owner of the website or main administrator i.e. webmaster', 1, 2, to_date('2004-06-23 16:33:02', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:33:02', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Site Owners Email Address', 'SITE_OWNER_EMAIL', 'catsadmin@tronox.com', 'Email address of the site owner', 4, 3, to_date('2004-06-23 16:34:04', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:34:04', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Default From Email Address', 'FROM_EMAIL', 'catsadmin@tronox.com', 'The email address used in the from field when sending emails', 4, 4, to_date('2004-06-23 16:35:33', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:35:33', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Country', 'SITE_COUNTRY', 'Australia', 'Where the company is located', 1, 3, to_date('2004-06-23 16:38:47', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:38:47', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Zone', 'SITE_ZONE', 'Western Australia', 'What zone is the company in', 1, 4, to_date('2004-06-23 16:39:56', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:39:56', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Address', 'SITE_ADDRESS', 'TIWEST Pty Ltd\r\n1 Brodie Hall Drive\r\nBently\r\nWA 6983', 'Company physical address', 1, 5, to_date('2005-07-20 04:20:22', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:40:27', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_textarea(');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Mailing Address', 'SITE_MAILING_ADDRESS', 'TIWEST Pty Ltd\r\nLocked Bag  381\r\nBentley Delivery Centre\r\nBently\r\nWA 6983', 'Company mailing address', 1, 6, to_date('2005-07-20 04:20:38', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-06-23 16:41:20', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_textarea(');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Maximum display links', 'MAX_DISPLAY_PAGE_LINKS', '20', 'Set the maximum allowed links before paging begins', 2, 10, to_date('2005-07-18 12:12:37', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-18 12:12:37', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Sites Default Theme', 'DEFAULT_STYLE', 'cats', 'Sets the root folder to be used for templates', 2, 8, to_date('2005-07-18 09:55:14', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-18 09:55:14', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Display Number of display string', 'TEXT_DISPLAY_NUMBER_OF', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> items)', 'Sets the Display Number Of text display at the bottom of paged lists', 2, 9, to_date('2005-07-18 12:08:44', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-18 12:08:44', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Session Name', 'SESSION_NAME', 'cats', 'The name of the session id', 3, 1, to_date('2005-07-20 04:17:42', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-09-04 12:15:56', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Session Info', 'STORE_SESSIONS', 'file', 'Where to store session information', 3, 2, to_date('2004-09-08 14:37:16', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-09-04 16:59:18', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''file\'', \''mysql\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Email Method', 'EMAIL_TRANSPORT', 'smtp', 'Email method to use. Choose smtp for windows machines', 4, 1, to_date('2004-09-08 10:28:49', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-09-08 10:28:49', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''sendmail\'', \''smtp\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Email Line Feed Type', 'EMAIL_LINEFEED', 'CRLF', 'Please use CRLF on windows servers', 4, 2, to_date('2004-09-08 14:43:48', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-09-08 10:31:56', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''CRLF\'', \''LF\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('From Email Name', 'FROM_EMAIL_NAME', 'CATS Admin', 'Default from email name', 4, 5, to_date('2005-07-20 04:18:44', 'YYYY-MM-DD HH24:MI:SS'), to_date('2004-09-08 10:48:33', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Showing page N of N display text', 'TEXT_RESULT_PAGE', 'Page %s of %d', 'Sets the format of the page results text of a recordset', 2, 11, to_date('2005-07-18 12:17:17', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-18 12:17:17', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Previous and Next button format', 'PREVNEXT_BUTTON_PREV', '&lt;&lt;', 'The label to use as the previous button', 2, 12, to_date('2005-07-18 12:18:35', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-18 12:18:35', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Previous and Next button Next button', 'PREVNEXT_BUTTON_NEXT', '&gt;&gt;', 'The label to use as the next button', 2, 13, to_date('2005-07-18 12:20:05', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-18 12:20:05', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Administrator Email Address', 'ADMIN_EMAIL', 'catsadmin@tronox.com', 'Email address of the main administrator or webmaster', 1, 14, to_date('2005-07-20 04:02:53', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-20 04:02:53', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL);
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Set Email Notification flag', 'EMAIL_NOTIFY_ON_PUBLISH', 'true', 'When a page is published you can have the system email certain individuals', 4, 15, to_date('2005-07-20 04:04:57', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-20 04:04:57', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''true\'', \''false\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Set Owner of page to recieve email notification', 'EMAIL_NOTIFY_OWNER_ON', 'false', 'When a page is published this user will recieve an email notification of the status of the job', 4, 16, to_date('2005-07-20 04:07:19', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-20 04:07:19', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''true\'', \''false\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Set Site Owner of page to recieve email notification', 'EMAIL_NOTIFY_SITE_OWNER_ON', 'false', 'When a page is published this user will recieve an email notification of the status of the job', 4, 16, to_date('2005-07-20 04:09:19', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-20 04:09:19', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''true\'', \''false\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Set Site Publisher of page to recieve email notification', 'EMAIL_NOTIFY_PUBLISHER_ON', 'true', 'When a page is published this user will recieve an email notification of the status of the job', 4, 17, to_date('2005-07-20 04:10:39', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-20 04:10:39', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''true\'', \''false\''),');
INSERT INTO catsdba.configuration (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES ('Set Editor of page to recieve email notification', 'EMAIL_NOTIFY_EDITOR_ON', 'true', 'When a page is published this user will recieve an email notification of the status of the job', 4, 18, to_date('2005-07-20 04:11:48', 'YYYY-MM-DD HH24:MI:SS'), to_date('2005-07-20 04:11:48', 'YYYY-MM-DD HH24:MI:SS'), NULL, 'ezw_cfg_select_option(array(\''true\'', \''false\''),');


INSERT INTO 
catsdba.configuration 
(
configuration_title, 
configuration_key, 
configuration_value, 
configuration_description, 
configuration_group_id, 
sort_order, 
last_modified, 
date_added, 
use_function, 
set_function
) VALUES (
'Site Name', 
'SITE_NAME', 
'CATS', 
'Set the website name. This value will be displayed globally throughout the site', 
1, 
1,
to_date(to_date('2004-09-05 01:23:19', 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS'), 
to_date(to_date('2004-06-23 16:30:04', 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS'), 
NULL, 
NULL
);











INSERT INTO 
catsdba.configuration (
configuration_title, 
configuration_key, 
configuration_value, 
configuration_description, 
configuration_group_id, 
sort_order, 
last_modified, 
date_added, 
use_function, 
set_function
) VALUES (
'Set Editor of page to recieve email notification', 
'EMAIL_NOTIFY_EDITOR_ON', 
'true', 
'When a page is published this user will recieve an email notification of the status of the job', 
4, 
18, 
to_date(to_date('2005-07-20 04:11:48', 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS'), 
to_date(to_date('2005-07-20 04:11:48', 'YYYY-MM-DD HH24:MI:SS'), 'YYYY-MM-DD HH24:MI:SS'), 
NULL, 
'ezw_cfg_select_option(array(\'''true\''', \'''false\'''),'
);