-- setup spooling so we can output the results to a file
set term off;
set heading off;
set linesize 2000;
spool transform.txt;

-- Fix main menu descriptions
update form_fields set field_label = 'CATS - Main Menu', group_mask = 9977086 where field_id = 1;
update form_fields set field_label = 'CATS - Administration', group_mask = 9962496 where field_id = 2;
update form_fields set field_label = 'CATS - Plant Change Request', group_mask = 9962688 where field_id = 3;
update form_fields set field_label = 'CATS - Configuration Settings', group_mask = 9437184 where field_id = 4;
update form_fields set field_name = 'checkboxes' where field_name = 'incident_checkboxes';
update form_fields set status = 128;
-- add origin stuff to form fields so we can get info from db instead query strings
alter table form_fields add(origin_table varchar2(255) default NULL,	register_origin varchar2(255) default NULL);
commit;


-- add message column to user_history_log
--alter table user_history_log add MSG varchar2(255);
commit;

-- add email_id to email options
alter table tblemailoptions add(email_id number );
update tblemailoptions set email_id=1;
alter table tblemailoptions modify(email_id number not null);
commit;

-- add critical and date created to action details
alter table tblaction_details add(date_created date default sysdate, critical varchar2(3));
-- set created date to a default of the start of the century
update tblaction_details set date_created = TO_DATE('1900-01-01','YYYY-MM-DD');
commit;
-- update action details with correct views
update tblaction_details set origin_table = 'ORIGINATING_PCR_ACTION' where origin_table in ('PCR_Actions');
commit;

CREATE OR REPLACE VIEW "CATSDBA"."VIEW_ACTION_DETAILS" ("ACTION_ID","REGISTER_ORIGIN","ACTION_TYPE","ACTION_TITLE","ACTION_DESCRIPTION","SCHEDULED_DATE","REMINDER_DATE","CHANGE_COMMENTS","MANAGED_BY_ID","ALLOCATED_TO_ID","NOTIFY_ALLOCATED_EMAIL","NOTIFY_ALLOCATED_PRINT","STATUS","CLOSING_DATE","CLOSED_BY","DATE_DUE_CHANGED_HISTORY","RECORD_LOCATION","RECORD_COMMENTS","ORIGIN_TABLE","REPORT_ID","SITE_ID","DEPARTMENT_ID","SECTION_ID","DATE_CREATED","CRITICAL","MANAGED_BY","M_SITE_ID","ALLOCATED_TO","A_SITE_ID","CLOSED_BY_NAME","C_SITE_ID","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","SECTION_DESCRIPTION","ACTIONCATEGORY") AS (
SELECT
	a.Action_Id,
	a.Register_Origin,
	a.Action_Type,
	a.Action_Title,
	a.Action_Description,
	a.Scheduled_Date,
	a.Reminder_Date,
	a.Change_Comments,
	a.Managed_By_Id,
	a.Allocated_To_Id,
	a.Notify_Allocated_Email,
	a.Notify_Allocated_Print,
	a.Status,
	a.Closing_Date,
	a.Closed_By,
	a.Date_Due_Changed_History,
	a.Record_Location,
	a.Record_Comments,
	a.Origin_Table,
	a.Report_Id,
	a.Site_Id,
	a.Department_Id,
	a.Section_Id,
	a.date_created,
	a.critical,
	e1.Last_Name || ', ' || e1.First_Name Managed_By,
	e1.Site_ID M_Site_ID,
	e2.Last_Name || ', ' || e2.First_Name Allocated_To,
	e2.Site_ID A_Site_ID,
	e3.Last_Name || ', ' || e3.First_Name Closed_By_Name,
	e3.Site_ID C_Site_ID,
	s.Site_Description,
	d.Department_Description,
	sec.Section_Description,
	F_ActionString(a.Action_Id, 'ActionCategory') ActionCategory
FROM
	tblAction_Details a,
	tblSite s,
	tblDepartment d,
	tblSection sec,
	tblEmployee_Details e1,
	tblEmployee_Details e2,
	tblEmployee_Details e3
WHERE
	a.Site_Id = s.Site_Id(+)
	AND a.Department_Id = d.Department_Id(+)
	AND a.Section_Id = sec.Section_Id(+)
	AND a.Managed_By_Id = e1.Employee_Number(+)
	AND a.Allocated_To_Id = e2.Employee_Number(+)
	AND a.Closed_By = e3.Employee_Number(+)
); 
commit;

-- add actions opened function
CREATE OR REPLACE FUNCTION "CATSDBA"."ACTIONS_OPENED" (p_reportid number,p_origin varchar2) return number
AS
cursor inctype is
SELECT
	count(A.Action_Id) Actions_Opened
FROM
	tblAction_Details A
WHERE
	A.Origin_Table = p_origin
	AND A.Report_Id = p_reportid
	AND lower(A.Status) = 'open'
;
b inctype%ROWTYPE;
iValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	iValue := b.Actions_Opened;
end loop;
close inctype;

return iValue;
END; 
/
commit;



CREATE OR REPLACE VIEW "CATSDBA"."VIEW_AUDITS" ("RECORD_TYPE","DATE_INSPECTION","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","LOCATION_DOCS","REPORT_ID","REGISTER_ORIGIN","AUDIT_NOTES","SITE_ID","DEPARTMENT_ID","STRDATE","ACTIONS_CREATED","ACTIONS_CLOSED") AS (
SELECT
	a.Record_Type,
	a.Date_Inspection,
	s.Site_Description,
	d.Department_Description,
	a.Location_Docs,
	a.Report_Id,
	a.Register_Origin,
	a.Audit_Notes,
	a.Site_Id,
	a.Department_Id,
	TO_CHAR(a.Date_Inspection) StrDate,
	actions_created(a.Report_Id,'Originating_Audit') actions_created,
	actions_closed(a.Report_Id,'Originating_Audit') actions_closed
FROM
	tblDepartment d,
	tblSite s,
	tblAudits a
WHERE
	s.Site_Id = a.Site_Id
	AND d.Department_Id = a.Department_Id
);
commit;

-- PCR STUFF
-- modify pcr make permanent, extend permanent
alter table tblpcr_details add(tmpmake varchar2(3), tmpextend varchar2(3));
update tblpcr_details set tmpmake='Yes' where make_permanent = 1;
update tblpcr_details set tmpextend='Yes' where extend_permanent = 1;
update tblpcr_details set tmpmake='No' where make_permanent = 0;
update tblpcr_details set tmpextend='No' where extend_permanent = 0;
alter table tblpcr_details drop column make_permanent;
alter table tblpcr_details drop column extend_permanent;
alter table tblpcr_details rename column tmpmake to make_permanent;
alter table tblpcr_details rename column tmpextend to extend_permanent;
commit;

-- PCR Functions
CREATE OR REPLACE FUNCTION "CATSDBA"."PCR_REASONFORCHANGE" (p_pcrid number, p_category varchar2) return varchar2
AS
cursor inctype is
SELECT
	distinct C.checkbox_desc 
FROM
	tblincidentform_checkboxes C,
	tblpcr_checkboxvalues CV
WHERE
	CV.checkboxid = C.checkbox_id
	and CV.checkboxtype = p_category
	and CV.pcrid = p_pcrid
;

a inctype%ROWTYPE;
mystring varchar2(200) DEFAULT NULL;
BEGIN
open inctype;
loop
	fetch inctype into a;
	exit when inctype%NOTFOUND;
	if (mystring is NULL) then
	mystring := a.checkbox_desc;
	else
	mystring := mystring || ', ' || a.checkbox_desc;
	end if;
end loop;
close inctype;

return mystring;
END;
/
commit;

-- PCR Search view

-- PCR Main View
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_DETAILS" ("PCR_ID","PCR_DISPLAYID","CRITICALITY_ID","PCR_TYPE","CHANGETYPE_ID","ORIGINATOR_ID","EMPLOYEE_NO","SITE_ID","DEPARTMENT_ID","SECTION_ID","POSITION_ID","CREW_ID","PCR_DATE","OPERATION_ID","TITLE","DESCRIPTION","AREA_ID","AREA_DESCRIPTION","EQUIPMENT_ID","AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2","WO_NUMBER","EVALUATION_ID","INITIALREVIEW_REQUIRED","MEETING_DATE","PROCESS_STEP","PROCESSSTEP_STATUS","OVERALL_STATUS","STATUS","UNIT","REASSIGNEDTO_ID","COMMENTS","ORIGINATORNAME","AUTHORITIYNAME_1","AUTHORITIYNAME_2","EVALUATIONNAME","REASSIGNEDTO","IMPACTLEVEL","TYPEOFCHANGE","OPERATION","CRITICALITY","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","POSITION_DESCRIPTION","SECTION_DESCRIPTION","TCP","ACTIONS_OPEN","ACTIONS_CLOSED","REASON_FOR_CHANGE","MAKE_PERMANENT","DATE_MAKE_PERMANENT","EXTEND_PERMANENT","DATE_EXTEND_PERMANENT","DUE_DATE") AS (
SELECT
	p.PCR_Id,
	p.PCR_DisplayId,
	p.Criticality_Id,
	p.PCR_Type,
	p.ChangeType_Id,
	p.Originator_Id,
	p.Employee_No,
	p.Site_Id,
	p.Department_Id,
	p.Section_Id,
	p.Position_Id,
	p.Crew_Id,
	p.PCR_Date,
	p.Operation_Id,
	p.Title,
	p.Description,
	p.Area_Id,
	ar.area_description,
	p.Equipment_Id,
	p.AuthorityId_1,
	p.AuthorityId_2,
	p.AuthorityStatus_1,
	p.AuthorityStatus_2,
	p.AuthorityDate_1,
	p.AuthorityDate_2,
	p.authoritycomment_1,
	p.authoritycomment_2,
	p.WO_Number,
	p.Evaluation_Id,
	p.InitialReview_Required,
	p.Meeting_Date,
	p.Process_Step,
	p.ProcessStep_Status,
	p.Overall_Status,
	p.Overall_Status||'-'||p.Process_Step||'-'||p.ProcessStep_Status Status,
	p.Unit,
	p.ReAssignedTo_Id,
	p.Comments,
	e1.Last_Name || ', ' || e1.First_Name AS OriginatorName,
	e2.Last_Name || ', ' || e2.First_Name AS AuthoritiyName_1,
	e3.Last_Name || ', ' || e3.First_Name AS AuthoritiyName_2,
	e4.Last_Name || ', ' || e4.First_Name AS EvaluationName,
	e5.Last_Name || ', ' || e5.First_Name AS ReassignedTo,
	r1.RefTable_Description AS ImpactLevel,
	r2.RefTable_Description AS TypeOfChange,
	r3.RefTable_Description AS Operation,
	r4.RefTable_Comment AS Criticality,
	s.Site_Description,
	d.Department_Description,
	o.Position_Description,
	s2.Section_Description,
	p.tcp,
	ACTIONS_OPENED(p.pcr_id, 'ORIGINATING_PCR_ACTION') ACTIONS_OPEN,
	ACTIONS_CLOSED(p.pcr_id, 'ORIGINATING_PCR_ACTION') ACTIONS_CLOSED,
	PCR_REASONFORCHANGE(p.pcr_id, 'Reason_For_Request') Reason_For_Change,
  p.MAKE_PERMANENT,
  p.DATE_MAKE_PERMANENT,
  p.EXTEND_PERMANENT,
  p.DATE_EXTEND_PERMANENT,
	so.due_date
FROM
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblEmployee_Details e4,
  tblEmployee_Details e5,
  tblRefTable r1,
  tblRefTable r2,
  tblRefTable r3,
	tblRefTable r4,
  tblSite s,
  tblDepartment d,
  tblPosition o,
  tblSection s2,
  tblPCR_Details p,
	tblPCR_Area ar,
	tblPCR_Signoff so
WHERE
  e1.Employee_Number = p.Originator_Id
  and e2.Employee_Number(+) = p.AuthorityId_1
  and e3.Employee_Number(+) = p.AuthorityId_2
  and e4.Employee_Number(+) = p.Evaluation_Id
  and e5.Employee_Number(+) = p.ReAssignedTo_Id
  and r1.RefTable_Id(+) = p.Criticality_Id
  and r2.RefTable_Id(+) = p.ChangeType_Id
  and r3.RefTable_Id(+) = p.Operation_Id
	and r4.RefTable_Id(+) = p.Criticality_Id
  and s.Site_Id = p.Site_Id
  and d.Department_Id = p.Department_Id
  and s2.Section_Id(+) = p.Section_Id
  and o.Position_Id(+) = p.Position_Id
	and ar.Area_Id(+) = p.Area_Id
	and so.PCR_ID = p.PCR_ID
);
commit;

-- pcr initial review VIEW_PCR_INITIALREVIEW

-- pcr initial review details
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_INITIALREVIEW_DETAILS" ("DETAIL_ID","SITE_ID","REVIEW_ID","REVIEWER_TYPE","CHECKLIST_LOC","FOLDER_LOC","REQUIREMENT","PASSWORD_FLAG","PCR_ID","REVIEWER_ID","DATE_REVIEWED","STATUS","COMMENTS","DOCUMENT_LOC","REQUIRED","REVIEW_TYPE") AS (
SELECT
	distinct r.detail_id,
	r.site,
	r.site_id,
	r.review_id,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag,
	a.PCR_Id,
	a.Reviewer_Id,
	a.Date_Reviewed,
	a.Status,
	a.Comments,
	a.Document_Loc,
	a.Required,
	ref.RefTable_Description Review_Type
FROM
	tblPCR_Reviewer_Detail r,
	tblPCR_InitialReview_Details a,
	tblRefTable ref
WHERE
	r.Detail_Id = a.Reviewer_Detail_Id(+)
	AND ref.RefTable_Id = r.Review_Id
);
commit;

-- pcr evaluation
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_EVALUATION" ("PCR_ID","EVALUATION_ID","ASSESSMENT_SCORE","ASSESSMENT_LEVEL",
"EVALUATION_NAME_ID","EVALUATION_DATE","EVALUATION_COMMENTS","AREA_REVIEW_REQ","TEAM_REVIEW_REQ","EXT_REVIEW_REQ",
"AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2",
"EVALUATION_NAME","AUTHORITIYNAME_1","AUTHORITIYNAME_2","PCR_EVALUATION_NAME","ASSESSMENT_SCORE_VALUE","SITE_ID") AS (
SELECT
	p.PCR_Id,
	p.Evaluation_Id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.EVALUATION_NAME_ID,
	e.EVALUATION_DATE,
	e.EVALUATION_COMMENTS,
	e.AREA_REVIEW_REQ,
	e.TEAM_REVIEW_REQ,
	e.EXT_REVIEW_REQ,
	e.AuthorityId_1,
	e.AuthorityId_2,
	e.AuthorityStatus_1,
	e.AuthorityStatus_2,
	e.AuthorityDate_1,
	e.AuthorityDate_2,
	e.authoritycomment_1,
	e.authoritycomment_2,
	e1.Last_Name || ', ' || e1.First_Name AS EVALUATION_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS AuthoritiyName_1,
	e3.Last_Name || ', ' || e3.First_Name AS AuthoritiyName_2,
	e4.Last_Name || ', ' || e4.First_Name AS PCR_EVALUATION_NAME,
	r1.RefTable_Description AS ASSESSMENT_SCORE_VALUE,
	p.site_id
FROM
	tblPCR_Evaluation e,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblEmployee_Details e4,
  tblRefTable r1,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = e.EVALUATION_NAME_ID
  and e2.Employee_Number(+) = e.AuthorityId_1
  and e3.Employee_Number(+) = e.AuthorityId_2
  and e4.Employee_Number(+) = p.Evaluation_Id
  and r1.RefTable_Id(+) = e.ASSESSMENT_SCORE
  and p.PCR_ID = e.PCR_ID
);
commit;

-- pcr area review
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_AREAREVIEW" ("PCR_ID","PCR_TYPE","MAKE_PERMANENT","DATE_MAKE_PERMANENT",
"EXTEND_PERMANENT","DATE_EXTEND_PERMANENT","ACTION","COMMENTS","DUE_BY","DUE_DATE","SUPERVISOR_ID","AFE_NUMBER",
"AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2",
"SUPERVISOR_NAME","AUTHORITIYNAME_1","AUTHORITIYNAME_2","SITE_ID",
"ASSESSMENT_SCORE","ASSESSMENT_LEVEL","CONFIRM_LEVEL","REVISED_SCORE","CRITICALITY") AS (
SELECT
	p.PCR_Id,
	p.PCR_Type, 
	p.Make_Permanent, 
	p.Date_Make_Permanent, 
	p.Extend_Permanent, 
	p.Date_Extend_Permanent,
	a.ACTION,
	a.COMMENTS,
	a.DUE_BY,
	a.DUE_DATE,
	a.SUPERVISOR_ID,
	a.AFE_NUMBER,
	a.AuthorityId_1,
	a.AuthorityId_2,
	a.AuthorityStatus_1,
	a.AuthorityStatus_2,
	a.AuthorityDate_1,
	a.AuthorityDate_2,
	a.authoritycomment_1,
	a.authoritycomment_2,
	e1.Last_Name || ', ' || e1.First_Name AS SUPERVISOR_NAME,
	a1.Last_Name || ', ' || a1.First_Name AS AuthoritiyName_1,
	a2.Last_Name || ', ' || a2.First_Name AS AuthoritiyName_2,
	p.site_id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.CONFIRM_LEVEL,
	e.REVISED_SCORE,
	r1.RefTable_Description AS Criticality
FROM
	tblPCR_AreaReview a,
	tblPCR_Details p,
  tblEmployee_Details e1,
  tblEmployee_Details a1,
  tblEmployee_Details a2,
	tblPCR_Evaluation e,
	tblRefTable r1
WHERE
  e1.Employee_Number(+) = a.SUPERVISOR_ID
  and a1.Employee_Number(+) = a.AuthorityId_1
  and a2.Employee_Number(+) = a.AuthorityId_2
  and p.PCR_ID = a.PCR_ID
	and e.PCR_ID = a.PCR_ID
	and r1.RefTable_Id(+) = p.Criticality_Id
);
commit;

-- pcr area review details
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_AREAREVIEW_DETAILS" ("DETAIL_ID","SITE_ID","REVIEW_ID","REVIEWER_TYPE","CHECKLIST_LOC","FOLDER_LOC","REQUIREMENT","PASSWORD_FLAG","PCR_ID","REVIEWER_ID","DATE_REVIEWED","STATUS","COMMENTS","DOCUMENT_LOC","REQUIRED","REVIEW_TYPE") AS (
SELECT
	r.detail_id,
	r.site_id,
	r.review_id,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag,
	a.PCR_Id,
	a.Reviewer_Id,
	a.Date_Reviewed,
	a.Status,
	a.Comments,
	a.Document_Loc,
	a.Required,
	ref.RefTable_Description Review_Type
FROM
	tblPCR_Reviewer_Detail r,
	tblPCR_AreaReview_Details a,
	tblRefTable ref
WHERE
	r.Detail_Id = a.Reviewer_Detail_Id(+)
	AND ref.RefTable_Id = r.Review_Id
);
commit;

-- pcr pcrt review VIEW_PCR_PCRTREVIEW

-- pcr pcrt review details
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_PCRTREVIEW_DETAILS" ("DETAIL_ID","SITE_ID","REVIEW_ID","REVIEWER_TYPE","CHECKLIST_LOC","FOLDER_LOC","REQUIREMENT","PASSWORD_FLAG","PCR_ID","REVIEWER_ID","DATE_REVIEWED","STATUS","COMMENTS","DOCUMENT_LOC","REQUIRED","REVIEW_TYPE") AS (
SELECT
	r.detail_id,
	r.site_id,
	r.review_id,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag,
	a.PCR_Id,
	a.Reviewer_Id,
	a.Date_Reviewed,
	a.Status,
	a.Comments,
	a.Document_Loc,
	a.Required,
	ref.RefTable_Description Review_Type
FROM
	tblPCR_Reviewer_Detail r,
	tblPCR_PCRTReview_Details a,
	tblRefTable ref
WHERE
	r.Detail_Id = a.Reviewer_Detail_Id(+)
	AND ref.RefTable_Id = r.Review_Id
);
commit;

-- pcr extension
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_EXTENSION" ("PCR_ID","EVALUATION_ID","ASSESSMENT_SCORE","ASSESSMENT_LEVEL",
"EVALUATION_NAME_ID","EVALUATION_DATE","EVALUATION_COMMENTS","AREA_REVIEW_REQ","TEAM_REVIEW_REQ","EXT_REVIEW_REQ",
"AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2",
"EVALUATION_NAME","AUTHORITIYNAME_1","AUTHORITIYNAME_2","PCR_EVALUATION_NAME","ASSESSMENT_SCORE_VALUE","SITE_ID") AS (
SELECT
	p.PCR_Id,
	p.Evaluation_Id,
	e.ASSESSMENT_SCORE,
	e.ASSESSMENT_LEVEL,
	e.EVALUATION_NAME_ID,
	e.EVALUATION_DATE,
	e.EVALUATION_COMMENTS,
	e.AREA_REVIEW_REQ,
	e.TEAM_REVIEW_REQ,
	e.EXT_REVIEW_REQ,
	e.AuthorityId_1,
	e.AuthorityId_2,
	e.AuthorityStatus_1,
	e.AuthorityStatus_2,
	e.AuthorityDate_1,
	e.AuthorityDate_2,
	e.authoritycomment_1,
	e.authoritycomment_2,
	e1.Last_Name || ', ' || e1.First_Name AS EVALUATION_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS AuthoritiyName_1,
	e3.Last_Name || ', ' || e3.First_Name AS AuthoritiyName_2,
	e4.Last_Name || ', ' || e4.First_Name AS PCR_EVALUATION_NAME,
	r1.RefTable_Description AS ASSESSMENT_SCORE_VALUE,
	p.site_id
FROM
	tblPCR_Evaluation e,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblEmployee_Details e4,
  tblRefTable r1,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = e.EVALUATION_NAME_ID
  and e2.Employee_Number(+) = e.AuthorityId_1
  and e3.Employee_Number(+) = e.AuthorityId_2
  and e4.Employee_Number(+) = p.Evaluation_Id
  and r1.RefTable_Id(+) = e.ASSESSMENT_SCORE
  and p.PCR_ID = e.PCR_ID
);
commit;


-- pcr extension details
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_EXTENSION_DETAILS" ("DETAIL_ID","SITE_ID","REVIEW_ID","REVIEWER_TYPE","CHECKLIST_LOC","FOLDER_LOC","REQUIREMENT","PASSWORD_FLAG","PCR_ID","REVIEWER_ID","DATE_REVIEWED","STATUS","COMMENTS","DOCUMENT_LOC","REQUIRED","REVIEW_TYPE") AS (
SELECT
	r.detail_id,
	r.site_id,
	r.review_id,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag,
	a.PCR_Id,
	a.Reviewer_Id,
	a.Date_Reviewed,
	a.Status,
	a.Comments,
	a.Document_Loc,
	a.Required,
	ref.RefTable_Description Review_Type
FROM
	tblPCR_Reviewer_Detail r,
	tblPCR_Extension_Details a,
	tblRefTable ref
WHERE
	r.Detail_Id = a.Reviewer_Detail_Id(+)
	AND ref.RefTable_Id = r.Review_Id
);
commit;

-- pcr actions
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_DEFAULT_ACTIONS" ("ACTION_ID","SITE_ID","SITE_DESCRIPTION","MANAGED_BY_ID","MANAGED_BY","ACTION_DESCRIPTION","DAYS_AFTER_PREV_STAGE") AS (
SELECT 
	a.Action_id, 
	a.Site_Id, 
	s.site_description,
	a.managed_by_id, 
	(e.last_name || ', ' || e.first_name) as managed_by, 
	a.action_description, 
	a.days_after_prev_stage
FROM
	tblPCR_Action a, 
	tblEmployee_details e,
	tblsite s
WHERE
	a.managed_by_id = e.employee_number
	and a.site_id = s.site_id
);
commit;

-- pcr sign off
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_SIGNOFF" ("PCR_ID","TYPE_OF_REVIEW","MEETING_DATE","MEETING_COMMENTS",
"DUE_DATE","AUDITBY_ID","SIGN_OFF","SIGNED_OFF_DATE",
"ENGINEER_ID","ENGINEER_APPROVE","ENGINEER_DATE",
"SUPER_ID","SUPER_APPROVE","SUPER_DATE",
"AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2",
"AUTHORITIYNAME_1","AUTHORITIYNAME_2","ENGINEER_NAME","SUPER_NAME","AUDITBY_NAME","SITE_ID") AS (
SELECT
	p.PCR_Id,
	s.TYPE_OF_REVIEW,
	s.MEETING_DATE,
	s.MEETING_COMMENTS,
	s.DUE_DATE,
	s.AUDITBY_ID,
	s.SIGN_OFF,
	s.SIGNED_OFF_DATE,
	s.ENGINEER_ID,
	s.ENGINEER_APPROVE,
	s.ENGINEER_DATE,
	s.SUPER_ID,
	s.SUPER_APPROVE,
	s.SUPER_DATE,
	s.AuthorityId_1,
	s.AuthorityId_2,
	s.AuthorityStatus_1,
	s.AuthorityStatus_2,
	s.AuthorityDate_1,
	s.AuthorityDate_2,
	s.authoritycomment_1,
	s.authoritycomment_2,
	a1.Last_Name || ', ' || a1.First_Name AS AuthoritiyName_1,
	a2.Last_Name || ', ' || a2.First_Name AS AuthoritiyName_2,
	e1.Last_Name || ', ' || e1.First_Name AS ENGINEER_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS SUPER_NAME,
	e3.Last_Name || ', ' || e3.First_Name AS AUDITBY_NAME,
	p.site_id
FROM
	tblPCR_Signoff s,
  tblEmployee_Details a1,
  tblEmployee_Details a2,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblEmployee_Details e3,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = s.ENGINEER_ID
	and e2.Employee_Number(+) = s.SUPER_ID
  and a1.Employee_Number(+) = s.AuthorityId_1
  and a2.Employee_Number(+) = s.AuthorityId_2
  and e3.Employee_Number(+) = s.AUDITBY_ID
  and p.PCR_ID = s.PCR_ID
);
commit;

-- pcr sign off details
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_SIGNOFF_DETAILS" ("DETAIL_ID","SITE_ID","REVIEW_ID","REVIEWER_TYPE","CHECKLIST_LOC","FOLDER_LOC","REQUIREMENT","PASSWORD_FLAG","PCR_ID","REVIEWER_ID","DATE_REVIEWED","STATUS","COMMENTS","DOCUMENT_LOC","REQUIRED","REVIEW_TYPE") AS (
SELECT
	r.detail_id,
	r.site_id,
	r.review_id,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag,
	a.PCR_Id,
	a.Reviewer_Id,
	a.Date_Reviewed,
	a.Status,
	a.Comments,
	a.Document_Loc,
	a.Required,
	ref.RefTable_Description Review_Type
FROM
	tblPCR_Reviewer_Detail r,
	tblPCR_SignOff_Details a,
	tblRefTable ref
WHERE
	r.Detail_Id = a.Reviewer_Detail_Id(+)
	AND ref.RefTable_Id = r.Review_Id
);
commit;


-- pcr post audit
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_POSTAUDIT" ("PCR_ID","AUDIT_COMMENTS","AUDIT_NAME_ID","AUDIT_DATE",
"AUDIT_COMPLETED_BY_ID","AUDIT_NAME","AUDIT_COMPLETED_BY","SITE_ID") AS (
SELECT
	p.PCR_Id,
	a.AUDIT_COMMENTS,
	a.AUDIT_NAME_ID,
	a.AUDIT_DATE,
	a.AUDIT_COMPLETED_BY_ID,
	e1.Last_Name || ', ' || e1.First_Name AS AUDIT_NAME,
	e2.Last_Name || ', ' || e2.First_Name AS AUDIT_COMPLETED_BY,
	p.site_id
FROM
	tblPCR_PostAudit a,
  tblEmployee_Details e1,
  tblEmployee_Details e2,
  tblPCR_Details p
WHERE
  e1.Employee_Number(+) = a.AUDIT_NAME_ID
	and e2.Employee_Number(+) = a.AUDIT_COMPLETED_BY_ID
  and p.PCR_ID = a.PCR_ID
);
commit;


-- pcr default reviewers 
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_REVIEWER_DETAIL" ("DETAIL_ID","SITE_ID","SITE_DESCRIPTION","REVIEW_ID","REVIEW_TYPE","REVIEWER_TYPE","CHECKLIST_LOC","FOLDER_LOC","REQUIREMENT","PASSWORD_FLAG") AS (
SELECT
	r.detail_id,
	r.site_id,
	s.site_description,
	r.review_id,
	ref.reftable_description review_type,
	r.reviewer_type,
	r.checklist_loc,
	r.folder_loc,
	r.requirement,
	r.password_flag
FROM
	tblpcr_reviewer_detail r,
	tblsite s,
	tblreftable ref
WHERE
	s.site_id = r.site_id
	and ref.reftable_id = r.review_id
);
commit;

CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_EMAIL" ("EMAIL_ID","SITE_ID","SITE_DESCRIPTION","SCREEN_ID","SCREEN_NAME","PERSON_DESCRIPTION","SUBJECT","MESSAGE") AS (
SELECT
	e.email_id,
	e.site_id,
	s.site_description,
	e.screen_id,
	t.screen_name,
	e.person_description,
	e.subject,
	e.message
FROM
	tblpcr_email e,
	tblsite s,
	tblpcr_tabs t
WHERE
	s.site_id = e.site_id
	and t.screen_id = e.screen_id
);
commit;

CREATE OR REPLACE VIEW "CATSDBA"."VIEW_PCR_DEFAULTNAMES" ("DETAIL_ID","AREA_ID","REVIEWER_ID","AREA_DESCRIPTION","EMP_NAME","SITE_ID") AS (
SELECT
	r.detail_id,
	r.area_id,
	r.reviewer_id,
	a.area_description,
	e.last_name || ', ' || e.first_name emp_name,
	a.site_id
FROM
	tblPCR_Reviewer_Name r,
	tblPCR_Area a,
	tblEmployee_Details e
WHERE
	r.area_id = a.area_id
	and r.reviewer_id = e.employee_number
);
commit;

-- create the pcr action originating view
CREATE OR REPLACE VIEW "CATSDBA"."ORIGINATING_PCR_ACTION" ("TYPE","REPORT_ID","DISPLAY_ID","SITE","DEPARTMENT","REPORT_DATE","EMPLOYEE","DETAILS") AS (
SELECT 
	'PCR Action' Type, 
	p.PCR_ID Report_Id, 
	p.PCR_DISPLAYID Display_Id,  
	p.SITE_DESCRIPTION Site,  
	p.DEPARTMENT_DESCRIPTION Department,  
	p.PCR_DATE Report_Date,  
	p.ORIGINATORNAME Employee,  
	p.TITLE Details 
FROM 
	View_PCR_Details p 
);
commit;
-- end PCR Stuff



-- update incident_details 
-- convert incident_date and report_date to timestamp datatype
alter table tblincident_details modify(incident_date timestamp, report_date timestamp);
-- incident_time to incident_date etc...
update tblincident_details set report_time = '00:00' where report_time like '24%';
update tblincident_details set incident_time = '00:00' where incident_time like '24%';

-- use the following to troubleshoot stupid time entries
--select incident_number, incident_time, report_time from tblincident_details where incident_time like '%.%' or report_time like '%.%';
--select incident_time from tblincident_details where incident_number = 32163
--select report_time from tblincident_details where incident_number = 32163
--update tblincident_details set incident_time = '01:50' where incident_number = 32163;
--update tblincident_details set report_time = '02:50' where incident_number = 32163;

-- update incident_date, report_date to include time from respective time fields
update tblincident_details set 
	incident_date = to_date(to_char(incident_date,'YYYY-MM-DD')||' '||incident_time,'YYYY-MM-DD HH24:MI'),
	report_date = to_date(to_char(report_date,'YYYY-MM-DD')||' '||report_time,'YYYY-MM-DD HH24:MI')

-- re-create search view to reflect changes in the incident_details table
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_INCIDENT_SEARCHVIEW" ("COMPANY","REALDATE","PRIMARYPERSONID","PRIMARYCOMPANYID","REPORTEDCOMPANYID","INJUREDCOMPANYID","INCDISPLYID","INCIDENTSTATUS","DEPARTMENT_ID","ESTIMATED_COST","INCIDENT_DESCRIPTION","INCIDENT_NUMBER","INCIDENT_REPORT_STATUS_ID","INITIALRISKCATEGORY","INITIALRISKLEVEL","INITIATED_BY_ID","INJURED_PERSON_COMPANY","INJURED_PERSON_ID","INJURY_STATUS","RECALCRISKCATEGORY","RECALCRISKLEVEL","RECALC_RISK_RESULT","REPORTED_TO","REPORT_TIME","SEVERITY_RATING","SITE_ID","SUBSTANTIATED","SUPERINTENDENT_ID","INCIDENT_DATE","REPORT_DATE","INCIDENT_CATEGORIES","CREATEDACTIONS","CLOSEDACTIONS","TYPE_OF_EMPLOYEE") AS (
SELECT
	I.COMPANY,
	I.INCIDENT_DATE RealDate,
	I.PRIMARYPERSONID,
	I.PRIMARYCOMPANYID,
	I.REPORTEDCOMPANYID,
	I.INJUREDCOMPANYID,
	I.INCDISPLYID,
	I.INCIDENTSTATUS,
	I.DEPARTMENT_ID,
	I.ESTIMATED_COST,
	I.INCIDENT_DESCRIPTION,
	I.INCIDENT_NUMBER,
	I.INCIDENT_REPORT_STATUS_ID,
	I.INITIALRISKCATEGORY,
	I.INITIALRISKLEVEL,
	I.INITIATED_BY_ID,
	I.INJURED_PERSON_COMPANY,
	I.INJURED_PERSON_ID,
	I.INJURY_STATUS,
	I.RECALCRISKCATEGORY,
	I.RECALCRISKLEVEL,
	I.RECALC_RISK_RESULT,
	I.REPORTED_TO,
	to_char(I.REPORT_DATE,'HH24:MI') REPORT_TIME,
	I.SEVERITY_RATING,
	I.SITE_ID,
	I.SUBSTANTIATED,
	I.SUPERINTENDENT_ID,
	To_Char (I.INCIDENT_DATE,'DD/MM/YYYY') INCIDENT_DATE,
	To_Char (I.REPORT_DATE,'DD/MM/YYYY') REPORT_DATE,
	getstring(I.INCIDENT_NUMBER,'IncidentCategory') incident_categories,
	ACTIONS_CREATED(I.INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS,
	ACTIONS_CLOSED(I.INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS,
	E.TYPE_OF_EMPLOYEE
FROM
	TBLINCIDENT_DETAILS I,
	TBLEMPLOYEE_DETAILS E
WHERE
	I.INITIATED_BY_ID = E.EMPLOYEE_NUMBER
);
commit;

-- add functions to get checked checkbox value
CREATE OR REPLACE FUNCTION "CATSDBA"."INCIDENT_CHECKED" (p_id number,p_type varchar2) return number
AS
cursor inctype is
SELECT 
	count(I.IncidentId) as CHECKED
FROM 
	tblIncident_Checkboxvalues I 
WHERE 
	I.IncidentId = p_id 
	AND I.CheckboxType = p_type
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END; 
/
commit;


-- add functions to get a list of all checked checkbox values
-- injuries
CREATE OR REPLACE FUNCTION "CATSDBA"."INJURIES_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT 
	count(I.IncidentId) as CHECKED
FROM 
	tblIncident_Checkboxvalues I 
WHERE 
	I.IncidentId = p_id 
	AND I.CheckboxType in ('InjuryClassification','InjuryLocation','InjuryMechanism','InjuryType','InjuryAgency')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END; 
/
commit;


-- environment
CREATE OR REPLACE FUNCTION "CATSDBA"."ENVIRONMENT_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT 
	count(I.IncidentId) as CHECKED
FROM 
	tblIncident_Checkboxvalues I 
WHERE 
	I.IncidentId = p_id 
	AND I.CheckboxType in ('EnviroClassification','EnviroImpactTo','EnviroImpactBy')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END; 
/
commit;


-- community
CREATE OR REPLACE FUNCTION "CATSDBA"."COMMUNITY_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT 
	count(I.IncidentId) as CHECKED
FROM 
	tblIncident_Checkboxvalues I 
WHERE 
	I.IncidentId = p_id 
	AND I.CheckboxType in ('CommunityClassification')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END; 
/
commit;


-- quality
CREATE OR REPLACE FUNCTION "CATSDBA"."QUALITY_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT 
	count(I.IncidentId) as CHECKED
FROM 
	tblIncident_Checkboxvalues I 
WHERE 
	I.IncidentId = p_id 
	AND I.CheckboxType in ('QualityClassification')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END; 
/
commit;


-- economic
CREATE OR REPLACE FUNCTION "CATSDBA"."ECONOMIC_CHECKED" (p_id number) return number
AS
cursor inctype is
SELECT 
	count(I.IncidentId) as CHECKED
FROM 
	tblIncident_Checkboxvalues I 
WHERE 
	I.IncidentId = p_id 
	AND I.CheckboxType in ('Economic')
;
b inctype%ROWTYPE;
intValue NUMBER;
BEGIN

open inctype;
loop
	fetch inctype into b;
	exit when inctype%NOTFOUND;
	intValue := b.CHECKED;
end loop;
close inctype;

return intValue;
END; 
/
commit;

-- re-create the incidents report print view to reflect change in dates/time columns
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_INCIDENT_PRINTVIEW" ("INCDISPLYID","PRIMARYPERSONID","PRIMARYCOMPANYID","REPORTEDCOMPANYID","INJUREDCOMPANYID","INCIDENTSTATUS","AREA","BODY_PART","CAUSE_OF_INJURY","COMPANY","CURRENT_LOCATION","DEPARTMENT_ID","ESTIMATED_COST","IMMEDIATE_CORRECTIVE_ACTIONS","INCIDENT_DESCRIPTION","INCIDENT_NUMBER","INCIDENT_REPORT_STATUS_ID","INCIDENT_TIME","INCIDENT_TITLE","INCIDENT_TYPE","INITIALRISKCATEGORY","INITIALRISKLEVEL","INITIATED_BY_ID","INJURED_PERSON_COMPANY","INJURED_PERSON_GENDER","INJURED_PERSON_ID","INJURY_STATUS","INVESTIGATION_COMPLETION_DATE","INVESTIGATION_REQUIRED","LOCATION","MECHANISM_OF_INJURY","PERSONS_INVOLVED_DESCRIPTION","RECALCRISKCATEGORY","RECALCRISKLEVEL","RECALC_RISK_RESULT","REPORTED_TO","INCIDENT_DATE","REPORT_DATE","REPORT_TIME","SEVERITY_RATING","SITE_ID","SUBSTANTIATED","SUPERINTENDENT_ID","INCIDENT_CATEGORIES","INCIDENT_REPORTTYPE","INCIDENT_IMPACTBY","INCIDENT_IMPACTTO","INCIDENT_INJURYTYPE","INCIDENT_INJURYMECHANISM","INCIDENT_INJURYAGENCY","INCIDENT_INJURYLOCATION","INCIDENT_INJURYCLASS","INCIDENT_ENVIROCLASS","INCIDENT_COMMUNITYCLASS","INCIDENT_QUALITYCLASS","INCIDENT_REPORTEDTO","INCIDENT_SYSTEMICCAUSE","INCIDENT_BEHAVIOURCAUSE","INCIDENT_ECONOMIC","DEPARTMENT","SITE","INJURED_PERSON","SUPERINTENDENT","INITIATED_BY","TYPE_OF_EMPLOYEE","REPORTEDTO","PRIMARYPERSON","RECALC_RISKCAT","RECALC_RISKLEVEL","INIT_RISKCAT","INIT_RISKLEVEL","REPORTEDBYCOMPANY","INJUREDCOMPANY","PRIMARYCOMPANY","CREATEDACTIONS","CLOSEDACTIONS") AS (
SELECT
	I.INCDISPLYID,
	I.PRIMARYPERSONID,
	I.PRIMARYCOMPANYID,
	I.REPORTEDCOMPANYID,
	I.INJUREDCOMPANYID,
	I.INCIDENTSTATUS,
	I.AREA,
	I.BODY_PART,
	I.CAUSE_OF_INJURY,
	I.COMPANY,
	I.CURRENT_LOCATION,
	I.DEPARTMENT_ID,
	I.ESTIMATED_COST,
	I.IMMEDIATE_CORRECTIVE_ACTIONS,
	I.INCIDENT_DESCRIPTION,
	I.INCIDENT_NUMBER,
	I.INCIDENT_REPORT_STATUS_ID,
	to_char(I.INCIDENT_DATE,'HH24:MI') INCIDENT_TIME,
	I.INCIDENT_TITLE,
	I.INCIDENT_TYPE,
	I.INITIALRISKCATEGORY,
	I.INITIALRISKLEVEL,
	I.INITIATED_BY_ID,
	I.INJURED_PERSON_COMPANY,
	I.INJURED_PERSON_GENDER,
	I.INJURED_PERSON_ID,
	I.INJURY_STATUS,
	I.INVESTIGATION_COMPLETION_DATE,
	I.INVESTIGATION_REQUIRED,
	I.LOCATION,
	I.MECHANISM_OF_INJURY,
	I.PERSONS_INVOLVED_DESCRIPTION,
	I.RECALCRISKCATEGORY,
	I.RECALCRISKLEVEL,
	I.RECALC_RISK_RESULT,
	I.REPORTED_TO,
	To_Char (I.INCIDENT_DATE,'DD/MM/YYYY') INCIDENT_DATE,
	To_Char (I.REPORT_DATE,'DD/MM/YYYY') REPORT_DATE,
	to_char(I.REPORT_DATE,'HH24:MI') REPORT_TIME,
	I.SEVERITY_RATING,
	I.SITE_ID,
	I.SUBSTANTIATED,
	I.SUPERINTENDENT_ID,
	getstring(I.INCIDENT_NUMBER,'IncidentCategory') incident_categories,
	getstring(I.INCIDENT_NUMBER,'IncidentReportType') incident_reporttype,
	getstring(I.INCIDENT_NUMBER,'EnviroImpactBy') incident_impactby,
	getstring(I.INCIDENT_NUMBER,'EnviroImpactTo') incident_impactto,
	getstring(I.INCIDENT_NUMBER,'InjuryType') incident_injurytype,
	getstring(I.INCIDENT_NUMBER,'InjuryMechanism') incident_injurymechanism,
	getstring(I.INCIDENT_NUMBER,'InjuryAgency') incident_injuryagency,
	getstring(I.INCIDENT_NUMBER,'InjuryLocation') incident_injurylocation,
	getstring(I.INCIDENT_NUMBER,'InjuryClassification') incident_injuryclass,
	getstring(I.INCIDENT_NUMBER,'EnviroClassification') incident_enviroclass,
	getstring(I.INCIDENT_NUMBER,'CommunityClassification') incident_communityclass,
	getstring(I.INCIDENT_NUMBER,'QualityClassification') incident_qualityclass,
	getstring(I.INCIDENT_NUMBER,'ReportedTo') incident_reportedto,
	getstring(I.INCIDENT_NUMBER,'SystemicCause') incident_SystemicCause,
	getstring(I.INCIDENT_NUMBER,'BehaviourCause') incident_BehaviourCause,
	getstring(I.INCIDENT_NUMBER,'Economic') incident_Economic,
	D.DEPARTMENT_DESCRIPTION DEPARTMENT,
	S.SITE_DESCRIPTION SITE,
	E1.LAST_NAME || ', ' || E1.FIRST_NAME INJURED_PERSON,
	E2.LAST_NAME || ', ' || E2.FIRST_NAME SUPERINTENDENT,
	E3.LAST_NAME || ', ' || E3.FIRST_NAME INITIATED_BY,
	E3.TYPE_OF_EMPLOYEE,
	E4.LAST_NAME || ', ' || E4.FIRST_NAME REPORTEDTO,
	E5.LAST_NAME || ', ' || E5.FIRST_NAME PRIMARYPERSON,
	R1.REFTABLE_DESCRIPTION RECALC_RISKCAT,
	R2.REFTABLE_DESCRIPTION RECALC_RISKLEVEL,
	R3.REFTABLE_DESCRIPTION INIT_RISKCAT,
	R4.REFTABLE_DESCRIPTION INIT_RISKLEVEL,
	C1.COMPANYNAME REPORTEDBYCOMPANY,
	C2.COMPANYNAME INJUREDCOMPANY,
	C3.COMPANYNAME PRIMARYCOMPANY,
	ACTIONS_CREATED(I.INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS,
	ACTIONS_CLOSED(I.INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS
FROM
	TBLINCIDENT_DETAILS I,
	TBLSITE S,
	TBLDEPARTMENT D,
	TBLEMPLOYEE_DETAILS E1,
	TBLEMPLOYEE_DETAILS E2,
	TBLEMPLOYEE_DETAILS E3,
	TBLEMPLOYEE_DETAILS E4,
	TBLEMPLOYEE_DETAILS E5,
	TBLREFTABLE R1,
	TBLREFTABLE R2,
	TBLREFTABLE R3,
	TBLREFTABLE R4,
	TBLCOMPANIES C1,
	TBLCOMPANIES C2,
	TBLCOMPANIES C3
WHERE
	I.INJURED_PERSON_ID = E1.EMPLOYEE_NUMBER(+)
	AND I.SUPERINTENDENT_ID = E2.EMPLOYEE_NUMBER
	AND I.INITIATED_BY_ID = E3.EMPLOYEE_NUMBER
	AND I.REPORTED_TO = E4.EMPLOYEE_NUMBER
	AND I.PRIMARYPERSONID = E5.EMPLOYEE_NUMBER
	AND I.SITE_ID = S.SITE_ID
	AND D.DEPARTMENT_ID = I.DEPARTMENT_ID
	AND I.RECALCRISKCATEGORY = R1.REFTABLE_ID(+)
	AND I.RECALCRISKLEVEL = R2.REFTABLE_ID(+)
	AND R3.REFTABLE_ID(+) = I.INITIALRISKCATEGORY
	AND R4.REFTABLE_ID(+) = I.INITIALRISKLEVEL
	AND C1.COMPANYID(+) = I.REPORTEDCOMPANYID
	AND C2.COMPANYID(+) = I.INJUREDCOMPANYID
	AND C3.COMPANYID(+) = I.PRIMARYCOMPANYID
);
commit;


-- add sort order to incident checkboxes
alter table tblIncidentForm_Checkboxes add(sort_order number);
commit;

-- EMPLOYEE STUFF HERE
-- add site_access column to eventually replace viewable_sites column
-- add induction_number for user defined employee_id ? sad but true
alter table tblemployee_details add(SITE_ACCESS VARCHAR2(50), INDUCTION_NUMBER NUMBER);
commit;
-- update induction_number to a default value of employee_number
update tblemployee_details set induction_number = employee_number;
commit;

-- REPLACE EMPLOYEE_DETAILS VIEW
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_EMPLOYEE_DETAILS" ("EMPLOYEE_NUMBER","INDUCTION_NUMBER","FIRST_NAME","LAST_NAME","SITE_ID","DEPARTMENT_ID","POSITION","INDUCTION_DATE","CAN_CHANGE_DUE_DATE","INCIDENT_PRIVILEGED_USER","ACTION_PRIVILEGED_USER","TYPE_OF_EMPLOYEE","CONTRACTOR_COMPANY","EMAIL_ADDRESS","TERMINATION_DATE","ACCESS_ID","GROUP_NAME","GROUP_MASK","PASSWORD","USER_STATUS","VIEWABLE_SITES","SITE_ACCESS","EMP_NAME","COMPANYID","SECTION_ID","VERSION","TERMINATION_DATE_STRING","INDUCTION_DATE_STRING","COMPANYNAME","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","POSITION_NAME","SECTION_DESCRIPTION") AS (
SELECT
	e.Employee_Number,
	e.INDUCTION_NUMBER,
	e.First_Name,
	e.Last_Name,
	e.Site_Id,
	e.Department_Id,
	e.Position,
	e.Induction_Date,
	e.Can_Change_Due_Date,
	e.Incident_Privileged_User,
	e.Action_Privileged_User,
	e.Type_Of_Employee,
	e.Contractor_Company,
	e.Email_Address,
	e.Termination_Date,
	e.Access_Id,
	e.Group_Name,
	e.GROUP_MASK,
	e.Password,
	e.User_Status,
	e.Viewable_Sites,
	e.SITE_ACCESS,
	e.Last_Name || ', ' || e.First_Name Emp_Name,
	e.CompanyId,
	e.Section_Id,
	e.Version,
	To_Char(e.Termination_Date) Termination_Date_String,
	To_Char(e.Induction_Date) Induction_Date_String,
	c.CompanyName,
	s.Site_Description,
	d.Department_Description,
	p.Position_Description Position_Name,
	a.Section_Description
FROM
	tblEmployee_Details e,
	tblSite s,
	tblDepartment d,
	tblPosition p,
	tblCompanies c,
	tblSection a
WHERE
	e.Site_Id = s.Site_Id
	AND e.Department_Id = d.Department_Id
	AND e.Position = p.Position_Id(+)
	AND e.CompanyId = c.CompanyId(+)
	AND e.Section_Id = a.Section_Id(+)
);
commit;

CREATE INDEX catsdba.pk_employee_lname on catsdba.tblemployee_details 
(
  last_name
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
-- Create a temp table for employee access mapping
-- This table may be deleted after the import.php script is run
DROP INDEX catsdba.pk_ug_id;
DROP TABLE catsdba.ug;
DROP SEQUENCE catsdba.ug_seq;
commit;

CREATE TABLE catsdba.ug (
  ug_id NUMBER NOT NULL,
  ug_name varchar2(64) default NULL,
  ug_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_ug_id on catsdba.ug 
(
  ug_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.ug_seq;
CREATE OR REPLACE TRIGGER catsdba.ug_trg
	BEFORE INSERT ON catsdba.ug
	FOR EACH ROW
	BEGIN
		SELECT catsdba.ug_seq.nextval INTO :new.ug_id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Super Administrator', 524288+1024+512+32+16+8+4+2, 10);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Administrators', 1024+512+32+16+8+4+2, 1);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Admin Assistants', 512+32+16+8+4+2, 7);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Incident Analysis', 32+16+8+4+2, 2);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Edit (Privileged)', 16+8+4+2, 5);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Edit (Restricted)', 8+4+2, 4);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Reader (Privileged)', 4+2, 3);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('Reader', 2, 9);
INSERT INTO catsdba.ug (ug_name, ug_mask, status) VALUES ('No Access', 0, 8);
commit;

-- add group mask and role mask to tblemployee_details
alter table catsdba.tblemployee_details add(group_mask number, role_mask number);
commit;
-- update rolw_mask for multiple roles access
update catsdba.tblemployee_details e set e.role_mask = 0;
commit;
-- update group_mask for multiple groups access
update catsdba.tblemployee_details e set group_mask = (select u.ug_mask from catsdba.ug u where u.ug_name=e.group_name);
commit;
-- update me to god like status ;)
--update catsdba.tblemployee_details e set group_mask = 9977086 where upper(access_id) = 'VIANET';
update catsdba.tblemployee_details e set group_mask = (select sum(user_group_mask) from user_group) where upper(access_id) = 'VIANET';
commit;
-- update kevin lilje to root admin and everything below
update catsdba.tblemployee_details e set group_mask = (1588478) where upper(access_id) = 'K_LILJE';
commit;
-- update actions privileged user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+2048) where e.action_privileged_user = 1;
commit;
-- update can change action due date user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+4096) where e.CAN_CHANGE_DUE_DATE = 1;
commit;
-- update incidents privileged user settings
update catsdba.tblemployee_details e set e.group_mask = (e.group_mask+8192) where e.incident_privileged_user = 1;
commit;


-- Create Action history table etc...
DROP INDEX catsdba.pk_action_history_id;
DROP TABLE catsdba.action_history;
DROP SEQUENCE catsdba.action_history_seq;
commit;

CREATE TABLE catsdba.action_history (
  id NUMBER NOT NULL,
  action_id NUMBER NOT NULL,
  employee_number NUMBER NOT NULL,
  date_time timestamp default sysdate,
  comments varchar2(1000) default 'No Comment',
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_action_history_id on catsdba.action_history 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_action_history_action_id on catsdba.action_history 
(
  action_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.action_history_seq;
CREATE OR REPLACE TRIGGER catsdba.action_history_trg
	BEFORE INSERT ON catsdba.action_history
	FOR EACH ROW
	BEGIN
		SELECT catsdba.action_history_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

-- update employee_details user status
update status_values set status_title = 'Inactive' where status_title = 'Disabled' and status_type = 'TBLEMPLOYEE_DETAILS';

--testing
--select count(*) from (select distinct access_id from tblemployee_details);
--select count(*) from (select access_id from tblemployee_details);

--select user_status from tblemployee_details where lower(access_id) = 'a_hayward';
--end testing




CREATE OR REPLACE VIEW "CATSDBA"."VIEW_INCIDENT_DETAILS_STRING" ("INCIDENT_NUMBER","INCIDENT_DATE","INCIDENT_TYPE_VALUE","INCIDENT_TYPE_STRING","INCIDENT_DESCRIPTION","INCIDENT_TYPE","ENV_INCIDENT_DETAILS","BODY_PART","MECHANISM_OF_INJURY","INJURY_STATUS_VALUE","INJURED_PERSON_ID","INJURED_PERSON_COMPANY","INJURY_STATUS","EMP_NAME","TYPE_OF_EMPLOYEE","INJURED_SITE_ID","COMPANY","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","SUPER_SITE_ID","INIT_SITE_ID","DEPARTMENT_ID") AS (
SELECT 
	DISTINCT I.Incident_Number,
	To_Char(I.Incident_Date,'DD/MM/YYYY'),
	I.Incident_Type,
	A.Incident_Type_String,
	I.Incident_Description,
	I.Incident_Type,
	I.Env_Incident_Details,
	I.Body_Part,
	I.Mechanism_of_Injury,
	I.Injury_Status,
	I.Injured_Person_Id,
	I.Injured_Person_Company,
	AI.Injury_Status_Value,
	E3.Emp_Name,
	E3.Type_Of_Employee,
	E3.Site_ID Injured_Site_ID,
	E3.Contractor_Company Company,
	E3.Site_Description,
	E3.Department_Description,
	E2.Site_ID Super_Site_ID,
	E.Site_ID Init_Site_ID,
	E3.Department_Id
FROM
	tblIncident_Details I,
	tblIncident_Type T,
	All_Incident_Types A,
	View_Employee_Details E,
	View_Employee_Details E2,
	View_Employee_Details E3,
	All_Injury_Status AI
WHERE 
	I.Incident_Type=A.Incident_Type
	AND (BITAND(I.Incident_Type,T.Incident_Type)+0) > 0
	AND E2.Employee_Number = I.Superintendent_Id
	AND E.Employee_Number = I.Initiated_By_Id
	AND I.Injured_Person_Id = E3.Employee_Number(+)
	AND I.Injury_Status=AI.Injury_Status(+)
);
commit;

-- lost days modifications
alter table tbllost_days add(tmpyears number(4), tmpmonths number(2));
update tbllost_days set tmpyears=years, tmpmonths=months;
alter table tbllost_days drop column years;
alter table tbllost_days drop column months;
alter table tbllost_days rename column tmpyears to years;
alter table tbllost_days rename column tmpmonths to months;
commit;

CREATE OR REPLACE VIEW "CATSDBA"."VIEW_LOST_DAYS" ("LOST_DAYS_ID","YEARS","MONTHS","YEAR_MONTH","LOST_DAYS","RESTRICTED_DAYS","MONTH_NAME","INCIDENT_NUMBER","LOCATION","AREA","INCIDENT_DATE","INJURED_PERSON_ID","INCDISPLYID","SITE_ID","EMPLOYEE_NUMBER","EMP_NAME","SITE_DESCRIPTION") AS (
SELECT
	L.Lost_Days_Id,
	L.Years,
	L.Months,
	L.Year_Month,
	L.Lost_Days,
	L.Restricted_Days,
	initcap(to_char(to_date('2000-' || cast(L.Months as varchar2(2)) || '-1','YYYY-MM-DD'),'MONTH')) Month_Name,
	I.Incident_Number,
	I.Location,
	I.Area,
	I.Incident_Date,
	I.Injured_Person_Id,
	I.IncDisplyId,
	E.Site_Id,
	E.Employee_Number,
	E.Emp_Name,
	E.Site_Description
FROM
	tblIncident_Details I,
	tblLost_Days L,
	VIEW_EMPLOYEE_DETAILS E
WHERE
	L.Incident_Number = I.Incident_Number
	AND E.Employee_Number = I.Injured_Person_Id
);
commit;


CREATE OR REPLACE VIEW "CATSDBA"."VIEW_GOVERNMENT_DETAILS" ("DATE_COMPLETED","DATE_STRING","SITE_DESCRIPTION","GOVT_DEPARTMENT","LOCATION","LOCATION_OF_DOCUMENTS","COMMENTS","EMP_NAME","CONDUCTED_BY_ID","GOVERNMENT_ID","REQUIREMENT_TYPE","REFTABLE_DESCRIPTION","SHIFT_ID","TIWEST_CONTACT_ID","SITE_ID","SHIFT") AS (
SELECT
	G.Date_Completed,
	TO_CHAR(G.Date_Completed) Date_String,
	S.Site_Description,
	G.Govt_Department,
	G.Location,
	G.Location_Of_Documents,
	G.Comments,
	E.Last_Name || ', ' || E.First_Name Emp_Name,
	G.Conducted_By_Id,
	G.Government_Id,
	G.Requirement_Type,
	R1.RefTable_Description,
	G.Shift_Id,
	G.Tiwest_Contact_Id,
	G.Site_Id,
	R2.RefTable_Description Shift
	FROM
	tblRefTable R1,
	tblRefTable R2,
	tblEmployee_Details E,
	tblSite S,
	tblGovernment_Details G
WHERE
	S.Site_Id = G.Site_Id
	AND E.Employee_Number = G.Tiwest_Contact_Id
	AND R1.RefTable_Id = G.Requirement_Type
	AND R2.RefTable_Id = G.Shift_Id
);
commit;

CREATE OR REPLACE VIEW "CATSDBA"."VIEW_ORGANISATIONAL_STRUCTURE" ("SITE_ID","SITE_DESCRIPTION","SITE_FOLDER","DEPARTMENT_ID","DEPARTMENT_DESCRIPTION","SECTION_ID","SECTION_DESCRIPTION","EMAIL_FROM_ADDRESS") AS (
SELECT
	s.Site_Id,
	s.Site_Description,
	s.site_folder,
	d.Department_Id,
	d.Department_Description,
	a.Section_Id,
	a.Section_Description,
	s.email_from_address
FROM
	tblSite s,
	tblDepartment d,
	tblSection a
WHERE
	s.Site_Id = d.Site_Id
	and d.Department_Id = a.Department_Id(+)
)
ORDER BY
	s.Site_Description ASC ;
commit;


-- add reported_by_id for employee number instead of employee name
alter table TBLOTHER_REPORTS add(reported_by_id number);
-- update the table with employee_numbers
update TBLOTHER_REPORTS o set o.reported_by_id = (select e.employee_number from view_employee_details e where e.emp_name = o.reported_by and e.user_status='Active');
-- re-create the view to include employee name from tblemployee_Details
CREATE OR REPLACE VIEW "CATSDBA"."VIEW_OTHER_REPORTS" ("RECORD_TYPE","DATE_COMPLETED","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","REPORTED_BY","REPORTED_BY_ID","LOCATION_DOCS","REPORT_ID","REGISTER_ORIGIN","REPORT_NOTES","COMMENTS","DEPARTMENT_ID","STRDATE","SITE_ID") AS (
SELECT
	o.Record_Type,
	o.Date_Completed,
	s.Site_Description,
	d.Department_Description,
	e.last_name||', '||e.first_name Reported_by,
	o.Reported_by_id,
	o.Location_Docs,
	o.Report_Id,
	o.Register_Origin,
	o.Report_Notes,
	o.Comments,
	o.Department_Id,
	TO_CHAR(o.Date_Completed) StrDate,
	s.Site_ID
FROM
	tblDepartment d,
	tblSite s,
	tblOther_Reports o,
	tblEmployee_Details e
WHERE
	s.Site_Id = o.Site_Id AND
	d.Department_Id = o.Department_Id AND
	e.employee_number = o.reported_by_id
);
commit;

-- move pcr checkboxes to the main checkboxes table
insert into TBLINCIDENTFORM_CHECKBOXES (checkbox_type, checkbox_desc, checkbox_comment, dropdown_display) (select checkbox_type, checkbox_desc, checkbox_comment, dropdown_display from TBLPCR_CHECKBOXES);
commit;

CREATE OR REPLACE VIEW "CATSDBA"."VIEW_SCHEDULE" ("SCHEDULER_ID","REGISTER_ORIGIN","ACTION_TITLE","SCHEDULER_DESCRIPTION","COMPLETION_DATE","ACTION_RAISED_DATE","FREQUENCY","NO_OF_DAYS_NOTICE","MANAGED_BY_ID","REPORT_ID","DEPARTMENT_ID","SECTION_ID","STATUS","COMPLETION_DATE_STRING","MANAGED_BY","POSITION","POSITIONID","SITE_ID","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","SECTION_DESCRIPTION","ACTIONS_CREATED","ACTIONS_CLOSED") AS (
SELECT
	sc.Scheduler_Id,
	sc.Register_Origin,
	sc.Action_Title,
	sc.Scheduler_Description,
	sc.Completion_Date,
	sc.Action_Raised_Date,
	sc.Frequency,
	sc.No_Of_Days_Notice,
	sc.Managed_By_Id,
	sc.Report_Id,
	sc.department_Id,
	sc.Section_Id,
	sc.Status,
	TO_CHAR(sc.Completion_Date) Completion_Date_String,
	e.Emp_Name Managed_By,
	e.Position_Name Position,
	e.Position PositionID,
	s.Site_Id,
	s.Site_Description,
	d.Department_Description,
	sec.Section_Description,
	actions_created(sc.scheduler_id,'Originating_Schedule') actions_created,
	actions_closed(sc.scheduler_id,'Originating_Schedule') actions_closed
FROM
	tblScheduler sc,
	tblSite s,
	tblDepartment d,
	tblSection sec,
	VIEW_EMPLOYEE_DETAILS e
WHERE
	sc.Managed_By_Id = e.Employee_Number
	AND sc.Site_Id = s.Site_Id
	AND sc.Department_Id = d.Department_Id
	AND sc.Section_Id = sec.Section_Id(+)
);
commit;

-- turn spooling off
spool off;