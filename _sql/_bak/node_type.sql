# DBTools DBMYSQL - MySQL Database Dump
#

SET FOREIGN_KEY_CHECKS=0;

# Dumping Table Structure for node_type

#
CREATE TABLE `node_type` (
  `id` int(20) NOT NULL auto_increment,
  `name` varchar(128) default NULL,
  `description` varchar(255) default NULL,
  `icon` varchar(255) default NULL,
  `icon_plus` varchar(255) default NULL,
  `icon_minus` varchar(255) default NULL,
  `thumbnail` varchar(255) default NULL,
  `allowed_child_types` varchar(255) default NULL,
  `status` smallint(6) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#
# Dumping Data for node_type
#
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (1, 'page', 'Standard content page', 'article.gif', 'articles-plus.gif', 'articles-minus.gif', 'article.gif', '1,2,3,4,5,9,10', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (2, 'news', 'Standard News page', 'newsarticle.gif', 'folder-plus.gif', 'folder-minus.gif', 'newsarticle.gif', '2', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (3, 'product', 'Standard Product page', 'product.gif', 'gallery-plus.gif', 'gallery-minus.gif', 'product.gif', '3', 0);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (4, 'testimonial', 'Standard Testimonial page', 'testimonial.gif', 'users-plus.gif', 'users-minus.gif', 'testimonials.gif', '4', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (5, 'course', 'Standard Courses page', 'course.gif', 'plugin-plus.gif', 'plugin-minus.gif', 'course.gif', '5,9', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (6, 'schedule', 'Standard Schedule page', 'schedule.gif', 'group-plus.gif', 'group-minus.gif', 'schdule.gif', '6', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (7, 'campaign', 'Advertising/Marketing Campaign', 'campaign.gif', 'folder-plus.gif', 'folder-minus.gif', 'campaign.gif', '7', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (8, 'catalogue', 'Catalogue Products/Image etc', 'catalogue.gif', 'plugin-plus.gif', 'plugin-minus.gif', 'catalogue.gif', '1,2,3,4,5,8,9,10', 0);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (9, 'url', 'Internal/External URL', 'url.gif', 'www-plus.gif', 'www-minus.gif', 'url.gif', '1,2,3,4,5,9,10', 1);
INSERT INTO `node_type` (id, name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, status) VALUES (10, 'forum', 'Forum plugin', 'forum.gif', 'forum-plus.gif', 'forum-minus.gif', 'forum.gif', '10', 0);
SET FOREIGN_KEY_CHECKS=1

