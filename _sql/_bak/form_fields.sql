DROP ROLE cats_admin; 
CREATE ROLE cats_admin; 
GRANT
	ALL PRIVILEGES
TO CATS_ADMIN;

GRANT cats_admin TO catsdba;



DROP INDEX catsdba.pk_form_fields_pid;
DROP INDEX catsdba.pk_form_fields_id;
DROP TABLE catsdba.form_fields;
DROP SEQUENCE catsdba.form_fields_seq;
commit;


CREATE TABLE catsdba.form_fields (
  field_id NUMBER NOT NULL,
  field_pid NUMBER default NULL,
  field_group_type varchar2(50) default NULL,
  field_name varchar2(128) default NULL,
  field_label varchar2(128) default NULL,
  field_type varchar2(10) default 'm',
	field_type_id NUMBER default 1,
  field_length NUMBER default NULL,
  field_value varchar2(1000),
  field_attributes varchar2(255) default NULL,
	field_table_name varchar2(50) default NULL,
  field_column_name varchar2(50) default NULL,
  field_display_func varchar2(255) default NULL,
  field_set_func varchar2(255) default NULL,
	col_num SMALLINT default NULL,
  col_span SMALLINT default NULL,
  row_span SMALLINT default NULL,
  col_count SMALLINT default NULL,
  mandatory SMALLINT default NULL,
  options varchar2(1000),
  sort_order NUMBER default 1,
  role_mask NUMBER default NULL,
  group_mask NUMBER default NULL,
  status SMALLINT default NULL
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_fields_id ON catsdba.form_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_form_fields_pid ON catsdba.form_fields
(
    field_pid
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_fields_seq;
CREATE OR REPLACE TRIGGER catsdba.form_fields_trg
	BEFORE INSERT ON catsdba.form_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_fields_seq.nextval INTO :new.field_id FROM dual;
	END;
/
commit;

INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'cats', 'CATS', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'admin', 'ADMIN', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'pcr', 'PCR', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'actions', 'Actions', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'audits_and_inspections', 'Audits & Inspections', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'government_inspections', 'Government Inspections', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'incidents', 'Incidents', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'major_hazards', 'Major Hazard Register', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'meeting_minutes', 'Meeting Minutes', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'other_records', 'Other Records', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'schedules', 'Schedules', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'site_specific', 'Site Specific Obligations', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'workload_review', 'Workload Review', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'organisation_structure', 'Organisation Structure', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'reallocate', 'Re-Allocate responsibility', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'employees', 'Employees', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'checkboxes', 'Incident Form Checkboxes', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'references', 'Reference Table', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'risk_definitions', 'Risk Definitions', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'work_hours', 'Work Hours', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'motd', 'Message of the Day', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'lost_days', 'Lost Days', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'email_configuration', 'Email Configuration', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_search', 'PCR Search', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_maintain_actions', 'Maintain Actions', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_maintain_reviewers', 'Maintain Reviewers', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_maintain_email', 'Maintain Email Messages', 'm', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'search', 'search_actions', 'Search Actions', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'site_id', 'Site', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'department_id', 'Department', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'section_id', 'Section', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_managed_by', 'Action Managed By', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'allocated_to', 'Allocated To', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'managed_by_or_allocated_to', 'Managed By Or Allocated To', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_type', 'Action Type', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'origin', 'Origin', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'get_dropdown(\''origin\'')', 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_category_id', 'Category', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '{''function'':''get_dropdown(\''action_category_id\'')''}', 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'status', 'Status', 's', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_number', 'Action Number', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'due_date_from', 'Due Date From', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'due_date_to', 'Due Date To', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'closed_date_from', 'Closed Date From', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'closed_date_to', 'Closed Date To', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'edit', 'edit_actions', 'Edit Action', 's', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'origin', 'Origin', 's', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'action_category_id', 'Action Category', 'g', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '{''function'':''get_checkboxes(\''action_category_id\'')''}', 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'action_title', 'Action Title', 's', 128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'description', 'Description', 's', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'scheduled_date', 'Scheduled Completion Date', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'reminder_date', 'Date Next Reminder to be Sent to Manager', 'd', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'managed_by', 'Action Managed By', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '{''function'':''get_dropdown(\''employee_id\'')''}', 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'notify_managed_by', 'Notify Managed By Person By Email', 'c', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'site_id', 'Site', 's', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'department_id', 'Department', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'section_id', 'Section', 's', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'allocated_to', 'Allocated To', 'n', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'notify_allocated_to', 'Notify Allocated to Person By Email', 'c', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'origin_table', 'Table of Origin', 'c', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
commit;

DROP INDEX catsdba.pk_form_types_id;
DROP TABLE catsdba.form_types;
DROP SEQUENCE catsdba.form_types_seq;
commit;

CREATE TABLE catsdba.form_types (
  id NUMBER NOT NULL,
  name varchar2(128) default NULL,
  description varchar2(255) default NULL,
  icon varchar2(255) default NULL,
  icon_plus varchar2(255) default NULL,
  icon_minus varchar2(255) default NULL,
  thumbnail varchar2(255) default NULL,
  allowed_child_types varchar2(255) default NULL,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_types_id ON catsdba.form_types
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_types_seq;
CREATE OR REPLACE TRIGGER catsdba.form_types_trg
	BEFORE INSERT ON catsdba.form_types
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_types_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Menu', 'Standard Top Level Menu', 'menu.gif', 'menu-plus.gif', 'menu-minus.gif', 'menu.gif', '1,2,3,4,5,9,10', 'html_menu_element(', '', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Number', 'Standard Input Field with number formatter', 'number.gif', 'number-plus.gif', 'number-minus.gif', 'number.gif', '2', 'html_number_field(', 'db_number', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Text', 'Standard Input field', 'text.gif', 'text-plus.gif', 'text-minus.gif', 'text.gif', '3', 'html_draw_input_field(', 'db_sanitize(', '', 0);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Combo', 'Standard Combo List', 'combo.gif', 'combo-plus.gif', 'combo-minus.gif', 'combo.gif', '4', 'html_draw_combo_field(', '', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Group', 'Group of Items/Collection', 'group.gif', 'group-plus.gif', 'group-minus.gif', 'group.gif', '5,9', 'html_group_collection(', '', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Date', 'Standard Input field with date widget', 'date.gif', 'date-plus.gif', 'date-minus.gif', 'date.gif', '6', 'html_date_field(', '', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('DateTime', 'Standard Input field with datetime widget', 'dtime.gif', 'dtime-plus.gif', 'dtime-minus.gif', 'dtime.gif', '7', 'html_date_time_field(', '', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Radio', 'Standard Radio button', 'radio.gif', 'radio-plus.gif', 'radio-minus.gif', 'radio.gif', '1,2,3,4,5,8,9,10', 'html_draw_input_field(', '', '', 0);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Checkbox', 'Standard Checkbox', 'checkbox.gif', 'checkbox-plus.gif', 'checkbox-minus.gif', 'checkbox.gif', '1,2,3,4,5,9,10', 'html_draw_checkbox_field(', '', '', 1);
INSERT INTO form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Helper', 'Helper', 'helper.gif', 'helper-plus.gif', 'helper-minus.gif', 'helper.gif', '10', 'html_draw_help_field(', '', '', 0);

commit;


CREATE TABLE catsdba.user_group (
  user_group_id NUMBER NOT NULL,
  user_group_name varchar2(64) default NULL,
  user_group_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_group_id on catsdba.user_group 
(
  user_group_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_group_seq;
CREATE OR REPLACE TRIGGER catsdba.user_group_trg
	BEFORE INSERT ON catsdba.user_group
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_group_seq.nextval INTO :new.user_group_id FROM dual;
	END;
/
commit;


INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Master Administrator', 1073741824, 2);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Publishers', 64, 2);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Editors', 32, 2);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Marketing', 16, 2);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Trainers', 8, 2);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Forum Members', 4, 0);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Members', 2, 0);
INSERT INTO user_group (user_group_name, user_group_mask, status) VALUES ('Administrator', 128, 2);


CREATE TABLE catsdba.user_role (
  user_role_id NUMBER NOT NULL,
  user_role_name varchar2(64) default NULL,
  user_role_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_role_id on catsdba.user_role 
(
  user_role_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_role_seq;
CREATE OR REPLACE TRIGGER catsdba.user_role_trg
	BEFORE INSERT ON catsdba.user_role
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_role_seq.nextval INTO :new.user_role_id FROM dual;
	END;
/
commit;

INSERT INTO user_role (user_role_name, user_role_mask, status) VALUES ('Administrator', 2147483648, 2);
INSERT INTO user_role (user_role_name, user_role_mask, status) VALUES ('Publisher', 8, 0);
INSERT INTO user_role (user_role_name, user_role_mask, status) VALUES ('Editor', 4, 0);
INSERT INTO user_role (user_role_name, user_role_mask, status) VALUES ('Trainer', 2, 0);

commit;


CREATE TABLE catsdba.user_history_log (
  id NUMBER NOT NULL,
  session_id varchar2(50) default NULL,
  user_id NUMBER default 0,
  node_id NUMBER default 0,
  ip varchar2(15) default NULL,
  request varchar2(255) default NULL,
  date_time date NOT NULL
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_history_log_id on catsdba.user_history_log 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_history_log_seq;
CREATE OR REPLACE TRIGGER catsdba.user_history_log_trg
	BEFORE INSERT ON catsdba.user_history_log
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_history_log_seq.nextval INTO :new.user_history_log_id FROM dual;
	END;
/
commit;


CREATE TABLE catsdba.status_values (
  id NUMBER NOT NULL,
  status_title varchar2(50) default NULL,
  status_mask NUMBER default NULL,
  status_type varchar2(50) default NULL,
  status_sub_type varchar2(50) default NULL,
  group_mask NUMBER default 0,
  status SMALLINT default 1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_status_values_id on catsdba.status_values 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.status_values_seq;
CREATE OR REPLACE TRIGGER catsdba.status_values_trg
	BEFORE INSERT ON catsdba.status_values
	FOR EACH ROW
	BEGIN
		SELECT catsdba.status_values_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('On', 1, 'switch', NULL, 0, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Off', 0, 'switch', NULL, 0, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Hide from menu', 0, 'node_mask', NULL, 192, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Approval needed', 1, 'node_mask', NULL, 0, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Reject Item', 256, 'node_mask', 'pending', 192, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Publish Item', 128, 'node_mask', '', 192, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Re-Publish Item', 132, 'node_mask', 'published', 192, 1);
INSERT INTO status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Archive', 32, 'test', 'published', 192, 1);
commit;


