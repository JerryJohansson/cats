# DBTools DBMYSQL - MySQL Database Dump
#

SET FOREIGN_KEY_CHECKS=0;

# Dumping Table Structure for user_history_log

#
CREATE TABLE `user_history_log` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(50) default NULL,
  `user_id` int(11) default '0',
  `node_id` int(11) default '0',
  `ip` varchar(15) default NULL,
  `request` varchar(255) default NULL,
  `date_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#
SET FOREIGN_KEY_CHECKS=1

