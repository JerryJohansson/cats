# DBTools DBMYSQL - MySQL Database Dump
#

SET FOREIGN_KEY_CHECKS=0;

# Dumping Table Structure for user_group

#
CREATE TABLE `user_group` (
  `user_group_id` int(11) NOT NULL default '0',
  `user_group_name` varchar(64) default NULL,
  `user_group_mask` bigint(20) default '0',
  `status` smallint(6) default '0',
  PRIMARY KEY  (`user_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#
# Dumping Data for user_group
#
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (1, 'Master Administrator', 1073741824, 2);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (2, 'Publishers', 64, 2);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (3, 'Editors', 32, 2);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (4, 'Marketing', 16, 2);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (5, 'Trainers', 8, 2);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (6, 'Forum Members', 4, 0);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (7, 'Members', 2, 0);
INSERT INTO `user_group` (user_group_id, user_group_name, user_group_mask, status) VALUES (8, 'Administrator', 128, 2);
SET FOREIGN_KEY_CHECKS=1

