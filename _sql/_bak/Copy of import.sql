--DROP ROLE cats_admin; 
--CREATE ROLE cats_admin; 
--GRANT
--	ALL PRIVILEGES
--TO CATS_ADMIN;
--GRANT cats_admin TO catsdba;

DROP INDEX catsdba.pk_form_types_id;
DROP TABLE catsdba.form_types;
DROP SEQUENCE catsdba.form_types_seq;
commit;

CREATE TABLE catsdba.form_types (
  id NUMBER NOT NULL,
  name varchar2(128) default NULL,
  description varchar2(255) default NULL,
  icon varchar2(255) default NULL,
  icon_plus varchar2(255) default NULL,
  icon_minus varchar2(255) default NULL,
  thumbnail varchar2(255) default NULL,
  allowed_child_types varchar2(255) default NULL,
	display_function varchar2(255) default '',
	set_function varchar2(255) default '',
	get_function varchar2(255) default '',
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_types_id ON catsdba.form_types
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_types_seq;
CREATE OR REPLACE TRIGGER catsdba.form_types_trg
	BEFORE INSERT ON catsdba.form_types
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_types_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Menu', 'Standard Top Level Menu', 'menu.gif', 'menu-plus.gif', 'menu-minus.gif', 'menu.gif', '1,2,3,4,5,9,10', 'html_menu_element(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Number', 'Standard Input Field with number formatter', 'number.gif', 'number-plus.gif', 'number-minus.gif', 'number.gif', '2', 'html_number_field(', 'db_number', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Text', 'Standard Input field', 'text.gif', 'text-plus.gif', 'text-minus.gif', 'text.gif', '3', 'html_draw_input_field(', 'db_sanitize(', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Combo', 'Standard Combo List', 'combo.gif', 'combo-plus.gif', 'combo-minus.gif', 'combo.gif', '4', 'html_draw_combo_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Group', 'Group of Items/Collection', 'group.gif', 'group-plus.gif', 'group-minus.gif', 'group.gif', '5,9', 'html_group_collection(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Date', 'Standard Input field with date widget', 'date.gif', 'date-plus.gif', 'date-minus.gif', 'date.gif', '6', 'html_date_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('DateTime', 'Standard Input field with datetime widget', 'dtime.gif', 'dtime-plus.gif', 'dtime-minus.gif', 'dtime.gif', '7', 'html_date_time_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Radio', 'Standard Radio button', 'radio.gif', 'radio-plus.gif', 'radio-minus.gif', 'radio.gif', '1,2,3,4,5,8,9,10', 'html_draw_input_field(', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Checkbox', 'Standard Checkbox', 'checkbox.gif', 'checkbox-plus.gif', 'checkbox-minus.gif', 'checkbox.gif', '1,2,3,4,5,9,10', 'html_draw_checkbox_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Helper', 'Helper', 'helper.gif', 'helper-plus.gif', 'helper-minus.gif', 'helper.gif', '10', 'html_draw_help_field(', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Tabs','Tab Container', 'tab.gif', 'tab-plus.gif', 'tab-minus.gif', 'tab.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormTabs', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Edit','Form Editor', 'edit.gif', 'edit-plus.gif', 'edit-minus.gif', 'edit.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormEdit', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Search','Search Filter Form', 'search.gif', 'search-plus.gif', 'search-minus.gif', 'search.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormSearch', '', '', 0);

commit;

DROP INDEX catsdba.pk_form_fields_pid;
DROP INDEX catsdba.pk_form_fields_id;
DROP TABLE catsdba.form_fields;
DROP SEQUENCE catsdba.form_fields_seq;
commit;


CREATE TABLE catsdba.form_fields (
  field_id NUMBER NOT NULL,
  field_pid NUMBER default NULL,
  field_group_type varchar2(50) default NULL,
  field_name varchar2(128) default NULL,
  field_label varchar2(128) default NULL,
  field_type varchar2(50) default 'Menu',
	field_type_id NUMBER default 1,
  field_length NUMBER default NULL,
  field_value varchar2(1000),
  field_attributes varchar2(255) default NULL,
	field_table_name varchar2(50) default NULL,
  field_column_name varchar2(50) default NULL,
  field_display_func varchar2(255) default NULL,
	field_display_params varchar2(255) default NULL,
  field_set_func varchar2(255) default NULL,
	field_set_params varchar2(255) default NULL,
	col_num SMALLINT default NULL,
  col_span SMALLINT default NULL,
  row_span SMALLINT default NULL,
  col_count SMALLINT default NULL,
  mandatory SMALLINT default NULL,
  options varchar2(1000),
  sort_order NUMBER default 1,
  role_mask NUMBER default NULL,
  group_mask NUMBER default NULL,
  auth_site_id varchar2(255) default NULL,
	status SMALLINT default NULL
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_fields_id ON catsdba.form_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_form_fields_pid ON catsdba.form_fields
(
    field_pid
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_fields_seq;
CREATE OR REPLACE TRIGGER catsdba.form_fields_trg
	BEFORE INSERT ON catsdba.form_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_fields_seq.nextval INTO :new.field_id FROM dual;
	END;
/
commit;


INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'CATS', 'cats', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'ADMIN', 'admin', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'PCR', 'pcr', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'actions', 'Actions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'audits_and_inspections', 'Audits &'||' Inspections', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'government_requirements', 'Government Inspections', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'incidents', 'Incidents', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'major_hazards', 'Major Hazard Register', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'meeting_minutes', 'Meeting Minutes', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'other_reports', 'Other Records', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'schedules', 'Schedules', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'site_specific', 'Site Specific Obligations', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', 'dashboard', 'Workload Review', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 0, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'organisational_structure', 'Organisation Structure', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'reallocate', 'Re-Allocate responsibility', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'employees', 'Employees', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'incident_checkboxes', 'Incident Form Checkboxes', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'references', 'Reference Table', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'risk_definitions', 'Risk Definitions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'work_hours', 'Work Hours', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'motd', 'Message of the Day', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 1024+512+256, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'lost_days', 'Lost Days', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', 'email_configuration', 'Email Configuration', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 256, 256, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_search', 'PCR Search', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_maintain_actions', 'Maintain Actions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_maintain_reviewers', 'Maintain Reviewers', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', 'pcr_maintain_email', 'Maintain Email Messages', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 'search', 'search_actions', 'Search Actions', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'site_id', 'Site', 'Helper', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'department_id', 'Department', 'Helper', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'section_id', 'Section', 'Helper', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_managed_by', 'Action Managed By', 'Helper', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'allocated_to', 'Allocated To', 'Helper', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'managed_by_or_allocated_to', 'Managed By Or Allocated To', 'Helper', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_type', 'Action Type', 'Combo', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'origin', 'Origin', 'Hidden', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 'get_dropdown(\''origin\'')', 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_category_id', 'Category', 'Group', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '{''function'':''get_dropdown(\''action_category_id\'')''}', 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'status', 'Status', 'Combo', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'action_number', 'Action Number', 'Number', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'due_date_from', 'Due Date From', 'Date', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'due_date_to', 'Due Date To', 'Date', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'closed_date_from', 'Closed Date From', 'Date', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'closed_date_to', 'Closed Date To', 'Date', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 'edit', 'edit_actions', 'Edit Action', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'origin', 'Origin', 'Hidden', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'action_category_id', 'Action Category', 'Group', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '{''function'':''get_checkboxes(\''action_category_id\'')''}', 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'action_title', 'Action Title', 'Text', 128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'description', 'Description', 'Text', 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'scheduled_date', 'Scheduled Completion Date', 'Date', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'reminder_date', 'Date Next Reminder to be Sent to Manager', 'Date', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'managed_by', 'Action Managed By', 'Helper', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, '{''function'':''get_dropdown(\''employee_id\'')''}', 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'notify_managed_by', 'Notify Managed By Person By Email', 'Checkbox', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'site_id', 'Site', 'Helper', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'department_id', 'Department', 'Helper', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'section_id', 'Section', 'Helper', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'allocated_to', 'Allocated To', 'Helper', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'notify_allocated_to', 'Notify Allocated to Person By Email', 'Checkbox', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (44, 'edit', 'origin_table', 'Table of Origin', 'Hidden', 3, 'Yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', 'CFG', 'cfg', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 4, 2048, 2048, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (59, 'menu', 'configuration', 'Configuration Settings', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2048, 2048, 0);
commit;

-- update field_type_id
update catsdba.form_fields f set field_type_id = (select t.id from catsdba.form_types t where t.name = f.field_type);
-- field_display_params
update catsdba.form_fields set field_display_params = '''search''' where field_pid in (select field_id from catsdba.form_fields where field_pid=0);
commit;



DROP INDEX catsdba.pk_help_fields_field_id;
DROP INDEX catsdba.pk_help_fields_id;
DROP TABLE catsdba.help_fields;
DROP SEQUENCE catsdba.help_fields_seq;
commit;

CREATE TABLE catsdba.help_fields (
	id NUMBER NOT NULL,
  field_id NUMBER default 0,
	help_tip varchar2(500) default '<h2>Help Page Under Construction</h2>',
	help_user_guide varchar2(2000) default '<h2>User Guide Page Under Construction</h2>'
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_help_fields_id ON catsdba.help_fields
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_help_fields_field_id ON catsdba.help_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.help_fields_seq;
CREATE OR REPLACE TRIGGER catsdba.help_fields_trg
	BEFORE INSERT ON catsdba.help_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.help_fields_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

insert into catsdba.help_fields (select 0 as id, field_id, '<h1>'||field_label||'</h1><h4>Help Page Under Construction</h4>' as help_tip, '<h1>'||field_label||'</h1><h3>Table:'||field_table_name||'</h3><h3>Column:'||field_column_name||'</h3><h4>User Guide Page Under Construction</h4>' as help_user_guide from catsdba.form_fields);
commit;



DROP INDEX catsdba.pk_user_group_id;
DROP TABLE catsdba.user_group;
DROP SEQUENCE catsdba.user_group_seq;
commit;

CREATE TABLE catsdba.user_group (
  user_group_id NUMBER NOT NULL,
  user_group_name varchar2(64) default NULL,
  user_group_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_group_id on catsdba.user_group 
(
  user_group_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_group_seq;
CREATE OR REPLACE TRIGGER catsdba.user_group_trg
	BEFORE INSERT ON catsdba.user_group
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_group_seq.nextval INTO :new.user_group_id FROM dual;
	END;
/
commit;

--1048576+524288+262144+131072+65536+32786+16384+8192+4096+2048+1024+512+256+128+64+32+16+8+4+2
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Master Administrator', 2097168, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Root Administrators', 4096+2048+1024+512+256+128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Super Administrators', 4096, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Administrators', 128, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Admin Assistant', 64, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Incident Analysis', 32, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Editor (Privileged)', 16, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Editor (Restricted)', 8, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Reader (Privileged)', 4, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('Reader (Restricted)', 2, 0);
INSERT INTO catsdba.user_group (user_group_name, user_group_mask, status) VALUES ('No Access', 0, 0);

commit;

DROP INDEX catsdba.pk_user_role_id;
DROP TABLE catsdba.user_role;
DROP SEQUENCE catsdba.user_role_seq;
commit;

CREATE TABLE catsdba.user_role (
  user_role_id NUMBER NOT NULL,
  user_role_name varchar2(64) default NULL,
  user_role_mask NUMBER default 0,
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_role_id on catsdba.user_role 
(
  user_role_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_role_seq;
CREATE OR REPLACE TRIGGER catsdba.user_role_trg
	BEFORE INSERT ON catsdba.user_role
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_role_seq.nextval INTO :new.user_role_id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Super Administrator', 2147483648, 2);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Administrator', 128, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Admin Assistant', 64, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Editor', 32, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Trainer', 16, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('Viewer', 2, 0);
INSERT INTO catsdba.user_role (user_role_name, user_role_mask, status) VALUES ('No Access', 0, 0);

commit;




DROP INDEX catsdba.pk_user_history_log_id;
DROP TABLE catsdba.user_history_log;
DROP SEQUENCE catsdba.user_history_log_seq;
commit;

CREATE TABLE catsdba.user_history_log (
  id NUMBER NOT NULL,
  log_type varchar2(50) default NULL,
	session_id varchar2(50) default NULL,
  employee_number NUMBER default 0,
  form_id NUMBER default 0,
	form_name varchar2(128) default NULL,
  ip varchar2(15) default NULL,
  request varchar2(255) default NULL,
  date_time date default sysdate
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_user_history_log_id on catsdba.user_history_log 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.user_history_log_seq;
CREATE OR REPLACE TRIGGER catsdba.user_history_log_trg
	BEFORE INSERT ON catsdba.user_history_log
	FOR EACH ROW
	BEGIN
		SELECT catsdba.user_history_log_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

DROP INDEX catsdba.pk_status_values_id;
DROP TABLE catsdba.status_values;
DROP SEQUENCE catsdba.status_values_seq;
commit;

CREATE TABLE catsdba.status_values (
  id NUMBER NOT NULL,
  status_title varchar2(50) default NULL,
  status_mask NUMBER default NULL,
  status_type varchar2(50) default NULL,
  status_sub_type varchar2(50) default NULL,
  group_mask NUMBER default 0,
  status SMALLINT default 1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_status_values_id on catsdba.status_values 
(
  id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.status_values_seq;
CREATE OR REPLACE TRIGGER catsdba.status_values_trg
	BEFORE INSERT ON catsdba.status_values
	FOR EACH ROW
	BEGIN
		SELECT catsdba.status_values_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Active', 1, 'TBLEMPLOYEE_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Disabled', 0, 'TBLEMPLOYEE_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Open', 0, 'TBLACTION_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Closed - Performed', 1, 'TBLACTION_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Closed - Not Performed', 2, 'TBLACTION_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Open', 0, 'TBLINCIDENT_DETAILS', NULL, 0, 1);
INSERT INTO catsdba.status_values (status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES ('Closed', 1, 'TBLINCIDENT_DETAILS', NULL, 0, 1);
commit;

