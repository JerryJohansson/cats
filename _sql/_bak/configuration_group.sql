DROP INDEX catsdba.pk_cfg_group_id;
DROP TABLE catsdba.cfg_group;
DROP SEQUENCE catsdba.cfg_group_seq;
commit;

CREATE TABLE cfg_group (
  cfg_group_id number NOT NULL,
  cfg_group_title varchar2(64) NOT NULL,
  cfg_group_description varchar2(255) NOT NULL,
  sort_order number default NULL,
  visible SMALLINT default 1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_cfg_group_id ON catsdba.cfg_group
(
    cfg_group_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.cfg_group_seq;
CREATE OR REPLACE TRIGGER catsdba.cfg_group_trg
	BEFORE INSERT ON catsdba.cfg_group
	FOR EACH ROW
	BEGIN
		SELECT catsdba.cfg_group_seq.nextval INTO :new.cfg_group_id FROM dual;
	END;
/
commit;

INSERT INTO cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Site Administration', 'This is where we set all default application configuration settings', 1, 1);
INSERT INTO cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Module Options', 'Configuration settings specific to default and plugin modules', 2, 1);
INSERT INTO cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Sessions', 'Session options', 3, 1);
INSERT INTO cfg_group (cfg_group_title, cfg_group_description, sort_order, visible) VALUES ('Email Configuration', 'Email Configuration settings such as Transport Method and Linefeed type', 4, 1);


