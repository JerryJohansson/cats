--DROP ROLE cats_admin; 
--CREATE ROLE cats_admin; 
--GRANT
--	ALL PRIVILEGES
--TO CATS_ADMIN;
--GRANT cats_admin TO catsdba;

DROP INDEX catsdba.pk_form_types_id;
DROP TABLE catsdba.form_types;
DROP SEQUENCE catsdba.form_types_seq;
commit;

CREATE TABLE catsdba.form_types (
  id NUMBER NOT NULL,
  name varchar2(128) default NULL,
  description varchar2(255) default NULL,
  icon varchar2(255) default NULL,
  icon_plus varchar2(255) default NULL,
  icon_minus varchar2(255) default NULL,
  thumbnail varchar2(255) default NULL,
  allowed_child_types varchar2(255) default NULL,
	display_function varchar2(255) default '',
	set_function varchar2(255) default '',
	get_function varchar2(255) default '',
  status SMALLINT default 0
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_types_id ON catsdba.form_types
(
    id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_types_seq;
CREATE OR REPLACE TRIGGER catsdba.form_types_trg
	BEFORE INSERT ON catsdba.form_types
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_types_seq.nextval INTO :new.id FROM dual;
	END;
/
commit;

INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Menu', 'Standard Top Level Menu', 'menu.gif', 'menu-plus.gif', 'menu-minus.gif', 'menu.gif', '1,2,3,4,5,9,10', 'html_menu_element(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Number', 'Standard Input Field with number formatter', 'number.gif', 'number-plus.gif', 'number-minus.gif', 'number.gif', '2', 'html_number_field(', 'db_number', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Text', 'Standard Input field', 'text.gif', 'text-plus.gif', 'text-minus.gif', 'text.gif', '3', 'html_draw_input_field(', 'db_sanitize(', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Combo', 'Standard Combo List', 'combo.gif', 'combo-plus.gif', 'combo-minus.gif', 'combo.gif', '4', 'html_draw_combo_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Group', 'Group of Items/Collection', 'group.gif', 'group-plus.gif', 'group-minus.gif', 'group.gif', '5,9', 'html_group_collection(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Date', 'Standard Input field with date widget', 'date.gif', 'date-plus.gif', 'date-minus.gif', 'date.gif', '6', 'html_date_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('DateTime', 'Standard Input field with datetime widget', 'dtime.gif', 'dtime-plus.gif', 'dtime-minus.gif', 'dtime.gif', '7', 'html_date_time_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Radio', 'Standard Radio button', 'radio.gif', 'radio-plus.gif', 'radio-minus.gif', 'radio.gif', '1,2,3,4,5,8,9,10', 'html_draw_input_field(', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Checkbox', 'Standard Checkbox', 'checkbox.gif', 'checkbox-plus.gif', 'checkbox-minus.gif', 'checkbox.gif', '1,2,3,4,5,9,10', 'html_draw_checkbox_field(', '', '', 1);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Helper', 'Helper', 'helper.gif', 'helper-plus.gif', 'helper-minus.gif', 'helper.gif', '10', 'html_draw_help_field(', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Tabs','Tab Container', 'tab.gif', 'tab-plus.gif', 'tab-minus.gif', 'tab.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormTabs', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Edit','Form Editor', 'edit.gif', 'edit-plus.gif', 'edit-minus.gif', 'edit.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormEdit', '', '', 0);
INSERT INTO catsdba.form_types (name, description, icon, icon_plus, icon_minus, thumbnail, allowed_child_types, display_function, set_function, get_function, status) VALUES ('Search','Search Filter Form', 'search.gif', 'search-plus.gif', 'search-minus.gif', 'search.gif', '1,2,3,4,5,6,7,8,9,10,11', '\$this->getFormSearch', '', '', 0);

commit;

DROP INDEX catsdba.pk_form_fields_pid;
DROP INDEX catsdba.pk_form_fields_id;
DROP TABLE catsdba.form_fields;
DROP SEQUENCE catsdba.form_fields_seq;
commit;


CREATE TABLE catsdba.form_fields (
  field_id NUMBER NOT NULL,
  field_pid NUMBER default NULL,
  field_group_type varchar2(50) default NULL,
  field_name varchar2(128) default NULL,
  field_label varchar2(128) default NULL,
  field_type varchar2(50) default 'Menu',
	field_type_id NUMBER default 1,
  field_length NUMBER default NULL,
  field_value varchar2(1000),
  field_attributes varchar2(255) default NULL,
	field_table_name varchar2(50) default NULL,
  field_column_name varchar2(50) default NULL,
  field_display_func varchar2(255) default NULL,
	field_display_params varchar2(255) default NULL,
  field_set_func varchar2(255) default NULL,
	field_set_params varchar2(255) default NULL,
	col_num SMALLINT default NULL,
  col_span SMALLINT default NULL,
  row_span SMALLINT default NULL,
  col_count SMALLINT default NULL,
  mandatory SMALLINT default NULL,
  options varchar2(1000),
  sort_order NUMBER default 1,
  role_mask NUMBER default NULL,
  group_mask NUMBER default NULL,
  auth_site_id varchar2(255) default NULL,
	status SMALLINT default NULL,
	page_name varchar2(255) default NULL
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE UNIQUE INDEX catsdba.pk_form_fields_id ON catsdba.form_fields
(
    field_id
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE INDEX catsdba.pk_form_fields_pid ON catsdba.form_fields
(
    field_pid
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
TABLESPACE          catsdata
;
CREATE SEQUENCE catsdba.form_fields_seq;
CREATE OR REPLACE TRIGGER catsdba.form_fields_trg
	BEFORE INSERT ON catsdba.form_fields
	FOR EACH ROW
	BEGIN
		SELECT catsdba.form_fields_seq.nextval INTO :new.field_id FROM dual;
	END;
/
commit;


INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'CATS', 'cats', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'ADMIN', 'admin', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'PCR', 'pcr', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (0, 'menu', NULL, 'CFG', 'cfg', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 4, 2048, 2048, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'actions', 'Actions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'audits_and_inspections', 'Audits &'||' Inspections', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'government_requirements', 'Government Inspections', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'incidents', 'Incidents', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4+2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'major_hazards', 'Major Hazard Register', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'meeting_minutes', 'Meeting Minutes', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'other_reports', 'Other Records', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'schedules', 'Schedules', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'site_specific', 'Site Specific Obligations', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8+4, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (1, 'menu', NULL, 'dashboard', 'Workload Review', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 0, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'organisational_structure', 'Organisation Structure', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'reallocate', 'Re-Allocate responsibility', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'employees', 'Employees', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'incident_checkboxes', 'Incident Form Checkboxes', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'references', 'Reference Table', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 1024+512+256+128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'risk_definitions', 'Risk Definitions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'work_hours', 'Work Hours', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32+16+8, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'motd', 'Message of the Day', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 1024+512+256, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'lost_days', 'Lost Days', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 128, 128+64+32, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (2, 'menu', NULL, 'email_configuration', 'Email Configuration', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 256, 256, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_search', 'PCR Search', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_maintain_actions', 'Maintain Actions', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_maintain_reviewers', 'Maintain Reviewers', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (3, 'menu', NULL, 'pcr_maintain_email', 'Maintain Email Messages', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);

--next field_id=29
-- insert config menus 
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 'menu', NULL, 'configuration', 'Configuration Settings', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2048, 2048, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (4, 'menu', NULL, 'forms', 'Forms Settings', 'Menu', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, 2048, 2048, 0);
commit;

--next field_id=31
-- insert action main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 'search', NULL, 'search_actions', 'Search Actions', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 'edit', 'Action_Register_Edit', 'edit_actions', 'Edit Action', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (5, 'edit', 'Action_Register_View', 'view_actions', 'View Action', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
commit;

--next field_id=34
-- insert audits and inspections main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 'search', NULL, 'search_audits', 'Search Audits and Inspections', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 'edit', 'Audit_Edit', 'edit_audits', 'Edit Audits and Inspections', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (6, 'edit', 'Audit_View', 'view_audits', 'View Audits and Inspections', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
commit;

--next field_id=37
-- insert government requirements main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 'search', NULL, 'search_government_requirements', 'Search Government Inspections', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 'edit', 'Government_Requirement_Edit', 'edit_government_requirements', 'Edit Government Inspections', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 2, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (7, 'edit', 'Government_Requirement_View', 'view_government_requirements', 'View Government Inspections', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 3, NULL, NULL, 0);
commit;

--next field_id=39
-- insert incidents main tabs
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 'search', NULL, 'search_incidents', 'Search Incidents', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 'edit', 'Incident_Report_Edit', 'edit_incidents', 'Edit Incidents', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (8, 'edit', 'Incident_Report_View', 'view_incidents', 'View Incidents', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=41
-- insert major_hazards, Major Hazard Register
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 'search', NULL, 'search_major_hazards', 'Search Major Hazard Register', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 'edit', 'Major_Hazard_Register_Edit', 'edit_major_hazards', 'Edit Major Hazard Register', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (9, 'edit', 'Major_Hazard_Register_View', 'view_major_hazards', 'View Major Hazard Register', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=43
-- insert meeting_minutes, Meeting Minutes
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 'search', NULL, 'search_meeting_minutes', 'Search Meeting Minutes', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 'edit', 'MeetingMinutes_Edit', 'edit_meeting_minutes', 'Edit Meeting Minutes', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (10, 'edit', 'MeetingMinutes_View', 'view_meeting_minutes', 'View Meeting Minutes', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=46
-- insert other_reports, Other Records
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 'search', NULL, 'search_other_reports', 'Search Other Records', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 'edit', 'OtherRecords_Edit', 'edit_other_reports', 'Edit Other Records', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (11, 'edit', 'OtherRecords_View', 'view_other_reports', 'View Other Records', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=49
-- insert schedules, Schedules
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 'search', NULL, 'search_schedules', 'Search Schedules', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 'edit', 'Schedule_Edit', 'edit_schedules', 'Edit Schedules', 'Edit', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (12, 'edit', 'Schedule_View', 'view_schedules', 'View Schedules', 'View', 50, 'Action', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=52
-- insert site_specific, Site Specific Obligations
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 'search', NULL, 'search_site_specific', 'Search Site Specific Obligations', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 'edit', 'Site_Specific_Obligation_Edit', 'edit_site_specific', 'Edit Site Specific Obligations', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (13, 'edit', 'Site_Specific_Obligation_View', 'view_site_specific', 'View Site Specific Obligations', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=55
-- insert dashboard, Workload Review (this page does not have search/edit/new)
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (14, 'search', NULL, 'search_dashboard', 'Search Workload Review', 'Search', 64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
commit;

--next field_id=56
-- insert organisational_structure, Organisation Structure
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 'search', NULL, 'search_organisational_structure', 'Search Organisation Structure', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 'edit', 'Organisational_Structure_Edit', 'edit_organisational_structure', 'Edit Organisation Structure', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (15, 'edit', 'Organisational_Structure_View', 'view_organisational_structure', 'View Organisation Structure', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=59
-- insert reallocate, Re-Allocate responsibility
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (16, 'search', NULL, 'search_reallocate', 'Search Re-Allocate Responsibility', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
commit;

--next field_id=60
-- insert employees, Employees
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 'search', NULL, 'search_employees', 'Search Employees', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 'edit', 'Employee_Register_Edit', 'edit_employees', 'Edit Employee Details', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (17, 'edit', 'Employee_Register_View', 'view_employees', 'View Employee Details', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=63
-- insert incident_checkboxes, Incident Form Checkboxes
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 'search', 'Incident_Checkbox_New', 'search_incident_checkboxes', 'Search Incident Checkboxes', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 'edit', 'Incident_Checkbox_Edit', 'edit_incident_checkboxes', 'Edit Incident Checkboxes', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (18, 'edit', 'Incident_Checkbox_View', 'view_incident_checkboxes', 'View Incident Checkboxes', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=66
-- insert references, Reference Table
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 'search', NULL, 'search_references', 'Search Reference Table', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 'edit', 'Reference_Table_Edit', 'edit_references', 'Edit Reference Table', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (19, 'edit', 'Reference_Table_View', 'view_references', 'View Reference Table', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=69
-- insert risk_definitions, Risk Definitions
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 'search', NULL, 'search_risk_definitions', 'Search Risk Definitions', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 'edit', 'Administration', 'edit_risk_definitions', 'Edit Risk Definitions', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (20, 'edit', 'Administration', 'view_risk_definitions', 'View Risk Definitions', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=72
-- insert work_hours, Work Hours
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 'search', NULL, 'search_work_hours', 'Search Work Hours', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 'edit', 'Work_Hours_Edit', 'edit_work_hours', 'Edit Work Hours', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (21, 'edit', 'Work_Hours_View', 'view_work_hours', 'View Work Hours', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=75
-- insert motd, Message of the Day
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 'search', NULL, 'search_motd', 'Search Message of the Day', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 'edit', 'MOTD_Edit', 'edit_motd', 'Edit Message of the Day', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (22, 'edit', 'MOTD_View', 'view_motd', 'View Message of the Day', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=78
-- insert lost_days, Lost Days
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 'search', NULL, 'search_lost_days', 'Search Lost Days', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 'edit', 'Lost_Days_Edit', 'edit_lost_days', 'Edit Lost Days', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (23, 'edit', 'Lost_Days_View', 'view_lost_days', 'View Lost Days', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=81
-- insert email_configuration, Email Configuration
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (24, 'edit', 'Administration', 'edit_email_configuration', 'Edit Email Configuration', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (24, 'edit', 'Administration', 'view_email_configuration', 'View Email Configuration', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=83
-- insert pcr_search, PCR Search
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 'search', 'PCR_Details_Edit', 'search_pcr_details', 'Search PCR Details', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 'edit', 'PCR_Details_Edit', 'edit_pcr_details', 'Edit PCR Details', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (25, 'edit', 'PCR_Details_New', 'view_pcr_details', 'View PCR Details', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=86
-- insert pcr_maintain_actions, Maintain Actions
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 'search', 'PCR_Admin_Actions_List', 'search_pcr_maintain_actions', 'Search Maintain Actions', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 'edit', 'PCR_Admin_Actions_List', 'edit_pcr_maintain_actions', 'Edit Maintain Actions', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (26, 'edit', 'PCR_Admin_Actions_List', 'view_pcr_maintain_actions', 'View Maintain Actions', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=89
-- insert pcr_maintain_reviewers, Maintain Reviewers
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 'search', 'PCR_Admin_Maintain_Reviewers', 'search_pcr_maintain_reviewers', 'Search Maintain Reviewers', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 'edit', 'PCR_Admin_Maintain_Reviewers', 'edit_pcr_maintain_reviewers', 'Edit Maintain Reviewers', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (27, 'edit', 'PCR_Admin_Maintain_Reviewers', 'view_pcr_maintain_reviewers', 'View Maintain Reviewers', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=92
-- insert pcr_maintain_email, Maintain Email Messages
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'search', 'PCR_Admin_Email_Messages', 'search_pcr_maintain_email', 'Search Maintain Email Messages', 'Search', 64, 'Search', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 2, 2, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'edit', 'PCR_Admin_Email_Messages', 'edit_pcr_maintain_email', 'Edit Maintain Email Messages', 'Edit', 50, 'Edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, 1, NULL, NULL, 0);
INSERT INTO catsdba.form_fields (field_pid, field_group_type, page_name, field_name, field_label, field_type, field_length,field_value, field_attributes, col_num, col_span, row_span, field_column_name, field_display_func, field_set_func, col_count, mandatory, options, sort_order, role_mask, group_mask, status) VALUES (28, 'edit', 'PCR_Admin_Email_Messages', 'view_pcr_maintain_email', 'View Maintain Email Messages', 'View', 50, 'View', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, 1, NULL, NULL, 0);
commit;

--next field_id=95

-- END FORM_FIELDS ################################################################################


-- update field_type_id
update catsdba.form_fields f set field_type_id = (select t.id from catsdba.form_types t where t.name = f.field_type);
-- field_display_params
update catsdba.form_fields set field_display_params = '''search''' where field_pid in (select field_id from catsdba.form_fields where field_pid=0);
commit;
