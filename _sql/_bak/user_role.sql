# DBTools DBMYSQL - MySQL Database Dump
#

SET FOREIGN_KEY_CHECKS=0;

# Dumping Table Structure for user_role

#
CREATE TABLE `user_role` (
  `user_role_id` int(11) NOT NULL auto_increment,
  `user_role_name` varchar(64) default NULL,
  `user_role_mask` bigint(20) default '0',
  `status` smallint(6) default '0',
  PRIMARY KEY  (`user_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#
# Dumping Data for user_role
#
INSERT INTO `user_role` (user_role_id, user_role_name, user_role_mask, status) VALUES (1, 'Administrator', 2147483648, 2);
INSERT INTO `user_role` (user_role_id, user_role_name, user_role_mask, status) VALUES (2, 'Publisher', 8, 0);
INSERT INTO `user_role` (user_role_id, user_role_name, user_role_mask, status) VALUES (3, 'Editor', 4, 0);
INSERT INTO `user_role` (user_role_id, user_role_name, user_role_mask, status) VALUES (4, 'Trainer', 2, 0);
SET FOREIGN_KEY_CHECKS=1

