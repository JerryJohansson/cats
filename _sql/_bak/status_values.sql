# DBTools DBMYSQL - MySQL Database Dump
#

SET FOREIGN_KEY_CHECKS=0;

# Dumping Table Structure for status_values

#
CREATE TABLE `status_values` (
  `id` int(11) NOT NULL auto_increment,
  `status_title` varchar(50) default NULL,
  `status_mask` int(11) default NULL,
  `status_type` varchar(50) default NULL,
  `status_sub_type` varchar(50) default NULL,
  `group_mask` int(11) default '0',
  `status` smallint(6) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
#
# Dumping Data for status_values
#
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (1, 'On', 1, 'switch', NULL, 0, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (2, 'Off', 0, 'switch', NULL, 0, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (3, 'Hide from menu', 0, 'node_mask', NULL, 192, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (4, 'Approval needed', 1, 'node_mask', NULL, 0, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (5, 'Reject Item', 256, 'node_mask', 'pending', 192, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (6, 'Publish Item', 128, 'node_mask', '', 192, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (7, 'Re-Publish Item', 132, 'node_mask', 'published', 192, 1);
INSERT INTO `status_values` (id, status_title, status_mask, status_type, status_sub_type, group_mask, status) VALUES (8, 'Archive', 32, 'test', 'published', 192, 1);
SET FOREIGN_KEY_CHECKS=1

