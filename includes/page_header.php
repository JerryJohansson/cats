<html>

<head> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="X-UA-Compatible" content="IE11, IE=10, IE=9, IE=8, IE=7" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE11" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE10" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>CATS[<?php echo $server_name;?>] - <?PHP if(isset($page_title)) echo $page_title; ?></title>

<link rel="shortcut icon" href="style/favicon.ico" type="image/x-icon" />


<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/jquery-ui.min.css">
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-datetimepicker.css">

	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
    <![endif]-->

<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
<script src="bootstrap-3.3.2-dist/js/moment-with-locales.js"></script>
<script src="bootstrap-3.3.2-dist/js/bootstrap-datetimepicker.js"></script>

<?php if ($m!="login"){ ?>

<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>default.css" />

<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>toolbars.css" />

<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>css.php" title="Extra Cool2" id="main" />

<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>print.css" media="print" />

<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>forms.css" />

<?php } ?>

<script language="JavaScript" src="js/constants.php"></script>

<script language="JavaScript" src="js/global.js"></script>

<script language="JavaScript" src="js/form.js"></script>

<?php if (($p=="edit") || ($p=="new") || $p=="search"){ ?>



<?php } ?>


  <script src="js/foundation-datetimepicker-master/example/js/foundation-datetimepicker.js"></script>
  <link rel="stylesheet" href="js/foundation-datetimepicker-master/example/stylesheets/foundation-datepicker.css">
  <link rel="stylesheet" href="js/foundation-datetimepicker-master/example/stylesheets/example.css">
	
	
