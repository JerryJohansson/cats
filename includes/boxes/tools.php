<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Tools box menu
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
define('BOX_HEADING_TOOLS','Tools');
define('BOX_TOOLS_BACKUP','Backup Database');
define('BOX_TOOLS_BANNER_MANAGER','Banner Mangement');
define('BOX_TOOLS_MAIL','Mailout');
define('BOX_TOOLS_NEWSLETTER_MANAGER','Newsletter Management');
define('BOX_TOOLS_SERVER_INFO','Server Information');
define('BOX_TOOLS_WHOS_ONLINE','Who\'s Online');

?>
<!-- tools //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_TOOLS,
                     'link'  => ezw_href_link(FILENAME_BANNER_MANAGER, 'selected_box=tools'));

  if ($_SESSION['selected_box'] == 'tools') {
    $contents[] = array('text'  => '<div id="sectionLinks">' . 
                                   ezw_admin_files_boxes(FILENAME_BANNER_MANAGER, BOX_TOOLS_BANNER_MANAGER, 'selected_box=tools') . 
                                   ezw_admin_files_boxes(FILENAME_MAIL, BOX_TOOLS_MAIL, 'selected_box=tools') .
                                   ezw_admin_files_boxes(FILENAME_NEWSLETTERS, BOX_TOOLS_NEWSLETTER_MANAGER, 'selected_box=tools') .
																	 ezw_admin_files_boxes(FILENAME_BACKUP, BOX_TOOLS_BACKUP, 'selected_box=tools') .
                                   ezw_admin_files_boxes(FILENAME_SERVER_INFO, BOX_TOOLS_SERVER_INFO, 'selected_box=tools') .
                                   //ezw_admin_files_boxes(FILENAME_WHOS_ONLINE, BOX_TOOLS_WHOS_ONLINE, 'selected_box=tools') . 
		'</div>');
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- tools_eof //-->
