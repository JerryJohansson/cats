<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Reports box menu
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
define('BOX_HEADING_REPORTS','Reports');
define('BOX_REPORTS_CAMPAIGNS_VIEWED','Campaigns Viewed');
define('BOX_REPORTS_CAMPAIGNS_SIGNUPS','Campaign Signups');
?>
<!-- reports //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array(
		'text'  => BOX_HEADING_REPORTS,
    'link'  => ezw_href_link(FILENAME_STATS_CAMPAIGNS_VIEWED, 'selected_box=reports')
	);

  if ($_SESSION['selected_box'] == 'reports') {
    $contents[] = array(
			'text'  =>	'<div id="sectionLinks">' . ezw_admin_files_boxes(FILENAME_STATS_CAMPAIGNS_VIEWED, BOX_REPORTS_CAMPAIGNS_VIEWED) .
									ezw_admin_files_boxes(FILENAME_STATS_CAMPAIGNS_SIGNUPS, BOX_REPORTS_CAMPAIGNS_SIGNUPS) .
			'</div>'
		);
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- reports_eof //-->
