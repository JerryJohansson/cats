<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Administrator Menus
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
?>
<!-- catalog //-->
          <tr>
            <td>
<?php
	if(ezw_admin_files_access(FILENAME_ADMIN_GROUPS))
		$administrator_default_filename = FILENAME_ADMIN_GROUPS;
	elseif(ezw_admin_files_access(FILENAME_ADMIN_FILES))
		$administrator_default_filename = FILENAME_ADMIN_FILES;
	elseif(ezw_admin_files_access(FILENAME_USERS))
		$administrator_default_filename = FILENAME_USERS;
		
	
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_ADMINISTRATOR,
                     'link'  => ezw_href_link($administrator_default_filename, 'selected_box=administrator'));

  if ($selected_box == 'administrator') {
    $contents[] = array('text'  => '<div id="sectionLinks">' . ezw_admin_files_boxes(FILENAME_ADMIN_GROUPS, BOX_ADMINISTRATOR_GROUPS) .
                                   ezw_admin_files_boxes(FILENAME_ADMIN_FILES, BOX_ADMINISTRATOR_BOXES) . 
																	 ezw_admin_files_boxes(FILENAME_USERS, 'Current Users') . 
																	 ezw_admin_files_boxes(FILENAME_USERS_PENDING, 'Pending Users') . 
																	 ezw_admin_files_boxes(FILENAME_USERS_ARCHIVED, 'Archived Users') . 
																	 '</div>');
  }

  $box = new Box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- catalog_eof //-->
