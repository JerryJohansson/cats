<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Administrator Menus
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
?>
<!-- catalog //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => "Orders",
                     'link'  => ezw_href_link(FILENAME_ORDERS, 'selected_box=orders'));

  if ($selected_box == 'orders') {
    $contents[] = array('text'  => '<div id="sectionLinks">' . ezw_admin_files_boxes(FILENAME_ORDERS, "Orders") .
                                   ezw_admin_files_boxes(FILENAME_ORDERS_INVOICE, "Invoices") . 
																	 ezw_admin_files_boxes(FILENAME_ORDERS_PACKINGSLIP, 'Packing Slips') . 
																	 ezw_admin_files_boxes(FILENAME_ORDERS_STATUS, 'Order Status') . 
																	 '</div>');
  }

  $box = new Box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- catalog_eof //-->
