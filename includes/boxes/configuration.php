<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Configuration box menu
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
define('BOX_HEADING_CONFIGURATION','Configuration');
?>
<!-- configuration //-->
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_CONFIGURATION,
                     'link'  => ezw_href_link(FILENAME_CONFIGURATION, 'gID=1&selected_box=configuration'));

  if ($_SESSION['selected_box'] == 'configuration') {
    $cfg_groups = '<div id="sectionLinks">';
    $configuration_groups_query = ezw_db_query("select configuration_group_id as cgID, configuration_group_title as cgTitle from " . TABLE_CONFIGURATION_GROUP . " where visible = '1' order by sort_order");
    while ($configuration_groups = ezw_db_fetch_array($configuration_groups_query)) {
      $cfg_groups .= '<a href="' . ezw_href_link(FILENAME_CONFIGURATION, 'gID=' . $configuration_groups['cgID'], 'NONSSL') . '">' . $configuration_groups['cgTitle'] . '</a>';
    }
		$cfg_groups .= '</div>';
    $contents[] = array('text'  => $cfg_groups);
  }

  $box = new Box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
<!-- configuration_eof //-->
