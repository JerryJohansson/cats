<?php

/*
	@version   v5.20.12  30-Mar-2018
	@copyright (c) 2000-2013 John Lim (jlim#natsoft.com). All rights reserved.
	@copyright (c) 2014      Damien Regad, Mark Newnham and the ADOdb community
	  Released under both BSD license and Lesser GPL library license.
	  Whenever there is any discrepancy between the two licenses,
	  the BSD license will take precedence.
	  Set tabs to 4 for best viewing.

  	This class provides recordset pagination with
	First/Prev/Next/Last links.

	Feel free to modify this class for your own use as
	it is very basic. To learn how to use it, see the
	example in adodb/tests/testpaging.php.

	"Pablo Costa" <pablo@cbsp.com.br> implemented Render_PageLinks().

	Please note, this class is entirely unsupported,
	and no free support requests except for bug reports
	will be entertained by the author.

*/
class ADODB_Pager {
	var $id; 	// unique id for pager (defaults to 'adodb')
	var $db; 	// ADODB connection object
	var $sql; 	// sql used
	var $rs;	// recordset generated
	var $curr_page;	// current page number before Render() called, calculated in constructor
	var $rows;		// number of rows per page
    var $linksPerPage=10; // number of links per page in navigation bar
    var $showPageLinks;
    var $sort_enabled;
	var $module;
	var $action_page;
	var $caption_title;
	var $show_action_dropdown=false; // this is used as a flag to insert the hidden field used to post the operation of the dropdown box
	var $use_form_pager=false;
	var $linkArray=false;
	var $ignoreColumns=false;

	var $gridAttributes = 'cellspacing="0" border="0" rules="cols" class="results" width="100%" style="border-color:#ccc;"';
	var $gridColumnAttributes;

	// Localize text strings here
	var $first = '<code>|&lt;</code>';
	var $prev = '<code>&lt;&lt;</code>';
	var $next = '<code>>></code>';
	var $last = '<code>>|</code>';
	var $moreLinks = '...';
	var $startLinks = '...';
	var $gridHeader = false;
	var $htmlSpecialChars = true;
	var $page = 'Page';
	var $linkSelectedColor = 'red';
	var $cache = 0;  #secs to cache with CachePageExecute()

	//----------------------------------------------
	// constructor
	//
	// $db	adodb connection object
	// $sql	sql statement
	// $id	optional id to identify which pager,
	//		if you have multiple on 1 page.
	//		$id should be only be [a-z0-9]*
	//
	public function __construct(&$db,$sql,$id = 'cats', $showPageLinks = true, $columnAttributes = false, $sort_enabled = true, $order_by_id = '', $order_direction = '')
	{
	global $PHP_SELF;

		$curr_page = $id.'_curr_page';
		if (!empty($PHP_SELF)) $PHP_SELF = htmlspecialchars($_SERVER['PHP_SELF']); // htmlspecialchars() to prevent XSS attacks

		$this->action_page = (isset($_GET['p'])) ? $_GET['p']:"index";
		$this->module = (isset($_GET['m'])) ? $_GET['m']:"index";
		$this->sess_sort_id = $this->module.$this->action_page;
		
		$sess_curr_page = $this->sess_sort_id.$curr_page;

		$this->sql = $sql;
		$this->id = $id;
		$this->db = $db;
		$this->showPageLinks = $showPageLinks;
		$this->gridColumnAttributes = $columnAttributes;
		$this->sort_enabled = $sort_enabled;

		if($this->sort_enabled==true){
			
			$fsort_by=$id.'_sort_by';
			$fsort_dir=$id.'_dir';
			$sess_sort_by = $this->sess_sort_id.$fsort_by;
			$sess_sort_dir = $this->sess_sort_id.$fsort_dir;
			
			if(!(isset($_SESSION[$sess_sort_by])) && $order_by_id!=''){
				$_SESSION[$sess_sort_by] = $order_by_id;
			}else if(isset($_GET[$fsort_by])){
				$_SESSION[$sess_sort_by] = $_GET[$fsort_by];
			}
			
			 
			if(!(isset($_SESSION[$sess_sort_dir])) && $order_direction!=''){
				$_SESSION[$sess_sort_dir] = $order_direction;
			}else if(isset($_GET[$fsort_dir])){
				$_SESSION[$sess_sort_dir] = $_GET[$fsort_dir];
			}
			
			if (!empty($_SESSION[$sess_sort_dir])) $this->order_dir = $_SESSION[$sess_sort_dir];
			if (!empty($_SESSION[$sess_sort_dir])) $this->order_by = $_SESSION[$sess_sort_by];
			//echo("<br>order by=".$this->order_by."<br>order dir=".$this->order_dir);
			$ok = $this->setSortOrder();
			
		}

		$next_page = $id.'_next_page';

		if (isset($_GET[$next_page])) {
			$_SESSION[$sess_curr_page] = (integer) $_GET[$next_page];
		}
		if (empty($_SESSION[$sess_curr_page])) $_SESSION[$sess_curr_page] = 1; ## at first page

		$this->curr_page = $_SESSION[$sess_curr_page];

	}

	function setSortOrder(){
		if($this->sort_enabled==true){
			$sql = $this->sql;
			if(!preg_match("/( ORDER BY )/i",$sql)){
				$this->sql .= " ORDER BY " . $this->order_by . " " . $this->order_dir . " ";
				return true;
			}
		}
		return false;
	}

	function setColumnAttributes($val){
		$this->gridColumnAttributes = $val;
	}

	//---------------------------
	// Display link to first page
	function Render_First($anchor=true)
	{
	global $PHP_SELF;
		if ($anchor) {
	?>
		<a href="javascript:_m.pager('<?php echo $this->id;?>_next_page=1');"><?php echo $this->first;?></a> &nbsp;
	<?php
		} else {
			print "$this->first &nbsp; ";
		}
	}

	//--------------------------
	// Display link to next page
	function render_next($anchor=true)
	{
	global $PHP_SELF;

		if ($anchor) {
		?>
		<a href="javascript:_m.pager('<?php echo $this->id,'_next_page=',$this->rs->AbsolutePage() + 1 ?>');"><?php echo $this->next;?></a> &nbsp;
		<?php
		} else {
			print "$this->next &nbsp; ";
		}
	}

	//------------------
	// Link to last page
	//
	// for better performance with large recordsets, you can set
	// $this->db->pageExecuteCountRows = false, which disables
	// last page counting.
	function render_last($anchor=true)
	{
	global $PHP_SELF;

		if (!$this->db->pageExecuteCountRows) return;

		if ($anchor) {
		?>
			<a href="javascript:_m.pager('<?php echo $this->id,'_next_page=',$this->rs->LastPageNo() ?>');"><?php echo $this->last;?></a> &nbsp;
		<?php
		} else {
			print "$this->last &nbsp; ";
		}
	}

	//---------------------------------------------------
	// original code by "Pablo Costa" <pablo@cbsp.com.br>
        function render_pagelinks()
        {
        global $PHP_SELF;
            $pages        = $this->rs->LastPageNo();
            $linksperpage = $this->linksPerPage ? $this->linksPerPage : $pages;
            for($i=1; $i <= $pages; $i+=$linksperpage)
            {
                if($this->rs->AbsolutePage() >= $i)
                {
                    $start = $i;
                }
            }
			$numbers = '';
            $end = $start+$linksperpage-1;
			$link = $this->id . "_next_page";
            if($end > $pages) $end = $pages;


			if ($this->startLinks && $start > 1) {
				$pos = $start - 1;
				$numbers .= "<a href=\"javascript:_m.pager('$link=$pos');\">$this->startLinks</a>  ";
            }

			for($i=$start; $i <= $end; $i++) {
                if ($this->rs->AbsolutePage() == $i)
                    $numbers .= "<font color=$this->linkSelectedColor><b>$i</b></font>  ";
                else
                   $numbers .= "<a href=\"javascript:_m.pager('$link=$i');\">$i</a>  ";

            }
			if ($this->moreLinks && $end < $pages)
				$numbers .= "<a href=\"javascript:_m.pager('$link=$i');\">$this->moreLinks</a>  ";
            print $numbers . ' &nbsp; ';
        }
	// Link to previous page
	function render_prev($anchor=true)
	{
	global $PHP_SELF,$MODULE,$PAGE;
		if ($anchor) {
			if($MODULE!='') $q.='&m='.$MODULE;
			if($PAGE!='') $q.='&p='.$PAGE;
	?>
		<a href="javascript:_m.pager('<?php echo $this->id,'_next_page=',$this->rs->AbsolutePage() - 1 ;?>');"><?php echo $this->prev;?></a> &nbsp;
	<?php
		} else {
			print "$this->prev &nbsp; ";
		}
	}

	//--------------------------------------------------------
	// Simply rendering of grid. You should override this for
	// better control over the format of the grid
	//
	// We use output buffering to keep code clean and readable.
	function RenderGrid()
	{

	global $gSQLBlockRows; // used by rs2html to indicate how many rows to display
		include_once(ADODB_DIR.'/cats_tohtml.inc.php');
		ob_start();
		$gSQLBlockRows = $this->rows;
		$id = ($this->sort_enabled==true)?$this->id:"";
		$sess_sort_id = ($this->sort_enabled==true)?$this->sess_sort_id:"";
		if($this->use_form_pager){// var_dump($this->rs, $this->gridHeader);
		cats_rs2html($this->rs, $this->gridHeader, $this->gridColumnAttributes, $this->linkArray, $id, $sess_sort_id, $this->ignoreColumns, $this->htmlSpecialChars, true, $this->gridAttributes );
		}else{
			include_once(ADODB_DIR.'/tohtml.inc.php');
			rs2html($this->rs, $this->gridAttributes, $this->gridHeader, $this->htmlSpecialChars, true, $id, $sess_sort_id, $this->gridColumnAttributes);
		}
		$s = ob_get_contents();
		ob_end_clean();
		return $s;
	}

	//-------------------------------------------------------
	// Navigation bar
	//
	// we use output buffering to keep the code easy to read.
	function RenderNav()
	{
		ob_start();
		if (!$this->rs->AtFirstPage()) {
			$this->Render_First();
			$this->Render_Prev();
		} else {
			$this->Render_First(false);
			$this->Render_Prev(false);
		}
        if ($this->showPageLinks){
            $this->Render_PageLinks();
        }
		if (!$this->rs->AtLastPage()) {
			$this->Render_Next();
			$this->Render_Last();
		} else {
			$this->Render_Next(false);
			$this->Render_Last(false);
		}
		$s = ob_get_contents();
		ob_end_clean();
		return $s;
	}

	//-------------------
	// This is the footer
	function RenderPageCount()
	{
		$max_row_count=$this->rows;
		if (!$this->db->pageExecuteCountRows) return '';
		$lastPage = $this->rs->LastPageNo();
		if ($lastPage == -1) $lastPage = 1; // check for empty rs.
		if ($this->curr_page > $lastPage) $this->curr_page = 1;
		$num_rows=$this->rs->MaxRecordCount();
		$rec_count=$this->rs->RecordCount();
		$rec_curr_count=($max_row_count*($this->curr_page-1)+$rec_count);
		$s = "<font size=-1>$this->page ".$this->curr_page."/".$lastPage."</font> - Viewing records <b>".($rec_curr_count-$rec_count+1)."</b> to <b>".($rec_curr_count)."</b> of <b>".$num_rows."</b>";
		// check for the delete flag
		$sql = $this->sql;
		if(preg_match("/delete:(\w+|):/i",$sql,$sub_arr)){
			if($sub_arr[1]==''){
				$this->show_action_dropdown = true;
				$s.='</td><td><span style="float:right;"><select name="'.CATS_FORM_ACTION_SELECT.'" onchange="_m.select_action(this);" class="sfield">
		<option>Select an Action</option>
		<option value="del">Delete Selected</option></select></span>';
		//<option value="open">Status Open</option>
		//<option value="close">Status Close</option>
		
			}
		}
		$s.='</td></tr></table>';
		return $s;
	}

	//-----------------------------------
	// Call this class to draw everything.
	function Render($rows=10)
	{
	global $ADODB_COUNTRECS;
	
		$this->rows = $rows;
		
		if ($this->db->dataProvider == 'informix') $this->db->cursorType = IFX_SCROLL;
		
		$savec = $ADODB_COUNTRECS;
		if ($this->db->pageExecuteCountRows) $ADODB_COUNTRECS = true;
		if ($this->cache)
			$rs = $this->db->CachePageExecute($this->cache,$this->sql,$rows,$this->curr_page);
		else
			$rs = $this->db->PageExecute($this->sql,$rows,$this->curr_page);
		$ADODB_COUNTRECS = $savec;
		//print_r($rs);
		//var_dump($this->db->ErrorMsg()); //support dev
		$this->rs = &$rs;
		if (!$rs) {
			print "<h3>Query failed: $this->sql</h3>";
			return;
		}
		
		if (!$rs->EOF && (!$rs->AtFirstPage() || !$rs->AtLastPage())) 
			$page_nav = $this->RenderNav();
		else
			$page_nav = "&nbsp;";
		$page_nav = '<table width="100%"><tr><td width="95%">'.$page_nav;
		
		$grid = $this->RenderGrid();
		$page_count = $this->RenderPageCount();

		$this->RenderLayout($page_nav.$page_count, $grid, $page_nav.$page_count);

		$rs->Close();
		$this->rs = false;
	}

	//------------------------------------------------------
	// override this to control overall layout and formating
	function RenderLayout($header,$grid,$footer,$attributes='cellspacing="0" border="0" class="admin" width="100%"')
	{
		$extra_html = "";
		if($this->show_action_dropdown) $extra_html .= '<input type="hidden" name="'.CATS_FORM_ACTION_SELECT.'_act" />';
		$title = (!empty($this->caption_title))?'<caption id="tog"><a name="todo" onclick="this.blur();">' . $this->caption_title . '</a></caption>':'';
		echo '<fieldset class="tbar" style="margin-bottom:10px;">',
			'<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">',
			$title,
			'<tr><td>',
			$header,
			'</td></tr><tr><td>',
				$grid,
			'</td></tr><tr><td>',
				$footer,
			'</td></tr></table>',
			'</fieldset>',
				$extra_html;
	}
}
