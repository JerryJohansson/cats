<?php 
/*
  V4.65 22 July 2005  (c) 2000-2005 John Lim (jlim@natsoft.com.my). All rights reserved.
  Released under both BSD license and Lesser GPL library license. 
  Whenever there is any discrepancy between the two licenses, 
  the BSD license will take precedence.
  
  Some pretty-printing by Chris Oxenreider <oxenreid@state.net>
*/ 
  
// specific code for tohtml
GLOBAL $gSQLMaxRows,$gSQLBlockRows,$ADODB_ROUND;

$ADODB_ROUND=4; // rounding
$gSQLMaxRows = 1000; // max no of rows to download
$gSQLBlockRows=100; // max no of rows per table block

/*
+------------------------------------------------------------------------
|
|  Function: RecordSet to HTML Table
|  Purposes: 
|		Convert a recordset to a html table. Multiple tables are generated
|		if the number of rows is > $gSQLBlockRows. This is because
|		web browsers normally require the whole table to be downloaded
|		before it can be rendered, so we break the output into several
|		smaller faster rendering tables.
|  Returns: html table or prints html table
|  Params: 
|		$rs: the recordset
|		$th_array: contains the replacement strings for the headers (optional)
|		$th_attributes: the table tag attributes (optional)
|		$link_array: array of links delimited by | - type|url|field_id
|		$htmlspecialchars: flag to print escaped html tags
|		$echo: flag to echo table or return table as string
|		$sort_id: id to use for sorting
|		$sess_sort_id
|		$ignore_columns
|		$table_attributes: main tables attributes
|		$row_attribs: attributes for each row
|	Usage:
|		$rs = db_query("select * from table");
|		cats_rs2html($rs, array('header 1','header 2'), array('width="50%"','width="50%"'));
+------------------------------------------------------------------------
*/
function cats_rs2html(&$rs, $th_array=false, $th_attributes=false, $link_array=false, $sort_id='', $sess_sort_id = '', $ignore_columns=false, $htmlspecialchars=true, $echo = true, $table_attributes='', $row_attribs=' onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);" ' )
{
	$s ='';$rows=0;$docnt = false;$delete_button=false;$edit_button=false;$delete_url='';$delete_id='';$edit_url='';$edit_id='';
	GLOBAL $gSQLMaxRows,$gSQLBlockRows,$ADODB_ROUND;

	if (!$rs) {
		printf(ADODB_BAD_RS,'cats_rs2html');
		return false;
	}
	
	$draw_column = array();
	
	if (!$table_attributes) $table_attributes = "BORDER='1' WIDTH='98%'";
	$typearr = array();
	$ncols = $rs->FieldCount();
	$hdr = "\n<TABLE COLS=$ncols $table_attributes>\n<TR>\n";
	$edit_button = $edit_url = $edit_id = $edit_close = $edit_protocol = [];
	for ($i=0; $i < $ncols; $i++) {	
		
		$field = $rs->FetchField($i);
		if(is_array($ignore_columns) && in_array($field->name, $ignore_columns))
			$draw_column[$i] = false;
		else
			$draw_column[$i] = true;
		
		if($draw_column[$i]==true){
			// comment by dev
			// $col_attribs = (is_array($th_attributes))? $th_attributes[$i]:"";
			$col_attribs = (is_array($th_attributes))? isset($th_attributes[$i])?$th_attributes[$i]:"":"";
			if ($field) {
				if ($th_array) $fname = $th_array[$i];
				else $fname = htmlspecialchars($field->name);	
				$typearr[$i] = $rs->MetaType($field->type,$field->max_length);
			} else {
				$fname = 'Field '.($i+1);
				$typearr[$i] = 'C';
			}
			if (strlen($fname)==0) $fname = '&nbsp;';
			
			$thattribs="";
			$type = $typearr[$i];
			switch($type) {
				case 'D':
					if($col_attribs=="") $thattribs=' align="left" ';
					else $thattribs=$col_attribs;
					break;
				case 'T':
					if($col_attribs=="") $thattribs=' align="left" ';
					else $thattribs=$col_attribs;
					break;
				case 'N':
					if($col_attribs=="") $thattribs=' align="right" ';
					else $thattribs=$col_attribs;
				case 'I':
					//$thattribs=' align="left" ';
					break;
				default:
					if ($th_array) { // add hyperlink support for editing and searching child items etc.
						if($col_attribs!="") $thattribs=$col_attribs;
						else $thattribs='';//' width="3%"';
						if (strpos(strtolower(":".$fname),"edit:")==1){
							$ex=explode(":",$th_array[$i]);
							$edit_button[$i]= $ex;
							$edit_protocol[$i]=($edit_button[$i][1])?$edit_button[$i][1].":":"";
							$fname = '';//$edit_button[0];
							$edit_url[$i] = $edit_button[$i][2];
							$edit_id[$i] = $edit_button[$i][3];
							if(count($edit_button[$i])>3)
								$edit_close[$i] = $edit_button[$i][4];
							else
								$edit_close[$i] = '';
						}
						//print_r($edit_button);
						if (strpos(strtolower(":".$fname),"delete:")==1){
							$delete_button=explode(":",$th_array[$i]);
							$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
							$fname = '';//$delete_button[0];
							$delete_url = $delete_button[2];
							$delete_id = $delete_button[3];
							if(count($delete_button)>3)
								$delete_close = $delete_button[4];
							else
								$delete_close = '';
						}
						//print_r($delete_button);
					}
			}
			
			if($sort_id!=''){
				$fsort_query="";
				
				// handle date sorting correctly
				switch($field->name){
					case 'INCIDENT_DATE':
						$fsort_name='REALDATE';
						break;
					default:
						$fsort_name=htmlspecialchars($field->name);
						break;
				}
				
				$fsort_by_name=$sort_id.'_sort_by';
				$fsort_dir_name=$sort_id.'_dir';
				$sess_sort_dir = $sess_sort_id.$fsort_dir_name;
				
				if($sort_found=isset($_SESSION[$sess_sort_dir])){
					$fsort_dir = $_SESSION[$sess_sort_dir];
				}else{
					$fsort_dir = "ASC";
				}
				
				switch($fsort_dir){
					case 'ASC':case 'asc':
						$fsort_dir='DESC';
						break;
					case 'DESC':case 'desc':default:
						$fsort_dir='ASC';
						break;
				}
				
				$fsort_query .= "$fsort_dir_name=$fsort_dir&";
				$fsort_query .= "$fsort_by_name=$fsort_name";
				if(!empty($fname))
					$fname='<a href="javascript:_m.pager_sort(\'' . $fsort_query . '\');">'.$fname.'</a>';
			}
			$hdr .= "	<th$thattribs>$fname</th>\n";
		} // draw column
	} // for loop fields
	
	
	$hdr .= "\n</TR>";
	if ($echo) print $hdr."\n\n";
	else $html = $hdr;
	
	// smart algorithm - handles ADODB_FETCH_MODE's correctly by probing...
	$numoffset = isset($rs->fields[0]) ||isset($rs->fields[1]) || isset($rs->fields[2]);
	$i_hilite = 0;
	while (!$rs->EOF) {
		
		$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
		$i_hilite++;
		
		$s .= "<TR valign=top $row_hilite $row_attribs>\n";
		
		for ($i=0; $i < $ncols; $i++) {
			if ($i===0) $v=($numoffset) ? $rs->fields[0] : reset($rs->fields);
			else $v = ($numoffset) ? $rs->fields[$i] : next($rs->fields);
			$field = $rs->FetchField($i);
			// comment dev
			// $type = $typearr[$i];
			$type = isset($typearr[$i])?$typearr[$i]:'';
			$value = '';
			
			if($draw_column[$i]==true){
				
				switch($type) {
					case 'D':
						if (empty($v)) $value = "&nbsp;";
						else if (!strpos($v,':')) $value = $rs->UserDate($v,CATS_SQL_DATE_FORMAT_SHORT) ."&nbsp;";
						else $value = $rs->UserTimeStamp($v,CATS_SQL_DATE_FORMAT_LONG) ."&nbsp;";
						break;
					case 'T':
						if (empty($v)) $value = "&nbsp;";
						else $value = $rs->UserTimeStamp($v,CATS_SQL_DATE_FORMAT_LONG) ."&nbsp;";
						break;
					case 'N':
						if (abs($v) - round($v,0) < 0.00000001) $v = round($v);
						else $v = round($v,$ADODB_ROUND);
					case 'I':
						$value = stripslashes((trim($v))) ."&nbsp;";
						break;
					default:
						if((int)strpos(":".$v,"edit:")==1){
							if($rs->Fields($edit_id[$i]) === NULL)
								$value = '&nbsp;';
							else{
							$value = "<a href='" . $edit_protocol[$i] . $edit_url[$i] . $rs->Fields($edit_id[$i]) . $edit_close[$i] . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[$i][0]) . "_bg.gif' border='0' /></a>";
							}
						}else if((int)strpos(":".$v,"delete:")==1){
							if($delete_protocol!=''){
								$value = "<a href='" . $delete_protocol . $delete_url . $rs->Fields($delete_id) . $delete_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($delete_button[0]) . "_bg.gif' border='0' /></a>";
							}else{
								$value = "<input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" />";
							}
						} else {																				if ((substr($v,0,2)=="\\\\") || (substr($v,1,2)==":\\")) {																if ($htmlspecialchars) $v = htmlspecialchars($v);																$value = $v;															} else {												
								if ($htmlspecialchars) $v = htmlspecialchars(trim($v));
								$v = trim($v);
								if (strlen($v) == 0) $v = '&nbsp;';
								// FIXED 12th March 2006, cameron@sandboxsw.com
								// Do not strip slashes if the output contains a file path
								// Otherwise we lose \\UNC paths
								//$v = str_replace("\\\\", "\\\\\\\\", $v);
								$value = str_replace("\n",'<br>',stripslashes($v));															}								
						}
				}
				
				if(is_array($link_array) && trim($value)!='&nbsp;'){
					$links = explode("|", isset($link_array[$i]) ? $link_array[$i] : null);
					if(count($links)>1){
						// link type, url, field_id 
						//print_r($links);
						switch($links[0]){
							case 'ctrl':
								// maybe add some type of control here liks checkbox or radio button
								break;
							case 'js':
								$value = "<a href=\"javascript:{$links[1]}".$rs->Fields($links[2]).");\">{$value}</a>";
								break;
							default:
								$value = "<a href=\"{$links[1]}".$rs->Fields($links[2])."\">{$value}</a>";
								break;
						}
					}
				}
				
				$s .= "	<td>{$value}</td>\n";
			} // draw column
		} // for
		
		$s .= "</TR>\n\n";
			  
		$rows += 1;
		if ($rows >= $gSQLMaxRows) {
			$rows = "<p>Truncated at $gSQLMaxRows</p>";
			break;
		} // switch

		$rs->MoveNext();
	
		// additional EOF check to prevent a widow header
		if (!$rs->EOF && $rows % $gSQLBlockRows == 0) {
			if ($echo) print $s . "</TABLE>\n\n";
			else $html .= $s ."</TABLE>\n\n";
			$s = $hdr;
		}
	 } // while

	if ($echo) print $s."</TABLE>\n\n";
	else $html .= $s."</TABLE>\n\n";
	
	if ($docnt) if ($echo) print "<H2>".$rows." Rows</H2>";
	
	return ($echo) ? $rows : $html;
}
 
?>