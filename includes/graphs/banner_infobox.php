<?php
/*
  $Id: banner_infobox.php,v 1.1.1.1 2005/06/08 09:39:38 Vern Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  include(DIR_WS_CLASSES . 'phplot.php');

  $stats = array();
  $banner_stats_query = ezw_db_query("select dayofmonth(banners_history_date) as name, banners_shown as value, banners_clicked as dvalue from " . TABLE_BANNERS_HISTORY . " where banners_id = '" . $banner_id . "' and to_days(now()) - to_days(banners_history_date) < " . $days . " order by banners_history_date");
  while ($banner_stats = ezw_db_fetch_array($banner_stats_query)) {
    $stats[] = array($banner_stats['name'], $banner_stats['value'], $banner_stats['dvalue']);
  }

  if (sizeof($stats) < 1) $stats = array(array(date('j'), 0, 0));
	
  $graph = new PHPlot(200, 220, DIR_FS_GRAPHS . 'banner_infobox-' . $banner_id . '.' . $banner_extension);

  $graph->SetFileFormat($banner_extension);
  $graph->SetIsInline(1);
  $graph->SetPrintImage(0);

  $graph->draw_vert_ticks = 0;
  $graph->SetSkipBottomTick(1);
  $graph->SetDrawXDataLabels(0);
  $graph->SetDrawYGrid(0);
	$plot_type = 'bar'; // bars,lines,linepoints,area,points,pie,thinbarline
  $graph->SetPlotType($plot_type); // bars,lines,linepoints,area,points,pie,thinbarline
	
	if($plot_type=='thinbarline') $graph->SetDataType('data-data');
	$graph->SetImageBorderType('raised');
	$graph->SetDrawPlotAreaBackground(1);
  
	
		$graph->SetPlotBgColor(array(0,222,222));
		$graph->SetBackgroundColor(array(200,222,222)); //can use rgb values or "name" values
		$graph->SetLabelColor('black');
		$graph->SetTextColor('black');
		$graph->SetGridColor('black');
		$graph->SetLightGridColor(array(175,175,175));
		$graph->SetTickColor('black');
		$graph->SetTitleColor(array(0,0,0)); // Can be array or name
		$graph->data_color = array('blue','green','yellow','red','orange');
		$graph->error_bar_color = array('blue','green','yellow','red','orange');
		$graph->data_border_color = array('black');
	

	$graph->SetDrawDataLabels(1);
  $graph->SetLabelScalePosition(1);
  $graph->SetMarginsPixels(15,15,15,30);

  $graph->SetTitleFontSize('5');
  $graph->SetTitle(TEXT_BANNERS_LAST_3_DAYS);

  $graph->SetDataValues($stats);
  $graph->SetDataColors(array('blue','red'),array('blue', 'red'));

  $graph->DrawGraph();

  $graph->PrintImage();
?>
