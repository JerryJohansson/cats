<style>

  
    #menu {
        padding-left: 16px;
        padding-right: 16px;
        z-index: 9999;
    }
     
    .menu {
        border:none;
        border:0px;
        margin:0px;
        padding:0px;
        font-family: Tahoma;
        font-size:14px;
        text-align:center;
        z-index: 9999;
    }
         
    .menu ul {
        background:#FFFFFF;
        height:35px;
        list-style:none;
        margin:0;
        padding:0;
        border: 0px;
        text-align:center;
    }
     
    .menu li {
        float: left;
        padding:0px;
        text-align: center;
        border: 0px;
        width: inherit;
    }
     
    .menu li a {
        background:#F0F0F0 url("images/seperator.gif") bottom right no-repeat;
        color:#000000;
        display:block;
        line-height:35px;
        margin:0px;
        padding:0px 25px;
        text-align:center;
        text-decoration:none;
        border: 0px;
    }
    .menu li a:hover, .menu ul li:hover a {
        background: #999999 url("images/hover.gif") bottom center no-repeat;
        color:#000000;
        text-decoration:none;
        border: 0px;
    }
     
    .menu li ul {
        background:#F0F0F0;  
        display:none;
        height:auto;
        padding:0px;
        margin:0px;
        border:0px;
        position: absolute;
        width: auto;
        z-index:9999;
        text-align: center;
        border: 0px;
        /*top:1em;
        /*left:0;*/
    }
    .menu li:hover ul {
        display:block;
        border: 0px;
        text-align:center;
    }
     
    .menu li li {
        background:url('images/sub_sep.gif') bottom left no-repeat;
        display:block;
        float:none;
        margin:0px;
        padding:0px;
        width:225px;
        border: 0px;
        text-align:left;
    }
     
    .menu li:hover li a {
        background:none;
        border: 0px;
        text-align:left;
    }
         
    .menu li ul a {
        display:block;
        height:35px;
        font-size:12px;
        margin:0px;
        border: 0px;
        padding:0px 10px 0px 15px;
        text-align:left;
        visited: 
    }
             
    .menu li ul a:hover, .menu li ul li:hover a {
        background:#999999 url('images/hover_sub.gif') center left no-repeat;
        border:0px;
        color: #FFFFFF;
        text-decoration:none;
        text-align:left;
        
    }
     
    .menu p {
        clear:left;
        border: 0px;
        text-align: left;
    }  

    .menu a:visited {
        text-decoration: none;
    }  
  
	#newbar a, .newbar a, #newbar a:visited, .newbar a:visited, .tool_bar a, #tool_bar a, .tool_bar a:visited, #tool_bar a:visited {

		display: block; 

		color:#000000;  

		float: left;  

		background-repeat: no-repeat;  
 
		background-position: left;  

		border: 1px solid buttonface;  

		padding: 5px 5px 5px 23px;   

		text-decoration: none; 

	}  
  
	#top_personalnew
	{
		background-image: url("style/cats/images/moon2.png");
		background-repeat:no-repeat;
		width: 350px;
		height:54px;
		/*display: block;*/
		
		position:absolute; 

		float:right; 

		z-index: 10000; 

		top: 0px; 

		right: 0px;
		
	}
  
  </style>
  
  <script type="text/javascript">

    function openMenu(id) {
        document.getElementById(id).style.display = "block";
    }

    function closeMenu(id) {
        document.getElementById(id).style.display = "none";
    }

</script>

      <table width="100%">
        <tr>
          <td align="left">
            <a href="javascript:doMenu('index','dashboard');"><img src="style/cats/images/cats2.png" style="height: 54px"/></a>
          </td>
          <td align="centre">
          
              <div id="menu">
                  <div class="menu">
                      <ul>
                          <li>
                            <img src="style/cats/icons/menu_left.png" style="height: 35px"/>
                          </li>
                          <li><a href="javascript:top.show_screen('dashboard','search')" onmouseover="openMenu('item1');" onmouseout="closeMenu('item1');">CATS</a>
                            <ul id="item1" onmouseover="openMenu('item1');" onmouseout="closeMenu('item1');">
                                  <li><a href="javascript:top.show_screen('actions','search')">Actions</a></li>
                                  <li><a href="javascript:top.show_screen('audits_and_inspections','search')">Audits & Inspections</a></li>
                                  <li><a href="javascript:top.show_screen('government_requirements','search')">Government Inspections</a></li>
                                  <li><a href="javascript:top.show_screen('major_hazards','search')">High Level Business Risks</a></li>
                                  <li><a href="javascript:top.show_screen('incidents','search')">Incidents</a></li>
                                  <li><a href="javascript:top.show_screen('meeting_minutes','search')">Meeting Minutes</a></li>
                                  <li><a href="javascript:top.show_screen('schedules','search')">Schedules</a></li>
                                  <li><a href="javascript:top.show_screen('site_specific','search')">Site Specific Obligations</a></li>
                                  <li><a href="javascript:top.show_screen('dashboard','search')">Workload Review</a></li>
                                  <li>----------------------------------------</li>
                                  <li><a href="javascript:top.show_screen('other_reports','search')">Other Records</a></li>
                            </ul>
                        </li>
                     <li> 
					  <!-- <a href="javascript:top.show_screen('early_advice','search')">Early Advice</a>-->
					    <li><a href="javascript:top.show_screen('dashboard','search')" onmouseover="openMenu('item5');" onmouseout="closeMenu('item5');">Early Advice</a>
						  <ul id="item5" onmouseover="openMenu('item5');" onmouseout="closeMenu('item5');">
                                  <li><a href="javascript:top.show_screen('early_advice','trigger_new');">New </a></li>
								  <li><a href="javascript:top.show_screen('early_advice' ,'search')">Search</a></li>
						  </ul>	  
                      </li>
                          <li><a href="#" onmouseover="openMenu('item2');" onmouseout="closeMenu('item2');">Process Change Request</a>
                            <ul id="item2" onmouseover="openMenu('item2');" onmouseout="closeMenu('item2');">
                                    <li><a href="javascript:top.show_screen('pcr_maintain_actions','search')">Maintain Actions</a></li>
                                    <li><a href="javascript:top.show_screen('pcr_maintain_email','search')">Maintain Email Messages</a></li>
                                    <li><a href="javascript:top.show_screen('pcr_meetings','search')">Maintain Meetings</a></li>
                                    <li><a href="javascript:top.show_screen('pcr_maintain_reviewers','search')">Maintain Reviewers</a></li>
                                    <li><a href="javascript:top.show_screen('pcr_site_areas','search')">Maintain PCR Site Areas</a></li>
                                    <li><a href="javascript:top.show_screen('pcr_search','search')">Search PCRS</a></li>
                            </ul>
                        </li>


                     <li><a href="#" onmouseover="openMenu('item3');" onmouseout="closeMenu('item3');">Reports</a>
                            <ul id="item3" onmouseover="openMenu('item3');" onmouseout="closeMenu('item3');">
                                    <li><a href="javascript:window_withfocus('http://boprd/','boprd')">BO-Reports</a></li>

                            <?php  
                                if(cats_user_is_super_administrator() || cats_user_is_incident_analysis() || cats_user_is_editor(true)){	
                            ?>						
                                    <li> 
                                        <a href="javascript:void(window.open('<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/DWProWatch.aspx','Statistical_Analysis'))">Statistical Analysis</a>
                                    </li>
                            <?php } ?>


                                    <li><a href="javascript:window_withfocus('https://app.powerbi.com/groups/2a395363-d7fe-44f4-b6f1-abef0ca224fb/list/reports','powerBi')">Power-BI Reports</a></li>
                                    <li><a href="javascript:window_withfocus('https://app.powerbi.com/groups/2a395363-d7fe-44f4-b6f1-abef0ca224fb/list/dashboards','dashboards')">Power-BI Dashboards</a></li>
                            </ul>
        
                      </li>



                          
                        <li><a href="#" onmouseover="openMenu('item4');" onmouseout="closeMenu('item4');">Admin</a>
                            <ul id="item4" onmouseover="openMenu('item4');" onmouseout="closeMenu('item4');">
                                    <li><a href="javascript:top.show_screen('employees','search')">Employees</a></li>
                                    <li><a href="javascript:top.show_screen('lost_days','search')">Lost Days</a></li>
                                    <li><a href="javascript:top.show_screen('references','search')">Reference Table</a></li>
                                    <li><a href="javascript:top.show_screen('reallocate','search')">Re-Allocate responsibility</a></li>
                                    <li><a href="javascript:top.show_screen('work_hours','search')">Work Hours</a></li>
                                    <?php  
                                        if(cats_user_is_super_administrator()){    
                                    ?>                        
                                        <li><a href="javascript:top.show_screen('organisational_structure','search')">Sites</a></li>
                                    <?php } ?>
                            </ul>
        
                      </li>
                      <li>
                          <img src="style/cats/icons/menu_right.png" style="height: 35px"/>
                      </li>
                      </ul>
                         

                  </div>
              </div>
           
            </td>
            <td align="right">
			  
				<div id="top_personal" class="bar" style="width: 350px ; background-image:('style/cats/images/moon2.png'); no-repeat 0 100%">
				
					<table style="width:100px; background-color:white">
						<tr>
							<td>
								<a
									href="javascript:_hide_message();"
									class="top_right"
									style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_unlock.gif); z-index: 100000; "></a>
							</td>
							<td>
								<a
									title="My Employee Details"
									class="top_right"
									href="javascript:_account();"
									style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_my_employee_details_bg.gif); z-index: 100000; "></a>
							</td>
							<td>
								<a
									title="My History"
									class="top_right"
									href="javascript:gui.session.history.display();"
									style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_my_history_bg.gif); z-index: 100000; "></a>
							</td>
							<td>
								<a
									title="Submit a Bug/Feature Request"
									class="top_right"
									href="javascript:open_bugz_tracker();"
									style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_bug_bg.gif); z-index: 100000; "></a>
							</td>
							<td>
								<a
									title="Logout"
									class="top_right"
									href="javascript:window.close();"
									style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_close_bg.gif); z-index: 100000; "></a>
							</td>
							<td>
								&nbsp;
							</td>
							<td>
								&nbsp;
							</td>
							<td>
								&nbsp;
							</td>
							<td>
								<img src="style/cats/images/moon2.png" style="height: 54px" />
							</td>
						</tr>
					</table>

				<!--	<img src="style/cats/images/moon2.png" style="height: 54px" style="" />-->
					
				</div>			  
					  
					</td>
				  </tr>
				</table>