<?php
$s_ob_get_contents = ob_get_contents(); 
ob_end_clean();// comment dev

?>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>CATS[test] - <?PHP if(isset($page_title)) echo $page_title; ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>toolbars.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>forms.css" />
<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>css.php" title="Extra Cool" id="main" />
<script language="JavaScript" src="js/constants.php"></script>
<script language="JavaScript" src="js/global.js"></script>
<script>
<?php
if(isset($results_iframe_target)){
	echo "FORM_TARGET = '$results_iframe_target';";
?>

window.onload = function(){
	// resize result iframe to consume the parent page
	init_resize_results_container();
	parent.scrollTo(0,0);
	init_document_handlers();
}
<?php
}else{
?>
window.onload = function(){
	init_document_handlers();
}
<?php
}
?>
</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><table id="msg_table_div" width="100%" height="100%" style="background:#fff;">
<tr>
	<td valign="top">
		<p>&nbsp;</p>
		<table class="admin" align="center" border="0" cellspacing="0" cellpadding="6" width="600" style="background:#fff;">
		<tr>
			<th colspan="2">
				<?php
				if(isset($page_title)){
					echo $page_title;
				}else{
					echo "CATS: Message";
				}
				?>
			</th>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid #ccc;">
				<table>
				<tr>
					<td class="<?php echo($info_image);?>"></td>
					<td>
						<?php //echo $s_ob_get_contents;?>
						<p><?PHP if (isset($_SESSION['messageStack']) && $_SESSION['messageStack']->size > 0) echo $_SESSION['messageStack']->output(); ?></p>
						<p>Please click the OK button below to continue</p>
						
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th colspan="2" align="center">
				<form name="form1" method="post" action="" onSubmit="return false">
				<center>
				<?php
				if(!isset($return_js_function)){
					if(!isset($success_remove_page)) $success_remove_page = 'edit';
					$return_js_function = isset($success_return_module)?"top._edit_success('$success_return_module','$m','$success_remove_page')":"top._edit_success('$m')";
				}
				?>
					<input type="button" class="button" name="Submit" value="OK" onClick="<?php echo($return_js_function);?>;">
					<input type="button" class="button" name="Cancel" value="Cancel" onClick="top.gui.session.history.back(2);">
					<input type="button" class="button" name="Back" value="Back" onClick="top.gui.session.history.back();">
				</center>
				</form>
			</th>
		</tr>
		</table>
		
	</td>
</tr>
</table>

<?php
	if($page_title == "Access Denied"){
	?>
	<script>
		$( function() {
			if($("#search_form")){
				
				if($("#msg_table_div"))
					$("#msg_table_div").hide();
				$("#search_form").html("<table class='admin' align='center' border='0' cellspacing='0' cellpadding='6' width='600' style='background:#fff;'><tr><th colspan='2'>Access Denied</th></tr><tr><td colspan='2' style='border-bottom:1px solid #ccc;'><table><tr><td class='halt'></td><td><p><div class='messageStackError'><span>You do not have access to view this function. </span></div></p><p>Please click the OK button below to continue</p></td></tr></table></td></tr><tr><th colspan='2' align='center'><form name='form1' method='post' action='' onSubmit='return false'><center><input type='button' class='button' name='Submit' value='OK' onClick='top._edit_success(\'pcr_meetings\');'><input type='button' class='button' name='Cancel' value='Cancel' onClick='top.gui.session.history.back(2);'><input type='button' class='button' name='Back' value='Back' onClick='top.gui.session.history.back();'></center></form></th></tr></table>");
				
			}	
		} );
	</script>
<?php }	?>

