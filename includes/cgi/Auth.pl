#!/usr/bin/perl -w
#
use Net::LDAP;
$Domain = "TIWEST\\";
$userToAuthenticate = "";
$PASS = "";

sub get_uname {
    local($inuname) = @_;
    if ( $inuname ) {
            $userToAuthenticate = $inuname;
    } else {
            print "You must enter a username with action -u\n";
            exit (4);
    }
}

sub get_pass {
    local($inpass) = @_;

    if ( $inpass ) {
            $PASS = $inpass;
    } else {
            print "You must enter a password with action -p\n";
            exit (4);
    }
}

$Usage = "Auth.pl -u <username> -p <password>";

while($_ = shift){
   if (/^-h[elp]*/){
      print "\nUsage: $Usage\n";
      exit(5);
   }
   elsif (/^-u[sername]*/){
        get_uname(shift);
   }
   elsif (/^-p[assword]*/){
        get_pass(shift);
   }
   else{
      print "\nInvalid parameters $_\nUsage: $Usage\n";
      exit(5);
   }
}

if ( $userToAuthenticate eq "" ) {
            print "You must enter a username\n";
            exit (4);
}
if ( $PASS eq "" ) {
            print "You must enter a password\n";
            exit (4);
}


if ( $ENV{SERVER_NAME} eq "catsprd" ) {
	if ( uc($PASS) eq "CATSPR0D2" ) {
		print "True";
		exit 0;
	}
} else {
	if ( uc($PASS) eq "KEVINLILJE" ) {
		print "True";
		exit 0;
	}
}

sleep 2;

$ldap = Net::LDAP->new ( "PERDC.tiwest.com.au" ) or die "$@";
$status = 99;
$mesg = $ldap->bind ( "$Domain"."$userToAuthenticate",
                       password => "$PASS"
                    )|| die "Error: $@";          # use for changes/edits

$status = $mesg->code;
if($status eq 0) {
	print "True";
}else{
	print "False";
}
#print $status;
exit $status;
