<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Menu Generation
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
require_once("includes/config.php");
//require_once("includes/classes/container.php");
require_once("includes/classes/categories.php");
$menucat = new Category;
$root_id = 0;
$result = $menucat->getChildren($root_id);

$menurows='';
$scriptvalues='';
$i=1;

//echo "hello"
// Some position vars
$top = 135; 				// Distance from top of page
$cell = 14;				// Size of each table cell
$between_cells = 5;		// Distance between each cell
$running_total = $top;	// Total from the top
$count = 0;				// Make sure we don't pad the top one

while($data = mysql_fetch_array($result))
{
	$count++;
	// Lets make some dodgy code!
	// This will count the chars and 'guess' how big the resulting table cell will be
	// for the purposes of menu positioning
	$this_total = 0;		// Position of this cell
	$word_array = explode(" ", stripslashes($data['title']));
	$maxchars = 23;
	$current_line = 0;
	$total_lines = 1;
	$current_word = 0;
	
	foreach($word_array as $key => $value)
	{
		// Do it
		$current_word = strlen($value);		// Length of the word
		if(($current_word+$current_line) >= $maxchars)
		{
			$total_lines++;
			$current_line = $current_word+1;	// Plus a space
		}
		else
		{
			$current_line += $current_word+1;
		}
	}
	
	// But wait! For safety ...
	if(strlen($data['title']) <= $maxchars)
		$total_lines = 1;
	
	// Now calculate the position
	if($count = 1)
	{
		$this_total = ($total_lines*$cell);
	}
	else
	{
		$this_total = ($total_lines*$cell)+$between_cells;
	}
	
	$menurows .= '<a 
		onMouseOver="popUp(\'elMenu'.$i.'\',event);" 
		onMouseOut="popDown(\'elMenu'.$i.'\');" 
		href="'.$data['type'].'.php?id='.$data['id'].'">'.stripslashes($data['title']).'</a>';
		
  $scriptvalues .= 'HM_Array'.$i.' = [
[100,180,'.$running_total.',,,,,,,0,0,0,1,1,1,"null","null",]';

	// Setup the next total
	$this_total = ($total_lines*$cell)+$between_cells;
	$running_total += $this_total;

    $result2 = $menucat->getChildren($data['id']);
    while($data2 = mysql_fetch_array($result2))
    {
        $scriptvalues .= ',
["'.stripslashes($data2['title']).'","'.$data2['type'].'.php?id='.$data2['id'].'",1,0,0]';
    }
    $i++;
    $scriptvalues .= '
]
';
}


function getMenusJSArrayByNode($id=0, $prefix='')
{
	$TopNodeID = 0;
	$MENU_TOP = 140;
	$MENU_LEFT = 0;
	$MENU_WIDTH = 150;
	$MENU_BORDER_WIDTH = 1;
	$MENU_SCREEN_POSITION = "left";
	// font_color, mouseover_font_color, background_color, mouseover_background_color, border_color, separator_color
	// top_is_permanent, top_is_horizontal, tree_is_horizontal, position_under, top_more_images_visible
	// tree_more_images_visible, evaluate_upon_tree_show, evaluate_upon_tree_hide, right_to_left, display_on_click
	$MENU_STYLE1 = "'#813F3F','white','#FFCC00','#813F3F','#813F3F','#813F3F',1,0,0,1,1,1,'null','null',,0";
	$MENU_STYLE2 = "'#813F3F','white','#FFCC00','#813F3F','#813F3F','#813F3F',1,1,0,1,1,1,'',''";
	$MENU_STYLE3 = "'#813F3F','white','#FFCC00','#813F3F','#813F3F','#813F3F',1,1,0,1,1,1,'null','null',,0";
	$MENU_TEXT_ALIGN = "left";
	$MenuClick = 0;
	$iIndex = 1;
	$order = "id ASC";
	
	$result = ezw_db_query("SELECT * FROM `page` WHERE `pid`='$id' ORDER BY $order") or die(mysql_error());
	while ($row = ezw_db_fetch_array($result))
	{
		if( $id == $TopNodeID )
		{
			$MenuTop = $MENU_TOP;
			$MenuLeft = $MENU_LEFT;//"'HM_f_CenterMenu(\"HM_Menu1\")'";
			$MenuWidth = "150";
			$MenuStyle = $MENU_STYLE1;
		}
		else
		{
			$MenuTop = 0;//22;
			$MenuLeft = 0;//5;
			$MenuWidth = $MENU_WIDTH;
			$MenuStyle = $MENU_STYLE2;
		}
		
		if(!isset($s)) $s =	"HM_Array" . $prefix . "=[[".$MenuWidth.",".$MenuLeft.",".$MenuTop.",".$MenuStyle."]";
		
		$SubMenu = getMenusJSArrayByNode($row['id'], $prefix . "_" . $iIndex);
		$HasChildren = ($SubMenu)?"1":"0";
		
		$s .= ",['" . (($row['id']!=1) ? addslashes($row['title']):"Home") . "', '" . $row['type'].".php?id=".$row['id'] . "',1,0," . $HasChildren . "]\n";
		
		$SubMenus .= $SubMenu;
		$iIndex++;
		
	}
	if(strlen($s)>0) $s .= "];\n".$SubMenus;
	return $s;
}
function getHM_Menus()
{
	ezw_db_connect();
	$returnVal = getMenusJSArrayByNode(0,1);
	ezw_db_close();
	return $returnVal;
}
$scriptvalues = getHM_Menus();
?>
