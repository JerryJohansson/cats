<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Configuration Menus
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

  if (ezw_admin_check_boxes('administrator.php') == true) {
    include(DIR_WS_BOXES . 'administrator.php');
  } 
  if (ezw_admin_check_boxes('configuration.php') == true) {
    include(DIR_WS_BOXES . 'configuration.php');
  }
	if (ezw_admin_check_boxes('articles.php') == true) {
    include(DIR_WS_BOXES . 'articles.php');
  } 
	if (ezw_admin_check_boxes('newsdesk.php') == true) {
    include(DIR_WS_BOXES . 'newsdesk.php');
  } 
  if (ezw_admin_check_boxes('catalog.php') == true) {
    include(DIR_WS_BOXES . 'catalog.php');
  } 
  if (ezw_admin_check_boxes('modules.php') == true) {
    include(DIR_WS_BOXES . 'modules.php');
  } 
  if (ezw_admin_check_boxes('orders.php') == true) {
    include(DIR_WS_BOXES . 'orders.php');
  } 
  if (ezw_admin_check_boxes('taxes.php') == true) {
    include(DIR_WS_BOXES . 'taxes.php');
  } 
  if (ezw_admin_check_boxes('localization.php') == true) {
    include(DIR_WS_BOXES . 'localization.php');
  } 
  if (ezw_admin_check_boxes('reports.php') == true) {
    include(DIR_WS_BOXES . 'reports.php');
  } 
  if (ezw_admin_check_boxes('tools.php') == true) {
    include(DIR_WS_BOXES . 'tools.php');
  }
	/*
	include(DIR_WS_BOXES . 'administrator.php');
	include(DIR_WS_BOXES . 'configuration.php');
	include(DIR_WS_BOXES . 'reports.php');
	include(DIR_WS_BOXES . 'tools.php');
	*/
?>
<!-- back to content //-->
          <tr>
            <td>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="admin" style="background:#fff;">
<caption id="tog"><a name="Back" href="e_frame.php" style="background-image:url(/images/icon_plus.gif);padding-left:24px;background-position:left;">Back To Content</a></caption>
</table>
            </td>
          </tr>
<!-- back to content_eof //-->
