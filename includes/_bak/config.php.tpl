<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Site Configuration - Main
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
// Set session handlers etc-----------------
ob_start();
$oldSessionName=session_name("ezw");
session_start();
setlocale (LC_ALL, 'ena');
putenv('TZ=Australia/Perth');
if(isset($_REQUEST['show_env']) && $_REQUEST['show_env']=='php_info') exit(phpinfo());
//echo('GROUPS:'.$_SESSION['user_details']['groups'].(192 & (int)($_SESSION['user_details']['groups']+0)).'<br>');
//------------------------------------------

// Start the clock for the page parse time log
define('PAGE_PARSE_START_TIME', microtime());
// Used in the "Backup Manager" to compress backups
define('LOCAL_EXE_GZIP', '/usr/bin/gzip');
define('LOCAL_EXE_GUNZIP', '/usr/bin/gunzip');
define('LOCAL_EXE_ZIP', '/usr/local/bin/zip');
define('LOCAL_EXE_UNZIP', '/usr/local/bin/unzip');
// Define all mask values used within the site
define('MASK_NORMAL', 0);					// Normal system object. No attributes are set.
define('MASK_READ_ONLY', 1);			// Read-only object.
define('MASK_HIDDEN', 2);					// This is a sa - admin feature only and is hidden or FSO(Hidden file).
define('MASK_SYSTEM', 4);					// System file.
define('MASK_DIRECTORY', 16);			// Folder or directory.
define('MASK_ARCHIVE', 32);				// This object has been archived or FSO(File has changed since last backup.)
define('MASK_HISTORY', 64);				// This object has been put in its parents history list
define('MASK_PUBLISHED', 128);		// This object is published and and has been backed up
define('MASK_REJECTED', 256);		// This object is published and and has been backed up
define('MASK_ALIAS', 1024);				// Link or shortcut.
define('MASK_COMPRESSED', 2048);	// Compressed object.
define('MASK_DELETED', 4096);			// The Deleted flag.
// Only allow the following groups into administration
define('EZW_ADMIN_MASK', 248);		// admin(128), publishers(64), editors(32), Marketing(16), trainers(8)
define('EZW_PUBLISH_MASK', 192);
define('EZW_EDITOR_MASK', 32);

// Define form constants
define('EZW_FORM_PREFIX','ezw::');
define('EZW_FORM_MAIL_SUBMIT',EZW_FORM_PREFIX.'submit');
define('EZW_FORM_MAIL_TYPE',EZW_FORM_PREFIX.'mid');
define('EZW_FORM_CAMPAIGN_TYPE', EZW_FORM_PREFIX.'cid');
define('EZW_FORM_RETURN_URL', EZW_FORM_PREFIX.'returnurl');

// Banners Count constants
define('BANNER_TOP_COUNT',1);
define('BANNER_LEFT_COUNT',1);
define('BANNER_RIGHT_COUNT',1);
define('BANNER_BOTTOM_COUNT',1);
// Banner type constants
define('BANNER_TOP_TYPE','468x60');
define('BANNER_LEFT_TYPE','160-left');
define('BANNER_RIGHT_TYPE','160-right');
define('BANNER_BOTTOM_TYPE','468x60-b');

/* Default Template ID */
define('DEFAULT_TEMPLATE_ID', 1);

define('DEFAULT_KEYWORDS','Worklink, Worklink Program, job placement, The Hills & Associated Colleges, online training, vocational career, education, training, australia, international, preparation, university, diplomas, certificates, short courses, single subjects, microsoft certification, linux, cisco, perth, western australia');
define('DEFAULT_DESCRIPTION','The Hills & Associated Colleges - Worklink Program is a leader in providing quality work experience placements, job placement using innovative hands on in the work place experience.');

/* Default Page Stuff */
define('DEFAULT_MENU_SYSTEM', 'hm'); // Default menu system = Brain Jar menus (mb, bj, hm(needs template change))

$CHILD_CREATION_DISABLED_LIST = array("news","schedule","testimonial");
$DELETE_DISABLED_LIST = array("news","schedule","testimonial");

$local_config=(isset($local_config_override_path) && $local_config_override_path!='')?$local_config_override_path:"includes/local/config.php";
if(file_exists($local_config)) {
//if(file_exists("includes/local/config.php")) {
	$domain_split=explode("/",$_SERVER['SCRIPT_NAME']);
	define('SERVER_DOMAIN',$domain_split[1]);
	define('SITE_IS_LIVE','false');
	
	include($local_config);
}else{
	define('SERVER_DOMAIN',$_SERVER['HTTP_HOST']);//preg_replace("/(www.)/i","",$_SERVER['HTTP_HOST']));
// define constants
	define('SITE_IS_LIVE','true');
	
// define main application constants
	define('HTTP_SERVER', 'http://'.SERVER_DOMAIN); // eg, http://localhost - should not be empty for productive servers
	define('HTTP_ADMIN_SERVER', 'http://'.SERVER_DOMAIN); // 
	define('DIR_FS_ROOT', '/home/httpd/vhosts/worklink.org.au/httpdocs/');//'/worklink_admin/httpdocs/');//'/home/httpd/vhosts/worklink.org.au/httpdocs/'); // where the pages are located on the server
	define('DIR_WS_ROOT', '/'); // absolute path required
	define('DIR_WS_PUBLIC', HTTP_SERVER.DIR_WS_ROOT);
	define('DIR_FS_ADMIN', DIR_FS_ROOT . 'ezadmin/'); // absolute path required
	define('DIR_WS_ADMIN', DIR_WS_ROOT.'ezadmin/'); // virtual path required
// define image and files upload paths and dirs
	define('DIR_WS_IMAGES', DIR_WS_ROOT . 'images/'); // relative paths only
	define('DIR_FS_IMAGES', DIR_FS_ROOT . 'images/');
	define('DIR_WS_GRAPHS', DIR_WS_IMAGES . 'graphs/'); // relative paths only
	define('DIR_FS_GRAPHS', DIR_FS_IMAGES . 'graphs/');
	define('DIR_WS_BANNERS', DIR_WS_IMAGES . 'banners/'); // relative paths only
	define('DIR_FS_BANNERS', DIR_FS_IMAGES . 'banners/');
	define('DIR_WS_GALLERY', DIR_WS_IMAGES . 'cms/');
	define('DIR_FS_GALLERY', DIR_FS_IMAGES . 'cms/');
	define('DIR_WS_THUMBS', DIR_WS_GALLERY . 'thumbs/');
	define('DIR_FS_THUMBS', DIR_FS_GALLERY . 'thumbs/');
	define('DIR_WS_FILES', DIR_WS_ROOT . 'assets/');
	define('DIR_FS_FILES', DIR_FS_ROOT . 'assets/');
// define application specific dirs
	define('DIR_FS_INCLUDES', DIR_FS_ROOT . 'includes/');
	
	define('IS_ADMIN',(strstr($REQUEST_URI, DIR_WS_ADMIN)!=""));
	//echo(IS_ADMIN);
	//echo($REQUEST_URI);
	if(IS_ADMIN)
	{
  	define('DIR_WS_INCLUDES', '../includes/');
		define('DIR_WS_TEMPLATES',HTTP_SERVER.DIR_WS_ADMIN.'templates/');
		define('DIR_WS_BACKUP', DIR_WS_ADMIN . 'backup/');
		define('DIR_FS_BACKUP', DIR_FS_ADMIN . 'backup/');
	}
	else
	{
		define('DIR_WS_INCLUDES', 'includes/');
		define('DIR_WS_TEMPLATES',DIR_WS_PUBLIC.'templates/');
		/*
		if(isset($_REQUEST['show_site'])){
			$_SESSION['is_live'] = true;
		}else{
			if(isset($_SESSION['is_live'])){
				if($_SESSION['is_live'] == false){
					require("index.htm");
					exit;
				}
			}else{
				$_SESSION['is_live'] = false;
			}
		}
		*/
	}
	define('DIR_WS_BOXES', DIR_WS_INCLUDES . 'boxes/');
	define('DIR_FS_BOXES', DIR_FS_INCLUDES . 'boxes/');
	define('DIR_WS_FUNCTIONS', DIR_WS_INCLUDES . 'functions/');
	define('DIR_WS_CLASSES', DIR_WS_INCLUDES . 'classes/');
	define('DIR_WS_MODULES', DIR_WS_INCLUDES . 'modules/');
	define('DIR_WS_LANGUAGES', DIR_WS_INCLUDES . 'languages/');
	
// define special files
	define('ADMIN_PAGE','index.php?');
	define('ADMIN_TEMPLATE', DIR_WS_TEMPLATES . 'main.htm');
// define any special files now that all app dirs are defined
	define('STORE_PAGE_PARSE_TIME_LOG','/var/log/page_parse_time_log.log');
// define our database connection
	define('DB_SERVER', 'localhost'); // eg, localhost - should not be empty for productive servers
	define('DB_SERVER_USERNAME', 'worklink_admin');
	define('DB_SERVER_PASSWORD', 'w0rkl1nk');
	define('DB_DATABASE', 'worklink_cms');
	define('USE_PCONNECT', 'false'); // use persistent connections?	
}

//print_r(get_defined_constants());
//--------------------------------------------------------------
if(!(isset($config_details_only) && $config_details_only==true)){ // if we want the main configuration details only, ignore definitions
	// include the database functions and table definitions
	require_once(DIR_WS_FUNCTIONS . 'database.php');
	require_once(DIR_WS_INCLUDES . 'database_tables.php');
	// include filename definitions
	require_once(DIR_WS_INCLUDES . 'filenames.php');
	
	// Get all configuration data from the db and define as constants
	// make a connection to the database... now
	ezw_db_connect() or die('Unable to connect to database server!');
	$configuration_query = ezw_db_query('select configuration_key as cfgkey, configuration_value as cfgval from configuration');
	while ($configuration = ezw_db_fetch_array($configuration_query)) {
	//echo($configuration['cfgkey'].':'.$configuration['cfgval'].'<br>');
		define($configuration['cfgkey'], $configuration['cfgval']);
	}
	// include general functions
	require_once(DIR_WS_FUNCTIONS . 'general.php');
	require_once(DIR_WS_FUNCTIONS . 'html_output.php');
	require_once(DIR_WS_FUNCTIONS . 'sessions.php');
	// include security functions
	require_once(DIR_WS_FUNCTIONS . 'security.php');
	require_once(DIR_WS_FUNCTIONS . 'banner.php');
	
	
	if(IS_ADMIN)
	{
		require_once(DIR_WS_FUNCTIONS . 'admin_general.php');
		require_once(DIR_WS_CLASSES."object_info.php");
		require_once(DIR_WS_CLASSES.'table.php');
		require_once(DIR_WS_CLASSES.'box.php');
		require_once(DIR_WS_CLASSES.'message_stack.php');
		$messageStack = new messageStack;
		$language = "english";
		require_once(DIR_WS_INCLUDES."languages/".$language.".php");
		if(!isset($_SESSION['selected_box']))
		{
			$_SESSION['selected_box'] = 'configuration';
		}
		if(isset($_REQUEST['selected_box']))
		{
			$_SESSION['selected_box'] = $_REQUEST['selected_box'];
		}
		
		define('DIR_WS_ICONS',DIR_WS_IMAGES.'icons/');
		// define more admin constants
		define('BOX_WIDTH','200');
		define('MAX_DISPLAY_SEARCH_RESULTS',20);
		define('MAX_DISPLAY_PAGE_LINKS',10);
	}
}
$_SESSION['theme'] = defined('TEMPLATE_DEFAULT_GROUP')?TEMPLATE_DEFAULT_GROUP:"worklink";
//print_r(get_defined_constants());
//exit;
//var_dump($_SESSION);
if(!isset($_SESSION['selected_menu']))
{
	$_SESSION['selected_menu'] = 'b_about_us';
}
if(isset($_REQUEST['selected_menu']))
{
	$_SESSION['selected_menu'] = $_REQUEST['selected_menu'];
}
define('SELECTED_MENU',$_SESSION['selected_menu']);
?>
