<?PHP
/*
+---------------------------------------------------------------------------
| CONFIG FILE - Always gets loaded first
+---------------------------------------------------------------------------
*/
// Set session handlers etc-----------------
ob_start();
$oldSessionName=session_name("cats");
session_start();
putenv('TZ=Australia/Perth');
//------------------------------------------
// Define all mask values used within the site
define('MASK_NORMAL', 0);					// Normal system object. No attributes are set.
define('MASK_READ_ONLY', 1);			// Read-only object.
define('MASK_HIDDEN', 2);					// This is a sa - admin feature only and is hidden or FSO(Hidden file).
define('MASK_SYSTEM', 4);					// System file.
define('MASK_DIRECTORY', 16);			// Folder or directory.
define('MASK_ARCHIVE', 32);				// This object has been archived or FSO(File has changed since last backup.)
define('MASK_HISTORY', 64);				// This object has been put in its parents history list
define('MASK_PUBLISHED', 128);		// This object is published and and has been backed up
define('MASK_ALIAS', 1024);				// Link or shortcut.
define('MASK_COMPRESSED', 2048);	// Compressed object.
define('MASK_DELETED', 4096);			// The Deleted flag.
define('ADMIN_SECURITY_MASK',16+32+46+128+256+512+1024+2048);
// Define form constants
define('CATS_FORM_PREFIX','cats::');
define('CATS_FORM_MAIL_SUBMIT',CATS_FORM_PREFIX.'submit');
define('CATS_FORM_MAIL_TYPE',CATS_FORM_PREFIX.'mid');
define('CATS_FORM_FUNCTION', CATS_FORM_PREFIX.'func');
define('CATS_FORM_RETURN_URL', CATS_FORM_PREFIX.'rurl');

$m=(isset($_REQUEST['m']))?$_REQUEST['m']:(isset($_SESSION['m'])?$_SESSION['m']:'dashboard');
$p=(isset($_REQUEST['p']))?$_REQUEST['p']:(isset($_SESSION['p'])?$_SESSION['p']:'index');
$a=(isset($_REQUEST['a']))?$_REQUEST['a']:(isset($_SESSION['a'])?$_SESSION['a']:'search');

$local_config="C:/Projects/vianet/cats/www_source/includes/local/conf.php";
if(file_exists($local_config)) {
	include($local_config);
}else{
	define('SERVER_DOMAIN',$_SERVER['HTTP_HOST']);
	define('SITE_IS_LIVE',true);

	define('CATS_ROOT_PATH','/var/www/html/');
	define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
	define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
	define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
	define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
	define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php	

	$config = parse_ini_file(CATS_INCLUDE_PATH."conf/default.ini");
	$config = array_merge($config, parse_ini_file(CATS_INCLUDE_PATH."conf/cats_".((isset($_SESSION['USER_GROUP']) && ($_SESSION['USER_GROUP'] & ADMIN_SECURITY_MASK)>0)?"admin":"user").".ini"));
	foreach ($config as $key => $value) {
		if(!defined($key))
			define($key, $value);
	}
}
//--------------------------------------------------------------
if(!(isset($config_details_only) && $config_details_only==true)){ // if we want the main configuration details only, ignore definitions
	// include the database functions and table definitions
	require_once(CATS_FUNCTIONS_PATH . 'database.php');

	// include general functions
	require_once(CATS_FUNCTIONS_PATH . 'general.php');
	require_once(CATS_FUNCTIONS_PATH . 'html_output.php');
	require_once(CATS_FUNCTIONS_PATH . 'sessions.php');
	// include security functions
//	require_once(CATS_FUNCTIONS_PATH . 'security.php');
//	require_once(CATS_FUNCTIONS_PATH . 'banner.php');

	require_once(CATS_FUNCTIONS_PATH . 'admin_general.php');
	
	require_once(CATS_CLASSES_PATH . 'object_info.php');
	require_once(CATS_CLASSES_PATH . 'table.php');
	//require_once(CATS_CLASSES_PATH.'box.php');
	require_once(CATS_CLASSES_PATH.'message_stack.php');
	$_SESSION['messageStack'] = new messageStack;
	if(!isset($_SESSION['selected_box']))
	{
		$_SESSION['selected_box'] = 'dashboard';
	}
	if(isset($_REQUEST['selected_box']))
	{
		$_SESSION['selected_box'] = $_REQUEST['selected_box'];
	}
	
	define('DIR_WS_ICONS',DIR_WS_IMAGES.'icons/');
	// define more admin constants
	define('BOX_WIDTH','200');
	define('MAX_DISPLAY_SEARCH_RESULTS',20);
	define('MAX_DISPLAY_PAGE_LINKS',10);

	if($m != "" && $p != "") {
		if(file_exists(CATS_MODULES_PATH."$m/$p.php")){
			require_once(CATS_MODULES_PATH."$m/$p.php");
		}else{
			$_SESSION['messageStack']->add("Either Module or Page does not exist. Make sure the Module and Page are defined and that they exist.","error");
		}
	} else {
		$_SESSION['messageStack']->add("Either Module or Page was not declared. Make sure the Module and Page are defined and that they exist.","error");
	}
	if($_SESSION['messageStack']->size > 0){
		echo($_SESSION['messageStack']->output());
	}

}
?>