<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');
require_once (CATS_CLASSES_PATH . 'navigation.php');
require_once (CATS_ADODB_PATH.'adodb.inc.php');

$a = (isset($_GET['a']))?$_GET['a']:""; // action
$p = (isset($_GET['p']))?$_GET['p']:""; // param
$m = (isset($_GET['m']))?$_GET['m']:""; // mode
$n = (isset($_GET['n']))?$_GET['n']:""; // name
$id = (isset($_GET['id']))?$_GET['id']:""; // id
?>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Helper</title>
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>top.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>dialog.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>css.php" title="Extra Cool" id="main" />
<script language="JavaScript" src="<?php echo WS_PATH;?>js/constants.php"></script>
<script language="JavaScript" src="<?php echo WS_PATH;?>js/global.js"></script>
<style type="text/css">
<!--
body, body.dialog{
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	overflow:hidden;
}
-->
</style>
<script>
var FORM_TARGET = "menu[<?php echo $id;?>]";
onload = function(){
	var h=get_height();
	var p=parent.IFrameMenus[FORM_TARGET];
	p.style.height=h;
	p.setAttribute('loaded', true);
	top.set_menus_loaded();
}
function get_height(){
	return document.body.firstChild.offsetHeight;
}
</script>
</head>
<body class="dialog"><?


function get_menu($id, $t='Administration'){
	require_once(CATS_CLASSES_PATH . "navigation.php");
	$nav = new Navigation("edit","admin");
	return $nav->drawItems($id,$t);
}

switch($a){
	case 'get_menu':
		if($id>0) $s = get_menu($id);
		else $s = "Error: Missing id";
		echo($s);
		break;
	default:
		echo("Error: Missing parameters");
		break;
}
require_once(CATS_INCLUDE_PATH."page_footer.php");
?>
