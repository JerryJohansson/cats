<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'cats_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'html_functions.php');
require_once (CATS_CLASSES_PATH . 'message_stack.php');

require_once (CATS_ADODB_PATH.'adodb.inc.php');

$a = (isset($_GET['a']))?$_GET['a']:""; // action
$p = (isset($_GET['p']))?$_GET['p']:""; // param
$m = (isset($_GET['m']))?$_GET['m']:""; // mode
$n = (isset($_GET['n']))?$_GET['n']:""; // name
$id = (isset($_GET['id']))?$_GET['id']:""; // id
?>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Helper</title>
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>top.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>dialog.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>toolbars.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>css.php" title="Extra Cool" id="main" />
<script language="JavaScript" src="<?php echo WS_PATH;?>js/constants.php"></script>
<script language="JavaScript" src="<?php echo WS_PATH;?>js/global.js"></script>
<style type="text/css">
<!--
body, body.dialog{
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	overflow:hidden;
}
.dlist {
	font: bold 11px Verdana, Arial, Helvetica, sans-serif;
}
.dlist span {
	display:block;
}
-->
</style>
<script>
var FORM_TARGET = "menu[<?php echo $id;?>]";

function get_height(){
	return document.body.firstChild.offsetHeight;
}
var oReturn = new Object();
oReturn.action = false;
window.returnValue = oReturn;
function _ok(){
	oReturn.action = true;
	oReturn.a = get_chosen_list();
	window.returnValue = oReturn;
	window.close();
}
function _cancel(){
	oReturn.action = false;
	window.returnValue = oReturn;
	window.close();
}
function get_chosen_list(){
	var f = document.forms[0];
	var flds = f.fields;
	var i = 0;
	var x = flds.length;
	for(i=0;i<x;i++){
		if(flds[i].value!=""){
			chosen_list.push(flds[i].value);
		}
	}
	//chosen_list.push(val);
	return chosen_list;
}
var chosen_list = new Array();
var orig_list = new Array();
function switch_select(btn,to,from){
	var chosen = to;//element('fields');
	var fields = from;//element('selector');
	if(fields.selectedIndex>0){
		var val = fields.options[fields.selectedIndex].value;
		//alert(val)
		//chosen_list.push(val);
		//alert(chosen_list)
		fields.options[fields.selectedIndex] = null;
		var child = document.createElement("span");
		child.innerHTML = val;
		
		//var obj=chosen.appendChild(child);
		chosen.options[chosen.options.length]=new Option(val,val);
	}
}

function add_field_checkbox(itm,from,to){
	var f=itm.form;
	var chosen = element('fields');
	var fields = f.elements['chks[]'];
	var i = 0;
	var x = fields.length;
	for(i=0;i<x;i++){
		if(fields[i].checked)
			chosen_list.push(fields[i].value);
	}
}
function store(){
	var fields = element('selector');
	var i=0;
	var x=fields.options.length;
	for(i=0;i<x;i++){
		orig_list.push([fields.options[i].value,fields.options[i].text]);
	}
}
window.onload = function(){
	//alert(window.dialogArguments.module);
	//oReturn.action = false;
	//window.returnValue = oReturn;
}
</script>
</head>
<body class="dialog">
<form onSubmit="return false">
<fieldset class="tbar" id="tab_panel[0]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" class="search" style="color:#333" onClick="toggle_search()">Field Chooser</a></caption>
<tr>
	<td style="border-bottom:1px solid #ccc;">
		<table width="100%">
		<tr>
			<td class="info"></td>
			<td>
				<p>Choose the columns you wish to include in the report by selecting them from the list on the left and clicking &quot;Add Field&quot;, alternatively you could double click them to transfer from one list to another.. </p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%"><tr><td width="50%"><div>
		<?php
		//echo html_form_columns_checkbox_list('VIEW_INCIDENT_SEARCHVIEW',"selector","");
		$p='results';
		require_once(CATS_MODULES_PATH.$_GET['m']."/properties.php");
		$TABLES=getPropertiesTables('results');
		// get the Array of Fields in the Table
		$ALL_FIELDS=html_form_columns_options($TABLES['view']);
		// get the Array of Default Fields
		$DEFAULT_FIELDS=getDefaultExportFields();
		// resultant array of Fields
		$OTHER_FIELDS=array();
		// Cycle through all Table Fields
		foreach($ALL_FIELDS as $k1 => $v1) {
			$key1=(is_array($v1))?$v1['id']:$v1;
			$val1=(is_array($v1))?$v1['text']:$v1;
			$KEY_MATCH = FALSE;
			
			// Cycle through Default Fields
			foreach($DEFAULT_FIELDS as $k2 => $v2) {
				$key2=(is_array($v2))?$v2['id']:$v2;
				$val2=(is_array($v2))?$v2['text']:$v2;
				
				// Do the Keys Match
				if ($key1 == $key2) {
					$KEY_MATCH = TRUE;
				}
			}			
			
			// If not a Default Field, add it
			if ($KEY_MATCH == FALSE) {
				$OTHER_FIELDS[]=array("id" => $key1,"text" => $val1);
			}
		}
		// Build the Select TAG
		echo html_form_columns_helper_list($OTHER_FIELDS,"selector",""," size=15 style='width:100%' ondblclick='switch_select(this,this.form.fields,this)' ","Choose Fields:");
		?></div></td><td>
		<div id_name="fields" class="dlist" style="width:100%;height:250px;overflow:auto">
		<?php
		// Build the Select TAG
		echo html_form_columns_helper_list($DEFAULT_FIELDS,"fields",""," size=15 style='width:100%' ondblclick='switch_select(this,this.form.selector,this)' ","Selected Fields List:");
		?>
		</div></td></tr>
		<tr>
		<td>
			<input type="button" name="add" style="width:100%" value="Add Field" onClick="switch_select(this,this.form.fields,this.form.selector)" />
		</td>
		<td>
			<input type="button" name="remove" style="width:100%" value="Remove Field" onClick="switch_select(this,this.form.selector,this.form.fields)" />
		</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<fieldset class="tbar" id="hide" style="text-align:right; ">
<div id="buttons">
<input type="button" class="button" name="ok" value="OK" onClick="_ok();">
<input type="button" class="button" name="cancel" value="Cancel" onClick="_cancel();">
</div>
</fieldset>	
</fieldset>	
</form>
<?php
require_once(CATS_INCLUDE_PATH."page_footer.php");
?>
