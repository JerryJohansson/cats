<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');
require_once (CATS_CLASSES_PATH . 'navigation.php');
require_once (CATS_ADODB_PATH.'adodb.inc.php');

$a = (isset($_GET['a']))?$_GET['a']:""; // action
$p = (isset($_GET['p']))?$_GET['p']:""; // param
$m = (isset($_GET['m']))?$_GET['m']:""; // mode
$n = (isset($_GET['n']))?$_GET['n']:""; // name
$id = (isset($_GET['id']))?$_GET['id']:""; // id
?>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Helper</title>
<script>
var path=top.gui.session.path;
var skins=top.gui.session.skins_path;

document.writeln('<link rel="stylesheet" type="text/css" href="'+top.gui.session.skins_path+'default.css" />');
document.writeln('<link rel="stylesheet" type="text/css" href="'+top.gui.session.skins_path+'top.css" />');
document.writeln('<link rel="stylesheet" type="text/css" href="'+top.gui.session.skins_path+'css.php" title="Extra Cool" id="main" />');
document.writeln('<scr'+'ipt language="JavaScript" src="'+top.gui.session.path+'js/constants.php"></scr'+'ipt>');
document.writeln('<scr'+'ipt language="JavaScript" src="'+top.gui.session.path+'js/global.js"></scr'+'ipt>);');
</script>

<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	overflow:auto;
}
.title {
	background-image: url(<?php echo WS_ICONS_PATH;?>icon_dialog.gif);
	background-position:left;
	background-repeat:no-repeat;
	padding: 5px 1px 5px 30px;
	font: bold 13px Verdana;
	color: #333333;

}
.title_bg {
	background-image: url(<?php echo WS_STYLE_PATH;?>images/toolbar_bg.jpg);
}
-->
</style>
<script>
var FORM_TARGET = "menu[<?php echo $id;?>]";
onload = function(){
	var h=get_height();
	//alert(FORM_TARGET)
	parent.IFrameMenus[FORM_TARGET].style.height=h;
}
function get_height(){
	return document.body.firstChild.offsetHeight;
}
</script>
</head>
<body><?


function get_menu($id, $t='Administration'){
	require_once(CATS_CLASSES_PATH . "navigation.php");
	$nav = new Navigation("edit","admin");
	return $nav->drawItems($id,$t);
}

switch($a){
	case 'get_menu':
		if($id>0) $s = get_menu($id);
		else $s = "Error: Missing id";
		echo($s);
		break;
	default:
		echo("Error: Missing parameters");
		break;
}
require_once(CATS_INCLUDE_PATH."page_footer.php");
?>
