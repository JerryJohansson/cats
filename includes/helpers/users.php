<?php

/*

+--------------------------------------------------------------------------

|   ezwebmaker CMS User Manager - Admin

|   ========================================

|   by Vernon Laskey

|   (c) 2004 Buzmedia

|   http://www.buzmedia.com.au

|   Email: vern@buzmedia.com.au

+---------------------------------------------------------------------------

*/

error_reporting  (E_ERROR | E_WARNING | E_PARSE);

$config_details_only=true;

require_once ("../conf.php");

require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'js_functions.php');

require_once (CATS_CLASSES_PATH.'users.php');

require_once (CATS_ADODB_PATH.'adodb.inc.php');



$a = (isset($_GET['a']))?$_GET['a']:""; // action

$p = (isset($_GET['p']))?$_GET['p']:""; // param

$m = (isset($_GET['m']))?$_GET['m']:""; // mode

$n = (isset($_GET['n']))?$_GET['n']:""; // name

$id = (isset($_GET['id']))?$_GET['id']:""; // id

if(isset($_REQUEST['site_id'])) $site_id = $_REQUEST['site_id'];



$results = false;

$html = "";

$s = '<div class="dialog">';

$user = new User;

switch($a){

	case 'get_employee_list':

		$s .= '

		<div style="background-color:#eee;width:100%;"><form action="users.php" method="get" target="get_employee_list_search_target"><img src="'.WS_ICONS_PATH.'btn_close_bg.gif" align="right" onclick="top.hideHelpers();" />

			<input type="hidden" name="a" value="get_employee_list_search" />';

		if(isset($site_id)) {

			$s .= '	<input type="hidden" name="site_id" value="'.$site_id.'" />';

		}

		$s.='	<input type="hidden" name="m" value="users" />

			<input type="hidden" name="n" value="get_employee_list" />

			<label style="font:bold 11px verdana;" for="id_p">Surname:</label><input type="text" id="id_p" name="p" value="'.$p.'"><input type=submit value=search><input type=button value=cancel onclick="top.hideHelpers();">

		</div>

		<iframe name="get_employee_list_search_target" id="get_employee_list_search_target" 

			src="about:blank" 

			style="width:100%;height:200px;">

		</iframe>

		';

		$html = $s;

		break;

	case 'get_employee_list_action_confirm':

		$s .= '

		<div style="background-color:#eee;width:100%;"><form action="users.php" method="get" target="get_employee_list_search_target"><img src="'.WS_ICONS_PATH.'btn_close_bg.gif" align="right" onclick="top.hideHelpers();" />

			<input type="hidden" name="a" value="get_employee_list_search_action_confirm" />';

		if(isset($site_id)) {

			$s .= '	<input type="hidden" name="site_id" value="'.$site_id.'" />';

		}

		$s.='	<input type="hidden" name="m" value="users" />

			<input type="hidden" name="n" value="get_employee_list_action_confirm" />

			<label style="font:bold 11px verdana;" for="id_p">Surname:</label><input type="text" id="id_p" name="p" value="'.$p.'"><input type=submit value=search><input type=button value=cancel onclick="top.hideHelpers();">

		</div>

		<iframe name="get_employee_list_search_target" id="get_employee_list_search_target" 

			src="about:blank" 

			style="width:100%;height:200px;">

		</iframe>

		';

		$html = $s;

		break;
		
	case 'get_site_employee_list': // search users by site

		$s .= '

		<div style="background-color:#eee;width:100%;"><form action="users.php" method="get" target="get_employee_list_search_target"><img src="'.WS_ICONS_PATH.'btn_close_bg.gif" align="right" onclick="top.hideHelpers();" />

			<input type="hidden" name="a" value="get_employee_list_search" />';

		if(!empty($id)) {

			$s .= '	<input type="hidden" name="site_id" value="'.$id.'" />';

		}

		$s.='	<input type="hidden" name="m" value="users" />

			<input type="hidden" name="n" value="get_site_employee_list" />

			<label style="font:bold 11px verdana;" for="id_p">Surname:</label><input type="text" id="id_p" name="p" value="'.$p.'"><input type=submit value=search><input type=button value=cancel onclick="top.hideHelpers();">

		</div>

		<iframe name="get_employee_list_search_target" id="get_employee_list_search_target" 

			src="about:blank" 

			style="width:100%;height:200px;">

		</iframe>

		';

		$html = $s;

		break;

	case 'get_employee_list_search':

		$results = true;

		$filter = (isset($p))?$p:"";

		$i_hilite=0;

		if(!empty($filter)){

			if(isset($site_id)) $user->site_id = $site_id;

			if($arr = $user->getEmployeesArray($id,$filter)){

				$s='<div><table class="admin" cellpadding="5" style="background-color:#fff;" width="100%">';

				$s.="<tr>";

				$s.="<th>Emp. No.</th><th>Name</th><th>Site</th><th>Dept.</th><th>Sect.</th>";

				$s.="</tr>";

				$row_attribs = ' onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';

				foreach($arr as $key => $opt){

					$value=db_input($opt['ID']);

					$text_value=db_input($opt['TEXT']);

					$site_id=db_input($opt['SITE_ID']);

					$site=db_input($opt['SITE']);

					$dept_id=db_input($opt['DEPARTMENT_ID']);

					$dept=db_input($opt['DEPARTMENT']);

					$sect_id=db_input($opt['SECTION_ID']);

					$sect=db_input($opt['SECTION']);

					$pos_id=db_input($opt['POSITION_ID']);

					$pos=db_input($opt['POSITION']);

					

					$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";

					$i_hilite++;

					

					$s .= "<tr $row_hilite $row_attribs onclick=\"_row_down(this);top.HelperObjects.set('$n',{emp_id:'$value',emp_val:'$text_value',site_id:'$site_id',site_val:'$site',dep_id:'$dept_id',dep_val:'$dept',sec_id:'$sect_id',sec_val:'$sect',pos_id:'$pos_id',pos_val:'$pos'});\" ><td>$value</td><td>$text_value</td><td>$site</td><td>$dept</td><td>$sect</td></tr>";

				}

				$s .= "</table>";

			}

		}else{

			$s .= '<p>No Results Found</p>';

		}

		$html = $s;

		break;

	case 'get_employee_list_search_action_confirm':

		$results = true;

		$filter = (isset($p))?$p:"";

		$i_hilite=0;

		if(!empty($filter)){

			if(isset($site_id)) $user->site_id = $site_id;

			if($arr = $user->getEmployeesArray($id,$filter)){

				$s='<div><table class="admin" cellpadding="5" style="background-color:#fff;" width="100%">';

				$s.="<tr>";

				$s.="<th>Emp. No.</th><th>Name</th><th>Site</th><th>Dept.</th><th>Sect.</th>";

				$s.="</tr>";

				$row_attribs = ' onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';

				foreach($arr as $key => $opt){

					$value=db_input($opt['ID']);

					$text_value=db_input($opt['TEXT']);

					$site_id=db_input($opt['SITE_ID']);

					$site=db_input($opt['SITE']);

					$dept_id=db_input($opt['DEPARTMENT_ID']);

					$dept=db_input($opt['DEPARTMENT']);

					$sect_id=db_input($opt['SECTION_ID']);

					$sect=db_input($opt['SECTION']);

					$pos_id=db_input($opt['POSITION_ID']);

					$pos=db_input($opt['POSITION']);

					

					$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";

					$i_hilite++;

					

					$s .= "<tr $row_hilite $row_attribs onclick=\"alert('Please discuss this action with' + '$text_value'.split(',')[1] + ' ' + '$text_value'.split(',')[0] + ' and agree on the expected completion date before entering into CATS.'); _row_down(this);top.HelperObjects.set('$n',{emp_id:'$value',emp_val:'$text_value',site_id:'$site_id',site_val:'$site',dep_id:'$dept_id',dep_val:'$dept',sec_id:'$sect_id',sec_val:'$sect',pos_id:'$pos_id',pos_val:'$pos'});\" ><td>$value</td><td>$text_value</td><td>$site</td><td>$dept</td><td>$sect</td></tr>";

				}

				$s .= "</table>";

			}

		}else{

			$s .= '<p>No Results Found</p>';

		}

		$html = $s;

		break;
		
		
	default:

		$html = "Error: Missing parameters";

		break;

}

?>

<html>

<head> 

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>Helper</title>

<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>default.css" />

<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>toolbars.css" />

<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>dialog.css" />

<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>css.php" title="Extra Cool" id="main" />

<script language="JavaScript" src="<?php echo WS_PATH;?>js/constants.php"></script>

<script language="JavaScript" src="<?php echo WS_PATH;?>js/global.js"></script>

<style type="text/css">

<!--

body {

	margin: 0px;

	border: none;

	overflow: <?php echo(($results)?'auto':'hidden');?>;

}

-->

</style>

<?php

if($results){

?>

<script>

onload = function(){

	resize();

}

function resize(){

	var maxh = 190;

	var minh = 80;
	var maxw = 600;
	var minw = 80;

	var h=document.body.firstChild.offsetHeight;
	var w=document.body.offsetWidth;

	h=(h>maxh)?maxh:h;

	h=(h<minh)?minh:h;
	w=(w>maxh)?maxw:w;
	w=(w<minh)?minw:w;

	window.resizeTo(w,h);

	parent.resize();

}

</script>

<?php

}else{

?>

<script>

onload = function(){

	var frm=document.forms[0];

	<?php if($p!='') echo('frm.submit();'); ?>

	resize();

	var el=frm.elements['p'];

	if(el.value!=''){

		el.select();

	}else{

		el.focus();

	}

}

function resize(){

	window.resizeTo(document.body.offsetWidth,document.body.firstChild.offsetHeight);

}

</script>

<?php

} // end:if results==true

?>

</head>

<body>

<?php

echo($html);

?>

</div>

<?php

require_once(CATS_INCLUDE_PATH."page_footer.php");

?>

