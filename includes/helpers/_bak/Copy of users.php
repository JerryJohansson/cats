<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'js_functions.php');
require_once (CATS_CLASSES_PATH.'users.php');
require_once (CATS_ADODB_PATH.'adodb.inc.php');

$a = (isset($_GET['a']))?$_GET['a']:""; // action
$p = (isset($_GET['p']))?$_GET['p']:""; // param
$m = (isset($_GET['m']))?$_GET['m']:""; // mode
$n = (isset($_GET['n']))?$_GET['n']:""; // name
$id = (isset($_GET['id']))?$_GET['id']:""; // id
?>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Helper</title>
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>toolbars.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>css.php" title="Extra Cool" id="main" />
<script language="JavaScript" src="<?php echo WS_PATH;?>js/constants.php"></script>
<script language="JavaScript" src="<?php echo WS_PATH;?>js/global.js"></script>
<?php 
$results = false;
$overflow = 'hidden';
if(preg_match("/search$/",$a)) {
	$results = true;
	$overflow = 'auto';
}
?>
<style type="text/css">
<!--
body {
	margin: 0px;
	border: none;
	overflow: <?php echo $overflow;?>;
}
-->
</style>
<?php
if($results){
?>
<script>
onload = function(){
	resize();
}
function resize(){
	var h=document.body.firstChild.offsetHeight;
	if(h<190){
		window.resizeTo(document.body.offsetWidth,h);
		parent.resize();
	}
}
</script>
<?php
}else{
?>
<script>
var ONLOAD_SUBMIT=false;
var ONLOAD_RESIZE=false;
var frm=null;
onload = function(){
	frm=document.forms[0];
	if(ONLOAD_SUBMIT) frm.submit();
	if(ONLOAD_RESIZE) resize();
	if(frm) {
		var el=frm.elements['p'];
		if(el.value!=''){
			el.select();
		}else{
			el.focus();
		}
	}
}
function resize(){
	window.resizeTo(document.body.offsetWidth,document.body.firstChild.offsetHeight);
}
</script>
<?php
} // end:if results==true
?>
</head>
<body><div>
<?php
$s = "";
$user = new User;
switch($a){
	case 'get_employee_list':
		$s = ($p!='')?'<script>ONLOAD_SUBMIT=true;':'<script>';
		$s .= 'ONLOAD_RESIZE=true;</script>';
		$s .= '
		<div style="background-color:#eee;width:100%;"><form action="users.php" method="get" target="get_employee_list_search_target"><img src="'.WS_ICONS_PATH.'btn_close_bg.gif" align="right" onclick="top.hideHelpers();" />
			<input type="hidden" name="a" value="get_employee_list_search" />
			<input type="hidden" name="m" value="users" />
			<input type="hidden" name="n" value="get_employee_list" />
			<label style="font:bold 11px verdana;" for="id_p">Surname:</label><input type="text" id="id_p" name="p" value="'.$p.'"><input type=submit value=search><input type=button value=cancel onclick="top.hideHelpers();">
		</div>
		<iframe name="get_employee_list_search_target" id="get_employee_list_search_target" 
			src="about:blank" 
			style="width:100%;height:200px;">
		</iframe>
		';
		echo($s);
		break;
	case 'get_employee_list_search':
		$filter = (isset($p))?$p:"";
		$i_hilite=0;
		if(!empty($filter)){
			if($arr = $user->getEmployeesArray($id,$filter)){
				$s='<table class="admin" cellpadding="5" style="background-color:#fff;" width="100%">';
				$s.="<tr>";
				$s.="<th>Emp. No.</th><th>Name</th><th>Site</th><th>Dept.</th><th>Sect.</th>";
				$s.="</tr>";
				$row_attribs = ' onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';
				foreach($arr as $key => $opt){
					$value=db_input($opt['ID']);
					$text_value=db_input($opt['TEXT']);
					$site_id=db_input($opt['SITE_ID']);
					$site=db_input($opt['SITE']);
					$dept_id=db_input($opt['DEPARTMENT_ID']);
					$dept=db_input($opt['DEPARTMENT']);
					$sect_id=db_input($opt['SECTION_ID']);
					$sect=db_input($opt['SECTION']);
					
					$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
					$i_hilite++;
					
					$s .= "<tr $row_hilite $row_attribs onclick=\"_row_down(this);top.HelperObjects.set('$n',{emp_id:'$value',emp_val:'$text_value',site_id:'$site_id',site_val:'$site',dep_id:'$dept_id',dep_val:'$dept',sec_id:'$sect_id',sec_val:'$sect'});\" ><td>$value</td><td>$text_value</td><td>$site</td><td>$dept</td><td>$sect</td></tr>";
				}
				$s .= "</table>";
			}
		}else{
			$s .= '<p>No Results Found</p>';
		}
		echo($s);
		break;
	case 'get_mimms_employee_list': // MIMMS Employee Helper Search Criteria
		$s = ($p!='')?'<script>ONLOAD_SUBMIT=true;':'<script>';
		$s .= 'ONLOAD_RESIZE=true;</script>';
		$s .= '
		<div style="background-color:#eee;width:100%;"><form action="users.php" method="get" target="get_mimms_employee_list_search_target"><img src="'.WS_ICONS_PATH.'btn_close_bg.gif" align="right" onclick="top.hideHelpers();" />
			<input type="hidden" name="a" value="get_mimms_employee_list_search" />
			<input type="hidden" name="m" value="users" />
			<input type="hidden" name="n" value="get_mimms_employee_list" />
			<label style="font:bold 11px verdana;" for="id_p">Surname:</label>';
			$s .= '<input type="text" id="id_p" name="p" value="'.$p.'" />';
			$s .= '<label style="font:bold 11px verdana;" for="id_emp_id">Emp. No.:</label>';
			$s .= '<input type="text" id="id_emp_id" name="emp_id" />';
			$s .= '<label style="font:bold 11px verdana;" for="id_status">Status:</label>';
			$s .= '<select class="sfield" style="width:auto; min-width:50px;" name="status" id="id_status"><option></option><option value="Active" selected="true">Active</option><option value="Inactive">Inactive</option></select>';
			$s .= '<input type=submit value=search><input type=button value=cancel onclick="top.hideHelpers();">
		</div>
		<iframe name="get_mimms_employee_list_search_target" id="get_mimms_employee_list_search_target" 
			src="about:blank" 
			style="width:100%;height:200px;">
		</iframe>
		';
		echo($s);
		break;
	case 'get_mimms_employee_list_search': // MIMMS Employees Helper search results table
		$filter = (isset($p))?$p:"";
		$status = (isset($_GET['status']))?$_GET['status']:'';
		$emp_id = (isset($_GET['emp_id']))?$_GET['emp_id']:0;
		$i_hilite=0;
		//if(!empty($filter)){
			if($arr = $user->getMimmsEmployeesArray($emp_id,$filter,$status)){
				$s='<table class="admin" cellpadding="5" style="background-color:#fff;" width="100%">';
				$s.="<tr>";
				$s.="<th>Emp. No.</th><th>Name</th><th>Site</th><th>Position</th><th>Employee Type</th>";
				$s.="</tr>";
				$row_attribs = ' onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';
				foreach($arr as $key => $opt){
					$value=db_input($opt['EMPID']);
					$type=db_input($opt['EMPLOYEE_TYPE']);
					$lname=db_input($opt['SURNAME']);
					$fname=db_input($opt['GIVEN_NAME']);
					$site_id=db_input($opt['SITE_ID']);
					$site=db_input($opt['SITE']);
					$pos_id=db_input($opt['POSITION_ID']);
					$pos=db_input($opt['POSITION']);
					$email=db_input($opt['EMAIL']);
					$uid=db_input($opt['USERNAME']);
					$pwd=db_input($opt['PASS_WORD']);
					$dept_id=db_input($opt['DEPARTMENT_ID']);
					$dept=db_input($opt['DEPARTMENT']);
					
					$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
					$i_hilite++;
					
					$s .= "<tr $row_hilite $row_attribs onclick=\"_row_down(this);top.HelperObjects.set('$n',";
					$s .= "{id:'$value',val:'$lname, $fname',emp_id:'$value',emp_lname:'$lname',emp_fname:'$fname',emp_type:'$type',";
					$s .= "site_id:'$site_id',site_val:'$site',";
					$s .= "dept_id:'$dept_id',dept_val:'$dept',";
					$s .= "pos_id:'$pos_id',pos_val:'$pos',";
					$s .= "email:'$email',uid:'$uid',pwd:'$pwd'});\" >";
					$s .= "<td>$value</td><td>$lname, $fname</td><td>$site</td><td>$pos</td><td>$type</td></tr>";
				}
				$s .= "</table>";
			//}
		}else{
			$s .= '<p>No Results Found</p>';
		}
		echo($s);
		break;
	default:
		echo("Error: Missing parameters");
		break;
}
echo '</div>';
require_once(CATS_INCLUDE_PATH."page_footer.php");
?>
