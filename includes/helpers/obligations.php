<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'js_functions.php');
require_once (CATS_CLASSES_PATH.'users.php');
require_once (CATS_ADODB_PATH.'adodb.inc.php');

$a = (isset($_GET['a']))?$_GET['a']:""; // action
$p = (isset($_GET['p']))?$_GET['p']:""; // param
$m = (isset($_GET['m']))?$_GET['m']:""; // mode
$n = (isset($_GET['n']))?$_GET['n']:""; // name
$id = (isset($_GET['id']))?$_GET['id']:""; // id
?>
<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Helper</title>
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>toolbars.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>dialog.css" />
<link rel="stylesheet" type="text/css" href="<?php echo WS_STYLE_PATH;?>css.php" title="Extra Cool" id="main" />
<script language="JavaScript" src="<?php echo WS_PATH;?>js/constants.php"></script>
<script language="JavaScript" src="<?php echo WS_PATH;?>js/global.js"></script>
<?php 
$results = false;
$overflow = 'auto';
if(preg_match("/search$/",$a)) {
	$results = true;
	$overflow = 'auto';
}
?>
<style type="text/css">
<!--
body {
	margin: 0px;
	border: none;
	overflow: <?php echo $overflow;?>;
}
-->
</style>
<?php
if($results){
?>
<script>
onload = function(){
	resize();
}
function resize(){
	var h=document.body.firstChild.offsetHeight;
	if(h<150){
		window.resizeTo(document.body.offsetWidth,h);
		parent.resize();
	}
}
</script>
<?php
}else{
?>
<script>
var ONLOAD_SUBMIT=false;
var ONLOAD_RESIZE=false;
var frm=null;
onload = function(){
//alert(ONLOAD_SUBMIT)
	if(ONLOAD_SUBMIT) frm.submit();
	if(ONLOAD_RESIZE) resize();
	if(frm) {
		var el=frm.elements['p'];
		if(el.value!=''){
			el.select();
		}else{
			el.focus();
		}
	}
	fix_table("result");
}
function resize(){
	window.resizeTo(document.body.offsetWidth,document.body.firstChild.offsetHeight);
}

</script>
<?php
} // end:if results==true
?>
</head>
<body>
<?php
$s = "";
$user = new User;
switch($a){
	case 'get_obligation_responsibilities_list':
		$s = '<script>ONLOAD_SUBMIT=true;';
		$s .= 'ONLOAD_RESIZE=true;frm=document.forms[0];</script>';
		$s .= '
		<div style="background-color:#eee;width:100%;"><form action="obligations.php" method="get" target="get_obligation_responsibilities_list_search_target"><img src="'.WS_ICONS_PATH.'btn_close_bg.gif" align="right" onclick="top.hideHelpers();" />
			<input type="hidden" name="a" value="get_obligation_responsibilities_list_search" />
			<input type="hidden" name="m" value="obligations" />
			<input type="hidden" name="n" value="get_obligation_responsibilities_list" />
			<label style="font:bold 11px verdana;" for="id_p"> Position:</label><input type="text" id="id_p" name="p" value="'.$p.'"><input type=submit value=search><input type=button value=cancel onclick="top.hideHelpers();">
		</div>
		
		<iframe name="get_obligation_responsibilities_list_search_target" id="get_obligation_responsibilities_list_search_target" 
			src="about:blank" 
			style="width:100%;height:175px;">
		</iframe>
		';
		echo($s);
		break;
	case 'get_obligation_responsibilities_list_search':
		$filter = (isset($p))?$p:"";
		$i_hilite=0;
		//if(!empty($filter)){
			$sql = "SELECT DISTINCT Position_Id AS ID, Position_Description AS TEXT FROM tblPosition WHERE lower(Position_Description) LIKE lower('%".$filter."%') ORDER BY Position_Description ASC";
			$rs = db_query($sql);
			//print_r();
			if($arr = $rs->GetAll()){
				$s='<div class="dialog"><table id="result_table_hdr" class="admin" cellpadding="5" style="background-color:#fff;" width="100%">';
				$s.='<tr>';
				$s.='<th>Obligation ID</th><th>Description</th>';
				$s.='</tr></table><div id="result_table_div" style="height: 140px; overflow: auto;">';
				$s .= '<table id="result_table" class="admin" cellpadding="5" style="background-color:#fff;" width="100%">';
				$row_attribs = ' onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';
				$n = 'get_obligation_responsibilities_list';
				foreach($arr as $key => $opt){
					$value=db_input($opt['ID']);
					$text_value=db_input($opt['TEXT']);
					
					$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
					$i_hilite++;
					
					$s .= "<tr $row_hilite $row_attribs onclick=\"_row_down(this);top.HelperObjects.set('$n',{id:'$value',val:'$text_value'});\" ><td>$value</td><td>$text_value</td></tr>";
				}
				$s .= "</table>";
			}else{
				$s .= '<div class="dialog"><div id="result_table_div" style="height: 140px; overflow: auto;"><p>No Results Found</p></div></div>';
			}
		echo($s);
		break;
	
	default:
		echo("Error: Missing parameters");
		break;
}
echo '</div>';
require_once(CATS_INCLUDE_PATH."page_footer.php");
?>
