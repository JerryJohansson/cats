<?php
/*
	Envokes a download of a csv file compiled by query within main results module page
*/
	//echo($search_sql);
	if(isset($_POST['cats::report']) || isset($_REQUEST['cats_report'])){
		$s_ob_get_contents = ob_get_contents();
		ob_end_clean();
		
		// Now send the file with header() magic
		header("Expires: Mon, 26 Nov 1962 00:00:00 GMT");
		header("Last-Modified: " . gmdate("D,d M Y H:i:s") . " GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Content-Type: Application/octet-stream");
		header("Content-disposition: attachment; filename={$m}.csv");
		
		require_once(CATS_ADODB_PATH . 'toexport.inc.php');
		//echo($search_sql);
		$rs=$db->Execute($search_sql);
		echo(rs2csv($rs));
		exit;
	}
?>
