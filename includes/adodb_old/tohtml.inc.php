<?php 
/*
  V4.65 22 July 2005  (c) 2000-2005 John Lim (jlim@natsoft.com.my). All rights reserved.
  Released under both BSD license and Lesser GPL library license. 
  Whenever there is any discrepancy between the two licenses, 
  the BSD license will take precedence.
  
  Some pretty-printing by Chris Oxenreider <oxenreid@state.net>
*/ 
  
// specific code for tohtml
GLOBAL $gSQLMaxRows,$gSQLBlockRows,$ADODB_ROUND;

$ADODB_ROUND=4; // rounding
$gSQLMaxRows = 1000; // max no of rows to download
$gSQLBlockRows=20; // max no of rows per table block

// RecordSet to HTML Table
//------------------------------------------------------------
// Convert a recordset to a html table. Multiple tables are generated
// if the number of rows is > $gSQLBlockRows. This is because
// web browsers normally require the whole table to be downloaded
// before it can be rendered, so we break the output into several
// smaller faster rendering tables.
//
// $rs: the recordset
// $ztabhtml: the table tag attributes (optional)
// $zheaderarray: contains the replacement strings for the headers (optional)
//
//  USAGE:
//	include('adodb.inc.php');
//	$db = ADONewConnection('mysql');
//	$db->Connect('mysql','userid','password','database');
//	$rs = $db->Execute('select col1,col2,col3 from table');
//	rs2html($rs, 'BORDER=2', array('Title1', 'Title2', 'Title3'));
//	$rs->Close();
//
// RETURNS: number of rows displayed


function rs2html(&$rs,$ztabhtml=false,$zheaderarray=false,$htmlspecialchars=true,$echo = true, $sort_id='', $sess_sort_id = '', $zcolumnarray=false, $row_attribs=' onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);" ')
{
$s ='';$rows=0;$docnt = false;$delete_button=false;$edit_button=false;$delete_url='';$delete_id='';$edit_url='';$edit_id='';
GLOBAL $gSQLMaxRows,$gSQLBlockRows,$ADODB_ROUND;

	if (!$rs) {
		printf(ADODB_BAD_RS,'rs2html');
		return false;
	}
	
	if (! $ztabhtml) $ztabhtml = "BORDER='1' WIDTH='98%'";
	//else $docnt = true;
	$typearr = array();
	$ncols = $rs->FieldCount();
	$hdr = "<TABLE COLS=$ncols $ztabhtml><tr>\n\n";
	for ($i=0; $i < $ncols; $i++) {	
		
		$field = $rs->FetchField($i);
		$col_attribs = (is_array($zcolumnarray))?$zcolumnarray[$i]:"";
		if ($field) {
			if ($zheaderarray) $fname = $zheaderarray[$i];
			else $fname = htmlspecialchars($field->name);	
			$typearr[$i] = $rs->MetaType($field->type,$field->max_length);
			//print " $field->max_length $field->name $field->type $typearr[$i] ";
		} else {
			$fname = 'Field '.($i+1);
			$typearr[$i] = 'C';
		}
		if (strlen($fname)==0) $fname = '&nbsp;';
		
		$thattribs="";
		$type = $typearr[$i];
		switch($type) {
			case 'D':
				if($col_attribs=="") $thattribs=' align="left" ';
				else $thattribs=$col_attribs;
				break;
			case 'T':
				if($col_attribs=="") $thattribs=' align="left" ';
				else $thattribs=$col_attribs;
				break;
			case 'N':
				if($col_attribs=="") $thattribs=' align="right" ';
				else $thattribs=$col_attribs;
			case 'I':
				//$thattribs=' align="left" ';
				break;
			default:
				if ($zheaderarray) { // add hyperlink support for editing and searching child items etc.
					if($col_attribs!="") $thattribs=$col_attribs;
					else $thattribs='';//' width="3%"';
					if (strpos(strtolower(":".$fname),"edit:")==1){
						$edit_button=explode(":",$zheaderarray[$i]);
						$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
						$fname = '';//$edit_button[0];
						$edit_url = $edit_button[2];
						$edit_id = $edit_button[3];
						if(count($edit_button)>3)
							$edit_close = $edit_button[4];
						else
							$edit_close = '';
					}
					//print_r($edit_button);
					if (strpos(strtolower(":".$fname),"delete:")==1){
						$delete_button=explode(":",$zheaderarray[$i]);
						$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
						$fname = '';//$delete_button[0];
						$delete_url = $delete_button[2];
						$delete_id = $delete_button[3];
						if(count($delete_button)>3)
							$delete_close = $delete_button[4];
						else
							$delete_close = '';
					}
					//print_r($delete_button);
				}
		}
		
		if($sort_id!=''){
			$fsort_query="";
			$fsort_name=htmlspecialchars($field->name);
			$fsort_by_name=$sort_id.'_sort_by';
			$fsort_dir_name=$sort_id.'_dir';
			$sess_sort_dir = $sess_sort_id.$fsort_dir_name;
			
			if($sort_found=isset($_SESSION[$sess_sort_dir])){
				$fsort_dir = $_SESSION[$sess_sort_dir];//$_GET[$fsort_dir_name];
			}else{
				$fsort_dir = "ASC";
			}
			
			switch($fsort_dir){
				case 'ASC':case 'asc':
					$fsort_dir='DESC';
					break;
				case 'DESC':case 'desc':default:
					$fsort_dir='ASC';
					break;
			}
			
			$fsort_query .= "$fsort_dir_name=$fsort_dir&";
			$fsort_query .= "$fsort_by_name=$fsort_name";
			//$fname='<a href="' . $_SERVER['SCRIPT_NAME'] . '?' . $fsort_query . '">'.$fname.'</a>';
			if(!empty($fname))
				$fname='<a href="javascript:_m.pager_sort(\'' . $fsort_query . '\');">'.$fname.'</a>';
		}
		$hdr .= "<th$thattribs>$fname</th>";
	}
	$hdr .= "\n</tr>";
	if ($echo) print $hdr."\n\n";
	else $html = $hdr;
	
	// smart algorithm - handles ADODB_FETCH_MODE's correctly by probing...
	$numoffset = isset($rs->fields[0]) ||isset($rs->fields[1]) || isset($rs->fields[2]);
	$i_hilite = 0;
	while (!$rs->EOF) {
		
		$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
		$i_hilite++;
		
		$s .= "<TR valign=top $row_hilite $row_attribs>\n";
		
		for ($i=0; $i < $ncols; $i++) {
			if ($i===0) $v=($numoffset) ? $rs->fields[0] : reset($rs->fields);
			else $v = ($numoffset) ? $rs->fields[$i] : next($rs->fields);
			$field = $rs->FetchField($i);
			$type = $typearr[$i];
			//if($zheaderarray==false||(is_array($zheaderarray) && $field->name==$zheaderarray[$i]['name'])){
				switch($type) {
				case 'D':
					if (empty($v)) $s .= "<TD> &nbsp; </TD>\n";
					else if (!strpos($v,':')) {
						//$s .= "	<TD>".$rs->UserDate($v,"D d, M Y") ."&nbsp;</TD>\n";
						$s .= "	<TD>".$rs->UserDate($v,CATS_SQL_DATE_FORMAT_SHORT) ."&nbsp;</TD>\n";
					}
					break;
				case 'T':
					if (empty($v)) $s .= "<TD> &nbsp; </TD>\n";
					else $s .= "	<TD>".$rs->UserTimeStamp($v,CATS_SQL_DATE_FORMAT_LONG) ."&nbsp;</TD>\n";
					//else $s .= "	<TD>".$rs->UserTimeStamp($v,"D d, M Y, h:i:s") ."&nbsp;</TD>\n";
				break;
				
				case 'N':
					if (abs($v) - round($v,0) < 0.00000001)
						$v = round($v);
					else
						$v = round($v,$ADODB_ROUND);
				case 'I':
					$s .= "	<TD>".stripslashes((trim($v))) ."&nbsp;</TD>\n";
						
				break;
				/*
				case 'B':
					if (substr($v,8,2)=="BM" ) $v = substr($v,8);
					$mtime = substr(str_replace(' ','_',microtime()),2);
					$tmpname = "tmp/".uniqid($mtime).getmypid();
					$fd = @fopen($tmpname,'a');
					@ftruncate($fd,0);
					@fwrite($fd,$v);
					@fclose($fd);
					if (!function_exists ("mime_content_type")) {
						function mime_content_type ($file) {
							return exec("file -bi ".escapeshellarg($file));
						}
					}
					$t = mime_content_type($tmpname);
					$s .= (substr($t,0,5)=="image") ? " <td><img src='$tmpname' alt='$t'></td>\\n" : " <td><a
					href='$tmpname'>$t</a></td>\\n";
					break;
				*/
	
				default:
					if((int)strpos(":".$v,"edit:")==1){
						$s .= "	<TD width=5><a href='" . $edit_protocol . $edit_url . $rs->Fields($edit_id) . $edit_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[0]) . "_bg.gif' border='0' /></a></TD>\n";
					}else if((int)strpos(":".$v,"delete:")==1){
						if($delete_protocol!=''){
							$s .= "	<TD width=5><a href='" . $delete_protocol . $delete_url . $rs->Fields($delete_id) . $delete_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($delete_button[0]) . "_bg.gif' border='0' /></a></TD>\n";
						}else{
							$s .= "	<TD width=5><input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" /></TD>\n";
						}
					}else{
						if ($htmlspecialchars) $v = htmlspecialchars(trim($v));
						$v = trim($v);
						if (strlen($v) == 0) $v = '&nbsp;';
						$s .= "	<TD>". str_replace("\n",'<br>',stripslashes($v)) ."</TD>\n";
					}
				}
			//} // fi $zheaderarray==false||(is_array($zheaderarray) && $field->name==$zheaderarray[$i])
		} // for
		$s .= "</TR>\n\n";
			  
		$rows += 1;
		if ($rows >= $gSQLMaxRows) {
			$rows = "<p>Truncated at $gSQLMaxRows</p>";
			break;
		} // switch

		$rs->MoveNext();
	
	// additional EOF check to prevent a widow header
		//if()
		//echo($rows .":" . $gSQLBlockRows .":" . ($rows % $gSQLBlockRows) . "<br>");
		if (!$rs->EOF && $rows % $gSQLBlockRows == 0) {
		//if (connection_aborted()) break;// not needed as PHP aborts script, unlike ASP
			if ($echo) print $s . "</TABLE>\n\n";
			else $html .= $s ."</TABLE>\n\n";
			$s = $hdr;
		}
	} // while

	if ($echo) print $s."</TABLE>\n\n";
	else $html .= $s."</TABLE>\n\n";
	
	if ($docnt) if ($echo) print "<H2>".$rows." Rows</H2>";
	
	return ($echo) ? $rows : $html;
}
 
// pass in 2 dimensional array
function arr2html(&$arr,$ztabhtml='',$zheaderarray='')
{
	if (!$ztabhtml) $ztabhtml = 'BORDER=1';
	
	$s = "<TABLE $ztabhtml>";//';print_r($arr);

	if ($zheaderarray) {
		$s .= '<TR>';
		for ($i=0; $i<sizeof($zheaderarray); $i++) {
			$s .= "	<TH>{$zheaderarray[$i]}</TH>\n";
		}
		$s .= "\n</TR>";
	}
	
	for ($i=0; $i<sizeof($arr); $i++) {
		$s .= '<TR>';
		$a = &$arr[$i];
		if (is_array($a)) 
			for ($j=0; $j<sizeof($a); $j++) {
				$val = $a[$j];
				if (empty($val)) $val = '&nbsp;';
				$s .= "	<TD>$val</TD>\n";
			}
		else if ($a) {
			$s .=  '	<TD>'.$a."</TD>\n";
		} else $s .= "	<TD>&nbsp;</TD>\n";
		$s .= "\n</TR>\n";
	}
	$s .= '</TABLE>';
	print $s;
}


// pass in 2 dimensional array
function arr2html_form(&$arr,$ztabhtml='',$zheaderarray='')
{
	if (!$ztabhtml) $ztabhtml = 'BORDER=1';
	
	$s = "<table $ztabhtml>";//';print_r($arr);

	if ($zheaderarray) {
		$s .= '<tr>';
		for ($i=0; $i<sizeof($zheaderarray); $i++) {
			$s .= "	<th>{$zheaderarray[$i]}</th>\n";
		}
		$s .= "\n</tr>";
	}
	
	for ($i=0; $i<sizeof($arr); $i++) {
		$s .= '<tr';
		$a = &$arr[$i];
		if (is_array($a)){ 
			$len=sizeof($a);
			$s .= ' id="row_'.$a[0].'">';
			if($len>2){
				for ($j=0; $j<$len; $j++) {
					$val = $a[$j];
					if (empty($val)) $val = '&nbsp;';
					$s .= "	<td>$val</td>\n";
				}
			}else{
				$lbl=ucwords(preg_replace("/_/"," ",strtolower($a[0])));
				$s .= " <td class=\"label\"><label for=\"{$a[0]}\">$lbl</label></td>\n";
				$s .= "	<td>{$a[1]}</td>\n";
			}
		} else if ($a) {
			$s .= ' id="row_'.$a.'">';
			$s .=  '	<td>'.$a."</td>\n";
		} else $s .= "	<td>&nbsp;</td>\n";
		$s .= "\n</tr>\n";
	}
	$s .= '</table>';
	print $s;
}

?>