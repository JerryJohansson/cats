
<script src="http://catsdev/js/jquery.min.js"></script>
 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://catsdev/bootstrap-3.3.2-dist/css/bootstrap.min.css">

 
<!-- Optional theme -->
<link rel="stylesheet" href="http://catsdev/bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">
 
 
<!-- Latest compiled and minified JavaScript -->
<script src="http://catsdev/bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://catsdev/js/html5shiv.min.js"></script>
      <script src="http://catsdev/js/respond.min.js"></script>
    <![endif]-->


<style>

#top_personal{

	display:none;

	background-color: transparent; 

	position:absolute; 

	float:right; 

	z-index: 10000; 

	top: 0px; 

	right: 0px;

}

</style>


<div class="bs-example">
    <nav id="myNavbar" class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<img src="style/cats/images/cats2.png" style="height: 54px"/>
                
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">CATS <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:top.show_screen('actions','search')">Actions</a></li>
                            <li><a href="javascript:top.show_screen('audit_and_inspections','search')">Audits & Inspections</a></li>
                            <li><a href="javascript:top.show_screen('government_requirements','search')">Government Inspections</a></li>
                            <li><a href="javascript:top.show_screen('major_hazards','search')">High Level Business Risks</a></li>
                            <li><a href="javascript:top.show_screen('incidents','search')">Incidents</a></li>
                            <li><a href="javascript:top.show_screen('meeting_minutes','search')">Meeting Minutes</a></li>
                            <li><a href="javascript:top.show_screen('schedules','search')">Schedules</a></li>
                            <li><a href="javascript:top.show_screen('site_specific','search')">Site Specific Obligations</a></li>
                            <li><a href="javascript:top.show_screen('dashboard','search')">Workload Review</a></li>
                            <li class="divider"></li>
                            <li><a href="javascript:top.show_screen('other_reports','search')">Other Records</a></li>

                        </ul>
                    </li>
                    <li><a href="javascript:top.show_screen('early_advice','search')">Early Advice</a></li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Plant Change Request <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:top.show_screen('pcr_maintain_actions','search')">Maintain Actions</a></li>
                            <li><a href="javascript:top.show_screen('pcr_maintain_email','search')">Maintain Email Messages</a></li>
                            <li><a href="javascript:top.show_screen('pcr_meetings','search')">Maintain Meetings</a></li>
                            <li><a href="javascript:top.show_screen('pcr_maintain_reviewers','search')">Maintain Reviewers</a></li>
                            <li><a href="javascript:top.show_screen('pcr_site_areas','search')">Maintain PCR Site Areas</a></li>
                            <li><a href="javascript:top.show_screen('pcr_search','search')">Search PCRS</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:top.show_screen('employees','search')">Employees</a></li>
                            <li><a href="javascript:top.show_screen('lost_days','search')">Lost Days</a></li>
                            <li><a href="javascript:top.show_screen('references','search')">Reference Table</a></li>
                            <li><a href="javascript:top.show_screen('reallocate','search')">Re-Allocate responsibility</a></li>
                            <li><a href="javascript:top.show_screen('work_hours','search')">Work Hours</a></li>
                        </ul>
                    </li>
                </ul>


				
				
				        <div class="nav navbar-nav navbar-right">
					         <img  src="style/cats/images/moon2.png" style="height: 54px"/>
                </div>				
				

            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
</div>
