<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_ADODB_PATH.'adodb.inc.php');
require_once (CATS_FUNCTIONS_PATH.'db_functions.php');
require_once (CATS_FUNCTIONS_PATH.'html_functions.php');
require_once (CATS_FUNCTIONS_PATH.'js_functions.php');
require_once (CATS_FUNCTIONS_PATH.'main_functions.php');
require_once (CATS_FUNCTIONS_PATH.'cats_functions.php');

if(isset($_REQUEST['uid'])) $uid = $_REQUEST['uid'];
if(isset($_REQUEST['pwd'])) $pwd = $_REQUEST['pwd'];

if(isset($_REQUEST['mode'])) $mode = $_REQUEST['mode'];
if(isset($_REQUEST['rurl'])) $rurl = $_REQUEST['rurl'];
if(isset($_REQUEST['n'])) $n = $_REQUEST['n']; // name
$p=(isset($_REQUEST['p'])) ? $_REQUEST['p'] : '';
$id=(isset($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
if(isset($_REQUEST['action'])) $action = $_REQUEST['action'];
if(!isset($action)) {
	if(isset($_REQUEST['a'])) $a = $_REQUEST['a']; // action shorthand
	$action = $a;
}

//print_r($p);
$s="";
switch($action){
	case 'get_checkboxes_array':
		$names = (isset($p))?explode("|",$p):array();
		$type = $_GET['type'];
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','type':'$type','options':";
		$c = '{';
		foreach($names as $name){
			//$sql = "Select Checkbox_Id as ID, Checkbox_Desc as TEXT, Checkbox_Comment as TITLE, 0 as CHECKED from tblIncidentForm_Checkboxes WHERE Checkbox_Type = '$name'";
			$sql = "Select Checkbox_Id as ID, Checkbox_Comment as TEXT, Checkbox_Desc as TITLE, 1 as CHECKED from tblIncidentForm_Checkboxes WHERE Checkbox_Type = '$name'";
			$arr=db_get_all($sql);
			$c .= "'$name':" . js_get_checkboxes($arr) . ",";
		}
		$s .= preg_replace("/,$/i","}",$c);
		exit('{'.$s.'}');
		break;
	case 'get_edit_checkboxes_array':
		$names = (isset($p))?explode("|",$p):array();
		$type = $_GET['type'];
		$types = cats_get_checkboxes_values_array();
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','type':'$type','options':";
		$c = '{';
		foreach($names as $name){
			$inc_type = $types[$name];
			//$sql = "SELECT B.Checkbox_Id as ID, B.Checkbox_Desc as TEXT, B.Checkbox_Comment as TITLE, count(I.IncidentId) as CHECKED FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = '$inc_type' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = '$name' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
			$sql = "SELECT B.Checkbox_Id as ID, B.Checkbox_Comment as TEXT, B.Checkbox_Desc as TITLE, count(I.IncidentId) as CHECKED FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = '$inc_type' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = '$name' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
			$arr=db_get_all($sql);
			$c .= "'$name':" . js_get_checkboxes($arr) . ",";
		}
		$s .= preg_replace("/,$/i","}",$c);
		exit('{'.$s.'}');
		break;
	case 'get_sections_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$where='';
		if($id!=0)	$where = " where department_id = $id ";
		$sql = "SELECT Section_Id as ID, Section_Description as TEXT FROM tblSection $where ORDER BY Section_Description ASC";
		$arr=db_get_all($sql);
		$s .= js_get_options($arr);
		exit('{'.$s.'}');
		break;
	case 'get_alpha_incidents_array':
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','options':";
		$s .= get_alpha_incidents($p);
		exit('{'.$s.'}');
		break;
	default:
		break;
}

function get_alpha_incidents($filter){
	$sql = "SELECT Incident_Number as ID, Incident_Number as TEXT FROM tblIncident_Details WHERE Incident_Number like '$filter%' ORDER BY Incident_Number ASC";
	$arr=db_get_all($sql);
	return js_get_options($arr);
}
?>
