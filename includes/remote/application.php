<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_ADODB_PATH.'adodb.inc.php');
require_once (CATS_FUNCTIONS_PATH.'db_functions.php');
require_once (CATS_FUNCTIONS_PATH.'html_functions.php');
require_once (CATS_FUNCTIONS_PATH.'js_functions.php');
require_once (CATS_FUNCTIONS_PATH.'main_functions.php');

$p=(isset($_REQUEST['p'])) ? $_REQUEST['p'] : '';
$a=(isset($_REQUEST['a'])) ? $_REQUEST['a'] : '';
$n=(isset($_REQUEST['n'])) ? $_REQUEST['n'] : '';
$module=(isset($_REQUEST['rm'])) ? $_REQUEST['rm'] : '';
$action=$a;
$s="";
switch($action){
	case 'get_edit_templates_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		$arr=get_edit_templates($module);
		$s .= get_template_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'get_edit_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		$s .= get_edit_template($module,$id);
		exit('{'.$s.'}');
		break;
	case 'get_search_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		$s .= get_search_template($module,$id);
		exit('{'.$s.'}');
		break;
	case 'new_edit_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		save_post($module,'template',$_GET);
		$arr=get_edit_templates($module);
		$s .= get_template_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'save_edit_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		save_post($module,'template',$_GET,$id);
		$arr=get_edit_templates($module);
		$s .= get_template_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'delete_edit_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		delete_post($module,'template',$_GET,$id);
		$arr=get_edit_templates($module);
		$s .= get_template_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'new_search_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		save_post($module,'filter',$_GET);
		$arr=get_search_templates($module);
		$s .= get_search_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'save_search_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		save_post($module,'filter',$_GET,$id);
		$arr=get_search_templates($module);
		$s .= get_search_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'delete_search_template':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		delete_post($module,'filter',$_GET,$id);
		$arr=get_search_templates($module);
		$s .= get_search_groups($arr);
		exit('{'.$s.'}');
		break;
	case 'get_search_templates_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','options':";
		$arr=get_search_templates($module);
		$s .= get_search_groups($arr);
		exit('{'.$s.'}');
		break;
	default:
		break;
}


function get_template_groups($arr){
	return '[[\'Actions\'],[[\'\',\'\'],[\'Action::New...\',\'New...\'],[\'Action::Save...\',\'Save...\',true],[\'Action::Delete...\',\'Delete...\',true]],[\'Templates\'],'.js_get_options($arr,'ID','TEXT',false) . ']';
}
function get_edit_templates($m){
	$uid = $_SESSION['user_details']['user_id'];
	$sql = "SELECT id as ID , template_name as TEXT from templates where employee_number = {$uid} and module = '{$m}'";
	//echo($sql);
	$arr=db_get_all($sql);
	return $arr;
}
function get_edit_template($m,$id){
	//$m='actions';
	$uid = $_SESSION['user_details']['user_id'];
	$sql = "SELECT template_object from templates where employee_number = {$uid} and module = '{$m}' and id = {$id}";
	//echo $sql;
	$s = db_get_one($sql);
	$arr = unserialize($s);
	//print_r($arr);
	$ret = '';
	if(is_array($arr)){
		$ret = array2js($arr);
	}
	
	return $ret;
}
function get_search_groups($arr){
	return '[[\'Actions\'],[[\'\',\'\'],[\'Action::New...\',\'New...\'],[\'Action::Save...\',\'Save...\',true],[\'Action::Delete...\',\'Delete...\',true]],[\'Filters\'],'.js_get_options($arr,'ID','TEXT',false) . ']';
}
function get_search_templates($m){
	$uid = $_SESSION['user_details']['user_id'];
	$sql = "SELECT id as ID , filter_name as TEXT from filters where employee_number = {$uid} and module = '{$m}'";
	//echo($sql);
	$arr=db_get_all($sql);
	return $arr;
}
function get_search_template($m,$id){
	//$m='actions';
	$uid = $_SESSION['user_details']['user_id'];
	$sql = "SELECT filter_object from filters where employee_number = {$uid} and module = '{$m}' and id = {$id}";
	//echo $sql;
	$s = db_get_one($sql);
	$arr = unserialize($s);
	//print_r($arr);
	$ret = '';
	if(is_array($arr)){
		$ret = array2js($arr);
	}
	
	return $ret;
}
function array2js($arr){
	if(is_array($arr)){
		foreach($arr as $key => $val){
			$ret .= "['$key',".array2js($val)."],";
		}
		$ret = '['.preg_replace("/,$/i","",$ret).']';
	}else{
		$ret = "'{$arr}'";
	}
	return $ret;
}
function save_post($m,$type,$arr,$id='',$del=false){
	global $db;
	parse_str($arr['p'],$object);
	switch($type){
		case 'template':
			$uid = $_SESSION['user_details']['user_id'];
			$gmask = $_SESSION['user_details']['groups'];
			if($id!=''){
				if($del)
					$sql="delete from templates where id = '{$id}' and employee_number = {$uid} ";
				else
					$sql="update templates set template_object = '".serialize($object)."' where id = '{$id}' and employee_number = {$uid} ";
			}else{
				$sql="insert into templates ( template_name, template_description, template_object, template_type , module , employee_number , group_mask ) values( '{$object['TEMPLATE_NAME']}','{$object['TEMPLATE_NAME']}' , '".serialize($object)."' , 'search' , '{$m}' , {$uid} , {$gmask}) ";
			}
			break;
		case 'filter':
			$uid = $_SESSION['user_details']['user_id'];
			$gmask = $_SESSION['user_details']['groups'];
			if($id!=''){
				if($del)
					$sql="delete from filters where id = '{$id}' and employee_number = {$uid} ";
				else
					$sql="update filters set filter_object = '".serialize($object)."' where id = '{$id}' and employee_number = {$uid} ";
			}else{
				$sql="insert into filters ( filter_name, filter_description, filter_object, filter_type , module , employee_number , group_mask ) values( '{$object['TEMPLATE_NAME']}','{$object['TEMPLATE_NAME']}' , '".serialize($object)."' , 'search' , '{$m}' , {$uid} , {$gmask}) ";
			}
			break;
		default:
			
			break;
	}
	
	$ret=$db->Execute($sql);
	if($ret){
		
	}else{
		echo $sql."\n\n";
		echo $db->ErrorMsg();
	}
}
function delete_post($m,$type,$arr,$id){
	save_post($m,$type,$arr,$id,true);
}

?>
