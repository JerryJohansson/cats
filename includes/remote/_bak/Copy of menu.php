<?php
$m=(isset($_REQUEST['m']))?$_REQUEST['m']:'dashboard';
$t=(isset($_REQUEST['t']))?$_REQUEST['t']:'CATS';

/*array(
	'actions|government_inspections|major_hazards|other_records|site_specific';
	'Actions|Government Inspections|Major Hazards Register|Other Records|Site Specific Obligations';
	
	'audits_and_inspections|incidents|meeting_minutes|schedules|workload_review';
	'Audits & Inspections|Incidents|Meeting Minutes|Schedules|Workload Review';

	
	'organisation_structure|employees|references|work_hours|lost_days';
	'Organisation Structure|Employees|Reference Table|Work Hours|Lost Days';
	
	'reallocate|checkboxes|risk_definitions|motd|email_configuration';
	'Re-Allocate Responsibility|Incident Form Checkboxes|Risk Definitions|Message of the day|Email Configuration';
	
	'pcr_search|pcr_maintain_reviewers';
	'PCR Search|Maintain Reviewers';
	
	'pcr_maintain_actions|pcr_maintain_email';
	'Maintain Actions|Email Messages';
)*/

?>
<div id="menu_cont">
	<table align="center" width="100%" class="title"><tr><td id="menu_title"><?php echo($t);?></td><td align="right"><img src="style/images/btn_close.gif" onmousedown="this.src='style/images/btn_close_down.gif'" onmouseout="this.src='style/images/btn_close.gif'" onclick="parent.doMenu();" /></td></tr></table>
	<table align="center" width="100%" class="title"><tr><td><div id="menu">
<?php
	switch($m){
		case 'cats':
			?>
<div id="menu_cats">
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	<a href="javascript:parent.doMenu('actions');" class="actions">Actions</a>
	<a href="javascript:parent.doMenu('government_inspections');" class="government_inspections">Government Inspections</a>
	<a href="javascript:parent.doMenu('major_hazards');" class="major_hazards">Major Hazards Register</a>
	<a href="javascript:parent.doMenu('other_records');" class="other_records">Other Records</a>
	<a href="javascript:parent.doMenu('site_specific');" class="site_specific">Site Specific Obligations</a>
	</td>
	<td id="splashLinks" width="50%">
	<a href="javascript:parent.doMenu('audits_and_inspections');" class="audits_and_inspections">Audits & Inspections</a>
	<a href="javascript:parent.doMenu('incidents');" class="incidents">Incidents</a>
	<a href="javascript:parent.doMenu('meeting_minutes');" class="meeting_minutes">Meeting Minutes</a>
	<a href="javascript:parent.doMenu('schedules');" class="schedules">Schedules</a>
	<a href="javascript:parent.doMenu('workload_review');" class="workload_review">Workload Review</a>
	</td>
</tr>
</table>
</div>
			<?php
			break;
		case 'admin':
			?>
<div id="menu_admin">
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	<a href="javascript:parent.doMenu('organisation_structure');" class="organisation_structure">Organisation Structure</a>
	<a href="javascript:parent.doMenu('employees');" class="employees">Employees</a>
	<a href="javascript:parent.doMenu('references');" class="references">Reference Table</a>
	<a href="javascript:parent.doMenu('work_hours');" class="work_hours">Work Hours</a>
	<a href="javascript:parent.doMenu('lost_days');" class="lost_days">Lost Days</a>
	</td>
	<td id="splashLinks" width="50%">
	<a href="javascript:parent.doMenu('reallocate');" class="reallocate">Re-Allocate Responsibility</a>
	<a href="javascript:parent.doMenu('checkboxes');" class="checkboxes">Incident Form Checkboxes</a>
	<a href="javascript:parent.doMenu('risk_definitions');" class="risk_definitions">Risk Definitions</a>
	<a href="javascript:parent.doMenu('motd');" class="motd">Message of the day</a>
	<a href="javascript:parent.doMenu('email_configuration');" class="email_configuration">Email Configuration</a>
	</td>
</tr>
</table>
</div>
			<?php
			break;
		case 'pcr':case 'pcr_admin':
			?>
<div id="menu_pcr_admin">
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	<a href="javascript:parent.doMenu('pcr_search');" class="pcr_search">PCR Search</a>
	<a href="javascript:parent.doMenu('pcr_maintain_reviewers');" class="pcr_maintain_reviewers">Maintain Reviewers</a>
	</td>
	<td id="splashLinks" width="50%">
	<a href="javascript:parent.doMenu('pcr_maintain_actions');" class="pcr_maintain_actions">Maintain Actions</a>
	<a href="javascript:parent.doMenu('pcr_maintain_email');" class="pcr_maintain_email">Email Messages</a>
	</td>
</tr>
</table>
</div>
			<?php
			break;
		default:
			?>
<div id="top_logo">
<a href="javascript:doMenu('dashboard')" class="cats_home"></a>
<a href="javascript:showMenu('main', 'CATS - Main Menu')" class="cats">CATS</a>
<a href="javascript:showMenu('admin', 'CATS - Administration')" class="admin">ADMIN</a>
<a href="javascript:showMenu('pcr_admin', 'PCR - Administration')" class="pcr">PCR</a>
<a name="title" id="page_title"></a>
</div>
			<?php
			break;
	}

?></div></td></tr></table>
</div>
