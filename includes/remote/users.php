<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');
require_once (CATS_FUNCTIONS_PATH . 'js_functions.php');
require_once (CATS_CLASSES_PATH.'users.php');
require_once (CATS_ADODB_PATH.'adodb.inc.php');

if(isset($_REQUEST['uid'])) $uid = $_REQUEST['uid'];
if(isset($_REQUEST['pwd'])) $pwd = $_REQUEST['pwd'];
if(isset($_REQUEST['action'])) $action = $_REQUEST['action'];
if(!isset($action)) {
	if(isset($_REQUEST['a'])) $a = $_REQUEST['a']; // action shorthand
	$action = $a;
}
if(isset($_REQUEST['mode'])) $mode = $_REQUEST['mode'];
if(isset($_REQUEST['rurl'])) $rurl = $_REQUEST['rurl'];

if(isset($_REQUEST['p'])) $p = $_REQUEST['p']; // param
//if(isset($_REQUEST['m'])) $m = $_REQUEST['m']; // mode
if(isset($_REQUEST['n'])) $n = $_REQUEST['n']; // name
if(isset($_REQUEST['id'])) $id = $_REQUEST['id']; // id
if(isset($_REQUEST['site_id'])) $site_id = $_REQUEST['site_id'];
//$site_id = 3;
$s = '';
//echo($uid.':'.$pwd);
//echo $action;
$user = new User;
switch($action){
	case 'Login':
		$ret = $user->login($uid,$pwd,$action,$rurl);
		exit($ret);
		break;
	case 'Logout':
		$ret = $user->login($uid,$pwd,$action,$rurl);
		exit($ret);
		break;
	case 'get_employee_array':
		$filter = (isset($p))?$p:"";
		$s .= (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		if(!empty($filter)){
			if(isset($site_id)) $user->site_id = $site_id;
			$ret = $user->getEmployeesArray($id,$filter);
			$s .= js_get_options($ret);
		}else{
			$s .= '[]';
		}
		
		exit('{'.$s.'}');
		break;
	case 'check_login':
		$uid = $_GET['uid'];
		$pwd = $_GET['pwd'];
		$where = " WHERE (UPPER(ACCESS_ID) = UPPER('$uid') AND UPPER(PASSWORD) = UPPER('$pwd'))";
		if(check_employee_details($where))
			exit("true");
		else
			exit("false");
		break;
	case 'check_employee_name':
		$fname = $_GET['fname'];
		$lname = $_GET['lname'];
		$where = " WHERE (UPPER(FIRST_NAME) = UPPER('$fname') AND UPPER(LAST_NAME) = UPPER('$lname')) ";
		if(check_employee_details($where))
			exit("true");
		else
			exit("false");
		break;
	case 'check_employee_number':
		$enum = $_GET['enum'];
		$where = " WHERE EMPLOYEE_NUMBER = $enum ";
		if(check_employee_details($where))
			exit("true");
		else
			exit("false");
		break;
	case 'check_induction_number':
		$inum = $_GET['inum'];
		$where = " WHERE INDUCTION_NUMBER = $inum ";
		if(check_employee_details($where))
			exit("true");
		else
			exit("false");
		break;
	case 'renew':
		$ret = $user->renew_session();
		exit('{'.$ret.'}');
		break;
	default:
		break;
}

// private functions
function check_employee_details($where){
	$sql = "select employee_number from tblemployee_details $where ";
	$emp = db_get_one($sql);
	return $emp;
}
?>
