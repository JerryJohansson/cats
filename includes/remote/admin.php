<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS User Manager - Admin
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
$config_details_only=true;
require_once ("../conf.php");
require_once (CATS_ADODB_PATH.'adodb.inc.php');
require_once (CATS_FUNCTIONS_PATH.'db_functions.php');
require_once (CATS_FUNCTIONS_PATH.'html_functions.php');
require_once (CATS_FUNCTIONS_PATH.'js_functions.php');
require_once (CATS_FUNCTIONS_PATH.'main_functions.php');

if(isset($_REQUEST['uid'])) $uid = $_REQUEST['uid'];
if(isset($_REQUEST['pwd'])) $pwd = $_REQUEST['pwd'];

if(isset($_REQUEST['mode'])) $mode = $_REQUEST['mode'];
if(isset($_REQUEST['rurl'])) $rurl = $_REQUEST['rurl'];
if(isset($_REQUEST['n'])) $n = $_REQUEST['n']; // name
$p=(isset($_REQUEST['p'])) ? $_REQUEST['p'] : '';
$id=(isset($_REQUEST['id'])) ? $_REQUEST['id'] : 0;
if(isset($_REQUEST['action'])) $action = $_REQUEST['action'];
if(!isset($action)) {
	if(isset($_REQUEST['a'])) $a = $_REQUEST['a']; // action shorthand
	$action = $a;
}

$s="";
switch($action){
	case 'get_functions_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$arr=html_form_functions_options($p);//preg_replace("/(functions.php)/i","",$p));
		$s .= js_get_options($arr,'id','text');
		exit('{'.$s.'}');
		break;
	case 'get_tables_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$arr=db_get_tables();
		$s .= js_get_options($arr,'CNAME','CNAME');
		exit('{'.$s.'}');
		break;
	case 'get_columns_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$arr=db_get_columns($p);
		$s .= js_get_options($arr,'CNAME','CNAME');
		exit('{'.$s.'}');
		break;
	case 'get_departments_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$where='';
		if($id!=0)	$where = " where site_id = $id ";
		//$sql = "select Department_Id as ID, Department_Description as TEXT FROM tblDepartment $where ORDER BY Department_Description ASC";
		$sql = get_sql_dd('department',false,false,$where);
		$arr=db_get_all($sql);
		$s .= js_get_options($arr);
		exit('{'.$s.'}');
		break;
	case 'get_sections_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$where='';
		if($id!=0)	$where = " where department_id = $id ";
		//$sql = "SELECT Section_Id as ID, Section_Description as TEXT FROM tblSection $where ORDER BY Section_Description ASC";
		$sql = get_sql_dd('section',false,false,$where);
		$arr=db_get_all($sql);
		$s .= js_get_options($arr);
		exit('{'.$s.'}');
		break;
	case 'get_shifts_array':
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$where='';
		if($id!=0)	$where = " where section_id = $id ";
		$sql = get_sql_dd('shift',false,false,$where);
		$arr=db_get_all($sql);
		$s .= js_get_options($arr);
		exit('{'.$s.'}');
		break;
	case 'get_risk_definitions_array':
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id',";
		$s .= risk_definition_array($id, '0');
		$s .= ",";
		$s .= risk_definition_array($id, '1');
		exit('{'.$s.'}');
		break;
	case 'check_companies':
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id',";
		$s .= check_companies($id, $p);
		exit('{'.$s.'}');
		break;
	case 'check_mims_companies':
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id',";
		$s .= check_mims_companies($id, $p);
		exit('{'.$s.'}');
		break;
	case 'get_alpha_companies_array':
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','options':";
		$s .= get_alpha_companies($p);
		exit('{'.$s.'}');
		break;
	case 'get_config_control':
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'html':";
		$s .= "'".get_config_control($p,$id)."'";
		exit('{'.$s.'}');
		break;
	case 'get_pcr_area_array':	
		$filter = (isset($p))?$p:"";
		$s = (isset($n))?"'name':'$n',":"";
		$s .= "'m':'$m','a':'$action','id':'$id','filter':'$filter','options':";
		$where='';
		if($id!=0)	$where = " where site_id = $id ";
		//$sql = "SELECT Section_Id as ID, Section_Description as TEXT FROM tblSection $where ORDER BY Section_Description ASC";
		//$sql = "SELECT Area_Id ID, Area_Description TEXT FROM tblPCR_Area $where ORDER BY Area_Description ASC";
		$sql = get_sql_dd('pcr_area',false,$where);
		$arr=db_get_all($sql);
		$s .= js_get_options($arr);
		exit('{'.$s.'}');
		break;
	default:
		break;
}
// private functions
function get_config_control($name, $value){
	$arr = array(
		"CATS_DEFAULT_ROW_COUNT"=>"html_row_count_dd",
		"CATS_REUSE_LOGIN"=>"html_yes_no_dd",
		"DEFAULT_STYLE"=>"html_get_skin_dd",
		"CATS_MAINTENANCE"=>"html_yes_no_dd",
		"CATS_HISTORY_LENGTH"=>"html_draw_input_field"
	);
	return $arr[$name]('CONFIGURATION_VALUE[]', $value, ' style="width:100%" ');
}

function get_alpha_companies($filter){
	$sql = "SELECT Supplier_No as ID, Supplier_Name || '-' || Supplier_No as TEXT FROM MIMS_Suppliers WHERE UPPER(Supplier_Name) like '$filter%' ORDER BY Supplier_Name ASC";
	$arr=db_get_all($sql);
	return js_get_options($arr);
}

function check_companies($id, $name){
	global $db;
	$s = "";
	$new_id = 0;
	$sql = "SELECT CompanyId FROM tblCompanies  WHERE CompanyId = $id";
	$rs = db_query($sql);
	if($rs->RecordCount()>0){
	  //do nothing as this company already exists in the tblcompanies table
		$s .= "'company_id':0";
	}else{
		// Shouldn't Happen
	}
	return $s;
}

function check_mims_companies($id, $name){
	global $db;
	$s = "";
	$new_id = 0;
	$sql = "SELECT CompanyId FROM tblCompanies  WHERE MIMS_Id = '$id'";
	$rs = db_query($sql);
	if($rs->RecordCount()>0){
	  //do nothing as this company already exists in the tblcompanies table
		$s .= "'company_id':0";
	}else{
		//trim out the MIMS ID
		$companyname = str_replace("-".$id, "", $name);
	  //we need to add this company to the tblcompanies table
		$sql =  "Insert into tblCompanies ( MIMS_Id, CompanyName, CompanyStatus) values ('".$id."',q'{".$companyname."}','Active')";
	  	$ret = $db->Execute($sql);
		$new_id=$db->Insert_ID("Companies_seq");
		$s .= "'company_id':$new_id";
	}
	return $s;
}

function risk_definition_array($type = '1', $category = '0', $parameters = ''){
	global $db;
	//$db->debug=true;
	$categories = array('Consequence', 'Likelihood');
	$sql = "SELECT RISKDEFID as ID, RISKDEFDESC as TEXT FROM View_Risk_Definitions WHERE RiskDefType = $type AND RiskRangeCL = $category ";
	$arr=db_get_all($sql);
	
	$category_name = $categories[$category];
	$s="'$category_name':[";
	$id='ID';
	$text='TEXT';
	foreach($arr as $key => $opt){
		$s .= "['{$opt[$id]}','{$opt[$text]}'],";
	}
	$s = preg_replace("/,$/i","",$s);
	$s .= "]";
	return $s;
}
?>
