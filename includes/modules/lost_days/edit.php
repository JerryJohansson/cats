<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
$RECORD=false;
if($id>0){
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	if(cats_user_is_reader() && $p=='view')
		$grant_access =true;
if($p=='edit' && (cats_user_is_super_administrator() || cats_user_is_administrator() || cats_user_is_editor(true) || cats_user_is_reader(true))){
		$grant_access = true;
	}
	if($grant_access == false){
		$_SESSION['messageStack']->add("You do not have access to the Lost Days details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}else{
	if($a=='add'){
		//$_POST["INCIDENT_NUMBER"] = isset($_REQUEST['INCIDENT_NUMBER'])?$_REQUEST['INCIDENT_NUMBER']:0;
	}
}
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	init_resize_editor();
	return true;
}
onload = function(){
	init();
}

/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

</script>

</head>
<body class="edit">
<?php
// Assign what javascript handler to use for client side validation
$onSubmit = '';
if ($action == 'edit')
    $onSubmit = "return CATS_validateForm(this, 'YEARS','','RisNum','LOST_DAYS','','RisNum');";
else
    $onSubmit = "return CATS_validateForm(this, 'INCIDENT_NUMBER','','R','YEARS','','RisNum','LOST_DAYS','','RisNum');";

?>
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="<?php echo($onSubmit) ?>">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
</fieldset>
<fieldset class="tool_bar">
<?php if($action=='edit'){ ?>
<a
	title="New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed = new Editor("edit");

// Check whether to edit or add a new record
if($id > 0)
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id));
else
	echo($ed->buildForm($FIELDS));

?>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" class="submit" name="cats::Save" value="Save" >
<?php if($action=='edit'){ ?>
<input type="button" class="button" name="cats::New" value="New Lost Day" onClick="_m.newModule();" >
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>
</div>
</form>