<?php



/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search'){
	$ret = array();
	switch($action){
		case 'search':
		case 'results':			
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Lost Days',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Lost Day',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Lost Days',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables()
{
	return array(
		'table'=>'TBLLOST_DAYS', 
		'id'=>'LOST_DAYS_ID',
		'view'=> 'VIEW_LOST_DAYS'
	);
}


/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{
	$ret = array();
	
	switch($action)
	{	
		case 'results':
		case 'search': // search
			$ret = array(
				'INCDISPLYID' => array('label'=>'Incident Number', 'delim'=>"'", 'operator'=>'IN', 'value'=>'', 'func'=>'html_draw_string_in_clause_field', 'col'=>1, 'col_span'=>2), 
				'INJURED_PERSON_ID' => array('label'=>'Injured Persons Name', 'value'=>NULL, 'func'=>'html_draw_injured_employee_helper_search', 'col'=>1, 'col_span'=>2),
				'MONTHS_FROM' => array('label'=>'Month From', 'operator'=>'NUM_FROM', 'value'=>NULL, 'func'=>'html_months_search_dd', 'col'=>1, 'col_span'=>1), 
				'MONTHS_TO' => array('label'=>'Month To', 'operator'=>'NUM_TO', 'value'=>NULL, 'func'=>'html_months_search_dd', 'col'=>2, 'col_span'=>1),
				'YEARS_FROM' => array('label'=>'Year From', 'operator'=>'NUM_FROM', 'value'=>NULL, 'func'=>'html_draw_number_field', 'params2'=>'4', 'col'=>1, 'col_span'=>1),
				'YEARS_TO' => array('label'=>'Year To', 'operator'=>'NUM_TO', 'value'=>NULL, 'func'=>'html_draw_number_field', 'params2'=>'4', 'col'=>2, 'col_span'=>1)
			);
			break;	
		case 'new':
			$ret =  array(
				'INCIDENT_NUMBER' => array('label'=>'Incident Number', 'value'=>$_GET['INCIDENT_NUMBER'], 'func'=>'html_form_draw_incident_numbers_display', 'col'=>1, 'col_span'=>2), 
				'EMP_NAME' => array('label'=>'Injured Person\'s Name', 'value'=>$_GET['EMP_ID'], 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>2), 
				'MONTHS' => array('label'=>'Month', 'value'=>NULL, 'func'=>'html_months_dd', 'col'=>1, 'col_span'=>2), 
				'YEARS' => array('label'=>'Year', 'value'=>NULL, 'func'=>'html_years_dd', 'col'=>1, 'col_span'=>2), 
				'LOST_DAYS' => array('label'=>'Lost Calendar Days', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2), 
				'RESTRICTED_DAYS' => array('label'=>'Restricted Calendar Days', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2),
				'ROSTERED_DAYS' => array('label'=>'Lost Rostered Days', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'edit':
			$ret =  array(
				'INCIDENT_NUMBER' => array('label'=>'Incident Number', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'EMP_NAME' => array('label'=>'Injured Person\'s Name', 'value'=>'Calculated From Incident Number', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'MONTHS' => array('label'=>'Month', 'value'=>NULL, 'func'=>'html_months_dd', 'col'=>1, 'col_span'=>2), 
				'YEARS' => array('label'=>'Year', 'value'=>NULL, 'func'=>'html_years_dd', 'col'=>1, 'col_span'=>2), 
				'LOST_DAYS' => array('label'=>'Lost Calendar Days', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2), 
				'RESTRICTED_DAYS' => array('label'=>'Restricted Calendar Days', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2),
				'ROSTERED_DAYS' => array('label'=>'Lost Rostered Days', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$ret =  array(
				'INCIDENT_NUMBER' => array('label'=>'Incident Number', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'EMP_NAME' => array('label'=>'Injured Persons Name', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'MONTHS' => array('label'=>'Month', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'YEARS' => array('label'=>'Year', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'LOST_DAYS' => array('label'=>'Lost Days', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'RESTRICTED_DAYS' => array('label'=>'Restricted Days', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'ROSTERED_DAYS' => array('label'=>'Rostered Days', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
			break;

	}
	// set default request form values used to store return values to redirect user back to orginating documents etc...
	// this array will merge with the other page fields(edit/new)
	if(isset($_GET['rm'])){
		$ret = array_merge($ret, 
			array(
				'rm' => array('label'=>'Return module', 'value'=>$_GET['rm'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'rp' => array('label'=>'Return page', 'value'=>$_GET['rp'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'rid' => array('label'=>'Return REPORT_ID', 'value'=>$_GET['rid'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'rt' => array('label'=>'Return tab', 'value'=>$_GET['rt'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
			)
		);
	}
	$ret = array_merge($ret, 
		array('rurl' => array('label'=>'Referrer', 'value'=>$_SERVER['HTTP_REFERER'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0))
	);
	return $ret;
}



// Fix page parameter
if($p=='post')
	$pg=(isset($a) && $a=='add')?'new':'edit';
else
    $pg=$p;


// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($pg);
$TABLES  = getPropertiesTables($pg);
$FIELDS  = getPropertiesFields($pg);

?>