<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Site',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Site',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Organisational Structure',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=> 'TBLSITE', 
					'id'=>'SITE_ID',
					'view'=> 'VIEW_ORGANISATIONAL_STRUCTURE',
					'filter'=> '1=2'
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=> 'TBLSITE', 
					'id'=>'SITE_ID',
					'view'=> 'VIEW_ORGANISATIONAL_STRUCTURE',
					'filter'=> '1=2'
				)
			);
			break;
		case 'department':
			$ret = array_merge(
				array(
					'table'=> 'TBLDEPARTMENT', 
					'id'=>'DEPARTMENT_ID',
					'view'=> 'VIEW_ORGANISATIONAL_STRUCTURE',
					'filter'=> '1=2'
				)
			);
			break;
		case 'section':
			$ret = array_merge(
				array(
					'table'=> 'TBLSECTION', 
					'id'=>'SECTION_ID',
					'view'=> 'VIEW_ORGANISATIONAL_STRUCTURE',
					'filter'=> '1=2'
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=> 'TBLSITE', 
					'id'=>'SITE_ID',
					'view'=> 'VIEW_ORGANISATIONAL_STRUCTURE',
					'filter'=> '1=2'
				)
			);
			break;
	}
	return $ret;
}


function getPropertiesFields($page = 'search'){
	global $a;
	$ret = array();
	switch($page){
		case 'edit';
			$ret = array(
				'SITE_ID' => array('label'=>'Site ID', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
		case 'new':
			$ret = array_merge($ret, 
				array(
					'SITE_DESCRIPTION' => array('label'=>'Site Description', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'EMAIL_FROM_ADDRESS' => array('label'=>'Email From Address', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PCR_ADMIN_EMAIL' => array('label'=>'PCR Admin Email', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'MIMS_DISTRICT' => array('label'=>'MIMS District', 'value'=>NULL, 'func'=>'html_draw_input_field', 'maxlength'=>'4', 'col'=>1, 'col_span'=>2),
					'SITE_CODE' => array('label'=>'Site Code', 'value'=>NULL, 'func'=>'html_draw_input_field', 'maxlength'=>'2', 'col'=>1, 'col_span'=>2),
					'SITE_FOLDER' => array('label'=>'Default Folder Location', 'value'=>NULL, 'func'=>'html_get_folder_location_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search': case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>1, 'col_span'=>2), 
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'department':
			if($a == 'add'){
				$ret = array(
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department Description', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'DEPARTMENT_NAME' => array('label'=>'Department Name', 'value'=>'department_name', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'DEPARTMENT_INACTIVE_FLAG' => array('label'=>'In-Active', 'value'=>'', 'func'=>'html_draw_y_n_radioset', 'col'=>2, 'col_span'=>2),
					'INACTIVE_FLAG' => array('label'=>'In-Active (table name)', 'value'=>'INACTIVE_FLAG', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)

				);
			}else{
				$ret = array(
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'DEPARTMENT_ID' => array('label'=>'Department ID', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department Description', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'DEPARTMENT_INACTIVE_FLAG' => array('label'=>'In-Active', 'value'=>'', 'func'=>'html_draw_y_n_radioset', 'col'=>2, 'col_span'=>2),
					'INACTIVE_FLAG' => array('label'=>'In-Active (table name)', 'value'=>'INACTIVE_FLAG', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)

				);
			}
			break;
		case 'section':
			if($a=='add'){
				$ret = array(
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
					'SECTION_DESCRIPTION' => array('label'=>'Section Description', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SECTION_NAME' => array('label'=>'Section Name', 'value'=>'section_name', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'SECTION_INACTIVE_FLAG' => array('label'=>'In-Active', 'value'=>'', 'func'=>'html_draw_y_n_radioset', 'col'=>2, 'col_span'=>2),
					'INACTIVE_FLAG' => array('label'=>'In-Active (table name)', 'value'=>'INACTIVE_FLAG', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)

				);
			}else{
				$ret = array(
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'SECTION_ID' => array('label'=>'Section ID', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'SECTION_DESCRIPTION' => array('label'=>'Section Description', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SECTION_INACTIVE_FLAG' => array('label'=>'In-Active', 'value'=>'', 'func'=>'html_draw_y_n_radioset', 'col'=>2, 'col_span'=>2),
					'INACTIVE_FLAG' => array('label'=>'In-Active (table name)', 'value'=>'INACTIVE_FLAG', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)

				);
			}
			break;
	}
	return $ret;
}
if($p=='post'){
	if(isset($_GET['page'])) $pg = $_GET['page'];
	else $pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);

?>