<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
// init the tabs index for tab iteration
$iTabs=0;
$RS=array();
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE="<?php echo $m;?>";
var PAGE="search";
var FORM_TARGET="<?php echo $results_iframe_target;?>";
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	init_resize_editor();
	return true;
}

/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

function do_cancel(){
	top.show_screen('dashboard','search');
}
function do_save(){
	esc_values();
	var f=document.forms[0];
	var o=f.elements['RiskPerspective']
	var id=o.options[o.selectedIndex].value;
	f.setAttribute("action","index.php?m="+MODULE+"&p=post&a=edit&id="+id);
	f.submit();
}
onload = function(){
	init();
	unesc_values();
}

//remotely go and get the values for the chosen perspective

function get_remote_values(){
	var f=document.forms[0];
	var o=f.elements['RiskPerspective'];
	if((x=o.selectedIndex)==0) return false;
	var id = o.options[x].value;
	var name = 'risk_definitions_helper';
	var url = cats_get_path(CATS_REMOTE_PATH,"admin");
	var obj={
		'a':'get_risk_definitions_array',
		'm':'admin',
		'n':name,
		'id':id
	};
	var q = remote._query(obj);
	oXML=_HTTPRequest(url,"post",set_remote_values,q);
}
function set_remote_values(o){
	try{
		eval('var e='+o);
		if(e){
			var con=e.Consequence;
			var lik=e.Likelihood;
			var f=document.forms[0];
			var els=f.elements;
			var x=parseInt(f.elements['Consequence_cnt'].value);
			var i=0;
			for(i=0;i<x;i++){ 
				els["Consequence_txt[" + i + "]"].value = unescape(con[i][1]);
				els["Likelihood_txt[" + i + "]"].value = unescape(lik[i][1]);
				els["Consequence[" + i + "]"].value = con[i][1];
				els["Likelihood[" + i + "]"].value = lik[i][1];
				els["ConsequenceID[" + i + "]"].value = con[i][0];
				els["LikelihoodID[" + i + "]"].value = lik[i][0];
			}
		}		
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n\n"+o);}
}
function esc_values() {
	var f=document.forms[0];
	var x=parseInt(f.elements['Consequence_cnt'].value);
	var i=0;
	for(i=0;i<x;i++){ 
		var objCon = element("Consequence_txt[" + i + "]");
		var objConEsc = element("Consequence[" + i + "]");
		objConEsc.value = escape(objCon.value);
		var objLike = element("Likelihood_txt[" + i + "]");
		var objLikeEsc = element("Likelihood[" + i + "]");
		objLikeEsc.value = escape(objLike.value);
	}
}
function unesc_values() {
	var f=document.forms[0];
	var x=parseInt(f.elements['Consequence_cnt'].value);
	var i=0;
	for(i=0;i<x;i++){ 
		var objCon = element("Consequence_txt[" + i + "]");
		var objConEsc = element("Consequence[" + i + "]");
		objCon.value = unescape(objConEsc.value);
		var objLike = element("Likelihood_txt[" + i + "]");
		var objLikeEsc = element("Likelihood[" + i + "]");
		objLike.value = unescape(objLikeEsc.value);
	}
}
</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data">
<div id="tool_bar_c">
<fieldset class="tool_bar">
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:do_save();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:do_cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo ucwords((string)$module_name_txt);?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1">
<tr id="row_REGISTER_ORIGIN">
 <td class="label"><label title="Risk Perspective">Risk Perspective</label></td>
	<td colspan="3">
		<?php echo html_form_draw_risk_perspectives_dd('RiskPerspective',$id,' onchange="get_remote_values();" ');?>
	</td>
</tr>
<?php
echo html_risk_definition_helper($id,'0');
echo html_risk_definition_helper($id,'1');
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="Submit" value="Save" onclick="do_save();">
<input type="button" class="button" name="Cancel" value="Cancel" onclick="do_cancel();">
<input type="button" class="button" name="Reset" value="Reset" onclick="_m.refresh();">
</fieldset>		
</fieldset>

</div>
</form>