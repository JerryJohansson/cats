<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once('properties.php');

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

//$db->debug=true;
$ret = false;
switch($action){
	case "add": // Update node
		//print_r($_POST);
		if(isset($_POST['Consequence'])){
			$categories = array('Consequence', 'Likelihood');
			$arr=array();
			$con = $_POST['Consequence'];
			$con_id = $_POST['ConsequenceID'];
			$lik = $_POST['Likelihood'];
			$lik_id = $_POST['LikelihoodID'];
			$x = sizeof($con);
			$i = 0;
			for($i=0;$i<$x;$i++){
				$sql = "insert into {$TABLES['table']} (RiskDefDesc)";
				$sql .= "  values('".$con[$i]."') ";
				$ok = db_query($sql);
				
				$sql = "insert into {$TABLES['table']} (RiskDefDesc) ";
				$sql .= "  values('".$lik[$i]."') ";
				$ok = db_query($sql);
			}
			if($ok){
				$ret = true;
			}
		}
		break;
	case "edit": // Update node
		//print_r($_POST);
		if(isset($_POST['Consequence'])){
			$categories = array('Consequence', 'Likelihood');
			$arr=array();
			$con = $_POST['Consequence'];
			$con_id = $_POST['ConsequenceID'];
			$lik = $_POST['Likelihood'];
			$lik_id = $_POST['LikelihoodID'];
			$x = sizeof($con);
			$i = 0;
			for($i=0;$i<$x;$i++){
				$sql = "update {$TABLES['table']} set ";
				$sql .= " RiskDefDesc = '".$con[$i]."'";
				$sql .= " where {$TABLES['id']} = ".$con_id[$i]." ";
				$ok = db_query($sql);
				
				$sql = "update {$TABLES['table']} set ";
				$sql .= " RiskDefDesc = '".$lik[$i]."'";
				$sql .= " where {$TABLES['id']} = ".$lik_id[$i]." ";
				$ok = db_query($sql);
			}
			if($ok){
				$_SESSION['messageStack']->add("Risk Definitions updated successfully.",'success');
				$ret = true;
			}else{
				$_SESSION['messageStack']->add("An error occured while updating Risk Definitions.<br>Error: ".$db->ErrorMsg());
				$ret = false;
			}
		}
		break;
	default:
		$ret = false;
		break;
}

if($ret) {
	$return_js_function = "window.location.href='index.php?m=$m&p=edit&id=$id';";
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
// house cleaning to be done
$db=db_close();
?>