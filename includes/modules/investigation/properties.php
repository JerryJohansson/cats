<?php
/*
modified by Craig Wood SRA IT, 06/02/2015 
Added Golden Rule array in the new/edit page
Change 2.2 SRA functional Requirements Doc 1.0 Jan 2015

*/
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Investigation',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Investigation',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Investigation',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLINVESTIGATION_DETAILS',
					'id'=>'INVESTIGATION_NUMBER',
					'view'=>'VIEW_INVESTIGATION_PRINTVIEW',
					'CHECKBOXES'=> array('table'=>'TBLINVESTIG_CHECKBOXVALUES','id'=>'INVESTIGATIONID', 'fields'=>'INVESTIGATIONID,CHECKBOXID,CHECKBOXTYPE'),
						'DOCUMENTLOCATION'=>array(
						'table'=>'TBLMULTIDOCUMENTS',
						'id'=>'REPORT_ID'
						)
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLINVESTIGATION_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'TBLINCIDENT_DETAILS',
					'filter'=>' 1=2 ',
					'CHECKBOXES'=> array('table'=>'TBLINVESTIG_CHECKBOXVALUES','id'=>'INVESTIGATIONID', 'fields'=>'INVESTIGATIONID,CHECKBOXID,CHECKBOXTYPE'),
						'DOCUMENTLOCATION'=>array(
						'table'=>'TBLMULTIDOCUMENTS',
						'id'=>'REPORT_ID'
						)
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INVESTIGATION_PRINTVIEW',
					'filter'=>' 1=2 ',
					'CHECKBOXES'=> array('table'=>'TBLINVESTIG_CHECKBOXVALUES','id'=>'INVESTIGATIONID', 'fields'=>'INVESTIGATIONID,CHECKBOXID,CHECKBOXTYPE'),
						'DOCUMENTLOCATION'=>array(
						'table'=>'TBLMULTIDOCUMENTS',
						'id'=>'REPORT_ID'
						)
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INCIDENT_SEARCHVIEW',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page = 'search'){
	global $id, $p;

	$ret = array();
	switch($page){
		case 'edit': case 'new':
			$ret = array_merge($ret,
				array(
				
					'NOTE' => array('group'=>'row_title', 'label'=>'NOTE: The purpose of the ICAM investigation is NOT to fix blame but to obtain the relevant facts and prevent a recurrence. <button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal">ICAM Info</button>', 'func'=>'html_display_value_icam', 'col'=>2, 'col_span'=>2),
					'NOTE2' => array('group'=>'row_title', 'label'=>'* Denotes this is a Mandatory field.', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_report_details' => array('label'=>'Report Details', 'group'=>'row_header'),
					'INCIDENT_NUMBER' => array('label'=>'Incident Number', 'value'=>'1', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>50),
					'INCIDENT_TYPE' => array('label'=>'Incident Type', 'value'=>'1', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>50),
					'INCDISPLYID' => array('label'=>'* Incident Report No.', 'value'=>'', 'func'=>'html_draw_disabled_field', 'col'=>1, 'col_span'=>1, 'maxlength'=>50),
					//'INCIDENTSTATUS' => array('label'=>'Incident Status', 'value'=>'', 'func'=>'html_get_incident_status_radioset', 'col'=>2, 'col_span'=>1),
					//'SITE_ID' => array('label'=>'* Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					//'DEPARTMENT_ID' => array('label'=>'* Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					//'DEPARTMENT_ID' => array('label'=>'* Department', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					//'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd_linked', 'params2'=>'"SHIFT_ID"', 'col'=>1, 'col_span'=>1),
					//'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_filtered_section_dd_linked', 'params2'=>'"SHIFT_ID"', 'col'=>1, 'col_span'=>1),
					//'SHIFT_ID' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_form_draw_shift_dd_linked', 'col'=>2, 'col_span'=>1),
					//'SHIFT_ID' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_form_draw_filtered_shift_dd', 'col'=>2, 'col_span'=>1),
					//'INITIATED_BY_ID' => array('label'=>'* Report Initiated By', 'value'=>NULL, 'func'=>'html_form_draw_employee_dd', 'col'=>1, 'col_span'=>1),
					//'REPORTEDCOMPANYID' => array('label'=>'* Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>2, 'col_span'=>1),
					//'INCIDENT_DATE' => array('label'=>'Incident Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field', 'col'=>1, 'col_span'=>1),
					//'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'REPORT_DATE' => array('label'=>'Report Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field', 'col'=>2, 'col_span'=>1),
					//'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'REPORTED_TO' => array('label'=>'* Reported To', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					//'SUPERINTENDENT_ID' => array('label'=>'* Responsible Superintendent', 'value'=>NULL, 'func'=>'html_form_draw_employee_dd', 'col'=>2, 'col_span'=>1),
					'LOCATION' => array('label'=>'* Location', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					//'AREA' => array('label'=>'* Area', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					//'EQUIPMENT_NO' => array('label'=>'Equipment', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					//'GOLDENRULEARRAY' => array('label'=>'Golden Rule', 'value'=>NULL, 'func'=>'html_form_draw_golden_rule_dd', 'col'=>1, 'col_span'=>1),
					//'GOLDENRULE' => array('label'=>'Golden Rule', 'value'=>NULL, 'func'=>'html_form_draw_golden_rule_dd', 'col'=>1, 'col_span'=>1),
					//'GOLDENRULEOTHER' => array('label'=>'Golden Rule Other', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1),
					//'row_incident_type' => array('label'=>'Incident Type', 'group'=>'row_header'),
					//'Incident_Category' => array('label'=>'Incident Category', 'value'=>'', 'func'=>'html_draw_incident_category_checkboxset', 'col'=>1, 'col_span'=>2),
					//'Report_Type' => array('label'=>'Type of Report', 'value'=>'', 'func'=>'html_draw_incident_report_type_checkboxset', 'col'=>1, 'col_span'=>2),
					//'row_person_involved' => array('label'=>'Persons Involved', 'group'=>'row_header'),
					//'PRIMARYPERSONID' => array('label'=>'* Primary Person Involved', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					//'PRIMARYCOMPANYID' => array('label'=>'* Primary Person Involved Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>2, 'col_span'=>1),
					//'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Other Persons Involved & Company Name', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					//'INJURED_PERSON_ID' => array('label'=>'Injured Persons Name', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					//'INJUREDCOMPANYID' => array('label'=>'Injured Persons Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>2, 'col_span'=>1),
					//'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_gender_radioset', 'col'=>1, 'col_span'=>2),
					//'row_incident_details' => array('label'=>'Incident Details', 'group'=>'row_header'),
					'INCIDENT_TITLE' => array('label'=>'* Incident Title', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					//'INCIDENT_DESCRIPTION' => array('label'=>'* Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					//'INCIDENT_DESCRIPTION2' => array('label'=>'* Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),// testcraigwood----------------------------------------------
					//'PHOTO_OF_INCIDENT' => array('label'=>'* Photo of Incident', 'value'=>'', 'func'=>'html_imagefile_input_field', 'col'=>1, 'col_span'=>2), // testcraigwood----------------------------------------------------
					//'PHOTO_OF_INCIDENT' => array('label'=>'* Photo of Incident', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),// testcraigwood----------------------------------------------------
					//'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					//'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					
					
					'row_timeline_events' => array('label'=>'Timeline Events', 'group'=>'row_header'),
					'timeline_last_row' => array('label'=>'Timeline Last Row', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>50),
					'rows_timeline_events' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_timeline_events', 'col'=>1, 'col_span'=>2),				
					'TLROW1' => array('label'=>'TLROW1', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW2' => array('label'=>'TLROW2', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW3' => array('label'=>'TLROW3', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW4' => array('label'=>'TLROW4', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW5' => array('label'=>'TLROW5', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW6' => array('label'=>'TLROW6', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW7' => array('label'=>'TLROW7', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW8' => array('label'=>'TLROW8', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW9' => array('label'=>'TLROW9', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW10' => array('label'=>'TLROW10', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW11' => array('label'=>'TLROW11', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW12' => array('label'=>'TLROW12', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW13' => array('label'=>'TLROW13', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW14' => array('label'=>'TLROW14', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW15' => array('label'=>'TLROW15', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW16' => array('label'=>'TLROW16', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW17' => array('label'=>'TLROW17', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW18' => array('label'=>'TLROW18', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW19' => array('label'=>'TLROW19', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW20' => array('label'=>'TLROW20', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW21' => array('label'=>'TLROW21', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW22' => array('label'=>'TLROW22', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW23' => array('label'=>'TLROW23', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW24' => array('label'=>'TLROW24', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW25' => array('label'=>'TLROW25', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW26' => array('label'=>'TLROW26', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW27' => array('label'=>'TLROW27', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW28' => array('label'=>'TLROW28', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW29' => array('label'=>'TLROW29', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLROW30' => array('label'=>'TLROW30', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),

					'TLDATE1' => array('label'=>'TLDATE1', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE2' => array('label'=>'TLDATE2', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE3' => array('label'=>'TLDATE3', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE4' => array('label'=>'TLDATE4', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE5' => array('label'=>'TLDATE5', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE6' => array('label'=>'TLDATE6', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE7' => array('label'=>'TLDATE7', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE8' => array('label'=>'TLDATE8', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE9' => array('label'=>'TLDATE9', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE10' => array('label'=>'TLDATE10', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE11' => array('label'=>'TLDATE11', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE12' => array('label'=>'TLDATE12', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE13' => array('label'=>'TLDATE13', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE14' => array('label'=>'TLDATE14', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE15' => array('label'=>'TLDATE15', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE16' => array('label'=>'TLDATE16', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE17' => array('label'=>'TLDATE17', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE18' => array('label'=>'TLDATE18', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE19' => array('label'=>'TLDATE19', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE20' => array('label'=>'TLDATE20', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE21' => array('label'=>'TLDATE21', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE22' => array('label'=>'TLDATE22', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE23' => array('label'=>'TLDATE23', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE24' => array('label'=>'TLDATE24', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE25' => array('label'=>'TLDATE25', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE26' => array('label'=>'TLDATE26', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE27' => array('label'=>'TLDATE27', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE28' => array('label'=>'TLDATE28', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE29' => array('label'=>'TLDATE29', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDATE30' => array('label'=>'TLDATE30', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					
					'TLTIME1' => array('label'=>'TLTIME1', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME2' => array('label'=>'TLTIME2', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME3' => array('label'=>'TLTIME3', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME4' => array('label'=>'TLTIME4', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME5' => array('label'=>'TLTIME5', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME6' => array('label'=>'TLTIME6', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME7' => array('label'=>'TLTIME7', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME8' => array('label'=>'TLTIME8', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME9' => array('label'=>'TLTIME9', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME10' => array('label'=>'TLTIME10', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME11' => array('label'=>'TLTIME11', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME12' => array('label'=>'TLTIME12', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME13' => array('label'=>'TLTIME13', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME14' => array('label'=>'TLTIME14', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME15' => array('label'=>'TLTIME15', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME16' => array('label'=>'TLTIME16', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME17' => array('label'=>'TLTIME17', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME18' => array('label'=>'TLTIME18', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME19' => array('label'=>'TLTIME19', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME20' => array('label'=>'TLTIME20', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME21' => array('label'=>'TLTIME21', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME22' => array('label'=>'TLTIME22', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME23' => array('label'=>'TLTIME23', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME24' => array('label'=>'TLTIME24', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME25' => array('label'=>'TLTIME25', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME26' => array('label'=>'TLTIME26', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME27' => array('label'=>'TLTIME27', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME28' => array('label'=>'TLTIME28', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME29' => array('label'=>'TLTIME29', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLTIME30' => array('label'=>'TLTIME30', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					
					'TLDESCRIPTION1' => array('label'=>'TLDESCRIPTION1', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION2' => array('label'=>'TLDESCRIPTION2', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION3' => array('label'=>'TLDESCRIPTION3', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION4' => array('label'=>'TLDESCRIPTION4', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION5' => array('label'=>'TLDESCRIPTION5', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION6' => array('label'=>'TLDESCRIPTION6', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION7' => array('label'=>'TLDESCRIPTION7', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION8' => array('label'=>'TLDESCRIPTION8', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION9' => array('label'=>'TLDESCRIPTION9', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION10' => array('label'=>'TLDESCRIPTION10', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION11' => array('label'=>'TLDESCRIPTION11', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION12' => array('label'=>'TLDESCRIPTION12', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION13' => array('label'=>'TLDESCRIPTION13', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION14' => array('label'=>'TLDESCRIPTION14', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION15' => array('label'=>'TLDESCRIPTION15', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION16' => array('label'=>'TLDESCRIPTION16', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION17' => array('label'=>'TLDESCRIPTION17', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION18' => array('label'=>'TLDESCRIPTION18', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION19' => array('label'=>'TLDESCRIPTION19', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION20' => array('label'=>'TLDESCRIPTION20', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION21' => array('label'=>'TLDESCRIPTION21', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION22' => array('label'=>'TLDESCRIPTION22', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION23' => array('label'=>'TLDESCRIPTION23', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION24' => array('label'=>'TLDESCRIPTION24', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION25' => array('label'=>'TLDESCRIPTION25', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION26' => array('label'=>'TLDESCRIPTION26', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION27' => array('label'=>'TLDESCRIPTION27', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION28' => array('label'=>'TLDESCRIPTION28', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION29' => array('label'=>'TLDESCRIPTION29', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),
					'TLDESCRIPTION30' => array('label'=>'TLDESCRIPTION30', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>100),					
					
							
					
					
					'row_losses_associated' => array('label'=>'Losses Associated with the Incident', 'group'=>'row_header'),
						'LOSSES_PEOPLE' => array('label'=>'Person Impact', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>1),
						'LOSSES_EQUIP' => array('label'=>'Equipment Impact', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>1),
						'LOSSES_EQUIP_DOLLAR' => array('label'=>'Equipment Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
						'LOSSES_ENVIRONMENTAL' => array('label'=>'Environmental Impact', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>1),
						'LOSSES_ENVIRONMENTAL_DOLLAR' => array('label'=>'Environmental Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
						'LOSSES_PROCESS' => array('label'=>'Process Impact', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>1),
						'LOSSES_PROCESS_DOLLAR' => array('label'=>'Process Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
						'LOSSES_PROCESS_DESCRIPTION' => array('label'=>'Process Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),

					'row_involved_person_detail' => array('label'=>'Involved Persons Details', 'group'=>'row_header'),
					'INJURED_PERSON' => array('label'=>'Primary Injured Persons Name', 'value'=>NULL, 'func'=>'html_draw_disabled_field', 'col'=>1, 'col_span'=>1),
					'PRIMARY_PERSON_CONTRACTOR_NAME' => array('label'=>'Primary Injured Persons Name (Visitor/Contractor)', 'value'=>NULL, 'func'=>'html_draw_disabled_field', 'col'=>1, 'col_span'=>1),
					
					'WITNESS_1' => array('label'=>'Secondary Involved/Injured/Witness Persons Name', 'value'=>NULL, 'func'=>'html_draw_disabled_field', 'col'=>1, 'col_span'=>2),
					'WITNESS_2' => array('label'=>'Third Involved/Injured/Witness Persons Name', 'value'=>NULL, 'func'=>'html_draw_disabled_field', 'col'=>1, 'col_span'=>2),

                     'row_injury_details' => array('label'=>'Injury Details', 'group'=>'row_header'),//--------------------------------Craig Wood test------------------------
					 'TREATMENT_OUTCOME' => array('label'=>'Treatment Outcome', 'value'=>NULL, 'func'=>'html_draw_investigation_TREATMENT_OUTCOME_checkboxset', 'col'=>1, 'col_span'=>1),//-------ERROR HERE??-------------------------Craig Wood test------------------------
					 
					 


					 
					'row_consequence_ranking' => array('label'=>'Consequence Ranking', 'group'=>'row_header'),
						'RISK_CONSEQUENCE_ACTUAL' => array('label'=>'Actual Incident Consequence', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_consequence_actual' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_consequence_actual_modal', 'col'=>1, 'col_span'=>2),
						'RISK_LIKELIHOOD_ACTUAL' => array('label'=>'Actual Incident Likelihood', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_likelihood_actual' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_likelihood_actual_modal', 'col'=>1, 'col_span'=>2),
						'RISK_RATING_ACTUAL' => array('label'=>'ACTUAL RISK', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'row_RISK_RATING_ACTUAL_empty' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_line_break', 'col'=>1, 'col_span'=>1),
						
						'RISK_CONSEQUENCE_POTENTIAL' => array('label'=>'Risk Consequence', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_consequence_potential' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_consequence_potential_modal', 'col'=>1, 'col_span'=>2),
						'RISK_LIKELIHOOD_POTENTIAL' => array('label'=>'Potential Likelihood', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_likelihood_potential' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_likelihood_potential_modal', 'col'=>1, 'col_span'=>2),
						'RISK_RATING_POTENTIAL' => array('label'=>'RISK CONSEQUENCE', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'row_RISK_RATING_POTENTIAL_empty' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_line_break', 'col'=>1, 'col_span'=>1),
					

					'row_absent_or_failed_defences' => array('label'=>'Absent or failed Defences', 'group'=>'row_header'),
						'DF_1' => array('label'=>'DF1 - Awareness - hazard identification?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF1_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_11' => array('label'=>'DF11 - Control and Recovery - procedures?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF11_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_2' => array('label'=>'DF2 - Awareness - communication?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF2_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_12' => array('label'=>'DF12 - Control and Recovery - bypass values / circuits?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF12_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_3' => array('label'=>'DF3 - Awareness - competence / knowledge?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF3_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_13' => array('label'=>'DF13 - Control and Recovery - emergency shut down?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF13_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_4' => array('label'=>'DF4 - Awareness - supervision?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF4_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_14' => array('label'=>'DF14 - Protection and Containment - PPE?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF14_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_5' => array('label'=>'DF5 - Awareness - work instruction / procedures?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF5_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_15' => array('label'=>'DF15 - Protection and Containment - fire fightning?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF15_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_6' => array('label'=>'DF6 - Detection - visual warning systems?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF6_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_16' => array('label'=>'DF16 - Protection and Containment - spill response?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF16_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_7' => array('label'=>'DF7 - Detection - aural warning systems?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF7_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_17' => array('label'=>'DF17 - Protection and Containment - bunding / barricading / exclusion zones?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF17_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'DF_8' => array('label'=>'DF8 - Detection - speed / movement detectors?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF8_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_18' => array('label'=>'DF18 - Escape and Rescue - safe access / egress?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF18_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_9' => array('label'=>'DF9 - Detection - vigilance / fatigue?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF9_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_19' => array('label'=>'DF19 - Escape and Rescue - emergency planning / response?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF19_checkboxset', 'col'=>2, 'col_span'=>1),

						'DF_10' => array('label'=>'DF10 - Detection - gas / substance?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF10_checkboxset', 'col'=>1, 'col_span'=>1),
						'DF_20' => array('label'=>'DF20 - Escape and Rescue - emergency communication?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF20_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'DF_21' => array('label'=>'DF21 - Other?', 'value'=>NULL, 'func'=>'html_draw_investigation_ABS_OR_FAILED_DF21_checkboxset', 'col'=>1, 'col_span'=>1),
						
						
					    'rows_codes_absent_or_failed_defences' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_codes_df', 'col'=>1, 'col_span'=>2),//--------------------------//---------------------//----------------------- Craig Wood SRA test	
							'DF_CODEROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODEROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODEROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODEROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

							'DF_CODEROW5' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW5' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODEROW6' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW6' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODEROW7' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW7' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODEROW8' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'DF_CODE_FACTOR_ROW8' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							
					
					
					
					
					
					'row_individual_team_actions' => array('label'=>'Individual/Team Actions', 'group'=>'row_header'),
						'IT_1' => array('label'=>'IT1 - Supervisory error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT1_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_8' => array('label'=>'IT8 - Equipment / materials handeling error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT8_checkboxset', 'col'=>2, 'col_span'=>1),

						'IT_2' => array('label'=>'IT2 - Operating authority error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT2_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_9' => array('label'=>'IT9 - Horseplay / thrill seeking error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT9_checkboxset', 'col'=>2, 'col_span'=>1),

						'IT_3' => array('label'=>'IT3 - Operating speed?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT3_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_10' => array('label'=>'IT10 - Hazard recognition / perception?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT10_checkboxset', 'col'=>2, 'col_span'=>1),

						'IT_4' => array('label'=>'IT4 - Equipment use error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT4_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_11' => array('label'=>'IT11 - Hazard management error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT11_checkboxset', 'col'=>2, 'col_span'=>1),

						'IT_5' => array('label'=>'IT5 - PPE use error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT5_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_12' => array('label'=>'IT12 - Work method error or violation?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT12_checkboxset', 'col'=>2, 'col_span'=>1),

						'IT_6' => array('label'=>'IT6 - Procedural compliance?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT6_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_13' => array('label'=>'IT13 - Occupational hygiene practices?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT13_checkboxset', 'col'=>2, 'col_span'=>1),

						'IT_7' => array('label'=>'IT7 - Change management error?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT7_checkboxset', 'col'=>1, 'col_span'=>1),
						'IT_14' => array('label'=>'IT14 - Other?', 'value'=>NULL, 'func'=>'html_draw_investigation_IT14_checkboxset', 'col'=>2, 'col_span'=>1),
						
					    'rows_codes_individual_team' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_codes_it', 'col'=>1, 'col_span'=>2),//--------------------------//---------------------//----------------------- Craig Wood SRA test	
							'IT_CODEROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODE_FACTOR_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODEROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODE_FACTOR_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODEROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODE_FACTOR_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODEROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'IT_CODE_FACTOR_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
					
					
					
					
					'row_task_environmental_conditions_wp' => array('label'=>'Task/Environmental Conditions - Workplace', 'group'=>'row_header'),
						'WF_1' => array('label'=>'WF1 - Task Planning / Preparation / manning', 'value'=>NULL, 'func'=>'html_draw_investigation_WF1_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_13' => array('label'=>'WF13 - Fire and / or explosion hazard', 'value'=>NULL, 'func'=>'html_draw_investigation_WF13_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'WF_2' => array('label'=>'WF2 - Hazard Analysis / Job Safety Analysis / Take 5', 'value'=>NULL, 'func'=>'html_draw_investigation_WF2_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_14' => array('label'=>'WF14 - Lightning', 'value'=>NULL, 'func'=>'html_draw_investigation_WF14_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_3' => array('label'=>'WF3 - Work Procedures availability and suitability', 'value'=>NULL, 'func'=>'html_draw_investigation_WF3_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_15' => array('label'=>'WF15 - Equipment / material temperature / conditions', 'value'=>NULL, 'func'=>'html_draw_investigation_WF15_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_4' => array('label'=>'WF4 - Permit to work availability and suitability', 'value'=>NULL, 'func'=>'html_draw_investigation_WF4_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_16' => array('label'=>'WF16 - Noise', 'value'=>NULL, 'func'=>'html_draw_investigation_WF16_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_5' => array('label'=>'WF5 - Abnormal operational situation / condition', 'value'=>NULL, 'func'=>'html_draw_investigation_WF5_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_17' => array('label'=>'WF17 - Ventilation', 'value'=>NULL, 'func'=>'html_draw_investigation_WF17_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_6' => array('label'=>'WF6 - Tools / equipment condition / availability', 'value'=>NULL, 'func'=>'html_draw_investigation_WF6_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_18' => array('label'=>'WF18 - Gas, dust or fumes', 'value'=>NULL, 'func'=>'html_draw_investigation_WF18_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_7' => array('label'=>'WF7 - Materials availability and suitability', 'value'=>NULL, 'func'=>'html_draw_investigation_WF7_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_19' => array('label'=>'WF19 - Radiation', 'value'=>NULL, 'func'=>'html_draw_investigation_WF19_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_8' => array('label'=>'WF8 - Equipment integrity', 'value'=>NULL, 'func'=>'html_draw_investigation_WF8_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_20' => array('label'=>'WF20 - Chemical', 'value'=>NULL, 'func'=>'html_draw_investigation_WF20_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'WF_9' => array('label'=>'WF9 - Housekeeping', 'value'=>NULL, 'func'=>'html_draw_investigation_WF9_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_21' => array('label'=>'WF21 - Wildlife', 'value'=>NULL, 'func'=>'html_draw_investigation_WF21_checkboxset', 'col'=>2, 'col_span'=>1),

						'WF_10' => array('label'=>'WF10 - Weather conditions', 'value'=>NULL, 'func'=>'html_draw_investigation_WF10_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_22' => array('label'=>'WF22 - Surface Gradient/Conditions', 'value'=>NULL, 'func'=>'html_draw_investigation_WF22_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'WF_11' => array('label'=>'WF11 - Congestion / restriction / access', 'value'=>NULL, 'func'=>'html_draw_investigation_WF11_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_23' => array('label'=>'WF23 - Reduced/restricted visibility', 'value'=>NULL, 'func'=>'html_draw_investigation_WF23_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'WF_12' => array('label'=>'WF12 - Routine / non-routine task', 'value'=>NULL, 'func'=>'html_draw_investigation_WF12_checkboxset', 'col'=>1, 'col_span'=>1),
						'WF_24' => array('label'=>'WF24 - Other Factor', 'value'=>NULL, 'func'=>'html_draw_investigation_WF24_checkboxset', 'col'=>2, 'col_span'=>1),
						
						
					    'rows_codes_workplace_factors' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_codes_wf', 'col'=>1, 'col_span'=>2),//--------------------------//---------------------//----------------------- Craig Wood SRA test	
							'WF_CODEROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODE_FACTOR_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODEROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODE_FACTOR_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODEROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODE_FACTOR_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODEROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'WF_CODE_FACTOR_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
					
					
					'row_task_environmental_conditions_hf' => array('label'=>'Task/Environmental Conditions - Human Factor', 'group'=>'row_header'),
						'HF_1' => array('label'=>'HF1 - Complacency / motivation / descensitisation to hazard', 'value'=>NULL, 'func'=>'html_draw_investigation_HF1_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_14' => array('label'=>'HF14 -  Personal issues', 'value'=>NULL, 'func'=>'html_draw_investigation_HF14_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'HF_2' => array('label'=>'HF2 - Drugs/Alcohol influence', 'value'=>NULL, 'func'=>'html_draw_investigation_HF2_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_15' => array('label'=>'HF15 - Distraction / pre-occupation', 'value'=>NULL, 'func'=>'html_draw_investigation_HF15_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_3' => array('label'=>'HF3 - Familitarity with task', 'value'=>NULL, 'func'=>'html_draw_investigation_HF3_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_16' => array('label'=>'HF16 - Experience / Knowledge / Skills for task', 'value'=>NULL, 'func'=>'html_draw_investigation_HF16_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_4' => array('label'=>'HF4 - Fatigue', 'value'=>NULL, 'func'=>'html_draw_investigation_HF4_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_17' => array('label'=>'HF17 - Competency', 'value'=>NULL, 'func'=>'html_draw_investigation_HF17_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_5' => array('label'=>'HF5 - Situational awareness', 'value'=>NULL, 'func'=>'html_draw_investigation_HF5_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_18' => array('label'=>'HF18 - Behavioural beliefs (gains > risks) ', 'value'=>NULL, 'func'=>'html_draw_investigation_HF18_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_6' => array('label'=>'HF6 - Time / productivity pressures', 'value'=>NULL, 'func'=>'html_draw_investigation_HF6_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_19' => array('label'=>'HF19 - Personality / attitude', 'value'=>NULL, 'func'=>'html_draw_investigation_HF19_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_7' => array('label'=>'HF7 - Peer pressure / supervisory example', 'value'=>NULL, 'func'=>'html_draw_investigation_HF7_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_20' => array('label'=>'HF20 - Poor communications', 'value'=>NULL, 'func'=>'html_draw_investigation_HF20_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'HF_8' => array('label'=>'HF8 - Physical stress', 'value'=>NULL, 'func'=>'html_draw_investigation_HF8_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_21' => array('label'=>'HF21 - Poor shift patterns & overtime working', 'value'=>NULL, 'func'=>'html_draw_investigation_HF21_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_9' => array('label'=>'HF9 - Mental capabilities', 'value'=>NULL, 'func'=>'html_draw_investigation_HF9_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_22' => array('label'=>'HF22 - Passive tolerance of violations', 'value'=>NULL, 'func'=>'html_draw_investigation_HF22_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_10' => array('label'=>'HF10 - Physical stress', 'value'=>NULL, 'func'=>'html_draw_investigation_HF10_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_23' => array('label'=>'HF23 - Perceived licence to bend rules', 'value'=>NULL, 'func'=>'html_draw_investigation_HF23_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_11' => array('label'=>'HF11 - Mental stress', 'value'=>NULL, 'func'=>'html_draw_investigation_HF11_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_24' => array('label'=>'HF24 - Change of routine', 'value'=>NULL, 'func'=>'html_draw_investigation_HF24_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_12' => array('label'=>'HF12 - Confidence level', 'value'=>NULL, 'func'=>'html_draw_investigation_HF12_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_25' => array('label'=>'HF25 - Reliance on undocumented knowledge', 'value'=>NULL, 'func'=>'html_draw_investigation_HF25_checkboxset', 'col'=>2, 'col_span'=>1),

						'HF_13' => array('label'=>'HF13 - Secondary goals', 'value'=>NULL, 'func'=>'html_draw_investigation_HF13_checkboxset', 'col'=>1, 'col_span'=>1),
						'HF_26' => array('label'=>'HF26 - Other human factor', 'value'=>NULL, 'func'=>'html_draw_investigation_HF26_checkboxset', 'col'=>2, 'col_span'=>1),

						
						
						
					    'rows_codes_human_factors' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_codes_hf', 'col'=>1, 'col_span'=>2),//--------------------------//---------------------//----------------------- Craig Wood SRA test	
							'HF_CODEROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODE_FACTOR_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODEROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODE_FACTOR_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODEROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODE_FACTOR_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODEROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'HF_CODE_FACTOR_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
					
					
					
					'row_organisational_factors' => array('label'=>'Organisational Factor', 'group'=>'row_header'),
						'HW' => array('label'=>'HW - Hardware', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_HW_checkboxset', 'col'=>1, 'col_span'=>1),
						'MM' => array('label'=>'MM - Maintenance Management', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_MM_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'TR' => array('label'=>'TR - Training', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_TR_checkboxset', 'col'=>1, 'col_span'=>1),
						'DE' => array('label'=>'DE - Design', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_DE_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'OR' => array('label'=>'OR - Organisation', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_OR_checkboxset', 'col'=>1, 'col_span'=>1),
						'RM' => array('label'=>'RM - Risk Management', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_RM_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'CO' => array('label'=>'CO - Communication', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_CO_checkboxset', 'col'=>1, 'col_span'=>1),
						'MC' => array('label'=>'MC - Management of Change', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_MC_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'IG' => array('label'=>'IG - Incompatible Goals', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_IG_checkboxset', 'col'=>1, 'col_span'=>1),
						'CM' => array('label'=>'CM - Contractor Management', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_CM_checkboxset', 'col'=>2, 'col_span'=>1),
					
						'OC' => array('label'=>'OC - Organisational Culture', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_OC_checkboxset', 'col'=>1, 'col_span'=>1),
						'RI' => array('label'=>'RI - Regulatory Influense', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_RI_checkboxset', 'col'=>2, 'col_span'=>1),
						
						'OL' => array('label'=>'OL - Organisational Learning', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_OL_checkboxset', 'col'=>1, 'col_span'=>1),
						'VM' => array('label'=>'VM - Vehicle Management', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_VM_checkboxset', 'col'=>2, 'col_span'=>1),

						'MS' => array('label'=>'MS - Management Systems', 'value'=>NULL, 'func'=>'html_draw_investigation_OrgFactor_MS_checkboxset', 'col'=>1, 'col_span'=>1),
						
					
					    'rows_codes_organisational_factors' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_codes_of', 'col'=>1, 'col_span'=>2),//--------------------------//---------------------//----------------------- Craig Wood SRA test	
							'OF_CODEROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODE_FACTOR_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODEROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODE_FACTOR_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODEROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODE_FACTOR_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODEROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'OF_CODE_FACTOR_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

						
					
					
					'row_conclusions_and_observations' => array('label'=>'Conclusions and Observations', 'group'=>'row_header'),
					
					   'CONCLUSION_AND_OBSERVATION' => array('label'=>'Conclusion and Observation', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),//------------------//--------- Craig Wood sra test-------
					
					
					'row_recommendations' => array('label'=>'Recommendations ', 'group'=>'row_header'),
					
					   'RECOMMENDATION_1' => array('label'=>'Recommendation 1', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_2' => array('label'=>'Recommendation 2', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),	
					   'RECOMMENDATION_3' => array('label'=>'Recommendation 3', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_4' => array('label'=>'Recommendation 4', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_5' => array('label'=>'Recommendation 5', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_6' => array('label'=>'Recommendation 6', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_7' => array('label'=>'Recommendation 7', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_8' => array('label'=>'Recommendation 8', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_9' => array('label'=>'Recommendation 9', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					   'RECOMMENDATION_10' => array('label'=>'Recommendation 10', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					
					'row_key_learnings' => array('label'=>'Key Learnings', 'group'=>'row_header'),
					   'rows_key_learnings_table' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_key_learnings', 'col'=>1, 'col_span'=>2),
							'KEY_LEARING_TITLE_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_DESC_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_TITLE_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_DESC_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_TITLE_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_DESC_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_TITLE_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'KEY_LEARING_DESC_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
					
					
					
						
						
					
						
					'row_appendixes' => array('label'=>'Appendixes', 'group'=>'row_header'),
					   'rows_appendixes' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_appendixes', 'col'=>1, 'col_span'=>2),
							'APPENDIXES_URL_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DESC_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DATE_ROW1' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

							'APPENDIXES_URL_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DESC_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DATE_ROW2' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

							'APPENDIXES_URL_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DESC_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DATE_ROW3' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

							'APPENDIXES_URL_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DESC_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							'APPENDIXES_DATE_ROW4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
							
					
					
				



					'row_sign_off_icam1' => array('label'=>'1. ICAM Report Reviewed By', 'group'=>'row_header'),
						'SIGNOFF_PERSON_ID_4' => array('label'=>'ICAM Report Reviewed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
						'FORWARD_PERSON_ID_4' => array('label'=>'Forward to', 'value'=>NULL, 'func'=>'html_draw_employee_helper_with_approval_rejection_icam', 'col'=>2, 'col_span'=>1),
						'SIGNOFF_DATE_4' => array('label'=>'Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field_ver2', 'col'=>1, 'col_span'=>1),
						'STATUS_4' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
						'MESSAGE_4' => array('label'=>'Approval/Rejection Message', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
						'DO_EMAIL_FLAG_4' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

					'row_sign_off_icam2' => array('label'=>'2. ICAM Report Reviewed By', 'group'=>'row_header'),
						'SIGNOFF_PERSON_ID_5' => array('label'=>'ICAM Report Reviewed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
						'FORWARD_PERSON_ID_5' => array('label'=>'Forward to', 'value'=>NULL, 'func'=>'html_draw_employee_helper_with_approval_rejection_icam', 'col'=>2, 'col_span'=>1),
						'SIGNOFF_DATE_5' => array('label'=>'Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field_ver2', 'col'=>1, 'col_span'=>1),
						'STATUS_5' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
						'MESSAGE_5' => array('label'=>'Approval/Rejection Message', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
						'DO_EMAIL_FLAG_5' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),

					'row_sign_off_icam3' => array('label'=>'3. ICAM Report Reviewed By', 'group'=>'row_header'),
						'SIGNOFF_PERSON_ID_6' => array('label'=>'ICAM Report Reviewed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
						'FORWARD_PERSON_ID_6' => array('label'=>'Forward to', 'value'=>NULL, 'func'=>'html_draw_employee_helper_with_approval_rejection_icam', 'col'=>2, 'col_span'=>1),
						'SIGNOFF_DATE_6' => array('label'=>'Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field_ver2', 'col'=>1, 'col_span'=>1),
						'STATUS_6' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
						'MESSAGE_6' => array('label'=>'Approval/Rejection Message', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
						'DO_EMAIL_FLAG_6' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1),
					
					
					
					//'INITIALRISKCATEGORY' => array('label'=>'* Risk Category', 'value'=>NULL, 'func'=>'html_form_draw_risk_category_dd', 'col'=>1, 'col_span'=>1),
					//'INITIALRISKLEVEL' => array('label'=>'* Risk Level', 'value'=>NULL, 'func'=>'html_form_draw_risk_level_dd', 'col'=>2, 'col_span'=>1),
					//'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
					//		'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
				   //
				//		),
				//		'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = 'incident_details' AND Report_Id = $id"
				//	)
					//'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),

				)
			);
			if($p=='post'){
				$ret = array_merge($ret,
					array(
						'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),

						'RISK_LIKELIHOOD_ACTUAL' => array('label'=>'Actual Likelihood', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RISK_CONSEQUENCE_ACTUAL' => array('label'=>'Actual Consequence', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RISK_RATING_ACTUAL' => array('label'=>'ACTUAL CONSEQUENCE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RISK_LIKELIHOOD_POTENTIAL' => array('label'=>'Potential Likelihood', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RISK_CONSEQUENCE_POTENTIAL' => array('label'=>'Potential Consequence', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RISK_RATING_POTENTIAL' => array('label'=>'POTENTIAL CONSEQUENCE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						
					)
				);
			}
			break;
		case 'view':
/*
"INCDISPLYID","PRIMARYPERSONID","PRIMARYCOMPANYID","REPORTEDCOMPANYID","INJUREDCOMPANYID","INCIDENTSTATUS",
"AREA","BODY_PART","CAUSE_OF_INJURY","COMPANY","CURRENT_LOCATION","DEPARTMENT_ID","ESTIMATED_COST",
"IMMEDIATE_CORRECTIVE_ACTIONS","INCIDENT_DESCRIPTION","INCIDENT_NUMBER","INCIDENT_REPORT_STATUS_ID",
"INCIDENT_TIME","INCIDENT_TITLE","INCIDENT_TYPE","INITIALRISKCATEGORY","INITIALRISKLEVEL","INITIATED_BY_ID",
"INJURED_PERSON_COMPANY","INJURED_PERSON_GENDER","INJURED_PERSON_ID","INJURY_STATUS",
"INVESTIGATION_COMPLETION_DATE","INVESTIGATION_REQUIRED","LOCATION","MECHANISM_OF_INJURY",
"PERSONS_INVOLVED_DESCRIPTION","RECALCRISKCATEGORY","RECALCRISKLEVEL","RECALC_RISK_RESULT",
"REPORTED_TO","INCIDENT_DATE","REPORT_DATE","REPORT_TIME","SEVERITY_RATING","SITE_ID","SUBSTANTIATED",
"SUPERINTENDENT_ID","INCIDENT_CATEGORIES","INCIDENT_REPORTTYPE","INCIDENT_IMPACTBY","INCIDENT_IMPACTTO",
"INCIDENT_INJURYTYPE","INCIDENT_INJURYMECHANISM","INCIDENT_INJURYAGENCY","INCIDENT_INJURYLOCATION",
"INCIDENT_INJURYCLASS","INCIDENT_ENVIROCLASS","INCIDENT_COMMUNITYCLASS","INCIDENT_QUALITYCLASS",
"INCIDENT_REPORTEDTO","INCIDENT_SYSTEMICCAUSE","INCIDENT_BEHAVIOURCAUSE","INCIDENT_ECONOMIC",
"DEPARTMENT","SITE","INJURED_PERSON","SUPERINTENDENT","INITIATED_BY","TYPE_OF_EMPLOYEE","REPORTEDTO",
"PRIMARYPERSON","RECALC_RISKCAT","RECALC_RISKLEVEL","INIT_RISKCAT","INIT_RISKLEVEL","REPORTEDBYCOMPANY",
"INJUREDCOMPANY","PRIMARYCOMPANY","CREATEDACTIONS","CLOSEDACTIONS"
*/
			$ret = array_merge($ret,
				array(
					'NOTE' => array('group'=>'row_title', 'label'=>'NOTE: The purpose of the incident investigation is NOT to fix blame but to obtain the relevant facts and prevent a recurrence.', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_report_details' => array('label'=>'Report Details', 'group'=>'row_header'),
					'INCDISPLYID' => array('label'=>'Incident Report No.', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1, 'maxlength'=>50),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'value'=>'', 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),

					'SITE' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),

					'DEPARTMENT' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1),

					'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"SHIFT_ID"', 'col'=>1, 'col_span'=>1),

					'SHIFT_DESCRIPTION' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'INITIATED_BY' => array('label'=>'Report Initiated By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'REPORTEDBYCOMPANY' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'REPORTEDTO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'SUPERINTENDENT' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'EQUIPMENT_NO' => array('label'=>'Equipment', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_incident_type' => array('label'=>'Incident Type', 'group'=>'row_header'),
					'Incident_Category' => array('label'=>'Incident Category', 'value'=>'', 'func'=>'html_draw_incident_category_display', 'col'=>1, 'col_span'=>2),
					'Report_Type' => array('label'=>'Type of Report', 'value'=>'', 'func'=>'html_draw_incident_report_type_display', 'col'=>1, 'col_span'=>2),
					'row_person_involved' => array('label'=>'Persons Involved', 'group'=>'row_header'),
					'PRIMARYPERSON' => array('label'=>'Primary Person Involved', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'PRIMARYCOMPANY' => array('label'=>'Primary Person Involved Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Other Persons Involved & Company Name', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON' => array('label'=>'Injured Persons Name', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'INJUREDCOMPANY' => array('label'=>'Injured Persons Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_incident_details' => array('label'=>'Incident Details', 'group'=>'row_header'),
					'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					//'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'row_assessment_risk' => array('label'=>'Assessment of the Risk of Recurrence', 'group'=>'row_header'),
					'INIT_RISKCAT' => array('label'=>'Risk Category', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'INIT_RISKLEVEL' => array('label'=>'Risk Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					
					'row_consequence_ranking' => array('label'=>'Consequence Ranking', 'group'=>'row_header'),
						'RISK_LIKELIHOOD_ACTUAL' => array('label'=>'Actual Likelihood', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_likelihood_actual' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_likelihood_actual_modal', 'col'=>1, 'col_span'=>2),
						'RISK_CONSEQUENCE_ACTUAL' => array('label'=>'Actual Consequence', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_consequence_actual' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_consequence_actual_modal', 'col'=>1, 'col_span'=>2),
						'RISK_RATING_ACTUAL' => array('label'=>'ACTUAL CONSEQUENCE', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'row_RISK_RATING_ACTUAL_empty' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_line_break', 'col'=>1, 'col_span'=>1),

						'RISK_LIKELIHOOD_POTENTIAL' => array('label'=>'Potential Likelihood', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_likelihood_potential' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_likelihood_potential_modal', 'col'=>1, 'col_span'=>2),
						'RISK_CONSEQUENCE_POTENTIAL' => array('label'=>'Potential Consequence', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'rows_risk_consequence_potential' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_risk_consequence_potential_modal', 'col'=>1, 'col_span'=>2),
						'RISK_RATING_POTENTIAL' => array('label'=>'POTENTIAL CONSEQUENCE', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'row_RISK_RATING_POTENTIAL_empty' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_line_break', 'col'=>1, 'col_span'=>1),
					
					
					'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments where FormType = 'incident_details' AND Report_Id = $id",
						'params2'=>"READONLY"
					)
					//'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),

				)
			);
			break;
		case 'new_old':
			$ret = array_merge($ret,
				array(
					'INCDISPLYID' => array('label'=>'IncDisplyId', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2),
					'DEPARTMENT_ID' => array('label'=>'Department Id', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd', 'col'=>1, 'col_span'=>2),
					'INITIATED_BY_ID' => array('label'=>'Initiated By Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
					'REPORTEDCOMPANYID' => array('label'=>'ReportedCompanyId', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2),
					'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SUPERINTENDENT_ID' => array('label'=>'Superintendent Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Persons Involved Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON_ID' => array('label'=>'Injured Person Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
					'INJUREDCOMPANYID' => array('label'=>'InjuredCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALRISKCATEGORY' => array('label'=>'InitialRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALRISKLEVEL' => array('label'=>'InitialRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PRIMARYPERSONID' => array('label'=>'PrimaryPersonId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PRIMARYCOMPANYID' => array('label'=>'PrimaryCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENTSTATUS' => array('label'=>'IncidentStatus', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
				)
			);
			if($p=='post'){
				$ret = array_merge($ret,
					array(
						'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
					)
				);
			}
			break;
		case 'search':
		case 'results':
			if(cats_user_is_incident_privileged()){
				$ret = array(
					'INCDISPLYID' => array('label'=>'Incident No.', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'delim'=>"'", 'value'=>NULL, 'func'=>'html_get_incident_status_dd', 'col'=>2, 'col_span'=>1),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd', 'col'=>2, 'col_span'=>1),
					'SUPERINTENDENT_ID' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'TYPE_OF_EMPLOYEE' => array('label'=>'Type Of Employee', 'value'=>'', 'func'=>'html_form_draw_employee_type_dd', 'col'=>2, 'col_span'=>1),
					'INCIDENT_CATEGORY' => array('label'=>'Incident Category', 'value'=>NULL, 'func'=>'html_form_draw_incident_categories_dd', 'col'=>1, 'col_span'=>1),
					'REPORT_TYPE' => array('label'=>'Report Type', 'value'=>NULL, 'func'=>'html_form_draw_incident_report_types_dd', 'col'=>2, 'col_span'=>1),
					'INJUREDCOMPANYID' => array('label'=>'Injured Persons Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>1, 'col_span'=>1),
					'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>NULL, 'func'=>'html_form_draw_estimated_costs_dd', 'col'=>2, 'col_span'=>1),
					'REALDATE_FROM' => array('label'=>'Incident Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1),
					'REALDATE_TO' => array('label'=>'Incident Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'INITIALRISKCATEGORY' => array('label'=>'Assessment of Risk (Category)', 'value'=>NULL, 'func'=>'html_form_draw_risk_category_dd', 'col'=>1, 'col_span'=>1),
					'INITIALRISKLEVEL' => array('label'=>'Assessment of Risk (Level)', 'value'=>NULL, 'func'=>'html_form_draw_risk_level_dd', 'col'=>2, 'col_span'=>1),
					'RECALCRISKCATEGORY' => array('label'=>'Re-assessment of Risk (Category)', 'value'=>NULL, 'func'=>'html_form_draw_risk_category_dd', 'col'=>1, 'col_span'=>1),
					'RECALCRISKLEVEL' => array('label'=>'Re-assessment of Risk (Level)', 'value'=>NULL, 'func'=>'html_form_draw_risk_level_dd', 'col'=>2, 'col_span'=>1),
					'INITIATED_BY_ID' => array('label'=>'Initiated By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1),
					'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1),
					'PRIMARYPERSONID' => array('label'=>'Primary Person', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Other Persons', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1)
					//'cats::report_fields' => array('label'=>'Chosen Reporting Fields', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
					//new golden rule array
                   
				);
			}else{
				$ret = array(
					'INCDISPLYID' => array('label'=>'Incident No.', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1),
					'SUPERINTENDENT_ID' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd', 'col'=>2, 'col_span'=>1),
					'INCIDENT_CATEGORY' => array('label'=>'Incident Category', 'value'=>NULL, 'func'=>'html_form_draw_incident_categories_dd', 'col'=>1, 'col_span'=>1),
					'REPORT_TYPE' => array('label'=>'Report Type', 'value'=>NULL, 'func'=>'html_form_draw_incident_report_types_dd', 'col'=>2, 'col_span'=>1),
					'REALDATE_FROM' => array('label'=>'Incident Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1),
					'REALDATE_TO' => array('label'=>'Incident Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'delim'=>"'", 'value'=>NULL, 'func'=>'html_get_incident_status_dd', 'col'=>1, 'col_span'=>2)
				);
			}
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'INCDISPLYID',"text" => 'INCDISPLYID');
	$ret[] = array("id" => 'INCIDENT_DATE',"text" => 'INCIDENT_DATE');
	$ret[] = array("id" => 'INCIDENT_CATEGORIES',"text" => 'INCIDENT_CATEGORIES');
	$ret[] = array("id" => 'INCIDENT_TITLE',"text" => 'INCIDENT_TITLE');
	$ret[] = array("id" => 'INCIDENTSTATUS',"text" => 'INCIDENTSTATUS');
	$ret[] = array("id" => 'CREATEDACTIONS',"text" => 'CREATEDACTIONS');
	$ret[] = array("id" => 'CLOSEDACTIONS',"text" => 'CLOSEDACTIONS');
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);

//foreach($FIELDS as $key => $value) echo("$key, ");
?>


