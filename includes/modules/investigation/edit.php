<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH . 'sql/actions.php');
require_once('properties.php');

require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');

/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_edit";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$incidentId = $_REQUEST['incidentId'];
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
$currentTab=(isset($_REQUEST['t']))?$_REQUEST['t']:0;
$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;


// Security Check
if(!cats_user_is_editor()  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New Investigation " . cats_user_is_editor());
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

$sql_fields = "";
$incident_displayid = "";
$incident_location = "";
$incident_title = "";
$risk_likelihood_actual = "";
$risk_consequence_actual = "";
$risk_rating_actual = "";
$injured_person = "";
$primary_person_contractor_name = "";
$witness_1 = "";
$witness_2 = "";

if($id>0){
	/*Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!*/
	$user_site_id = $_SESSION['user_details']['site_id'];
	$user_site_access = $_SESSION['user_details']['site_access'];
	// have to explicitly select fields because oracle doesn't return time unless you ask it to :(
	if($p=='view'){
		$sql_fields = " * ";
	}else{
		//$sql_fields = "INCIDENT_NUMBER, INCDISPLYID, INCIDENTSTATUS, SITE_ID, DEPARTMENT_ID, INITIATED_BY_ID, REPORTEDCOMPANYID, ";
		//$sql_fields .= "TO_CHAR(INCIDENT_DATE,'YYYY-MM-DD HH24:MI') INCIDENT_DATE, TO_CHAR(REPORT_DATE,'YYYY-MM-DD HH24:MI') REPORT_DATE, ";
		//$sql_fields .= "REPORTED_TO, SUPERINTENDENT_ID, LOCATION, AREA, EQUIPMENT_NO, GOLDENRULE, GOLDENRULEOTHER, PRIMARYPERSONID, PRIMARYCOMPANYID, PERSONS_INVOLVED_DESCRIPTION, ";
		//$sql_fields .= "INJURED_PERSON_ID, INJUREDCOMPANYID, INJURED_PERSON_GENDER, INCIDENT_TITLE, INCIDENT_DESCRIPTION, IMMEDIATE_CORRECTIVE_ACTIONS, ";
		//$sql_fields .= "INITIALRISKCATEGORY, INITIALRISKLEVEL, SEVERITY_RATING, SUBSTANTIATED, ESTIMATED_COST, RECALCRISKCATEGORY, RECALCRISKLEVEL, ";
		//$sql_fields .= "SECTION_ID, SHIFT_ID, ";
		//$sql_fields .= "ACTIONS_CREATED(INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS, ACTIONS_CLOSED(INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS, ";
		//$sql_fields .= "INJURIES_CHECKED(INCIDENT_NUMBER) INJURIES, ENVIRONMENT_CHECKED(INCIDENT_NUMBER) ENVIRONMENT, COMMUNITY_CHECKED(INCIDENT_NUMBER) COMMUNITY, QUALITY_CHECKED(INCIDENT_NUMBER) QUALITY, ECONOMIC_CHECKED(INCIDENT_NUMBER) ECONOMIC";
		
		$sql_fields .= "INVESTIGATION_NUMBER, INCIDENT_NUMBER, INCDISPLYID, LOCATION, AREA, INCIDENT_TITLE, INCIDENT_DESCRIPTION, ";
		$sql_fields .= "PRIMARYPERSONID, SECONDARYPERSONID, THIRDPERSONID, ";
		$sql_fields .= "LOSSES_PEOPLE, LOSSES_EQUIP, LOSSES_EQUIP_DOLLAR, LOSSES_ENVIRONMENTAL, LOSSES_ENVIRONMENTAL_DOLLAR, LOSSES_PROCESS, LOSSES_PROCESS_DOLLAR, LOSSES_PROCESS_DESCRIPTION, ";
		$sql_fields .= "RISK_LIKELIHOOD_ACTUAL, RISK_CONSEQUENCE_ACTUAL, RISK_RATING_ACTUAL, RISK_LIKELIHOOD_POTENTIAL, RISK_CONSEQUENCE_POTENTIAL, RISK_RATING_POTENTIAL, INVESTIGATION_COMPLETION_DATE, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_4, FORWARD_PERSON_ID_4, SIGNOFF_DATE_4, MESSAGE_4, STATUS_4, "; 
		$sql_fields .= "SIGNOFF_PERSON_ID_5, FORWARD_PERSON_ID_5, SIGNOFF_DATE_5, MESSAGE_5, STATUS_5, "; 
		$sql_fields .= "SIGNOFF_PERSON_ID_6, FORWARD_PERSON_ID_6, SIGNOFF_DATE_6, MESSAGE_6, STATUS_6, ";
		$sql_fields .= "INJURED_PERSON, PRIMARY_PERSON_CONTRACTOR_NAME";
		

//		$sql_fields .= "DATEACTIONROW1, TIMEACTIONROW1, ACTIONROW1, ";
//		$sql_fields .= "DATEACTIONROW2, TIMEACTIONROW2, ACTIONROW2, ";
//		$sql_fields .= "DATEACTIONROW3, TIMEACTIONROW3, ACTIONROW3, ";
//		$sql_fields .= "DATEACTIONROW4, TIMEACTIONROW4, ACTIONROW4, ";
//		$sql_fields .= "DATEACTIONROW5, TIMEACTIONROW5, ACTIONROW5, ";
//		$sql_fields .= "DATEACTIONROW6, TIMEACTIONROW6, ACTIONROW6, ";
//		$sql_fields .= "DATEACTIONROW7, TIMEACTIONROW7, ACTIONROW7, ";
//		$sql_fields .= "DATEACTIONROW8, TIMEACTIONROW8, ACTIONROW8, ";
//		$sql_fields .= "DATEACTIONROW9, TIMEACTIONROW9, ACTIONROW9, ";
//		$sql_fields .= "DATEACTIONROW10, TIMEACTIONROW10, ACTIONROW10, ";
//		$sql_fields .= "DATEACTIONROW11, TIMEACTIONROW11, ACTIONROW11, ";
//		$sql_fields .= "DATEACTIONROW12, TIMEACTIONROW12, ACTIONROW12, ";

		$sql_fields .= "DF_CODEROW1, DF_CODE_FACTOR_ROW1, DF_CODEROW2, DF_CODE_FACTOR_ROW2, DF_CODEROW3, DF_CODE_FACTOR_ROW3, DF_CODEROW4, DF_CODE_FACTOR_ROW4, ";
		$sql_fields .= "DF_CODEROW5, DF_CODE_FACTOR_ROW5, DF_CODEROW6, DF_CODE_FACTOR_ROW6, DF_CODEROW7, DF_CODE_FACTOR_ROW7, DF_CODEROW8, DF_CODE_FACTOR_ROW8, ";
		$sql_fields .= "IT_CODEROW1, IT_CODE_FACTOR_ROW1, IT_CODEROW2, IT_CODE_FACTOR_ROW2, IT_CODEROW3, IT_CODE_FACTOR_ROW3, IT_CODEROW4, IT_CODE_FACTOR_ROW4, ";
		$sql_fields .= "WF_CODEROW1, WF_CODE_FACTOR_ROW1, WF_CODEROW2, WF_CODE_FACTOR_ROW2, WF_CODEROW3, WF_CODE_FACTOR_ROW3, WF_CODEROW4, WF_CODE_FACTOR_ROW4, ";
		$sql_fields .= "HF_CODEROW1, HF_CODE_FACTOR_ROW1, HF_CODEROW2, HF_CODE_FACTOR_ROW2, HF_CODEROW3, HF_CODE_FACTOR_ROW3, HF_CODEROW4, HF_CODE_FACTOR_ROW4, ";
		$sql_fields .= "OF_CODEROW1, OF_CODE_FACTOR_ROW1, OF_CODEROW2, OF_CODE_FACTOR_ROW2, OF_CODEROW3, OF_CODE_FACTOR_ROW3, OF_CODEROW4, OF_CODE_FACTOR_ROW4, ";

		$sql_fields .= "CONCLUSION_AND_OBSERVATION, RECOMMENDATION_1, RECOMMENDATION_2, RECOMMENDATION_3, ";
		$sql_fields .= "RECOMMENDATION_4, RECOMMENDATION_5, RECOMMENDATION_6, ";
		$sql_fields .= "RECOMMENDATION_7, RECOMMENDATION_8, RECOMMENDATION_9, RECOMMENDATION_10, ";
		$sql_fields .= "KEY_LEARING_TITLE_ROW1, KEY_LEARING_DESC_ROW1, KEY_LEARING_TITLE_ROW2, KEY_LEARING_DESC_ROW2, KEY_LEARING_TITLE_ROW3, KEY_LEARING_DESC_ROW3, KEY_LEARING_TITLE_ROW4, KEY_LEARING_DESC_ROW4, ";
		
		$sql_fields .= "APPENDIXES_URL_ROW1, APPENDIXES_DESC_ROW1, APPENDIXES_DATE_ROW1, ";
		$sql_fields .= "APPENDIXES_URL_ROW2, APPENDIXES_DESC_ROW2, APPENDIXES_DATE_ROW2, ";
		$sql_fields .= "APPENDIXES_URL_ROW3, APPENDIXES_DESC_ROW3, APPENDIXES_DATE_ROW3, ";
		$sql_fields .= "APPENDIXES_URL_ROW4, APPENDIXES_DESC_ROW4, APPENDIXES_DATE_ROW4, ";
		$sql_fields .= "ACTIONS_CREATED(INVESTIGATION_NUMBER, 'Originating_Investigation') CREATEDACTIONS, ACTIONS_CLOSED(INVESTIGATION_NUMBER, 'Originating_Investigation') CLOSEDACTIONS, ";
		$sql_fields .= "ACTIONS_CREATED(INVESTIGATION_NUMBER, 'Originating_Invest_Events') CREATEDEVENTS ";
		

		$sql = "SELECT $sql_fields FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
		//echo 'JERRY SQL: ' . $sql;
		$action_access = true;
		$RS[$id] = db_query($sql);
		$RECORD = db_get_array($RS[$id]);

		
	}
	
	
	
	
$action_access = true; // TODO JERRY SRA FIX THIS LATER
	
	//if($p!='view')
		// see if the user has access to this action by checking if he belongs to any one of the sites from the view
		//$action_access = cats_check_user_site_access($RECORD['SITE_ID']);
			

	if($action_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}else{
		$sql_fields .= "INCIDENT_NUMBER, INCDISPLYID, LOCATION, AREA, INCIDENT_TITLE, INCIDENT_DESCRIPTION, RISK_LIKELIHOOD_ACTUAL, RISK_CONSEQUENCE_ACTUAL, RISK_RATING_ACTUAL, RISK_LIKELIHOOD_POTENTIAL, RISK_CONSEQUENCE_POTENTIAL, RISK_RATING_POTENTIAL, PRIMARYPERSON, PRIMARY_PERSON_CONTRACTOR_NAME, WITNESS_1, WITNESS_2 ";
		$sql = "SELECT $sql_fields FROM VIEW_INCIDENT_PRINTVIEW WHERE INCIDENT_NUMBER = $incidentId";
		//echo 'JERRY SQL: ' . $sql;
		$action_access = true;
		$RS[$id] = db_query($sql);
		$RECORD = db_get_array($RS[$id]);
		
		$incident_displayid = $RECORD['INCDISPLYID'];
		$incident_location = $RECORD['LOCATION'];
		$incident_title = $RECORD['INCIDENT_TITLE'];
		$risk_likelihood_actual = $RECORD['RISK_LIKELIHOOD_ACTUAL'];
		$risk_consequence_actual = $RECORD['RISK_CONSEQUENCE_ACTUAL'];
		$risk_rating_actual = $RECORD['RISK_RATING_ACTUAL'];
		
		$risk_likelihood_potential = $RECORD['RISK_LIKELIHOOD_POTENTIAL'];
		$risk_consequence_potential = $RECORD['RISK_CONSEQUENCE_POTENTIAL'];
		$risk_rating_potential = $RECORD['RISK_RATING_POTENTIAL'];

		$injured_person = $RECORD['PRIMARYPERSON'];
		$primary_person_contractor_name = $RECORD['PRIMARY_PERSON_CONTRACTOR_NAME'];
		$witness_1 = $RECORD['WITNESS_1'];
		$witness_2 = $RECORD['WITNESS_2'];


		
	//email_to_jerry($POST, "DEBUG", '$sql: ' . $sql . ' $id: ' . $id . ' record: ' . $RECORD['INCIDENT_NUMBER']);


}

?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>tabs.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/incidents.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
//var PAGE = "<?php echo $p;?>";
var INCIDENT_ID = <?php echo $id;?>;
top.gui.edit_checkboxes = new Object();
function init(){
	// initialise the tabbing object
	init_tabs(<?php echo $currentTab;?>);
	// resize editing section
	init_resize_editor();
	// set the maxlength for textareas
	set_textarea_maxlength();
	
	SHOW_ANCHOR=false;
<?php
if($p!='view'){
	echo("	tab_toggle(element('tab_all_incidents'),'edit_checkboxes','Reported_To','Systemic_Cause','Behaviour_Cause');\n");
	if($RECORD['INJURIES']>0) echo("	element('tab_injuries').onclick();\n");
	if($RECORD['ENVIRONMENT']>0) echo("	element('tab_environment').onclick();\n");
	if($RECORD['COMMUNITY']>0) echo("	element('tab_community').onclick();\n");
	if($RECORD['QUALITY']>0) echo("	element('tab_quality').onclick();\n");
	if($RECORD['ECONOMIC']>0) echo("	element('tab_economic').onclick();\n");
}
?>
	
	
	// create document.mousedown handlers
	init_document_handlers();
	
	return true;
}
window.onload = init;
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------







</script>
<style type="text/css">
.tab_off, .tab_on {
	float:none;
	font: bold 11px verdana;
	color: #333333;
}
</style>
<style type="text/css">
.textDollarCost {
	margin-top:33px;
}
.signoffBy {
	margin-top:15px;
}
.textVeryHigh{
	background-color:red;
}
.textHigh{
	background-color:orange;
	color: black;
}
.textMedium{
	background-color:yellow;
	color: black;
}
.textLow{
	background-color:green;
}

.imgriskmatrixDiv{
	position: top: 145px; right: 100px; 
}

</style>
<style type="text/css">
table.imagetable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #999999;
	border-collapse: collapse;
}
table.imagetable th {
	background:#b5cfd2 url('cell-blue.jpg');
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #999999;
}
table.imagetable td {
	background:#dcddc0 url('cell-grey.jpg');
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #999999;
}
</style>



</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onSubmit="return CATS_validateForm(this, 'INCIDENT_NUMBER','','R');"<?php
}else{
	?>onSubmit="return CATS_validateForm(this, 'INCIDENT_NUMBER','','R');"<?php
}
?>>

 
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);">ICAM Investigation Details</a>

<?php
	$followUpActionClick = "";
	if($id>0) 
		$followUpActionClick = "return tab_onclick(this)"; 
	else 
		$followUpActionClick = "alert('Please save the investigation before create any Actions')";
		
	$viewEventClick = "";
	if($id>0) 
		$viewEventClick = "return tab_onclick(this)"; 
	else 
		$viewEventClick = "alert('Please save the investigation before create any Timeline Events')";
		
?>
	
<a
	title="View Follow-up Actions"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="<?PHP echo ''.$followUpActionClick?>" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/followup.gif);">Followup Actions</a>
<!--	
<a
	title="View Events"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="<?PHP echo ''.$viewEventClick?>" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/followup.gif);">Timeline Events</a>	
-->	
</fieldset>

<fieldset class="tool_bar">

<?php if($p != 'view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSaveForm();_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php
}else{
?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php if($p != 'view'){ ?>

<?php if($p == 'edit'){ ?>
<a
	title="Print Preview"
	href="javascript:void(window.open('<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/IncidentReportPDF.aspx?IncidentNumber=<?php echo($incidentId); ?>'));" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>
<?php } ?>
<?php
}else{
?>
<a
	title="Print"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>
<?php
}
?>
<?php
if(cats_user_is_administrator() && $p=='edit'){
?>
<!-- <a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a> -->
<?php
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

	<!-- --------------------------------------------------------------------------------->
	<!-- Modal windows - START -->
		
		<!-- ICAM Info Window -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="width: 500px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">What is an Investigation? What do we want from it?</h4>
              </div>
              <div class="modal-body" style="background-color: #dedef8;">



                <!-- Table goes in the document BODY -->
                <table class="imagetable">
                <tr>
	                <th>Step</th><th>Determine</th><th>Process</th><th>Tools</th>
                </tr>
                <tr>
	                <td>1.</td><td>What Happened</td><td>Data Collection</td><td>PEEPO</td>
                </tr>
                <tr>
	                <td>2.</td><td>Why it Happened</td><td>Collected data analysis</td><td>ICAM</td>
                </tr>
                <tr>
	                <td>3.</td><td>What are we going to do about it?</td><td>Develop recommendations</td><td>Hierarchy of control Benefit assessment</td>
                </tr>
                <tr>
	                <td>4.</td><td>What did we learn that we can share?</td><td>Key learnings</td><td>Incident Report Toolbox briefings</td>
                </tr>
                </table>

              </div>
			  <div>
				<a href="javascript:void(window.open('/files/Pocket Investigation Guide.pdf'));">Open iCam Information PDF</a>
			  
			  </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

		<!-- Risk / Consequence Window -->
        <div class="modal fade" id="myModalConsequence2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Potential Consequence, select from the appropriate category</h4>
              </div>
              <div class="modal-body" style="background-color: #dedef8;">



                <!-- Table goes in the document BODY -->
                <table class="imagetable">
                <tr>
	                <th>Select Level</th><th>Safety</th><th>Environment</th><th>Community /<br/> Reputation</th><th>Quality</th><th>Economic</th>
                </tr>
                <tr>
					<td><button id="btn5" type="button" class="btn btn-xs" style="width:100px; background-color:red" onclick="javascript:setConsequence('Catastrophic')" >Catastropic JERRY</button></td>
					<td>Potential fatality from injury or occ illness</td>
					<td>Catastrophic impact / Off-site release impoacting on external partis</td>
					<td>Catastrophic impact on reputation / National Media / Ongoing State Medie / Court Action</td>
					<td>Loss of major customer or market</td>
					<td>>$5 mill</td>
                </tr>
				<tr>
					<td><button id="btn4" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:red" onclick="javascript:setConsequence('Major')" >Major</button></td>
					<td>Occ illness or Injury resulting in permanent disability</td>
					<td>Major impact / Off-site release</td>
					<td>Major impact on reputation / Regional or State Media / Significant stakeholder interest / Fine or Notice issued</td>
					<td>Quality issue affecting multiple customers</td>
					<td>$500K - $5 mill</td>
				</tr>
				<tr>
					<td><button id="btn3" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:orange; color:black" onclick="javascript:setConsequence('Moderate')" >Moderate</button></td>
					<td>Occ illnes or injury resulting in LTI, RWI or MTI</td>
					<td>Moderate impact / On-site release not immediately contained</td>
					<td>Moderate impact on reputation / Local Media / Repeat External complaint / Complaint irate</td>
					<td>Quality excursion affecting multiple lots</td>
					<td>$50K - $500K</td>
				</tr>
				<tr>
					<td><button id="btn2" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:yellow; color:black" onclick="javascript:setConsequence('Minor')" >Minor</button></td>
					<td>Occ illnes or injury resulting in First Aid Treatment</td>
					<td>Minor impact / On-site release immediatly contained</td>
					<td>Minor impact on reputation / Local Issue / External complaint over minor issue</td>
					<td>Quality excursion affecting multiple lots</td>
					<td>$5K - $50K</td>
				</tr>
				<tr>
					<td><button id="btn1" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:green" onclick="javascript:setConsequence('Insignificant')" >Insignificant</button></td>
					<td>No Injury or occupational illness</td>
					<td>No or negligible environmental impact</td>
					<td>Unsubstantiated External Inquiry / No stakeholder interest</td>
					<td>Quality isue within specification</td>
					<td><5K</td>
				</tr>
                </table>

              </div>
              <div class="modal-footer">
                <button type="button" id="BthConsequenceClose2" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
		

		<!-- Likelihood Window -->
        <div class="modal fade" id="myModalLikelihood2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Determine the likelihood of the specific consequence</h4>
              </div>
              <div class="modal-body" style="background-color: #dedef8;">



                <!-- Table goes in the document BODY -->
                <table class="imagetable">
                <tr>
	                <th>Select Likelihood</th><th>Description</th>
                </tr>
                <tr>
					<td><button id="btnA" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:red" onclick="javascript:setLikelihood('Almost Certain')" >Almost Certain</button></td>
					<td>Consequence is expected to occur in most circumstances. <br/> Greater than 1 in 10 or Expected to occur in the next month</td>
                </tr>
				
                <tr>
					<td><button id="btnB" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:red" onclick="javascript:setLikelihood('Likely')" >Likely</button></td>
					<td>Consequence will probably occur in some circumstances.<br/>  <1 in 10 but > 1 in 100 OR Could occur in the next year</td>
                </tr>
                <tr>
					<td><button id="btnC" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:orange; color:black" onclick="javascript:setLikelihood('Moderate')" >Moderate</button></td>
					<td>The consequence might occur at some time.<br/>  <1 in 100 but > 1,000 OR Could occur in the next 5 years.</td>
                </tr>
                <tr>
					<td><button id="btnD" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:yellow; color:black" onclick="javascript:setLikelihood('Unlikely')" >Unlikely</button></td>
					<td>The consequence could occur at some time, but not expected. <br/>  <1 in 1,000 but > 1 in 100,000 OR Could occur in next 20 years.</td>
                </tr>
                <tr>
					<td><button id="btnD" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:green; " onclick="javascript:setLikelihood('Rare')" >Rare</button></td>
					<td>The consequence may occur in exceptional circumstances.<br/>  <1 in 1000,000 OR Could occur in the next 50 years.</td>
                </tr>
                </table>

              </div>
              <div class="modal-footer">
                <button type="button" id="BthLikelihoodClose2" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
		
		
		
		
	<!-- Modal windows - END -->
	<!----------------------------------------------------------------------------------->
		
		

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed = new Editor($m,$p,$a);
if($id>0){
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
}else{
	echo($ed->buildForm($FIELDS));
}
?>

<div style="height:300px">
</div>

<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
?>
<!-- <input type="button" class="btn btn-warning btn-sm active" name="cats::InvestigationReportPDF" value="Create PDF Investigation / ICAM Report" onClick="goToInvestigationReportPDF();" >-->
<input type="button" class="btn btn-default btn-sm active" name="cats::IncidentReport" value="Create Investigation / ICAM Report" onClick="goToInvestigationReport();" >
<input type="button" class="btn btn-default btn-sm active" name="btnAudit" value="Audit" onClick="openAudit();" >

<?php	
}
?>
<?php if($p != 'view'){ ?>
<input type="submit" id="saveButton" class="btn btn-primary btn-sm active" name="cats::Save" value="Save" onclick="preSaveForm()">
<?php if($p == 'edit'){ ?>
<!-- <input type="button" class="button" name="cats::View" value="Print Preview" onClick="_m.view();" > -->
<?php } ?>
<!--input type="button" class="button" name="cats::viewnotes" value="View Case Notes" onclick="top.show_edit_screen('injury_case_notes','index.php?INCIDENT_NUMBER=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&INCDISPLYID=<?php echo($RECORD['INCDISPLYID']); ?>&m=injury_case_notes&p=edit&a=edit&rm=incidents&rp=edit&rt=2');" -->
<?php
}else{
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<!-- <input type="button" class="button" name="cats::Print" value="Print" onClick="printPage();" > -->
<?php
}
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>
<?php
if($p=='edit'||$p=='view'){
?>
<!-- BEGIN:: Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">

<?php
if($p=='edit'){
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php
}
?>

<caption id="tog"><a name="todo" onClick="this.blur();">Follow-up Actions for this Investigation</a></caption>
<tr id="row_ACTIONS_CREATED">
 <td class="label"><label title="">No. of Actions Created</label></td>
	<td valign="middle"><span id="ACTIONS_CREATED" ><?php echo($RECORD['CREATEDACTIONS']);?></span></td>
 <td class="label"><label title="">No. of Actions Closed</label></td>
	<td valign="middle"><span id="ACTIONS_CLOSED" ><?php echo($RECORD['CLOSEDACTIONS']);?></span></td>
</tr>
<tr>
	<td colspan="4">
<?php
// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","","");

// Follow-up Actions
$action_edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
$col_attributes = array(' width="2%" ','','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date',$action_edit_button);
$sql = "SELECT ACTION_ID,Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date, '$action_edit_button' as edit  FROM VIEW_ACTION_DETAILS  WHERE Report_Id = $id  AND Origin_Table='Originating_Investigation'  ORDER BY Action_Title ASC";
//db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
cats_form_pager($sql,$col_headers,$col_attributes,'', '', '', 'ASC', $col_links, false, 1000);


?>
	</td>
</tr>
<?php
if($p=='view'){ // ***************   VIEW   ******************* //
?>
<tr id="row_RECALCRISKCATEGORY">
	<td class="label"><label title="">Re-assessment of the 'Risk of Recurrence' (Risk Category)</label></td>
	<td>
		<?php echo html_display_value('RECALCRISKCATEGORY', $RECORD['RECALC_RISKCAT'], ' title="Re-assessment of Risk (Category)" ');?>
	</td>
<!--	<td class="label"><label title="">Risk Level</label></td>
	<td>-->
		<?php echo html_draw_hidden_field('RECALCRISKLEVEL', $RECORD['RECALC_RISKLEVEL'], ' title="Re-assessment of Risk (Level)" ');?>
<!--	</td>-->
</tr>
<?php
}else{ // ***************   EDIT   ******************* //
?>
<!--
<tr id="row_RECALCRISKCATEGORY">
	<td class="label"><label title="">Re-assessment of the 'Risk of Recurrence' (Risk Category)</label></td>
	<td>
		<?php echo html_form_draw_risk_category_dd('RECALCRISKCATEGORY', $RECORD['RECALCRISKCATEGORY'], ' title="Re-assessment of Risk (Category)" ');?>
	</td>
<!--	<td class="label"><label title="">Risk Level</label></td>
	<td>-->
		<?php echo html_draw_hidden_field('RECALCRISKLEVEL', $RECORD['RECALCRISKLEVEL'], ' title="Re-assessment of Risk (Level)" ');?>
<!--	</td>-->
<!--</tr>-->
<?php
}
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
?>
<input type="button" class="button" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php
}
?>
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Followup Actions -->

<!-- BEGIN:: Lost Days for this Incident -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">


<?php
if($p=='edit'){
?>
<input type="button" class="button" name="cats::new_action" value="New Timeline Event" onClick="doNewEvent()" >
<?php
}
?>
<caption id="tog"><a name="events" onClick="this.blur();">Timeline Events for this Investigation</a></caption>
<tr id="row_EVENTS_CREATED">
 <td class="label"><label title="">No. of Timeline Events Created</label></td>
	<td valign="middle"><span id="EVENTS_CREATED" ><?php echo($RECORD['CREATEDEVENTS']);?></span></td>
</tr>
<tr>
	<td colspan="4">
<?php
// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","","");

// Follow-up Actions
$action_edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
$col_attributes = array(' width="2%" ',' width="100" ',' width="80%" ',' width="3%" ');
$col_headers=array('ID','Timeline Event Date/Time', 'Description', $action_edit_button);
$sql = "SELECT ACTION_ID, EVENT_DATE, Action_Description, '$action_edit_button' as edit  FROM VIEW_ACTION_DETAILS  WHERE Report_Id = $id  AND Origin_Table='Originating_Invest_Events'  ORDER BY EVENT_DATE ASC";
//db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
cats_form_pager($sql,$col_headers,$col_attributes,'', '', '', 'ASC', $col_links, false, 1000);
?>

















	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
	if($RECORD['INJURED_PERSON_ID'] > 0){
?>
<input type="button" class="button" name="cats::new_lostdays" value="New Lost Days" onClick="top.show_edit_screen('lost_days','index.php?INCIDENT_NUMBER=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&INCDISPLYID=<?php echo($RECORD['INCDISPLYID']); ?>&EMP_ID=<?php echo($RECORD['INJURED_PERSON_ID']); ?>&m=lost_days&p=new&a=add&rm=incidents&rp=edit&rid=<?php echo($id); ?>&rt=2');" >
<?php
	}
}
?>
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Lost Days for this Incident -->
<?php
}












if($p=='view'){
	require_once('view.inc.php');
}else{
	require_once('edit.inc.php');
}


?>
</div>







</form>
<!-- php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?> -->

<form id="ApproveRejectForm" method="POST" action="includes/modules/investigation/postApprovalRejection.php" >

	<input type="hidden" id="INCIDENT_NUMBER" name="INCIDENT_NUMBER" value="<?php echo($incidentId); ?>"/>
	<input type="hidden" id="INVESTIGATION_NUMBER" name="INVESTIGATION_NUMBER" value="<?php echo($id); ?>"/>

	<input type="hidden" id="APPROVAL_STATUS" name="APPROVAL_STATUS" value=""/>
	<input type="hidden" id="WFL_STAGE" name="WFL_STAGE" value=""/>
	<input type="hidden" id="SIGN_OFF_BY" name="SIGN_OFF_BY" value=""/>
	<input type="hidden" id="SIGN_OFF_DATE_TIME" name="SIGN_OFF_DATE_TIME" value=""/>
	<input type="hidden" id="FORWARD_TO" name="FORWARD_TO" value=""/>
	<input type="hidden" id="MESSAGE" name="MESSAGE" value=""/>

</form>


<script type='text/javascript'>

	function goToInvestigationReport()
	{
		window.open("<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/InvestigationReport.aspx?InvestigationNumber=<?php echo($id); ?>&IncidentNumber=<?php echo($incidentId); ?>");
	}
	
	function goToInvestigation()
	{
		window.location.href = "<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/InvestigationSearch.aspx?IncidentNumber=<?php echo($incidentId); ?>";
	}

	function goToInvestigationReportPDF()
	{
		window.open("<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/InvestigationReportPDF.aspx?InvestigationNumber=<?php echo($id); ?>&IncidentNumber=<?php echo($incidentId); ?>");
	}
	
	
<?php

	$sql = "select employee_number as ID, nvl(Related_Superintendent, 0) as TEXT from tblemployee_details ";
	$val = html_db_options($sql, 'ID', 'TEXT', true);

	echo('var employeeSuperintendentMappingArray = [');
	foreach($val as $row){
		echo('[' . $row['id'] . ',' . $row['text'] . '],'); 
	}
	echo('[[ 0 ], [ 0]]];');
	
	
	
	$RS2=array();
	$RECORD2=false;	
	
	$sql2 = 'SELECT INVESTIGATION_NUMBER, TL_ROWNO, TL_DATE, TL_TIME, TL_DESCRIPTION FROM TBLINVESTIGATION_TIMELINE WHERE INVESTIGATION_NUMBER = ' . $id;
	//echo ' alert("JERRY SQL: ' . $sql2 . '");';
	
	$result = db_query($sql2);
	
	while ($row = db_fetch_array($result))
	{
		echo ' addTimeLine("' . $row["INVESTIGATION_NUMBER"] . '","' . $row["TL_ROWNO"] . '","' . $row["TL_DATE"] . '","' . $row["TL_TIME"] . '","' . $row["TL_DESCRIPTION"] . '");';
	}
	

?>
	var isTimelineDisplayed = false;

	function addTimeLine(investigationNo, rowNo, date, time, desc)
	{
		if(!isTimelineDisplayed)
		{
			//alert('Do display time line');
			isTimelineDisplayed = true;
			
			$("#TimelineBtn").click();
		}else{
			addNewEvent();
		}
		//alert('addTimeline: ' + investigationNo + ',' + rowNo + ',' + date + ',' + time + ',' + desc);
		setTimelineRow(rowNo, date, time, desc);
	}

	function setTimelineRow(rowNo, date, time, desc)
	{
		$("#_TLROW1" + rowNo).val(rowNo);
		$("#_TLDATE" + rowNo).val(date);
		$("#_TLTIME" + rowNo).val(time);
		$("#_TLDESCRIPTION" + rowNo).val(desc);
		
	}
	
	var forwardPersonId4Onload = $("#FORWARD_PERSON_ID_4").val();
	var forwardPersonId5Onload = $("#FORWARD_PERSON_ID_5").val();
	var forwardPersonId6Onload = $("#FORWARD_PERSON_ID_6").val();
	

	$(document).ready(function() {
	
	//alert("Ready 1");
		var investigationId = <?PHP echo $id; ?>;
	
		$("#INCIDENT_NUMBER").val('<?php echo($incidentId); ?>');	
		var incNo = $("#INCIDENT_NUMBER").val();
		//alert('INCIDENT_NUMBER: ' + incNo);
		if(investigationId == 0)
		{
			setFieldsForNewInvestigation();
			//$(".bfield").click(function(){
			//	alert('Please save the investigation first. Checkboxes are dependent of a saved/existing investigation to be registered.');
			//});
		}
		$(".bfield").click(function(){
			openCodes(this.id); //Open up the code section for the checkbox
		});
		
		openCodesWhenDataExist();		
		
		var PRIMARYPERSON =  $("#PRIMARYPERSON").val();
		if(PRIMARYPERSON == ("Visitor, Contractor"))
		{
			$("#row_PRIMARYPERSON").hide();
		}
		else
		{
			$("#row_PRIMARY_PERSON_CONTRACTOR_NAME").hide();
		}		
		
		$("#BtnLikelihoodActualOpen").hide();
		$("#BtnConsequenceActualOpen").hide();
		
		// Approved = green
		if($("#STATUS_1").val() == "APPROVED")
		{
			$("#STATUS_1").addClass("textLow");	
		}
		if($("#STATUS_2").val() == "APPROVED")
		{
			$("#STATUS_2").addClass("textLow");	
		}
		if($("#STATUS_3").val() == "APPROVED")
		{
			$("#STATUS_3").addClass("textLow");	
		}
		if($("#STATUS_4").val() == "APPROVED")
		{
			$("#STATUS_4").addClass("textLow");	
		}
		if($("#STATUS_5").val() == "APPROVED")
		{
			$("#STATUS_5").addClass("textLow");	
		}
		if($("#STATUS_6").val() == "APPROVED")
		{
			$("#STATUS_6").addClass("textLow");	
		}
		
		
		// Rejected = red
		if($("#STATUS_1").val() == "REJECTED")
		{
			$("#STATUS_1").addClass("textVeryHigh");	
		}
		if($("#STATUS_2").val() == "REJECTED")
		{
			$("#STATUS_2").addClass("textVeryHigh");	
		}
		if($("#STATUS_3").val() == "REJECTED")
		{
			$("#STATUS_3").addClass("textVeryHigh");	
		}		
		if($("#STATUS_4").val() == "REJECTED")
		{
			$("#STATUS_4").addClass("textVeryHigh");	
		}		
		if($("#STATUS_5").val() == "REJECTED")
		{
			$("#STATUS_5").addClass("textVeryHigh");	
		}		
		if($("#STATUS_6").val() == "REJECTED")
		{
			$("#STATUS_6").addClass("textVeryHigh");	
		}		
		
		setRiskRating($("#RISK_LIKELIHOOD_ACTUAL").val(), $("#RISK_CONSEQUENCE_ACTUAL").val(), "ACTUAL");
		setRiskRating($("#RISK_LIKELIHOOD_POTENTIAL").val(), $("#RISK_CONSEQUENCE_POTENTIAL").val(), "POTENTIAL");
	
		// Will disable the fields for writing. Must enable the once you want to save - before posting.
		$("#LOCATION").prop('disabled', true);
		$("#AREA").prop('disabled', true);
		$("#INCIDENT_TITLE").prop('disabled', true);
		$("#INCIDENT_DESCRIPTION").prop('disabled', true);
		$("#RISK_LIKELIHOOD_ACTUAL").prop('disabled', true);
		$("#RISK_LIKELIHOOD_POTENTIAL").prop('disabled', true);
		$("#RISK_CONSEQUENCE_ACTUAL").prop('disabled', true);
		$("#RISK_CONSEQUENCE_POTENTIAL").prop('disabled', true);
		$("#RISK_RATING_ACTUAL").prop('disabled', true);
		$("#RISK_RATING_POTENTIAL").prop('disabled', true);

//		$("#_DATEACTIONROW1").val($("#DATEACTIONROW1").val());
//		$("#_TIMEACTIONROW1").val($("#TIMEACTIONROW1").val());
//		$("#_ACTIONROW1").val($("#ACTIONROW1").val());
//		$("#_DATEACTIONROW2").val($("#DATEACTIONROW2").val());
//		$("#_TIMEACTIONROW2").val($("#TIMEACTIONROW2").val());
//		$("#_ACTIONROW2").val($("#ACTIONROW2").val());
//		$("#_DATEACTIONROW3").val($("#DATEACTIONROW3").val());
//		$("#_TIMEACTIONROW3").val($("#TIMEACTIONROW3").val());
//		$("#_ACTIONROW3").val($("#ACTIONROW3").val());
//		$("#_DATEACTIONROW4").val($("#DATEACTIONROW4").val());
//		$("#_TIMEACTIONROW4").val($("#TIMEACTIONROW4").val());
//		$("#_ACTIONROW4").val($("#ACTIONROW4").val());
//		$("#_DATEACTIONROW5").val($("#DATEACTIONROW5").val());
//		$("#_TIMEACTIONROW5").val($("#TIMEACTIONROW5").val());
//		$("#_ACTIONROW5").val($("#ACTIONROW5").val());
//		$("#_DATEACTIONROW6").val($("#DATEACTIONROW6").val());
//		$("#_TIMEACTIONROW6").val($("#TIMEACTIONROW6").val());
//		$("#_ACTIONROW6").val($("#ACTIONROW6").val());
//		$("#_DATEACTIONROW7").val($("#DATEACTIONROW7").val());
//		$("#_TIMEACTIONROW7").val($("#TIMEACTIONROW7").val());
//		$("#_ACTIONROW7").val($("#ACTIONROW7").val());
//		$("#_DATEACTIONROW8").val($("#DATEACTIONROW8").val());
//		$("#_TIMEACTIONROW8").val($("#TIMEACTIONROW8").val());
//		$("#_ACTIONROW8").val($("#ACTIONROW8").val());
//		$("#_DATEACTIONROW9").val($("#DATEACTIONROW9").val());
//		$("#_TIMEACTIONROW9").val($("#TIMEACTIONROW9").val());
//		$("#_ACTIONROW9").val($("#ACTIONROW9").val());
//		$("#_DATEACTIONROW10").val($("#DATEACTIONROW10").val());
//		$("#_TIMEACTIONROW10").val($("#TIMEACTIONROW10").val());
//		$("#_ACTIONROW10").val($("#ACTIONROW10").val());
//		$("#_DATEACTIONROW11").val($("#DATEACTIONROW11").val());
//		$("#_TIMEACTIONROW11").val($("#TIMEACTIONROW11").val());
//		$("#_ACTIONROW11").val($("#ACTIONROW11").val());
//		$("#_DATEACTIONROW12").val($("#DATEACTIONROW12").val());
//		$("#_TIMEACTIONROW12").val($("#TIMEACTIONROW12").val());
//		$("#_ACTIONROW12").val($("#ACTIONROW12").val());

		//Absent or failed Defences
		$("#_DF_CODEROW1").val($("#DF_CODEROW1").val());
		$("#_DF_CODE_FACTOR_ROW1").val($("#DF_CODE_FACTOR_ROW1").val());
		$("#_DF_CODEROW2").val($("#DF_CODEROW2").val());
		$("#_DF_CODE_FACTOR_ROW2").val($("#DF_CODE_FACTOR_ROW2").val());
		$("#_DF_CODEROW3").val($("#DF_CODEROW3").val());
		$("#_DF_CODE_FACTOR_ROW3").val($("#DF_CODE_FACTOR_ROW3").val());
		$("#_DF_CODEROW4").val($("#DF_CODEROW4").val());
		$("#_DF_CODE_FACTOR_ROW4").val($("#DF_CODE_FACTOR_ROW4").val());
		$("#_DF_CODEROW5").val($("#DF_CODEROW5").val());
		$("#_DF_CODE_FACTOR_ROW5").val($("#DF_CODE_FACTOR_ROW5").val());
		$("#_DF_CODEROW6").val($("#DF_CODEROW6").val());
		$("#_DF_CODE_FACTOR_ROW6").val($("#DF_CODE_FACTOR_ROW6").val());
		$("#_DF_CODEROW7").val($("#DF_CODEROW7").val());
		$("#_DF_CODE_FACTOR_ROW7").val($("#DF_CODE_FACTOR_ROW7").val());
		$("#_DF_CODEROW8").val($("#DF_CODEROW8").val());
		$("#_DF_CODE_FACTOR_ROW8").val($("#DF_CODE_FACTOR_ROW8").val());

		//Individual/Team Actions
		$("#_IT_CODEROW1").val($("#IT_CODEROW1").val());
		$("#_IT_CODE_FACTOR_ROW1").val($("#IT_CODE_FACTOR_ROW1").val());
		$("#_IT_CODEROW2").val($("#IT_CODEROW2").val());
		$("#_IT_CODE_FACTOR_ROW2").val($("#IT_CODE_FACTOR_ROW2").val());
		$("#_IT_CODEROW3").val($("#IT_CODEROW3").val());
		$("#_IT_CODE_FACTOR_ROW3").val($("#IT_CODE_FACTOR_ROW3").val());
		$("#_IT_CODEROW4").val($("#IT_CODEROW4").val());
		$("#_IT_CODE_FACTOR_ROW4").val($("#IT_CODE_FACTOR_ROW4").val());
		
		//Task/Environmental Conditions - Workplace
		$("#_WF_CODEROW1").val($("#WF_CODEROW1").val());
		$("#_WF_CODE_FACTOR_ROW1").val($("#WF_CODE_FACTOR_ROW1").val());
		$("#_WF_CODEROW2").val($("#WF_CODEROW2").val());
		$("#_WF_CODE_FACTOR_ROW2").val($("#WF_CODE_FACTOR_ROW2").val());
		$("#_WF_CODEROW3").val($("#WF_CODEROW3").val());
		$("#_WF_CODE_FACTOR_ROW3").val($("#WF_CODE_FACTOR_ROW3").val());
		$("#_WF_CODEROW4").val($("#WF_CODEROW4").val());
		$("#_WF_CODE_FACTOR_ROW4").val($("#WF_CODE_FACTOR_ROW4").val());
		
		//Task/Environmental Conditions - Human Factor
		$("#_HF_CODEROW1").val($("#HF_CODEROW1").val());
		$("#_HF_CODE_FACTOR_ROW1").val($("#HF_CODE_FACTOR_ROW1").val());
		$("#_HF_CODEROW2").val($("#HF_CODEROW2").val());
		$("#_HF_CODE_FACTOR_ROW2").val($("#HF_CODE_FACTOR_ROW2").val());
		$("#_HF_CODEROW3").val($("#HF_CODEROW3").val());
		$("#_HF_CODE_FACTOR_ROW3").val($("#HF_CODE_FACTOR_ROW3").val());
		$("#_HF_CODEROW4").val($("#HF_CODEROW4").val());
		$("#_HF_CODE_FACTOR_ROW4").val($("#HF_CODE_FACTOR_ROW4").val());
		
		//Organisational Factor
		$("#_OF_CODEROW1").val($("#OF_CODEROW1").val());
		$("#_OF_CODE_FACTOR_ROW1").val($("#OF_CODE_FACTOR_ROW1").val());
		$("#_OF_CODEROW2").val($("#OF_CODEROW2").val());
		$("#_OF_CODE_FACTOR_ROW2").val($("#OF_CODE_FACTOR_ROW2").val());
		$("#_OF_CODEROW3").val($("#OF_CODEROW3").val());
		$("#_OF_CODE_FACTOR_ROW3").val($("#OF_CODE_FACTOR_ROW3").val());
		$("#_OF_CODEROW4").val($("#OF_CODEROW4").val());
		$("#_OF_CODE_FACTOR_ROW4").val($("#OF_CODE_FACTOR_ROW4").val());
		
		// Key Learnings
		$("#_KEY_LEARING_TITLE_ROW1").val($("#KEY_LEARING_TITLE_ROW1").val());
		$("#_KEY_LEARING_DESC_ROW1").val($("#KEY_LEARING_DESC_ROW1").val());
		$("#_KEY_LEARING_TITLE_ROW2").val($("#KEY_LEARING_TITLE_ROW2").val());
		$("#_KEY_LEARING_DESC_ROW2").val($("#KEY_LEARING_DESC_ROW2").val());
		$("#_KEY_LEARING_TITLE_ROW3").val($("#KEY_LEARING_TITLE_ROW3").val());
		$("#_KEY_LEARING_DESC_ROW3").val($("#KEY_LEARING_DESC_ROW3").val());
		$("#_KEY_LEARING_TITLE_ROW4").val($("#KEY_LEARING_TITLE_ROW4").val());
		$("#_KEY_LEARING_DESC_ROW4").val($("#KEY_LEARING_DESC_ROW4").val());

		//Appendixes
		$("#_APPENDIXES_URL_ROW1").val($("#APPENDIXES_URL_ROW1").val());
		$("#_APPENDIXES_DESC_ROW1").val($("#APPENDIXES_DESC_ROW1").val());
		$("#_APPENDIXES_DATE_ROW1").val($("#APPENDIXES_DATE_ROW1").val());
		$("#_APPENDIXES_URL_ROW2").val($("#APPENDIXES_URL_ROW2").val());
		$("#_APPENDIXES_DESC_ROW2").val($("#APPENDIXES_DESC_ROW2").val());
		$("#_APPENDIXES_DATE_ROW2").val($("#APPENDIXES_DATE_ROW2").val());
		$("#_APPENDIXES_URL_ROW3").val($("#APPENDIXES_URL_ROW3").val());
		$("#_APPENDIXES_DESC_ROW3").val($("#APPENDIXES_DESC_ROW3").val());
		$("#_APPENDIXES_DATE_ROW3").val($("#APPENDIXES_DATE_ROW3").val());
		$("#_APPENDIXES_URL_ROW4").val($("#APPENDIXES_URL_ROW4").val());
		$("#_APPENDIXES_DESC_ROW4").val($("#APPENDIXES_DESC_ROW4").val());
		$("#_APPENDIXES_DATE_ROW4").val($("#APPENDIXES_DATE_ROW4").val());
		
		
		
		$("#LOSSES_PEOPLE_DOLLAR").addClass("textDollarCost");
		$("#LOSSES_EQUIP_DOLLAR").addClass("textDollarCost");
		$("#LOSSES_ENVIRONMENTAL_DOLLAR").addClass("textDollarCost");
		$("#LOSSES_PROCESS_DOLLAR").addClass("textDollarCost");
		
		
		
		
		$("#SIGNOFF_PERSON_ID_4_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_5_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_6_text").addClass("signoffBy");
		
		//$("#riskmatriximg").addClass("imgriskmatrixCSS");
	
		//$( ".button" ).addClass("btn btn-default btn-sm active");
		//$( ".button" ).removeClass("button");

		//$( ".submit" ).addClass("btn btn-primary btn-sm active");
		//$( ".submit" ).removeClass("submit");

		//$( ".reset" ).addClass("btn btn-default btn-sm active");
		//$( ".reset" ).removeClass("reset");


		
//----




		
		
		var selectedGoldenRule = $("#GOLDENRULE").val();
		var goldRuleEl = document.getElementById("GOLDENRULE");
		if(goldRuleEl != null)
		{
			var goldText = goldRuleEl.options[goldRuleEl.selectedIndex].innerHTML;
			if(goldText != 'Other Activities / Equipment')
			{
				$("#row_GOLDENRULEOTHER").hide();
			}
		}	

		var cats_user_is_administrator = '<?PHP echo cats_user_is_administrator();?>';
		var cats_user_is_admin_group = '<?PHP echo cats_user_is_admin_group();?>';
		var cats_user_is_super_administrator = '<?PHP echo cats_user_is_super_administrator();?>';
		var cats_user_is_root_administrator = '<?PHP echo cats_user_is_root_administrator();?>';
		var cats_user_is_editor = '<?PHP echo cats_user_is_editor();?>';

		var goldenRulePermission = false;
 
		if (cats_user_is_administrator == '1' ||
			 cats_user_is_admin_group == '1' ||
			 cats_user_is_super_administrator == '1' ||
			 cats_user_is_root_administrator == '1' ||
			 cats_user_is_editor == '1' ){
			 
			 goldenRulePermission = true;
			 
		}else
		{
			$("#GOLDENRULE").attr("disabled","disabled");
			$("#GOLDENRULEOTHER").attr("disabled","disabled");
		}
		
		
		 
	

		
		$("#INITIATED_BY_ID").on('change', function() {
		
			var selectedInitiated = $("#INITIATED_BY_ID").val();
			
			for( var i = 0, len = employeeSuperintendentMappingArray.length; i < len; i++ ) {
				var currentVal = employeeSuperintendentMappingArray[i];	
				var empId = currentVal[0];
				var superIntend = currentVal[1];
					
				if( empId == selectedInitiated && superIntend > 0) {
					$("#SUPERINTENDENT_ID").val(superIntend);					
					break;
				}
			}	
		});
		
		//-------------------------
		// ON OPEN
		//-------------------------
		// On Open, hide or show code fields - Absent of failed Defences
		if($("#_DF_CODEROW1").val() != "" || $("#_DF_CODE_FACTOR_ROW1").val() != "")
			$("#trDF2").show();
		else
			$("#trDF2").hide();
		if($("#_DF_CODEROW2").val() != "" || $("#_DF_CODE_FACTOR_ROW2").val() != "")
			$("#trDF3").show();
		else
			$("#trDF3").hide();
		if($("#_DF_CODEROW3").val() != "" || $("#_DF_CODE_FACTOR_ROW3").val() != "")
			$("#trDF4").show();
		else
			$("#trDF4").hide();
		if($("#_DF_CODEROW4").val() != "" || $("#_DF_CODE_FACTOR_ROW4").val() != "")
			$("#trDF5").show();
		else
			$("#trDF5").hide();
		if($("#_DF_CODEROW5").val() != "" || $("#_DF_CODE_FACTOR_ROW5").val() != "")
			$("#trDF6").show();
		else
			$("#trDF6").hide();
		if($("#_DF_CODEROW6").val() != "" || $("#_DF_CODE_FACTOR_ROW6").val() != "")
			$("#trDF7").show();
		else
			$("#trDF7").hide();
		if($("#_DF_CODEROW7").val() != "" || $("#_DF_CODE_FACTOR_ROW7").val() != "")
			$("#trDF8").show();
		else
			$("#trDF8").hide();
			
			
			
			
			
			
			
			
		// On Open, hide or show code fields - Individual/Team Actions
		if($("#_IT_CODEROW1").val() != "" || $("#_IT_CODE_FACTOR_ROW1").val() != "")
			$("#trIT2").show();
		else
			$("#trIT2").hide();
		if($("#_IT_CODEROW2").val() != "" || $("#_IT_CODE_FACTOR_ROW2").val() != "")
			$("#trIT3").show();
		else
			$("#trIT3").hide();
		if($("#_IT_CODEROW3").val() != "" || $("#_IT_CODE_FACTOR_ROW3").val() != "")
			$("#trIT4").show();
		else
			$("#trIT4").hide();
			
		// On Open, hide or show code fields - Task/Environmental Conditions - Workplace
		if($("#_WF_CODEROW1").val() != "" || $("#_WF_CODE_FACTOR_ROW1").val() != "")
			$("#trWF2").show();
		else
			$("#trWF2").hide();
		if($("#_WF_CODEROW2").val() != "" || $("#_WF_CODE_FACTOR_ROW2").val() != "")
			$("#trWF3").show();
		else
			$("#trWF3").hide();
		if($("#_WF_CODEROW3").val() != "" || $("#_WF_CODE_FACTOR_ROW3").val() != "")
			$("#trWF4").show();
		else
			$("#trWF4").hide();			

		// On Open, hide or show code fields - Task/Environmental Conditions - Human Factor
		if($("#_HF_CODEROW1").val() != "" || $("#_HF_CODE_FACTOR_ROW1").val() != "")
			$("#trHF2").show();
		else
			$("#trHF2").hide();
		if($("#_HF_CODEROW2").val() != "" || $("#_HF_CODE_FACTOR_ROW2").val() != "")
			$("#trHF3").show();
		else
			$("#trHF3").hide();
		if($("#_HF_CODEROW3").val() != "" || $("#_HF_CODE_FACTOR_ROW3").val() != "")
			$("#trHF4").show();
		else
			$("#trHF4").hide();		

		// On Open, hide or show code fields - Organisational Factor
		if($("#_OF_CODEROW1").val() != "" || $("#_OF_CODE_FACTOR_ROW1").val() != "")
			$("#trOF2").show();
		else
			$("#trOF2").hide();
		if($("#_OF_CODEROW2").val() != "" || $("#_OF_CODE_FACTOR_ROW2").val() != "")
			$("#trOF3").show();
		else
			$("#trOF3").hide();
		if($("#_OF_CODEROW3").val() != "" || $("#_OF_CODE_FACTOR_ROW3").val() != "")
			$("#trOF4").show();
		else
			$("#trOF4").hide();		

			
		// On Open, hide or show code fields - Key Learning
		if($("#_KEY_LEARING_TITLE_ROW1").val() != "" || $("#_KEY_LEARING_DESC_ROW1").val() != "")
			$("#trKL2").show();
		else
			$("#trKL2").hide();
		if($("#_KEY_LEARING_TITLE_ROW2").val() != "" || $("#_KEY_LEARING_DESC_ROW2").val() != "")
			$("#trKL3").show();
		else
			$("#trKL3").hide();
		if($("#_KEY_LEARING_TITLE_ROW3").val() != "" || $("#_KEY_LEARING_DESC_ROW3").val() != "")
			$("#trKL4").show();
		else
			$("#trKL4").hide();				
			

		// On Open, hide or show code fields - Appendixes
		if($("#_APPENDIXES_URL_ROW1").val() != "" || $("#_APPENDIXES_DESC_ROW1").val() != "" || $("#_APPENDIXES_DATE_ROW1").val() != "")
			$("#trAPX2").show();
		else
			$("#trAPX2").hide();
		if($("#_APPENDIXES_URL_ROW2").val() != "" || $("#_APPENDIXES_DESC_ROW2").val() != "" || $("#_APPENDIXES_DATE_ROW2").val() != "")
			$("#trAPX3").show();
		else
			$("#trAPX3").hide();
		if($("#_APPENDIXES_URL_ROW3").val() != "" || $("#_APPENDIXES_DESC_ROW3").val() != "" || $("#_APPENDIXES_DATE_ROW3").val() != "")
			$("#trAPX4").show();
		else
			$("#trAPX4").hide();	

		
			
		// On Open, hide or show Recommendation fields 2 - 10
		if($("#RECOMMENDATION_1").val() != "" || $("#RECOMMENDATION_2").val() != "")
			$("#row_RECOMMENDATION_2").show();
		else
			$("#row_RECOMMENDATION_2").hide();
		if($("#RECOMMENDATION_2").val() != "" || $("#row_RECOMMENDATION_3").val() != "")
			$("#row_RECOMMENDATION_3").show();
		else
			$("#row_RECOMMENDATION_3").hide();
		if($("#RECOMMENDATION_3").val() != "" || $("#RECOMMENDATION_4").val() != "")
			$("#row_RECOMMENDATION_4").show();
		else
			$("#row_RECOMMENDATION_4").hide();
		if($("#RECOMMENDATION_4").val() != "" || $("#RECOMMENDATION_5").val() != "")
			$("#row_RECOMMENDATION_5").show();
		else
			$("#row_RECOMMENDATION_5").hide();
		if($("#RECOMMENDATION_5").val() != "" || $("#RECOMMENDATION_6").val() != "")
			$("#row_RECOMMENDATION_6").show();
		else
			$("#row_RECOMMENDATION_6").hide();
		if($("#RECOMMENDATION_6").val() != "" || $("#RECOMMENDATION_7").val() != "")
			$("#row_RECOMMENDATION_7").show();
		else
			$("#row_RECOMMENDATION_7").hide();
		if($("#RECOMMENDATION_7").val() != "" || $("#RECOMMENDATION_8").val() != "")
			$("#row_RECOMMENDATION_8").show();
		else
			$("#row_RECOMMENDATION_8").hide();
		if($("#RECOMMENDATION_8").val() != "" || $("#RECOMMENDATION_9").val() != "")
			$("#row_RECOMMENDATION_9").show();
		else
			$("#row_RECOMMENDATION_9").hide();
		if($("#RECOMMENDATION_9").val() != "" || $("#RECOMMENDATION_10").val() != "")
			$("#row_RECOMMENDATION_10").show();
		else
			$("#row_RECOMMENDATION_10").hide();

			
			
		//------------------------------
		// ON Change
		//------------------------------
		
		// On changed values, hide or show code fields - Absend or failed Defences
		$("#_DF_CODEROW1").on('change', function() {
			$("#trDF2").show();
		});
		$("#_DF_CODE_FACTOR_ROW1").on('change', function() {
			$("#trDF2").show();
		});
		$("#_DF_CODEROW2").on('change', function() {
			$("#trDF3").show();
		});
		$("#_DF_CODE_FACTOR_ROW2").on('change', function() {
			$("#trDF3").show();
		});
		$("#_DF_CODEROW3").on('change', function() {
			$("#trDF4").show();
		});
		$("#_DF_CODE_FACTOR_ROW3").on('change', function() {
			$("#trDF4").show();
		});
		$("#_DF_CODEROW4").on('change', function() {
			$("#trDF5").show();
		});
		$("#_DF_CODE_FACTOR_ROW4").on('change', function() {
			$("#trDF5").show();
		});
		$("#_DF_CODEROW5").on('change', function() {
			$("#trDF6").show();
		});
		$("#_DF_CODE_FACTOR_ROW5").on('change', function() {
			$("#trDF6").show();
		});
		$("#_DF_CODEROW6").on('change', function() {
			$("#trDF7").show();
		});
		$("#_DF_CODE_FACTOR_ROW6").on('change', function() {
			$("#trDF7").show();
		});
		$("#_DF_CODEROW7").on('change', function() {
			$("#trDF8").show();
		});
		$("#_DF_CODE_FACTOR_ROW7").on('change', function() {
			$("#trDF8").show();
		});
		
		
		
		
		
		
		
		// On changed values, hide or show code fields - Individual/Team Actions
		$("#_IT_CODEROW1").on('change', function() {
			$("#trIT2").show();
		});
		$("#_IT_CODE_FACTOR_ROW1").on('change', function() {
			$("#trIT2").show();
		});
		$("#_IT_CODEROW2").on('change', function() {
			$("#trIT3").show();
		});
		$("#_IT_CODE_FACTOR_ROW2").on('change', function() {
			$("#trIT3").show();
		});
		$("#_IT_CODEROW3").on('change', function() {
			$("#trIT4").show();
		});
		$("#_IT_CODE_FACTOR_ROW3").on('change', function() {
			$("#trIT4").show();
		});		
		
		// On changed values, hide or show code fields - Task/Environmental Conditions - Workplace
		$("#_WF_CODEROW1").on('change', function() {
			$("#trWF2").show();
		});
		$("#_WF_CODE_FACTOR_ROW1").on('change', function() {
			$("#trWF2").show();
		});
		$("#_WF_CODEROW2").on('change', function() {
			$("#trWF3").show();
		});
		$("#_WF_CODE_FACTOR_ROW2").on('change', function() {
			$("#trWF3").show();
		});
		$("#_WF_CODEROW3").on('change', function() {
			$("#trWF4").show();
		});
		$("#_WF_CODE_FACTOR_ROW3").on('change', function() {
			$("#trWF4").show();
		});		
				
		// On changed values, hide or show code fields - Task/Environmental Conditions - Human Factor
		$("#_HF_CODEROW1").on('change', function() {
			$("#trHF2").show();
		});
		$("#_HF_CODE_FACTOR_ROW1").on('change', function() {
			$("#trHF2").show();
		});
		$("#_HF_CODEROW2").on('change', function() {
			$("#trHF3").show();
		});
		$("#_HF_CODE_FACTOR_ROW2").on('change', function() {
			$("#trHF3").show();
		});
		$("#_HF_CODEROW3").on('change', function() {
			$("#trHF4").show();
		});
		$("#_HF_CODE_FACTOR_ROW3").on('change', function() {
			$("#trHF4").show();
		});				
		
		// On changed values, hide or show code fields - Organisational Factor
		$("#_OF_CODEROW1").on('change', function() {
			$("#trOF2").show();
		});
		$("#_OF_CODE_FACTOR_ROW1").on('change', function() {
			$("#trOF2").show();
		});
		$("#_OF_CODEROW2").on('change', function() {
			$("#trOF3").show();
		});
		$("#_OF_CODE_FACTOR_ROW2").on('change', function() {
			$("#trOF3").show();
		});
		$("#_OF_CODEROW3").on('change', function() {
			$("#trOF4").show();
		});
		$("#_OF_CODE_FACTOR_ROW3").on('change', function() {
			$("#trOF4").show();
		});				
				
				
		// On changed values, hide or show code fields - Key Learnings
		$("#_KEY_LEARING_TITLE_ROW1").on('change', function() {
			$("#trKL2").show();
		});
		$("#_KEY_LEARING_DESC_ROW1").on('change', function() {
			$("#trKL2").show();
		});
		$("#_KEY_LEARING_TITLE_ROW2").on('change', function() {
			$("#trKL3").show();
		});
		$("#_KEY_LEARING_DESC_ROW2").on('change', function() {
			$("#trKL3").show();
		});
		$("#_KEY_LEARING_TITLE_ROW3").on('change', function() {
			$("#trKL4").show();
		});
		$("#_KEY_LEARING_DESC_ROW3").on('change', function() {
			$("#trKL4").show();
		});				
							
		// On changed values, hide or show code fields - Appendix
		$("#_APPENDIXES_URL_ROW1").on('change', function() {
			$("#trAPX2").show();
		});
		$("#_APPENDIXES_DESC_ROW1").on('change', function() {
			$("#trAPX2").show();
		});
		$("#_APPENDIXES_DATE_ROW1").on('change', function() {
			$("#trAPX2").show();
		});
		
		$("#_APPENDIXES_URL_ROW2").on('change', function() {
			$("#trAPX3").show();
		});
		$("#_APPENDIXES_DESC_ROW2").on('change', function() {
			$("#trAPX3").show();
		});
		$("#_APPENDIXES_DATE_ROW2").on('change', function() {
			$("#trAPX3").show();
		});		
		
		$("#_APPENDIXES_URL_ROW3").on('change', function() {
			$("#trAPX4").show();
		});
		$("#_APPENDIXES_DESC_ROW3").on('change', function() {
			$("#trAPX4").show();
		});
		$("#_APPENDIXES_DATE_ROW3").on('change', function() {
			$("#trAPX4").show();
		});			

				
		// On changed values, hide or show Recommendations 2 - 10
		$("#RECOMMENDATION_1").on('change', function() {
			$("#row_RECOMMENDATION_2").show();
		});
		$("#RECOMMENDATION_2").on('change', function() {
			$("#row_RECOMMENDATION_3").show();
		});
		$("#RECOMMENDATION_3").on('change', function() {
			$("#row_RECOMMENDATION_4").show();
		});
		$("#RECOMMENDATION_4").on('change', function() {
			$("#row_RECOMMENDATION_5").show();
		});
		$("#RECOMMENDATION_5").on('change', function() {
			$("#row_RECOMMENDATION_6").show();
		});
		$("#RECOMMENDATION_6").on('change', function() {
			$("#row_RECOMMENDATION_7").show();
		});
		$("#RECOMMENDATION_7").on('change', function() {
			$("#row_RECOMMENDATION_8").show();
		});
		$("#RECOMMENDATION_8").on('change', function() {
			$("#row_RECOMMENDATION_9").show();
		});
		$("#RECOMMENDATION_9").on('change', function() {
			$("#row_RECOMMENDATION_10").show();
		});
				
				
				
				
		
		$("#GOLDENRULE").on('change', function() {
		
			if(!goldenRulePermission)
			{
				alert("Need higher user permission to set/change Golden Rule.");
				$("#GOLDENRULE").val(selectedGoldenRule);
				return -1;
			}
		
			var selectedGoldenRule = $("#GOLDENRULE").val();
			var goldRuleEl = document.getElementById("GOLDENRULE");
			var goldText = goldRuleEl.options[goldRuleEl.selectedIndex].innerHTML;
			//alert('goldText: ' + goldText);
			if(goldText == 'Other Activities / Equipment')
			{
				$("#row_GOLDENRULEOTHER").show();
			}
			else
			{
				$("#row_GOLDENRULEOTHER").hide();
				$("#GOLDENRULEOTHER").val("");
			}
			
			
			
		});
		
		
			
		
		
		var forwardPersonId4 = $("#FORWARD_PERSON_ID_4").val();
		var forwardPersonId5 = $("#FORWARD_PERSON_ID_5").val();
		if(forwardPersonId4 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_5").hide();
			$("#row_row_sign_off_icam2").hide();
			$("#SIGNOFF_PERSON_ID_5_text").hide();
			$("#FORWARD_PERSON_ID_5").hide();
			$("#FORWARD_PERSON_ID_5_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_5").hide();
			$("#btnReject_FORWARD_PERSON_ID_5").hide();
			$("#btnClear_FORWARD_PERSON_ID_5").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_5").hide();
			$("#row_SIGNOFF_DATE_5").hide();
			$("#SIGNOFF_DATE_5_d").hide();
			$("#STATUS_5").hide();
			$("#row_MESSAGE_5").hide();
			$("#MESSAGE_5").hide();
		}
		if(forwardPersonId5 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_6").hide();
			$("#row_row_sign_off_icam3").hide();
			$("#SIGNOFF_PERSON_ID_6_text").hide();
			$("#FORWARD_PERSON_ID_6").hide();
			$("#FORWARD_PERSON_ID_6_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_6").hide();
			$("#btnReject_FORWARD_PERSON_ID_6").hide();
			$("#btnClear_FORWARD_PERSON_ID_6").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_6").hide();
			$("#row_SIGNOFF_DATE_6").hide();
			$("#SIGNOFF_DATE_6_d").hide();
			$("#STATUS_6").hide();
			$("#row_MESSAGE_6").hide();
			$("#MESSAGE_6").hide();
		}
		
		
	});		
	

			//$(function (){
			//	$('#DF_CODE_FACTOR_ROW1').change(function (){
			////	  var val1 = $("#_DF_CODE_FACTOR_ROW1").html();
			//	  var val1 = $("#_DF_CODE_FACTOR_ROW1").val();
    
			//	alert(' TEST :' + val1 + ' ' + val2);
				

			//})});			
	
	function setLikelihoodActual(likelihood)
	{
		//alert('Likelihood: ' + estimation); RISK_LIKELIHOOD_ACTUAL
		
		$("#RISK_LIKELIHOOD_ACTUAL").val(likelihood);
		setRiskRating(likelihood, $("#RISK_CONSEQUENCE_ACTUAL").val(), "ACTUAL");
		$("#BthLikelihoodActualClose").click();
	}

	function setConsequenceActual(consequence)
	{
		//alert('setConsequenceActual: ' + consequence);
		$("#RISK_CONSEQUENCE_ACTUAL").val(consequence);
		setRiskRating($("#RISK_LIKELIHOOD_ACTUAL").val(), consequence, "ACTUAL");
		$("#BthConsequenceCloseActual").click();
	}
	
	function setLikelihoodPotential(likelihood)
	{
		//alert('Likelihood: ' + estimation); 
		
		$("#RISK_LIKELIHOOD_POTENTIAL").val(likelihood);
		setRiskRating(likelihood, $("#RISK_CONSEQUENCE_POTENTIAL").val(), "POTENTIAL");
		$("#BthLikelihoodPotentialClose").click();
	}

	function setConsequencePotential(consequence)
	{
		//alert('setConsequencePotential: ' + consequence);
		$("#RISK_CONSEQUENCE_POTENTIAL").val(consequence);
		setRiskRating($("#RISK_LIKELIHOOD_POTENTIAL").val(), consequence, "POTENTIAL");
		$("#BthConsequenceClosePotential").click();
	}
	
	
	function setRiskRating(likelihood, consequence, type)
	{
		//alert('setRiskRating: ' + likelihood + ' ' + consequence + ' ' + type);
		if(likelihood != "" && likelihood != null && consequence != "" && consequence != null)
		{
			if(likelihood == "A - Almost Certain")
			{ 
				if(consequence == "5 - Catastrophic")
					setVeryHigh(type, 'A5');
				if(consequence == "4 - Major")
					setVeryHigh(type, 'A4');
				if(consequence == "3 - Moderate")
					setHigh(type, 'A3');
				if(consequence == "2 - Minor")
					setHigh(type, 'A2');
				if(consequence == "1 - Insignificant")
					setMedium(type, 'A1');
			}
			else if(likelihood == "B - Likely")
			{ 
				if(consequence == "5 - Catastrophic")
					setVeryHigh(type, 'B5');
				if(consequence == "4 - Major")
					setHigh(type, 'B4');
				if(consequence == "3 - Moderate")
					setHigh(type, 'B3');
				if(consequence == "2 - Minor")
					setMedium(type, 'B2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'B1');
			}
			else if(likelihood == "C - Moderate")
			{ 
				if(consequence == "5 - Catastrophic")
					setHigh(type, 'C5');
				if(consequence == "4 - Major")
					setHigh(type, 'C4');
				if(consequence == "3 - Moderate")
					setMedium(type, 'C3');
				if(consequence == "2 - Minor")
					setMedium(type, 'C2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'C1');
			}
			else if(likelihood == "D - Unlikely")
			{ 
				if(consequence == "5 - Catastrophic")
					setHigh(type, 'D5');
				if(consequence == "4 - Major")
					setMedium(type, 'D4');
				if(consequence == "3 - Moderate")
					setMedium(type, 'D3');
				if(consequence == "2 - Minor")
					setLow(type, 'D2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'D1');
			}
			else if(likelihood == "E - Rare")
			{ 
				if(consequence == "5 - Catastrophic")
					setMedium(type, 'E5');
				if(consequence == "4 - Major")
					setMedium(type, 'E4', '');
				if(consequence == "3 - Moderate")
					setMedium(type, 'E3');
				if(consequence == "2 - Minor")
					setLow(type, 'E2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'E1');
			}
		
		}
	}
	
	function setVeryHigh(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - Very High");
			$("#RISK_RATING_ACTUAL").removeClass("textHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textMedium");
			$("#RISK_RATING_ACTUAL").removeClass("textLow");
			$("#RISK_RATING_ACTUAL").addClass("textVeryHigh");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - Very High");
			$("#RISK_RATING_POTENTIAL").removeClass("textHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textMedium");
			$("#RISK_RATING_POTENTIAL").removeClass("textLow");
			$("#RISK_RATING_POTENTIAL").addClass("textVeryHigh");
		}
	}
	
	function setHigh(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - High");
			$("#RISK_RATING_ACTUAL").removeClass("textVeryHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textMedium");
			$("#RISK_RATING_ACTUAL").removeClass("textLow");
			$("#RISK_RATING_ACTUAL").addClass("textHigh");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - High");
			$("#RISK_RATING_POTENTIAL").removeClass("textVeryHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textMedium");
			$("#RISK_RATING_POTENTIAL").removeClass("textLow");
			$("#RISK_RATING_POTENTIAL").addClass("textHigh");
		}
	}
	
	function setMedium(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - Medium");
			$("#RISK_RATING_ACTUAL").removeClass("textVeryHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textLow");
			$("#RISK_RATING_ACTUAL").addClass("textMedium");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - Medium");
			$("#RISK_RATING_POTENTIAL").removeClass("textVeryHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textLow");
			$("#RISK_RATING_POTENTIAL").addClass("textMedium");
		}
	}
	
	function setLow(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - Low");
			$("#RISK_RATING_ACTUAL").removeClass("textVeryHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textMedium");
			$("#RISK_RATING_ACTUAL").addClass("textLow");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - Low");
			$("#RISK_RATING_POTENTIAL").removeClass("textVeryHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textMedium");
			$("#RISK_RATING_POTENTIAL").addClass("textLow");
		}
	}

	function submitApprove(field)
	{
	
		var signoffBy = "";
		var signoffDate = "";
		var forwardTo = "";
		var msg = "";
	
		//alert(' submitApprove field: ' + field);
	
		if(field == 'FORWARD_PERSON_ID_4') //iCAM
		{
			$("#WFL_STAGE").val("4");
			signoffBy = $("#SIGNOFF_PERSON_ID_4").val();
			signoffDate = $("#SIGNOFF_DATE_4").val();
			forwardTo = $("#FORWARD_PERSON_ID_4").val();
			msg = $("#MESSAGE_4").val();
			if(isWorkflowStepValidForIcam(signoffBy, forwardTo, msg))
			{
				$("#STATUS_4").val("APPROVED");
				$("#STATUS_4").removeClass("textVeryHigh");
				$("#STATUS_4").addClass("textLow");
				$("#DO_EMAIL_FLAG_4").val("True");
			}
		}
		else if(field == 'FORWARD_PERSON_ID_5') //iCAM
		{
			$("#WFL_STAGE").val("5");
			signoffBy = $("#SIGNOFF_PERSON_ID_5").val();
			signoffDate = $("#SIGNOFF_DATE_5").val();
			forwardTo = $("#FORWARD_PERSON_ID_5").val();
			msg = $("#MESSAGE_5").val();
			if(isWorkflowStepValidForIcam(signoffBy, forwardTo, msg))
			{
				$("#STATUS_5").val("APPROVED");
				$("#STATUS_5").removeClass("textVeryHigh");
				$("#STATUS_5").addClass("textLow");
				$("#DO_EMAIL_FLAG_5").val("True");
			}
		}
		else if(field == 'FORWARD_PERSON_ID_6') //iCAM
		{
			$("#WFL_STAGE").val("6");
			signoffBy = $("#SIGNOFF_PERSON_ID_6").val();
			signoffDate = $("#SIGNOFF_DATE_6").val();
			forwardTo = $("#FORWARD_PERSON_ID_6").val();
			msg = $("#MESSAGE_6").val();
			if(isWorkflowStepValidForIcam(signoffBy, forwardTo, msg))
			{
				$("#STATUS_6").val("APPROVED");
				$("#STATUS_6").removeClass("textVeryHigh");
				$("#STATUS_6").addClass("textLow");
				$("#DO_EMAIL_FLAG_6").val("True");
			}
		}
		
		$("#APPROVAL_STATUS").val("APPROVED");
		$("#SIGN_OFF_BY").val(signoffBy);
		$("#SIGN_OFF_DATE_TIME").val(signoffDate);
		$("#FORWARD_TO").val(forwardTo);
		$("#MESSAGE").val(msg);

		// TEST:	
		var wflstage = $("#WFL_STAGE").val()
		var wflstatus = $("#APPROVAL_STATUS").val()
		signoffBy = $("#SIGN_OFF_BY").val()
		signoffDate = $("#SIGN_OFF_DATE_TIME").val()
		forwardTo = $("#FORWARD_TO").val();
		msg = $("#MESSAGE").val();
		
		//alert('Reject: stage: ' + wflstage + ' wflstatus: ' + wflstatus + ' signoffBy: ' + signoffBy + ' Date: ' + signoffDate + ' ForwardTo: ' + forwardTo + ' msg: ' + msg);
		
		//$("#ApproveRejectForm").submit();
		
	}
	
	
	function submitReject(field)
	{
	
		var signoffBy = "";
		var signoffDate = "";
		var forwardTo = "";
		var msg = "";
		
		if(field == 'FORWARD_PERSON_ID_4') //iCAM
		{
			$("#WFL_STAGE").val("4");
			signoffBy = $("#SIGNOFF_PERSON_ID_4").val();
			signoffDate = $("#SIGNOFF_DATE_4").val();
			forwardTo = $("#FORWARD_PERSON_ID_4").val();
			msg = $("#MESSAGE_4").val();
			if(isWorkflowStepValidForIcam(signoffBy, forwardTo, msg))
			{
				$("#STATUS_4").val("REJECTED");
				$("#STATUS_4").removeClass("textLow");
				$("#STATUS_4").addClass("textVeryHigh");				
				$("#DO_EMAIL_FLAG_4").val("True");
			}
		}
		else if(field == 'FORWARD_PERSON_ID_5') //iCAM
		{
			$("#WFL_STAGE").val("5");
			signoffBy = $("#SIGNOFF_PERSON_ID_5").val();
			signoffDate = $("#SIGNOFF_DATE_5").val();
			forwardTo = $("#FORWARD_PERSON_ID_5").val();
			msg = $("#MESSAGE_5").val();
			if(isWorkflowStepValidForIcam(signoffBy, forwardTo, msg))
			{
				$("#STATUS_5").val("REJECTED");
				$("#STATUS_5").removeClass("textLow");
				$("#STATUS_5").addClass("textVeryHigh");				
				$("#DO_EMAIL_FLAG_5").val("True");
			}
		}
		else if(field == 'FORWARD_PERSON_ID_6') //iCAM
		{
			$("#WFL_STAGE").val("6");
			signoffBy = $("#SIGNOFF_PERSON_ID_6").val();
			signoffDate = $("#SIGNOFF_DATE_6").val();
			forwardTo = $("#FORWARD_PERSON_ID_6").val();
			msg = $("#MESSAGE_6").val();
			if(isWorkflowStepValidForIcam(signoffBy, forwardTo, msg))
			{
				$("#STATUS_6").val("REJECTED");
				$("#STATUS_6").removeClass("textLow");
				$("#STATUS_6").addClass("textVeryHigh");				
				$("#DO_EMAIL_FLAG_6").val("True");
			}
		}
		
		$("#APPROVAL_STATUS").val("REJECTED");
		$("#SIGN_OFF_BY").val(signoffBy);
		$("#SIGN_OFF_DATE_TIME").val(signoffDate);
		$("#FORWARD_TO").val(forwardTo);
		$("#MESSAGE").val(msg);

		// TEST:	
		var wflstage = $("#WFL_STAGE").val()
		var wflstatus = $("#APPROVAL_STATUS").val()
		signoffBy = $("#SIGN_OFF_BY").val()
		signoffDate = $("#SIGN_OFF_DATE_TIME").val()
		forwardTo = $("#FORWARD_TO").val();
		msg = $("#MESSAGE").val();
		
		//alert('Reject: stage: ' + wflstage + ' wflstatus: ' + wflstatus + ' signoffBy: ' + signoffBy + ' Date: ' + signoffDate + ' ForwardTo: ' + forwardTo + ' msg: ' + msg);
		
		//$("#ApproveRejectForm").submit();
	}
	
	function preSaveForm()
	{
	
		var forwardPersonId4Current = $("#FORWARD_PERSON_ID_4").val();
		var forwardPersonId5Current = $("#FORWARD_PERSON_ID_5").val();
		var forwardPersonId6Current = $("#FORWARD_PERSON_ID_6").val();
		
		var hasForward4Changed = (forwardPersonId4Current != forwardPersonId4Onload);
		var hasForward5Changed = (forwardPersonId5Current != forwardPersonId5Onload);
		var hasForward6Changed = (forwardPersonId6Current != forwardPersonId6Onload);
		var signoffBy = "";
		var signoffDate = "";
		var forwardTo = "";
		var msg = "";
		
		//alert("changes: " + hasForward4Changed + " " + forwardPersonId4Current + " " + forwardPersonId4Onload);
		
		//if(hasForward4Changed)
		//{
			forwardTo = $("#FORWARD_PERSON_ID_4").val();
			if(forwardTo != "")
			{
				signoffBy = $("#SIGNOFF_PERSON_ID_4").val();
				//$("#DO_EMAIL_FLAG_4").val("True");
				$("#WFL_STAGE").val("4");			
				signoffDate = $("#SIGNOFF_DATE_4").val();
				msg = $("#MESSAGE_4").val(); 
			}				
		//}
		//if(hasForward5Changed)
		//{
			forwardTo = $("#FORWARD_PERSON_ID_5").val();
			if(forwardTo != "")
			{
				//$("#DO_EMAIL_FLAG_5").val("True");
				$("#WFL_STAGE").val("5");			
				signoffBy = $("#SIGNOFF_PERSON_ID_5").val();
				signoffDate = $("#SIGNOFF_DATE_5").val();
				msg = $("#MESSAGE_5").val(); 	
			}				
		//}
		//if(hasForward6Changed)
		//{
			forwardTo = $("#FORWARD_PERSON_ID_6").val();
			if(forwardTo != "")
			{
				//$("#DO_EMAIL_FLAG_6").val("True");
				$("#WFL_STAGE").val("6");
				signoffBy = $("#SIGNOFF_PERSON_ID_6").val();
				signoffDate = $("#SIGNOFF_DATE_6").val();
				msg = $("#MESSAGE_6").val(); 			
			}
		//}
        
        if(forwardTo != "")
		{
			$("#SIGN_OFF_BY").val(signoffBy);
			$("#SIGN_OFF_DATE_TIME").val(signoffDate);
			$("#FORWARD_TO").val(forwardTo);
			$("#MESSAGE").val(msg);		
		}				
		
		//Consequence Ranking---------------------------
		$("#RISK_LIKELIHOOD_ACTUAL").prop('disabled', false);
		$("#RISK_LIKELIHOOD_POTENTIAL").prop('disabled', false);
		$("#RISK_CONSEQUENCE_ACTUAL").prop('disabled', false);
		$("#RISK_CONSEQUENCE_POTENTIAL").prop('disabled', false);
		$("#RISK_RATING_ACTUAL").prop('disabled', false);
		$("#RISK_RATING_POTENTIAL").prop('disabled', false);

		//Sequence of Events---------------------------------
//		$("#DATEACTIONROW1").val($("#_DATEACTIONROW1").val());
//		$("#TIMEACTIONROW1").val($("#_TIMEACTIONROW1").val());
//		$("#ACTIONROW1").val($("#_ACTIONROW1").val());
		
//		$("#DATEACTIONROW2").val($("#_DATEACTIONROW2").val());
//		$("#TIMEACTIONROW2").val($("#_TIMEACTIONROW2").val());
//		$("#ACTIONROW2").val($("#_ACTIONROW2").val());

//		$("#DATEACTIONROW3").val($("#_DATEACTIONROW3").val());
//		$("#TIMEACTIONROW3").val($("#_TIMEACTIONROW3").val());
//		$("#ACTIONROW3").val($("#_ACTIONROW3").val());
		
//		$("#DATEACTIONROW4").val($("#_DATEACTIONROW4").val());
//		$("#TIMEACTIONROW4").val($("#_TIMEACTIONROW4").val());
//		$("#ACTIONROW4").val($("#_ACTIONROW4").val());

//		$("#DATEACTIONROW5").val($("#_DATEACTIONROW5").val());
//		$("#TIMEACTIONROW5").val($("#_TIMEACTIONROW5").val());
//		$("#ACTIONROW5").val($("#_ACTIONROW5").val());

//		$("#DATEACTIONROW6").val($("#_DATEACTIONROW6").val());
//		$("#TIMEACTIONROW6").val($("#_TIMEACTIONROW6").val());
//		$("#ACTIONROW6").val($("#_ACTIONROW6").val());

//		$("#DATEACTIONROW7").val($("#_DATEACTIONROW7").val());
//		$("#TIMEACTIONROW7").val($("#_TIMEACTIONROW7").val());
//		$("#ACTIONROW7").val($("#_ACTIONROW7").val());

//		$("#DATEACTIONROW8").val($("#_DATEACTIONROW8").val());
//		$("#TIMEACTIONROW8").val($("#_TIMEACTIONROW8").val());
//		$("#ACTIONROW8").val($("#_ACTIONROW8").val());

//		$("#DATEACTIONROW9").val($("#_DATEACTIONROW9").val());
//		$("#TIMEACTIONROW9").val($("#_TIMEACTIONROW9").val());
//		$("#ACTIONROW9").val($("#_ACTIONROW9").val());
		
//		$("#DATEACTIONROW10").val($("#_DATEACTIONROW10").val());
//		$("#TIMEACTIONROW10").val($("#_TIMEACTIONROW10").val());
//		$("#ACTIONROW10").val($("#_ACTIONROW10").val());

//		$("#DATEACTIONROW11").val($("#_DATEACTIONROW11").val());
//		$("#TIMEACTIONROW11").val($("#_TIMEACTIONROW11").val());
//		$("#ACTIONROW11").val($("#_ACTIONROW11").val());

//		$("#DATEACTIONROW12").val($("#_DATEACTIONROW12").val());
//		$("#TIMEACTIONROW12").val($("#_TIMEACTIONROW12").val());
//		$("#ACTIONROW12").val($("#_ACTIONROW12").val());
		
		//Absent or failed Defences
		$("#DF_CODEROW1").val($("#_DF_CODEROW1").val());
		$("#DF_CODE_FACTOR_ROW1").val($("#_DF_CODE_FACTOR_ROW1").val());
		$("#DF_CODEROW2").val($("#_DF_CODEROW2").val());
		$("#DF_CODE_FACTOR_ROW2").val($("#_DF_CODE_FACTOR_ROW2").val());
		$("#DF_CODEROW3").val($("#_DF_CODEROW3").val());
		$("#DF_CODE_FACTOR_ROW3").val($("#_DF_CODE_FACTOR_ROW3").val());
		$("#DF_CODEROW4").val($("#_DF_CODEROW4").val());
		$("#DF_CODE_FACTOR_ROW4").val($("#_DF_CODE_FACTOR_ROW4").val());
		$("#DF_CODEROW5").val($("#_DF_CODEROW5").val());
		$("#DF_CODE_FACTOR_ROW5").val($("#_DF_CODE_FACTOR_ROW5").val());
		$("#DF_CODEROW6").val($("#_DF_CODEROW6").val());
		$("#DF_CODE_FACTOR_ROW6").val($("#_DF_CODE_FACTOR_ROW6").val());
		$("#DF_CODEROW7").val($("#_DF_CODEROW7").val());
		$("#DF_CODE_FACTOR_ROW7").val($("#_DF_CODE_FACTOR_ROW7").val());
		$("#DF_CODEROW8").val($("#_DF_CODEROW8").val());
		$("#DF_CODE_FACTOR_ROW8").val($("#_DF_CODE_FACTOR_ROW8").val());
		
		//Individual/Team Actions
		$("#IT_CODEROW1").val($("#_IT_CODEROW1").val());
		$("#IT_CODE_FACTOR_ROW1").val($("#_IT_CODE_FACTOR_ROW1").val());
		$("#IT_CODEROW2").val($("#_IT_CODEROW2").val());
		$("#IT_CODE_FACTOR_ROW2").val($("#_IT_CODE_FACTOR_ROW2").val());
		$("#IT_CODEROW3").val($("#_IT_CODEROW3").val());
		$("#IT_CODE_FACTOR_ROW3").val($("#_IT_CODE_FACTOR_ROW3").val());
		$("#IT_CODEROW4").val($("#_IT_CODEROW4").val());
		$("#IT_CODE_FACTOR_ROW4").val($("#_IT_CODE_FACTOR_ROW4").val());

		//Task/Environmental Conditions - Workplace
		$("#WF_CODEROW1").val($("#_WF_CODEROW1").val());
		$("#WF_CODE_FACTOR_ROW1").val($("#_WF_CODE_FACTOR_ROW1").val());
		$("#WF_CODEROW2").val($("#_WF_CODEROW2").val());
		$("#WF_CODE_FACTOR_ROW2").val($("#_WF_CODE_FACTOR_ROW2").val());
		$("#WF_CODEROW3").val($("#_WF_CODEROW3").val());
		$("#WF_CODE_FACTOR_ROW3").val($("#_WF_CODE_FACTOR_ROW3").val());
		$("#WF_CODEROW4").val($("#_WF_CODEROW4").val());
		$("#WF_CODE_FACTOR_ROW4").val($("#_WF_CODE_FACTOR_ROW4").val());
		
		//Task/Environmental Conditions - Human Factor
		$("#HF_CODEROW1").val($("#_HF_CODEROW1").val());
		$("#HF_CODE_FACTOR_ROW1").val($("#_HF_CODE_FACTOR_ROW1").val());
		$("#HF_CODEROW2").val($("#_HF_CODEROW2").val());
		$("#HF_CODE_FACTOR_ROW2").val($("#_HF_CODE_FACTOR_ROW2").val());
		$("#HF_CODEROW3").val($("#_HF_CODEROW3").val());
		$("#HF_CODE_FACTOR_ROW3").val($("#_HF_CODE_FACTOR_ROW3").val());
		$("#HF_CODEROW4").val($("#_HF_CODEROW4").val());
		$("#HF_CODE_FACTOR_ROW4").val($("#_HF_CODE_FACTOR_ROW4").val());

		//Organisational Factor
		$("#OF_CODEROW1").val($("#_OF_CODEROW1").val());
		$("#OF_CODE_FACTOR_ROW1").val($("#_OF_CODE_FACTOR_ROW1").val());
		$("#OF_CODEROW2").val($("#_OF_CODEROW2").val());
		$("#OF_CODE_FACTOR_ROW2").val($("#_OF_CODE_FACTOR_ROW2").val());
		$("#OF_CODEROW3").val($("#_OF_CODEROW3").val());
		$("#OF_CODE_FACTOR_ROW3").val($("#_OF_CODE_FACTOR_ROW3").val());
		$("#OF_CODEROW4").val($("#_OF_CODEROW4").val());
		$("#OF_CODE_FACTOR_ROW4").val($("#_OF_CODE_FACTOR_ROW4").val());
		
		//Key Learnings
		$("#KEY_LEARING_TITLE_ROW1").val($("#_KEY_LEARING_TITLE_ROW1").val());
		$("#KEY_LEARING_DESC_ROW1").val($("#_KEY_LEARING_DESC_ROW1").val());
		$("#KEY_LEARING_TITLE_ROW2").val($("#_KEY_LEARING_TITLE_ROW2").val());
		$("#KEY_LEARING_DESC_ROW2").val($("#_KEY_LEARING_DESC_ROW2").val());
		$("#KEY_LEARING_TITLE_ROW3").val($("#_KEY_LEARING_TITLE_ROW3").val());
		$("#KEY_LEARING_DESC_ROW3").val($("#_KEY_LEARING_DESC_ROW3").val());
		$("#KEY_LEARING_TITLE_ROW4").val($("#_KEY_LEARING_TITLE_ROW4").val());
		$("#KEY_LEARING_DESC_ROW4").val($("#_KEY_LEARING_DESC_ROW4").val());
		
		//Appendixes
		$("#APPENDIXES_URL_ROW1").val($("#_APPENDIXES_URL_ROW1").val());
		$("#APPENDIXES_DESC_ROW1").val($("#_APPENDIXES_DESC_ROW1").val());
		$("#APPENDIXES_DATE_ROW1").val($("#_APPENDIXES_DATE_ROW1").val());
		$("#APPENDIXES_URL_ROW2").val($("#_APPENDIXES_URL_ROW2").val());
		$("#APPENDIXES_DESC_ROW2").val($("#_APPENDIXES_DESC_ROW2").val());
		$("#APPENDIXES_DATE_ROW2").val($("#_APPENDIXES_DATE_ROW2").val());
		$("#APPENDIXES_URL_ROW3").val($("#_APPENDIXES_URL_ROW3").val());
		$("#APPENDIXES_DESC_ROW3").val($("#_APPENDIXES_DESC_ROW3").val());
		$("#APPENDIXES_DATE_ROW3").val($("#_APPENDIXES_DATE_ROW3").val());
		$("#APPENDIXES_URL_ROW4").val($("#_APPENDIXES_URL_ROW4").val());
		$("#APPENDIXES_DESC_ROW4").val($("#_APPENDIXES_DESC_ROW4").val());
		$("#APPENDIXES_DATE_ROW4").val($("#_APPENDIXES_DATE_ROW4").val());
		
		var timeline_last_row = $("#timeline_last_row").val();
		var description = "";
		for(i=1;i<=timeline_last_row;i++)
		{
			$("#TLROW" + i).val($("#_TLROW" + i).val());
			$("#TLDATE" + i).val($("#_TLDATE" + i).val());
			$("#TLTIME" + i).val($("#_TLTIME" + i).val());
			$("#TLDESCRIPTION" + i).val($("#_TLDESCRIPTION" + i).val());
			
			//if(i>20)
			//alert('TLDESCRIPTION' + i + ': ' + $("#TLDESCRIPTION" + i).val());
		}
	
	
		if($("#SIGNOFF_DATE_4_d").val() != "")
			setHiddenDateTime("SIGNOFF_DATE_4");
		if($("#SIGNOFF_DATE_5_d").val() != "")
			setHiddenDateTime("SIGNOFF_DATE_5");
		if($("#SIGNOFF_DATE_6_d").val() != "")
			setHiddenDateTime("SIGNOFF_DATE_6");
	
	
		
	}
	
	function setFieldsForNewInvestigation()
	{
		var incidentDisplayId = '<?php echo($incident_displayid); ?>';
		var incidentLocation = '<?php echo($incident_location); ?>';
		var incidentTitle = '<?php echo($incident_title); ?>';
		var risk_likelihood_actual = '<?php echo($risk_likelihood_actual); ?>';
		var risk_consequence_actual = '<?php echo($risk_consequence_actual); ?>';
		var risk_rating_actual = '<?php echo($risk_rating_actual); ?>';
		
		var risk_likelihood_potential = "<?php echo isset($risk_likelihood_potential)?($risk_likelihood_potential):''; ?>";
		var risk_consequence_potential = "<?php echo isset($risk_consequence_potential)?($risk_consequence_potential):''; ?>";
		var risk_rating_potential = "<?php echo isset($risk_rating_potential)?($risk_rating_potential):''; ?>";

		var injured_person = '<?php echo($injured_person); ?>';
		var primary_person_contractor_name = '<?php echo($primary_person_contractor_name); ?>';
		var witness_1 = '<?php echo($witness_1); ?>';
		var witness_2 = '<?php echo($witness_2); ?>';
		
		
		if(incidentDisplayId != "")
			$("#INCDISPLYID").val(incidentDisplayId);
		if(incidentTitle != "")
			$("#LOCATION").val(incidentLocation);
		if(incidentTitle != "")
			$("#INCIDENT_TITLE").val(incidentTitle);
		if(risk_likelihood_actual != "")
			$("#RISK_LIKELIHOOD_ACTUAL").val(risk_likelihood_actual);
		if(risk_consequence_actual != "")
			$("#RISK_CONSEQUENCE_ACTUAL").val(risk_consequence_actual);
		if(risk_rating_actual != "")
			$("#RISK_RATING_ACTUAL").val(risk_rating_actual);

		if(risk_likelihood_potential != "")
			$("#RISK_LIKELIHOOD_POTENTIAL").val(risk_likelihood_potential);
		if(risk_consequence_potential != "")
			$("#RISK_CONSEQUENCE_POTENTIAL").val(risk_consequence_potential);
		if(risk_rating_potential != "")
			$("#RISK_RATING_POTENTIAL").val(risk_rating_potential);			
			
		//if(injured_person != "")
		//	$("#PRIMARYPERSON").val(primary_person);			
		//if(primary_person_contractor_name != "")
		//	$("#PRIMARY_PERSON_CONTRACTOR_NAME").val(primary_person_contractor_name);			
		if(witness_1 != "")
			$("#WITNESS_1").val(witness_1);			
		if(witness_2 != "")
			$("#WITNESS_2").val(witness_2);			
			
	}

	function isWorkflowStepValid(signoffBy, forwardTo, msg)
	{
		if(signoffBy == "")
		{
			alert("Please set Sign-off Incident Report By");
			return false;
		}
//		if(forwardTo == "")
//		{
//			alert("Please set Forward to");
//			return false;
//		}
		if(msg == "")
		{
			alert("Please set the Approval/Rejection message");
			return false;
		}
		return true;
	}

	function isWorkflowStepValidForIcam(signoffBy, forwardTo, msg)
	{
		if(signoffBy == "")
		{
			alert("Please set Sign-off ICAM Report By");
			return false;
		}
//		if(forwardTo == "")
//		{
//			alert("Please set Forward to");
//			return false;
//		}
		if(msg == "")
		{
			alert("Please set the Approval/Rejection message");
			return false;
		}
		return true;
	}
	
	function openCodesWhenDataExist()
	{

		if($("#DF_CODEROW1").val() != '' || $("#DF_CODE_FACTOR_ROW1").val() != '' ||
			$("#DF_CODEROW2").val() != '' || $("#DF_CODE_FACTOR_ROW2").val() != '' ||
			$("#DF_CODEROW3").val() != '' || $("#DF_CODE_FACTOR_ROW3").val() != '' ||
			$("#DF_CODEROW4").val() != '' || $("#DF_CODE_FACTOR_ROW4").val() != '' ||
			$("#DF_CODEROW5").val() != '' || $("#DF_CODE_FACTOR_ROW5").val() != '' ||
			$("#DF_CODEROW6").val() != '' || $("#DF_CODE_FACTOR_ROW6").val() != '' ||
			$("#DF_CODEROW7").val() != '' || $("#DF_CODE_FACTOR_ROW7").val() != '' ||
			$("#DF_CODEROW8").val() != '' || $("#DF_CODE_FACTOR_ROW8").val() != '')
		{
			$('#DFCodeBoxDiv1').show();
		}
		if($("#IT_CODEROW1").val() != '' || $("#IT_CODE_FACTOR_ROW1").val() != '' ||
			$("#IT_CODEROW2").val() != '' || $("#IT_CODE_FACTOR_ROW2").val() != '' ||
			$("#IT_CODEROW3").val() != '' || $("#IT_CODE_FACTOR_ROW3").val() != '' ||
			$("#IT_CODEROW4").val() != '' || $("#IT_CODE_FACTOR_ROW4").val() != '')
		{
			$('#ITCodeBoxDiv1').show();
		}
		if($("#WF_CODEROW1").val() != '' || $("#WF_CODE_FACTOR_ROW1").val() != '' ||
			$("#WF_CODEROW2").val() != '' || $("#WF_CODE_FACTOR_ROW2").val() != '' ||
			$("#WF_CODEROW3").val() != '' || $("#WF_CODE_FACTOR_ROW3").val() != '' ||
			$("#WF_CODEROW4").val() != '' || $("#WF_CODE_FACTOR_ROW4").val() != '')
		{
			$('#WFCodeBoxDiv1').show();
		}
		if($("#HF_CODEROW1").val() != '' || $("#HF_CODE_FACTOR_ROW1").val() != '' ||
			$("#HF_CODEROW2").val() != '' || $("#HF_CODE_FACTOR_ROW2").val() != '' ||
			$("#HF_CODEROW3").val() != '' || $("#HF_CODE_FACTOR_ROW3").val() != '' ||
			$("#HF_CODEROW4").val() != '' || $("#HF_CODE_FACTOR_ROW4").val() != '')
		{
			$('#HFCodeBoxDiv1').show();
		}	
		if($("#OF_CODEROW1").val() != '' || $("#OF_CODE_FACTOR_ROW1").val() != '' ||
			$("#OF_CODEROW2").val() != '' || $("#OF_CODE_FACTOR_ROW2").val() != '' ||
			$("#OF_CODEROW3").val() != '' || $("#OF_CODE_FACTOR_ROW3").val() != '' ||
			$("#OF_CODEROW4").val() != '' || $("#OF_CODE_FACTOR_ROW4").val() != '')
		{
			$('#OFCodeBoxDiv1').show();
		}
//		if($("#DATEACTIONROW1").val() != '' || $("#TIMEACTIONROW1").val() != '' || $("#ACTIONROW1").val() != '' ||
//			$("#DATEACTIONROW2").val() != '' || $("#TIMEACTIONROW2").val() != '' || $("#ACTIONROW2").val() != '' ||
//			$("#DATEACTIONROW3").val() != '' || $("#TIMEACTIONROW3").val() != '' || $("#ACTIONROW3").val() != '' ||
//			$("#DATEACTIONROW4").val() != '' || $("#TIMEACTIONROW4").val() != '' || $("#ACTIONROW4").val() != '' ||
//			$("#DATEACTIONROW5").val() != '' || $("#TIMEACTIONROW5").val() != '' || $("#ACTIONROW5").val() != '' ||
//			$("#DATEACTIONROW6").val() != '' || $("#TIMEACTIONROW6").val() != '' || $("#ACTIONROW6").val() != '' ||
//			$("#DATEACTIONROW7").val() != '' || $("#TIMEACTIONROW7").val() != '' || $("#ACTIONROW7").val() != '' ||
//			$("#DATEACTIONROW8").val() != '' || $("#TIMEACTIONROW8").val() != '' || $("#ACTIONROW8").val() != '' ||
//			$("#DATEACTIONROW9").val() != '' || $("#TIMEACTIONROW9").val() != '' || $("#ACTIONROW9").val() != '' ||
//			$("#DATEACTIONROW10").val() != '' || $("#TIMEACTIONROW10").val() != '' || $("#ACTIONROW10").val() != '')
//		{
//			$('#SequenceOfEventsGroupSubDIV').show();
//		}

		
		$(".bfield").each(function(index){
			//alert('chbx id: ' + this.id);
			if(this.id.indexOf("DF_") > -1 && $(this).is(":checked"))
			{
				$('#DFCodeBoxDiv1').show();
			}		
			if(this.id.indexOf("IT_") > -1 && $(this).is(":checked"))
			{
				$('#ITCodeBoxDiv1').show();
			}	
			if(this.id.indexOf("WF_") > -1 && $(this).is(":checked"))
			{
				$('#WFCodeBoxDiv1').show();
			}	
			if(this.id.indexOf("HF_") > -1 && $(this).is(":checked"))
			{
				$('#HFCodeBoxDiv1').show();
			}
			if((this.id.indexOf("HW[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("TR[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("OR[") > -1 && $(this).is(":checked")) ||
				(this.id.indexOf("CO[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("IG[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("PR[") > -1 && $(this).is(":checked")) ||
				(this.id.indexOf("MM[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("DE[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("RM[") > -1 && $(this).is(":checked")) ||
				(this.id.indexOf("MC[") > -1 && $(this).is(":checked")) || 
				(this.id.indexOf("CM[") > -1 && $(this).is(":checked")) )
			{
				$('#OFCodeBoxDiv1').show();
			}
		});
		
		
		if($("#KEY_LEARING_TITLE_ROW1").val() != '' || $("#KEY_LEARING_DESC_ROW1").val() != '' ||
			$("#KEY_LEARING_TITLE_ROW2").val() != '' || $("#KEY_LEARING_DESC_ROW2").val() != '' ||
			$("#KEY_LEARING_TITLE_ROW3").val() != '' || $("#KEY_LEARING_DESC_ROW3").val() != '' ||
			$("#KEY_LEARING_TITLE_ROW4").val() != '' || $("#KEY_LEARING_DESC_ROW4").val() != '')
		{
			$('#KeyLearningBoxDiv1').show();
		}

		if($("#APPENDIXES_URL_ROW1").val() != '' || $("#APPENDIXES_DESC_ROW1").val() != '' || $("#APPENDIXES_DATE_ROW1").val() != '' ||
			$("#APPENDIXES_URL_ROW2").val() != '' || $("#APPENDIXES_DESC_ROW2").val() != '' || $("#APPENDIXES_DATE_ROW2").val() != '' ||
			$("#APPENDIXES_URL_ROW3").val() != '' || $("#APPENDIXES_DESC_ROW3").val() != '' || $("#APPENDIXES_DATE_ROW3").val() != '' ||
			$("#APPENDIXES_URL_ROW4").val() != '' || $("#APPENDIXES_DESC_ROW4").val() != '' || $("#APPENDIXES_DATE_ROW4").val() != '' 
			)
		{
			$('#AppendicesBoxDiv1').show();
		}	
		
	
		
	}
	
	function openCodes(chkbxId)
	{
		//alert('id: ' + chkbxId);
		if(chkbxId.indexOf("DF_") > -1)
		{
			$('#DFCodeBoxDiv1').show();
		}
		else if(chkbxId.indexOf("IT_") > -1)
		{
			$('#ITCodeBoxDiv1').show();
		}
		else if(chkbxId.indexOf("WF_") > -1)
		{
			$('#WFCodeBoxDiv1').show();
		}
		else if(chkbxId.indexOf("HF_") > -1)
		{
			$('#HFCodeBoxDiv1').show();
		}
		else if((chkbxId.indexOf("HW[") > -1) || (chkbxId.indexOf("TR[") > -1) || (chkbxId.indexOf("OR[") > -1)
				|| (chkbxId.indexOf("CO[") > -1) || (chkbxId.indexOf("IG[") > -1) || (chkbxId.indexOf("PR[") > -1)
				|| (chkbxId.indexOf("MM[") > -1) || (chkbxId.indexOf("DE[") > -1) || (chkbxId.indexOf("RM[") > -1)
				|| (chkbxId.indexOf("MC[") > -1) || (chkbxId.indexOf("CM[") > -1))
		{
			$('#OFCodeBoxDiv1').show();
		}
	}
	
	
	function clearWFLfields(field)
	{
	
		
		if(field == 'FORWARD_PERSON_ID_4') //iCAM
		{
			$("#SIGNOFF_PERSON_ID_4").val("");
			$("#SIGNOFF_PERSON_ID_4_text").val("");
			$("#SIGNOFF_DATE_4").val("");
			$("#FORWARD_PERSON_ID_4_text").val("");
			$("#FORWARD_PERSON_ID_4").val("");
			$("#MESSAGE_4").val("");
			$("#STATUS_4").val("");
			$("#STATUS_4").removeClass("textLow");
			$("#STATUS_4").removeClass("textVeryHigh");
		}
		else if(field == 'FORWARD_PERSON_ID_5') //iCAM
		{
			$("#SIGNOFF_PERSON_ID_5").val("");
			$("#SIGNOFF_PERSON_ID_5_text").val("");
			$("#SIGNOFF_DATE_5").val("");
			$("#FORWARD_PERSON_ID_5_text").val("");
			$("#FORWARD_PERSON_ID_5").val("");
			$("#MESSAGE_5").val("");
			$("#STATUS_5").val("");
			$("#STATUS_5").removeClass("textLow");
			$("#STATUS_5").removeClass("textVeryHigh");
		}
		else if(field == 'FORWARD_PERSON_ID_6') //iCAM
		{
			$("#SIGNOFF_PERSON_ID_6").val("");
			$("#SIGNOFF_PERSON_ID_6_text").val("");
			$("#SIGNOFF_DATE_6").val("");
			$("#FORWARD_PERSON_ID_6_text").val("");
			$("#FORWARD_PERSON_ID_6").val("");
			$("#MESSAGE_6").val("");
			$("#STATUS_6").val("");
			$("#STATUS_6").removeClass("textLow");
			$("#STATUS_6").removeClass("textVeryHigh");
		}
	}

	function saveAndSend()
	{
		$("#saveButton").click();
	}

	 function openAudit()
	 {
 		window.open("<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/AuditReportInvestigations.aspx?investigationNumber=<?php echo($id); ?>");
	 }	
	
	function doNewEvent()
	{
		var _m_event=new _module("investigation_events","<?php echo $p;?>","<?php echo $id;?>",document);
		_m_event.newAction();
	}
	
	function getLastVisibleTimeline()
	{
		for(i=30;i>0;i--)
		{
			//alert(i);
			if($("#trTL" + i).is(":visible"))
			{
				return i;
			}
		}
	}
	
	function addNewEvent()
	{
		
		var lastVisible = getLastVisibleTimeline();
		//alert('LastVisible (getLastVisibleTimeline): ' + lastVisible);
		//alert('trTL1: ' +  $("#trTL1").is(":visible"));
		//alert('trTL2: ' +  $("#trTL2").is(":visible"));
		if(lastVisible < 30)
		{
			lastVisible++;
			$("#timeline_last_row").val(lastVisible);
			$("#trTL" + lastVisible).show();
			
			//$("#TLROW" + lastVisible).val(999);
			//	alert('SETTING Last visible: ' + lastVisible);
		}else{
			alert("Sorry, but there's a defined system limit of 30 events. Please do it outside of CATS and attach as a document below.");
		}
		
		//alert('timeline_last_row: ' + $("#timeline_last_row").val());
		
	}
	
</script>

