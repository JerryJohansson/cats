<?php

require_once ("../../conf.php");

require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'js_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'mail_functions.php');

require_once (CATS_CLASSES_PATH.'users.php');

require_once (CATS_ADODB_PATH.'adodb.inc.php');



  echo 'postApprovalRejection:' . '<br/>';

  echo 'baseDir: ' . $baseDir . '<br/>';
  echo 'CATS_FUNCTIONS_PATH: ' . CATS_FUNCTIONS_PATH  . '<br/>';
  
  
  echo 'INCIDENT_NUMBER: ' . $_POST["INCIDENT_NUMBER"]  . '<br/>';
  echo 'INVESTIGATION_NUMBER: ' . $_POST["INVESTIGATION_NUMBER"]  . '<br/>';
  
  echo 'APPROVAL_STATUS: ' . $_POST["APPROVAL_STATUS"]  . '<br/>';
  echo 'WFL_STAGE: ' . $_POST["WFL_STAGE"]  . '<br/>';
  echo 'SIGN_OFF_BY: ' . $_POST["SIGN_OFF_BY"]  . '<br/>';
  echo 'SIGN_OFF_DATE_TIME: ' . $_POST["SIGN_OFF_DATE_TIME"]  . '<br/>';
  echo 'FORWARD_TO: ' . $_POST["FORWARD_TO"]  . '<br/>';
  echo 'MESSAGE: ' . $_POST["MESSAGE"]  . '<br/>';

  

	$config = parse_ini_file("../../conf/" . $_SERVER['SERVER_NAME'] . "/cats_admin.ini");

	foreach ($config as $key => $value) {

		if(!defined($key))

			define($key, $value);

	}

    $conn = OCILogon(DB_USERNAME, DB_PASSWORD, "dw2tst.tiwest.com.au/" . DB_DATABASE);

	
	$stage = $_POST["WFL_STAGE"];
	
	
	///////////////////////
	$emailToAlert = "";
	$firstNameToAlert = "";
	$lastNameToAlert = "";
	$firstNameFrom = "";
	$lastNameFrom = "";
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $_POST["FORWARD_TO"]);
	while($tab = $rs->FetchRow()){

		$emailToAlert = $tab['EMAIL_ADDRESS'];	
		$firstNameToAlert = $tab['FIRST_NAME'];
		$lastNameToAlert = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $_POST["SIGN_OFF_BY"]);
	while($tab = $rs->FetchRow()){

		$firstNameFrom = $tab['FIRST_NAME'];
		$lastNameFrom = $tab['LAST_NAME'];
	}
	
	
	
	echo 'Dear ' . $emailToAlert . ' ' . $firstNameToAlert . ' ' . $lastNameToAlert . '. Approved / rejected by ' . $firstNameFrom . ' ' . $lastNameFrom . ' ';
	///////////////////////
	
	// Update investigation with the sign off values
	$sql = "update CATSDBA.TBLINVESTIGATION_DETAILS set SIGNOFF_PERSON_ID_" . $stage . " = " . $_POST["SIGN_OFF_BY"] . ", FORWARD_PERSON_ID_" . $stage . " = " . $_POST["FORWARD_TO"] . ", MESSAGE_" . $stage . " = '" . $_POST["MESSAGE"] . "', STATUS_" . $stage . " = '" .$_POST["APPROVAL_STATUS"] . "', SIGNOFF_DATE_" . $stage . " = TO_TIMESTAMP('" . $_POST["SIGN_OFF_DATE_TIME"] . "','RRRR-MM-DD HH24:MI') WHERE INVESTIGATION_NUMBER = " . $_POST["INVESTIGATION_NUMBER"];
    echo "Stage:" . $stage;
    echo "SQL: " . $sql;
	$stmt = OCIParse($conn, $sql);
    OCIExecute($stmt);

	
    OCIFreeStatement($stmt);  

    OCILogoff($conn);	
	
	email_wfl_approval_rejection_investigation($_POST);
	
	sleep(1);
	
	
?>

<script>
	//top.show_screen('dashboard','search');
	//window.location.href = "/";
</script>
