<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');

$severityRating = "";

//$db->debug=true;
if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;

//update_consequence_ranking("TBLINVESTIGATION_DETAILS", $id, $_POST);




$reqString = "";
	foreach ($_POST as $key => $value)
		$reqString .= $key.'='.$value.'   ' ;
	$reqString .= '    action: ' . $action; 

switch($action){
	case "add": // Add record

		//$db->debug=true;
		echo("<br><b>id=".$id."</b><br>");
		echo("<br><b>incident_number=".$_POST['INCIDENT_NUMBER']."</b><br>");

		// check overlapping Incident ID
		$sql = "SELECT COUNT(*) FROM tblinvestigation_details WHERE incident_number = '".$_POST['INCIDENT_NUMBER']."'";
		if (db_get_one($sql) == 0) {
			// Do the Insert
			$editor = new Editor($m, $p, $a);
			$ret = $editor->insert();

			$id=$db->Insert_ID("investigation_details_seq");
			echo("<br><b>id=".$id."</b><br>");

			// Now Update the Timestamp Fields
			//if($ret == true){
				//print_r($_POST);
				$ret = update_timestamps("tblinvestigation_details", $id);
				update_audit_new("tblinvestigation_details_audit", $id);
				update_consequence_ranking_in_incident();
				saveTimelineEvents($id);
				
				//Email for approval/rejection workflow of incident or icam report
				if($_POST['DO_EMAIL_FLAG_1'] == 'True' || $_POST['DO_EMAIL_FLAG_2'] == 'True' || $_POST['DO_EMAIL_FLAG_3'] == 'True' || $_POST['DO_EMAIL_FLAG_4'] == 'True' || $_POST['DO_EMAIL_FLAG_5'] == 'True' || $_POST['DO_EMAIL_FLAG_6'] == 'True')
					email_wfl_approval_rejection_investigation($_POST, $id);
				
				
			//}
			

		} else {
			$ret = false;
			$_SESSION['messageStack']->add("An Investigation with Incident Number ".$_POST['INCIDENT_NUMBER']." already exists.");
		}

/*		if(empty($_POST['INCDISPLYID']) && $id > 0){
			// update INCDISPLYID if it wasn't specified
			$sql = "update tblincident_details set INCDISPLYID = '{$id}/".date('Y')."' where INCIDENT_NUMBER = $id ";
			$db->Execute($sql);
		}
*/
		//if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
			$ret = update_documents_sequence($id,"incident_details");
				if($ret == false){
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				}

		if($ret)
			$return_js_function = "location.href='index.php?m=$m&p=edit&id=$id';";

		break;
	case "edit": // Update node
		//$db->debug=true;
	//email_to_jerry($POST, "DEBUG", 'ret: ' . $ret . ' a) id:' . $id);			
		
		if($id>0){


		
		
			$sql = "SELECT INVESTIGATION_NUMBER FROM tblinvestigation_details WHERE INVESTIGATION_NUMBER = '".$id."'";
			if (db_get_one($sql) == $id) {
				$editor = new Editor($m, $p, $a);
				$ret = ($editor->update($id) > 0);
	//email_to_jerry($POST, "DEBUG", 'ret: ' . $ret . ' b) id:' . $id);			

				// Now Update the Timestamp Fields
				//if($ret == true){
	//email_to_jerry($POST, "DEBUG", 'ret: ' . $ret . ' c) id:' . $id);			
					//print_r($_POST);
					saveTimelineEvents($id);

					$ret = update_timestamps("tblinvestigation_details", $id);
					update_audit_edit("tblinvestigation_details_audit", $id);
					update_consequence_ranking_in_incident();
				//}
			

			} else {
				$ret = false;
				$_SESSION['messageStack']->add("Another Investigation with Incident Number".$_POST['INCIDENT_NUMBER']." already exists.");
			}
		}else{
			$ret = false;
			$_SESSION['messageStack']->add("The Operation failed because there was no ID.");
		}
			//email_to_jerry($POST, "DEBUG", 'ret: ' . $ret . ' d) id:' . $id);			
		//if($ret)
		//	email_to_jerry($POST, "DEBUG", 'ret: ' . $ret . ' e) id:' . $id);			

			//print_r($_POST);
	
	
			
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
			
//Email for approval/rejection workflow of incident or icam report
if($_POST['DO_EMAIL_FLAG_1'] == 'True' || $_POST['DO_EMAIL_FLAG_2'] == 'True' || $_POST['DO_EMAIL_FLAG_3'] == 'True' || $_POST['DO_EMAIL_FLAG_4'] == 'True' || $_POST['DO_EMAIL_FLAG_5'] == 'True' || $_POST['DO_EMAIL_FLAG_6'] == 'True')
	email_wfl_approval_rejection_investigation($_POST, $id);
			
			//$ret = update_documents_sequence($id,"incident_details");
				if($ret == false){
					$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'"; 
				}

		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		if($ret = $editor->delete($id)){
			$sql_del="delete from " . $TABLES['CHECKBOXES']['table'] . " where " . $TABLES['CHECKBOXES']['id'] . " = $id ";
			$ret = db_query($sql_del);
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Incident records deleted successfully.","success");
						// now delete the action category checkbox values using the same delete array
						if($ret = db_bulk_delete($TABLES['CHECKBOXES']['table'],$TABLES['CHECKBOXES']['id'],$delete)){
							$_SESSION['messageStack']->add("Incident checkbox values deleted successfully.","success");
						}
					}
					if(!$ret) $_SESSION['messageStack']->add("Errors occurred while deleting incidents.<br>".$db->ErrorMsg());
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");
		$ret = false;
		break;
}
if($db->debug==true) exit;
if($ret) {
	if(isset($_POST['rm']) && !empty($_POST['rm'])){
		main_redirect("index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}");
	}
	include(CATS_INCLUDE_PATH . 'success.inc.php'); 
	
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
/*
+------------------------------------------------------------------------
|  Functions
+------------------------------------------------------------------------
*/
// function update_consequence_ranking($table, $id, $_POST){ comment dev
function update_consequence_ranking($table, $id, $postData){
	global $db;
	$ret = true;



	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET RISK_LIKELIHOOD_ACTUAL='".$postData['RISK_LIKELIHOOD_ACTUAL']."', RISK_CONSEQUENCE_ACTUAL='".$postData['RISK_CONSEQUENCE_ACTUAL']."', ";
	$update_sql .= "  RISK_RATING_ACTUAL='".$postData['RISK_RATING_ACTUAL']."', RISK_LIKELIHOOD_POTENTIAL='".$postData['RISK_LIKELIHOOD_POTENTIAL']."', ";
	$update_sql .= "  RISK_CONSEQUENCE_POTENTIAL='".$postData['RISK_CONSEQUENCE_POTENTIAL']."', RISK_RATING_POTENTIAL='".$postData['RISK_RATING_POTENTIAL']."' ";
	$update_sql .= " WHERE incident_number=".$id;

	//$reqString = "";
	//foreach ($_POST as $key => $value)
    //$reqString .= $key.'='.$value.'\n';
	
	//email_to_jerry($POST, "DEBUG", 'Investigation\post DEBUG: ' . $reqString);	
	
	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

function update_timestamps($table, $id){
	global $db;
	$ret = true;

	//email_to_jerry($POST, "DEBUG", 'update_timestamps: ' .  $$table . ' ' . $id);						


	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET signoff_date_4=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_4']."','YYYY-MM-DD HH24:MI '), signoff_date_5=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_5']."','YYYY-MM-DD HH24:MI '), signoff_date_6=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_6']."','YYYY-MM-DD HH24:MI ')";
	$update_sql .= " WHERE investigation_number=".$id;

	//email_to_jerry($POST, "DEBUG", 'update_sql: ' .  $update_sql);						

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}
function update_audit_new($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE investigation_number=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}
function update_audit_edit($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE audit_id >= (select max(audit_id) -1 from ".$table.") and investigation_number=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

function update_consequence_ranking_in_incident()
{
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE TBLINCIDENT_DETAILS ";
	$update_sql .= " SET RISK_LIKELIHOOD_POTENTIAL = '" . $_POST['RISK_LIKELIHOOD_POTENTIAL'] . "', RISK_CONSEQUENCE_POTENTIAL ='" . $_POST['RISK_CONSEQUENCE_POTENTIAL'] . "', RISK_RATING_POTENTIAL = '" .  $_POST['RISK_RATING_POTENTIAL'] . "' ";
	$update_sql .= " WHERE INCIDENT_NUMBER=".$_POST['INCIDENT_NUMBER'];

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;  
}

function update_documents_sequence($id,$type){
	global $TABLES,$db;
	$ret = true;
	// if action category exists then replace the checkbox values
	if(isset($_POST['DOCUMENTLOCATION'])){
		// replace linked records
		$sql_del="delete from " . $TABLES['DOCUMENTLOCATION']['table'] . " where FORMTYPE = '$type' and " . $TABLES['DOCUMENTLOCATION']['id'] . " = $id ";
		$success = db_query($sql_del);
	
		$arr=array();
		$act_cat = $_POST['DOCUMENTLOCATION'];
		$update = false;
		$docerr = false;
		foreach($act_cat as $key => $val){
			if(!empty($val)){
								
				if($val{0}!='\\' or $val{1}!='\\'){			
					$_SESSION['messageStack']->add("Document Location contains path '".$val."' which is an incorrectly formatted network path. The document must be located on the network.");
					$docerr = true;
				}else{
					$arr[] = array($id, $val, $type);
					$update = true;
				}
			}
		}
		if($update){
			// insert new checkbox values
			if($ret = db_bulk_insert($TABLES['DOCUMENTLOCATION']['table'],"REPORT_ID,DOCUMENTLOCATION,FORMTYPE",$arr,$db->debug)){
				$_SESSION['messageStack']->add("Supporting Documents were successfully updated.","success");
			}else{
				$_SESSION['messageStack']->add("An error occured while updating Supporting Document records.<br>".$db->ErrorMsg());
			}
		} // update records is true
		
		// If document path error then set return to false even though some records may have been updated
		if($docerr==true){
			$ret=false;
		}
		
	}
	return $ret;
}

function update_checkbox_values_sequence($tables, $id){
	global $db;
	$ret = true;

	
	// get checkbox values array so we can map
	// tblIncidentForm_Checkboxes.Checkbox_Type to tblIncident_CheckboxValues.CheckboxType
	$types = cats_get_checkboxes_values_array();
	// delete * checkbox values and insert the new values if they exist
	$ret = db_query("delete from {$tables['table']} where {$tables['id']}=$id ");
	// loop through types array and check if we have a matching array in the POST
	foreach($types as $key => $chktype){

	
		if(isset($_POST[$key]) && is_array($_POST[$key])){
			
			$checkbox = $_POST[$key];

			$arr=array();
			$update = false;
			// only add value to the array if it is not empty
			foreach($checkbox as $k => $chkid){
				//email_to_jerry($POST, "DEBUG", 'FOREACH $key isset ['. $key . ']: ' . isset($_POST[$key]) . ' $checkbox: ' . $checkbox . ' $chkid: ' . $chkid );						
				if(!empty($chkid)){
					$update = true;
					$arr[] = array($id, $chkid, $chktype);
					//email_to_jerry($POST, "DEBUG", 'FOREACH $key isset ['. $key . ']: ' . isset($_POST[$key]) . ' $checkbox: ' . $checkbox . ' $id: ' . $id . ' $chkid: ' . $chkid . ' $chktype: ' . $chktype . ' $tables[table]: ' . $tables['table'] );						
				}
			}
			if($update){
				// delete the checkbox values by type
				//$ret = db_query("delete from {$tables['table']} where CHECKBOXTYPE='$chktype' and {$tables['id']}=$id ");

				// insert new checkbox values
				if($ret = db_bulk_insert($tables['table'],$tables['fields'],$arr,$db->debug)){
					$_SESSION['messageStack']->add("Checkbox ($key) values were successfully updated.","success");
				}else{
					$_SESSION['messageStack']->add("An error occured while updating ($key) checkbox value records.<br>".$db->ErrorMsg());
				}
			} // update records is true
		}
	} // loop: checkbox values array
	return (bool)$ret;
}

function saveTimelineEvents($id)
{

	//echo 'saveTimelineEvents: ' . $id;
	
	$timeline_last_row = $_POST['timeline_last_row'];
	$sql_del="delete from TBLINVESTIGATION_TIMELINE where INVESTIGATION_NUMBER = " .  $id;
	$success = db_query($sql_del);

	for($i=1; $i <= 30; $i++) {

		$TLRow = $i; //$_POST['TLROW' . $i];
		$TLDate = $_POST['TLDATE' . $i];
		$TLDate = str_replace("'","", $TLDate);
		$TLDate = str_replace("–","-", $TLDate);
		$TLDate = str_replace("≈","ca", $TLDate);
		
		$TLTime = $_POST['TLTIME' . $i];
		$TLTime = str_replace("'","", $TLTime);
		$TLTime = str_replace("–","-", $TLTime);
		$TLTime = str_replace("≈","ca", $TLTime);
		
		$TLDescription = str_replace("'","", $_POST['TLDESCRIPTION' . $i]);
		$TLDescription = str_replace("–","-", $TLDescription);
		$TLDescription = str_replace("≈","ca", $TLDescription);
		
		
		
		
		if($TLDate != "" || $TLTime != "" || $TLDescription != "")
		{
			//echo 'TLRow: ' . $TLRow . ' TLDate: ' . $TLDate . ' TLTime: ' . $TLTime . ' TLDescription: ' . $TLDescription . ' id: ' . $id;
			$sql_insert="insert into TBLINVESTIGATION_TIMELINE (\"INVESTIGATION_NUMBER\", \"TL_ROWNO\", \"TL_DATE\", \"TL_TIME\", \"TL_DESCRIPTION\") VALUES (" . $id . ", " . $TLRow . ", '" . $TLDate . "', '" . $TLTime . "', '" . $TLDescription . "')";
			echo 'sql_insert: ' . $sql_insert;
			$success = db_query($sql_insert);
		}
		
			
		
	}	
	
}


// house cleaning to be done
$db=db_close();


?>