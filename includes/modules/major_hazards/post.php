<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

//$db->debug=true;

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;
switch($action){
	case "add": // Add record
		//$db->debug=true;
		$editor = new Editor($m, $p, $a);
		$ret = $editor->insert();
		$id=$db->Insert_ID("hazard_register_seq");
		echo("<br><b>id=".$id."</b><br>");

		if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
		
		if(ret)
			$return_js_function = "window.location.href='index.php?m=$m&p=edit&id=$id';";
		
		break;
	case "edit": // Update node
		//$db->debug=true;
		if($id>0){
			$editor = new Editor($m, $p, $a);
			$ret = ($editor->update($id) > 0);
		}else{
			$ret = false;
			$_SESSION['messageStack']->add("The Operation failed because there was no ID.");	
		}
		if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
		
		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		if($ret = $editor->delete($id)){
			$sql_del="delete from " . $TABLES['CHECKBOXES']['table'] . " where " . $TABLES['CHECKBOXES']['id'] . " = $id ";
			$ret = db_query($sql_del);
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("High Level Business Risk records deleted successfully.","success");
						// now delete the action category checkbox values using the same delete array
						if($ret = db_bulk_delete($TABLES['CHECKBOXES']['table'],$TABLES['CHECKBOXES']['id'],$delete)){
							$_SESSION['messageStack']->add("High Level Business Risk checkbox values deleted successfully.","success");
						}
					}
					if(!$ret) $_SESSION['messageStack']->add("Errors occurred while deleting High Level Business Risks.<br>".$db->ErrorMsg());
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");	
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");	
		$ret = false;
		break;
}
if($db->debug==true) exit;
if($ret) {
	if(isset($_POST['rm']) && !empty($_POST['rm'])){
		main_redirect("index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}");
	}
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
/*
+------------------------------------------------------------------------
|  Functions
+------------------------------------------------------------------------
*/
function update_checkbox_values_sequence($tables, $id){
	global $db;
	$ret = true;

	//add dev
	// $mappedFields = array();
	// if(isset($tables['fields'])){
	// 	$flds = explode(",",$tables['fields']);
	// 	for ($i = 0; $i < count($flds); $i++) {
	// 		array_push($mappedFields, strtolower($flds[$i])) ;
	// 	}
	// }
	//end dev
	
	// get checkbox values array so we can map 
	// tblIncidentForm_Checkboxes.Checkbox_Type to tblIncident_CheckboxValues.CheckboxType
	$types = cats_get_checkboxes_values_array();
	// delete * checkbox values and insert the new values if they exist
	$ret = db_query("DELETE FROM {$tables['table']} WHERE {$tables['id']}=$id ");
	// loop through types array and check if we have a matching array in the POST
	foreach($types as $key => $chktype){
		if(isset($_POST[$key]) && is_array($_POST[$key])){
			$checkbox = $_POST[$key];
			$arr=array();
			$update = false;
			// only add value to the array if it is not empty
			foreach($checkbox as $k => $chkid){
				if(!empty($chkid)){
					$update = true;
					$arr[] = array($id, $chkid, $chktype);//comment dev
					// $mappedValues = array();
					// $helparr= array($id, $chkid, $chktype);
					// if(!empty($mappedFields)){
					// 	for ($i = 0; $i < count($mappedFields); $i++) {
					// 		$mappedValues[strtolower($mappedFields[$i])] =  $helparr[$i];
							
					// 	}
					// }
					// array_push($arr, $mappedValues);
				}
			}
			if($update){
				// delete the checkbox values by type
				//$ret = db_query("delete from {$tables['table']} where CHECKBOXTYPE='$chktype' and {$tables['id']}=$id ");
				
				// insert new checkbox values
				if($ret = db_bulk_insert($tables['table'],$tables['fields'],$arr,$db->debug)){//comment dev
				// if($ret = db_bulk_insert_dev($tables['table'],$tables['fields'],$arr,$db->debug)){
					$_SESSION['messageStack']->add("Checkbox ($key) values were successfully updated.","success");
				}else{
					$_SESSION['messageStack']->add("An error occured while updating ($key) checkbox value records.<br>".$db->ErrorMsg());
				}
			} // update records is true
		}
	} // loop: checkbox values array
	return (bool)$ret;
}

// house cleaning to be done
$db=db_close();
?>