<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit High Level Business Risk',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New High Level Business Risk',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search High Level Business Risks',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLHAZARDREGISTER', 
					'id'=>'HAZARD_ID',
					'view'=>'VIEW_HAZARDREGISTER_DETAILS',
					'CHECKBOXES'=> array(
						'table'=>'TBLHAZARD_CHECKBOXVALUES',
						'id'=>'HAZARDID', 
						'fields'=>'HAZARDID,CHECKBOXID,CHECKBOXTYPE'
					)
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLHAZARDREGISTER', 
					'id'=>'HAZARD_ID',
					'view'=>'VIEW_HAZARDREGISTER_DETAILS',
					'filter'=>' 1=2 ',
					'CHECKBOXES'=> array(
						'table'=>'TBLHAZARD_CHECKBOXVALUES',
						'id'=>'HAZARDID', 
						'fields'=>'HAZARDID,CHECKBOXID,CHECKBOXTYPE'
					)
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'table'=>'TBLHAZARDREGISTER', 
					'id'=>'HAZARD_ID',
					'view'=>'VIEW_HAZARDREGISTER_DETAILS',
					'CHECKBOXES'=> array(
						'table'=>'TBLHAZARD_CHECKBOXVALUES',
						'id'=>'HAZARDID', 
						'fields'=>'HAZARDID,CHECKBOXID,CHECKBOXTYPE'
					)
				)
			);
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLHAZARDREGISTER', 
					'id'=>'HAZARD_ID',
					'view'=>'VIEW_HAZARDREGISTER_DETAILS',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();
	switch($page){
		case 'view':
			$ret = array_merge($ret, 
				array(
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'PROCESS_TYPE' => array('label'=>'Process', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'DATEANALYSED' => array('label'=>'Date Analysed', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'Hazard_Category' => array('label'=>'Category', 'value'=>'', 'func'=>'html_draw_hazard_category_display', 'col'=>1, 'col_span'=>2), 
					'HAZARD_TYPE' => array('label'=>'Primary Source', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'EVENT' => array('label'=>'Risk Statement', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'IMPACT' => array('label'=>'Impact', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'CURRENT_CONTROL' => array('label'=>'Current Control Measures', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'RISK_MEMBERS' => array('label'=>'Risk Team Members', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'Initial_Risk' => array('label'=>'Initial Risk Values', 'value'=>'', 'func'=>'html_display_initial_risk_table', 'col'=>1, 'col_span'=>2),
					'Current_Risk' => array('label'=>'Current Risk Values', 'value'=>'', 'func'=>'html_display_current_risk_table', 'col'=>1, 'col_span'=>2),
					'RISK_REMOVED' => array('label'=>'', 'value'=>0, 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'DATE_RISKREMOVED' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'COMMENTS' => array('label'=>'Recommendations Comments', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
				)
			);	
			break;
		case 'new':
		case 'edit':			
			$ret = array_merge($ret, 
				array(
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
					'PROCESS_ID' => array('label'=>'Process', 'value'=>NULL, 'func'=>'html_form_draw_major_hazard_process_dd', 'col'=>1, 'col_span'=>2), 
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
					'DATEANALYSED' => array('label'=>'Date Analysed', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'Hazard_Category' => array('label'=>'Category', 'value'=>'', 'func'=>'html_draw_hazard_category_checkboxset', 'col'=>1, 'col_span'=>2), 
					'HAZARDTYPE_ID' => array('label'=>'Primary Source', 'value'=>NULL, 'func'=>'html_form_draw_major_hazard_type_dd', 'col'=>1, 'col_span'=>2), 
					'EVENT' => array('label'=>'Risk Statement', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
					'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'IMPACT' => array('label'=>'Impact', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'CURRENT_CONTROL' => array('label'=>'Current Control Measures', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'RISK_MEMBERS' => array('label'=>'Risk Team Members', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'Initial_Risk' => array('label'=>'Initial Risk Values', 'value'=>'', 'func'=>'html_draw_initial_risk_table', 'col'=>1, 'col_span'=>2),
					'Current_Risk' => array('label'=>'Current Risk Values', 'value'=>'', 'func'=>'html_draw_current_risk_table', 'col'=>1, 'col_span'=>2),
					'COMMENTS' => array('label'=>'Recommendations Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),

					'INIRISK_0_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_0_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_0_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'INIRISK_0_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'INIRISK_1_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_1_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),  
					'INIRISK_1_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'INIRISK_1_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_2_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_2_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_2_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'INIRISK_2_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_3_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_3_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_3_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'INIRISK_3_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_4_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_4_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_4_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'INIRISK_4_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'INIRISK_OR' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'INIRISK_OC' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					
					'RESRISK_0_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_0_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_0_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'RESRISK_0_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_1_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_1_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_1_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'RESRISK_1_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_2_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_2_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_2_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'RESRISK_2_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_3_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_3_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_3_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'RESRISK_3_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_4_R' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_4_L' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_4_C' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'RESRISK_4_NA' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'RESRISK_OR' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
//					'RESRISK_OC' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),

					'RISK_REMOVED' => array('label'=>'', 'value'=>0, 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2), 
					'DATE_RISKREMOVED' => array('label'=>'', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2) 
				)
			);	
			break;
		case 'search': 
		case 'results': 
			$ret = array(
				'HAZARD_ID' => array('label'=>'Hazard No.', 'operator'=>"IN", 'value'=>NULL, 'func'=>'html_draw_number_in_clause_field', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'PROCESS_ID' => array('label'=>'Process', 'value'=>NULL, 'func'=>'html_form_draw_major_hazard_process_dd', 'col'=>1, 'col_span'=>2), 
				'HAZARDTYPE_ID' => array('label'=>'Primary Source', 'value'=>NULL, 'func'=>'html_form_draw_major_hazard_type_dd', 'col'=>1, 'col_span'=>2), 
				'CATEGORY' => array('label'=>'Hazard Category', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_major_hazard_category_dd', 'col'=>1, 'col_span'=>2), 
				'RISK_REMOVED' => array('label'=>'Hazard Status', 'value'=>0, 'func'=>'html_form_draw_hazard_status_dd', 'col'=>1, 'col_span'=>2),
				'DATEANALYSED_FROM' => array('label'=>'Date Registered From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'DATEANALYSED_TO' => array('label'=>'Date Registered To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1) 
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'HAZARD_TYPE',"text" => 'HAZARD_TYPE');
	$ret[] = array("id" => 'EVENT',"text" => 'EVENT');
	$ret[] = array("id" => 'SITE_DESCRIPTION',"text" => 'SITE_DESCRIPTION');
	$ret[] = array("id" => 'PROCESS_TYPE',"text" => 'PROCESS_TYPE');
	$ret[] = array("id" => 'LOCATION',"text" => 'LOCATION');
	$ret[] = array("id" => 'DATEANALYSED',"text" => 'DATEANALYSED');
	return $ret;
}

if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>