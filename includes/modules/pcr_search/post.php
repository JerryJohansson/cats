<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');
// Fix page parameter
$p_refer = parse_url($_SERVER['HTTP_REFERER']);
parse_str($p_refer['query'],$q_refer);
$action_page = $q_refer['p'];

// Call the functions and store the results in the following variables
$TABLES  = getPropertiesTables($action_page);
$FIELDS  = getPropertiesFields($action_page);
//$db->debug=true;

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;
switch($action){
	case "add": // Add record
		// $db->debug=true;
		// insert has a trigger to also insert records into related tables:
		// tblpcr_initialreview, tblpcr_evaluation, tblpcr_areareview, tblpcr_pcrtreview, tblpcr_extension, tblpcr_actions, tblpcr_signoff, tblpcr_postaudit
		$editor = new Editor($m, $p, $a);
		$ret = $editor->insert();
		$id=$db->Insert_ID("tblpcr_details_seq");
		
		// if the insert was a success then go ahead and insert the rest of the pcr details
		// -- we need to create a record for each module that links to the main record by pcr_id
		// e.g. insert into TABLE[next_table(n)] (PCR_ID) values ($id)
		
		//$_SESSION['messageStack']->add("The Record was successfully added to {$this->module}.","success")

		if($ret){
			$ret = create_pcr_record($id);
			$ret = update_pcr_categories($id);//update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
			$ret = update_documents_sequence($id,'pcr_details');
			$ret = email_pcr_send_notification($action,$id);
		}
		
		$return_js_function = "top._edit_success('pcr_search','".$m."','view','view','index.php?m=pcr_search&p=view&id=".$id."')";
		
		break;
	case "edit": // Update node
		//$db->debug=true;
		// Stay on PCR screen instead of returning to search screen
		$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";

		if($id>0){
			$editor = new Editor($m, $p, $a);
			$ret = ($editor->update($id) > 0);
			
			if($ret){		
				// Perform Other Updates
				switch($action_page){
					case 'pcr_initial_review':
						$ret = update_pcr_reviewers($id, $action_page);
						break;
					case 'pcr_evaluation':
						$ret = update_pcr_evaluation($id,$action_page);
						$ret = update_pcr_wo_number($id,$action_page);
						$ret = update_documents_sequence($id,$action_page);					
						break;
					case 'pcr_area_review':
						$ret = update_pcr_area_review($id, $action_page);
						$ret = update_pcr_reviewers($id, $action_page);
						break;
					case 'pcr_pcrt_review':
						$ret = update_pcr_reviewers($id, $action_page);
						$ret = update_documents_sequence($id,$action_page);
						break;
					case 'pcr_extension':
						$ret = update_pcr_reviewers($id, $action_page);
						$ret = update_documents_sequence($id,$action_page);
						break;
					case 'pcr_actions':
						$ret = update_pcr_actions($id);
						break;
					case 'pcr_sign_off':
						$ret = update_pcr_signoff($id, $action_page);
						// $ret = update_pcr_reviewers($id, $action_page);
						$ret = update_documents_sequence($id,$action_page);
						break;
					case 'pcr_post_audit':
						$ret = update_documents_sequence($id,$action_page);
						break;
					default:
						$ret = update_pcr_categories($id);
						$ret = update_documents_sequence($id,$action_page);
						break;
				}
				
				// Update PCR Status
				$ret = update_pcr_status($id, $action_page);
				$ret = email_pcr_send_notification($action,$id);
			}
		}else{
			$ret = false;
			$_SESSION['messageStack']->add("The Operation failed because there was no ID.");	
		}
		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		if($ret = $editor->delete($id)){
			$tables = get_pcr_tables();
			foreach($tables as $t => $name){
				$sql_del="delete from $t where pcr_id = $id";
				
				if($ret = $db->Execute($sql_del)){
					// return the value returned from db execute
					$_SESSION['messageStack']->add("$name Tab deleted successfully.","success");
				}else{
					// this didn't work
					// TODO: add error logging
					$_SESSION['messageStack']->add("An error occured while deleting $name Tab.<br>Error: ".$db->ErrorMsg());
					return false;
				}
			} //END:: loop{$TABLES['ACTIONS']['table']}
			// delete pcr actions
			$sql_del="delete from " . $TABLES['ACTIONS']['table'] . " where " . $TABLES['ACTIONS']['id'] . " = $id ";
			if($ret = $db->Execute($sql_del)) $_SESSION['messageStack']->add($db->Affected_Rows()." PCR Actions deleted successfully.","success");
			else $_SESSION['messageStack']->add("An error occured while deleting PCR Actions.<br>Error: ".$db->ErrorMsg());
			// delete checkboxes
			$sql_del="delete from " . $TABLES['Reason_For_Request']['table'] . " where CHECKBOXTYPE = 'Reason_For_Request' and " . $TABLES['Reason_For_Request']['id'] . " = $id ";
			if($ret = $db->Execute($sql_del)) $_SESSION['messageStack']->add($db->Affected_Rows()." reason for request data deleted successfully.","success");
			else $_SESSION['messageStack']->add("An error occured while deleting reason for request data.<br>Error: ".$db->ErrorMsg());
			// delete documents
			$sql_del="delete from " . $TABLES['DOCUMENTLOCATION']['table'] . " where formtype in('pcr_extension','pcr_pcrt_review','pcr_sign_off','pcr_evaluation','pcr_details') and " . $TABLES['DOCUMENTLOCATION']['id'] . " = $id ";
			if($ret = $db->Execute($sql_del)) $_SESSION['messageStack']->add($db->Affected_Rows()." associated documents deleted successfully.","success");
			else $_SESSION['messageStack']->add("An error occured while deleting associated documents.<br>Error: ".$db->ErrorMsg());
			
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make sure we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}

					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Incident records deleted successfully.","success");
						// now delete the action category checkbox values using the same delete array
						if(!is_null($TABLES['CHECKBOXES']['table'])&& !is_null($TABLES['CHECKBOXES']['id'])){
						if($ret = db_bulk_delete($TABLES['CHECKBOXES']['table'],$TABLES['CHECKBOXES']['id'],$delete)){
							$_SESSION['messageStack']->add("Incident checkbox values deleted successfully.","success");
						}
						}
					}
					if(!$ret) $_SESSION['messageStack']->add("Errors occurred while deleting incidents.<br>".$db->ErrorMsg());
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");	
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");	
		$ret = false;
		break;
}
if($db->debug==true) exit;
if($ret) {
	if(isset($_POST['rm']) && !empty($_POST['rm'])){
		main_redirect("index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}");
	}
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
/*
+------------------------------------------------------------------------
|  Functions
+------------------------------------------------------------------------
*/
/*
+------------------------------------------------------------------------
|  Create a new pcr and all of its related records etc...
+------------------------------------------------------------------------
*/
function create_pcr_record($id){
	global $db;
	$ret = true;
	$tables = get_pcr_tables_array();
	$default_fields = 'AUTHORITYID_1,AUTHORITYID_2,AUTHORITYSTATUS_1,AUTHORITYSTATUS_2,AUTHORITYDATE_1,AUTHORITYDATE_2,AUTHORITYCOMMENT_1,AUTHORITYCOMMENT_2';
	foreach($tables as $t => $name){
		switch($t){
			case 'TBLPCR_INITIALREVIEW': case 'TBLPCR_SIGNOFF':
				$fields = $default_fields;
				break;
			case 'TBLPCR_EVALUATION':
				$fields = 'AUTHORITYID_1,AUTHORITYID_2,AUTHORITYSTATUS_1,AUTHORITYSTATUS_2,AUTHORITYDATE_1,AUTHORITYDATE_2,AUTHORITYCOMMENT_1,AUTHORITYCOMMENT_2,EVALUATION_NAME_ID';
				break;
			default:
				$fields = '';
				break;
		}
		// returns an array used for the update/insert
		$sql_data = array('PCR_ID'=>$id);
		if($fields!=''){
			$arr_fields = explode(",",$fields);
			foreach($arr_fields as $key){
				$sql_data[$key] = $_POST[$key];
			}
		}
		// sql used for dummy recordset object used in GetInsertSQL
		$sql = "select * from $t where 1=2";
		// get a recordset to use as the base of the insert statement
		$rs = $db->Execute($sql);
		// Create an insert statement based on the recordset($rs) and
		// sql data array($sql_data)
		$sql = $db->GetInsertSQL($rs,$sql_data);
		if($ret = $db->Execute($sql)){
			// return the value returned from db execute
			$_SESSION['messageStack']->add("$name Tab created successfully.","success");
		}else{
			// this didn't work
			// TODO: add error logging
			$_SESSION['messageStack']->add("An error occured while adding $name Tab.<br>Error: ".$db->ErrorMsg());
			return false;
		}
	} // END::loop
	return $ret;
}
function get_pcr_tables_array($all=false){
	//$tables = "TBLPCR_INITIALREVIEW,TBLPCR_EVALUATION,TBLPCR_AREAREVIEW,TBLPCR_PCRTREVIEW,TBLPCR_EXTENSION,TBLPCR_SIGNOFF,TBLPCR_POSTAUDIT";
	$tables = array(
		"TBLPCR_INITIALREVIEW"=>"Initial Review",
		"TBLPCR_EVALUATION"=>"Evaluation",
		"TBLPCR_AREAREVIEW"=>"Area Review",
		"TBLPCR_PCRTREVIEW"=>"PCRT Review",
		"TBLPCR_EXTENSION"=>"Extension",
		"TBLPCR_SIGNOFF"=>"Sign Off",
		"TBLPCR_POSTAUDIT"=>"Post Audit"
	);
	if($all) $tables=array_merge($tables,array("TBLPCR_DETAILS"=>"PCR Details"));
	return $tables;
}
/*
+------------------------------------------------------------------------
|  Update PCR Status
+------------------------------------------------------------------------
*/
function update_pcr_status($id, $page) {
	global $db, $TABLES;
	$ret = true;
	$table = "tblPCR_Details";
	
	// Get the existing status
	$fetch_sql = "SELECT overall_status FROM ".$table." WHERE pcr_id=".$id;
	$overall_status = db_get_one($fetch_sql);
	
	// Only update status if not Closed
	if($overall_status=='Closed')
		return $ret;
	
	switch($page){
		case 'pcr_initial_review':
			$process_step = 'Initial Review';
			$processstep_status = 'Performed';
			$overall_status = 'Open';
			break;
		case 'pcr_evaluation':
			$process_step = 'Evaluation';
			$processstep_status = 'Performed';
			$overall_status = 'Open';
			break;
		case 'pcr_area_review':
			$process_step = 'Area Review';
			$processstep_status = 'Performed';
			$overall_status = 'Open';
			break;
		case 'pcr_pcrt_review':
			$process_step = '';
			$processstep_status = '';
			$overall_status = 'Open';
			break;
		case 'pcr_extension':
			$process_step = 'Extension Review';
			$processstep_status = 'Performed';
			$overall_status = 'Open';
			break;
		case 'pcr_actions':
			$process_step = 'Actions';
			$processstep_status = 'Selected';
			$overall_status = 'Open';
			break;
		case 'pcr_sign_off':
			// Fetch Sign Off Value from Database
			$fetch_sql = "SELECT sign_off FROM tblpcr_signoff WHERE pcr_id=".$id;
			$sign_off = db_get_one($fetch_sql);
			
			// Set the Appropriate Status Values
			switch($sign_off){
				case 'Approved':
					$process_step = 'PCR Signed-Off';
					$processstep_status = 'Approved';
					$overall_status = 'Closed';
					break;
				case 'Cancelled':
					$process_step = 'PCR Signed-Off';
					$processstep_status = 'Cancelled';
					$overall_status = 'Closed';
					break;
				default:
					$process_step = 'PCR Signed-Off';
					$processstep_status = 'More Info Required';
					$overall_status = 'Open';
					break;
			}
			break;
		case 'pcr_post_audit':
			$process_step = 'Post Audit';
			$processstep_status = 'Complete - Post Audited';
			$overall_status = 'Open';		
			break;
		default:
			return $ret;
			break;
	}
	
	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET process_step='".$process_step."', processstep_status='".$processstep_status."', overall_status='".$overall_status."'";  
	$update_sql .= " WHERE pcr_id=".$id;
	
	// Do the Update
	$ret = $db->Execute($update_sql);
	
	// Return to the caller
	return $ret;
}

/*
+------------------------------------------------------------------------
|  Update Evaluation Details
+------------------------------------------------------------------------
*/
function update_pcr_evaluation($id, $page){
	global $db, $TABLES;
	$ret = true;
	$table = "tblPCR_Evaluation";
	
	// if assessment score exists then replace the values
	if(isset($_POST['ASSESSMENT_SCORE'])){
		$assessment_score = $_POST['ASSESSMENT_SCORE'];
		$assessment_level = $_POST['ASSESSMENT_LEVEL'];
		$evaluation_name_id = $_POST['EVALUATION_NAME_ID'];
		$evaluation_date = $_POST['EVALUATION_DATE'];
		$evaluation_comments = $_POST['EVALUATION_COMMENTS'];
		$area_reveiw_req = $_POST['AREA_REVIEW_REQ'];
		$team_review_req = $_POST['TEAM_REVIEW_REQ'];
		$ext_review_req = $_POST['EXT_REVIEW_REQ'];
		
		// Set the fields to insert
		$insert_fields = "PCR_ID,ASSESSMENT_SCORE,ASSESSMENT_LEVEL,EVALUATION_NAME_ID,EVALUATION_DATE,EVALUATION_COMMENTS,AREA_REVIEW_REQ,TEAM_REVIEW_REQ,EXT_REVIEW_REQ";
		
		// delete the records and re-insert with new values
		$ret = db_query("DELETE FROM $table WHERE pcr_id = $id");
		
		// define the value array
		$arr = array();
		$arr[] = array($id, $assessment_score, $assessment_level, $evaluation_name_id, $evaluation_date, $evaluation_comments, $area_reveiw_req, $team_review_req, $ext_review_req);
	
		// insert new values
		if($ret = db_bulk_insert($table, $insert_fields, $arr, $db->debug)){
			$_SESSION['messageStack']->add("Evaluation values were successfully updated.","success");
		}else{
			$_SESSION['messageStack']->add("An error occured while updating Evaluation values.<br>".$db->ErrorMsg());
		}
	}
	return $ret;
}
/*
+------------------------------------------------------------------------
|  Update WO Number
+------------------------------------------------------------------------
*/
function update_pcr_wo_number($id, $page){
	global $db, $TABLES;
	$ret = true;
	$table = "tblPCR_Details";
	
	// if assessment score exists then replace the values
	if(isset($_POST['WO_NUMBER'])){
		$wo_number = $_POST['WO_NUMBER'];
		
		// set the Update SQL
		$update_sql = "UPDATE ".$table;
		$update_sql .= " SET wo_number='".$wo_number."'";  
		$update_sql .= " WHERE pcr_id=".$id;
		
		// Do the Update
		if($ret = $db->Execute($update_sql)){
			$_SESSION['messageStack']->add("Parent PCR W/O No. successfully updated.","success");
		}else{
			$_SESSION['messageStack']->add("An error occured while updating Parent PCR W/O No.<br>".$db->ErrorMsg());
		}
	}
	return $ret;
}
/*
+------------------------------------------------------------------------
|  Update Sign Off Details
+------------------------------------------------------------------------
*/
function update_pcr_area_review($id, $page){
	global $db, $TABLES;
	$ret = true;
	$table = "tblPCR_AreaReview";

	if(isset($_POST['PCR_TYPE'])){
		$pcr_type = $_POST['PCR_TYPE'];
		$make_permanent = $_POST['MAKE_PERMANENT'];
		$date_make = $_POST['DATE_MAKE'];
		$extend_permanent = $_POST['EXTEND_PERMANENT'];
		$date_extend = $_POST['DATE_EXTEND'];
		$confirmation = $_POST['CONFIRMATION'];
		$revised_score = $_POST['REVISED_SCORE'];
		$action = $_POST['ACTION'];
		$comments = $_POST['COMMENTS'];
		$due_by = $_POST['DUE_BY'];
		$due_date = $_POST['DUE_DATE'];
		$afe_number = $_POST['AFE_NUMBER'];
		
		// delete the records and re-insert with new values
		$ret = db_query("DELETE FROM $table WHERE pcr_id = $id");
		
		// Set the fields to insert
		$insert_fields = "PCR_ID,PCR_TYPE,MAKE_PERMANENT,DATE_MAKE,EXTEND_PERMANENT,DATE_EXTEND,CONFIRMATION,REVISED_SCORE,ACTION,COMMENTS,DUE_BY,DUE_DATE,AFE_NUMBER";
		
		// define the value array
		$arr = array();
		$arr[] = array($id, $pcr_type, $make_permanent, $date_make, $extend_permanent, $date_extend, $confirmation, $revised_score, $action, $comments, $due_by, $due_date, $afe_number);
		
		// insert new values
		if($ret = db_bulk_insert($table, $insert_fields, $arr, $db->debug)){
			$_SESSION['messageStack']->add("Area Review values were successfully updated.","success");
		}else{
			$_SESSION['messageStack']->add("An error occured while updating Area Review values.<br>".$db->ErrorMsg());
		}
	}
	
	return $ret;
}
/*
+------------------------------------------------------------------------
|  Update Sign Off Details
+------------------------------------------------------------------------
*/
function update_pcr_signoff($id, $page){
	global $db, $TABLES;
	$ret = true;
	$table = "tblPCR_SignOff";

	if(isset($_POST['TYPE_OF_REVIEW'])){
		$type_of_review = $_POST['TYPE_OF_REVIEW'];
		$meeting_date = $_POST['MEETING_DATE'];
		$meeting_comments = $_POST['MEETING_COMMENTS'];
		$sign_off = $_POST['SIGN_OFF'];
		$signed_off_date = $_POST['SIGNED_OFF_DATE'];
		$auditby_id = $_POST['AUDITBY_ID'];
		$due_date = $_POST['DUE_DATE'];
		
		// delete the records and re-insert with new values
		$ret = db_query("DELETE FROM $table WHERE pcr_id = $id");
		
		// Set the fields to insert
		$insert_fields = "PCR_ID,TYPE_OF_REVIEW,MEETING_DATE,MEETING_COMMENTS,SIGN_OFF,SIGNED_OFF_DATE,AUDITBY_ID,DUE_DATE";
		
		// define the value array
		$arr = array();
		$arr[] = array($id, $type_of_review, $meeting_date, $meeting_comments,$sign_off,$signed_off_date,$auditby_id,$due_date);
		
		// insert new values
		if($ret = db_bulk_insert($table, $insert_fields, $arr, $db->debug)){
			$_SESSION['messageStack']->add("Sign Off values were successfully updated.","success");
		}else{
			$_SESSION['messageStack']->add("An error occured while updating Sign Off values.<br>".$db->ErrorMsg());
		}
	}
	
	if(isset($_POST['ENGINEER_ID'])){
		$engineer_id = $_POST['ENGINEER_ID'];
		$engineer_approve = $_POST['ENGINEER_APPROVE'];
		$engineer_date = $_POST['ENGINEER_DATE'];
		$super_id = $_POST['SUPER_ID'];
		$super_approve = $_POST['SUPER_APPROVE'];
		$super_date = $_POST['SUPER_DATE'];
		
		// delete the records and re-insert with new values
		$ret = db_query("DELETE FROM $table WHERE pcr_id = $id");
		
		// Set the fields to insert
		$insert_fields = "PCR_ID,ENGINEER_ID,ENGINEER_APPROVE,ENGINEER_DATE,SUPER_ID,SUPER_APPROVE,SUPER_DATE";
		
		// define the value array
		$arr = array();
		$arr[] = array($id, $engineer_id, $engineer_approve, $engineer_date, $super_id, $super_approve, $super_date);
		
		// insert new values
		if($ret = db_bulk_insert($table, $insert_fields, $arr, $db->debug)){
			$_SESSION['messageStack']->add("Sign Off values were successfully updated.","success");
		}else{
			$_SESSION['messageStack']->add("An error occured while updating Sign Off values.<br>".$db->ErrorMsg());
		}
	}
	
	return $ret;
}
/*
+------------------------------------------------------------------------
|  Update Reviewer Details
+------------------------------------------------------------------------
*/
function update_pcr_reviewers($id, $page){
	global $db, $TABLES;
	$ret = false;
	
	switch($page){
		case 'pcr_initial_review':
			$table = "tblPCR_InitialReview_Details";
			break;
		case 'pcr_area_review':
			$table = "tblPCR_AreaReview_Details";
			break;
		case 'pcr_pcrt_review':
			$table = "tblPCR_PCRTReview_Details";
			break;
		case 'pcr_extension':
			$table = "tblPCR_Extension_Details";
			break;
		case 'pcr_sign_off':
			$table = "tblPCR_SignOff_Details";
			break;
		default:
			$_SESSION['messageStack']->add("An error occured while updating reviewers. This page does not require reviewers");
			return false;
	}
	
	if(isset($_POST['REVIEWER_DETAIL_ID']) && is_array($_POST['REVIEWER_DETAIL_ID'])){
		$detail_id = $_POST['DETAIL_ID'];
		$reviewer_detail_id = $_POST['REVIEWER_DETAIL_ID'];
		$reviewer_id = $_POST['REVIEWER_ID'];
		$date_reviewed = $_POST['DATE_REVIEWED'];
		//$status = $_POST['STATUS_RADIO'];
		$comments = $_POST['COMMENTS_TEXT'];
		$document = $_POST['DOCUMENT_LOC'];
		//$required = $_POST['REQUIRED_RADIO'];
		
 		// get the last detail id in the database
		$rs = db_query("SELECT MAX(detail_id) FROM $table");
		if($rs->EOF){
			$max_detail_id = 1;
		} else {
			if($row = $rs->FetchRow()) {
				$max_detail_id = $row['MAX(DETAIL_ID)'] + 1;
			} else {
				$max_detail_id = 1;
			}
		}
		
		if(is_array($reviewer_detail_id)){ // lets make sure we have an array
			$insert_fields = "PCR_ID,DETAIL_ID,REVIEWER_DETAIL_ID,REVIEWER_ID,DATE_REVIEWED,STATUS,COMMENTS,DOCUMENT_LOC,REQUIRED";
			// delete the records and re-insert with new values
			$ret = db_query("delete from $table where pcr_id = $id");
			// make the array for binding to our compiled insert statement
			for($i=0; $i < sizeof($reviewer_detail_id); $i++){
				if(!empty($reviewer_detail_id[$i])){
					// Update or New Insert ?
					if($detail_id[$i] < 1) {
						$detail_id[$i] = $max_detail_id;
						$max_detail_id += 1;
					}
					// get status and required
					$status = $_POST['STATUS_'.$i];
					$required = $_POST['REQUIRED_'.$i];
					// build the insert array
					$add[] = array( 
						$id, 
						$detail_id[$i], 
						$reviewer_detail_id[$i],
						$reviewer_id[$i], 
						$date_reviewed[$i], 
						$status, 
						$comments[$i], 
						$document[$i],
						$required
					); // array must be in this format to bind to our compiled statement
				}
			} //END:: loop
			// do the bulk insert on the main table using the id as the delete criteria
			$rows = 0;
			if($ret = db_bulk_insert($table,$insert_fields,$add, $db->debug)){
				$rows = $db->Affected_Rows();
				$_SESSION['messageStack']->add("Reviewer Details updated successfully.","success");
			}else{
				// let me know how you got here if it ever happens
				$_SESSION['messageStack']->add("Problems updating reviewer details.<br>Error: ".$db->ErrorMsg());
			}
		}
	}
	return $ret;
}
/*
+------------------------------------------------------------------------
|  Update PCR Actions
+------------------------------------------------------------------------
*/
function update_pcr_actions($id){
	global $TABLES, $db;
	$ret = false;
	$add = array();
/*
echo("<pre>");
print_r($_POST);
echo("<hr>");
print_r($_POST['ACTION_CONFIRMED']);
echo("</pre>");
foreach($_POST['ACTION_CONFIRMED'] as $key => $value){
	echo("update {$TABLES['ACTIONS']['table']} set action_confirmed = '$value' where action_register_id = $key");
}
exit;
*/
	if(isset($_POST['ACTION_CONFIRMED']) && is_array($_POST['ACTION_CONFIRMED'])){
		$rows = 0;
		foreach($_POST['ACTION_CONFIRMED'] as $key => $value){
			if($ret = $db->Execute("update {$TABLES['ACTIONS']['table']} set action_confirmed = '$value' where action_register_id = $key")){
				$rows += $db->Affected_Rows();
			}else{
				$_SESSION['messageStack']->add("Problems updating confirmed actions.<br>Error: ".$db->ErrorMsg());
			}
		}
		if($rows>0) $_SESSION['messageStack']->add("$rows Confirmed actions updated successfully.","success");
	}
	if(isset($_POST['ACTION_DESCRIPTION']) && is_array($_POST['ACTION_DESCRIPTION'])){
		$stage_id = $_POST['STAGE_ID'];
		$title = $_POST['ACTION_DESCRIPTION'];
		$manged_by = $_POST['MANAGED_BY_ID'];
		$allocated_to = $_POST['ALLOCATED_TO_ID'];
		$status = $_POST['STATUS'];
		$due_date = $_POST['DUE_DATE'];
		$days = $_POST['DAYS_AFTER_PREV_STAGE'];
		$site_id = $_POST['SITE_ID'];
		$action_confirmed = 'N';
		$raise_now = $_POST['RAISE_NOW'];
		
		if(is_array($title)){ // lets make sure we have an array
			$insert_fields = "PCR_ID,STAGE_ID,SITE_ID,MANAGED_BY_ID,ALLOCATED_TO_ID,ACTION_DESCRIPTION,DUE_DATE,DAYS_AFTER_PREV_STAGE,RAISE_NOW,STATUS,ACTION_CONFIRMED";
			// delete the records and re-insert with new values
			$ret = db_query("delete from tblpcr_actions where pcr_id = $id and action_register_id is null");
			// make the array for binding to our compiled insert statement
			for($i=0; $i < sizeof($title); $i++){
				if(!empty($title[$i])){
					if(empty($status[$i])) $status[$i]='pending';
					if($stage_id[$i]==1) $days[$i]=NULL;
					//PCR_ID,STAGE_ID,SITE_ID,MANAGED_BY_ID,ALLOCATED_TO_ID,ACTION_DESCRIPTION,DUE_DATE,DAYS_AFTER_PREV_STAGE,RAISE_NOW,STATUS
					$add[] = array( 
						$id, 
						$stage_id[$i], 
						$site_id, 
						$manged_by[$i], 
						$allocated_to[$i], 
						$title[$i], 
						$due_date[$i], //"to_date('{$due_date[$i]}','YYYY-MM-DD')", 
						$days[$i], 
						$raise_now[$i], 
						$status[$i],
						$action_confirmed
					); // array must be in this format to bind to our compiled statement
				}
			} //END:: loop
			// do the bulk insert on the main table using the id as the delete criteria
			//print_r($TABLES['CONFIGURATION']);
			$rows = 0;
			if(!empty($add)) {
				if($ret = db_bulk_insert($TABLES['ACTIONS']['table'],$insert_fields,$add, $db->debug)){
					$rows = $db->Affected_Rows();
					$_SESSION['messageStack']->add("$rows PCR Actions updated successfully.","success");
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Problems updating action details.<br>Error: ".$db->ErrorMsg());
				}
			} else {
				$_SESSION['messageStack']->add("No update for PCR Actions was neccessary.","success");
			}
		}
	}
	return $ret;
}

function update_pcr_categories($id){
	global $TABLES,$db;
	$ret = true;
	// if action category exists then replace the checkbox values
	if(isset($_POST['Reason_For_Request'])){
		// replace linked records
		$sql_del="delete from " . $TABLES['Reason_For_Request']['table'] . " where CHECKBOXTYPE = 'Reason_For_Request' and " . $TABLES['Reason_For_Request']['id'] . " = $id ";
		$success = db_query($sql_del);
	
		$arr=array();
		$act_cat = $_POST['Reason_For_Request'];
		foreach($act_cat as $key => $val){
			if(!empty($val)){
				$update = true;
				$type='Reason_For_Request';
				$arr[] = array($id, $val, $type);
			}
		}
		if($update){
			// insert new checkbox values
			if($ret = db_bulk_insert($TABLES['Reason_For_Request']['table'],"PCRID,CHECKBOXID,CHECKBOXTYPE",$arr,$db->debug)){
				$_SESSION['messageStack']->add("Reason For Change values were successfully updated.","success");
			}else{
				$_SESSION['messageStack']->add("An error occured while updating Reason For Change values.<br>".$db->ErrorMsg());
			}
		} // update records is true
	}
	return $ret;
}
function update_documents_sequence($id,$type){
	global $TABLES,$db;
	$ret = true;
	// if action category exists then replace the checkbox values
	if(isset($_POST['DOCUMENTLOCATION'])){
		// replace linked records
		$sql_del="delete from " . $TABLES['DOCUMENTLOCATION']['table'] . " where FORMTYPE = '$type' and " . $TABLES['DOCUMENTLOCATION']['id'] . " = $id ";
		$success = db_query($sql_del);
	
		$arr=array();
		$act_cat = $_POST['DOCUMENTLOCATION'];
		$update = false;
		$docerr = false;
		foreach($act_cat as $key => $val){
			if(!empty($val)){
								
				if($val{0}!='\\' or $val{1}!='\\'){
					$_SESSION['messageStack']->add("Document Location contains path '".$val."' which is an incorrectly formatted network path. The document must be located on the network.");
					$docerr = true;
				}else{
					$arr[] = array($id, $val, $type);
					$update = true;
				}
			}
		}
		if($update){
			// insert new checkbox values
			if($ret = db_bulk_insert($TABLES['DOCUMENTLOCATION']['table'],"REPORT_ID,DOCUMENTLOCATION,FORMTYPE",$arr,$db->debug)){
				$_SESSION['messageStack']->add("Supporting Documents were successfully updated.","success");
			}else{
				$_SESSION['messageStack']->add("An error occured while updating Supporting Document records.<br>".$db->ErrorMsg());
			}
		} // update records is true
		
				
		// If document path error then set return to false even though some records may have been updated
		if($docerr==true){
			$ret=false;
		}

	}
	return $ret;
}
function update_checkbox_values_sequence($tables, $id){
	global $db;
	$ret = true;
	
	// get checkbox values array so we can map 
	// tblIncidentForm_Checkboxes.Checkbox_Type to tblIncident_CheckboxValues.CheckboxType
	$types = array("Reason_For_Change"=>"Reason_For_Change");//cats_get_checkboxes_values_array();
	// delete * checkbox values and insert the new values if they exist
	$ret = db_query("delete from {$tables['table']} where {$tables['id']}=$id ");
	// loop through types array and check if we have a matching array in the POST
	foreach($types as $key => $chktype){
		
		if(isset($_POST[$key]) && is_array($_POST[$key])){
			$checkbox = $_POST[$key];
			
			$arr=array();
			$update = false;
			// only add value to the array if it is not empty
			foreach($checkbox as $k => $chkid){
				if(!empty($chkid)){
					$update = true;
					$arr[] = array($id, $chkid, $chktype);
				}
			}
			if($update){
				// delete the checkbox values by type
				//$ret = db_query("delete from {$tables['table']} where CHECKBOXTYPE='$chktype' and {$tables['id']}=$id ");
				
				// insert new checkbox values
				if($ret = db_bulk_insert($tables['table'],$tables['fields'],$arr,$db->debug)){
					$_SESSION['messageStack']->add("Checkbox ($key) values were successfully updated.","success");
				}else{
					$_SESSION['messageStack']->add("An error occured while updating ($key) checkbox value records.<br>".$db->ErrorMsg());
				}
			} // update records is true
		}
	} // loop: checkbox values array
	return (bool)$ret;
}

// house cleaning to be done
$db=db_close();
?>