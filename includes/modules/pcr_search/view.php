<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
// Changes for Work Item #12887
// $module_name_txt = (strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false)?"CMR":"PCR";//ucwords(preg_replace("/_/"," ",$m));
$module_name_txt = "PCR";
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'view';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$current_tab=(isset($_GET['t']))?$_GET['t']:(isset($_SESSION['t'])?$_SESSION['t']:'view_pcr_details');
$RS = array();

if($id>0){
	$sql = "SELECT PCR_DISPLAYID,PCR_TYPE,CRITICALITY,STATUS,TITLE,SITE_ID FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	
	// see if the user has access to this action by checking if he belongs to any one of the sites from the view
	$page_access = cats_check_user_site_access($RECORD['SITE_ID']) || cats_user_is_super_administrator();

	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this PCR details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}
$RS = array();
?>
<link rel="stylesheet" type="text/css" href="<?php echo(WS_STYLE_PATH);?>tabs.css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
var PI_SITE_ID = 3;

function init(){
	// create document.mousedown handlers
	//alert(document.documentElement.offsetHeight-element('tool_bar_c').offsetHeight)
	_t.init();
	init_document_handlers(false);
	//alert(document.documentElement.offsetHeight-element('tool_bar_c').offsetHeight)
	window.onresize=init_resize_tabs;
	return true;
}
window.onload=init;

function get_panel_height(){
	return (document.documentElement.offsetHeight-(element('tool_bar_c').offsetHeight)) + 1500;
}
function get_panel_width(){
	return (document.body.clientWidth) + 500;
}
function init_resize_tabs(){
	_t.container.style.height=get_panel_height();
	var p=_t.panels[_t.selected];
	p.style.height=get_panel_height();
	p.style.width=get_panel_width();
	try{
		p.contentWindow.window.init_resize_tab_editor();
	}catch(e){}
}
function toggle_details(itm){
	var rows=itm.firstChild.rows;
	var show=rows[1].style.display;
	rows[1].style.display=(show=='none')?'':'none';
	rows[2].style.display=(show=='none')?'':'none';
	init_resize_tabs();
}
/*********************************
Create Tab Object for page tabbing
*********************************/
function _tabs(){
	this.a=arguments;
	this.default_tab=this.selected=this.a[0];
	this.module=this.a[1];
	this.id=this.a[2];
	this.name='main_tabs';
	this.panels=new Object();
	this.history_added=false;
	this.init = function(){
		var a=arguments;
		this.details=element('general_details');
		this.tabs=element(this.name);
		this.container=element('Lcontentbody');
		var b = this.tabs;
		var a = b.getElementsByTagName("A");
		var id=i=0;
		for(i=0;i<a.length;i++) {
			this.details.style.display=(this.selected=='view_pcr_details')?'none':'block';
			a[i].className=(a[i].id==this.selected)?'tab_on':'tab_off';
			a[i].onclick=function(){
				this.blur();
				_t.selected='';
				var b = _t.tabs;
				var a = b.getElementsByTagName("A");
				var id=i=0;
				for(i=0;i<a.length;i++){
					if(a[i]==this) _t.selected=this.id;
					else if(_t.panels[a[i].id]) _t.panels[a[i].id].style.display='none';
					a[i].className=(a[i].id==_t.selected)?'tab_on':'tab_off';
				}
				_t.details.style.display=(_t.selected=='view_pcr_details')?'none':'block';
				_t.tab(this.id);
				setTimeout(init_resize_tabs,500);
			}
		}
		this.tab(this.selected);
		init_resize_tabs();
	}
	this.tab=function(id){
		if((this.panels[id])==null)
			this.add(id);
		this.panels[id].style.display='block';
	}
	this.add=function(name){
		if(!this.history_added){
			old=top.gui.session.history.all.pop();
			top.gui.session.history.add('pcr_search','edit');
			this.history_added=true;
		}
		top._show_message("Loading...");
		var o=create_element("screen_"+name,"iframe",true,this.container);
		var url="index.php?m="+this.module+"&p="+name+"&id="+this.id;
		o.setAttribute("src", url);
		o.style.backgroundColor='#ffffff';
		o.style.border='none';
		//o.style.borderColor="red";
		//o.style.borderWidth="2px";
		//o.style.borderStyle="solid";
		o.style.width=this.container.offsetWidth;
		o.style.height=get_panel_height();
		this.panels[name]=o;
	}
	return this;
}
var _t=new _tabs("<?php echo($current_tab); ?>","<?php echo $m;?>","<?php echo $id;?>");
</script>
</head>
<body class="edit">
<!-- BEGIN:: Of Tab Buttons -->
<div id="tool_bar_c">
<fieldset class="tabs" id="main_tabs">
<?php 
$TESTING=false; // set to true to show all tabs
if($TESTING){ ?>
<a
	title="View <?PHP echo $module_name_txt;?> Details"
	id="view_pcr_details"
	href="javascript:{}"
	><?PHP echo $module_name_txt;?> Details</a>
<a
	title="View Initial Review Details"
	id="view_pcr_initial_review"
	href="javascript:{}">Initial Review</a>
<a
	title="View Evaluation Details"
	id="view_pcr_evaluation"
	href="javascript:{}">Evaluation</a>
<a
	title="View Area Review Details"
	id="view_pcr_area_review"
	href="javascript:{}">Area Review</a>
<a
	title="View PCRT Review Details"
	id="view_pcr_pcrt_review"
	href="javascript:{}">PCRT Review</a>
<a
	title="View Extension Details"
	id="view_pcr_extension"
	href="javascript:{}">Extension</a>
<a
	title="View Action Details"
	id="view_pcr_actions"
	href="javascript:{}">Actions</a>
<a
	title="View Sign Off Details"
	id="view_pcr_sign_off"
	href="javascript:{}">Sign Off</a>
<a
	title="View Post Audit Details"
	id="view_pcr_post_audit"
	href="javascript:{}">Post Audit</a>
<?php }else{ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="view_pcr_details"
	href="javascript:{}"
	><?PHP echo $module_name_txt;?> Details</a>
<?php 
// Changes for Work Item #12887
// if($RECORD['SITE_ID']!=3 && !is_direct_to_evaluation($id)){ 
/* if(!is_direct_to_evaluation($id)){ ?>
<a
	title="Edit Initial Review Details"
	id="view_pcr_initial_review"
	href="javascript:{}">Initial Review</a>
<?php } */ ?>
<a
	title="Edit Evaluation Details"
	id="view_pcr_evaluation"
	href="javascript:{}">Evaluation</a>
<?php if(!is_direct_to_actions($id)){ ?>
<a
	title="Edit Area Review Details"
	id="view_pcr_area_review"
	href="javascript:{}">Area Review</a>
<?php } ?>
<?php 
// Changes for Work Item #12887
// if($RECORD['SITE_ID']==3 && !is_direct_to_actions($id)){
if(!is_direct_to_actions($id)){ ?>
<a
	title="Edit PCRT Review Details"
	id="view_pcr_pcrt_review"
	href="javascript:{}">PCRT Review</a>
<a
	title="Edit Extension Details"
	id="view_pcr_extension"
	href="javascript:{}">Extension</a>
<?php } ?>
<a
	title="Edit Action Details"
	id="view_pcr_actions"
	href="javascript:{}">Actions</a>
<?php if(is_pcr_actions_complete($id)){ ?>
<a
	title="Edit Sign Off Details"
	id="view_pcr_sign_off"
	href="javascript:{}">Sign Off</a>
<a
	title="Edit Post Audit Details"
	id="view_pcr_post_audit"
	href="javascript:{}">Post Audit</a>
<?php } ?>
<?php } //testing ?>
</fieldset>
<fieldset class="tbar" id="general_details" onClick="toggle_details(this);"><?php
// Instantiate a new editor
$ed = new Editor("edit");
if($id > 0)
	echo($ed->buildForm($FIELDS, $TABLES['view'], $TABLES['id'], $id, isset($RS[$id])?$RS[$id]:false));
else{
	$_SESSION['messageStack']->add("You can't edit a PCR if it has no ID. Please note how you got here and notify Bentley IS department of the steps you took to get this message.");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}
?>
</fieldset>
</div>
<!-- END:: Of Tab Buttons -->

<div id="Lcontentbody">
<!-- Main Tabs:: Dynamically created iframes go here -->
</div>