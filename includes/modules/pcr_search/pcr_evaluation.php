<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
$do_not_set_properties = true;
require_once('properties.php');
$GENERAL = getPropertiesGeneral($p);
$TABLES  = getPropertiesTables($p);

$module_name = ucwords($m);
$module_name_txt = "Evaluation";//ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
$db->debug=false;
if($id>0){
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	
	// see if the user has access to this action by checking if he belongs to any one of the sites from the view
	$page_access = cats_check_user_site_access($RECORD['SITE_ID']);
	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this record");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}else{
	$_SESSION['messageStack']->add("The ID for this record was not supplied. Please note how you got here and notify Tiwest IT department of the steps you took.");
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
$RS = array();
// Call the functions and store the results in the following variables
$FIELDS  = getPropertiesFields($p);
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/pcr_search.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>

var MODULE = "<?php echo $m;?>";
function init(){
	// create document.mousedown handlers
	<?php 
	// Changes for Work Item #12887
	// if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false) echo("init_tabs();"); 
	?>
	// hide or display fields dependent on site id 
	set_textarea_maxlength();
	init_document_handlers();
	init_resize_tab_editor();
	return true;
}
window.onload=init;
window.onresize=init_resize_tab_editor;



/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

</script>
<?php


// Assign what javascript handler to use for client side validation
$onSubmit = '';
$onSubmit = "return CATS_validateForm(this, 'EVALUATION_NAME_ID','','R','EVALUATION_DATE','','R');";

?>
<style type="text/css">
	FIELDSET.tbar {
	
	padding-right: 10px !important;
	
}
</style>
</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="<?php echo($onSubmit) ?>">
<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<?php 
// Changes for Work Item #12887
// if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false){ 
if(false){
?>
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:{}" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<a
	title="Authority to Proceed"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:{}" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/approve.gif);">Authority to Proceed</a>
</fieldset>
<?php } ?>
<fieldset class="tool_bar">
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->
<div id="Lcontentbody">
<!-- Edit Main Fields -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
// Instantiate a new editor
$ed = new Editor("edit");

// Check whether to edit or add a new record
echo($ed->buildForm($FIELDS[0],$TABLES['view'],$TABLES['id'],$id, isset($RS[$id])?$RS[$id]:false));
?>
<fieldset class="tbar" style="text-align:right;  margin-right: 0em;">
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()">
<?php if($action=='edit'){ ?>
<!-- <input type="button" class="button" name="cats::New" value="New Site" onClick="_m.newModule();" > -->
<?php } ?>
<input type="button" class="button" name="Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>
</fieldset>

<?php 
// Changes for Work Item #12887
// if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false){ 
if(false){
?>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
if($id > 0)
	echo($ed->buildForm($FIELDS[1], $TABLES['view'], $TABLES['id'], $id, $RS[$id]));
else
	echo($ed->buildForm($FIELDS[1]));
?>
<fieldset class="tbar" style="text-align:right;  margin-right: 0em;">
<?php if($p!='view'){ ?>
<input type="submit" class="submit" name="cats::Save_ATP" value="Save" onClick="preSubmitForm()"/>
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Back to Details" onClick="showhide(this);">
</fieldset>		
</fieldset>
<?php } // END:: hide panel ?>
</div>
</form>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">

	function preSubmitForm()
	{
		if($("#EVALUATION_DATE_d").val() != "")
			setHiddenDate("EVALUATION_DATE");
		if($("#AUTHORITYDATE_1_d").val() != "")
			setHiddenDate("AUTHORITYDATE_1");
		if($("#AUTHORITYDATE_2_d").val() != "")
			setHiddenDate("AUTHORITYDATE_2");
		if($("#MEETING_DATE_d").val() != "")
			setHiddenDate("MEETING_DATE");
	}
</script>