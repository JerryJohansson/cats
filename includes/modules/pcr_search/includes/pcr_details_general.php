<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');

$ID = "3626";

$PCR_DETAILS = Get_PCR_Details_On_ID($_POST, $_GET);
?>

<!-- START: PCR General Details -->
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr>
    <td class="normalLabel" width="16%">PCR No.</td>
    <td width="84%" class="normal"><?PHP echo($PCR_DETAILS["PCR_DISPLAYID"]); ?>&nbsp;</td>
  </tr>
  <tr >
    <td valign="top" class="normalLabel">Type of PCR:</td>
    <td class="normal"><?PHP echo($PCR_DETAILS["PCR_TYPE"]); ?>&nbsp;</td>
  </tr>
  <tr >
    <td valign="top" class="normalLabel">Criticality:</td>
    <td class="normal"><?PHP echo($PCR_DETAILS["IMPACTLEVEL"]); ?>&nbsp;</td>
  </tr>
  <tr >
    <td valign="top" class="normalLabel">Status:</td>
    <td class="normal"><?PHP echo($PCR_DETAILS["OVERALL_STATUS"]); ?>&nbsp;</td>
  </tr>
  <tr >
    <td valign="top" class="normalLabel">Title:</td>
    <td class="normal"><?PHP echo($PCR_DETAILS["TITLE"]); ?>&nbsp;</td>
  </tr>
</table>
<br>
<br>
<!-- END: PCR General Details -->