<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";

$where = "";
$multi_sites = $_SESSION['user_details']['site_access'];
$users_site  = $_SESSION['user_details']['site_id'];


if(is_array($_SESSION['user_details']) && $multi_sites!='')
	$where = $multi_sites;
else
	$where = $users_site;

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST) > 0)
	$_SESSION[$m.'_'.$p] = $_POST;
else if(isset($_SESSION[$m.'_'.$p]))
	$_POST=$_SESSION[$m.'_'.$p];


// If the post is equal to search then perform the following
if(isset($_POST['cats::search'])||isset($_POST['cats::report']))
{

	$sql = "SELECT PCR_ID, PCR_DisplayId AS ID, Title, ImpactLevel, OriginatorName, Site_Description, Department_Description, PCR_Date, Actions_Open, Actions_Closed";

	// create action controls array to hold our buttons
	$action_controls = array();
		if(isset($_POST['cats::report'])){
		if(isset($_POST['cats::report_fields']) && !empty($_POST['cats::report_fields']))
			$sql = "select ".$_POST['cats::report_fields']." ";
		else
			$sql = "Select * ";
	}else{
		if(cats_user_is_editor(true)) {
			// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
			//$edit_button = "edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=main&id=:{$TABLES['id']}:\")";
			$edit_button = "edit:javascript:top.edit(\"$m\",:{$TABLES['id']}:,\"main\")";
			// put it into an array so we can merge with column headers later
			$action_controls[] = $edit_button;
			$sql .= ", '$edit_button' as edit ";
		}
		if(cats_user_is_super_administrator()) {
			// delete button string can be a button or an action checkbox for each row...
			// the checkboxes also have a dropdown which is displayed to the right of the grid navigation
			// The button control sql looks like this:
			// delete:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=post&a=del&id=:{$TABLES['id']}:\")";
			$delete_button = "delete:::{$TABLES['id']}:";
			$action_controls[] = $delete_button;
			$sql .= ", '$delete_button' as del ";
		}
	}
	$sql .= " FROM {$TABLES['view']} ";

	// column headers and attributes
	// these are used when rendering the search results
	// col_attributes - use this array to attach attributes to the header columns
	$col_attributes = array('',' width="5%" ', ' width="30%" ',' width="5%" ', ' width="10%" ',' width="10%" ',' width="15%" ',' width="10%" ',' width="5%" ',' width="5%" ');
	// col_headers - used to label our heading columns

	$col_headers = explode("|","|ID|Title|Impact Level|Originator|Site|Department|Date Entered|Actions Open|Actions Closed");
	$col_headers = array_merge($col_headers,$action_controls); // append edit and delete buttons if they exist

	$col_ignore = array('PCR_ID');
	$col_links = array("","","js|top.view('$m',|PCR_ID");

	// the order by column name used for the default sorting criteria
	$order_by_col = "PCR_ID";

	$sql_where = "";
	// Create Where String and append it to the Select String
	foreach($FIELDS as $column_name => $props){
		// the criterias operator I.E operator of <= will look like COLUMN_NAME <= 'SEARCH VALUE'
		$operator = isset($props['operator']) ? " {$props['operator']} " : " = ";
		// the values delimiter I.E a delim of ' will look like 'SEARCH VALUE'
		//$delim = $props['delim'];comment  dev
		$delim = isset($props['delim'])?$props['delim']:'';
		// create the user specific sites criteria and add to the where string
		if($column_name == "SITE_ID" && (!isset($_POST[$column_name]) || empty($_POST[$column_name]))){
			$sql_where .= (($sql_where == "") ? " WHERE ":" AND ") . db_get_user_sites_where_string();
		}else{
			switch($column_name){
/*
				case 'DUE_DATE_FROM':
					if (isset($_POST[$column_name]) && !empty($_POST[$column_name])) {
						if (isset($_POST['DUE_DATE_TO']) && !empty($_POST['DUE_DATE_TO'])) {
							// Both To And From Set
							$sql_where .= (($sql_where == "") ? " WHERE ":" AND ");
							$sql_where .= "pcr_id IN(SELECT pcr_id FROM tblpcr_actions WHERE due_date >= TO_DATE('".$_POST['DUE_DATE_FROM']."','YYYY-MM-DD') AND due_date <= TO_DATE('".$_POST['DUE_DATE_TO']."','YYYY-MM-DD'))";
						} else {
							// Only DUE_DATE_FROM Set
							$sql_where .= (($sql_where == "") ? " WHERE ":" AND ");
							$sql_where .= "pcr_id IN(SELECT pcr_id FROM tblpcr_actions WHERE due_date >= TO_DATE('".$_POST[$column_name]."','YYYY-MM-DD'))";
						}
					}
					break;
				case 'DUE_DATE_TO':
					if (isset($_POST[$column_name]) && !empty($_POST[$column_name])) {
						if (isset($_POST['DUE_DATE_FROM']) && !empty($_POST['DUE_DATE_FROM'])) {
							// Should have been picked up by the DUE_DATE_FROM
						} else {
							// Only DUE_DATE_TO Set
							$sql_where .= (($sql_where == "") ? " WHERE ":" AND ");
							$sql_where .= "pcr_id IN(SELECT pcr_id FROM tblpcr_actions WHERE due_date <= TO_DATE('".$_POST[$column_name]."','YYYY-MM-DD'))";
						}
					}
					break;
*/
				default:
					// sql_where is passed by reference
					db_get_where_clause($column_name,$sql_where,$operator,$delim);
					break;
			}
		}
	}
	$search_sql = $sql . $sql_where;


	$db->debug = false;
	if($db->debug == true)
	{
		print_r($_POST);
		echo('<br>'.$search_sql);
	}

	require_once(CATS_ADODB_PATH . 'cats_csv.php');
}

?>
<script>
var FORM_TARGET="<?php echo $results_iframe_target;?>";

/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	// resize result iframe to consume the parent page
	init_resize_results_container();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><form name="results" action="index.php" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
	<td width="100%">

<!-- BEGIN:: Results Table -->
<?php
// Search Results
if(isset($search_sql))
{
    cats_form_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], $m, $order_by_col, 'ASC', $col_links, $col_ignore);
}
else
	echo("<h2>No Search results</h2>");
?>
<!-- END:: Results table -->
	</td>
</tr>
</table>
</form>