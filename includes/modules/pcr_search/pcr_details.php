<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
// Changes for Work Item #12887
//$module_name_txt = (strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false)?"CMR":"PCR";//ucwords(preg_replace("/_/"," ",$m));
$module_name_txt = "PCR";
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


if($id>0){
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);

	// var_dump('pcr_details');
	// see if the user has access to this action by checking if he belongs to any one of the sites from the view
	$page_access = cats_check_user_site_access($RECORD['SITE_ID']) || cats_user_is_super_administrator();

	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this PCR details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}
if((!cats_user_is_pcr_administrator() && !cats_user_is_super_administrator())  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New PCR");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/pcr_search.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
var PI_SITE_ID = 3;
var SELECTED_AREA_ID = '<?php echo $RECORD['AREA_ID']; ?>';
function init(){
	
	<?php // Changes for Work Item #12887
	//if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false) echo("init_tabs();"); 
	?>
	
	<?php if($p!='view') echo(($id>0)?'show_area_reason();':'show_area_reason('.$_SESSION['user_details']['site_id'].');');?>
	
	set_textarea_maxlength();
	init_document_handlers();
	init_resize_tab_editor();
	return true;
}
window.onload=init;
window.onresize=init_resize_tab_editor;

/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

function get_area_timeout(){
	setTimeout(get_area, 500);
}
function show_area_reason(){
	var args=arguments;
	var id=(args.length>0)?args[0]:document.forms[0].SITE_ID.value;
	get_area();
	// Changes for Work Item #12887
	// show_reason((id!=PI_SITE_ID));
	show_reason(false);
}
function show_reason(show){
	var ids=['row_CHANGETYPE_ID','row_Reason_For_Request','row_CRITICALITY_ID'];
	var i=0;
	var cnt=ids.length;
	for(i=0;i<cnt;i++){
		if(i==(ids.length-1)) show=!show;
		try{element(ids[i]).style.display = (show?'':'none');}catch(e){}
	}
}

function get_area(){
	var e=document.forms[0].elements;
	var id = e['SITE_ID'].value;
	var to = e['AREA_ID'];
	//get_remote_dd(id, to, 'admin', 'get_pcr_area_array', SELECTED_AREA_ID);
}

function PCR_validateForm(f){
<?php 
// Changes for Work Item #12887
//if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false){
//	$auth_validation = ",'AUTHORITYID_1','','R','AUTHORITYID_2','','R'";
//}else{
	$auth_validation = '';
//}
?>
// Changes for Work Item #12887
//	if(f.SITE_ID.value == PI_SITE_ID){
		return CATS_validateForm(f, 'CRITICALITY_ID','','R','ORIGINATOR_ID','','R','PCR_DATE','','R','DESCRIPTION','','R');
//	}else{
//		return CATS_validateForm(f, 'CHANGETYPE_ID','','R','ORIGINATOR_ID','','R','PCR_DATE','','R','DESCRIPTION','','R'<?php echo($auth_validation); ?>);
//	}
}
</script>
<style type="text/css">
	FIELDSET.tbar {
	
	padding-right: 10px !important;
	
}
</style>
</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return PCR_validateForm(this);">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<?php 
// Changes for Work Item #12887
// if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false){ 
if(false){
?>
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:{}" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<a
	title="Authority to Proceed"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:{}" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/approve.gif);">Authority to Proceed</a>
</fieldset>
<?php } ?>
<fieldset class="tool_bar">
<?php if($action=='edit'){ ?>
<a
	title="New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php } ?>
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
// Instantiate a new editor
$ed = new Editor("edit");
if($id > 0){
	//echo($ed->buildForm($FIELDS[0], $TABLES['view'], $TABLES['id'], $id, $RS[$id])); comment dev
	echo($ed->buildForm($FIELDS[0], $TABLES['view'], $TABLES['id'], $id, isset($RS[$id])?$RS[$id]: false));
}
else
	echo($ed->buildForm($FIELDS[0]));

?>
<fieldset class="tbar" style="text-align:right;  margin-right: 0em;">
<?php if($p!='view'){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()"/>
<?php }elseif(cats_user_is_editor()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit('main');" />
<?php } ?>
<?php if($action=='edit'){ ?>
<input type="button" class="button" name="cats::New" value="New <?php echo($module_name_txt);?>" onClick="_m.newModule();" >
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>

<?php 
// Changes for Work Item #12887
// if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false){ 
if(false){
?>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
if($id > 0)
	echo($ed->buildForm($FIELDS[1], $TABLES['view'], $TABLES['id'], $id, $RS[$id]));
else
	echo($ed->buildForm($FIELDS[1]));
?>
<fieldset class="tbar" style="text-align:right; ">
<?php if($p!='view'){ ?>
<input type="submit" class="submit" name="cats::Save_ATP" value="Save" />
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Back to Details" onClick="showhide(this);">
</fieldset>		
</fieldset>
<?php } // END:: hide panel ?>
</div>
</form>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">

	function preSubmitForm()
	{
		if($("#PCR_DATE_d").val() != "")
			setHiddenDate("PCR_DATE");
		if($("#AUTHORITYDATE_1_d").val() != "")
			setHiddenDate("AUTHORITYDATE_1");
		if($("#AUTHORITYDATE_2_d").val() != "")
			setHiddenDate("AUTHORITYDATE_2");
		if($("#MEETING_DATE_d").val() != "")
			setHiddenDate("MEETING_DATE");
	}
</script>