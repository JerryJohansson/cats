<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH . 'forms/sequences.php');
$do_not_set_properties = true;
require_once('properties.php');
$GENERAL = getPropertiesGeneral($p);
$TABLES  = getPropertiesTables($p);

$module_name = ucwords($m);
$module_name_txt = "Area Review";//ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();

if($id>0){
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	
	// see if the user has access to this action by checking if he belongs to any one of the sites from the view
	$page_access = cats_check_user_site_access($RECORD['SITE_ID']);
	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this record");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}else{
	$_SESSION['messageStack']->add("The ID for this record was not supplied. Please note how you got here and notify Tiwest IT department of the steps you took.");
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
$RS = array();
// Call the functions and store the results in the following variables
$FIELDS  = getPropertiesFields($p);
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/pcr_search.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	<?php 
	// Changes for Work Item #12887
	// if(strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false) echo("init_tabs();"); 
	?>
	
	set_textarea_maxlength();
	init_document_handlers();
	init_resize_tab_editor();
	return true;
}
window.onload=init;
window.onresize=init_resize_tab_editor;

/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);


function PCR_validateForm(f){
<?php 
// Changes for Work Item #12887
// if(strpos("BE|CO|CH|",$_SESSION['user_details']['site_code'])!==false){
//	$auth_validation = ",'AUTHORITYID_1','','R','AUTHORITYID_2','','R'";
// }else {
    $auth_validation = "";
// }
?>
// Changes for Work Item #12887
//	if(f.SITE_ID.value == PI_SITE_ID){
		return true;
//	}else{
//		return CATS_validateForm(f<?php echo($auth_validation); ?>);
//	}
}
</script>
<style type="text/css">
	FIELDSET.tbar {
	
	padding-right: 10px !important;
	
}
</style>
</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return PCR_validateForm(this);">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<?php 
// Changes for Work Item #12887
// if(strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false){ 
if(false){
?>
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:{}" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<a
	title="Authority to Proceed"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:{}" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/approve.gif);">Authority to Proceed</a>
</fieldset>
<?php } ?>
<fieldset class="tool_bar">
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
// Instantiate a new editor
$ed = new Editor("edit");
echo($ed->buildForm($FIELDS[0], $TABLES['view'], $TABLES['id'], $id, isset($RS[$id])?$RS[$id]:false));
?>
<fieldset class="tbar" style="text-align:right;  margin-right: 0em;">
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()"/>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>

<?php 
// Changes for Work Item #12887
// if(strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false){ 
if(false){
?>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
echo($ed->buildForm($FIELDS[1], $TABLES['view'], $TABLES['id'], $id, $RS[$id]));
?>
<fieldset class="tbar" style="text-align:right;  margin-right: 0em;">
<input type="submit" class="submit" name="cats::Save_ATP" value="Save" onClick="preSubmitForm()"/>
<input type="button" class="button" name="cats::Cancel" value="Back to Details" onClick="showhide(this);">
</fieldset>		
</fieldset>
<?php } // END:: hide panel ?>
</div>
</form>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">

	function preSubmitForm()
	{
		if($("#DATE_MAKE_d").val() != "")
			setHiddenDate("DATE_MAKE");
		if($("#DATE_EXTEND_d").val() != "")
			setHiddenDate("DATE_EXTEND");
		if($("#DUE_DATE_d").val() != "")
			setHiddenDate("DUE_DATE");
		if($("#AUTHORITYDATE_1_d").val() != "")
			setHiddenDate("AUTHORITYDATE_1");
		if($("#AUTHORITYDATE_2_d").val() != "")
			setHiddenDate("AUTHORITYDATE_2");
		if($("#MEETING_DATE_d").val() != "")
			setHiddenDate("MEETING_DATE");
			
		if($("#DATE_REVIEWED[0]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[0]");
		if($("#DATE_REVIEWED[1]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[1]");
		if($("#DATE_REVIEWED[2]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[2]");
		if($("#DATE_REVIEWED[3]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[3]");
		if($("#DATE_REVIEWED[4]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[4]");
		if($("#DATE_REVIEWED[5]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[5]");
		if($("#DATE_REVIEWED[6]_d").val() != "")
			setHiddenDate("DATE_REVIEWED[6]");
			
			
			
	}
</script>