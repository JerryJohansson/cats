<?php


/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($page = 'search')
{
	$ret = array();
	// Changes for Work Item #12887
	// $name = (strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false)?"CMR":"PCR";
	// $title = (strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false)?"Change Management Request":"Plant Change Request";
	$name = "PCR";
	$title = "Process Change Request";
	switch($page){
		case 'search':
		case 'results':		
			$ret = array_merge($ret, 
				array(
					'name'=>"Search $name", // Short title
					'title'=>"Search $title", // Full title description
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>"New $name",
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'pcr_details':
			$ret = array_merge($ret, 
				array(
					'name'=>"Edit $name",
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view_pcr_details':
			$ret = array_merge($ret, 
				array(
					'name'=>"View $name",
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}




/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/


function getPropertiesTables($page = 'search')
{
	switch($page){
		case 'pcr_details': case 'view_pcr_details': 
		case 'new': case 'main': case 'view':
			$ret = array(
				'table'=>'TBLPCR_DETAILS', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_DETAILS',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				),
				'Reason_For_Request'=>array(
					'table'=>'TBLPCR_CHECKBOXVALUES', 
					'id'=>'PCRID'
				)
			);
			break;
		case 'pcr_initial_review': case 'view_pcr_initial_review':
			$ret = array(
				'table'=>'TBLPCR_INITIALREVIEW', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_INITIALREVIEW'
			);
			break;
		case 'pcr_evaluation': case 'view_pcr_evaluation':
			$ret = array(
				'table'=>'TBLPCR_EVALUATION', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_EVALUATION',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				)
			);
			break;
		case 'pcr_area_review': case 'view_pcr_area_review':
			$ret = array(
				'table'=>'TBLPCR_AREAREVIEW', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_AREAREVIEW'
			);
			break;
		case 'pcr_pcrt_review': case 'view_pcr_pcrt_review':
			$ret = array(
				'table'=>'TBLPCR_PCRTREVIEW', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_PCRTREVIEW',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				)					
			);
			break;
		case 'pcr_extension': case 'view_pcr_extension':
			$ret = array(
				'table'=>'TBLPCR_EXTENSION', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_EXTENSION',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				)					
			);
			break;
		case 'pcr_actions': case 'view_pcr_actions':
			$ret = array(
				'table'=>'TBLPCR_EVALUATION', // to set the supervisor id - only for northern ops
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_ACTIONS',
				'ACTIONS'=>array(
					'table'=>'TBLPCR_ACTIONS', 
					'id'=>'PCR_ID',
					'view'=> 'VIEW_PCR_ACTIONS_DETAILS'
				)
			);
			break;
		case 'pcr_sign_off': case 'view_pcr_sign_off':
			$ret = array(
				'table'=>'TBLPCR_SIGNOFF', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_SIGNOFF',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				)
			);
			break;
		case 'pcr_post_audit': case 'view_pcr_post_audit':
			$ret = array(
				'table'=>'TBLPCR_POSTAUDIT', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_POSTAUDIT',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				)
			);
			break;
		default:
			$ret = array(
				'table'=>'TBLPCR_DETAILS', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_SEARCH',
				'DOCUMENTLOCATION'=>array(
					'table'=>'TBLMULTIDOCUMENTS', 
					'id'=>'REPORT_ID'
				),
				'Reason_For_Request'=>array(
					'table'=>'TBLPCR_CHECKBOXVALUES', 
					'id'=>'PCRID'
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($page='search')
{
	$ret = array();
	global $id, $RECORD;
	switch($page)
	{
		case 'results':
		case 'search':
			$ret = array(
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 
			  	'CRITICALITY_ID' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_form_draw_pcr_impact_level_dd', 'col'=>2, 'col_span'=>1), 				
				'ORIGINATOR_ID' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
				'AREA_DESCRIPTION' => array('label'=>'Area', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1), 
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>1),
				'OVERALL_STATUS' => array('label'=>'PCR Status', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_pcr_status_types_dd', 'col'=>2, 'col_span'=>1), 
				//'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_form_draw_pcr_type_of_change_dd', 'col'=>1, 'col_span'=>1), 
				'PCR_TYPE' => array('label'=>'Type Of PCR', 'delim'=>"'", 'value'=>'', 'func'=>'html_draw_pcr_type_dd', 'col'=>2, 'col_span'=>1), 
				'EQUIPMENT_ID' => array('label'=>'Equipment No', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 				
				'TCP' => array('label'=>'TCP', 'value'=>'', 'delim'=>"'", 'func'=>'html_draw_yes_no_dd', 'col'=>2, 'col_span'=>2),
				'DUE_DATE_FROM' => array('label'=>'Due Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'DUE_DATE_TO' => array('label'=>'Due Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
				'PCR_DATE_FROM' => array('label'=>'Raised Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'PCR_DATE_TO' => array('label'=>'Raised Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			
			);
			break;
		case 'main': 
		case 'view':
			$ret =  array(
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
				'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
				'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
				'STATUS' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'new':
			$sess = $_SESSION['user_details'];
			$ret =  array(
				array(
					'CRITICALITY_ID' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_form_draw_pcr_impact_level_dd', 'col'=>1, 'col_span'=>2),
					'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>1),
					'TCP' => array('label'=>'TCP', 'value'=>'', 'func'=>'html_draw_yes_no_radioset', 'col'=>2, 'col_span'=>1),
					'ORIGINATOR_ID' => array('label'=>'Originator', 'value'=>$sess['user_id'], 'func'=>'html_draw_pcr_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1),
					'EMPLOYEE_NO' => array('label'=>'Employee No', 'value'=>$sess['user_id'], 'func'=>'html_form_show_hidden', 'params'=>' style=\"width:100%;border:none;\" ', 'col'=>2, 'col_span'=>1),
					'SITE_ID' => array('label'=>'Site', 'value'=>$sess['site_id'], 'func'=>'html_draw_site_linked', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>$sess['department_id'], 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1), 
					'SECTION_ID' => array('label'=>'Section', 'value'=>$sess['section_id'], 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>1), 
					'POSITION_ID' => array('label'=>'Position', 'value'=>$sess['position_id'], 'func'=>'html_form_draw_position', 'col'=>2, 'col_span'=>1),
					'PCR_DATE' => array('label'=>'Date Raised', 'value'=>date(CATS_APP_DATE_FORMAT_SHORT), 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_form_draw_pcr_type_of_change_dd', 'col'=>1, 'col_span'=>2), 
					//'Reason_For_Request' => array('label'=>'Reason For Change', 'value'=>NULL, 'func'=>'html_draw_pcr_reason_for_change_checkboxset', 'col'=>1, 'col_span'=>2),
					'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AREA_ID' => array('label'=>'Area', 'value'=>NULL, 'func'=>'html_draw_pcr_site_area_dd', 'col'=>1, 'col_span'=>1),
					'EQUIPMENT_ID' => array('label'=>'Equipment No', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
					'PROCESS_STEP' => array('label'=>'', 'value'=>'PCR Registration', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'PROCESSSTEP_STATUS' => array('label'=>'', 'value'=>'Entered', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'OVERALL_STATUS' => array('label'=>'', 'value'=>'Open', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = 0"
					)
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'WO_NUMBER' => array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALREVIEW_REQUIRED' => array('label'=>'Are Initial Stakeholders/Expert Reviewers Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.GO_TO_EVALUATION);\" ', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1)
				)
			);
			break;
			
		case 'pcr_details':
			$ret =  array(
				array(
					'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'CRITICALITY_ID' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_form_draw_pcr_impact_level_dd', 'col'=>1, 'col_span'=>2),
					'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>1),
					'TCP' => array('label'=>'TCP', 'value'=>'', 'func'=>'html_draw_yes_no_radioset', 'col'=>2, 'col_span'=>1),
					'ORIGINATOR_ID' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_draw_pcr_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1),
					'EMPLOYEE_NO' => array('label'=>'Employee No', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'params'=>' style=\"width:100%;border:none;\" ', 'col'=>2, 'col_span'=>1),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_site_linked', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1), 
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>1), 
					'POSITION_ID' => array('label'=>'Position', 'value'=>'', 'func'=>'html_form_draw_position', 'col'=>2, 'col_span'=>1),
					'PCR_DATE' => array('label'=>'Date Raised', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_form_draw_pcr_type_of_change_dd', 'col'=>1, 'col_span'=>2), 
					//'Reason_For_Request' => array('label'=>'Reason For Change', 'value'=>NULL, 'func'=>'html_draw_pcr_reason_for_change_checkboxset', 'col'=>1, 'col_span'=>2),
					'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AREA_ID' => array('label'=>'Area', 'value'=>NULL, 'func'=>'html_draw_pcredit_site_area_dd', 'col'=>1, 'col_span'=>1),
					'EQUIPMENT_ID' => array('label'=>'Equipment No', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
					'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = $id"
					)
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'WO_NUMBER' => array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALREVIEW_REQUIRED' => array('label'=>'Are Initial Stakeholders/Expert Reviewers Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.GO_TO_EVALUATION);\" ', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1)
				)
			);
			break;
		case 'view_pcr_details':
//"PCR_ID","PCR_DISPLAYID","CRITICALITY_ID","PCR_TYPE","CHANGETYPE_ID","ORIGINATOR_ID","EMPLOYEE_NO","SITE_ID","DEPARTMENT_ID","SECTION_ID","POSITION_ID","CREW_ID","PCR_DATE","OPERATION_ID","TITLE","DESCRIPTION","AREA_ID",
//"AREA","EQUIPMENT_ID","AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2","WO_NUMBER","EVALUATION_ID","INITIALREVIEW_REQUIRED","MEETING_DATE","PROCESS_STEP","PROCESSSTEP_STATUS","OVERALL_STATUS","UNIT","REASSIGNEDTO_ID","COMMENTS","ORIGINATORNAME","AUTHORITIYNAME_1","AUTHORITIYNAME_2","EVALUATIONNAME","REASSIGNEDTO","IMPACTLEVEL","TYPEOFCHANGE","OPERATION","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","POSITION_DESCRIPTION","SECTION_DESCRIPTION","TCP","ACTIONS_OPEN","ACTIONS_CLOSED","MAKE_PERMANENT","DATE_MAKE_PERMANENT","EXTEND_PERMANENT","DATE_EXTEND_PERMANENT","DUE_DATE"
			$ret =  array(
				array(
					'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					//'PCR_ID' => array('label'=>'PCR ID', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'TITLE' => array('label'=>'Title', 'value'=>'N/A', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>'N/A', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'TCP' => array('label'=>'TCP', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					//'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'ORIGINATORNAME' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'POSITION_DESCRIPTION' => array('label'=>'Position', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'PCR_DATE' => array('label'=>'Date Raised', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					//'REASON_FOR_CHANGE' => array('label'=>'Reason For Change', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AREA_DESCRIPTION' => array('label'=>'Area', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'EQUIPMENT_ID' => array('label'=>'Equipment No', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments  where FormType = 'pcr_details' AND Report_Id = $id",
						'params2'=>"READONLY"
					)	
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYNAME_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYNAME_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'WO_NUMBER' => array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'INITIALREVIEW_REQUIRED' => array('label'=>'Are Initial Stakeholders/Expert Reviewers Required?', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>' onclick=\"check_other_radio(this,this.form.GO_TO_EVALUATION);\" ', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>2, 'col_span'=>1)
				)

			);
			break;

		case 'pcr_initial_review':
			$ret = array( 
				array(
					'reviewers' => array('label'=>'Select Initial Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_initial_reviewers_sequence')
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1)
				)
			);
			break;
		case 'view_pcr_initial_review':
			$ret = array( 
				array(
					'reviewers' => array('label'=>'Select Initial Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_initial_reviewers_sequence')
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>2, 'col_span'=>1)
				)
			);
			break;		

		case 'pcr_evaluation':
			$arr_main_form = array(
				'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
						'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
					),
					'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = $id"
					//'sql'=>"SELECT Document_Type, Document_Location, 'delete:javascript:remove_sequence_row(this, ::)' from tblPCR_Evaluation_Documents  where PCR_Id = $id"
				)
			);
			// Kwinana only fields
// Changes for Work Item #12887
//			if($RECORD['SITE_ID']==3){
				$arr_main_form['ASSESSMENT_SCORE']=array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_form_draw_pcr_assessment_score_dd', 'params'=>' onchange=\"sync_dd(this,this.form.ASSESSMENT_LEVEL);\" ', 'col'=>1, 'col_span'=>1);
				$arr_main_form['ASSESSMENT_LEVEL']=array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_form_draw_pcr_assessment_level_dd', 'params'=>' onchange=\"sync_dd(this,this.form.ASSESSMENT_SCORE);\" ', 'col'=>2, 'col_span'=>1);
//			}
			
			$arr_main_form['EVALUATION_NAME_ID'] = array('label'=>'Name', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1);
			$arr_main_form['EVALUATION_DATE'] = array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1);
			$arr_main_form['EVALUATION_COMMENTS'] = array('label'=>'Evaluation Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2);
			$arr_main_form['WO_NUMBER'] = array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2);
			
			// Kwinana only fields
// Changes for Work Item #12887
//			if($RECORD['SITE_ID']==3){
				$arr_main_form = array_merge($arr_main_form,
					array(
						'AREA_REVIEW_REQ' => array('label'=>'Is an Area Review Required?', 'value'=>'Yes', 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),	
						'TEAM_REVIEW_REQ' => array('label'=>'Is PCRT Review Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),	
						'EXT_REVIEW_REQ' => array('label'=>'Is an Extension Review Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2)
					)
				);
//			}
			
			$ret =  array($arr_main_form
				,
				array(
					'row_message' => array('label'=>'DO NOT APPROVE WITHOUT VIEWING APPROPRIATE RISK ASSESSMENTS', 'group'=>'row_header'), 
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'AREA_REVIEW_REQ' => array('label'=>'Is an Area Review Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),	
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
				)
			);
			break;
		case 'view_pcr_evaluation':
			$arr_main_form = array(
				'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
						'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
					),
					'sql'=>"SELECT DocumentLocation from tblMultiDocuments  where FormType = 'pcr_evaluation' AND Report_Id = $id",
					'params2'=>"READONLY"
				)
			);
			// Kwinana only fields
// Changes for Work Item #12887
//			if($RECORD['SITE_ID']==3){
				$arr_main_form['ASSESSMENT_SCORE']=array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_form_display_pcr_assessment_score', 'col'=>1, 'col_span'=>1);
				$arr_main_form['ASSESSMENT_LEVEL']=array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1);
//			}
			
			$arr_main_form['EVALUATION_NAME_ID'] = array('label'=>'Name', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1);
			$arr_main_form['EVALUATION_DATE'] = array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1);
			$arr_main_form['EVALUATION_COMMENTS'] = array('label'=>'Evaluation Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2);
			$arr_main_form['WO_NUMBER'] = array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2);
			
			// Kwinana only fields
// Changes for Work Item #12887
//			if($RECORD['SITE_ID']==3){
				$arr_main_form = array_merge($arr_main_form,
					array(
						'AREA_REVIEW_REQ' => array('label'=>'Is an Area Review Required?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),	
						'TEAM_REVIEW_REQ' => array('label'=>'Is PCRT Review Required?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),	
						'EXT_REVIEW_REQ' => array('label'=>'Is an Extension Review Required?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
					)
				);
//			}
			
			$ret =  array($arr_main_form
				,
				array(
					'row_message' => array('label'=>'DO NOT APPROVE WITHOUT VIEWING APPROPRIATE RISK ASSESSMENTS', 'group'=>'row_header'), 
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'AREA_REVIEW_REQ' => array('label'=>'Is an Area Review Required?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),	
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1)
				)
			);
			break;

		case 'pcr_area_review':
			$arr_main_form = array(
				'reviewers' => array('label'=>'Select Area Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_area_reviewers_sequence')
			);
			if($RECORD['CRITICALITY']=='NC'){
				$arr_main_form = array_merge($arr_main_form,
					array(
						/* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
						'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
						'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>2),
						'MAKE_PERMANENT' => array('label'=>'Make Trial Permanent?', 'value'=>NULL, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>1),
						'DATE_MAKE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
						'EXTEND_PERMANENT' => array('label'=>'Extend Permanent Changes To All Lines? ', 'value'=>NULL, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>1),
						'DATE_EXTEND' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
						'ASSESSMENT_SCORE' => array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_pcr_assessment_score_display_value', 'col'=>1, 'col_span'=>1),
						'ASSESSMENT_LEVEL' => array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
						'CONFIRMATION' => array('label'=>'Confirmation of Assessment Level by PCRT', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>1),
						'REVISED_SCORE' => array('label'=>'Revised Score', 'value'=>NULL, 'func'=>'html_draw_revised_score_field', 'col'=>2, 'col_span'=>1),
						'ACTION' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_draw_pcr_action_taken_radioset', 'col'=>1, 'col_span'=>2),
						'COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
						'DUE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'html_draw_pcr_due_by_radioset', 'col'=>1, 'col_span'=>1),
						'DUE_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
						'AFE_NUMBER' => array('label'=>'AFE Number (if applicable)', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
						/* END RECOMMEDATION CONTROL */
					)
				);
			}
			$ret =  array(
			  $arr_main_form,
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'GO_TO_ACTION' => array('label'=>'Go Directly to Actions', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'SUPERVISOR_ID' => array('label'=>'Who\'s the Work Supervisor', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1)
				)
			);
			break;	
		case 'view_pcr_area_review':
			$arr_main_form = array(
				'reviewers' => array('label'=>'Select Area Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_area_reviewers_sequence')
			);
			if($RECORD['CRITICALITY']=='NC'){
				$arr_main_form = array_merge($arr_main_form,
					array(
						/* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
						'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
						'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
						'MAKE_PERMANENT' => array('label'=>'Make Trial Permanent?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
						'DATE_MAKE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
						'EXTEND_PERMANENT' => array('label'=>'Extend Permanent Changes To All Lines? ', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
						'DATE_EXTEND' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
						'ASSESSMENT_SCORE' => array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_pcr_assessment_score_display_value', 'col'=>1, 'col_span'=>1),
						'ASSESSMENT_LEVEL' => array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
						'CONFIRMATION' => array('label'=>'Confirmation of Assessment Level by PCRT', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
						'REVISED_SCORE' => array('label'=>'Revised Score', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
						'ACTION' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
						'COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
						'DUE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
						'DUE_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
						'AFE_NUMBER' => array('label'=>'AFE Number (if applicable)', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
						/* END RECOMMEDATION CONTROL */
					)
				);
			}
			$ret =  array(
			  $arr_main_form,
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'GO_TO_ACTION' => array('label'=>'Go Directly to Actions', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'SUPERVISOR_ID' => array('label'=>'Who\'s the Work Supervisor', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>2, 'col_span'=>1)
				)
			);
			break;			

		case 'pcr_pcrt_review':
			// $show_reviewers = ($RECORD['REVIEW_TYPE']=='PCRT Walkthrough')?'':'style=\"display:none;\"';comment dev
			$show_reviewers = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='PCRT Walkthrough')?'':'style=\"display:none;\"';
			$ret =  array(			    
				array(
					'TYPE_OF_REVIEW' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_draw_pcr_review_type_radioset', 'col'=>1, 'col_span'=>2),
					'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = $id"
					),
					'reviewers' => array('label'=>'Select PCRT Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_pcrt_reviewers_sequence','params'=>$show_reviewers),
					/* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
					'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
					'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>2),
					'MAKE_PERMANENT' => array('label'=>'Make Trial Permanent?', 'value'=>NULL, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>1),
					'DATE_MAKE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'EXTEND_PERMANENT' => array('label'=>'Extend Permanent Changes To All Lines? ', 'value'=>NULL, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>1),
					'DATE_EXTEND' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'ASSESSMENT_SCORE' => array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_pcr_assessment_score_display_value', 'col'=>1, 'col_span'=>1),
					'ASSESSMENT_LEVEL' => array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'CONFIRMATION' => array('label'=>'Confirmation of Assessment Level by PCRT', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>1),
					'REVISED_SCORE' => array('label'=>'Revised Score', 'value'=>NULL, 'func'=>'html_draw_revised_score_field', 'col'=>2, 'col_span'=>1),
					'ACTION' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_draw_pcr_action_taken_radioset', 'col'=>1, 'col_span'=>2),
					'COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'DUE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'html_draw_pcr_due_by_radioset', 'col'=>1, 'col_span'=>1),
					'DUE_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AFE_NUMBER' => array('label'=>'AFE Number (if applicable)', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
					/* END RECOMMEDATION CONTROL */
				) // no authority to proceed
			);
			break;
		case 'view_pcr_pcrt_review':
			// $show_reviewers = ($RECORD['REVIEW_TYPE']=='PCRT Walkthrough')?'':'style=\"display:none;\"'; comment dev
		$show_reviewers = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='PCRT Walkthrough')?'':'style=\"display:none;\"';
			$ret =  array(			    
				array(
					'TYPE_OF_REVIEW' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_draw_pcr_meeting_link', 'col'=>1, 'col_span'=>2),
					'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments  where FormType = 'pcr_pcrt_review' AND Report_Id = $id",
						'params2'=>"READONLY"
					),
					'reviewers' => array('label'=>'Select PCRT Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_pcrt_reviewers_sequence','params'=>$show_reviewers),
					/* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
					'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
					'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'MAKE_PERMANENT' => array('label'=>'Make Trial Permanent?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'DATE_MAKE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'EXTEND_PERMANENT' => array('label'=>'Extend Permanent Changes To All Lines? ', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'DATE_EXTEND' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'ASSESSMENT_SCORE' => array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_pcr_assessment_score_display_value', 'col'=>1, 'col_span'=>1),
					'ASSESSMENT_LEVEL' => array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'CONFIRMATION' => array('label'=>'Confirmation of Assessment Level by PCRT', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'REVISED_SCORE' => array('label'=>'Revised Score', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'ACTION' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'DUE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'DUE_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AFE_NUMBER' => array('label'=>'AFE Number (if applicable)', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
					/* END RECOMMEDATION CONTROL */
				) // no authority to proceed
			);
			break;
			
		case 'pcr_extension':
			// $show_reviewers = ($RECORD['REVIEW_TYPE']=='Area')?'':'style=\"display:none;\"';comment dev
			$show_reviewers = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='Area')?'':'style=\"display:none;\"';

			$ret =  array(			    
				array(
					'TYPE_OF_REVIEW' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_draw_pcr_review_type_radioset', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = $id"
					),
					'reviewers' => array('label'=>'Select Extension Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_extension_reviewers_sequence', 'params'=>$show_reviewers),
					/* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
					'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
					'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>2),
					'ACTION' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_draw_pcr_extension_action_taken_radioset', 'col'=>1, 'col_span'=>2),
					'COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'DUE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'html_draw_pcr_due_by_radioset', 'col'=>1, 'col_span'=>1),
					'DUE_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
					/* END RECOMMEDATION CONTROL */
				) // no authority to proceed
			);
			break;
		case 'view_pcr_extension':
			$show_reviewers = ($RECORD['REVIEW_TYPE']=='Area')?'':'style=\"display:none;\"';

			$ret =  array(			    
				array(
					'TYPE_OF_REVIEW' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments  where FormType = 'pcr_extension' AND Report_Id = $id",
						'params2'=>"READONLY"
					),
					'reviewers' => array('label'=>'Select Extension Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_extension_reviewers_sequence', 'params'=>$show_reviewers),
					/* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
					'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
					'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTION' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'DUE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'DUE_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1)
					/* END RECOMMEDATION CONTROL */
				) // no authority to proceed
			);
			break;
				
		case 'pcr_actions':
			$ret =  array(
				'SITE_ID' => array('label'=>'Site ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'stage_1' => array('label'=>'Stage 1', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_actions_stage1_sequence'),
				'stage_2' => array('label'=>'Stage 2', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_actions_stage2_sequence'),
				'stage_3' => array('label'=>'Stage 3', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_actions_stage3_sequence')
			); // no authority to proceed
// Changes for Work Item #12887
			/* if($RECORD['SITE_ID']!=3){
				$ret = array_merge(
					array(
						'SUPERVISOR_ID' => array('label'=>'Actions Supervisor', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2)
					),
					$ret
				);
			} */
			break;
		case 'view_pcr_actions':
			$ret =  array(
				'SITE_ID' => array('label'=>'Site ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'stage_1' => array('label'=>'Stage 1', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_actions_stage1_sequence'),
				'stage_2' => array('label'=>'Stage 2', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_actions_stage2_sequence'),
				'stage_3' => array('label'=>'Stage 3', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_actions_stage3_sequence')
			); // no authority to proceed
// Changes for Work Item #12887
			/* if($RECORD['SITE_ID']!=3){
				$ret = array_merge(
					array(
						'SUPERVISOR_ID' => array('label'=>'Actions Supervisor', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>2)
					),
					$ret
				);
			} */
			break;	
				
		case 'pcr_sign_off':
// Changes for Work Item #12887
//			if($RECORD['SITE_ID']==3){
				// $show_reviewers = ($RECORD['REVIEW_TYPE']=='Area')?'':'style=\"display:none;\"';comment dev
				$show_reviewers = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='Area')?'':'style=\"display:none;\"';
				
				$arr_main_form = array(
					/* START GENERAL */
					'TYPE_OF_REVIEW' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_draw_pcr_review_type_radioset', 'col'=>1, 'col_span'=>2),	
					'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Other Minutes where this PCR is referenced', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = $id"
					),
					/* END GENERAL */
					// 'reviewers' => array('label'=>'Select Sign Off Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_get_pcr_signoff_reviewers_sequence','params'=>$show_reviewers),
					/* START RECOMMENDATION */
					'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
					'SIGN_OFF' => array('label'=>'PCR Team Sign-Off', 'value'=>NULL, 'func'=>'html_draw_pcr_sign_off_radioset', 'col'=>1, 'col_span'=>1),	
					'SIGNED_OFF_DATE' => array('label'=>'Signed-Off Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					/* END RECOMMENDATION */
					
					/* START PCR SIGN-OFF / CLOSURE */
					'row_closeure' => array('label'=>'PCR Sign-Off / Closure', 'group'=>'row_header'), 
					'AUDITBY_ID' => array('label'=>'Post Audit Required to be conducted by', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
					'DUE_DATE' => array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
					/* END PCR SIGN-OFF / CLOSURE */
				);
/*			}else{
				$arr_main_form = array(
					'row_closeure' => array('label'=>'Sign-Off / Closure', 'group'=>'row_header'), 
					'ENGINEER_ID' => array('label'=>'Engineer', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
					'ENGINEER_APPROVE' => array('label'=>'Engineer Approval', 'value'=>NULL, 'func'=>'html_draw_approve_reject_radioset', 'col'=>1, 'col_span'=>1), 
					'ENGINEER_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'SUPER_ID' => array('label'=>'Superintendent', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
					'SUPER_APPROVE' => array('label'=>'Superintendent Approval', 'value'=>NULL, 'func'=>'html_draw_approve_reject_radioset', 'col'=>1, 'col_span'=>1), 
					'SUPER_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1)
				);
			}
*/
			$ret =  array(			    
				$arr_main_form,
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'POST_AUDIT_REQUIRED' => array('label'=>'Post Audit Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>1),
					'POST_AUDIT_LEADER' => array('label'=>'Who leads Post Audit?', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1)
				)
			);
			break;
		case 'view_pcr_sign_off':
// Changes for Work Item #12887
//			if($RECORD['SITE_ID']==3){
				$show_reviewers = ($RECORD['REVIEW_TYPE']=='Area')?'':'style=\"display:none;\"';
				
				$arr_main_form = array(
					/* START GENERAL */
					'TYPE_OF_REVIEW' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),	
					'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'Document' => array('label'=>'Other Minutes where this PCR is referenced', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments  where FormType = 'pcr_sign_off'  AND Report_Id = $id",
						'params2'=>"READONLY"
					),
					/* END GENERAL */
					// 'reviewers' => array('label'=>'Select Sign Off Reviewers', 'group'=>'row_sequence', 'ext'=>true, 'func'=>'html_display_pcr_signoff_reviewers_sequence','params'=>$show_reviewers),
					/* START RECOMMENDATION */
					'row_recommend' => array('label'=>'Recommendation', 'group'=>'row_header'), 
					'SIGN_OFF' => array('label'=>'PCR Team Sign-Off', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
					'SIGNED_OFF_DATE' => array('label'=>'Signed-Off Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					/* END RECOMMENDATION */
					
					/* START PCR SIGN-OFF / CLOSURE */
					'row_closeure' => array('label'=>'PCR Sign-Off / Closure', 'group'=>'row_header'), 
					'AUDITBY_ID' => array('label'=>'Post Audit Required to be conducted by', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1), 
					'DUE_DATE' => array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1)
					/* END PCR SIGN-OFF / CLOSURE */
				);
/*			}else{
				$arr_main_form = array(
					'row_closeure' => array('label'=>'Sign-Off / Closure', 'group'=>'row_header'), 
					'ENGINEER_ID' => array('label'=>'Engineer', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>2), 
					'ENGINEER_APPROVE' => array('label'=>'Engineer Approval', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'ENGINEER_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'SUPER_ID' => array('label'=>'Superintendent', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>2), 
					'SUPER_APPROVE' => array('label'=>'Superintendent Approval', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'SUPER_DATE' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1)
				);
			}
*/
			$ret =  array(			    
				$arr_main_form,
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'POST_AUDIT_REQUIRED' => array('label'=>'Post Audit Required?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'POST_AUDIT_LEADER' => array('label'=>'Who leads Post Audit?', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>2, 'col_span'=>1)
				)
			);
			break;	
				
		case 'pcr_post_audit':
			$ret =  array(
				array(
					'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = '$page' AND Report_Id = $id"
					),
					'AUDIT_COMMENTS' => array('label'=>'Audit Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUDIT_DATE' => array('label'=>'Audit Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'AUDIT_COMPLETED_BY_ID' => array('label'=>'Audit Completed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>2, 'col_span'=>1)
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_draw_authority_status_radioset', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'WO_NUMBER' => array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALREVIEW_REQUIRED' => array('label'=>'Are Initial Stakeholders/Expert Reviewers Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.GO_TO_EVALUATION);\" ', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1),
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1)
				)
			);
			break;
		case 'view_pcr_post_audit':
			$ret =  array(
				array(
					'Document' => array('label'=>'Supporting document / Comments', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments  where FormType = 'pcr_post_audit' AND Report_Id = $id",
						'params2'=>"READONLY"
					),
					'AUDIT_COMMENTS' => array('label'=>'Audit Comments', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUDIT_DATE' => array('label'=>'Audit Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'AUDIT_COMPLETED_BY_ID' => array('label'=>'Audit Completed By', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>2, 'col_span'=>1)
				),
				array(
					'row_authority' => array('label'=>'Authority To Proceed', 'group'=>'row_header'), 
					'AUTHORITYID_1' => array('label'=>'Authority 1', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_1' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_1' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_1' => array('label'=>'Authority 1 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYID_2' => array('label'=>'Authority 2', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1),
					'AUTHORITYDATE_2' => array('label'=>'Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'AUTHORITYSTATUS_2' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AUTHORITYCOMMENT_2' => array('label'=>'Authority 2 Comment', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_requirements' => array('label'=>'Additional Requirements', 'group'=>'row_header'), 
					'WO_NUMBER' => array('label'=>'Parent PCR W/O No.', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'INITIALREVIEW_REQUIRED' => array('label'=>'Are Initial Stakeholders/Expert Reviewers Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'params'=>' onclick=\"check_other_radio(this,this.form.GO_TO_EVALUATION);\" ', 'col'=>1, 'col_span'=>1),
					'MEETING_DATE' => array('label'=>'Next Review Meeting', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'GO_TO_EVALUATION' => array('label'=>'Go Directly to Evaluation', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>' onclick=\"check_other_radio(this,this.form.INITIALREVIEW_REQUIRED);\" ', 'col'=>1, 'col_span'=>1),
					'EVALUATION_ID' => array('label'=>'Who\'s performing the evaluation', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>2, 'col_span'=>1)
				)
			);
			break;	

	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'PCR_DISPLAYID',"text" => 'PCR_DISPLAYID');
	$ret[] = array("id" => 'TITLE',"text" => 'TITLE');
	$ret[] = array("id" => 'IMPACTLEVEL',"text" => 'IMPACTLEVEL');
	$ret[] = array("id" => 'ORIGINATORNAME',"text" => 'ORIGINATORNAME');
	$ret[] = array("id" => 'SITE_DESCRIPTION',"text" => 'SITE_DESCRIPTION');
	$ret[] = array("id" => 'DEPARTMENT_DESCRIPTION',"text" => 'DEPARTMENT_DESCRIPTION');
	$ret[] = array("id" => 'PCR_DATE',"text" => 'PCR_DATE');
	$ret[] = array("id" => 'ACTIONS_OPEN',"text" => 'ACTIONS_OPEN');
	$ret[] = array("id" => 'ACTIONS_CLOSED',"text" => 'ACTIONS_CLOSED');
	return $ret;
}


if(!isset($do_not_set_properties)){
	// Fix page parameter
	if($p=='post')
		$pg=(isset($a) && $a=='add')?'new':'edit';
	else
		$pg=$p;
	
	// Call the functions and store the results in the following variables
	$GENERAL = getPropertiesGeneral($pg);
	$TABLES  = getPropertiesTables($pg);
	$FIELDS  = getPropertiesFields($pg);
}
?>