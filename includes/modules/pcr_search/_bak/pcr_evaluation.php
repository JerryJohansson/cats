<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');


// Get the PCR Evaluation Details
$PCR_Evaluation_Details = Get_PCR_Evaluation("3626");


?>




<SCRIPT LANGUAGE="JavaScript" SRC="js/MultiDocuments.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";//used for the remote scripting when getting the managed by site and department

//stuff we do when the form loads
function StartUp()
{
	// nothing yet ;)
	
	SelectDropDown('Evaluation_Name_Id', "10610");
	SelectAuthority();
	
	syncAssessment(element("Assessment_Score"),element("Assessment_Level"));
}




//convert the dates into a string format before saving them to the database
function ChangeDates()
{	
	var errors = PrepareDatesOut('Evaluation_Date','Modified_Date');
}



//enable all the form fields so we can save their values
function EnableFields()
{
	var x = document.frmData.elements.length;
	for(var i=0;i<x;i++){
		if(document.frmData.elements[i].name != "BrowseDialog"){
			document.frmData.elements[i].disabled = false;
		}
	}
}





//stuff we do when we submit the form
function SubmitForm()
{
	ChangeDates();
}

var aOptions = [];
var aValues = [];





function GetDocumentTypes(sMatch)
{
	var i=0,s="";
	for(i=0;i<aOptions.length;i++){
		sSelected = (aOptions[i]==sMatch) ? " selected " : "" ;
		s += "<option value=\"" + aOptions[i] + "\" " + sSelected + ">" + aOptions[i] + "</option>";
	}
	return s;
}



function GetFileLocation(item,txt)
{
	var i;
	for(i=0;i<aOptions.length;i++){
		if(aOptions[i]==item.options[item.selectedIndex].value){
			if(checkFileLocation(txt.value)){
				txt.value = aValues[i];
				break;
			}
		}
	}
}




function checkFileLocation(s)
{
	var i,ret=false;
	for(i=0;i<aValues.length;i++){
		if(s==aValues[i]) {
			ret = true;
			break;
		}
	}
	return ret;
}




function syncAssessment(o1,o2)
{
	try{o2.selectedIndex = o1.selectedIndex;}catch(e){}
}




function correctAssessmentLevels(item, f)
{
	var level = f.Assessment_Level;
	var score = f.Assessment_Score;
	var revised_score = item.value;
	var val, aScore=[], selectedIndex = i = 0;
	if(isNaN(revised_score)){
		if(revised_score!=''){
			alert('Please enter a number into the "Revised Score" field.');
			item.value = "";
			item.focus();
		}
	}
	for(i=0;i<score.options.length;i++){
		val = score.options[i].text.replace(/\s/gi,"").replace(/(\<)|-|(\>)/,"$1$2|")+"|"+i;
		if(val!='') aScore.push(val.split("|"));
	}
	for(i=0;i<aScore.length;i++){
		if(isNaN(aScore[i][0])){
			if(eval(revised_score + " " + aScore[i][0] + " " + aScore[i][1])){
				selectedIndex = aScore[i][2];
			}
		}else{
			if( (parseInt(revised_score) > parseInt(aScore[i][0])) && (parseInt(revised_score) < parseInt(aScore[i][1])) ){
				selectedIndex = aScore[i][2];
			}
		}
		if(selectedIndex > 0) break;
	}
	if(selectedIndex > 0) {
		score.selectedIndex = level.selectedIndex = selectedIndex;
	}
}


</script>
<body>
<form action="index.php?m=<?php echo $m;?>&p=post&a=evaluation&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">

<b>PCR Evaluation</b>
<?php include("includes/pcr_details_general.php"); ?>

<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td width="100%"><strong>DO NOT APPROVE WITHOUT VIEWING APPROPRIATE RISK ASSESSMENTS</strong></td>
  </tr>
</table>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr id="rowType_4" >
    <td colspan="4"><table id="tblDocuments" width="97%" cellspacing="4">
      </table>
      <a style="display: none" href="#" id="LoadDoc" target="_blank">Link To File</a> </td>
  </tr>
  <tr id="rowType_6" >
    <td class="normalLabel" colspan="4" height="35" valign="top"><input type="button" class="Button" name="btnDocument" value="Add Another Document" onClick="addDocumentTypeRow('tblDocuments','17%', 'Supporting document / Comments:', 'Document_', 'NumDocuments','3|100')">
    </td>
  </tr>
  <tr >
    <td class="normalLabel" width="17%">Assessment score:</td>
    <td class="normal">
	
<!--
	 <select name="Assessment_Score" class="inputTextBox" onChange="syncAssessment(this,this.form.Assessment_Level);">
        <option></option>
        <option value="585" > VH </option>
        <option value="586" > H </option>
        <option value="587" > M </option>
        <option value="588"  selected > L </option>
      </select>
-->

    <?PHP echo(get_assessment_score_dd($PCR_Evaluation_Details["ASSESSMENT_SCORE"])); ?>
    
	</td>
    <td class="normalLabel" width="17%">Assessment Level:</td>
    <td class="normal">
<!--
	<select name="Assessment_Level" class="inputTextBox" onChange="syncAssessment(this,this.form.Assessment_Score);">
        <option></option>
        <option value="QRA & HAZOP" > QRA & HAZOP </option>
        <option value="HAZOP" > HAZOP </option>
        <option value="Impact Checklist" > Impact Checklist </option>
        <option value="Area/PCRT Review"  selected > Area/PCRT Review </option>
    </select>
-->

    <?PHP echo(get_assessment_level_dd($PCR_Evaluation_Details["ASSESSMENT_LEVEL"])); ?>
    
	</td>
  </tr>
  <tr id="rowType_3" >
    <td colspan="4"><table width="100%" border="1" cellpadding="5" cellspacing="0">
        <tr valign="top">
          <td valign="top" class="normalLabel">Evaluation Comments:</td>
          <td class="normalLabel"><textarea name="Evaluation_Comments" class="inputTextBox" rows="3" cols="60" onBlur="CheckLength(this,0,500);"><?PHP echo($PCR_Evaluation_Details["EVALUATION_COMMENTS"]); ?></textarea>
          </td>
          <td class="normalLabel">Name:</td>
          <td class="normal"><select name="Evaluation_Name_Id" id="Name" class="inputTextBox">
              <option value=""></option>
              <script>
				document.write(ReadActiveEmps());
			  </script>
            </select>
          </td>
          <td class="normalLabel">Date:</td>
          <td class="normal"><input type="text" size="12" class="inputTextBox" name="Evaluation_Date" id="Date" value="<?PHP echo($PCR_Evaluation_Details["EVALUATION_DATE"]); ?>">
            <a href="#" onClick="callCalendar('anchor4','Evaluation_Date');" name="anchor4" id="anchor4"><img src="images/calendar2.jpg" width="30" height="31" align="top" border="0"></a> </td>
        </tr>
      </table></td>
  </tr>
  <tr id="rowType_4" >
    <td colspan="4"><hr/>
      <table width="100%" border="1" cellpadding="5" cellspacing="0">
        <tr>
          <td valign="top" class="normalLabel" width="30%">Is an Area Review required?:</td>
          <td class="normalLabel"><input align="left" type="checkbox" name="Area_Review_Req" <?PHP if (($PCR_Evaluation_Details["AREA_REVIEW_REQ"] == "Yes") || ($sSiteCode == "PI")) { echo("checked"); } ?> value="Yes">
            Yes </td>
        </tr>
        <tr>
          <td valign="top" class="normalLabel">Is PCRT Review required?:</td>
          <td class="normalLabel"><input align="left" type="checkbox" name="Team_Review_Req" <?PHP if (($PCR_Evaluation_Details["TEAM_REVIEW_REQ"] == "Yes") || ($sSiteCode == "PI")) { echo("checked"); } ?> value="Yes">
            Yes </td>
        </tr>
        <tr>
          <td valign="top" class="normalLabel">Is an Extension Review required?:</td>
          <td class="normalLabel"><input align="left" type="checkbox" name="Ext_Review_Req" <?PHP if (($PCR_Evaluation_Details["EXT_REVIEW_REQ"] == "Yes") || ($sSiteCode == "PI")) { echo("checked"); } ?> value="Yes">
            Yes </td>
        </tr>
      </table></td>
  </tr>
</table>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td><span class="normal">
      <input class="Button" type="submit" name="Save" value="Save">
      <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
      <input class="Button" type="reset" name="Reset" value="Reset">
      <input class="inputTextBox" type="hidden" name="MM_update" value="true">
      <input type="hidden" name="NumDocuments" value="0">
      <input class="inputTextBox" type="hidden" name="MM_recordId" value="3671">
      <input type="hidden" name="Modified_Date" value="18/10/2005">
      <input type="hidden" name="Process_Step" value="Evaluation">
      <input type="hidden" name="ProcessStep_Status" value="Performed">
      <input type="hidden" name="Overall_Status" value="Open">
</span></td>
  </tr>
</table>

<?php include("includes/authority_to_proceed_box.php"); ?>

</form>