<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');
?>


<script language="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";//used for the remote scripting when getting the managed by site and department

//convert the dates into a string format before saving them to the database
function ChangeDates(){  
	var x = document.frmData.NumReviewers.value;
	for(var i=0;i<x;i++){
		var sDate = "ReviewDate_" + i;
		var objDate = document.getElementById(sDate);
		objDate.value = ConvertDate(sDate, '/', 'AUS');
	}	
	document.frmData.Modified_Date.value = ConvertDate('Modified_Date', '/', 'AUS');
	document.frmData.PermanentDate.value = ConvertDate('PermanentDate', '/', 'AUS');
	document.frmData.ExtendDate.value = ConvertDate('ExtendDate', '/', 'AUS');
	document.frmData.ActionDate.value = ConvertDate('ActionDate', '/', 'AUS');
	document.frmData.MeetingDate.value = ConvertDate('MeetingDate', '/', 'AUS');
}

//select the required reviewer from the drop down boxes
function SelectReviewers(){
	var x = document.frmData.NumReviewers.value;
	for(var i=0;i<x;i++){
		var sDropDown = "ReviewerId_" + i;
		var objOld = document.getElementById("OldAReviewerId_" + i);
		var iDefault = objOld.value;
		if(iDefault == ""){//if a reviewer hasn't been saved yet we use the default reviewer
			var objDefault = document.getElementById("DefaultId_" + i);
			var iDefault = objDefault.value;
		}
		SelectDropDown(sDropDown,iDefault);
	}
	
}

//stuff we do when the form loads
function StartUp(){
	ShowTeam();
	SelectReviewers();
}

function ShowApprovers(sCheck, sRow, sRequired){
	var objCheck = document.getElementById(sCheck);
	var objRow = document.getElementById(sRow);
	var objRequired = document.getElementById(sRequired);
	var Required = objRequired.value;
	
	if(objCheck.checked == false){
		switch(Required){
			case "Mandatory" :
				objCheck.checked = true;
				alert("This Reviewer is Mandatory, you cannot change this selection");
				break;
			case "Recommended" :
				alert("You can change this selection, but this Reviewer is Recommended.");
				objRow.style.display = "none";
				break;
			case "Optional" :
				objRow.style.display = "none";
				break;
		}		
	}else{
		objRow.style.display = "block";
	}
}

//enable all the form fields so we can save their values
function EnableFields(){
	var x = document.frmData.elements.length;
	for(var i=0;i<x;i++){
		if(document.frmData.elements[i].name != "BrowseDialog"){
			document.frmData.elements[i].disabled = false;
		}
	}
}

//stuff we do when we submit the form
function SubmitForm(){
	ChangeDates();
	EnableFields();
}

//show or hide the reviewers section and the details section depending on what type of review it is
function ShowTeam(){
	var x = document.frmData.NumReviewers.value;
	if(document.frmData.Review_Type[0].checked == true){
		for(var i=0;i<x;i++){
			var objRow = document.getElementById("rowReviewerType_" + i);
			objRow.style.display = "none";
		}
		for(var i=1;i<7;i++){
			var objRow = document.getElementById("rowType_" + i);
			objRow.style.display = "block";
		}	
	}else{
		for(var i=0;i<x;i++){
			var objRow = document.getElementById("rowReviewerType_" + i);
			objRow.style.display = "block";
		}
		for(var i=1;i<7;i++){
			var objRow = document.getElementById("rowType_" + i);
			objRow.style.display = "none";
		}
	}
}

</script>



<body>

<form action="index.php?m=<?php echo $m;?>&p=post&a=pcrt_review&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">


<b>PCR PCRT Review</b>
<?php include("includes/pcr_details_general.php"); ?>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="17%">Type Of Review:</td>
    <td class="normal" width="32%" align="left"><input align="left" type="radio" onClick="ShowTeam()" name="Review_Type"  value="PCRT Meeting">
      PCRT Meeting
      <input align="left" type="radio" onClick="ShowTeam()" name="Review_Type"  value="PCRT Walkthrough">
      PCRT Walkthrough </td>
    <td class="normalLabel" width="12%">&nbsp;<span id="rowType_1">Meeting Date:</span></td>
    <td class="normal">&nbsp;<span id="rowType_2">
      <input type="hidden" size="12" class="inputTextBox" name="MeetingDate" id="Date" value="" onBlur="ValidateDateFields('MeetingDate')">
      <!--<a href="#" onClick="callCalendar('anchor4','MeetingDate');" name="anchor4" id="anchor4"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> -->
    </span> </td>
  </tr>
  <tr id="rowType_3" >
    <td valign="top" class="normalLabel">Comments:</td>
    <td class="normalLabel" colspan="3"><textarea name="Meeting_Comments" class="inputTextBox" rows="3" cols="50" onBlur="CheckLength(this,0,500);"></textarea>
    </td>
  </tr>
  <tr id="rowType_4" >
    <td colspan="4"><table id="tblDocuments" width="97%" cellspacing="4">
    </table></td>
  </tr>
  <tr id="rowType_5" >
    <td class="normalLabel" width="16%">&nbsp;Other minutes where &nbsp;PCR is referenced:</td>
    <td class="normal" colspan="3"><input class="LinkedTextbox" type="text" name="Document_1" size="90" maxlength="255" onDblClick="ClickLink(Document_1, LoadDoc)">
        <input class="Button" type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Document_1')">
        <input type="file" name="BrowseDialog" style="display: none">
      <a style="display: none" href="#" id="LoadDoc" target="_blank">Link To File</a>
        <input type="hidden" name="NumDocuments" value="2">
    </td>
  </tr>
  <tr id="rowType_6" >
    <td class="normalLabel" colspan="4" height="35" valign="top"><input type="button" class="Button" name="btnDocument" value="Add more minutes" onClick="addDocumentRow('tblDocuments','17%', 'Other minutes where PCR is referenced:', 'Document_', 'NumDocuments','90')">
    </td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="15%">Reviewer Type:</td>
    <td class="searchHeader" width="20%"> Environment
      <input type="hidden" name="ReviewerTypeString_0" value="Environment">
        <input type="hidden" name="DetailId_0" value="26">
    </td>
    <td class="normalLabel" width="8%">Required:</td>
    <td class="normal" width="10%"><input type="checkbox" name="Required_0" checked onClick="ShowApprovers('Required_0','rowReviewer_0', 'hdnRequired_0')" value="1">
      Yes
      <input type="hidden" name="hdnRequired_0" value="Mandatory">
    </td>
    <td class="normalLabel" width="13%">Reviewer Name:</td>
    <td class="normal"><input type="hidden" name="OldAReviewerId_0" value="">
        <input type="hidden" name="DefaultId_0" value="10817">
        <select name="ReviewerId_0" class="inputTextBox">
          <option value=""></option>
          <script>
				    document.write(ReadActiveEmps());
				  </script>
        </select>
    </td>
  </tr>
  <tr id="rowReviewer_0" style="display: block" >
    <td colspan="6"><table width="100%" border="1" cellspacing="0" cellpadding="5">
      <tr>
        <td width="15%" class="normalLabel">Date Reviewed:</td>
        <td class="normal" width="20%"><input type="text" size="12"  class="inputTextBox" name="ReviewDate_0" onBlur="ValidateDateFields('ReviewDate_0')" value="">
            <a href="#" onClick="callCalendar('anchor_0','ReviewDate_0');" name="anchor_0" id="anchor_0"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
        <td class="normalLabel" width="8%">Status:</td>
        <td class="normal"><input type="radio"  name="Approved_0"  id="Approved1_0" value="Proceed">
          Proceed
          <input type="radio"  name="Approved_0"  id="Approved2_0" value="Further Info Required">
          Further Info Required
          <input type="radio"  name="Approved_0"  id="Approved3_0" value="Reject">
          Reject </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Comments</td>
      </tr>
      <tr onClick="CheckPassword(0)">
        <td colspan="4" class="normalLabel" ><textarea class="inputTextBox"  name="Comment_0" onBlur="CheckLength(this,0,255);" cols="50" rows="2"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Location of Saved Checklist</td>
      </tr>
      <tr>
        <td colspan="4" class="normal" ><input class="LinkedTextbox"  type="text" name="Checklist_0" size="98" maxlength="255" onDblClick="ClickLink(Checklist_0, LoadDoc)" value="">
              <input class="Button" type="button"  name="btnLocation_0" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_0, LoadDoc)">
              <input class="Button"  type="button" name="btnLocation2" value="Browse..." onClick="Change_Location('Checklist_0')">
              <input type="hidden" name="FileLocation_0" value="">
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<br>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="15%">Reviewer Type:</td>
    <td class="searchHeader" width="20%"> Maintenance
      <input type="hidden" name="ReviewerTypeString_1" value="Maintenance">
        <input type="hidden" name="DetailId_1" value="23">
    </td>
    <td class="normalLabel" width="8%">Required:</td>
    <td class="normal" width="10%"><input type="checkbox" name="Required_1" checked onClick="ShowApprovers('Required_1','rowReviewer_1', 'hdnRequired_1')" value="1">
      Yes
      <input type="hidden" name="hdnRequired_1" value="Mandatory">
    </td>
    <td class="normalLabel" width="13%">Reviewer Name:</td>
    <td class="normal"><input type="hidden" name="OldAReviewerId_1" value="">
        <input type="hidden" name="DefaultId_1" value="10184">
        <select name="ReviewerId_1" class="inputTextBox">
          <option value=""></option>
          <script>
				    document.write(ReadActiveEmps());
				  </script>
        </select>
    </td>
  </tr>
  <tr id="rowReviewer_1" style="display: block" >
    <td colspan="6"><table width="100%" border="1" cellspacing="0" cellpadding="5">
      <tr>
        <td width="15%" class="normalLabel">Date Reviewed:</td>
        <td class="normal" width="20%"><input type="text" size="12"  class="inputTextBox" name="ReviewDate_1" onBlur="ValidateDateFields('ReviewDate_1')" value="">
            <a href="#" onClick="callCalendar('anchor_1','ReviewDate_1');" name="anchor_1" id="anchor_1"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
        <td class="normalLabel" width="8%">Status:</td>
        <td class="normal"><input type="radio"  name="Approved_1"  id="Approved1_1" value="Proceed">
          Proceed
          <input type="radio"  name="Approved_1"  id="Approved2_1" value="Further Info Required">
          Further Info Required
          <input type="radio"  name="Approved_1"  id="Approved3_1" value="Reject">
          Reject </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Comments</td>
      </tr>
      <tr onClick="CheckPassword(1)">
        <td colspan="4" class="normalLabel" ><textarea class="inputTextBox"  name="Comment_1" onBlur="CheckLength(this,0,255);" cols="50" rows="2"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Location of Saved Checklist</td>
      </tr>
      <tr>
        <td colspan="4" class="normal" ><input class="LinkedTextbox"  type="text" name="Checklist_1" size="98" maxlength="255" onDblClick="ClickLink(Checklist_1, LoadDoc)" value="">
              <input class="Button" type="button"  name="btnLocation_1" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_1, LoadDoc)">
              <input class="Button"  type="button" name="btnLocation3" value="Browse..." onClick="Change_Location('Checklist_1')">
              <input type="hidden" name="FileLocation_1" value="">
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<br>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="15%">Reviewer Type:</td>
    <td class="searchHeader" width="20%"> PCR Chair
      <input type="hidden" name="ReviewerTypeString_2" value="PCR Chair">
        <input type="hidden" name="DetailId_2" value="27">
    </td>
    <td class="normalLabel" width="8%">Required:</td>
    <td class="normal" width="10%"><input type="checkbox" name="Required_2" checked onClick="ShowApprovers('Required_2','rowReviewer_2', 'hdnRequired_2')" value="1">
      Yes
      <input type="hidden" name="hdnRequired_2" value="Mandatory">
    </td>
    <td class="normalLabel" width="13%">Reviewer Name:</td>
    <td class="normal"><input type="hidden" name="OldAReviewerId_2" value="">
        <input type="hidden" name="DefaultId_2" value="10603">
        <select name="ReviewerId_2" class="inputTextBox">
          <option value=""></option>
          <script>
				    document.write(ReadActiveEmps());
				  </script>
        </select>
    </td>
  </tr>
  <tr id="rowReviewer_2" style="display: block" >
    <td colspan="6"><table width="100%" border="1" cellspacing="0" cellpadding="5">
      <tr>
        <td width="15%" class="normalLabel">Date Reviewed:</td>
        <td class="normal" width="20%"><input type="text" size="12"  class="inputTextBox" name="ReviewDate_2" onBlur="ValidateDateFields('ReviewDate_2')" value="">
            <a href="#" onClick="callCalendar('anchor_2','ReviewDate_2');" name="anchor_2" id="anchor_2"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
        <td class="normalLabel" width="8%">Status:</td>
        <td class="normal"><input type="radio"  name="Approved_2"  id="Approved1_2" value="Proceed">
          Proceed
          <input type="radio"  name="Approved_2"  id="Approved2_2" value="Further Info Required">
          Further Info Required
          <input type="radio"  name="Approved_2"  id="Approved3_2" value="Reject">
          Reject </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Comments</td>
      </tr>
      <tr onClick="CheckPassword(2)">
        <td colspan="4" class="normalLabel" ><textarea class="inputTextBox"  name="Comment_2" onBlur="CheckLength(this,0,255);" cols="50" rows="2"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Location of Saved Checklist</td>
      </tr>
      <tr>
        <td colspan="4" class="normal" ><input class="LinkedTextbox"  type="text" name="Checklist_2" size="98" maxlength="255" onDblClick="ClickLink(Checklist_2, LoadDoc)" value="">
              <input class="Button" type="button"  name="btnLocation_2" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_2, LoadDoc)">
              <input class="Button"  type="button" name="btnLocation4" value="Browse..." onClick="Change_Location('Checklist_2')">
              <input type="hidden" name="FileLocation_2" value="">
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="15%">Reviewer Type:</td>
    <td class="searchHeader" width="20%"> Production
      <input type="hidden" name="ReviewerTypeString_3" value="Production">
        <input type="hidden" name="DetailId_3" value="24">
    </td>
    <td class="normalLabel" width="8%">Required:</td>
    <td class="normal" width="10%"><input type="checkbox" name="Required_3" checked onClick="ShowApprovers('Required_3','rowReviewer_3', 'hdnRequired_3')" value="1">
      Yes
      <input type="hidden" name="hdnRequired_3" value="Mandatory">
    </td>
    <td class="normalLabel" width="13%">Reviewer Name:</td>
    <td class="normal"><input type="hidden" name="OldAReviewerId_3" value="">
        <input type="hidden" name="DefaultId_3" value="10810">
        <select name="ReviewerId_3" class="inputTextBox">
          <option value=""></option>
          <script>
				    document.write(ReadActiveEmps());
				  </script>
        </select>
    </td>
  </tr>
  <tr id="rowReviewer_3" style="display: block" >
    <td colspan="6"><table width="100%" border="1" cellspacing="0" cellpadding="5">
      <tr>
        <td width="15%" class="normalLabel">Date Reviewed:</td>
        <td class="normal" width="20%"><input type="text" size="12"  class="inputTextBox" name="ReviewDate_3" onBlur="ValidateDateFields('ReviewDate_3')" value="">
            <a href="#" onClick="callCalendar('anchor_3','ReviewDate_3');" name="anchor_3" id="anchor_3"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
        <td class="normalLabel" width="8%">Status:</td>
        <td class="normal"><input type="radio"  name="Approved_3"  id="Approved1_3" value="Proceed">
          Proceed
          <input type="radio"  name="Approved_3"  id="Approved2_3" value="Further Info Required">
          Further Info Required
          <input type="radio"  name="Approved_3"  id="Approved3_3" value="Reject">
          Reject </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Comments</td>
      </tr>
      <tr onClick="CheckPassword(3)">
        <td colspan="4" class="normalLabel" ><textarea class="inputTextBox"  name="Comment_3" onBlur="CheckLength(this,0,255);" cols="50" rows="2"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Location of Saved Checklist</td>
      </tr>
      <tr>
        <td colspan="4" class="normal" ><input class="LinkedTextbox"  type="text" name="Checklist_3" size="98" maxlength="255" onDblClick="ClickLink(Checklist_3, LoadDoc)" value="">
              <input class="Button" type="button"  name="btnLocation_3" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_3, LoadDoc)">
              <input class="Button"  type="button" name="btnLocation5" value="Browse..." onClick="Change_Location('Checklist_3')">
              <input type="hidden" name="FileLocation_3" value="">
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="15%">Reviewer Type:</td>
    <td class="searchHeader" width="20%"> Safety
      <input type="hidden" name="ReviewerTypeString_4" value="Safety">
        <input type="hidden" name="DetailId_4" value="25">
    </td>
    <td class="normalLabel" width="8%">Required:</td>
    <td class="normal" width="10%"><input type="checkbox" name="Required_4" checked onClick="ShowApprovers('Required_4','rowReviewer_4', 'hdnRequired_4')" value="1">
      Yes
      <input type="hidden" name="hdnRequired_4" value="Mandatory">
    </td>
    <td class="normalLabel" width="13%">Reviewer Name:</td>
    <td class="normal"><input type="hidden" name="OldAReviewerId_4" value="">
        <input type="hidden" name="DefaultId_4" value="10959">
        <select name="ReviewerId_4" class="inputTextBox">
          <option value=""></option>
          <script>
				    document.write(ReadActiveEmps());
				  </script>
        </select>
    </td>
  </tr>
  <tr id="rowReviewer_4" style="display: block" >
    <td colspan="6"><table width="100%" border="1" cellspacing="0" cellpadding="5">
      <tr>
        <td width="15%" class="normalLabel">Date Reviewed:</td>
        <td class="normal" width="20%"><input type="text" size="12"  class="inputTextBox" name="ReviewDate_4" onBlur="ValidateDateFields('ReviewDate_4')" value="">
            <a href="#" onClick="callCalendar('anchor_4','ReviewDate_4');" name="anchor_4" id="anchor_4"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
        <td class="normalLabel" width="8%">Status:</td>
        <td class="normal"><input type="radio"  name="Approved_4"  id="Approved1_4" value="Proceed">
          Proceed
          <input type="radio"  name="Approved_4"  id="Approved2_4" value="Further Info Required">
          Further Info Required
          <input type="radio"  name="Approved_4"  id="Approved3_4" value="Reject">
          Reject </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Comments</td>
      </tr>
      <tr onClick="CheckPassword(4)">
        <td colspan="4" class="normalLabel" ><textarea class="inputTextBox"  name="Comment_4" onBlur="CheckLength(this,0,255);" cols="50" rows="2"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="4" class="normalLabel" >Location of Saved Checklist</td>
      </tr>
      <tr>
        <td colspan="4" class="normal" ><input class="LinkedTextbox"  type="text" name="Checklist_4" size="98" maxlength="255" onDblClick="ClickLink(Checklist_4, LoadDoc)" value="">
              <input class="Button" type="button"  name="btnLocation_4" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_4, LoadDoc)">
              <input class="Button"  type="button" name="btnLocation6" value="Browse..." onClick="Change_Location('Checklist_4')">
              <input type="hidden" name="FileLocation_4" value="">
        </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td colspan="5" class="searchHeader"><strong>Recommendation</strong></td>
  </tr>
  <tr >
    <td class="normalLabel" width="17%">Type Of PCR:</td>
    <td class="normal" width="32%"><input type="radio" name="PCR_Type" checked value="Permanent">
      Permanent
      <input type="radio" name="PCR_Type"  value="Trial">
      Trial
      <input type="radio" name="PCR_Type"  value="Emergency">
      Emergency </td>
    <td class="normal" width="30%"><input type="checkbox"  name="Permanent_Trial" value="1">
      Make Trial Permanent? </td>
    <td class="normalLabel" width="5%">Date:</td>
    <td class="normal"><input type="text" size="12" class="inputTextBox" name="PermanentDate" id="PermanentDate" value="" onBlur="ValidateDateFields('PermanentDate')">
      <a href="#" onClick="callCalendar('anchor1','PermanentDate');" name="anchor1" id="anchor1"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
  </tr>
  <tr >
    <td class="normalLabel">&nbsp;</td>
    <td class="normalLabel">&nbsp;</td>
    <td class="normal"><input type="checkbox"  name="Extend_Changes" value="1">
      Extend permanent changes to all lines? </td>
    <td class="normalLabel">Date:</td>
    <td class="normal"><input type="text" size="12" class="inputTextBox" name="ExtendDate" id="PermanentDate" value="" onBlur="ValidateDateFields('ExtendDate')">
      <a href="#" onClick="callCalendar('anchor2','ExtendDate');" name="anchor2" id="anchor2"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
  </tr>
  <tr >
    <td class="normalLabel">Original Assessment Score:</td>
    <td class="normal">&nbsp;L</td>
    <td class="normalLabel">Assessment Level:</td>
    <td class="normal" colspan="2">&nbsp;Area/PCRT Review</td>
  </tr>
  <tr >
    <td class="normalLabel">Confirmation of Assessment Level by PCRT:</td>
    <td class="normal"><input type="radio" name="Confirmation"  value="Yes">
      Yes
      <input type="radio" name="Confirmation"  value="No">
      No </td>
    <td class="normalLabel">Revised Score:</td>
    <td class="normal" colspan="2"><select name="Revised_Score" class="inputTextBox">
      <option value="585" > VH </option>
      <option value="586" > H </option>
      <option value="587" > M </option>
      <option value="588"  selected > L </option>
    </select>
    </td>
  </tr>
  <tr >
    <td class="normalLabel">Action:</td>
    <td class="normal" colspan="4"><input type="radio" name="ActionTaken"  value="Proceed with change">
      Proceed with change
      <input type="radio" name="ActionTaken"  value="Further information required">
      Further information required
      <input type="radio" name="ActionTaken"  value="Change rejected">
      Change rejected </td>
  </tr>
  <tr >
    <td valign="top" id="cellComment" class="normalLabel">Comments:</td>
    <td class="normal" colspan="4"><textarea name="Action_Comments" class="inputTextBox" rows="3" cols="100" onBlur="CheckLength(this,0,500);"></textarea>
        <input type="hidden" name="hdnLocked" value="No" />
      <input type="hidden" name="hdnCheck" value="" />
    </td>
  </tr>
  <tr >
    <td class="normalLabel">By:</td>
    <td class="normal"><input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',3)"  value="3 months">
      3 months
      <input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',6)" value="6 months">
      6 months
      <input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',12)" value="12 months">
      12 months </td>
    <td class="normalLabel" colspan="3">Date:&nbsp;
        <input type="text" size="12" class="inputTextBox" name="ActionDate" id="PermanentDate" value="" onBlur="ValidateDateFields('ActionDate')">
      <a href="#" onClick="callCalendar('anchor3','ActionDate');" name="anchor3" id="anchor3"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
  </tr>
  <tr >
    <td class="normalLabel">AFE Number<br>
      (if applicable):</td>
    <td colspan="4"><input type="text" class="inputTextBox" name="AFE_Number" value="" size="15">
    </td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td><input class="inputTextBox" type="hidden" name="MM_update" value="true">
      <input type="hidden" name="NumReviewers" value="5">
      <input type="hidden" name="Modified_Date" value="20/10/2005">
      <input class="inputTextBox" type="hidden" name="MM_recordId" value="3674">
      <input type="hidden" name="Process_Step" value="">
      <input type="hidden" name="ProcessStep_Status" value="">
      <input type="hidden" name="Overall_Status" value="Open"></td>
  </tr>
  <tr>
    <td><span class="normal">
      <input class="Button" type="submit" name="Save" value="Save">
      <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
      <input class="Button" type="reset" name="Reset" value="Reset">
    </span></td>
  </tr>
</table>
</form>