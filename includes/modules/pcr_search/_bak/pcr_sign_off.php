<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');
?>


<script language="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";//used for the remote scripting when getting the managed by site and department

//convert the dates into a string format before saving them to the database
function ChangeDates(){  
	var x = document.frmData.NumReviewers.value;
	for(var i=0;i<x;i++){
		var sDate = "ReviewDate_" + i;
		try{
		var objDate = document.getElementById(sDate);
		objDate.value = ConvertDate(sDate, '/', 'AUS');
		}catch(e){}
	}
	var errors = PrepareDatesOut('AuthorityDate_1','AuthorityDate_2','Modified_Date','Audit_Due_Date','Due_Date');
	
	try{document.frmData.Engineer_Date.value = ConvertDate('Engineer_Date', '/', 'AUS');}catch(e){}
	try{document.frmData.Super_Date.value = ConvertDate('Super_Date', '/', 'AUS');}catch(e){}
	try{document.frmData.MeetingDate.value = ConvertDate('MeetingDate', '/', 'AUS');}catch(e){}
	try{document.frmData.signed_off_date.value = ConvertDate('signed_off_date', '/', 'AUS');}catch(e){}
}

//select the required reviewer from the drop down boxes
function SelectReviewers(){
	var x = document.frmData.NumReviewers.value;
	for(var i=0;i<x;i++){
		var sDropDown = "ReviewerId_" + i;
		var objOld = document.getElementById("OldAReviewerId_" + i);
		var iDefault = objOld.value;
		if(iDefault == ""){//if a reviewer hasn't been saved yet we use the default reviewer
			var objDefault = document.getElementById("DefaultId_" + i);
			var iDefault = objDefault.value;
		}
		SelectDropDown(sDropDown,iDefault);
	}
}

//stuff we do when the form loads
function StartUp(){
	
	ShowTeam();
	
	SelectReviewers();
	SelectDropDown('Engineer_Id','');
	SelectDropDown('Super_Id','');
	SelectDropDown('AuditBy_Id','');
	SelectAuthority();
}

function ShowApprovers(sCheck, sRow, sRequired){
	var objCheck = document.getElementById(sCheck);
	var objRow = document.getElementById(sRow);
	var objRequired = document.getElementById(sRequired);
	var Required = objRequired.value;
	
	if(objCheck.checked == false){
		switch(Required){
			case "Mandatory" :
				objCheck.checked = true;
				alert("This Reviewer is Mandatory, you cannot change this selection");
				break;
			case "Recommended" :
				alert("You can change this selection, but this Reviewer is Recommended.");
				objRow.style.display = "none";
				break;
			case "Optional" :
				objRow.style.display = "none";
				break;
		}		
	}else{
		objRow.style.display = "block";
	}
}

//enable all the form fields so we can save their values
function EnableFields(){
	var x = document.frmData.elements.length;
	for(var i=0;i<x;i++){
		if(document.frmData.elements[i].name != "BrowseDialog"){
			document.frmData.elements[i].disabled = false;
		}
	}
}

//stuff we do when we submit the form
function SubmitForm(){
	ChangeDates();
	EnableFields();
	var i=0;
	var f=document.forms[0];
	var rad=f.Sign_Off;
	//var appr=rad[0];
	var cancel=rad[1];
	//alert(cancel.checked);
	if(cancel.checked==true){
		f.ProcessStep_Status.value = "Cancelled";
		f.Overall_Status.value = "Closed";
	}
	//alert(f.ProcessStep_Status.value+"\n"+f.Overall_Status.value);
}

//show or hide the reviewers section and the details section depending on what type of review it is
function ShowTeam(){
	var x = document.frmData.NumReviewers.value;
	if(document.frmData.Review_Type[0].checked == true){
		for(var i=0;i<x;i++){
			var objRow = document.getElementById("rowReviewerType_" + i);
			objRow.style.display = "none";
		}
		for(var i=1;i<7;i++){
			var objRow = document.getElementById("rowType_" + i);
			objRow.style.display = "block";
		}	
	}else{
		for(var i=0;i<x;i++){
			var objRow = document.getElementById("rowReviewerType_" + i);
			objRow.style.display = "block";
		}
		for(var i=1;i<7;i++){
			var objRow = document.getElementById("rowType_" + i);
			objRow.style.display = "none";
		}
	}
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
</script>




<body>
<form action="index.php?m=<?php echo $m;?>&p=post&a=sign_off&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">


<b>PCR Sign Off</b>
<?php include("includes/pcr_details_general.php"); ?>
<table border="1" width="100%">
  <tr bordercolor="#FFFFFF">
    <td class="normalLabel" width="17%">Type Of Review:</td>
    <td class="normal" width="32%" align="left"><input align="left" type="radio" onClick="ShowTeam()" name="Review_Type"  value="PCRT">
      PCRT
      <input align="left" type="radio" onClick="ShowTeam()" name="Review_Type"  value="Area">
      Area </td>
    <td class="normalLabel" width="12%">&nbsp;<span id="rowType_1">Meeting Date:</span></td>
    <td class="normal">&nbsp;<span id="rowType_2">
      <input type="text" size="12" class="inputTextBox" name="MeetingDate" id="MeetingDate" value="" onBlur="ValidateDateFields('MeetingDate')">
      <a href="#" onClick="callCalendar('anchor4','MeetingDate');" name="anchor4" id="anchor4"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </span> </td>
  </tr>
  <tr id="rowType_3" bordercolor="#FFFFFF">
    <td valign="top" class="normalLabel">Comments:</td>
    <td class="normalLabel" colspan="3"><textarea name="Meeting_Comments" class="inputTextBox" rows="3" cols="100" onBlur="CheckLength(this,0,500);"></textarea>
    </td>
  </tr>
  <tr id="rowType_4" bordercolor="#FFFFFF">
    <td colspan="4"><table id="tblDocuments" width="97%" cellspacing="4">
    </table></td>
  </tr>
  <tr id="rowType_5" bordercolor="#FFFFFF">
    <td class="normalLabel" width="16%">&nbsp;Other minutes where &nbsp;PCR is referenced:</td>
    <td class="normal" colspan="3"><input class="LinkedTextbox" type="text" name="Document_1" size="90" maxlength="255" onDblClick="ClickLink(Document_1, LoadDoc)">
        <input class="Button" type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Document_1')">
        <input type="file" name="BrowseDialog2" style="display: none">
        <a style="display: none" href="#" id="LoadDoc" target="_blank">Link To File</a>
        <input type="hidden" name="NumDocuments" value="2">
    </td>
  </tr>
  <tr id="rowType_6" bordercolor="#FFFFFF">
    <td class="normalLabel" colspan="4" height="35" valign="top"><input type="button" class="Button" name="btnDocument" value="Add more minutes" onClick="addDocumentRow('tblDocuments','17%', 'Other minutes where PCR is referenced:', 'Document_', 'NumDocuments','90')">
    </td>
  </tr>
</table>
<br>
<br>
<table border="1" width="100%">
  <tr bordercolor="#FFFFFF">
    <td colspan="4" class="searchHeader">Recommendation</td>
  </tr>
  <tr bordercolor="#FFFFFF">
    <td class="normalLabel" width="17%">PCR Team Sign-off:</td>
    <td class="normal"><input type="radio" name="Sign_Off"  value="Approve">
      Approve
      <input type="radio" name="Sign_Off"  value="Cancelled">
      Cancel </td>
    <td class="normalLabel" width="17%">Signed-Off Date:</td>
    <td class="normal"><input type="text" size="12" class="inputTextBox" name="signed_off_date" id="Signed-Off Date" value="" onBlur="ValidateDateFields('signed_off_date')">
      <a href="#" onClick="callCalendar('anchor2','signed_off_date');" name="anchor2" id="anchor2"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
  </tr>
</table>
<br>
<br>
<table border="1" width="100%">
  <tr bordercolor="#FFFFFF">
    <td colspan="5" class="searchHeader">PCR Sign-off / Closure</td>
  </tr>
  <tr bordercolor="#FFFFFF">
    <td class="normalLabel">Post Audit Required to be conducted by:</td>
    <td class="normal"><select name="AuditBy_Id" class="inputTextBox">
      <option value=""></option>
      <script>
				    document.write(ReadActiveEmps());
				  </script>
    </select>
    </td>
    <td colspan="3" class="normalLabel">Due Date:&nbsp;
        <input type="text" size="12" class="inputTextBox" name="Due_Date" id="Date" value="" onBlur="ValidateDateFields('Due_Date')">
      <a href="#" onClick="callCalendar('anchor3','Due_Date');" name="anchor3" id="anchor3"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
  </tr>
</table>
<br>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr>
    <td><input class="inputTextBox" type="hidden" name="MM_update" value="true">
      <input type="hidden" name="NumReviewers" value="0">
      <input type="hidden" name="Modified_Date" value="18/10/2005">
      <input class="inputTextBox" type="hidden" name="MM_recordId" value="3671">
      <input type="hidden" name="Process_Step" value="PCR signed-off">
      <input type="hidden" name="ProcessStep_Status" value="more information required">
      <input type="hidden" name="Overall_Status" value="Open"></td>
  </tr>
  <tr>
    <td class="normal"><input type="file" name="BrowseDialog" style="display: none">
        <input class="Button" type="submit" name="Save" value="Save">
        <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
        <input class="Button" type="reset" name="Reset" value="Reset">    </td>
  </tr>
</table>
<?php include("includes/authority_to_proceed_box.php"); ?>
</form>