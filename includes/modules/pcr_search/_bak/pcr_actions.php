<script language="JavaScript">
// Begin JavaScript
//alert('2');
function MM_validateForm() 
{
	var i,p,q,test,num,min,max,errors='',args=MM_validateForm.arguments,bDelete;
	var fInsert = document.getElementById("Insert_Flag");
	ChangeDates();
	if (errors) {
		fInsert.value = 'undefined';
		alert('The following error(s) occurred:\n'+errors);
	} else {
		fInsert.value = 'true';
	}
	
	document.MM_returnValue = (errors == '');
}

function ChangeDates()
{	
	var f = document.frmData;
	var sFObj = "DaysAfter_1_";
	var sSel = "Selected_1_"
	var maxRows = parseInt(f.elements["NumDocuments_1"].value);
	var i = 0;
	for(i=1;i<maxRows;i++){
		if(f.elements[sSel+i].checked==true) f.elements[sFObj+i].value = ConvertDate(sFObj+i, '/', 'AUS');
	}
}

function addActionRow(id, iLength, iStage)
{
	if (ValidateAddRow() == true) {
		var sNumDoc = "NumDocuments_" + iStage;
		var intRowNum = document.frmData.elements[sNumDoc].value;
		if (intRowNum == "undefined") {
			intRowNum = 0;
		}	
		var iaLength = iLength.split('|');
		var td1, td2, td3, td4, td5, td6, td7;
		var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
		var row = document.createElement("TR");
		row.vAlign = "top";
		td1 = document.createElement("TD");
		td2 = document.createElement("TD");
		td3 = document.createElement("TD");
		td4 = document.createElement("TD");
		td5 = document.createElement("TD");
		td6 = document.createElement("TD");
		td7 = document.createElement("TD");
		
		var s_append_id = ""+iStage + "_" + intRowNum ;
		var e_managed_by_name="Managed_By_" + iStage + "_" + intRowNum;
		var e_allocated_to_name="Allocated_" + iStage + "_" + intRowNum;
		var e_options = ReadActiveEmps().replace("selected>",">");
		
		td1.innerHTML = "<input name='Selected_" + s_append_id + "' type='checkbox' class='normal' value='" + intRowNum + "'>";
		td2.innerHTML = "<input name='Action_" + s_append_id + "' type='text' class='normal' value='' size='50'>";
		td3.innerHTML = "<select name='"+ e_managed_by_name +"' id='"+e_managed_by_name.replace(/_/gi," ")+"' class='inputTextBox'><option></option>"+e_options+"</select>";
		td4.innerHTML = "<select name='"+ e_allocated_to_name +"' id='"+e_allocated_to_name.replace(/_/gi," ")+"' class='inputTextBox'><option></option>"+e_options+"</select>";
		td5.innerHTML = "<input name='Status_" + s_append_id + "' type='text' disabled=true class='normal' value='' size='5'>";
		td6.innerHTML = "<input name='DaysAfter_" + s_append_id + "' type='text' class='normal' value='' size='5' onBlur='" + ((iStage==1)?"ValidateDateFields(\"DaysAfter_" + s_append_id + "\");'":"ValidateNumber(\"DaysAfter_" + s_append_id + "\");'") + ">";
		td7.innerHTML = "<input name='Confirm_" + s_append_id + "' type='checkbox' value='N' onClick='SetCheckBoxValue(\"Confirm_" + s_append_id + "\");'>" +
								"<input name='ActionId_" + s_append_id + "' type='hidden' class='normal' value='0'>"+
								"<input name='DetailId_" + s_append_id + "' type='hidden' class='normal' value='0'>";
	
		row.appendChild(td1);
		row.appendChild(td2);
		row.appendChild(td3);
		row.appendChild(td4);
		row.appendChild(td5);
		row.appendChild(td6);
		row.appendChild(td7);
		
		intRowNum++;
		
		row.id = "ActionRow_"+s_append_id;
		//alert(row.outerHTML);
		tbody.appendChild(row);//add the row to the table
		document.frmData.elements[sNumDoc].value = intRowNum;//update the the hidden field that stores the number of documents entered
	} else {
		alert("New search has not been performed.\nPlease re-select Site before adding new line");
		return;
	}
	
}

function get_site_employees(selected_id)
{
	var a=[['dd',2],[
"10941", "Albert, Lee","10597", "AlbertX, Lee","1212142", "Albrey, Graham","998961", "Allan, Tyronne","10429", "Allen, Tony","97053", "Allsop, Andrea","11016", "Anderson, David","97073", "AndersonX, David","10556", "Andrews, Maria","99277", "Baker, Glenn","998984", "Bazukiewicz, P","100062", "Beard, Gary","10058", "Benson, Steve","10835", "Bezanson, Kristy","11037", "Birtles, Julia","98153", "Birtles, Julia","10885", "Boston, Claire","100096", "Bowden, Paul","1212153", "Bowman, B","97055", "Bray, Simon","10974", "Brown, Rochelle","97049", "Brown, Rochelle 2","100093", "Brown, Darren","10539", "Burns, David","97044", "Caceres-Valverde, Marcela","10921", "Caceres-Valverde, Marcela","10279", "Charles, David","97063", "Clark, Adam","100106", "Clark, Derrick","10111", "Collette, Stephen","1212160", "Collins, Alan","998983", "Cosh, Murray","97074", "Coster, Nicole","10914", "Cowley, Lisa","97057", "Cronin, Damian","10581", "Dardengo, Eugene","10395", "Davies, Scott","97033", "Davis, James","10867", "Davis, James","67043", "De Jong, Rick","10761", "Dorrington, Graeme","10011", "Dunmall, Diane","10717", "Edmonds, Jamie","1212144", "Edwards, Tony","998982", "Enyon, Richard","11009", "Ferreira, Isabel","97041", "Gannaway, Mary","11015", "Garwood, Louise","97071", "Gasper, Lill","10856", "Gatland, Marc","97075", "Geluk, Elizabeth","10799", "Gherbaz, Eldo","10737", "Golos, David","10150", "Grgurich, Davorin","10530", "Haggarty, Jenny","10735", "Hansen-Knarhoi, Sally","10484", "Hill, Sue","99311", "Huang, Jabez","90767", "Hug, David","99370", "Hurst, Courtney","1212147", "Jakovich, Garry","998966", "Jarvis, John","97054", "Johannes, David","1212132", "Johns, Todd","10252", "Jones, Greg","10278", "Jones, Alun","97035", "Kelly, Julie","10828", "Kennedy, Sam","998990", "Kuther, Richard","90999", "Laskey, Vernon","10071", "Lawson, George","45037", "Lee, Graeme","97069", "Leo, Morgan","11012", "Leo, Morgan","10101", "Liddelow, Neil","10562", "Lilje, Kevin","1212123", "Liljez, Kevin","998962", "Lillee, Doug","97070", "Lim, Pan","10968", "Longman, Cindy","100108", "Luke, Colin","1212143", "MacDonald, Stephen","998958", "Maher, Leslie","97067", "Malkoc, Sadina","97078", "Martin, Vicki","97056", "Maurice, Katrina","97062", "Mayo, Renae","1212145", "McBain, Damien","1212146", "McGavin, Scott","10625", "Mccollum, David","10652", "Mcdonald, Allan","99322", "Mcgough, Ed","10457", "Mckenna, Richard","10872", "Mckenzie, Beth","10080", "Mcmullan, Richard","10666", "Melvill, Johann","97052", "Mignanelli, Tony","10246", "Millington, Stuart","10759", "Moore, Jodie","1212149", "Morris, Dennis","95113", "Morrissey, Jacinta","10718", "Muir, Lesa","97061", "Mulligan, Kaye","998964", "Murphy, Jack","100091", "Nepia, Morgan","97016", "Nowland, Nicole","998959", "O'Dywer, Bruce","10878", "O'Farrell, Karen","97066", "O'Halloran, Roslyn","10883", "Orrell, Tony","10775", "Orsmond, Glenn","10568", "Overend, Fetu","30394", "Padfield, Adrian","99362", "Peterson, Alan","10760", "Phillips, Trish","10596", "Pieremont, Ken","998960", "Pike, Aaron","10502", "Plester, Dennis","998963", "Pryer, Russell","100098", "Rameka, Hord","2", "Reserved For Pdit Interface, None","97068", "Rhone, Elaine","10908", "Rickaby, Tania","99999", "Ridley, Todd","99359", "Ridley, Todd","1212136", "Ridolfo, Tony","99476", "Roberts, Steve","100102", "Ross, Barry","100109", "Rylander, Tony","998967", "Salt, Quentin","998981", "Salt-, Quentin","97072", "Salundi, Jenny","10762", "Schoeman, Charmaine","10618", "Shirley, Arthur","11014", "Sjoland, Phillipe","1", "Slawinski, Brad","998956", "Smith, Bob","10777", "Smith, Lisa","10807", "Smith, Murray","10566", "Soanes, Dan","10776", "Sofoulis, Amanda","100078", "Sommerville, Daryl","10909", "Sommerville, Daryl","10846", "Stanford, Toni","10469", "Stevenson, Julie","998957", "Stonehouse, John","97060", "Subramaniam, Tanya","10845", "Tana, Lisa","1212148", "Taylor, Russell","10186", "Thornett, John","100092", "Tolley, Daniel","1212131", "Toms, Shane","97036", "Travis, Dennis","97065", "Trent, Emma","100086", "Turner, Will","10614", "Twynholm-Mason, Keith","10934", "Valas, Joe","97050", "Valas, Joe","97059", "Varley, Sue","998899", "Warner, Geoff","100095", "Whisson, Michael","998991", "Williams, Mick","100107", "Williams, Mort","100097", "Williamson, Evan","100094", "Willsher, Tim","1212133", "Wilson, Andrew","1212135", "Zaurs, Jason","100101", "dummy, dummy","1212125", "liljea, kk","1212126", "liljeb, kk","1212124", "liljey, kk","10646", "~Inactive Milby, Lynette"
	]];
	return CATS_getSelect(a,selected_id);
}

function ReadCheckboxDesc()
{
	var sTemp = ReturnDropFile('catsCheckboxDesc');
	return sTemp;
}

function SetCheckBoxValue(sCheckBoxName) 
{
	var objCheckBox = document.getElementById(sCheckBoxName);
	
	if (objCheckBox.checked == true) {
		objCheckBox.value = 'Y';
	} else {
		objCheckBox.value = 'N';
	}
}

function ValidateAddRow() 
{
	var sTServerURL = "../RemoteScripts/GetValues.asp";
	var iSite = document.getElementById("Site_Id").value;
	var objAction = document.getElementById("ActionId_0");//{value:1}; //
	var bReturn = true;
	var iAction, sWhere, sColumn1, sDataStr;
	return true;
	if (objAction == null ) {
		iAction = 0;
	} else {
		iAction = objAction.value;
	}
	
	sWhere = "Select count(*) " +
			 "From   tblPCR_Action " +
			 "where  Site_Id = " + iSite;
	sColumn1 = "count(*)";
	sDataStr = RSExecute(sTServerURL,"GetSingleValue", sWhere, sColumn1).return_value;
	alert(sDataStr);
	if (sDataStr > 0 && (iAction == '' || iAction == 0)) {
		bReturn = false;
	}
	
	if (iAction > 0 && bReturn) {
		sWhere = "Select site_id " +
				 "From   tblPCR_Action " +
				 "where  action_Id = " + iAction;
		sColumn1 = "Site_Id";
		
		sDataStr = RSExecute(sTServerURL,"GetSingleValue", sWhere, sColumn1).return_value;
		
		if (iSite != sDataStr) {
			bReturn = false;
		}
	}
	
	return bReturn;
}

function ValidateNumber(sName) 
{
	var sDays = document.getElementById(sName);

	if (sDays.value >= 0 && sDays.value <= 99999) {
	} else {
		alert(sDays.value + " is invalid number of days (0 - 99999)\nPlease re-enter days.");
		sDays.value = 0;
	}
}

function ShowActions(item)
{
	var f = item.form;
	var aItem = item.name.split("_");
	var iStage = parseInt(aItem[1]);
	var iRow = parseInt(aItem[2]);
	var sName = aItem[0];
	var sRow = "ActionRow_";
	var sSel = "Selected_"
	var maxRows = parseInt(f.elements["NumDocuments_"+iStage].value);
	var i = 0;
	for(i=1;i<maxRows;i++){
		if(item.value=="All"){	
			sDisplay = "block";
		}else if(f.elements[sSel+iStage+"_"+i].checked==true){
			sDisplay = "block";
		}else{
			sDisplay = "none";
		}
		document.getElementById(sRow+iStage+"_"+i).style.display=sDisplay;
	}
}

function StartUp()
{
	var f=document.forms[0];
	var i=j=0;
	var o;
	for(i=1;i<4;i++){
		o = f.elements["Show_"+i];
		for(j=0;j<o.length;j++){
			if(o[j].checked==true) ShowActions(o[j]);
		}
	}
	SelectAuthority();
}
// End JavaScript
</script>




<body>

<form action="index.php?m=<?php echo $m;?>&p=post&a=actions&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">



<b>PCR Actions</b>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr>
    <td width="199" valign="top" class="normalLabel"><strong>PCR No.</strong></td>
    <td width="691" class="normal">PI3801</td>
  </tr>
  <tr>
    <td valign="top" class="normalLabel"><strong>Type of PCR:</strong></td>
    <td class="normal">Trial</td>
  </tr>
  <tr>
    <td valign="top" class="normalLabel"><strong>Criticality:</strong></td>
    <td class="normal">NC</td>
  </tr>
  <tr>
    <td valign="top" class="normalLabel"><strong>Status:</strong></td>
    <td class="normal">Open-Area review-performed</td>
  </tr>
  <tr>
    <td valign="top" class="normalLabel"><strong>Title:</strong></td>
    <td class="normal">SR transfer Fault Finding Alarms</td>
  </tr>
</table>
<br>
<br>
<table width="800" border="1" cellpadding="5" cellspacing="0">
  <tr valign="top">
    <td class="normalLabel" width="60%" height="23"><h4>Stage 1 - Undertake Work </h4></td>
    <td class="normalLabel"><input type="radio" name="Show_1" value="All" onClick="ShowActions(this);"  checked=true  />
      Show All</td>
    <td class="normalLabel"><input type="radio" name="Show_1" value="Selected" onClick="ShowActions(this);"  />
      Show Selected</td>
  </tr>
</table>
<table width="800" border="1" cellpadding="5" cellspacing="0" id="tblAction_1">
  <tr>
    <th width="2%" class="normalLabel">Select</th>
    <th width="38%" class="normalLabel">Action Title</th>
    <th width="15%" class="normalLabel">Managed By</th>
    <th width="15%" class="normalLabel">Allocated To</th>
    <th width="10%" class="normalLabel">Status</th>
    <th width="10%" class="normalLabel"> Due Date </th>
    <th width="10%" class="normalLabel">Action Confirmed</th>
  </tr>
  <tr id="ActionRow_1_1">
    <td><input type="checkbox" name="Selected_1_1" value="1"  /></td>
    <td valign="top"><input name="Action_1_1" type="text" class="normal" value="Implement Change" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_1_1"			id="Managed_By_1_1"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_1_1|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_1_1"			id="Allocated_1_1"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_1_1" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_1_1" type="text" class="normal" value="" size="5" onBlur="ValidateDateFields('DaysAfter_1_1');"></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_1_1" type="hidden" value="3">
        <input name="DetailId_1_1" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_1_2">
    <td><input type="checkbox" name="Selected_1_2" value="2"  /></td>
    <td valign="top"><input name="Action_1_2" type="text" class="normal" value="Implement Trial" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_1_2"			id="Managed_By_1_2"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_1_2|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_1_2"			id="Allocated_1_2"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_1_2" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_1_2" type="text" class="normal" value="" size="5" onBlur="ValidateDateFields('DaysAfter_1_2');"></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_1_2" type="hidden" value="5">
        <input name="DetailId_1_2" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_1_3">
    <td><input type="checkbox" name="Selected_1_3" value="3"  /></td>
    <td valign="top"><input name="Action_1_3" type="text" class="normal" value="TP-ENG-001 Piping, Instrument Drawings" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_1_3"			id="Managed_By_1_3"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_1_3|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_1_3"			id="Allocated_1_3"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_1_3" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_1_3" type="text" class="normal" value="" size="5" onBlur="ValidateDateFields('DaysAfter_1_3');"></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_1_3" type="hidden" value="7">
        <input name="DetailId_1_3" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_1_4">
    <td><input type="checkbox" name="Selected_1_4" value="4"  /></td>
    <td valign="top"><input name="Action_1_4" type="text" class="normal" value="SOP - Area 1" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_1_4"			id="Managed_By_1_4"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_1_4|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_1_4"			id="Allocated_1_4"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_1_4" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_1_4" type="text" class="normal" value="" size="5" onBlur="ValidateDateFields('DaysAfter_1_4');"></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_1_4" type="hidden" value="33">
        <input name="DetailId_1_4" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_1_5">
    <td><input type="checkbox" name="Selected_1_5" value="5"  /></td>
    <td valign="top"><input name="Action_1_5" type="text" class="normal" value="SOP - Area 2" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_1_5"			id="Managed_By_1_5"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_1_5|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_1_5"			id="Allocated_1_5"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_1_5" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_1_5" type="text" class="normal" value="" size="5" onBlur="ValidateDateFields('DaysAfter_1_5');"></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_1_5" type="hidden" value="34">
        <input name="DetailId_1_5" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_1_6">
    <td><input type="checkbox" name="Selected_1_6" value="6"  /></td>
    <td valign="top"><input name="Action_1_6" type="text" class="normal" value="Write Report" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_1_6"			id="Managed_By_1_6"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_1_6|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_1_6"			id="Allocated_1_6"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_1_6" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_1_6" type="text" class="normal" value="" size="5" onBlur="ValidateDateFields('DaysAfter_1_6');"></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_1_6" type="hidden" value="53">
        <input name="DetailId_1_6" type="hidden" value=""></td>
  </tr>
</table>
<input type="button" class="Button" name="btnDocument" value="Add another action" onClick="addActionRow('tblAction_1','327|152|80|43', '1');">
<input type="hidden" name="NumDocuments_1" value="7">
<br>
<br>
<br>
<br>
<table width="800" border="1" cellpadding="5" cellspacing="0">
  <tr valign="top">
    <td class="normalLabel" width="60%" height="23"><h4>Stage 2 - Update Records </h4></td>
    <td class="normalLabel"><input type="radio" name="Show_2" value="All" onClick="ShowActions(this);"  checked=true  />
      Show All</td>
    <td class="normalLabel"><input type="radio" name="Show_2" value="Selected" onClick="ShowActions(this);"  />
      Show Selected</td>
  </tr>
</table>
<table width="800" border="1" cellpadding="5" cellspacing="0" id="tblAction_2">
  <tr>
    <th width="2%" class="normalLabel">Select</th>
    <th width="38%" class="normalLabel">Action Title</th>
    <th width="15%" class="normalLabel">Managed By</th>
    <th width="15%" class="normalLabel">Allocated To</th>
    <th width="10%" class="normalLabel">Status</th>
    <th width="10%" class="normalLabel"> How many days after Stage 1 completed </th>
    <th width="10%" class="normalLabel"> Raise Now </th>
    <th width="10%" class="normalLabel">Action Confirmed</th>
  </tr>
  <tr id="ActionRow_2_1">
    <td><input type="checkbox" name="Selected_2_1" value="1"  /></td>
    <td valign="top"><input name="Action_2_1" type="text" class="normal" value="Implement Change" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_2_1"			id="Managed_By_2_1"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_2_1|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_2_1"			id="Allocated_2_1"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_2_1" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_2_1" type="text" class="normal" value="30" size="5" onBlur="ValidateNumber('DaysAfter_2_1');"></td>
    <td><input name="Raise_Now_2_1" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_2_1');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_2_1" type="hidden" value="3">
        <input name="DetailId_2_1" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_2_2">
    <td><input type="checkbox" name="Selected_2_2" value="2"  /></td>
    <td valign="top"><input name="Action_2_2" type="text" class="normal" value="Implement Trial" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_2_2"			id="Managed_By_2_2"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_2_2|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_2_2"			id="Allocated_2_2"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_2_2" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_2_2" type="text" class="normal" value="90" size="5" onBlur="ValidateNumber('DaysAfter_2_2');"></td>
    <td><input name="Raise_Now_2_2" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_2_2');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_2_2" type="hidden" value="5">
        <input name="DetailId_2_2" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_2_3">
    <td><input type="checkbox" name="Selected_2_3" value="3"  /></td>
    <td valign="top"><input name="Action_2_3" type="text" class="normal" value="TP-ENG-001 Piping, Instrument Drawings" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_2_3"			id="Managed_By_2_3"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_2_3|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_2_3"			id="Allocated_2_3"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_2_3" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_2_3" type="text" class="normal" value="60" size="5" onBlur="ValidateNumber('DaysAfter_2_3');"></td>
    <td><input name="Raise_Now_2_3" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_2_3');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_2_3" type="hidden" value="7">
        <input name="DetailId_2_3" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_2_4">
    <td><input type="checkbox" name="Selected_2_4" value="4"  /></td>
    <td valign="top"><input name="Action_2_4" type="text" class="normal" value="SOP - Area 1" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_2_4"			id="Managed_By_2_4"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_2_4|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_2_4"			id="Allocated_2_4"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_2_4" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_2_4" type="text" class="normal" value="30" size="5" onBlur="ValidateNumber('DaysAfter_2_4');"></td>
    <td><input name="Raise_Now_2_4" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_2_4');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_2_4" type="hidden" value="33">
        <input name="DetailId_2_4" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_2_5">
    <td><input type="checkbox" name="Selected_2_5" value="5"  /></td>
    <td valign="top"><input name="Action_2_5" type="text" class="normal" value="SOP - Area 2" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_2_5"			id="Managed_By_2_5"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_2_5|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_2_5"			id="Allocated_2_5"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_2_5" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_2_5" type="text" class="normal" value="30" size="5" onBlur="ValidateNumber('DaysAfter_2_5');"></td>
    <td><input name="Raise_Now_2_5" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_2_5');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_2_5" type="hidden" value="34">
        <input name="DetailId_2_5" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_2_6">
    <td><input type="checkbox" name="Selected_2_6" value="6"  /></td>
    <td valign="top"><input name="Action_2_6" type="text" class="normal" value="Write Report" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_2_6"			id="Managed_By_2_6"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_2_6|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_2_6"			id="Allocated_2_6"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_2_6" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_2_6" type="text" class="normal" value="90" size="5" onBlur="ValidateNumber('DaysAfter_2_6');"></td>
    <td><input name="Raise_Now_2_6" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_2_6');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_2_6" type="hidden" value="53">
        <input name="DetailId_2_6" type="hidden" value=""></td>
  </tr>
</table>
<input type="button" class="Button" name="btnDocument2" value="Add another action" onClick="addActionRow('tblAction_2','327|152|80|43', '2');">
<input type="hidden" name="NumDocuments_2" value="7">
<br>
<br>
<br>
<br>
<table width="800" border="1" cellpadding="5" cellspacing="0">
  <tr valign="top">
    <td class="normalLabel" width="60%" height="23"><h4>Stage 3 - </h4></td>
    <td class="normalLabel"><input type="radio" name="Show_3" value="All" onClick="ShowActions(this);"  checked=true  />
      Show All</td>
    <td class="normalLabel"><input type="radio" name="Show_3" value="Selected" onClick="ShowActions(this);"  />
      Show Selected</td>
  </tr>
</table>
<table width="800" border="1" cellpadding="5" cellspacing="0" id="tblAction_3">
  <tr>
    <th width="2%" class="normalLabel">Select</th>
    <th width="38%" class="normalLabel">Action Title</th>
    <th width="15%" class="normalLabel">Managed By</th>
    <th width="15%" class="normalLabel">Allocated To</th>
    <th width="10%" class="normalLabel">Status</th>
    <th width="10%" class="normalLabel"> How many days after Stage 2 completed </th>
    <th width="10%" class="normalLabel"> Raise Now </th>
    <th width="10%" class="normalLabel">Action Confirmed</th>
  </tr>
  <tr id="ActionRow_3_1">
    <td><input type="checkbox" name="Selected_3_1" value="1"  /></td>
    <td valign="top"><input name="Action_3_1" type="text" class="normal" value="Implement Change" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_3_1"			id="Managed_By_3_1"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_3_1|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_3_1"			id="Allocated_3_1"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_3_1" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_3_1" type="text" class="normal" value="30" size="5" onBlur="ValidateNumber('DaysAfter_3_1');"></td>
    <td><input name="Raise_Now_3_1" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_3_1');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_3_1" type="hidden" value="3">
        <input name="DetailId_3_1" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_3_2">
    <td><input type="checkbox" name="Selected_3_2" value="2"  /></td>
    <td valign="top"><input name="Action_3_2" type="text" class="normal" value="Implement Trial" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_3_2"			id="Managed_By_3_2"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_3_2|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_3_2"			id="Allocated_3_2"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_3_2" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_3_2" type="text" class="normal" value="90" size="5" onBlur="ValidateNumber('DaysAfter_3_2');"></td>
    <td><input name="Raise_Now_3_2" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_3_2');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_3_2" type="hidden" value="5">
        <input name="DetailId_3_2" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_3_3">
    <td><input type="checkbox" name="Selected_3_3" value="3"  /></td>
    <td valign="top"><input name="Action_3_3" type="text" class="normal" value="TP-ENG-001 Piping, Instrument Drawings" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_3_3"			id="Managed_By_3_3"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_3_3|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_3_3"			id="Allocated_3_3"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_3_3" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_3_3" type="text" class="normal" value="60" size="5" onBlur="ValidateNumber('DaysAfter_3_3');"></td>
    <td><input name="Raise_Now_3_3" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_3_3');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_3_3" type="hidden" value="7">
        <input name="DetailId_3_3" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_3_4">
    <td><input type="checkbox" name="Selected_3_4" value="4"  /></td>
    <td valign="top"><input name="Action_3_4" type="text" class="normal" value="SOP - Area 1" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_3_4"			id="Managed_By_3_4"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_3_4|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_3_4"			id="Allocated_3_4"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_3_4" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_3_4" type="text" class="normal" value="30" size="5" onBlur="ValidateNumber('DaysAfter_3_4');"></td>
    <td><input name="Raise_Now_3_4" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_3_4');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_3_4" type="hidden" value="33">
        <input name="DetailId_3_4" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_3_5">
    <td><input type="checkbox" name="Selected_3_5" value="5"  /></td>
    <td valign="top"><input name="Action_3_5" type="text" class="normal" value="SOP - Area 2" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_3_5"			id="Managed_By_3_5"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_3_5|10748";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_3_5"			id="Allocated_3_5"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_3_5" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_3_5" type="text" class="normal" value="30" size="5" onBlur="ValidateNumber('DaysAfter_3_5');"></td>
    <td><input name="Raise_Now_3_5" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_3_5');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_3_5" type="hidden" value="34">
        <input name="DetailId_3_5" type="hidden" value=""></td>
  </tr>
  <tr id="ActionRow_3_6">
    <td><input type="checkbox" name="Selected_3_6" value="6"  /></td>
    <td valign="top"><input name="Action_3_6" type="text" class="normal" value="Write Report" size="50"></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Managed_By_3_6"			id="Managed_By_3_6"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());ONLOAD_SELECT_DEFAULTS +="|Managed_By_3_6|10410";</script>
    </select></td>
    <td class="normal" valign="top"><select class="inputTextBox"			name="Allocated_3_6"			id="Allocated_3_6"			style="width:auto">
      <option></option>
      <script>document.write(ReadActiveEmps());</script>
    </select></td>
    <td><input name="Status_3_6" type="text" class="normal" value="" size="5" disabled=true></td>
    <td><input name="DaysAfter_3_6" type="text" class="normal" value="90" size="5" onBlur="ValidateNumber('DaysAfter_3_6');"></td>
    <td><input name="Raise_Now_3_6" type="checkbox" value="Y" onClick="SetCheckBoxValue('Raise_Now_3_6');" ></td>
    <td class="normal" valign="top">N/A
      <input name="ActionId_3_6" type="hidden" value="53">
        <input name="DetailId_3_6" type="hidden" value=""></td>
  </tr>
</table>
<input type="button" class="Button" name="btnDocument3" value="Add another action" onClick="addActionRow('tblAction_3','327|152|80|43', '3');">
<input type="hidden" name="NumDocuments_3" value="7">
<br>
<br>
<br>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td><input type="hidden" name="MM_Search" value="false">
      <input type="hidden" name="RetainSearch" value="No">
      <input type="hidden" name="Site_Id" value="3">
      <input type="hidden" name="Process_Step" value="Actions">
      <input type="hidden" name="ProcessStep_Status" value="selected">
      <input type="hidden" name="Overall_Status" value="Open">
      <input name="Insert_Flag" id="Insert_Flag" type="text" class="normal" value="true" size="1" style="display:none"></td>
  </tr>
  <tr>
    <td><input class="Button" type="submit" name="Save" value="Save">
      <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
      <input class="Button" type="reset" name="Reset" value="Reset"></td>
  </tr>
</table>
</form>