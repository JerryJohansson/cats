<?PHP

     $sql_AreaReview = "SELECT * FROM View_PCR_AreaReview ". 
	                   "WHERE Review_Type = 'Area' ".
			           "AND Site_Id = ".iSiteId." ".
			           "AND PCR_ID = ".MM_PCRId." ".
			           "ORDER BY Reviewer_Type ASC";


?>

<body>
<form action="index.php?m=<?php echo $m;?>&p=post&a=area_review&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">

<b>PCR Area Review</b>
<?php include("includes/pcr_details_general.php"); ?>






<?PHP

    for ($i = 1; $i <= 5; $i++)
    {

?>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td class="normalLabel" width="15%">Reviewer Type:</td>
    <td class="searchHeader" width="20%"> Area Manager
      <input type="hidden" name="ReviewerTypeString_0" value="Area Manager">
      <input type="hidden" name="DetailId_0" value="21">
    </td>
    <td class="normalLabel" width="8%">Required:</td>
    <td class="normal" width="10%"><input type="checkbox" name="Required_0"  onClick="ShowApprovers('Required_0','rowReviewer_0', 'hdnRequired_0')" value="1">
      Yes
      <input type="hidden" name="hdnRequired_0" value="Optional">
    </td>
    <td class="normalLabel" width="13%">Reviewer Name:</td>
    <td class="normal"><input type="hidden" name="OldAReviewerId_0" value="10876">
      <input type="hidden" name="DefaultId_0" value="10876">
      <select name="ReviewerId_0" class="inputTextBox">
        <option value=""></option>
        <script>
		    document.write(ReadActiveEmps());
		</script>
      </select>
    </td>
  </tr>
  <tr id="rowReviewer_0" >
    <td colspan="6">
	
	
	
	<table width="100%" border="1" cellspacing="0" cellpadding="5">
        <tr>
          <td width="15%" class="normalLabel">Date Reviewed:</td>
          <td class="normal" width="20%"><input type="text" size="12"  class="inputTextBox" name="ReviewDate_0" onBlur="ValidateDateFields('ReviewDate_0')" value="">
            <a href="#" onClick="callCalendar('anchor_0','ReviewDate_0');" name="anchor_0" id="anchor_0"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> </td>
          <td class="normalLabel" width="8%">Status:</td>
          <td class="normal"><input type="radio"  name="Approved_0"  id="Approved1_0" value="Proceed">
            Proceed
            <input type="radio"  name="Approved_0"  id="Approved2_0" value="Further Info Required">
            Further Info Required
            <input type="radio"  name="Approved_0"  id="Approved3_0" value="Reject">
            Reject </td>
        </tr>
        <tr>
          <td class="normalLabel" colspan="4">Comments</td>
        </tr>
        <tr onClick="CheckPassword(0)">
          <td class="normalLabel" colspan="4"><textarea class="inputTextBox"   name="Comment_0" onBlur="CheckLength(this,0,255);" cols="50" rows="2"></textarea>
          </td>
        </tr>
        <tr>
          <td class="normalLabel" colspan="4">Location of Saved Checklist</td>
        </tr>
        <tr>
          <td class="normal" colspan="4"><input class="LinkedTextbox"  type="text" name="Checklist_0" size="98" maxlength="255" onDblClick="ClickLink(Checklist_0, LoadDoc)" value="">
            <input class="Button" type="button"  name="btnLocation_0" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_0, LoadDoc)">
            <input class="Button"  type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Checklist_0')">
            <input type="hidden" name="FileLocation_0" value="">
          </td>
        </tr>
      </table>
	  
	  
	  </td>
  </tr>
</table>
<?PHP
    }
?>
<br>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td colspan="5" class="searchHeader"><strong>Recommendation</strong></td>
  </tr>
  <tr >
    <td width="22%" valign="top" class="normalLabel" id="cellComment">Type Of PCR:</td>
    <td width="78%" colspan="4" class="normal"><input type="radio" name="PCR_Type"  value="Permanent">
Permanent
  <input type="radio" name="PCR_Type" checked value="Trial">
Trial
<input type="radio" name="PCR_Type"  value="Emergency">
Emergency</td>
  </tr>
  <tr >
    <td class="normalLabel">&nbsp;</td>
    <td colspan="4" class="normal"><input type="checkbox"  name="Permanent_Trial" value="1">
Make Trial Permanent? </td>
  </tr>
  <tr >
    <td class="normalLabel">Date:</td>
    <td colspan="4"><span class="normal">
      <input type="text" size="12" class="inputTextBox" name="PermanentDate" id="Date" value="" onBlur="ValidateDateFields('PermanentDate')">
      <a href="#" onClick="callCalendar('anchor1','PermanentDate');" name="anchor1" id="anchor1"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a></span></td>
  </tr>
  <tr >
    <td class="normalLabel">&nbsp;</td>
    <td colspan="4"><span class="normal">
      <input type="checkbox"  name="Extend_Changes" value="1">
Extend permanent changes to all lines? </span></td>
  </tr>
  <tr >
    <td class="normalLabel">Date:</td>
    <td colspan="4"><span class="normal">
      <input type="text" size="12" class="inputTextBox" name="ExtendDate" id="ExtendDate" value="" onBlur="ValidateDateFields('ExtendDate')">
      <a href="#" onClick="callCalendar('anchor2','ExtendDate');" name="anchor2" id="anchor2"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a></span></td>
  </tr>
  <tr >
    <td class="normalLabel">Original Assessment Score:</td>
    <td colspan="4"><span class="normal">&nbsp;L</span></td>
  </tr>
  <tr >
    <td class="normalLabel">Assessment Level:</td>
    <td colspan="4"><span class="normal">&nbsp;Area/PCRT Review</span></td>
  </tr>
  <tr >
    <td class="normalLabel">Confirmation of Assessment Level by PCRT:</td>
    <td colspan="4"><span class="normal">
      <input type="radio" name="Confirmation"  value="Yes">
Yes
<input type="radio" name="Confirmation"  value="No">
No </span></td>
  </tr>
  <tr >
    <td class="normalLabel">Revised Score:</td>
    <td colspan="4"><span class="normal">
      <select name="Revised_Score" class="inputTextBox">
        <option value="585" > VH </option>
        <option value="586" > H </option>
        <option value="587" > M </option>
        <option value="588"  selected > L </option>
      </select>
    </span></td>
  </tr>
  <tr >
    <td class="normalLabel">Action:</td>
    <td colspan="4"><span class="normal">
      <input type="radio" name="ActionTaken"  value="Proceed with change">
Proceed with change
<input type="radio" name="ActionTaken"  value="Further information required">
Further information required
<input type="radio" name="ActionTaken"  value="Change rejected">
Change rejected </span></td>
  </tr>
  <tr >
    <td class="normalLabel">Comments:</td>
    <td colspan="4"><span class="normal">
      <textarea name="Action_Comments" class="inputTextBox" rows="3" cols="50" onBlur="CheckLength(this,0,255);"></textarea>
    </span></td>
  </tr>
  <tr >
    <td class="normalLabel">By:</td>
    <td colspan="4"><span class="normal">
      <input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',3);"  value="3 months">
3 months
<input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',6);"  value="6 months">
6 months
<input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',12);"  value="12 months">
12 months </span></td>
  </tr>
  <tr >
    <td class="normalLabel">Date:&nbsp;</td>
    <td colspan="4"><span class="normalLabel">
      <input type="text" size="12" class="inputTextBox" name="ActionDate" id="ActionDate" value="" onBlur="ValidateDateFields('ActionDate')">
      <a href="#" onClick="callCalendar('anchor3','ActionDate');" name="anchor3" id="anchor3"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a></span></td>
  </tr>
  <tr >
    <td class="normalLabel">AFE Number 
(if applicable):</td>
    <td colspan="4"><input type="text" class="inputTextBox" name="AFE_Number" value="" size="15"></td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td><input class="inputTextBox" type="hidden" name="MM_update" value="true">
      <input type="hidden" name="NumReviewers" value="4">
      <input type="hidden" name="Modified_Date" value="18/10/2005">
      <input class="inputTextBox" type="hidden" name="MM_recordId" value="3671">
      <input type="hidden" name="Process_Step" value="Area review">
      <input type="hidden" name="ProcessStep_Status" value="performed">
      <input type="hidden" name="Overall_Status" value="Open"></td>
  </tr>
  <tr>
    <td><span class="normal">
      <input class="Button" type="submit" name="Save" value="Save">
      <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
      <input class="Button" type="reset" name="Reset" value="Reset">
    </span></td>
  </tr>
</table>

<?php include("includes/authority_to_proceed_box.php"); ?>

</form>