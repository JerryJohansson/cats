<?php


/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search')
{
	$ret = array();
	$name = ($_SESSION['user_details']['site_code']=='PI')?"PCR":"CMR";
	$title = ($_SESSION['user_details']['site_code']=='PI')?"Plant Change Request":"Change Management Request";
	switch($action){
		case 'search':
		case 'results':		
			$ret = array_merge($ret, 
				array(
					'name'=>"Search $name", // Short title
					'title'=>"Search $title", // Full title description
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>"New $name",
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>"Edit $name",
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}




/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/


function getPropertiesTables($page = 'search')
{
	switch($page){
		case 'pcr_initial_review':
			$ret = array(
				'table'=>'TBLPCR_INITIALREVIEW', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_INITIALREVIEW'
			);
			break;
		case 'pcr_evaluation':
			$ret = array(
				'table'=>'TBLPCR_EVALUATION', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_EVALUATION'
			);
			break;
		case 'pcr_area_review':
			$ret = array(
				'table'=>'TBLPCR_AREAREVIEW', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_AREAREVIEW'
			);
			break;
		case 'pcr_pcrt_review':
			$ret = array(
				'table'=>'TBLPCR_PCRTREVIEW', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_PCRTREVIEW'
			);
			break;
		case 'pcr_extension':
			$ret = array(
				'table'=>'TBLPCR_EXTENSION', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_EXTENSION'
			);
			break;
		case 'pcr_actions':
			$ret = array(
				'table'=>'TBLPCR_ACTIONS', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_ACTIONS'
			);
			break;
		case 'pcr_sign_off':
			$ret = array(
				'table'=>'TBLPCR_SIGNOFF', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_SIGNOFF'
			);
			break;
		case 'pcr_post_audit':
			$ret = array(
				'table'=>'TBLPCR_POSTAUDIT', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_POSTAUDIT'
			);
			break;
		default:
			$ret = array(
				'table'=>'TBLPCR_DETAILS', 
				'id'=>'PCR_ID',
				'view'=> 'VIEW_PCR_DETAILS'
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{
	$ret = array();
	global $id;
	switch($action)
	{
		case 'results':
		case 'search':
			$ret = array(
			  'PCR_DISPLAYID' => array('label'=>'PCR No', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 
			  'CRITICALITY_ID' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_form_draw_pcr_impact_level_dd', 'col'=>2, 'col_span'=>1), 				
				'ORIGINATOR_ID' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
				'AREA' => array('label'=>'Area', 'delim'=>"'", 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1), 
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>1),
				'OVERALL_STATUS' => array('label'=>'PCR Status', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_pcr_status_types_dd', 'col'=>2, 'col_span'=>1), 
				'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_form_draw_pcr_type_of_change_dd', 'col'=>1, 'col_span'=>1), 
				'PCR_TYPE' => array('label'=>'Type Of PCR', 'delim'=>"'", 'value'=>'', 'func'=>'html_draw_pcr_type_dd', 'col'=>2, 'col_span'=>1), 
				'EQUIPMENT_ID' => array('label'=>'Equipment No', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 				
				'TCP' => array('label'=>'TCP', 'value'=>'', 'delim'=>"'", 'func'=>'html_draw_yes_no_dd', 'col'=>2, 'col_span'=>2),
				'DUE_DATE_FROM' => array('label'=>'Due Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1), 
				'DUE_DATE_TO' => array('label'=>'Due Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
				'PCR_DATE_FROM' => array('label'=>'Entered Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1), 
				'PCR_DATE_TO' => array('label'=>'Entered Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1)
			
			);
			break;
		
		case 'new':
			$ret =  array(
			
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'', 'col'=>1, 'col_span'=>1),
				'CRITICALITY_ID' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_form_draw_pcr_impact_level_dd', 'col'=>2, 'col_span'=>1),
				'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>1),
				'TCP' => array('label'=>'TCP', 'value'=>'', 'func'=>'html_draw_yes_no_radioset', 'col'=>2, 'col_span'=>1),
				'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_form_draw_pcr_type_of_change_dd', 'col'=>1, 'col_span'=>2), 
				'ORIGINATOR_ID' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
			  'EMPLOYEE_NO' => array('label'=>'Employee No', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1),
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1),
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>1),
				'POSITION_ID' => array('label'=>'Position', 'value'=>'', 'func'=>'html_form_draw_employee_positions_dd', 'col'=>2, 'col_span'=>1),
				'PCR_DATE' => array('label'=>'Date Entered', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2),
				'Reason_For_Change' => array('label'=>'Reason For Change', 'value'=>NULL, 'func'=>'html_draw_pcr_reason_for_change_checkboxset', 'col'=>1, 'col_span'=>2),
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
				'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'AREA_ID' => array('label'=>'Area', 'value'=>NULL, 'func'=>'html_draw_pcr_site_area_dd', 'col'=>1, 'col_span'=>1),
				'EQUIPMENT_ID' => array('label'=>'Equipment No', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1) 				

			);
			break;
		case 'edit':
			$ret =  array(
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'', 'col'=>1, 'col_span'=>1),
				'CRITICALITY_ID' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_form_draw_pcr_impact_level_dd', 'col'=>2, 'col_span'=>1),
				'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_draw_pcr_type_radioset', 'col'=>1, 'col_span'=>1),
				'TCP' => array('label'=>'TCP', 'value'=>'', 'func'=>'html_draw_yes_no_radioset', 'col'=>2, 'col_span'=>1),
				'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_form_draw_pcr_type_of_change_dd', 'col'=>1, 'col_span'=>2), 
				'ORIGINATOR_ID' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_draw_pcr_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1),
		    'EMPLOYEE_NO' => array('label'=>'Employee No', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>2, 'col_span'=>1),
				//'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked', 'col'=>1, 'col_span'=>1), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_site_linked', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1), 
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>1), 
				//'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
				//'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1),
				//'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>1),
        'POSITION_ID' => array('label'=>'Position', 'value'=>'', 'func'=>'html_form_draw_position', 'col'=>2, 'col_span'=>1),
				'PCR_DATE' => array('label'=>'Date Entered', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2),
				'Reason_For_Change' => array('label'=>'Reason For Change', 'value'=>NULL, 'func'=>'html_draw_pcr_reason_for_change_checkboxset', 'col'=>1, 'col_span'=>2),
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
				'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'AREA_ID' => array('label'=>'Area', 'value'=>NULL, 'func'=>'html_draw_pcr_site_area_dd', 'col'=>1, 'col_span'=>1),
				'EQUIPMENT_ID' => array('label'=>'Equipment No', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1) 				

			);
			break;
		case 'view':
//"PCR_ID","PCR_DISPLAYID","CRITICALITY_ID","PCR_TYPE","CHANGETYPE_ID","ORIGINATOR_ID","EMPLOYEE_NO","SITE_ID","DEPARTMENT_ID","SECTION_ID","POSITION_ID","CREW_ID","PCR_DATE","OPERATION_ID","TITLE","DESCRIPTION","AREA_ID",
//"AREA","EQUIPMENT_ID","AUTHORITYID_1","AUTHORITYID_2","AUTHORITYSTATUS_1","AUTHORITYSTATUS_2","AUTHORITYDATE_1","AUTHORITYDATE_2","AUTHORITYCOMMENT_1","AUTHORITYCOMMENT_2","WO_NUMBER","EVALUATION_ID","INITIALREVIEW_REQUIRED","MEETING_DATE","PROCESS_STEP","PROCESSSTEP_STATUS","OVERALL_STATUS","UNIT","REASSIGNEDTO_ID","COMMENTS","ORIGINATORNAME","AUTHORITIYNAME_1","AUTHORITIYNAME_2","EVALUATIONNAME","REASSIGNEDTO","IMPACTLEVEL","TYPEOFCHANGE","OPERATION","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","POSITION_DESCRIPTION","SECTION_DESCRIPTION","TCP","ACTIONS_OPEN","ACTIONS_CLOSED","MAKE_PERMANENT","DATE_MAKE_PERMANENT","EXTEND_PERMANENT","DATE_EXTEND_PERMANENT","DUE_DATE"
			$ret =  array(
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
				'PCR_ID' => array('label'=>'PCR ID', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'DESCRIPTION' => array('label'=>'Detailed Description', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'PCR_TYPE' => array('label'=>'PCR Type', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'TCP' => array('label'=>'TCP', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'CHANGETYPE_ID' => array('label'=>'Type Of Change', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'ORIGINATORNAME' => array('label'=>'Originator', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
		    'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
        'POSITION_DESCRIPTION' => array('label'=>'Position', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'PCR_DATE' => array('label'=>'Date Entered', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'REASON_FOR_CHANGE' => array('label'=>'Reason For Change', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'AREA_DESCRIPTION' => array('label'=>'Area', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'EQUIPMENT_ID' => array('label'=>'Equipment No', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2) 				

			);
			break;


		case 'pcr_initial_review':
			$ret =  array(
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'', 'col'=>1, 'col_span'=>1)			

			);
			break;	

		case 'pcr_evaluation':
			$ret =  array(
				
				'ASSESSMENT_SCORE' => array('label'=>'Assessment Score', 'value'=>NULL, 'func'=>'html_form_draw_pcr_assessment_score_dd', 'col'=>1, 'col_span'=>1),	
				'ASSESSMENT_LEVEL' => array('label'=>'Assessment Level', 'value'=>NULL, 'func'=>'html_form_draw_pcr_assessment_level_dd', 'col'=>2, 'col_span'=>1),
        'EVALUATION_NAME' => array('label'=>'Name', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
				'EVALUATION_DATE' => array('label'=>'Date', 'delim'=>"'", 'operator'=>'DATE', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
				'EVALUATION_COMMENTS' => array('label'=>'Evaluation Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),	

        // OPTIONAL FOR KWINANA
				'AREA_REVIEW_REQ' => array('label'=>'Is an Area Review Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),	
				'TEAM_REVIEW_REQ' => array('label'=>'Is PCRT Review Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),	
				'EXT_REVIEW_REQ' => array('label'=>'Is an Extension Review Required?', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2)

			);
			break;

		case 'pcr_area_review':
			$ret =  array(
			  /* START GENERAL PCR DETAILS */
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'TYPE_OF_PCR' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'STATUS' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
		    /* END GENERAL PCR DETAILS */		
				
			  /* START RECOMMEDATION CONTROL (FLAGGED CONTROL)*/
				'PCR_TYPE' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'PERMANENT_TRIAL' => array('label'=>'Make Trial Permanent?', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
        'PERMANENTDATE' => array('label'=>'Date', 'delim'=>"'", 'operator'=>'DATE_PERM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
        'EXTEND_CHANGES' => array('label'=>'Extend Permanent Changes To All Lines? ', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
        'EXTENDDATE' => array('label'=>'Date', 'delim'=>"'", 'operator'=>'DATE_EXT', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
				'ASSESSMENT_SCORE' => array('label'=>'Original Assessment Score', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
        'ASSESSMENT_LEVEL' => array('label'=>'Original Assessment Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
        'CONFIRMATION' => array('label'=>'Assessment Level by PCRT', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
        'REVISED_SCORE' => array('label'=>'Revised Score', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
        'ACTIONTAKEN' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
        'ACTION_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
        'CHANGE_BY' => array('label'=>'By', 'value'=>NULL, 'func'=>'', 'col'=>1, 'col_span'=>2),
        'ACTIONDATE' => array('label'=>'Date', 'delim'=>"'", 'operator'=>'DATE_ACT', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
        'AFE_NUMBER' => array('label'=>'AFE Number (if applicable)', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
				/* END RECOMMEDATION CONTROL */
				
				);
			break;			

		case 'pcr_pcrt_review':
			$ret =  array(			    
			
			  /* START GENERwAL PCR DETAILS */
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'TYPE_OF_PCR' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'STATUS' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			  /* END GENERAL PCR DETAILS */		);
			break;	


		case 'pcr_extension':
			$ret =  array(
			  /* START GENERAL PCR DETAILS */
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'TYPE_OF_PCR' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'STATUS' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			    /* END GENERAL PCR DETAILS */		);
			break;	

		case 'pcr_actions':
			$ret =  array(			    /* START GENERAL PCR DETAILS */
				'PCR_DISPLAYID' => array('label'=>'PCR No', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'TYPE_OF_PCR' => array('label'=>'Type Of PCR', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'CRITICALITY' => array('label'=>'Criticality', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),	
				'STATUS' => array('label'=>'Status', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),	
				'TITLE' => array('label'=>'Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			    /* END GENERAL PCR DETAILS */		);
			break;	


		case 'pcr_sign_off':
			$ret =  array(			    
			 
			   /* START GENERAL */
         'REVIEW_TYPE' => array('label'=>'Type Of Review', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>1),	
				 'MEETINGDATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
         'MEETING_COMMENTS' => array('label'=>'Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				 /* END GENERAL */
			 
				 /* START RECOMMENDATION */
         'SIGN_OFF' => array('label'=>'PCR Team Sign-Off', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>1),	
				 'SIGNED_OFF_DATE' => array('label'=>'Signed-Off Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
				 /* END RECOMMENDATION */

				 /* START PCR SIGN-OFF / CLOSURE */
         'AUDITBY_ID' => array('label'=>'Tiwest Contact', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
				 'DUE_DATE' => array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1)
				 /* END PCR SIGN-OFF / CLOSURE */
				 );
			break;	

		case 'pcr_post_audit':
			$ret =  array(
			
				'AUDIT_COMMENTS' => array('label'=>'Audit Comments', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
        'COMPLETED_BY' => array('label'=>'Audit Completed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
				'AUDIT_DATE' => array('label'=>'Audit Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1)

			);
			break;	

	}
	return $ret;
}
//$db->debug=true;
// Fix page parameter
if($p=='post')
	$pg=(isset($a) && $a=='add')?'new':'edit';
else
	$pg=$p;

// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($pg);
$TABLES  = getPropertiesTables($pg);
$FIELDS  = getPropertiesFields($pg);

?>