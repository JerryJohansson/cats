<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = "PCR EVALUATION";//ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" SRC="js/MultiDocuments.js"></SCRIPT>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/validation_helpers.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	init_resize_editor();
	return true;
}
onload = function(){
	init();
}



/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

</script>
</head><body class="tb">
<?php


// Assign what javascript handler to use for client side validation
$onSubmit = '';
$onSubmit = "return CATS_validateForm(this, 'TYPEOFCHANGE','','R','ORIGINATOR_ID','','R','PCR_DESCRIPTION','','R');";

?>
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="<?php echo($onSubmit) ?>">
  <!-- BEGIN:: Of Toolbar Buttons -->
  <div id="tool_bar_c">
    <fieldset class="bar" id="tab_buttons">
    <a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Details</a>
    </fieldset>
    <fieldset class="tool_bar">
    <?php if($action=='edit'){ ?>
    <a
	title="New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
    <?php } ?>
    <a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a> <a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a> <a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>
    </fieldset>
  </div>
  <!-- END:: Of Toolbar Buttons -->
  <div id="Lcontentbody">
    <!-- Edit Main Fields -->
    <fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php include("includes/menu.php"); ?>	

<?php

// Instantiate a new editor
$ed = new Editor("edit");

// Check whether to edit or add a new record
echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id));

print_r($FIELDS);
print_r($TABLES['view']);
print_r($TABLES['id']);

?>

    <fieldset class="tbar" style="text-align:right; ">
    <input type="submit" name="cats::Save" value="Save" >
    <?php if($action=='edit'){ ?>
    <input type="button" name="cats::New" value="New Site" onClick="_m.newModule();" >
    <?php } ?>
    <input type="button" name="Cancel" value="Cancel" onClick="_m.cancel();">
    </fieldset>
    </fieldset>
  </div>
</form>