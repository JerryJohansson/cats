<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');

//$PCR_DETAILS = Get_PCR_Details_On_ID($_POST, $_GET);


?>


<SCRIPT LANGUAGE="JavaScript" SRC="js/MultiDocuments.js"></SCRIPT>
<script language="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";//used for the remote scripting when getting the managed by site and department

//convert the dates into a string format before saving them to the database
function ChangeDates()
{
	try{
	document.frmData.PCRDate.value = ConvertDate('PCRDate', '/', 'AUS');
	//document.frmData.AuthorityDate_1.value = ConvertDate('AuthorityDate_1', '/', 'AUS');
	//document.frmData.AuthorityDate_2.value = ConvertDate('AuthorityDate_2', '/', 'AUS');
	document.frmData.Meeting_Date.value = ConvertDate('Meeting_Date', '/', 'AUS');
	}catch(e){}
}



function CheckCategory()
{
	var bChecked = false;
	for(i=1;i<=6;i++){
		var objCheck = document.getElementById("ActionCategory_" + i);
		if(objCheck.checked == true){bChecked = true; break;}
	}
	if(bChecked == true){
		document.frmData.valCategory.value = "Don't Validate";
	}else{
		document.frmData.valCategory.value = "";
	}
}



function ShowDescription(SpanName, Message){
	var objSpan = document.getElementById(SpanName);
	objSpan.title = Message;
	return objSpan;
}



//stuff we do when the form loads
function StartUp()
{
	//these fields only get displayed if the logged on user is from the Pigment Plant(Kwinana site)
	if("BE" != 'PI'){
		ShowReason(true);//('block');
	}else{
		ShowReason(false);//('none');
	}
}



//fields that get displayed if the site is the Pigment Plant(Kwinana site)
function ShowReason(show){
	var i=0;
	var cnt=5;
	for(i=1;i<cnt;i++){
		if(i==3) show=!show;
		try{element("rowCO_" + i).style.display = (show?'block':'none');}catch(e){}
	}
}



function GetEmpDetails()
{
	var iEmpId = document.frmData.OriginatorId.value;
	var sFields = "EmployeeNo|SiteId|SiteDesc|DepartmentId|DepartmentDesc|SectionId|SectionDesc|PositionId|PositionDesc";
	var arFields = sFields.split("|");
	var x = arFields.length;

	if(iEmpId != ""){
		var sWhere = "SELECT Employee_Number, Site_Id,  Site_Description, Department_Id, Department_Description, Section_Id, Section_Description, Position, Position_Name FROM View_Employee_Details WHERE Employee_Number = " + iEmpId;
		var sColumns = "Employee_Number|Site_Id|Site_Description|Department_Id|Department_Description|Section_Id|Section_Description|Position|Position_Name";
		
		var DataString = RSExecute(serverURL, "GetSingleRowData", sWhere, sColumns);
		arData = DataString.return_value.split("|");
		
		for(var i=0; i<x;i++){
			var objField = document.getElementById(arFields[i]);
			if(arData[i] == null || arData[i] == 'null'){
				objField.value = "";
			}else{
				objField.value = arData[i];
			}
		}
		var iSiteId = document.frmData.SiteId.value;
		var sWhere = "Select Area_Id, Area_Description FROM tblPCR_Area WHERE Site_Id = " + iSiteId + " ORDER BY Area_Description ASC";
		var sColumn1 = "Area_Id";
		var sColumn2 = "Area_Description";
		ReturnData(sWhere, sColumn1, sColumn2, 'AreaId');
	}else{
		for(var i=0; i<x;i++){
			var objField = document.getElementById(arFields[i]);
			objField.value = "";
		}
	}
	
	//show or hide the required rows if the chosen site is the Pigment Plant (Kwinana)
	if(document.frmData.SiteId.value != 3){
		ShowReason(true);//('block');
	}else{
		ShowReason(false);//('none');
		//document.frmData.Review_Required.checked = true;
	}
}






function validatePCRDetails(f)
{
	var returnValue = true;
	returnValue = checkLengthOf("PCR_Title",255,"The 'Title' field must have no more than #MAX# characters.\nThe text will now be truncated.");
	if(returnValue) returnValue = checkLengthOf("PCR_Description",1000,"The 'Detailed Description' field must have no more than #MAX# characters.\nThe text will now be truncated.");
	if(returnValue){
		if(document.frmData.SiteId.value == 3){
			MM_validateForm('ImpactId','','R','PCR_Type','','R','OriginatorId','','R','PCRDate','','R','PCR_Description','','R');
		}else{
			MM_validateForm('PCR_Type','','R','ChangeTypeId','','R','OriginatorId','','R','PCRDate','','R','PCR_Description','','R');
		}
	}else{
		document.MM_returnValue = returnValue;
	}
}


</script>


<script type="text/JavaScript">
<!--
function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}
//-->
</script>


<body>
<form action="index.php?m=<?php echo $m;?>&p=post&a=new_pcr&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">

<b>PCR Details New</b>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td colspan="2"><span class="subheader"><strong>General Details</strong></span></td>
  </tr>
  <tr>
    <td width="24%"><span class="normalLabel">PCR No.:</span></td>
    <td width="76%"><span class="normal">Automatically generated</span></td>
  </tr>
  <tr>
    <td><span class="normalLabel hide">Criticality:</span></td>
    <td><span class="normal hide">
      <?PHP echo get_impact_level_dd(); ?>
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Type of PCR:</span></td>
    <td><span class="normal"><?PHP echo get_type_of_pcr_radio(); ?></span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">TCP:</span></td>
    <td><span class="normal">
      <input type="radio" name="TCP" id="id_TCP_yes" class="inputTextBox" value="Yes">
      <label for="id_TCP_yes">Yes</label>
      <input type="radio" name="TCP" id="id_TCP_no" class="inputTextBox" value="No" checked>
      <label for="id_TCP_no">No</label>
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Type of Change:</span></td>
    <td><span class="normal">
      <?PHP echo get_type_of_change_dd(); ?>
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Originator:</span></td>
    <td><span class="normal">
      <input type="text" name="OrigId" size="5" onChange="NameFilter(OrigId, OriginatorId);GetEmpDetails();" class="inputTextBox">
      <select name="OriginatorId" id="Originator" class="inputTextBox" onChange="GetEmpDetails()">
        <option value=""></option>
        <script>
				  document.write(ReadActiveEmps());
		</script>
      </select>
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Employee No:</span></td>
    <td><span class="normal">
      <input type="text" name="EMPLOYEE_NO" class="inputTextBox" size="10" id="Employee No">
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Site:</span></td>
    <td><span class="normal">
      <input type="hidden" name="SiteId">
      <input type="text" size="30" class="inputTextBox" name="SiteDesc">
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Department:</span></td>
    <td><span class="normal">
      <input type="hidden" name="DepartmentId">
      <input type="text" size="30" class="inputTextBox" name="DepartmentDesc">
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Section:</span></td>
    <td><span class="normal">
      <input type="hidden" name="SectionId">
      <input type="text" size="30" class="inputTextBox" name="SectionDesc">
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Position:</span></td>
    <td><span class="normal">
      <input type="hidden" name="PositionId">
      <input type="text" size="30" class="inputTextBox" name="PositionDesc">
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Date:</span></td>
    <td><span class="normal">
      <input type="text" size="12" class="inputTextBox" name="PCRDate" id="Date" onBlur="ValidateDateFields('PCRDate')" value="18/10/2005">
      <a href="#" onClick="callCalendar('anchor3','PCRDate');" name="anchor3" id="anchor3"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> (dd/mm/yyyy)</span></td>
  </tr>
  <tr>
    <td valign="top"><span class="normalLabel">Reason for Change:</span></td>
    <td>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        New Product/Service <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        Increased Production <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        Capacity Cost Reduction <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        Technical Capability <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        Economic Protection <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        Health Safety Environment <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        New Product/Service <br>
		<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
        Other Mandatory <br>

        <script>
			    document.write(ReadReasonFor());
			  </script></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Title:</span></td>
    <td><span class="normal">
      <input type="text" class="inputTextBox" id="Title" name="PCR_Title" style="width:80%" maxlength="255" onKeyPress="return checklength(255);" />
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Detailed Description:<br>
      (What and Why)</span></td>
    <td><span class="normal">
      <textarea class="inputTextBox" rows="3" id="Detailed Description" cols="95" style="width:80%" name="DESCRIPTION" onKeyPress="return checklength(1000);"></textarea>
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Area:</span></td>
    <td><span class="normal">
      <select name="AreaId" id="Area" class="inputTextBox">
        <option value=""></option>
      </select>
      </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Equipment No:</span></td>
    <td>
	
	<span class="normal">
      <input type="text" name="EQUIPMENT_ID" id="Equipment No" size="10" class="inputTextBox" />
    </span>
	
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="hidden" name="MM_insert" value="true">
      <input type="hidden" name="PCR_Id">
      <input type="hidden" name="PCRDisplay_Id">
      <input type="hidden" name="PROCESS_STEP" value="PCR Registration">
      <input type="hidden" name="PROCESSSTEP_STATUS" value="Entered">
      <input type="hidden" name="OVERALL_STATUS" value="Open"></td>
  </tr>
</table>



<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr>
    <td bgcolor="#CCCCCC" class="subheader" colspan="4">Supporting Documents</td>
  </tr>
  <tr>
    <td colspan="4">
	
	
	<table id="tblDocuments" width="97%" cellspacing="4">
    </table>
	
	  
	  
	  </td>
  </tr>
  <tr>
    <td class="normalLabel" width="16%">&nbsp;&nbsp;Document Location:</td>
    <td class="normal" colspan="3">
	
	  <input class="LinkedTextbox" type="text" name="Document_1" size="90" maxlength="255" onDblClick="ClickLink(Document_1, LoadDoc)">
      <input class="Button" type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Document_1')">
      <input type="file" name="BrowseDialog" style="display: none">
      <a style="display: none" href="#" id="LoadDoc" target="_blank">Link To File</a>
      <input type="hidden" name="NumDocuments" value="2">
	  
    </td>
  </tr>
  <tr>
    <td class="normalLabel" colspan="4" height="35" valign="top"><input type="button" class="Button" name="btnDocument" value="Add another document" onClick="addDocumentRow('tblDocuments','16%', 'Document Location:', 'Document_', 'NumDocuments','90')">
    </td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td><span class="normal">
      <input class="Button" type="submit" name="Save" value="Save" onClick="try{document.frmData.elements('BrowseDialog').disabled=true}catch(e){}">
      <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_callJS('history.back()')">
      <input class="Button" type="reset" name="Reset" value="Reset">
    </span></td>
  </tr>
</table>
</form>