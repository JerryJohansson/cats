<?PHP @ LANGUAGE="JAVASCRIPT" ?> 
<!--#INCLUDE file="../Include/History.asp"-->
<!--#include file="../Include/CATS_Variables.asp" -->
<!--#include file="../Connections/cats.asp" -->
<!--#include file="Include/PCRFunctions.asp" -->
<?PHP 


if(String(Session("MM_Maintenance")) == "Yes")
{
    Response.Redirect("Maintenance.htm");
}


var UsersSite = Session("MM_UserSite");
if(UsersSite == "" || UsersSite == null)
{
    Response.Redirect("../Logon.htm");
}




var rsAccess = Server.CreateObject("ADODB.Recordset");
rsAccess.ActiveConnection = MM_cats_STRING;
rsAccess.Source = "SELECT Group_Description FROM tblAccess_Groups  
                   WHERE Page_name = 'PCR_Extension' 
				   ORDER BY Group_Description ASC";
				   
rsAccess.CursorType = 0;
rsAccess.CursorLocation = 2;
rsAccess.LockType = 3;
rsAccess.Open();







// *** Restrict Access To Page: Grant or deny access to this page
var MM_dbGroups=rsAccess.Fields.Item("Group_Description").Value;
rsAccess.Close();
var MM_authorizedUsers=MM_dbGroups;
var MM_authFailedURL="../Failed.htm";
var MM_grantAccess=false;
if (String(Session("MM_Username")) != "undefined") 
{
  if (false || (String(Session("MM_UserAuthorization"))=="") || (MM_authorizedUsers.indexOf(String(Session("MM_UserAuthorization"))) >=0)) 
  {
      MM_grantAccess = true;
  }
}

if (!MM_grantAccess) 
{
  var MM_qsChar = "?";
  if (MM_authFailedURL.indexOf("?")>=0) MM_qsChar = "&";
  var MM_referrer = Request.ServerVariables("URL");
  if (String(Request.QueryString()).length > 0) MM_referrer = MM_referrer + "?" + String(Request.QueryString());
  MM_authFailedURL = MM_authFailedURL + MM_qsChar + "accessdenied=" + Server.URLEncode(MM_referrer);
  Response.Redirect(MM_authFailedURL);
}



// *** Edit Operations: declare variables

// set the form action variable
var MM_editAction = Request.ServerVariables("URL");
if (Request.QueryString) {
  MM_editAction += "?" + Request.QueryString;
}


/*

// boolean to abort record edit
var MM_abortEdit = false;
// query string to execute
var MM_editQuery = "";




// *** Insert Record: set variables
if (String(Request("MM_update")) != "undefined" &&
    String(Request("MM_recordId")) != "undefined") {
  
  	var MM_editConnection = MM_cats_STRING;
  	var MM_editTable  = "tblPCR_Extension";
  	var MM_editColumn = "PCR_Id";
  	var MM_recordId = "" + Request.Form("MM_recordId") + "";																																						  
  	var MM_fieldsStr = "PCR_Type|value|Permanent_Trial|value|Extend_Changes|value|PermanentDate|value|ExtendDate|value|Confirmation|value|Revised_Score|value|ActionTaken|value|Action_Comments|value|Change_By|value|ActionDate|value|AFE_Number|value|Review_Type|value|MeetingDate|value|Meeting_Comments|value"; 
  	var MM_columnsStr = "PCR_Type|',none,''|Make_Permanent|none,1,0|Extend_Permanent|none,1,0|Date_Make|',none,NULL|Date_Extend|',none,NULL|Confirmation|',none,''|Revised_Score|none,none,NULL|Action|',none,''|Comments|',none,''|Due_By|',none,''|Due_Date|',none,NULL|AFE_Number|',none,''|Type_Of_Review|',none,''|Meeting_Date|',none,NULL|Meeting_Comments|',none,''";

  	var MM_editRedirectUrl = "PCRExtension.asp?Report_Id=" + MM_recordId;
  	if(String(Request.QueryString("ReturnURL")) != "undefined") { 
	  	MM_editRedirectUrl = Request.QueryString("ReturnURL"); 
  	}

  	// create the MM_fields and MM_columns arrays
  	var MM_fields = MM_fieldsStr.split("|");
  	var MM_columns = MM_columnsStr.split("|");

  	// set the form values
  	for (var i=0; i+1 < MM_fields.length; i+=2) {
    	MM_fields[i+1] = String(Request.Form(MM_fields[i]));
  	}
  	// create the sql update statement
  	MM_editQuery = "update " + MM_editTable + " set ";
  	for (var i=0; i+1 < MM_fields.length; i+=2) {
    	var formVal = MM_fields[i+1];
    	var MM_typesArray = MM_columns[i+1].split(",");
    	var delim =    (MM_typesArray[0] != "none") ? MM_typesArray[0] : "";
    	var altVal =   (MM_typesArray[1] != "none") ? MM_typesArray[1] : "";
    	var emptyVal = (MM_typesArray[2] != "none") ? MM_typesArray[2] : "";
    	if (formVal == "" || formVal == "undefined") {
      		formVal = emptyVal;
    	} else {
      		if (altVal != "") {
        		formVal = altVal;
      		} else if (delim == "'") { // escape quotes
        		formVal = "'" + formVal.replace(/'/g,"''") + "'";
      		} else {
        		formVal = delim + formVal + delim;
      		}
    	}
    	MM_editQuery += ((i != 0) ? "," : "") + MM_columns[i] + " = " + formVal;
  	}
  	MM_editQuery += " where " + MM_editColumn + " = " + MM_recordId;
//		Response.Write(MM_editQuery); Response.End();
  	if (!MM_abortEdit) {
    	// execute the insert
    	var MM_editCmd = Server.CreateObject('ADODB.Command');
    	MM_editCmd.ActiveConnection = MM_editConnection;
    	MM_editCmd.CommandText = MM_editQuery;
    	MM_editCmd.Execute();
    	MM_editCmd.ActiveConnection.Close();
	}
	
	//*********************************************************
	//save the multi documents to the table tblMultiDocuments
	var sFormType = "PCR_Extension";
	var sFormArea = "";
	var sDocumentName = "Document_"; 
	var hdnCounter = "NumDocuments";
	var bEditPage = true;
	 ?>
	<!--#include file="Include/MultiDocumentsForm.asp" -->
	<?PHP  
	//*********************************************************
  
	//############## delete the details from tblMOCApproverDetails #######################
	var iPCRId = String(Request.Form("MM_recordId"));
	var MM_deleteCmd = Server.CreateObject('ADODB.Command');
	MM_deleteCmd.ActiveConnection = MM_editConnection;
	MM_editQuery = "delete from tblPCR_Extension_Details WHERE PCR_Id = " + iPCRId;
	MM_deleteCmd.CommandText = MM_editQuery;
	MM_deleteCmd.Execute();
	MM_deleteCmd.ActiveConnection.Close();

	//############## insert the details into tblMOCApproverDetails #######################
	var MM_editTable  = "tblPCR_Extension_Details";
	var MM_fieldsStr = "PCR_Id|value|Detail_Id|value|Reviewer_Id|value|Date_Reviewed|value|Status|value|Comments|value|Document_Loc|value|Required|value";
	var MM_columnsStr = "PCR_Id|none,none,NULL|Detail_Id|none,none,NULL|Reviewer_Id|none,none,NULL|Date_Reviewed|',none,NULL|Status|',none,''|Comments|',none,''|Document_Loc|',none,''|Required|none,1,0";
		
	// create the MM_fields and MM_columns arrays
	var MM_fields = MM_fieldsStr.split("|");
	var MM_columns = MM_columnsStr.split("|");
	  
	// set the form values
	for (var i=0; i+1 < MM_fields.length; i+=2) {
	  MM_fields[i+1] = String(Request.Form(MM_fields[i]));
	}
	  
	var bCloseCmd = false;  
	var iNumReviewers = Request.Form("NumReviewers");//number of documents to be saved
	for(c=0; c<iNumReviewers; c++){//loop for the number of documents to be saved
		var sReviewerTypeId = "DetailId_" + c;     
		var sReviewerId = "ReviewerId_" + c;
		var sDate = "ReviewDate_" + c;
		var sStatus = "Approved_" + c;
		var sComment = "Comment_" + c;
		var sChecklist = "Checklist_" + c;
		var sRequired = "Required_" + c;
		
		MM_fields[1] = iPCRId;  
		MM_fields[3] = String(Request.Form(sReviewerTypeId));
		MM_fields[5] = String(Request.Form(sReviewerId));
		MM_fields[7] = String(Request.Form(sDate));
		MM_fields[9] = String(Request.Form(sStatus));
		MM_fields[11] = String(Request.Form(sComment));
		MM_fields[13] = String(Request.Form(sChecklist));
		MM_fields[15] = String(Request.Form(sRequired));
		  
		// create the sql insert statement
		var MM_tableValues = "", MM_dbValues = "";
		for (var i=0; i+1 < MM_fields.length; i+=2) {
		  var formVal = MM_fields[i+1];
		  var MM_typesArray = MM_columns[i+1].split(",");
		  var delim =    (MM_typesArray[0] != "none") ? MM_typesArray[0] : "";
		  var altVal =   (MM_typesArray[1] != "none") ? MM_typesArray[1] : "";
		  var emptyVal = (MM_typesArray[2] != "none") ? MM_typesArray[2] : "";
		  if (formVal == "" || formVal == "undefined") {
			formVal = emptyVal;
		  } else {
			if (altVal != "") {
			  formVal = altVal;
			} else if (delim == "'") { // escape quotes
			  formVal = "'" + formVal.replace(/'/g,"''") + "'";
			} else {
			  formVal = delim + formVal + delim;
			}
		  }
		  MM_tableValues += ((i != 0) ? "," : "") + MM_columns[i];
		  MM_dbValues += ((i != 0) ? "," : "") + formVal;
		}
		MM_editQuery = "insert into " + MM_editTable + " (" + MM_tableValues + ") values (" + MM_dbValues + ")";
		
		if (!MM_abortEdit) {
		  // execute the insert
		  var MM_editCmd = Server.CreateObject('ADODB.Command');
		  MM_editCmd.ActiveConnection = MM_editConnection;
		  MM_editCmd.CommandText = MM_editQuery;
		  MM_editCmd.Execute();
		  bCloseCmd = true;
		}
	}//end for(c=0; c<iNumReviewers; c++)
	if(bCloseCmd == true){
		MM_editCmd.ActiveConnection.Close();
	}

    if (MM_editRedirectUrl) {
      Response.Redirect(MM_editRedirectUrl);
    }
}


*/



var rsPCRDetails__MMColParam = "1";
if(String(Request.QueryString("Report_Id")) != "undefined") 
{ 
    rsPCRDetails__MMColParam = String(Request.QueryString("Report_Id"));
}



var rsPCRDetails = Server.CreateObject("ADODB.Recordset");
rsPCRDetails.ActiveConnection = MM_cats_STRING;
rsPCRDetails.Source = "SELECT PCR_Id, PCR_DisplayId, Site_Id, Area_Id, Title, Description, PCR_Type, Overall_Status, ImpactLevel 
                       FROM View_PCR_Details 
					   WHERE PCR_Id = ";

rsPCRDetails.CursorType = 0;
rsPCRDetails.CursorLocation = 2;
rsPCRDetails.LockType = 3;
rsPCRDetails.Open();
var rsPCRDetails_numRows = 0;
var MM_PCRId = rsPCRDetails.Fields.Item("PCR_Id").Value;
var iSiteId = rsPCRDetails.Fields.Item("Site_Id").Value;
var site_code = getFieldValue("select Site_Code from tblSite where Site_Id = "+iSiteId);









var rsTeamDetails = Server.CreateObject("ADODB.Recordset");
rsTeamDetails.ActiveConnection = MM_cats_STRING;
rsTeamDetails.Source = "SELECT * FROM tblPCR_Extension 
                        WHERE PCR_Id = " + MM_PCRId;
						
rsTeamDetails.CursorType = 0;
rsTeamDetails.CursorLocation = 2;
rsTeamDetails.LockType = 3;
rsTeamDetails.Open();
var rsTeamDetails_numRows = 0;







var rsReviewers = Server.CreateObject("ADODB.Recordset");
rsReviewers.ActiveConnection = MM_cats_STRING;
rsReviewers.Source = "SELECT * FROM View_PCR_Extension 
                      WHERE Review_Type = 'Area' 
					  AND Site_Id = " + iSiteId + " 
					  ORDER BY Reviewer_Type ASC";
					  
rsReviewers.CursorType = 2;
rsReviewers.CursorLocation = 2;
rsReviewers.LockType = 3;
rsReviewers.Open();
var arDefaults = new Array();
var n = 0;
if(!rsReviewers.BOF || !rsReviewers.EOF){
	while(!rsReviewers.EOF){
		var iDetailId = rsReviewers.Fields.Item("Detail_Id").Value;
		var iAreaId = rsPCRDetails.Fields.Item("Area_Id").Value;
		var rsData = Server.CreateObject("ADODB.Recordset");
		rsData.ActiveConnection = MM_cats_STRING;
		rsData.Source = "SELECT Reviewer_Id 
		                 FROM tblPCR_Reviewer_Name 
						 WHERE Detail_Id = " + iDetailId + " 
						 AND Area_Id = " + iAreaId;
						 
		rsData.CursorType = 0;
		rsData.CursorLocation = 2;
		rsData.LockType = 3;
		rsData.Open();
		if(!rsData.BOF || !rsData.EOF){
			arDefaults[n] = rsData.Fields.Item("Reviewer_Id").Value;
		}else{
			arDefaults[n] = "";
		}
		rsData.Close();
		n++;
		rsReviewers.MoveNext();
	}
		rsReviewers.MoveFirst();
}
var rsReviewers_numRows = 0;
var Repeat1__index = 0;






var rsDocuments = Server.CreateObject("ADODB.Recordset");
rsDocuments.ActiveConnection = MM_cats_STRING;
rsDocuments.Source = "SELECT DocumentLocation  
                      FROM tblMultiDocuments  
					  WHERE FormType = 'PCR_Extension' 
					  AND Report_Id = " + MM_PCRId;
					  
rsDocuments.CursorType = 0;
rsDocuments.CursorLocation = 2;
rsDocuments.LockType = 3;
rsDocuments.Open();
var Repeat2__index = 1;



var MM_paramName = "";

 
// *** Go To Record and Move To Record: create strings for maintaining URL and Form parameters




// create the list of parameters which should not be maintained
var MM_removeList = "&index=";
if (MM_paramName != "") MM_removeList += "&" + MM_paramName.toLowerCase() + "=";
var MM_keepURL="",MM_keepForm="",MM_keepBoth="",MM_keepNone="";





// add the URL parameters to the MM_keepURL string
for (var items=new Enumerator(Request.QueryString); !items.atEnd(); items.moveNext()) {
  var nextItem = "&" + items.item().toLowerCase() + "=";
  if (MM_removeList.indexOf(nextItem) == -1) {
    MM_keepURL += "&" + items.item() + "=" + Server.URLencode(Request.QueryString(items.item()));
  }
}




// add the Form variables to the MM_keepForm string
for (var items=new Enumerator(Request.Form); !items.atEnd(); items.moveNext()) {
  var nextItem = "&" + items.item().toLowerCase() + "=";
  if (MM_removeList.indexOf(nextItem) == -1) {
    MM_keepForm += "&" + items.item() + "=" + Server.URLencode(Request.Form(items.item()));
  }
}





// create the Form + URL string and remove the intial '&' from each of the strings
MM_keepBoth = MM_keepURL + MM_keepForm;
if (MM_keepBoth.length > 0) MM_keepBoth = MM_keepBoth.substring(1);
if (MM_keepURL.length > 0)  MM_keepURL = MM_keepURL.substring(1);
if (MM_keepForm.length > 0) MM_keepForm = MM_keepForm.substring(1);




function OnTransactionAbort()
{
?>
<script>document.write("<?PHP MM_editQuery ?>");</script>
<?PHP 
}
?>

<html>
<head>
<title>PCR Details New</title>
<link rel="stylesheet" href="../Default.css" type="text/css">
<SCRIPT LANGUAGE="JavaScript" SRC="../JavaScript/AnchorPosition.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../JavaScript/PopupWindow.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../JavaScript/CalendarPopup.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../JavaScript/DateTimeFunctions.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="../JavaScript/Calendar.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="JavaScript/MultiDocuments.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" SRC="JavaScript/general.js"></SCRIPT>
<script language="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";//used for the remote scripting when getting the managed by site and department





//convert the dates into a string format before saving them to the database
function ChangeDates()
{  
	var x = document.frmData.NumReviewers.value;
	for(var i=0;i<x;i++){
		var sDate = "ReviewDate_" + i;
		var objDate = document.getElementById(sDate);
		objDate.value = ConvertDate(sDate, '/', 'AUS');
	}
	
	document.frmData.ActionDate.value = ConvertDate('ActionDate', '/', 'AUS');
	document.frmData.MeetingDate.value = ConvertDate('MeetingDate', '/', 'AUS');
}





//select the required reviewer from the drop down boxes
function SelectReviewers()
{
	var x = document.frmData.NumReviewers.value;
	for(var i = 0;i < x;i++)
	{
		var sDropDown = "ReviewerId_" + i;
		var objOld = document.getElementById("OldAReviewerId_" + i);
		var iDefault = objOld.value;
		
		//if a reviewer hasn't been saved yet we use the default reviewer
		if(iDefault == "")
		{
			var objDefault = document.getElementById("DefaultId_" + i);
			var iDefault = objDefault.value;
		}
		
		SelectDropDown(sDropDown,iDefault);
	}
	
}





//stuff we do when the form loads
function StartUp()
{
	ShowTeam();
	SelectReviewers();
}





function ShowApprovers(sCheck, sRow, sRequired)
{
	var objCheck = document.getElementById(sCheck);
	var objRow = document.getElementById(sRow);
	var objRequired = document.getElementById(sRequired);
	var Required = objRequired.value;
	
	if(objCheck.checked == false)
	{
		switch(Required)
		{
			case "Mandatory" :
				objCheck.checked = true;
				alert("This Reviewer is Mandatory, you cannot change this selection");
				break;
			
			case "Recommended" :
				alert("You can change this selection, but this Reviewer is Recommended.");
				objRow.style.display = "none";
				break;
			
			case "Optional" :
				objRow.style.display = "none";
				break;
		}		
	}
	else
	{
		objRow.style.display = "block";
	}
}





//enable all the form fields so we can save their values
function EnableFields()
{
	var x = document.frmData.elements.length;
	for(var i = 0;i < x; i++)
	{
		if(document.frmData.elements[i].name != "BrowseDialog")
		{
			document.frmData.elements[i].disabled = false;
		}
	}
}





//stuff we do when we submit the form
function SubmitForm()
{
	ChangeDates();
	EnableFields();
}





//show or hide the reviewers section and the details section depending on what type of review it is
function ShowTeam()
{
	var x = document.frmData.NumReviewers.value;
	if(document.frmData.Review_Type[0].checked == true)
	{
		for(var i=0;i<x;i++)
		{
			var objRow = document.getElementById("rowReviewerType_" + i);
			objRow.style.display = "none";
		}
		
		for(var i = 1; i < 6; i++)
		{
			var objRow = document.getElementById("rowType_" + i);
			objRow.style.display = "block";
		}	
	}
	else
	{
		for(var i = 0;i < x; i++)
		{
			var objRow = document.getElementById("rowReviewerType_" + i);
			objRow.style.display = "block";
		}
		
		for(var i=1;i<6;i++)
		{
			var objRow = document.getElementById("rowType_" + i);
			objRow.style.display = "none";
		}
		
	}
}

</script>
<?PHP  var DIR_WS_JAVASCRIPT="../javascript/";  ?>
<!-- #include file="../Include/CATS_Javascript.asp" -->
<SCRIPT LANGUAGE="JScript" SRC="JavaScript/PCRDropDowns.js"></SCRIPT>
</head>
<body bgcolor="#FFFFFF" text="#000000" link="#0000FF" vlink="#0000FF" alink="#0000FF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="StartUp()">
<!-- #include file="Include/PCRTabs.asp" -->
  																 
  <form ACTION="<?PHP MM_editAction ?>" METHOD="POST" name="frmData" onSubmit="SubmitForm()">
    <table width="100%" border="0">
      <tr> 
        <td  class="normal"> 
          <input class="Button" type="submit" name="Save1" value="Save">
          <input class="Button" type="button" name="Cancel1" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
          <input class="Button" type="reset" name="Reset1" value="Reset">
        </td>
      </tr>
	  <tr bordercolor="#CCCCCC">
		<td>
		  <table border="1" width="100%">
		    <tr bordercolor="#FFFFFF">
			  <td class="normalLabel" width="17%">Type Of Review:</td>
			  <td class="normal" width="32%" align="left">
			    <input align="left" type="radio" onClick="ShowTeam()" name="Review_Type" <?PHP if ($row_team_details["Type_Of_Review"] == "PCRT") { echo("checked"); }  ?> value="PCRT">PCRT
			    <input align="left" type="radio" onClick="ShowTeam()" name="Review_Type" <?PHP if ($row_team_details["Type_Of_Review"] == "Area") { echo("checked"); } ?> value="Area">Area
			  </td>
			  <td class="normalLabel" width="12%">&nbsp;<span id="rowType_1">Meeting Date:</span></td>
			  <td class="normal">
			    &nbsp;<span id="rowType_2">
			    <input type="text" size="12" class="inputTextBox" name="MeetingDate" id="Date" value="<?PHP echo($row_team_details["Meeting_Date"]); ?>" onBlur="ValidateDateFields('MeetingDate')">
			    <a href="#" onClick="callCalendar('anchor4','MeetingDate');" name="anchor4" id="anchor4"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> 
			    </span>
			  </td>
		    </tr>
		    <tr id="rowType_3" bordercolor="#FFFFFF">
			  <td valign="top" class="normalLabel">Comments:</td>
			  <td class="normalLabel" colspan="3">
			    <textarea name="Meeting_Comments" class="inputTextBox" rows="3" cols="100" onBlur="CheckLength(this,0,500);"><?PHP echo($row_team_details["Meeting_Comments"]); ?></textarea>
			  </td>
			</tr>
		  <tr id="rowType_4" bordercolor="#FFFFFF">
			  <td colspan="4">
			    <table id="tblDocuments" width="97%" cellspacing="4">
			    <?PHP 
				
				while(!rsDocuments.EOF)
				{ 
				
				?>
				  <tr> 
					<td class="normalLabel" width="17%">Other minutes where PCR is referenced:</td>
					<td class="normal" colspan="3"> 
					  <input class="LinkedTextbox" value="<?PHP echo($row_document["DocumentLocation"]); ?>" type="text" name="Document_<?PHP echo($Repeat2__index); ?>" size="90" onDblClick="ClickLink(Document_<?PHP echo($Repeat2__index); ?>, LoadDoc)" id="Document Location" >
					  <input class="Button" type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Document_<?PHP echo($Repeat2__index); ?>')">
					</td>
				  </tr>
				  <?PHP 
						$Repeat2__index++;
						rsDocuments.MoveNext();
				  }
				   ?>
			    </table>
			  </td>
			</tr>
			<tr id="rowType_5" bordercolor="#FFFFFF"> 
			  <td class="normalLabel" colspan="4" height="35" valign="top">
				<input type="hidden" name="NumDocuments" value="<?PHP echo($Repeat2__index); ?>">
				<input type="button" class="Button" name="btnDocument" value="Add more minutes" onClick="addDocumentRow('tblDocuments','17%', 'Other minutes where PCR is referenced:', 'Document_', 'NumDocuments','90')">
			  </td>
		  </tr>		  
		  </table>
			<a style="display: none" href="#" id="LoadDoc" target="_blank">Link To File</a>
		</td>
	  </tr>
<?PHP 
	  
	  while(!rsReviewers.EOF)
	  { 
          if (Repeat1__index % 2 == 1)
		      $bgColor = "#F3F3F3";
          else
              $bgColor = "#FFFFFF";
	  }
	  
?>
	  <tr id="rowReviewerType_<?PHP echo($Repeat1__index); ?>" bordercolor="#CCCCCC" bgcolor="<?PHP echo($bgColor); ?>">
		<td >
		  <table border="1" width="100%">
		    <tr bordercolor="<?PHP echo($bgColor); ?>">
			  <td class="normalLabel" width="15%">Reviewer Type:</td>
			  <td class="searchHeader" width="20%">
				<?PHP echo($row_reviewer["Reviewer_Type"]); ?>
				<input type="hidden" name="ReviewerTypeString_<?PHP echo($Repeat1__index); ?>" value="<?PHP echo($row_reviewer["Reviewer_Type"]); ?>">
				<input type="hidden" name="DetailId_<?PHP echo($Repeat1__index); ?>" value="<?PHP echo($row_reviewer["Detail_Id"]); ?>">
			  </td>
			  <td class="normalLabel" width="8%">Required:</td>
			  <td class="normal" width="10%">
				<?PHP 
				
				// find out if we need to check the required checkbox
				var iValue = $row_reviewer["Required"];
				if ($iValue == "" || $iValue == null)
				{
					if ($row_reviewer["Requirement"] == "Optional")
						$iValue = 0;
					else
						$iValue = 1;
				}
				
				// used to determine if we show the details or not
				if ($iValue == 1)
					$sDisplay = "block";
				else
					$sDisplay = "none";
				
				
				// used to determine if we give the user write access to the details
				$iReviewer = $row_reviewer["Reviewer_Id"];
				if ($iReviewer == "")
					$iReviewer = arDefaults[Repeat1__index];
				
				
				//the logged on user is either the saved reviewer or the default reviewer for this reviewer type
				if(parseInt(iReviewer) == parseInt(Session("MM_UserId")))
					$sDisabled = "";
				else
					$sDisabled = "disabled";
				
				?>
				<input type="checkbox" name="Required_<?PHP echo($Repeat1__index); ?>" <?PHP if ($iValue == 1) { echo("checked"); } ?> onClick="ShowApprovers('Required_<?PHP echo($Repeat1__index); ?>','rowReviewer_<?PHP echo($Repeat1__index); ?>', 'hdnRequired_<?PHP echo($Repeat1__index); ?>')" value="1">Yes
			    <input type="hidden" name="hdnRequired_<?PHP echo($Repeat1__index); ?>" value="<?PHP echo($row_reviewer["Requirement"]); ?>">
			  </td>
			  <td class="normalLabel" width="13%">Reviewer Name:</td>
			  <td class="normal">
			    <input type="hidden" name="OldAReviewerId_<?PHP echo($Repeat1__index); ?>" value="<?PHP echo($row_reviewer["Reviewer_Id"]); ?>">
			    <input type="hidden" name="DefaultId_<?PHP echo($Repeat1__index); ?>" value="<?PHP echo($arDefaults[Repeat1__index]); ?>">
					<select name="ReviewerId_<?PHP echo($Repeat1__index); ?>" class="inputTextBox">
						<option value=""></option>
						<script>
							document.write(ReadActiveEmps());
						</script>
			    </select>
			  </td>
			</tr>
		    <tr id="rowReviewer_<?PHP echo($Repeat1__index); ?>" style="display: <?PHP echo($sDisplay); ?>" bordercolor="<?PHP echo($bgColor); ?>">
			  <td colspan="6">
			    <table width="100%" border="0" cellspacing="0" cellpadding="2">
				  <tr>
				    <td width="15%" class="normalLabel">Date Reviewed:</td>
					<td class="normal" width="20%">
					  <input type="text" size="12" <?PHP echo($sDisabled); ?> class="inputTextBox" name="ReviewDate_<?PHP echo($Repeat1__index); ?>" onBlur="ValidateDateFields('ReviewDate_<?PHP echo($Repeat1__index); ?>')" value="<?PHP echo($row_reviewer["Date_Reviewed"]); ?>">
					  <a href="#" onClick="callCalendar('anchor_<?PHP echo($Repeat1__index); ?>','ReviewDate_<?PHP echo($Repeat1__index); ?>');" name="anchor_<?PHP echo($Repeat1__index); ?>" id="anchor_<?PHP echo($Repeat1__index); ?>"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> 
					</td>
					<td class="normalLabel" width="8%">Status:</td>
					<td class="normal">
					  <input type="radio" <?PHP echo($sDisabled); ?> name="Approved_<?PHP echo($Repeat1__index); ?>" <?PHP if ($row_reviewer["Status"] == "Proceed") { echo("checked"); } ?> id="Approved1_<?PHP echo($Repeat1__index); ?>" value="Proceed">Proceed
					  <input type="radio" <?PHP echo($sDisabled); ?> name="Approved_<?PHP Repeat1__index ?>" <?PHP if ($row_reviewer["Status"] == "Further Info Required") { echo("checked"); }  ?> id="Approved2_<?PHP echo($Repeat1__index); ?>" value="Further Info Required">Further Info Required
					  <input type="radio" <?PHP sDisabled ?> name="Approved_<?PHP Repeat1__index ?>" <?PHP if ($row_reviewer["Status"] == "Reject") { echo("checked"); } ?> id="Approved3_<?PHP echo($Repeat1__index); ?>" value="Reject">Reject
					</td>
				  </tr>
				  <tr>
				    <td colspan="4" class="normalLabel" >Comments</td>
				  </tr>
				  <tr onClick="CheckPassword(<?PHP echo($Repeat1__index); ?>)">
				    <td colspan="4" class="normalLabel" >
					  <textarea class="inputTextBox" <?PHP echo($sDisabled); ?> name="Comment_<?PHP echo($Repeat1__index); ?>" onBlur="CheckLength(this,0,255);" cols="127" rows="2"><?PHP echo($row_reviewer["Comments"]); ?></textarea>
				    </td>
				  </tr>
				  <tr>
				    <td colspan="4" class="normalLabel" >Location of Saved Checklist</td>
				  </tr>
				  <tr> 
				    <td colspan="4" class="normal" > 
					  <input class="LinkedTextbox" <?PHP echo($sDisabled); ?> type="text" name="Checklist_<?PHP echo($Repeat1__index); ?>" size="98" maxlength="255" onDblClick="ClickLink(Checklist_<?PHP echo($Repeat1__index); ?>, LoadDoc)" value="<?PHP echo($row_reviewer["Document_Loc"]); ?>">
					  <input class="Button" type="button" <?PHP echo($sDisabled); ?> name="btnLocation_<?PHP echo($Repeat1__index); ?>" value="Checklist Template" style="width: 130" onClick="ClickLink(FileLocation_<?PHP echo($Repeat1__index); ?>, LoadDoc)">
					  <input class="Button" <?PHP echo($sDisabled); ?> type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Checklist_<?PHP echo($Repeat1__index); ?>')">
					  <input type="hidden" name="FileLocation_<?PHP echo($Repeat1__index); ?>" value="<?PHP echo($row_reviewer["Checklist_Loc"]); ?>">
					</td>
				  </tr>
			    </table>
			  </td>
		    </tr>
		  </table>
		</td>
	  </tr>
	  <?PHP 
	  
	  
  	  $Repeat1__index++;
	  rsReviewers.MoveNext();
	  
	  
	  }
	   ?>
	  <tr id="rowRecommendation" bordercolor="#CCCCCC">
		<td >
		  <table border="1" width="100%">
		    <tr bordercolor="#FFFFFF">
			  <td colspan="5" class="searchHeader">Recommendation</td>
			</tr>
		    <tr bordercolor="#FFFFFF">
			  <td class="normalLabel">Action:</td>
			  <td class="normal" colspan="4">
			    <input type="radio" name="ActionTaken" <?PHP if ($row_team_details["Action"] == "Proceed with change") { echo("checked"); } ?> value="Proceed with change">Proceed with extension
			    <input type="radio" name="ActionTaken" <?PHP if ($row_team_details["Action"] == "Further information required") { echo("checked"); } ?> value="Further information required">Further information required
			    <input type="radio" name="ActionTaken" <?PHP if ($row_team_details["Action"] == "Change rejected") { echo("checked"); } ?> value="Change rejected">Change rejected
			  </td>
			</tr>
		    <tr bordercolor="#FFFFFF">
			  <td valign="top" id="cellComment" class="normalLabel">Comments:</td>
			  <td class="normal" colspan="4">
			    <textarea name="Action_Comments" class="inputTextBox" rows="3" cols="100" onBlur="CheckLength(this,0,500)"><?PHP echo($row_team_details["Comments"]); ?></textarea>
			  </td>
			</tr>
		    <tr bordercolor="#FFFFFF">
			  <td class="normalLabel">By:</td>
			  <td class="normal">
			    <input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',3)"  <?PHP if ($row_team_details["Due_By"] == "3 months") { echo("checked"); }  ?> value="3 months">3 months
			    <input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',6)"  <?PHP if ($row_team_details["Due_By"] == "6 months") { echo("checked"); } ?> value="6 months">6 months
			    <input type="radio" name="Change_By" onClick="RecalcDate('ActionDate','m',12)" <?PHP if ($row_team_details["Due_By"] == "12 months") { echo("checked"); } ?> value="12 months">12 months
			  </td>
			  <td class="normalLabel" colspan="3">Date:&nbsp;
			    <input type="text" size="12" class="inputTextBox" name="ActionDate" id="Date" value="<?PHP echo($row_team_details["Due_Date"]); ?>" onBlur="ValidateDateFields('ActionDate')">
			    <a href="#" onClick="callCalendar('anchor3','ActionDate');" name="anchor3" id="anchor3"><img src="../images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> 
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
      <tr> 
        <td class="normal">
		  <input type="file" name="BrowseDialog" style="display: none">
          <input class="Button" type="submit" name="Save" value="Save">
          <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
          <input class="Button" type="reset" name="Reset" value="Reset">
        </td>
      </tr>
    </table>
		<script language="JavaScript" src="../_Scriptlibrary/rs.htm"></script>
		<script language="JavaScript">RSEnableRemoteScripting("../_Scriptlibrary");</script>
		<input class="inputTextBox" type="hidden" name="MM_update" value="true">
		<input type="hidden" name="NumReviewers" value="<?PHP echo($Repeat1__index); ?>">
		<input class="inputTextBox" type="hidden" name="MM_recordId" value="<?PHP echo($row_pcr_details["PCR_Id"]);  ?>">
		<input type="hidden" name="Process_Step" value="">
		<input type="hidden" name="ProcessStep_Status" value="">
		<input type="hidden" name="Overall_Status" value="<?PHP echo($row_pcr_details["Overall_Status"]); ?>">
  </form>

</body>
</html>
