<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');

$ID = "3626";

$PCR_DETAILS = Get_PCR_Details_On_ID($_POST, $_GET);

?>

<script language="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";  //used for the remote scripting when getting the managed by site and department

//convert the dates into a string format before saving them to the database
function ChangeDates()
{  
	try
	{
	    document.frmData.Modified_Date.value = ConvertDate('Modified_Date', '/', 'AUS');
	    document.frmData.PCRDate.value = ConvertDate('PCRDate', '/', 'AUS');
	    document.frmData.AuthorityDate_1.value = ConvertDate('AuthorityDate_1', '/', 'AUS');
	    document.frmData.AuthorityDate_2.value = ConvertDate('AuthorityDate_2', '/', 'AUS');
	    document.frmData.Meeting_Date.value = ConvertDate('Meeting_Date', '/', 'AUS');
	}
	catch(e)
	{
	
	}
}


function CheckCategory()
{
	var bChecked = false;
	for(i=1;i<=6;i++)
	{
		var objCheck = document.getElementById("ActionCategory_" + i);
		if(objCheck.checked == true){bChecked = true; break;}
	}
	
	if(bChecked == true)
		document.frmData.valCategory.value = "Don't Validate";
	else
		document.frmData.valCategory.value = "";
}


function ShowDescription(SpanName, Message)
{
	var objSpan = document.getElementById(SpanName);
	objSpan.title = Message;
	return objSpan;
}


//stuff we do when the form loads
function StartUp()
{
	//these fields only get displayed if the logged on user is from the Pigment Plant(Kwinana site)
	if("PI" != 'PI'){
		//show the required reason for change checkboxes
		ShowReason(true);//('block');
	}else
		ShowReason(false);('none');
	
	
	CheckTheBoxes("", 'ReasonFor_', 'NumChecks_1');//Nature of Injury
	
	SelectDropDown('OriginatorId', "10813");//Originator
	SelectDropDown('AuthorityId_1', "");//Originator
	SelectDropDown('AuthorityId_2', "");//Originator
	SelectDropDown('Evaluation_Id', "");//Originator

}

//fields that get displayed if the site is the Pigment Plant(Kwinana site)
function ShowReason(show)
{
	var i=0;
	var cnt=5;
	for(i=1;i<cnt;i++){
		if(i==3) show=!show;
		try{element("rowCO_" + i).style.display = (show?'block':'none');}catch(e){}
	}
}


function GetEmpDetails(){
	var iEmpId = document.frmData.OriginatorId.value;
	var sFields = "EmployeeNo|SiteId|SiteDesc|DepartmentId|DepartmentDesc|SectionId|SectionDesc|PositionId|PositionDesc";
	var arFields = sFields.split("|");
	var x = arFields.length;
		
	if(iEmpId != ""){
		var sWhere = "SELECT Employee_Number, Site_Id,  Site_Description, Department_Id, Department_Description, Section_Id, Section_Description, Position, Position_Name FROM View_Employee_Details WHERE Employee_Number = " + iEmpId;
		var sColumns = "Employee_Number|Site_Id|Site_Description|Department_Id|Department_Description|Section_Id|Section_Description|Position|Position_Name";
		
		var DataString = RSExecute(serverURL, "GetSingleRowData", sWhere, sColumns);
		arData = DataString.return_value.split("|");
		
		for(var i=0; i<x;i++){
			var objField = document.getElementById(arFields[i]);
			if(arData[i] == null || arData[i] == 'null'){
				objField.value = "";
			}else{
				objField.value = arData[i];
			}
		}
		//alert(document.frmData.SiteId.value+":"+arData[1]);
		var sWhere = "Select Area_Id, Area_Description FROM tblPCR_Area WHERE Site_Id = " + document.frmData.SiteId.value + " ORDER BY Area_Description ASC";
		var sColumn1 = "Area_Id";
		var sColumn2 = "Area_Description";
		ReturnData(sWhere, sColumn1, sColumn2, 'AreaId');
	}else{
		for(var i=0; i<x;i++){
			var objField = document.getElementById(arFields[i]);
			objField.value = "";
		}
	}
	
	//show or hide the required rows if the chosen site is the Pigment Plant (Kwinana)
	if(document.frmData.SiteId.value != 3){
		ShowReason(true);//('block');
	}else{
		ShowReason(false);//('none');
	}
}



function validatePCRDetails_OLD(f)
{
	if(document.frmData.SiteId.value == 3){
		MM_validateForm('ImpactId','','R','PCR_Type','','R','OriginatorId','','R','PCRDate','','R','PCR_Description','','R');
	}else{
		MM_validateForm('PCR_Type','','R','ChangeTypeId','','R','OriginatorId','','R','PCRDate','','R','PCR_Description','','R');
	}
}



function validatePCRDetails(f)
{
	var returnValue = true;
	returnValue = checkLengthOf("PCR_Title",255,"The 'Title' field must have no more than #MAX# characters.\nThe text will now be truncated.");
	if(returnValue) returnValue = checkLengthOf("PCR_Description",1000,"The 'Detailed Description' field must have no more than #MAX# characters.\nThe text will now be truncated.");
	
	if(returnValue)
	{
		if(document.frmData.SiteId.value == 3)
			MM_validateForm('ImpactId','','R','PCR_Type','','R','OriginatorId','','R','PCRDate','','R','PCR_Description','','R');
		else
			MM_validateForm('PCR_Type','','R','ChangeTypeId','','R','OriginatorId','','R','PCRDate','','R','PCR_Description','','R');
	}
	else
		document.MM_returnValue = returnValue;
}
</script>

<body>
<form action="index.php?m=<?php echo $m;?>&p=post&a=edit_pcr&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">
<b>PCR Details Edit</b><br>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <td width="25%"><span class="subheader">PCR Number </span></td>
    <td width="25%"><span class="normal"><?PHP echo($PCR_DETAILS["PCR_DISPLAYID"]); ?></span></td>
    <td width="25%"><span class="normalLabel hide">Criticality:</span></td>
    <td width="25%"><span class="normal hide"><?PHP echo get_impact_level_dd( $PCR_DETAILS["CRITICALITY_ID"] ); ?></span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Type of PCR:</span></td>
    <td><span class="normal"><?PHP echo get_type_of_pcr_radio( $PCR_DETAILS["PCR_TYPE"] ); ?></span></td>
    <td><span class="normalLabel">TCP:</span></td>
    <td><span class="normal"><?PHP echo get_tcp_radio( $PCR_DETAILS["PCR_TYPE"] ); ?></span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Originator:</span></td>
    <td><span class="normal">
      <select name="OriginatorId" id="Originator" class="inputTextBox" onChange="GetEmpDetails()">
        <option value=""></option>
        <script>
				  document.write(ReadActiveEmps());
				</script>
      </select>
    </span></td>
    <td><span class="normalLabel">Employee No:</span></td>
    <td><span class="normal">
      <input type="text" name="EmployeeNo" value="<?PHP echo($PCR_DETAILS["EMPLOYEE_NO"]); ?>" class="inputTextBox" size="10" id="Employee No">
    </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Site:</span></td>
    <td><span class="normal">
      <input type="hidden" name="SiteId" value="<?PHP echo($PCR_DETAILS["SITE_ID"]); ?>">
      <input type="text" size="30" class="inputTextBox" name="SiteDesc" value="Kwinana">
    </span></td>
    <td><span class="normalLabel">Department:</span></td>
    <td><span class="normal">
      <input type="hidden" name="DepartmentId" value="<?PHP echo($PCR_DETAILS["DEPARTMENT_ID"]); ?>">
      <input type="text" size="30" class="inputTextBox" name="DepartmentDesc" value="PI-OPS AREA 2">
    </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Section:</span></td>
    <td><span class="normal">
      <input type="hidden" name="SectionId" value="<?PHP echo($PCR_DETAILS["SECTION_ID"]); ?>">
      <input type="text" size="30" class="inputTextBox" name="SectionDesc" value="">
    </span></td>
    <td><span class="normalLabel">Position:</span></td>
    <td><span class="normal">
      <input type="hidden" name="PositionId" value="<?PHP echo($PCR_DETAILS["POSITION_ID"]); ?>">
      <input type="text" size="30" class="inputTextBox" name="PositionDesc" value="Engineer - Process Area 2">
    </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Date:</span></td>
    <td colspan="3"><span class="normal">
      <input type="text" size="12" class="inputTextBox" name="PCRDate" value="13/10/2005" id="Date" onBlur="ValidateDateFields('PCRDate')">
      <a href="#" onClick="callCalendar('anchor3','PCRDate');" name="anchor3" id="anchor3"><img src="images/calendar2.jpg" width="30" height="31" align="absmiddle" border="0"></a> (dd/mm/yyyy)</span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Type of Change:</span></td>
    <td colspan="3"><span class="normal"><?PHP echo get_type_of_change_dd( $PCR_DETAILS["CHANGETYPE_ID"] ); ?></span></td>
  </tr>


  <tr>
    <td><span class="normalLabel">Title:</span></td>
    <td colspan="3"><span class="normal">
      <input type="text" class="inputTextBox" id="Title" name="PCR_Title" style="width:80%" value="<?PHP echo($PCR_DETAILS["TITLE"]); ?>" />
    </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Detailed Description:<br>
    (What and Why)</span></td>
    <td colspan="3"><span class="normal">
      <textarea class="inputTextBox" rows="3" id="Detailed Description" cols="95" style="width:80%" name="PCR_Description"><?PHP echo($PCR_DETAILS["DESCRIPTION"]); ?></textarea>
    </span></td>
  </tr>
  <tr>
    <td><span class="normalLabel">Area:</span></td>
    <td><span class="normal">
      <select name="AreaId" id="Area" class="inputTextBox">
        <option value=""></option>
        <option value="1" >Area 1</option>
        <option value="2" selected>Area 2</option>
        <option value="3" >Other</option>
      </select>
    </span></td>
    <td><span class="normalLabel">Equipment No:</span></td>
    <td><span class="normal">
      <input type="text" name="EquipmentId" id="Equipment No" size="10" class="inputTextBox" value="<?PHP echo($PCR_DETAILS["EQUIPMENT_ID"]); ?>" />
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3">
	
	<!--
	<input class="inputTextBox" type="hidden" name="MM_update" value="true">
    <input class="inputTextBox" type="hidden" name="MM_recordId" value="3671">
	-->
	
    <input type="hidden" name="Modified_Date" value="<?PHP echo($PCR_DETAILS["MODIFIED_DATE"]); ?>">
    <input type="hidden" name="Process_Step" value="<?PHP echo($PCR_DETAILS["PROCESS_STEP"]); ?>">
	<input type="hidden" name="ProcessStep_Status" value="<?PHP echo($PCR_DETAILS["PROCESSSTEP_STATUS"]); ?>">
	<input type="hidden" name="Overall_Status" value="<?PHP echo($PCR_DETAILS["OVERALL_STATUS"]); ?>">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><span class="normal">
      <input class="Button" type="submit" name="Save" value="Save" onClick="try{document.frmData.elements('BrowseDialog').disabled=true}catch(e){}">
      <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
      <input class="Button" type="reset" name="Reset" value="Reset">
    </span></td>
  </tr>
</table>
<?php include("includes/authority_to_proceed_box.php"); ?>
</form>
