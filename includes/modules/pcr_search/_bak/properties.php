<?php

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit PCR Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New PCR Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'Default PCR Actions',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search PCR Actions',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'pcr_actions':
			$ret = array_merge(
				array(
					'table'=>'TBLPCR_ACTION', 
					'id'=>'ACTION_ID',
					'view'=>'VIEW_PCR_DEFAULT_ACTIONS',
					'filter'=>' 1=2 '
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLPCR_ACTION', 
					'id'=>'ACTION_ID',
					'view'=>'VIEW_PCR_DEFAULT_ACTIONS',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();

	switch($page){
		case 'pcr_actions':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'params'=>' onchange=\"set_default_actions(this);\" ', 'col'=>1, 'col_span'=>1),
				'DEFAULT_ACTIONS' => array('label'=>'Default Actions', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
						'ACTION_DESCRIPTION' => array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_draw_input_field'), 
						'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper'),
						'DAYS_AFTER_PREV_STAGE' => array('label'=>'Days After Previous Stage', 'value'=>NULL, 'func'=>'html_draw_number_field')
					),
					'sql'=>"select action_description, managed_by_id, days_after_prev_stage, 'delete:javascript:remove_sequence_row(this, ::)' from view_pcr_default_actions where site_id = ".$_SESSION['user_details']['site_id']
				)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}
if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>