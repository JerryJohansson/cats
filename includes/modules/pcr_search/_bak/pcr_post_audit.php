<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_html.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_pcr_sql.php');
?>




<script language="JavaScript">
var serverURL = "../RemoteScripts/GetValues.asp";//used for the remote scripting when getting the managed by site and department

//stuff we do when the form loads
function StartUp(){
	// nothing yet ;)
	SelectDropDown('Lead_Auditor','');
}
//convert the dates into a string format before saving them to the database
function ChangeDates(){	
	document.frmData.Audit_Date.value = ConvertDate('Audit_Date', '/', 'AUS');
}


//enable all the form fields so we can save their values
function EnableFields(){
	var x = document.frmData.elements.length;
	for(var i=0;i<x;i++){
		if(document.frmData.elements[i].name != "BrowseDialog"){
			document.frmData.elements[i].disabled = false;
		}
	}
}

//stuff we do when we submit the form
function SubmitForm(){
	ChangeDates();
}
var aOptions = [];
var aValues = [];

function GetDocumentTypes(sMatch){
	var i=0,s="";
	for(i=0;i<aOptions.length;i++){
		sSelected = (aOptions[i]==sMatch) ? " selected " : "" ;
		s += "<option value=\"" + aOptions[i] + "\" " + sSelected + ">" + aOptions[i] + "</option>";
	}
	return s;
}
function GetFileLocation(item,txt){
	var i;
	for(i=0;i<aOptions.length;i++){
		if(aOptions[i]==item.options[item.selectedIndex].value){
			if(checkFileLocation(txt.value)){
				txt.value = aValues[i];
				break;
			}
		}
	}
}
function checkFileLocation(s){
	var i,ret=false;
	for(i=0;i<aValues.length;i++){
		if(s==aValues[i]) {
			ret = true;
			break;
		}
	}
	return ret;
}
</script>


<body>

<form action="index.php?m=<?php echo $m;?>&p=post&a=post_audit&id=<?PHP echo $id; ?>" method="post" name="frmData" onSubmit="">


<b>PCR Post Audit</b> 
<?php include("includes/pcr_details_general.php"); ?>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr id="rowType_4" >
    <td colspan="4"><table width="97%" border="1" cellpadding="5" cellspacing="0" id="tblDocuments">
      <tr>
        <td colspan="2" class="normalLabel">Supporting document / Comments:</td>
        </tr>
      <tr>
        <td><textarea name="Document_0" class="inputTextBox" rows="3" cols="100" onBlur="CheckLength(this,0,500);"></textarea>        </td>
        <td><input class="Button" type="button" name="btnForm" value="Form" onClick="LoadDocument(Document_0, LoadDoc)" id="Document Location" >
              <input class="Button" type="button" name="btnLocation" value="Browse..." onClick="Change_Location('Document_0')">        </td>
      </tr>
    </table>
      <a style="display: none" href="#" id="LoadDoc" target="_blank">Link To File</a> </td>
  </tr>
  <tr id="rowType_6" >
    <td class="normalLabel" colspan="4" height="35" valign="top"><input type="button" class="Button" name="btnDocument" value="Add Another Document" onClick="addDocumentTypeRow('tblDocuments','17%', 'Type of document:|Supporting document / Comments:', 'Doc_Type_|Document_', 'NumDocuments','3|100')">
    </td>
  </tr>
  <tr id="rowType_3" >
    <td colspan="4"><table width="100%" border="1" cellpadding="5" cellspacing="0">
      <tr valign="top">
        <td valign="top" class="normalLabel">Audit Comments:</td>
        <td colspan="3" valign="top" class="normalLabel"><textarea name="Audit_Comments" class="inputTextBox" rows="3" cols="60" onBlur="CheckLength(this,0,500);"></textarea>        </td>
        </tr>
      <tr>
        <td class="normalLabel">Audit Date:</td>
        <td class="normal"><input type="text" size="12" class="inputTextBox" name="Audit_Date" id="Date" value="">
            <a href="#" onClick="callCalendar('anchor4','Audit_Date');" name="anchor4" id="anchor4"><img src="images/calendar2.jpg" width="30" height="31" align="top" border="0"></a> </td>
        <td class="normalLabel">Audit Completed By:</td>
        <td class="normal"><input type="hidden" name="Audit_Completed_By" value="">
              <select name="Audit_Completed_By_Id" id="Audit Completed By" class="inputTextBox">
                <option value=""></option>
                <script>
								  document.write(ReadActiveEmps());
								</script>
              </select>        </td>
      </tr>
    </table></td>
  </tr>
</table>
<br>
<br>
<br>
<table width="100%" border="1" cellpadding="5" cellspacing="0">
  <tr >
    <td><input class="inputTextBox" type="hidden" name="MM_update" value="true">
      <input type="hidden" name="NumDocuments" value="0">
      <input class="inputTextBox" type="hidden" name="MM_recordId" value="3671">
      <input type="hidden" name="Process_Step" value="Post Audit">
      <input type="hidden" name="ProcessStep_Status" value="complete - post audited">
      <input type="hidden" name="Overall_Status" value="Open"></td>
  </tr>
  <tr>
    <td class="normal"><input type="file" name="BrowseDialog" style="display: none">
        <input class="Button" type="submit" name="Save" value="Save">
        <input class="Button" type="button" name="Cancel" value="Cancel" onClick="MM_goToURL('self','PCRFilterForm.asp?')">
        <input class="Button" type="reset" name="Reset" value="Reset">    </td>
  </tr>
</table>
</form>