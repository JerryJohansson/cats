<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$iTabs=0;
?>
<?php  

if(!cats_user_is_reader(true) && !cats_user_is_editor(true) && !cats_user_is_editor(false) && !cats_user_is_super_administrator() && !cats_user_is_pcr_administrator() && !cats_user_is_incident_privileged() && !cats_user_is_incident_analysis()){
	$_SESSION['messageStack']->add("You do not have access to the Search Process Change Request ");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

?>

<link rel="stylesheet" href="<?php echo WS_STYLE_PATH;?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

window.onload = init;
</script>
</head>
<body><form action="index.php?m=<?php echo $m;?>&p=results" target="<?php echo $results_iframe_target;?>" method="post">	
<table width="100%">
<tr valign="top">
	<td width="100%">		

<!-- BEGIN:: Search Table -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" class="search" onClick="preSeachForm(); toggle_search()"><?php 
echo $GENERAL['title'];
if(isset($show_search_results) && $show_search_results){
	echo ' (Click here to show Search Parameters)';
}
?></a></caption>
<tr>
	<td>
		<input type="button" class="button" name="cats::new" value="New PCR" onClick="_m.newModule()">
	</td>
</tr>
<tr>
	<td>
		<!-- <div  id="search_form" style="display:<?php  echo isset($search_display)?$search_display:'block';?>"> -->
			<div  id="search_form" ><!--  after discussion  -->
<?php
// Create the search form
$ed = new Editor("edit","admin");
echo($ed->buildForm($FIELDS));
?>
		<fieldset class="tbar" style="text-align:right;  margin-right: 5em;">
		<input type="reset" class="reset" value="Reset" /><input type="submit" class="submit" name="cats::search" value="Search" onClick="preSeachForm(); return _m.search(this)">
		
		<input type="button" class="submit" name="cats::report" value="Export To Excel" onClick="_m.report(this)"><?php echo html_draw_hidden_field('cats::report_fields',''); ?>
		<input type="button" class="button" name="Cancel" value="Cancel" onClick="_m.cancel();">
		</fieldset>		
		</div>
	</td>
</tr>
</table>

<!-- END:: Search Table -->
		
	</td>
</tr>
</table>
</form>
<?php
include (CATS_INCLUDE_PATH . 'results_iframe.inc.php');
?>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">
 
	function preSeachForm()
	{
		if($("#DUE_DATE_FROM_d").val() != "")
			setHiddenDate("DUE_DATE_FROM");
		if($("#DUE_DATE_TO_d").val() != "")
			setHiddenDate("DUE_DATE_TO");
		if($("#PCR_DATE_FROM_d").val() != "")
			setHiddenDate("PCR_DATE_FROM");
		if($("#PCR_DATE_TO_d").val() != "")
			setHiddenDate("PCR_DATE_TO");
			
	
	}

</script>