<?php


function getPropertiesTables(){
	return array('edit'=> array('name'=>'TBLINJURY_CASE_NOTES', 'id'=>'ID'),
		'search'=> array('name'=>'TABLE_NAME','id'=>'ID'),
		'edit_view'=> array('name'=>'TABLE_NAME','id'=>'ID')
	);
}

function getPropertiesFields(){
	return array(
		'DATE_OF_BIRTH' => array('label'=>'Date Of Birth', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'DATE_CASE_NOTE' => array('label'=>'Date Case Note', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'ISSUED_BY' => array('label'=>'Issued By', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FITNESS_STATUS' => array('label'=>'Fitness Status', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'CASE_NOTES' => array('label'=>'Case Notes', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
	);
}


$TABLES=getPropertiesTables();
$FIELDS=getPropertiesFields();
?>