<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Work Hours',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Work Hours Record',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Work Hours',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLWORK_HOURS', 
					'id'=>'WORK_HOURS_ID',
					'view'=>'VIEW_WORK_HOURS'
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLWORK_HOURS', 
					'id'=>'WORK_HOURS_ID',
					'view'=>'VIEW_WORK_HOURS',
					'filter'=>' 1=2 '
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLWORK_HOURS', 
					'id'=>'WORK_HOURS_ID',
					'view'=>'VIEW_WORK_HOURS',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
					'YEAR_MONTH' => array('label'=>'Year Month', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'EMPLOYEE_WORK_HOURS' => array('label'=>'Employee Work Hours', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2), 
					'CONTRACTOR_WORK_HOURS' => array('label'=>'Contractor Work Hours', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search': 
		case 'results': //explode("|","Site_Id|none,none,NULL|Year_Month_from|',none,NULL,DATE_FROM|Year_Month_to|',none,NULL,DATE_TO");
			$ret = array(
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'YEAR_MONTH_FROM' => array('label'=>'Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'YEAR_MONTH_TO' => array('label'=>'Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
					'YEAR_MONTH' => array('label'=>'Year Month', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
					'EMPLOYEE_WORK_HOURS' => array('label'=>'Employee Work Hours', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2), 
					'CONTRACTOR_WORK_HOURS' => array('label'=>'Contractor Work Hours', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}
if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>
