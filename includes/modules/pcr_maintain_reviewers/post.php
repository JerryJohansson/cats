<?php

/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id = isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;
switch($action){
	case "add": // Add record
		$editor = new Editor($m, $p, $a);
		$ret = $editor->insert();
		if($ret) $id=$db->Insert_ID("tblpcr_reviewer_detail_seq");
		
		// Update Default PCR Area Reviewers
		if($ret){
			$ret = update_default_area_reviewers($id);
		}
		
		$return_js_function = "top._edit_success('pcr_maintain_reviewers','".$m."','edit','edit','index.php?m=pcr_maintain_reviewers&p=edit&id=".$id."')";
		
		break;
	case "edit": // Update record
		//$db->debug=true;
		$editor = new Editor($m, $p, $a);
		$ret = ($editor->update($id) > 0);
		
		// Update Default PCR Area Reviewers
		if($ret){
			$ret = update_default_area_reviewers($id);
		}
		
		break;
	case "delete":
		$editor = new Editor($m, $p, $a);
		if($ret = $editor->delete($id)){
			$sql_del="DELETE FROM tblpcr_reviewer_name WHERE detail_id=".$id;
			if($ret = $db->Execute($sql_del)){
				$_SESSION['messageStack']->add("Default Area Reviewers deleted successfully","success");
			}else{
				$_SESSION['messageStack']->add("An error occured while deleting Default Area Reviewers<br>Error: ".$db->ErrorMsg());
				return false;
			}
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Records deleted successfully.","success");
					}else{
						$_SESSION['messageStack']->add("There was an error while deleting the records.");
					}
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");	
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");	
		$ret = false;
		break;
}
if($db->debug==true) exit;
if($ret) {
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}

/*
+------------------------------------------------------------------------
|  Update Default Reviewer Details
+------------------------------------------------------------------------
*/
function update_default_area_reviewers($id) {
	global $db, $TABLES;
	$ret = true;
	$table = 'tblpcr_reviewer_name';
	
	//$db->debug=true;
		
	if(isset($_POST['AREA_ID']) && is_array($_POST['AREA_ID'])){
		$detail_id = $id;
		$area_id = $_POST['AREA_ID'];
		$reviewer_id = $_POST['REVIEWER_ID'];
				
		if(is_array($area_id)){ // lets make sure we have an array
			$insert_fields = "detail_id,area_id,reviewer_id";
			// delete the records and re-insert with new values
			$ret = db_query("DELETE FROM $table WHERE detail_id=$id");
			// make the array for binding to our compiled insert statement
			for($i=0; $i < sizeof($area_id); $i++){
				if(!empty($area_id[$i])){
					// build the insert array
					$add[] = array( 
						$id, 
						$area_id[$i], 
						$reviewer_id[$i]
					); // array must be in this format to bind to our compiled statement
				}
			} //END:: loop
			// do the bulk insert on the main table using the id as the delete criteria
			$rows = 0;
			if($ret = db_bulk_insert($table,$insert_fields,$add, $db->debug)){
				$rows = $db->Affected_Rows();
				$_SESSION['messageStack']->add("Default Area Reviewer Details updated successfully.","success");
			}else{
				// let me know how you got here if it ever happens
				$_SESSION['messageStack']->add("Problems updating Default Area Reviewer Details.<br>Error: ".$db->ErrorMsg());
			}
		}
	}
	return $ret;
}


// house cleaning to be done
$db=db_close();
?>