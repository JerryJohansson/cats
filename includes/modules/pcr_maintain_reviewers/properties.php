<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Reviewer Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Reviewer',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'View Reviewer Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search PCR Reviewers',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables(){
	$ret = array(
		'table'=> 'TBLPCR_REVIEWER_DETAIL',
		'id'=> 'DETAIL_ID',
		'view'=> 'VIEW_PCR_REVIEWER_DETAIL'
	);
	return $ret;
}

function getPropertiesFields($page='search'){
	switch($page){
		case 'new':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2), 
				'REVIEW_ID' => array('label'=>'Review Type', 'value'=>NULL, 'func'=>'html_form_draw_review_type_dd', 'col'=>1, 'col_span'=>2), 
				'REVIEWER_TYPE' => array('label'=>'Reviewer Description', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'REQUIREMENT' => array('label'=>'Requirement Status', 'value'=>'', 'func'=>'html_draw_pcr_reviewer_requirement_radioset', 'col'=>1, 'col_span'=>2), 
				'CHECKLIST_LOC' => array('label'=>'Checklist Location', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
				'FOLDER_LOC' => array('label'=>'Folder Location', 'value'=>'', 'func'=>'html_get_folder_location_field', 'col'=>1, 'col_span'=>2),
				//'default_reviewers' => array('label'=>'Default Reviewers', 'value'=>'', 'func'=>'html_draw_area_reviewers_sequence', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'edit':
			$ret = array(
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'REVIEW_ID' => array('label'=>'Review Type', 'value'=>NULL, 'func'=>'html_form_draw_review_type_dd', 'col'=>1, 'col_span'=>2), 
				'REVIEWER_TYPE' => array('label'=>'Reviewer Description', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'REQUIREMENT' => array('label'=>'Requirement Status', 'value'=>'', 'func'=>'html_draw_pcr_reviewer_requirement_radioset', 'col'=>1, 'col_span'=>2), 
				'CHECKLIST_LOC' => array('label'=>'Checklist Location', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
				'FOLDER_LOC' => array('label'=>'Folder Location', 'value'=>'', 'func'=>'html_get_folder_location_field', 'col'=>1, 'col_span'=>2),
				'default_reviewers' => array('label'=>'Default Reviewers', 'value'=>'', 'func'=>'html_draw_area_reviewers_sequence', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'view':
			$ret = array(
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'REVIEW_TYPE' => array('label'=>'Review Type', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'REVIEWER_TYPE' => array('label'=>'Reviewer Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'REQUIREMENT' => array('label'=>'Requirement Status', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'CHECKLIST_LOC' => array('label'=>'Checklist Location', 'value'=>'', 'func'=>'html_get_file_location_value', 'col'=>1, 'col_span'=>2), 
				'FOLDER_LOC' => array('label'=>'Folder Location', 'value'=>'', 'func'=>'html_get_folder_location_value', 'col'=>1, 'col_span'=>2),
				'default_reviewers' => array('label'=>'Default Reviewers', 'value'=>'', 'func'=>'html_display_area_reviewers_sequence', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'search': case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2), 
				'REVIEW_ID' => array('label'=>'Review Type', 'value'=>NULL, 'func'=>'html_form_draw_review_type_search_dd', 'col'=>1, 'col_span'=>2), 
				'REVIEWER_TYPE' => array('label'=>'Reviewer Description', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_reviewer_titles_search_dd', 'col'=>1, 'col_span'=>2), 
				'REQUIREMENT' => array('label'=>'Requirement', 'delim'=>"'", 'value'=>'', 'func'=>'html_draw_pcr_reviewer_requirement_dd', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>