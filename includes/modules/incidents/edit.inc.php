<!-- BEGIN:: Incident Analysis -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo">Incident Analysis</a></caption>
<tr>
	<td>
		
		<div class="tab_off" id="tab_all_incidents" theclick="tab_toggle(this,'edit_checkboxes','Reported_To','Systemic_Cause','Behaviour_Cause');" ><a name="anc_all_incidents">All Incidents Details</a></div>
		
		<div class="div_off" id="all_incidents" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<tr>
				<td class="label">
					<label>LIFE</label>
				</td>
				<td>
					<?php echo (html_draw_incident_life_checkboxset('LIFE', $RECORD['LIFE'], $RECORD['INCIDENT_NUMBER'])); ?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<label>Severity</label>
				</td>
				<td>
					<?php echo (html_get_incident_severity_radioset('SEVERITY_RATING',$RECORD['SEVERITY_RATING']));?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<label>Externally Reported To</label>
				</td>
				<td><div id="divReported_To">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Systemic</label>
				</td>
				<td><div id="divSystemic_Cause">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Behaviour</label>
				</td>
				<td><div id="divBehaviour_Cause">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		
		<div class="tab_off" id="tab_injuries" onclick="tab_toggle(this,'edit_checkboxes','Injury_Classification','Injury_Location','Injury_Mechanism','Injury_Type','Injury_Agency');" ><a name="anc_injury">Injury Details</a></div>
		
		<div class="div_off" id="injuries" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<tr>
				<td class="label">
					<label>Injury Classification</label>
				</td>
				<td><div id="divInjury_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Location of Injury</label>
				</td>
				<td><div id="divInjury_Location">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Mechanism of Injury</label>
				</td>
				<td><div id="divInjury_Mechanism">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Injury Type</label>
				</td>
				<td><div id="divInjury_Type">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Agency of Injury</label>
				</td>
				<td><div id="divInjury_Agency">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		
		<div class="tab_off" id="tab_environment" onclick="tab_toggle(this,'edit_checkboxes','Environment_Classification','Environment_ImpactTo','Environment_ImpactBy');" ><a name="anc_environment">Environment Details</a></div>
			
		<div class="div_off" id="environment" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<tr>
				<td class="label">
					<label>Environment Classification</label>
				</td>
				<td><div id="divEnvironment_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact To (Environment)</label>
				</td>
				<td><div id="divEnvironment_ImpactTo">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact By (Agent)</label>
				</td>
				<td><div id="divEnvironment_ImpactBy">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		
		<div class="tab_off" id="tab_community" onclick="tab_toggle(this,'edit_checkboxes','Community_Classification','Contact_Source');" ><a name="anc_community">Community Details</a></div>
		
		<div class="div_off" id="community" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<tr>
				<td class="label">
					<label>Community Classification</label>
				</td>
				<td><div id="divCommunity_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Contact Source</label>
				</td>
				<td><div id="divContact_Source">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Substantiated?</label>
				</td>
				<td>
					<?php echo (html_get_incident_substantiated_radioset('SUBSTANTIATED',$RECORD['SUBSTANTIATED']));?>
				</td>
			</tr>
			</table>
		</div>
		
		<div class="tab_off" id="tab_quality" onclick="tab_toggle(this,'edit_checkboxes','Quality_Classification');" ><a name="anc_quality">Quality Details</a></div>
		
		<div class="div_off" id="quality" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<tr>
				<td class="label">
					<label>Quality Classification</label>
				</td>
				<td><div id="divQuality_Classification">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		
		<div class="tab_off" id="tab_economic" onclick="tab_toggle(this,'edit_checkboxes','Economic');" ><a name="anc_economic">Economic Details</a></div>
		
		<div class="div_off" id="economic" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<tr>
				<td class="label">
					<label>Economic Classification</label>
				</td>
				<td><div id="divEconomic">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
	</td>
</tr>
<tr>
	<td>
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="anc_quality">Estimated Cost</a></caption>
		<tr>
			<td>
				<?php echo (html_draw_estimated_cost_radioset('ESTIMATED_COST', $RECORD['ESTIMATED_COST']));?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Incident Analysis -->