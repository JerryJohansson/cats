<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');

$severityRating = "";

//$db->debug=true;
if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;

	$reqString = "";
	foreach ($_POST as $key => $value)
    $reqString .= $key.'='.$value.'\n';
	


switch($action){
	case "add": // Add record

		//$db->debug=true;
		
		if($_POST['SITE_ID'] == "")
		{
			$ret = false;
			$_SESSION['messageStack']->add("Update failed. Site must not be empty. Please select a Site.");
			break;
		}		

		// check overlapping Display ID
		$sql = "SELECT COUNT(*) FROM tblincident_details WHERE incdisplyid = '".$_POST['INCDISPLYID']."'";
		if (db_get_one($sql) == 0) {
			// Do the Insert
			$editor = new Editor($m, $p, $a);
			$ret = $editor->insert();

			$id=$db->Insert_ID("incident_details_seq");
			echo("<br><b>id=".$id."</b><br>");

			// Now Update the Timestamp Fields
			if($ret == true){
				//print_r($_POST);
				update_consequence_ranking("TBLINCIDENT_DETAILS", $id, $_POST);
				$ret = update_timestamps("tblincident_details", $id);
				update_audit_new("tblincident_details_audit", $id);				
			}

			} else {
			$ret = false;
			$_SESSION['messageStack']->add("An Incident with Incident Number ".$_POST['INCDISPLYID']." already exists.");
		}

/*		if(empty($_POST['INCDISPLYID']) && $id > 0){
			// update INCDISPLYID if it wasn't specified
			$sql = "update tblincident_details set INCDISPLYID = '{$id}/".date('Y')."' where INCIDENT_NUMBER = $id ";
			$db->Execute($sql);
		}
*/
		if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);

			if($_POST['DO_EMAIL_FLAG_1'] == 'True' || $_POST['DO_EMAIL_FLAG_2'] == 'True' || $_POST['DO_EMAIL_FLAG_3'] == 'True' || $_POST['DO_EMAIL_FLAG_4'] == 'True' || $_POST['DO_EMAIL_FLAG_5'] == 'True' || $_POST['DO_EMAIL_FLAG_6'] == 'True')
				email_wfl_approval_rejection_investigation($_POST, $id);			
			
			$ret = update_documents_sequence($id,"incident_details");
				if($ret == false){
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				}

		if($ret)
			$return_js_function = "location.href='index.php?m=$m&p=edit&id=$id';";

		break;
	case "edit": // Update node
		//$db->debug=true;
		
		if($_POST['SITE_ID'] == "")
		{
			$ret = false;
			$_SESSION['messageStack']->add("Update failed. Site must not be empty. Please select a Site.");
			break;
		}		
	

		
		if($id>0){

		//----SRA Jerry START
		if($_POST['SEVERITY_RATING'] == "Significant")
		{
			
			$severityRatingPrevious = db_get_field_value("select severity_rating from tblincident_details where incident_number=" . $id,"severity_rating");
			if($severityRatingPrevious == "Significant")
			{
				// Do nothing, previous update was also Significant.
			}
			else
			{
				$to = db_get_field_value("select EMAIL_ADDRESS from TBLEMAILS where EMAIL_NAME='Incident Severity Significant Email'");
				IncidentSeveritySignificantEmailSend($_POST, $id, $to,$subject,$message,$attachments=false,$priority=1,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false);
			}
		}	
		
		//----SRA Jerry End
		
		
			$sql = "SELECT INCIDENT_NUMBER FROM tblincident_details WHERE incdisplyid = '".$_POST['INCDISPLYID']."'";
			if (db_get_one($sql) == $id) {
				$editor = new Editor($m, $p, $a);
				$ret = ($editor->update($id) > 0);
				

				// Now Update the Timestamp Fields
				if($ret == true){
					//print_r($_POST);
					update_consequence_ranking("TBLINCIDENT_DETAILS", $id, $_POST);
					$ret = update_timestamps("tblincident_details", $id);
					update_audit_edit("tblincident_details_audit", $id);				
					update_pictures_for_report($id, $_POST); 
					
				}
			} else {
				$ret = false;
				$_SESSION['messageStack']->add("Another Incident with Incident Number ".$_POST['INCDISPLYID']." already exists.");
			}
		}else{
			$ret = false;
			$_SESSION['messageStack']->add("The Operation failed because there was no ID.");
		}

		if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);

			//Email for approval/rejection workflow of incident report
			if($_POST['DO_EMAIL_FLAG_1'] == 'True' || $_POST['DO_EMAIL_FLAG_2'] == 'True' || $_POST['DO_EMAIL_FLAG_3'] == 'True' || $_POST['DO_EMAIL_FLAG_INC_4'] == 'True' || $_POST['DO_EMAIL_FLAG_INC_5'] == 'True' || $_POST['DO_EMAIL_FLAG_INC_6'] == 'True' || $_POST['DO_EMAIL_FLAG_4'] == 'True' || $_POST['DO_EMAIL_FLAG_5'] == 'True' || $_POST['DO_EMAIL_FLAG_6'] == 'True')
				email_wfl_approval_rejection_investigation($_POST, $id);	

			$ret = update_documents_sequence($id,"incident_details");
				if($ret == false){
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				}

		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		if($ret = $editor->delete($id)){
			$sql_del="delete from " . $TABLES['CHECKBOXES']['table'] . " where " . $TABLES['CHECKBOXES']['id'] . " = $id ";
			$ret = db_query($sql_del);
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Incident records deleted successfully.","success");
						// now delete the action category checkbox values using the same delete array
						if($ret = db_bulk_delete($TABLES['CHECKBOXES']['table'],$TABLES['CHECKBOXES']['id'],$delete)){
							$_SESSION['messageStack']->add("Incident checkbox values deleted successfully.","success");
						}
					}
					if(!$ret) $_SESSION['messageStack']->add("Errors occurred while deleting incidents.<br>".$db->ErrorMsg());
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");
		$ret = false;
		break;
}
if($db->debug==true) exit;
if($ret) {
	if(isset($_POST['rm']) && !empty($_POST['rm'])){
		main_redirect("index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}");
	}
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tronox IT department of this message.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
/*
+------------------------------------------------------------------------
|  Functions
+------------------------------------------------------------------------
*/
// function update_consequence_ranking($table, $id, $_POST){comment dev
function update_consequence_ranking($table, $id, $postData){
	global $db;
	$ret = true;



	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET RISK_LIKELIHOOD_ACTUAL='".$postData['RISK_LIKELIHOOD_ACTUAL']."', RISK_CONSEQUENCE_ACTUAL='".$postData['RISK_CONSEQUENCE_ACTUAL']."', ";
	$update_sql .= "  RISK_RATING_ACTUAL='".$postData['RISK_RATING_ACTUAL']."', RISK_LIKELIHOOD_POTENTIAL='".$postData['RISK_LIKELIHOOD_POTENTIAL']."', ";
	$update_sql .= "  RISK_CONSEQUENCE_POTENTIAL='".$postData['RISK_CONSEQUENCE_POTENTIAL']."', RISK_RATING_POTENTIAL='".$postData['RISK_RATING_POTENTIAL']."' ";
	$update_sql .= "  INITIALRISKLEVEL=".$postData['INITIALRISKLEVEL'];
	$update_sql .= " WHERE incident_number=".$id;

	$reqString = "";
	foreach ($postData as $key => $value)
    $reqString .= $key.'='.$value.'\n';
	
	//email_to_jerry($POST, "DEBUG", $reqString);	
	
	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

// function update_pictures_for_report($id, $_POST) comment dev
function update_pictures_for_report($id, $postData)
{
	global $db;

	$s = "";
	$update_sql = "";
	
	$rs = db_query("select * from Tblincident_Photos Where Investigation_Number =" . $id); //incident_number
	while($tab = $rs->FetchRow()){
		$photoNumber = $tab['PHOTO_NUMBER'];	
		//$s .= 'photoNumber: ' . $photoNumber . ' - ' . $_POST['SHOW_IN_REPORT'][$photoNumber] . ' ';
		if($postData['SHOW_IN_REPORT'][$photoNumber] == 'on')
		{
			$update_sql = " UPDATE Tblincident_Photos" ;
			$update_sql .= " SET SHOW_IN_REPORT='Y' ";
			$update_sql .= " WHERE Investigation_Number=".$id . " and photo_number = " . $photoNumber;
		}else
		{
			$update_sql = " UPDATE Tblincident_Photos" ;
			$update_sql .= " SET SHOW_IN_REPORT='N' ";
			$update_sql .= " WHERE Investigation_Number=".$id . " and photo_number = " . $photoNumber;
		}
		$ret = $db->Execute($update_sql);
		//$_SESSION['messageStack']->add("info: " . $update_sql);
		
	}

	//$_SESSION['messageStack']->add("info: " . $s);

}


function update_timestamps($table, $id){
	global $db;
	$ret = true;



	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET incident_date=TO_TIMESTAMP('".$_POST['INCIDENT_DATE']."','YYYY-MM-DD HH24:MI '), report_date=TO_TIMESTAMP('".$_POST['REPORT_DATE']."','YYYY-MM-DD HH24:MI '), signoff_date_1=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_1']."','YYYY-MM-DD HH24:MI '), signoff_date_2=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_2']."','YYYY-MM-DD HH24:MI '), signoff_date_3=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_3']."','YYYY-MM-DD HH24:MI '), signoff_date_inc_4=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_INC_4']."','YYYY-MM-DD HH24:MI '), signoff_date_inc_5=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_INC_5']."','YYYY-MM-DD HH24:MI '), signoff_date_inc_6=TO_TIMESTAMP('".$_POST['SIGNOFF_DATE_INC_6']."','YYYY-MM-DD HH24:MI ')  ";
	$update_sql .= " WHERE incident_number=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}
function update_audit_new($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE incident_number=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}
function update_audit_edit($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE audit_id >= (select max(audit_id) -1 from ".$table.") and incident_number=".$id;
	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

function update_documents_sequence($id,$type){
	global $TABLES,$db;

	$ret = true;
	// if action category exists then replace the checkbox values
	if(isset($_POST['DOCUMENTLOCATION'])){
		// replace linked records
		$sql_del="delete from " . $TABLES['DOCUMENTLOCATION']['table'] . " where FORMTYPE = '$type' and " . $TABLES['DOCUMENTLOCATION']['id'] . " = $id ";
		$success = db_query($sql_del);
	
		$arr=array();
		$act_cat = $_POST['DOCUMENTLOCATION'];
		$update = false;
		$docerr = false;
		foreach($act_cat as $key => $val){
		
			if(!empty($val)){
								
				if($val{0}!='\\' or $val{1}!='\\'){			
					$_SESSION['messageStack']->add("Document Location contains path '".$val."' which is an incorrectly formatted network path. The document must be located on the network.");
					$docerr = true;
				}else{
					$arr[] = array($id, $val, $type);//comment dev
					// $mappedValues = array();
					// $mappedValues[strtolower('REPORT_ID')] = $id;
					// $mappedValues[strtolower('DOCUMENTLOCATION')] = $val;
					// $mappedValues[strtolower('FORMTYPE')] = $type;
					// array_push($arr, $mappedValues) ;
					$update = true;
				}
			}
		}
		if($update){ 
			// insert new checkbox values
			if($ret = db_bulk_insert($TABLES['DOCUMENTLOCATION']['table'],"REPORT_ID,DOCUMENTLOCATION,FORMTYPE",$arr,$db->debug)){//comment dev
			// if($ret = db_bulk_insert_dev($TABLES['DOCUMENTLOCATION']['table'],"REPORT_ID,DOCUMENTLOCATION,FORMTYPE",$arr,$db->debug)){
				$_SESSION['messageStack']->add("Supporting Documents were successfully updated.","success");
			
			}else{
				$_SESSION['messageStack']->add("An error occured while updating Supporting Document records.<br>".$db->ErrorMsg());
			}
		} // update records is true
		
		// If document path error then set return to false even though some records may have been updated
		if($docerr==true){
			$ret=false;
		}
		
	}
	return $ret;
}

function update_checkbox_values_sequence($tables, $id){
	global $db;
	$ret = true;


	//add dev
	// $mappedFields = array();
	// if(isset($tables['fields'])){
	// 	$flds = explode(",",$tables['fields']);
	// 	for ($i = 0; $i < count($flds); $i++) {
	// 		array_push($mappedFields, strtolower($flds[$i])) ;
	// 	}
	// }
	//end dev

	// get checkbox values array so we can map
	// tblIncidentForm_Checkboxes.Checkbox_Type to tblIncident_CheckboxValues.CheckboxType
	$types = cats_get_checkboxes_values_array();
	// delete * checkbox values and insert the new values if they exist
	$ret = db_query("delete from {$tables['table']} where {$tables['id']}=$id ");
	// loop through types array and check if we have a matching array in the POST
	foreach($types as $key => $chktype){

		if(isset($_POST[$key]) && is_array($_POST[$key])){
			$checkbox = $_POST[$key];

			$arr=array();
			$update = false;

			// only add value to the array if it is not empty
			foreach($checkbox as $k => $chkid){
				if(!empty($chkid)){
					$update = true;
					$arr[] = array($id, $chkid, $chktype);//comment dev
					// $mappedValues = array();
					// $helparr= array($id, $chkid, $chktype);
					// if(!empty($mappedFields)){
					// 	for ($i = 0; $i < count($mappedFields); $i++) {
					// 		$mappedValues[strtolower($mappedFields[$i])] =  $helparr[$i];
							
					// 	}
					// }
					// array_push($arr, $mappedValues);
				}
			}
			if($update){
				// delete the checkbox values by type
				//$ret = db_query("delete from {$tables['table']} where CHECKBOXTYPE='$chktype' and {$tables['id']}=$id ");

				// insert new checkbox values
				if($ret = db_bulk_insert($tables['table'],$tables['fields'],$arr,$db->debug)){//comment dev
				// if($ret = db_bulk_insert_dev($tables['table'],$tables['fields'],$arr,$db->debug)){
					$_SESSION['messageStack']->add("Checkbox ($key) values were successfully updated.","success");
				
				}else{
					$_SESSION['messageStack']->add("An error occured while updating ($key) checkbox value records.<br>".$db->ErrorMsg());
				}
			} // update records is true
		}
	} // loop: checkbox values array
	return (bool)$ret;
}

// house cleaning to be done
$db=db_close();


?>