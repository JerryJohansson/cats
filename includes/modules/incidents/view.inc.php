<!-- BEGIN:: Incident Analysis check dev $RECORD[] is set -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo">Incident Analysis</a></caption>
<tr>
	<td>
		
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">All Incidents</a></caption>
			<tr>
				<td class="label">
					<label>Severity</label>
				</td>
				<td>
					<?php echo (html_display_value('SEVERITY_RATING',isset($RECORD['SEVERITY_RATING'])?$RECORD['SEVERITY_RATING']:''));?>
				</td>
			</tr>
			<tr>
				<td class="label">
					<label>Externally Reported To</label>
				</td>
				<td><div id="divReported_To"><?php echo (html_display_value('Incident_ReportedTo', isset($RECORD['INCIDENT_REPORTEDTO'])?$RECORD['INCIDENT_REPORTEDTO']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Systemic</label>
				</td>
				<td><div id="divSystemic_Cause"><?php echo (html_display_value('Incident_SystemicCause', isset($RECORD['INCIDENT_SYSTEMICCAUSE'])?$RECORD['INCIDENT_SYSTEMICCAUSE']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Behaviour</label>
				</td>
				<td><div id="divBehaviour_Cause"><?php echo (html_display_value('Incident_BehaviourCause', isset($RECORD['INCIDENT_BEHAVIOURCAUSE'])?$RECORD['INCIDENT_BEHAVIOURCAUSE']:''));?></div></td>
			</tr>
			</table>

			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Injury</a></caption>
			<tr>
				<td class="label">
					<label>Injury Classification</label>
				</td>
				<td><div id="divInjury_Classification"><?php echo (html_display_value('Incident_InjuryClass', isset($RECORD['INCIDENT_INJURYCLASS'])?$RECORD['INCIDENT_INJURYCLASS']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Location of Injury</label>
				</td>
				<td><div id="divInjury_Location"><?php echo (html_display_value('Incident_InjuryLocation', isset($RECORD['INCIDENT_INJURYLOCATION'])?$RECORD['INCIDENT_INJURYLOCATION']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Mechanism of Injury</label>
				</td>
				<td><div id="divInjury_Mechanism"><?php echo (html_display_value('Incident_InjuryMechanism', isset($RECORD['INCIDENT_INJURYMECHANISM'])?$RECORD['INCIDENT_INJURYMECHANISM']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Injury Type</label>
				</td>
				<td><div id="divInjury_Type"><?php echo (html_display_value('Incident_InjuryType', isset($RECORD['INCIDENT_INJURYTYPE'])?$RECORD['INCIDENT_INJURYTYPE']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Agency of Injury</label>
				</td>
				<td><div id="divInjury_Agency"><?php echo (html_display_value('Incident_InjuryAgency', isset($RECORD['INCIDENT_INJURYAGENCY'])?$RECORD['INCIDENT_INJURYAGENCY']:''));?></div></td>
			</tr>
			</table>

			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Environment</a></caption>
			<tr>
				<td class="label">
					<label>Environment Classification</label>
				</td>
				<td><div id="divEnvironment_Classification"><?php echo (html_display_value('Incident_EnviroClass', isset($RECORD['INCIDENT_ENVIROCLASS'])?$RECORD['INCIDENT_ENVIROCLASS']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact To (Environment)</label>
				</td>
				<td><div id="divEnvironment_ImpactTo"><?php echo (html_display_value('Incident_ImpactTo', isset($RECORD['INCIDENT_IMPACTTO'])?$RECORD['INCIDENT_IMPACTTO']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact By (Agent)</label>
				</td>
				<td><div id="divEnvironment_ImpactBy"><?php echo (html_display_value('Incident_ImpactBy', isset($RECORD['INCIDENT_IMPACTBY'])?$RECORD['INCIDENT_IMPACTBY']:''));?></div></td>
			</tr>
			</table>

			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Community</a></caption>
			<tr>
				<td class="label">
					<label>Community Classification</label>
				</td>
				<td><div id="divCommunity_Classification"><?php echo (html_display_value('Incident_CommunityClass', isset($RECORD['INCIDENT_COMMUNITYCLASS'])?$RECORD['INCIDENT_COMMUNITYCLASS']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Contact Source</label>
				</td>
				<td><div id="divContact_Source"><?php echo (html_display_value('Incident_ContactSource', isset($RECORD['INCIDENT_CONTACTSOURCE'])?$RECORD['INCIDENT_CONTACTSOURCE']:''));?></div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Substantiated?</label>
				</td>
				<td>
					<?php echo (html_display_value('SUBSTANTIATED',isset($RECORD['SUBSTANTIATED'])?$RECORD['SUBSTANTIATED']:''));?>
				</td>
			</tr>
			</table>

			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Quality</a></caption>
			<tr>
				<td class="label">
					<label>Quality Classification</label>
				</td>
				<td><div id="divQuality_Classification"><?php echo (html_display_value('Incident_QualityClass', isset($RECORD['INCIDENT_QUALITYCLASS'])?$RECORD['INCIDENT_QUALITYCLASS']:''));?></div></td>
			</tr>
			</table>

			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Economic</a></caption>
			<tr>
				<td class="label">
					<label>Economic Classification</label>
				</td>
				<td><div id="divEconomic"><?php echo (html_display_value('Incident_Economic', isset($RECORD['INCIDENT_ECONOMIC'])?$RECORD['INCIDENT_ECONOMIC']:''));?></div></td>
			</tr>
			</table>
		
	</td>
</tr>
<tr>
	<td>
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="anc_quality">Estimated Cost</a></caption>
		<tr>
			<td>
				<?php echo (html_display_value('ESTIMATED_COST', isset($RECORD['ESTIMATED_COST'])?$RECORD['ESTIMATED_COST']:''));?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Incident Analysis -->