<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH . 'sql/actions.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_edit";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;
if($id>0){
	/*Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!*/
	$user_site_id = $_SESSION['user_details']['site_id'];
	$user_site_access = $_SESSION['user_details']['site_access'];
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	//echo $sql;
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	// see if the user has access to this action by checking if he belongs to any one of the sites from the view
	$action_access = cats_check_user_site_access($RECORD['SITE_ID']);
	
	if($action_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>tabs.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/incidents.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
var PAGE = "<?php echo $p;?>";
var INCIDENT_ID = <?php echo $id;?>;
top.gui.edit_checkboxes = new Object();
function init(){
	// initialise the tabbing object
	init_tabs();
	// resize editing section
	init_resize_editor();
	// set the maxlength for textareas
	set_textarea_maxlength();
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------
</script>

</head>
<body class="tb">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onSubmit="return CATS_validateForm(this, 'ACTION_TITLE','','R','SCHEDULED_DATE','','R','ACTION_DESCRIPTION','','R');"<?php
}else{
	?>onSubmit="return CATS_validateForm(this, 'ACTION_TITLE','','R','SCHEDULED_DATE','','R','ACTION_DESCRIPTION','','R','REMINDER_DATE','','R','MANAGED_BY_ID','','R','SITE_ID','','R','DEPARTMENT_ID','','R');"<?php
}
?>>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Details</a>
<?php
if($action=='edit'){
?>
<a
	title="View Follow-up Actions"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">Followup Actions</a>
<a
	title="View Lost Days"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">Lost Days</a>
<a
	title="View Analysis"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">View Analysis</a>
<?php
}
?>
</fieldset>
<fieldset class="tool_bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php
if(cats_user_is_administrator()){
?>
<a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<?php
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed = new Editor($m,$p,$a);
if($id>0){
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
}else{
	echo($ed->buildForm($FIELDS));
}
?>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="cats::Save" value="Save" >
<input type="button" name="cats::New" value="New Action" onclick="_m.newFollowupAction();" >
<input type="button" name="cats::Cancel" value="Cancel" onclick="_m.cancel();">
<input type="button" name="cats::new_action" value="New Action" onclick="_m.newFollowupAction();" >
<input type="button" name="cats::new_lostdays" value="New Lost Days" onclick="_m.newLostDays();" >
<input type="button" name="cats::viewnotes" value="View Case Notes" onclick="_m.viewCaseNotes();" >
</fieldset>		
</fieldset>
<?php
if($p=='edit'){
?>
<!-- BEGIN:: Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onclick="this.blur();">Follow-up Actions for this Incident</a></caption>
<tr id="row_ACTIONS_CREATED">
 <td class="label"><label title="">No. of Actions Created</label></td>
	<td><span id="ACTIONS_CREATED" ><?php echo(Get_Created_Actions($id, "Originating_Incident"));?></span></td>
 <td class="label"><label title="">No. of Actions Closed</label></td>
	<td><span id="ACTIONS_CLOSED" ><?php echo(Get_Closed_Actions($id, "Originating_Incident"));?></span></td>
</tr>
<tr>
	<td colspan="4">
<?php
// Follow-up Actions
$action_edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
$col_attributes = array(' width="2%" ','','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date',$action_edit_button);
$sql = "SELECT ACTION_ID,Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date, '$action_edit_button' as edit  FROM VIEW_ACTION_DETAILS  WHERE Report_Id = $id  AND Origin_Table='Originating_Incident'  ORDER BY Action_Title ASC";
db_render_pager($sql,$col_headers,$col_attributes);
?>
	</td>
</tr>
<tr id="row_RECALCRISKCATEGORY">
 <td class="label"><label title="">Re-assessment of the 'Risk of Recurrence'</label></td>
	<td colspan="3">
		<?php echo html_form_draw_risk_category_dd('RECALCRISKCATEGORY', $RECORD['RECALCRISKCATEGORY'], ' title="Re-assessment of Risk (Category)" ');?>
		&nbsp;-&nbsp;
		<?php echo html_form_draw_risk_level_dd('RECALCRISKLEVEL', $RECORD['RECALCRISKLEVEL'], ' title="Re-assessment of Risk (Level)" ');?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Followup Actions -->

<!-- BEGIN:: Lost Days for this Incident -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onclick="this.blur();">Lost Days</a></caption>
<tr>
	<td>
<?php
// Lost Days
$lost_days_edit_button = "";
$col_attributes = array(' width="5%" ', ' width="20%" ',' width="20%" ',' width="25%" ',' width="25%" ', '');
$col_headers=explode("|","ID,Month/Year|Lost Days|Restricted Days|Injured Person|$lost_days_edit_button");
$sql = "SELECT Lost_Days_Id, Months, Years, Lost_Days, Restricted_Days, Emp_Name, '$lost_days_edit_button' as edit  FROM VIEW_LOST_DAYS  WHERE Incident_Number = $id  ORDER BY Year_Month ASC";
db_render_pager($sql,$col_headers,$col_attributes);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Lost Days for this Incident -->
<?php
}
?>
<!-- BEGIN:: Incident Analysis -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo">Incident Analisis</a></caption>
<tr>
	<td>
		<div id="tabs">
			<div class="tab_off" id="tab_all_incidents" onclick="tab_toggle(this,'edit_checkboxes','Reported_To','Systemic_Cause','Behaviour_Cause');" >All Incidents</div>
			<div class="tab_off" id="tab_injuries" onclick="tab_toggle(this,'edit_checkboxes','Injury_Classification','Injury_Location','Injury_Mechanism','Injury_Type','Injury_Agency');" >Injury</div>
			<div class="tab_off" id="tab_environment" onclick="tab_toggle(this,'edit_checkboxes','Environment_Classification','Environment_ImpactTo','Environment_ImpactBy');" >Environment</div>
			<div class="tab_off" id="tab_community" onclick="tab_toggle(this,'edit_checkboxes','Community_Classification');" >Community</div>
			<div class="tab_off" id="tab_quality" onclick="tab_toggle(this,'edit_checkboxes','Quality_Classification');" >Quality</div>
			<div class="tab_off" id="tab_economic" onclick="tab_toggle(this,'edit_checkboxes','Economic');" >Economic</div>
		</div>
	</td>
</tr>
<tr>
	<td>
		<div class="div_off" id="all_incidents" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_all_incidents">All Incidents Details</a></caption>
			<tr>
				<td class="label">
					<label>Severity</label>
				</td>
				<td>
					<input type="radio" id="Serverity_1" name="SEVERITY_RATING" value="Minor"><label for="Serverity_1" class="normal">Minor</label>
					<input type="radio" id="Serverity_2" name="SEVERITY_RATING" value="Externally Reportable"><label for="Serverity_2" class="normal">Externally Reportable</label>
					<input type="radio" id="Serverity_3" name="SEVERITY_RATING" value="Significant" ><label for="Serverity_3" class="normal">Significant</label>
				</td>
			</tr>
			<tr>
				<td class="label">
					<label>Externally Reported To</label>
				</td>
				<td><div id="divReported_To">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Systemic</label>
				</td>
				<td><div id="divSystemic_Cause">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Behaviour</label>
				</td>
				<td><div id="divBehaviour_Cause">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="injuries" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_injury">Injury Details</a></caption>
			<tr>
				<td class="label">
					<label>Injury Classification</label>
				</td>
				<td><div id="divInjury_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Location of Injury</label>
				</td>
				<td><div id="divInjury_Location">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Mechanism of Injury</label>
				</td>
				<td><div id="divInjury_Mechanism">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Injury Type</label>
				</td>
				<td><div id="divInjury_Type">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Agency of Injury</label>
				</td>
				<td><div id="divInjury_Agency">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="environment" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_environment">Environment Details</a></caption>
			<tr>
				<td class="label">
					<label>Environment Classification</label>
				</td>
				<td><div id="divEnvironment_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact To (Environment)</label>
				</td>
				<td><div id="divEnvironment_ImpactTo">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact By (Agent)</label>
				</td>
				<td><div id="divEnvironment_ImpactBy">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="community" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_community">Community Details</a></caption>
			<tr>
				<td class="label">
					<label>Community Classification</label>
				</td>
				<td><div id="divCommunity_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Substantiated?</label>
				</td>
				<td>
					<input type="radio" name="SUBSTANTIATED" id="Sub_1" value="Substantiated" /><label for="Sub_1" class="chk">Substantiated</label>
					<input type="radio" name="SUBSTANTIATED" id="Sub_2" value="Unsubstantiated" /><label for="Sub_2" class="chk">Unsubstantiated</label>
				</td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="quality" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Quality Details</a></caption>
			<tr>
				<td class="label">
					<label>Quality Classification</label>
				</td>
				<td><div id="divQuality_Classification">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="economic" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_economic">Economic Details</a></caption>
			<tr>
				<td class="label">
					<label>Economic Classification</label>
				</td>
				<td><div id="divEconomic">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
	</td>
</tr>
<tr>
	<td>
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="anc_quality">Estimated Cost</a></caption>
		<tr>
			<td>
				<?php echo html_draw_estimated_cost_radioset('ESTIMATED_COST', $RECORD['ESTIMATED_COST']);?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Incident Analysis -->
</div>
</form>