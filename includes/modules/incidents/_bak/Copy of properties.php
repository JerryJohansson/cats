<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Incidents',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Incident',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Incidents',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS', 
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INCIDENT_DETAILS',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS', 
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INCIDENT_DETAILS',
					'filter'=>' 1=2 ',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS', 
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INCIDENT_SEARCHVIEW',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($action = 'search'){
	$ret = array();
	switch($action){
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'INCDISPLYID' => array('label'=>'IncDisplyId', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'DEPARTMENT_ID' => array('label'=>'Department Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'INITIATED_BY_ID' => array('label'=>'Initiated By Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'REPORTEDCOMPANYID' => array('label'=>'ReportedCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
					'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SUPERINTENDENT_ID' => array('label'=>'Superintendent Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Persons Involved Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'INJURED_PERSON_ID' => array('label'=>'Injured Person Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'INJUREDCOMPANYID' => array('label'=>'InjuredCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INITIALRISKCATEGORY' => array('label'=>'InitialRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INITIALRISKLEVEL' => array('label'=>'InitialRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PRIMARYPERSONID' => array('label'=>'PrimaryPersonId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PRIMARYCOMPANYID' => array('label'=>'PrimaryCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENTSTATUS' => array('label'=>'IncidentStatus', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'INCDISPLYID' => array('label'=>'IncDisplyId', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'DEPARTMENT_ID' => array('label'=>'Department Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'INITIATED_BY_ID' => array('label'=>'Initiated By Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'REPORTEDCOMPANYID' => array('label'=>'ReportedCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
					'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SUPERINTENDENT_ID' => array('label'=>'Superintendent Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Persons Involved Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'INJURED_PERSON_ID' => array('label'=>'Injured Person Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					'INJUREDCOMPANYID' => array('label'=>'InjuredCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INITIALRISKCATEGORY' => array('label'=>'InitialRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INITIALRISKLEVEL' => array('label'=>'InitialRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PRIMARYPERSONID' => array('label'=>'PrimaryPersonId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PRIMARYCOMPANYID' => array('label'=>'PrimaryCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'INCIDENTSTATUS' => array('label'=>'IncidentStatus', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search':
		case 'results':
			$ret = array(
				'INCIDENT_NUMBER' => array('label'=>'Incident Number', 'operator'=>'IN', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'SUPERINTENDENT_ID' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>2, 'col_span'=>1), 
				'INITIATED_BY_ID' => array('label'=>'Initiated By Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
				'REPORTEDCOMPANYID' => array('label'=>'ReportedCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
				'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
				'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Persons Involved Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'INJURED_PERSON_ID' => array('label'=>'Injured Person Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
				'INJUREDCOMPANYID' => array('label'=>'InjuredCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INITIALRISKCATEGORY' => array('label'=>'InitialRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INITIALRISKLEVEL' => array('label'=>'InitialRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'PRIMARYPERSONID' => array('label'=>'PrimaryPersonId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'PRIMARYCOMPANYID' => array('label'=>'PrimaryCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'INCIDENTSTATUS' => array('label'=>'IncidentStatus', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}
if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>