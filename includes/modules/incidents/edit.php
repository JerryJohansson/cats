<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH . 'sql/actions.php');
require_once('properties.php');



/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_edit";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
$currentTab=(isset($_REQUEST['t']))?$_REQUEST['t']:0;
$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;

// Security Check
if((!cats_user_is_editor(true) && !cats_user_is_editor(false) && !cats_user_is_super_administrator() && !cats_user_is_incident_privileged() && !cats_user_is_incident_analysis())  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New Incident");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

if($id>0){
	/*Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!*/
	$user_site_id = $_SESSION['user_details']['site_id'];
	$user_site_access = $_SESSION['user_details']['site_access'];
	// have to explicitly select fields because oracle doesn't return time unless you ask it to :(
	//if($p=='view'){
	//	$sql_fields = " * ";
	//}else{
		$sql_fields = "INCIDENT_NUMBER, INCDISPLYID, INCIDENTSTATUS, SITE_ID, DEPARTMENT_ID, INITIATED_BY_ID, REPORTEDCOMPANYID, ";
		$sql_fields .= "TO_CHAR(INCIDENT_DATE,'YYYY-MM-DD HH24:MI') INCIDENT_DATE, TO_CHAR(REPORT_DATE,'YYYY-MM-DD HH24:MI') REPORT_DATE, ";
		$sql_fields .= "REPORTED_TO, SUPERINTENDENT_ID, LOCATION, AREA, EQUIPMENT_NO, GOLDENRULE, GOLDENRULEOTHER, PRIMARYPERSONID, PRIMARYCOMPANYID, PERSONS_INVOLVED_DESCRIPTION, ";
		$sql_fields .= "INJURED_PERSON_ID, INJUREDCOMPANYID, INJURED_PERSON_GENDER, INCIDENT_TITLE, INCIDENT_DESCRIPTION, IMMEDIATE_CORRECTIVE_ACTIONS, ";
		$sql_fields .= "INITIALRISKCATEGORY, INITIALRISKLEVEL, SEVERITY_RATING, SUBSTANTIATED, ESTIMATED_COST, RECALCRISKCATEGORY, RECALCRISKLEVEL, ";
		$sql_fields .= "SECTION_ID, SHIFT_ID, ";
		$sql_fields .= "ACTIONS_CREATED(INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS, ACTIONS_CLOSED(INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS, ";
		$sql_fields .= "INJURIES_CHECKED(INCIDENT_NUMBER) INJURIES, ENVIRONMENT_CHECKED(INCIDENT_NUMBER) ENVIRONMENT, COMMUNITY_CHECKED(INCIDENT_NUMBER) COMMUNITY, QUALITY_CHECKED(INCIDENT_NUMBER) QUALITY, ECONOMIC_CHECKED(INCIDENT_NUMBER) ECONOMIC, ";
		$sql_fields .= "RISK_LIKELIHOOD_ACTUAL, RISK_CONSEQUENCE_ACTUAL,  RISK_RATING_ACTUAL, RISK_LIKELIHOOD_POTENTIAL, RISK_CONSEQUENCE_POTENTIAL, RISK_RATING_POTENTIAL, ";
		$sql_fields .= "TEAM_SELECTED_BY_PERSONID, INVESTIGATION_TEAM_PERSONID, DETAILED_DESCRIPTION_HOW, WITNESS_1_PERSONID, WITNESS_1_PERSONID, WITNESS_2_PERSONID, IMMEDIATE_CAUSES, UNDERLYING_CAUSES, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_1, FORWARD_PERSON_ID_1, TO_CHAR(SIGNOFF_DATE_1,'YYYY-MM-DD HH24:MI') SIGNOFF_DATE_1, STATUS_1, MESSAGE_1, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_2, FORWARD_PERSON_ID_2, TO_CHAR(SIGNOFF_DATE_2,'YYYY-MM-DD HH24:MI') SIGNOFF_DATE_2, STATUS_2, MESSAGE_2, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_3, FORWARD_PERSON_ID_3, TO_CHAR(SIGNOFF_DATE_3,'YYYY-MM-DD HH24:MI') SIGNOFF_DATE_3, STATUS_3, MESSAGE_3, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_INC_4, FORWARD_PERSON_ID_INC_4, TO_CHAR(SIGNOFF_DATE_INC_4,'YYYY-MM-DD HH24:MI') SIGNOFF_DATE_INC_4, STATUS_INC_4, MESSAGE_INC_4, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_INC_5, FORWARD_PERSON_ID_INC_5, TO_CHAR(SIGNOFF_DATE_INC_5,'YYYY-MM-DD HH24:MI') SIGNOFF_DATE_INC_5, STATUS_INC_5, MESSAGE_INC_5, ";
		$sql_fields .= "SIGNOFF_PERSON_ID_INC_6, FORWARD_PERSON_ID_INC_6, TO_CHAR(SIGNOFF_DATE_INC_6,'YYYY-MM-DD HH24:MI') SIGNOFF_DATE_INC_6, STATUS_INC_6, MESSAGE_INC_6, ";
		$sql_fields .= "PRIMARY_PERSON_ROLE, OBJECT_CAUSING_INJURY, PRIMARY_PERSON_DUTY_STATUS, PRIMARY_PERSON_CONTRACTOR_NAME ";
	//}
	$sql = "SELECT $sql_fields FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	//echo 'JERRY incident SQL: ' . $sql;
	//echo 'URL: ' . $_SERVER['SERVER_NAME'];
	$action_access = true;
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	
	
	$INCIDENTSTATUS = $RECORD['INCIDENTSTATUS'];
	
	if($p!='view')
		// see if the user has access to this action by checking if he belongs to any one of the sites from the view
		$action_access = cats_check_user_site_access($RECORD['SITE_ID']);
	
	if($action_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}


}
$RS=array();//add dev for load formdata
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>tabs.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/incidents.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
//var PAGE = "<?php echo $p;?>";
var INCIDENT_ID = <?php echo $id;?>;
top.gui.edit_checkboxes = new Object();
function init(){
	// initialise the tabbing object
	init_tabs(<?php echo $currentTab;?>);
	// resize editing section
	init_resize_editor();
	// set the maxlength for textareas
	set_textarea_maxlength();
	
	SHOW_ANCHOR=false;
<?php
if($p!='view'){
	echo("	tab_toggle(element('tab_all_incidents'),'edit_checkboxes','Reported_To','Systemic_Cause','Behaviour_Cause');\n");
	if($RECORD['INJURIES']>0) echo("	element('tab_injuries').onclick();\n");
	if($RECORD['ENVIRONMENT']>0) echo("	element('tab_environment').onclick();\n");
	if($RECORD['COMMUNITY']>0) echo("	element('tab_community').onclick();\n");
	if($RECORD['QUALITY']>0) echo("	element('tab_quality').onclick();\n");
	if($RECORD['ECONOMIC']>0) echo("	element('tab_economic').onclick();\n");
}
?>
	
	
	// create document.mousedown handlers
	init_document_handlers();
	
	return true;
}
window.onload = init;
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------
</script>
<style type="text/css">
.tab_off, .tab_on {
	float:none;
	font: bold 11px verdana;
	color: #333333;
}

.photoSubmitDIV{
	position: top: 145px; right: 100px; 
}
.signoffBy {
	margin-top:15px;
}
.textVeryHigh{
	background-color:red;
}
.textHigh{
	background-color:orange;
	color: black;
}
.textMedium{
	background-color:yellow;
	color: black;
}
.textLow{
	background-color:green;
}

.imgriskmatrixDiv{
	position: top: 145px; right: 100px; 
}


</style>
<style type="text/css">
table.imagetable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #999999;
	border-collapse: collapse;
}
table.imagetable th {
	background:#b5cfd2 url('cell-blue.jpg');
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #999999;
}
table.imagetable td {
	background:#dcddc0 url('cell-grey.jpg');
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #999999;
}
</style>
</head>
<body class="edit">
<form id="editIncidentForm" name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onSubmit="return CATS_validateForm(this, 'INCDISPLYID','','R','SITE_ID','','R','DEPARTMENT_ID','','R','INITIATED_BY_ID','','R','REPORTEDCOMPANYID','','R','INCIDENT_DATE','','R','REPORT_DATE','','R','REPORTED_TO','','R','SUPERINTENDENT_ID','','R','LOCATION','','R','AREA','','R','PRIMARYPERSONID','','R','PRIMARYCOMPANYID','','R','INCIDENT_TITLE','','R','INCIDENT_DESCRIPTION','','R','INITIALRISKCATEGORY','','R','INITIALRISKLEVEL','','R');"<?php
}else{
	?>onSubmit="return CATS_validateForm(this, 'INCDISPLYID','','R','SITE_ID','','R','DEPARTMENT_ID','','R','INITIATED_BY_ID','','R','REPORTEDCOMPANYID','','R','INCIDENT_DATE','','R','REPORT_DATE','','R','REPORTED_TO','','R','SUPERINTENDENT_ID','','R','LOCATION','','R','AREA','','R','PRIMARYPERSONID','','R','PRIMARYCOMPANYID','','R','INCIDENT_TITLE','','R','INCIDENT_DESCRIPTION','','R','INITIALRISKCATEGORY','','R','INITIALRISKLEVEL','','R');"<?php
}
?>>


<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
	<a
		title="Edit <?PHP echo $module_name_txt;?> Details"
		id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
		href="#" onClick="return tab_onclick(this);" 
		style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<?php
if($action=='edit'){
?>
<a
	title="View Follow-up Actions"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return selectTab(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/followup.gif);">Followup Actions</a>
<a
	title="View Lost Days"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return selectTab(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/lostdays.gif);">Lost Days</a>
	

<?php
}
?>
<a
	title="View Analysis"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return selectTab(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/analysis.gif);">View Analysis</a>

</fieldset>

<fieldset class="tool_bar">
<?php
if($p=='edit'){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<?php if($p != 'view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSaveForm(); " 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php
}else{
?>

<?php if(cats_user_is_super_administrator() || cats_user_is_incident_privileged() || cats_user_is_administrator() || cats_user_is_incident_analysis() || cats_user_is_editor(true) || cats_user_is_editor(false)){ ?>
	<a
		title="Edit <?PHP echo $module_name_txt;?>"
		href="javascript:_m.edit();" 
		class="main_edit"
		style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>

<?php
}
?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php if($p != 'view'){ ?>

<?php if($p == 'edit'){ ?>
<a
	title="Print Preview"
	href="javascript:_m.view();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>
<?php } ?>
<?php
}else{
?>
<a
	title="Print"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>
<?php
}
?>
<?php
if(cats_user_is_administrator() && $p=='edit'){
?>
<!-- <a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a> -->
<?php
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">


<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed = new Editor($m,$p,$a);
if($id>0){
	// echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id])); comment dev
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,isset($RS[$id])?$RS[$id]: false));
}else{
	echo($ed->buildForm($FIELDS));
}
?>

<?php
// Investigation
$investigationNumber = 0;
$sql = "SELECT INVESTIGATION_NUMBER FROM TBLINVESTIGATION_DETAILS WHERE INCIDENT_NUMBER = " . $id;
$rs = $db->Execute($sql);
if($rs->RecordCount()){
	while ($arr = $rs->FetchRow()) {
		$investigationNumber = $arr['INVESTIGATION_NUMBER'];
	}
}	
?>
<div style="height:300px">
</div>


<fieldset class="tbar" style="text-align:right; ">

<?php if($p != 'view'){ ?>
<input type="button" id="saveButton" class="btn btn-primary btn-sm active" name="cats::Save" value="Save" onclick="javascript:preSaveForm();">
<?php	
}
?>

	<input type="button" id="btnCreateIncidentReport" class="btn btn-default btn-sm active" name="cats::IncidentReport" value="Create Incident Report" onClick="goToIncidentReport();" >

<?php
if($p=='edit'){
?>
	<input type="button" id="btnEditICAM" class="btn btn-default btn-sm active" name="cats::Investigation" value="Edit ICAM Investigation" onclick="top.show_edit_screen('investigation','index.php?id=<?php echo($investigationNumber); ?>&m=investigation&p=edit&a=edit&rm=investigation&rp=edit&rt=2&incidentId=<?php echo($RECORD['INCIDENT_NUMBER']); ?>');" >
	<input type="button" id="btnAddICAM" class="btn btn-default btn-sm active" name="cats::Investigation" value="Add ICAM Investigation" onclick="top.show_edit_screen('investigation','index.php?id=0&incidentId=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&m=investigation&p=edit&a=add&rm=investigation&rp=add&rt=2');" >
<?php	
}
?>
	<input type="button" id="btnAudit" class="btn btn-default btn-sm active" name="btnAudit" value="Audit" onClick="openAudit();" >



<?php if($p == 'edit'){ ?>
<!-- <input type="button" class="button" name="cats::View" value="Print Preview" onClick="_m.view();" > -->
<!--input type="button" class="button" name="cats::viewnotes" value="View Case Notes" onclick="top.show_edit_screen('injury_case_notes','index.php?INCIDENT_NUMBER=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&INCDISPLYID=<?php echo($RECORD['INCDISPLYID']); ?>&m=injury_case_notes&p=edit&a=edit&rm=incidents&rp=edit&rt=2');" -->
<?php
}else{
?>
<?php if(cats_user_is_super_administrator() || cats_user_is_incident_privileged() || cats_user_is_administrator() || cats_user_is_incident_analysis() || cats_user_is_editor(true) || cats_user_is_editor(false)){ ?>

<input id="editBtn" type="button" class="btn btn-default btn-sm active" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<!-- <input type="button" class="button" name="cats::Print" value="Print" onClick="printPage();" > -->
<?php
}
?>
<?php
}
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">

<?php if($p == 'edit'){ ?>
<input type="button" class="btn btn-default btn-sm active" name="cats::New" value="New Incident" onClick="_m.newModule();" >
<?php
}
?>

</fieldset>		
</fieldset>
<?php
if($p=='edit'||$p=='view'){
?>
<!-- BEGIN:: Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Follow-up Actions for this Incident</a></caption>
<?php
if($p=='edit'){
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php
}
?>
<tr id="row_ACTIONS_CREATED">
 <td class="label"><label title="">No. of Actions Created</label></td>
	<td valign="middle"><span id="ACTIONS_CREATED" ><?php echo($RECORD['CREATEDACTIONS']);?></span></td>
 <td class="label"><label title="">No. of Actions Closed</label></td>
	<td valign="middle"><span id="ACTIONS_CLOSED" ><?php echo($RECORD['CLOSEDACTIONS']);?></span></td>
</tr>
<tr>
	<td colspan="4">
<?php
// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","","");

// Follow-up Actions
$action_edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
$col_attributes = array(' width="2%" ','','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date',$action_edit_button);
$sql = "SELECT ACTION_ID,Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date, '$action_edit_button' as edit  FROM VIEW_ACTION_DETAILS  WHERE Report_Id = $id  AND Origin_Table='Originating_Incident'  ORDER BY Action_Title ASC";
//db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
cats_form_pager($sql,$col_headers,$col_attributes,'', '', '', 'ASC', $col_links, false, 1000);
?>
	</td>
</tr>
<?php
if($p=='view'){ // ***************   VIEW   ******************* //
?>
<tr id="row_RECALCRISKCATEGORY">
	<td class="label"><label title="">Re-assessment of the 'Risk of Recurrence' (Risk Category)</label></td>
	<td>
		<?php echo html_display_value('RECALCRISKCATEGORY', isset($RECORD['RECALC_RISKCAT'])?$RECORD['RECALC_RISKCAT']:"", ' title="Re-assessment of Risk (Category)" ');?><!-- change dev check $RECORD['RECALC_RISKCAT'] -->

	</td>
<!--	<td class="label"><label title="">Risk Level</label></td>
	<td>-->
		<?php echo html_draw_hidden_field('RECALCRISKLEVEL', isset($RECORD['RECALC_RISKLEVEL'])?$RECORD['RECALC_RISKLEVEL']:"", ' title="Re-assessment of Risk (Level)" ');?><!-- change dev check $RECORD['RECALC_RISKLEVEL'] -->
<!--	</td>-->
</tr>
<?php
}else{ // ***************   EDIT   ******************* //
?>
<tr id="row_RECALCRISKCATEGORY">
	<td class="label"><label title="">Re-assessment of the 'Risk of Recurrence' (Risk Category)</label></td>
	<td>
		<?php echo html_form_draw_risk_category_dd('RECALCRISKCATEGORY', $RECORD['RECALCRISKCATEGORY'], ' title="Re-assessment of Risk (Category)" ');?>
	</td>
<!--	<td class="label"><label title="">Risk Level</label></td>
	<td>-->
		<?php echo html_draw_hidden_field('RECALCRISKLEVEL', $RECORD['RECALCRISKLEVEL'], ' title="Re-assessment of Risk (Level)" ');?>
<!--	</td>-->
</tr>
<?php
}
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php
}
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Followup Actions -->

<!-- BEGIN:: Lost Days for this Incident -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Lost Days</a></caption>
<tr>
	<td>
<?php
// Lost Days
$lost_days_edit_button = "edit:javascript:top.show_edit_screen(\"lost_days\",\"index.php?m=lost_days&p=edit&id=:LOST_DAYS_ID:\")";
//$lost_days_edit_button = "";
$col_attributes = array(' width="5%" ', ' width="20%" ',' width="20%" ',' width="25%" ',' width="25%" ', '');
$col_headers=explode("|","ID|Month/Year|Lost Calendar Days|Lost Rostered Days|Restricted Calendar Days|Injured Person|$lost_days_edit_button");
$sql = "SELECT Lost_Days_Id, Months||'/'||Years, Lost_Days, Rostered_Days, Restricted_Days, Emp_Name, '$lost_days_edit_button' as edit  FROM VIEW_LOST_DAYS  WHERE Incident_Number = $id  ORDER BY Year_Month ASC";
db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
	if($RECORD['INJURED_PERSON_ID'] > 0){
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::new_lostdays" value="New Lost Days" onClick="top.show_edit_screen('lost_days','index.php?INCIDENT_NUMBER=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&INCDISPLYID=<?php echo($RECORD['INCDISPLYID']); ?>&EMP_ID=<?php echo($RECORD['INJURED_PERSON_ID']); ?>&m=lost_days&p=new&a=add&rm=incidents&rp=edit&rid=<?php echo($id); ?>&rt=2');" >
<?php
	}
}
?>
<input type="button" class="btn btn-default btn-sm active" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Lost Days for this Incident -->
<?php
}

if($p=='view'){
	require_once('view.inc.php');
}else{
	require_once('edit.inc.php');
}


?>
</div>



</form>

	<!-- Delete photo form -->
	<form  action="http://<?PHP echo $_SERVER['SERVER_NAME']; ?>/includes/modules/incidents/deletephoto.php" id="FormDeletePhoto" method="POST"> 
		<input type="hidden" id="PHOTO_NUMBER" name="PHOTO_NUMBER" value=""/>
		<input type="hidden" id="INCIDENT_NUMBER" name="INCIDENT_NUMBER" value="<?PHP echo $id; ?>" />
	</form>




	<!-- --------------------------------------------------------------------------------->
	<!-- Modal windows - START -->
		
		<!-- Workflow Help Window -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="width: 550px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Workflow help</h4>
              </div>
              <div class="modal-body" style="background-color: #dedef8;">

				<img src="/files/Image/workflow.png" width="500px"/>

                <!-- Table goes in the document BODY -->
                <table class="imagetable" style="display:none">
                <tr>
	                <th>Step</th><th>Determine</th><th>Process</th><th>Tools</th>
                </tr>
                <tr>
	                <td>1.</td><td>What Happened</td><td>Data Collection</td><td>PEEPO</td>
                </tr>
                <tr>
	                <td>2.</td><td>Why it Happened</td><td>Collected data analysis</td><td>ICAM</td>
                </tr>
                <tr>
	                <td>3.</td><td>What are we going to do about it?</td><td>Develop recommendations</td><td>Hierarchy of control Benefit assessment</td>
                </tr>
                <tr>
	                <td>4.</td><td>What did we learn that we can share?</td><td>Key learnings</td><td>Incident Report Toolbox briefings</td>
                </tr>
                </table>

              </div>
			  <div>
			  
			  </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
	
		
      <!-- Upload Window -->
        <div class="modal fade" id="myModalUpload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload</h4>
              </div>
              <div class="modal-body" style="background-color: #dedef8;">



				   <div id="divPhotoSubmit">
						<form ENCTYPE="multipart/form-data"  action="http://<?PHP echo $_SERVER['SERVER_NAME']; ?>/includes/modules/incidents/attachphoto.php?id=<?PHP echo $id; ?>" id="FormPhoto" method="POST">
							<input type="hidden" id="INCIDENT_NUMBER" value="<?PHP echo $id; ?>"/>
							<input class="sfield" type="file" name="PHOTO_ATTACHMENTS" id="PHOTO_ATTACHMENTS"  title="Attach Photo of Incident" >
							<button id="uploadPhotoBtn" class="btn btn-primary btn-xs" type="button" onClick="doUploadPhoto()">Upload</button>
						</form>
					</div>

              </div>
              <div class="modal-footer">
                <button type="button" id="BthLikelihoodClose2" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
		
		
		<div id="dialog-confirm" title="Values have changed">
		  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Do you want to save your changes?</p>
		</div>	
		
		
		
	<!-- Modal windows - END -->
	<!----------------------------------------------------------------------------------->













<!-- php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?> -->

<script type='text/javascript'>

	function goToIncidentReport()
	{
		if(hasIncidentReportPermission)
		{
			void(window.open("<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/IncidentReportPDF.aspx?IncidentNumber=<?php echo($RECORD['INCIDENT_NUMBER']); ?>"));
		}
		else{
			alert("You need edit permission or higher to create an investigation.");
		}
	}
	
	function goToInvestigation()
	{
		window.location.href = "<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/InvestigationSearch.aspx?IncidentNumber=<?php echo($RECORD['INCIDENT_NUMBER']); ?>";
	}

	 function openAudit()
	 {
 		window.open("<?php echo(CATS_DOT_NET_APP_URL) ?>Pages/AuditReportIncidents.aspx?incidentNumber=<?php echo($RECORD['INCIDENT_NUMBER']); ?>");
	 }	
	
	
	
<?php

	$sql = "select employee_number as ID, nvl(Related_Superintendent, 0) as TEXT from tblemployee_details ";
	$val = html_db_options($sql, 'ID', 'TEXT', true);

	echo('var employeeSuperintendentMappingArray = [');
	foreach($val as $row){
		echo('[' . $row['id'] . ',' . $row['text'] . '],'); 
	}
	echo('[[ 0 ], [ 0]]];');
	
?>

	var hasIncidentReportPermission = false;
	var forwardPersonId1Onload = $("#FORWARD_PERSON_ID_1").val();
	var forwardPersonId2Onload = $("#FORWARD_PERSON_ID_2").val();
	var forwardPersonId3Onload = $("#FORWARD_PERSON_ID_3").val();
	var forwardPersonId4Onload = $("#FORWARD_PERSON_ID_INC_4").val();
	var forwardPersonId5Onload = $("#FORWARD_PERSON_ID_INC_5").val();
	var forwardPersonId6Onload = $("#FORWARD_PERSON_ID_INC_6").val();
	
	var IS_FIELD_CHANGED = false;

	$(document).ready(function() {
	
		var cats_user_is_administrator = '<?PHP echo cats_user_is_administrator();?>';
		var cats_user_is_admin_group = '<?PHP echo cats_user_is_admin_group();?>';
		var cats_user_is_super_administrator = '<?PHP echo cats_user_is_super_administrator();?>';
		var cats_user_is_root_administrator = '<?PHP echo cats_user_is_root_administrator();?>';
		var cats_user_is_editor = '<?PHP echo cats_user_is_editor();?>';
		var cats_user_is_editor_privilaged = '<?PHP echo cats_user_is_editor(true);?>';
		var cats_user_is_incident_privileged = '<?PHP echo cats_user_is_incident_privileged();?>';
		var typeOfView = '<?PHP echo $p;?>';
		var incidentStatus = "<?PHP echo isset($INCIDENTSTATUS)?$INCIDENTSTATUS:'';?>";

		//alert('incidentStatus:  ' + incidentStatus);
	
		// If 'View', and user is Editor (Privilaged or Restricted) or Administrator - and Incident is closed, Edit is not allowed.	
		if(cats_user_is_editor == '1' || cats_user_is_editor_privilaged == '1' || cats_user_is_administrator=='1'){ 
			//alert('before INCIDENTSTATUS INSIDE');
			
			if(typeOfView=='edit'){ //jerry
				if(incidentStatus == 'Closed')
				{
					document.getElementById("saveButton").disabled=true;
					$(':input').attr('disabled', true);
				}
			}  
			if(typeOfView=='view'){
				if(incidentStatus == 'Closed')
				{
					document.getElementById("editBtn").disabled=true;
					$(':input').attr('disabled', true);
				}

			}

			
		}
		
		// If incident closed, these fields are disabled. Make them always enabled.
		document.getElementById("btnCreateIncidentReport").disabled=false;
		document.getElementById("btnAudit").disabled=false;
	
		if(document.getElementsByName("DOCUMENTLOCATION[]")[0]) 
			document.getElementsByName("DOCUMENTLOCATION[]")[0].disabled=false;
		if(document.getElementsByName("DOCUMENTLOCATION[]")[1])
			document.getElementsByName("DOCUMENTLOCATION[]")[1].disabled=false;
		if(document.getElementsByName("DOCUMENTLOCATION[]")[2])
			document.getElementsByName("DOCUMENTLOCATION[]")[2].disabled=false;
		if(document.getElementsByName("DOCUMENTLOCATION[]")[3])
			document.getElementsByName("DOCUMENTLOCATION[]")[3].disabled=false;		
		if(document.getElementsByName("DOCUMENTLOCATION[]")[4])
			document.getElementsByName("DOCUMENTLOCATION[]")[4].disabled=false;		
		if(document.getElementsByName("DOCUMENTLOCATION[]")[5])
			document.getElementsByName("DOCUMENTLOCATION[]")[5].disabled=false;		
	
	//Restrictions of status update (Open/Closed) for
	// PCR Admin 	
	// Action Admin
	// Editors (Privilaged or Restricted)
	// Readers (Privilaged or Restricted)
	<?php if(cats_user_is_super_administrator() || cats_user_is_incident_privileged()){ 
	}else {?>
		document.getElementById("INCIDENTSTATUS[0]").disabled=true;
		document.getElementById("INCIDENTSTATUS[1]").disabled=true;
	<?php } ?>
	
	
	
	
		$("#INCIDENT_DATE_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
	
		$("#REPORT_DATE_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
		
		$("#SIGNOFF_DATE_1_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});

		$("#SIGNOFF_DATE_2_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});

		$("#SIGNOFF_DATE_3_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
		
		$("#SIGNOFF_DATE_INC_4_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
		
		$("#SIGNOFF_DATE_INC_5_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
	
		$("#SIGNOFF_DATE_INC_6_d").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
	
		$(".sfield").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
		
		
		$(".sfield_lge").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
		
		
		$(".bfield").on("propertychange change keyup paste input", function(){
		  IS_FIELD_CHANGED = true;
		});
		
	
		//$( ".sfield" ).blur(function() {
		//  alert( "Handler for .blur() called." );
		//});
	
		// Set the colors
		setRiskRating($("#RISK_LIKELIHOOD_ACTUAL").val(), $("#RISK_CONSEQUENCE_ACTUAL").val(), "ACTUAL");
		setRiskRating($("#RISK_LIKELIHOOD_POTENTIAL").val(), $("#RISK_CONSEQUENCE_POTENTIAL").val(), "POTENTIAL");
		//setLikelihoodActual($("#RISK_LIKELIHOOD_ACTUAL").val());
		//setConsequencePotential($("#RISK_CONSEQUENCE_POTENTIAL").val());
		
		$("#divPhotoSubmit").addClass("photoSubmitDIV");
	
//		$( ".button" ).addClass("btn btn-default btn-sm active");
//		$( ".button" ).removeClass("button");
		
//		$( ".submit" ).addClass("btn btn-primary btn-sm active");
//		$( ".submit" ).removeClass("submit");

		$( ".reset" ).addClass("btn btn-warning btn-sm active");
		$( ".reset" ).removeClass("reset");

		$("#SIGNOFF_PERSON_ID_1_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_2_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_3_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_INC_4_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_INC_5_text").addClass("signoffBy");
		$("#SIGNOFF_PERSON_ID_INC_6_text").addClass("signoffBy");
		
	// Approved = green
		if($("#STATUS_1").val() == "APPROVED")
		{
			$("#STATUS_1").addClass("textLow");	
		}
		if($("#STATUS_2").val() == "APPROVED")
		{
			$("#STATUS_2").addClass("textLow");	
		}
		if($("#STATUS_3").val() == "APPROVED")
		{
			$("#STATUS_3").addClass("textLow");	
		}
		if($("#STATUS_INC_4").val() == "APPROVED")
		{
			$("#STATUS_INC_4").addClass("textLow");	
		}
		if($("#STATUS_INC_5").val() == "APPROVED")
		{
			$("#STATUS_INC_5").addClass("textLow");	
		}
		if($("#STATUS_INC_6").val() == "APPROVED")
		{
			$("#STATUS_INC_6").addClass("textLow");	
		}
		
		
		// Rejected = red
		if($("#STATUS_1").val() == "REJECTED")
		{
			$("#STATUS_1").addClass("textVeryHigh");	
		}
		if($("#STATUS_2").val() == "REJECTED")
		{
			$("#STATUS_2").addClass("textVeryHigh");	
		}
		if($("#STATUS_3").val() == "REJECTED")
		{
			$("#STATUS_3").addClass("textVeryHigh");	
		}		
		if($("#STATUS_INC_4").val() == "REJECTED")
		{
			$("#STATUS_INC_4").addClass("textVeryHigh");	
		}		
		if($("#STATUS_INC_5").val() == "REJECTED")
		{
			$("#STATUS_INC_5").addClass("textVeryHigh");	
		}		
		if($("#STATUS_INC_6").val() == "REJECTED")
		{
			$("#STATUS_INC_6").addClass("textVeryHigh");	
		}		
		
		if($("#FORWARD_PERSON_ID_1").val() != "")
		{
			disableForwardFields(1);
		}
		if($("#FORWARD_PERSON_ID_2").val() != "")
		{
			disableForwardFields(2);
		}
		if($("#FORWARD_PERSON_ID_3").val() != "")
		{
			disableForwardFields(3);
		}
		if($("#FORWARD_PERSON_ID_INC_4").val() != "")
		{
			disableForwardFields(4);
		}
		if($("#FORWARD_PERSON_ID_INC_5").val() != "")
		{
			disableForwardFields(5);
		}
		if($("#FORWARD_PERSON_ID_INC_6").val() != "")
		{
			disableForwardFields(6);
		}

		
		
		var selectedGoldenRule = $("#GOLDENRULE").val();
		var goldRuleEl = document.getElementById("GOLDENRULE");
		if(goldRuleEl != null)
		{
			var goldText = goldRuleEl.options[goldRuleEl.selectedIndex].innerHTML;
			if(goldText != 'Other Activities / Equipment')
			{
				$("#row_GOLDENRULEOTHER").hide();
			}
		}
	
		$("#RISK_LIKELIHOOD_ACTUAL").prop('disabled', true);
		$("#RISK_LIKELIHOOD_POTENTIAL").prop('disabled', true);
		$("#RISK_CONSEQUENCE_ACTUAL").prop('disabled', true);
		$("#RISK_CONSEQUENCE_POTENTIAL").prop('disabled', true);
		$("#RISK_RATING_ACTUAL").prop('disabled', true);
		$("#RISK_RATING_POTENTIAL").prop('disabled', true);
	

		var goldenRulePermission = false;
		
 
		if (cats_user_is_administrator == '1' ||
			 cats_user_is_admin_group == '1' ||
			 cats_user_is_super_administrator == '1' ||
			 cats_user_is_root_administrator == '1' ||
			 cats_user_is_incident_privileged == '1' ||
			 cats_user_is_editor == '1' ){
			 
			 goldenRulePermission = true;
			 hasIncidentReportPermission = true;
			 
		}else
		{
			$("#GOLDENRULE").attr("disabled","disabled");
			$("#GOLDENRULEOTHER").attr("disabled","disabled");
		}
		
	
	
		$("#INITIATED_BY_ID").on('change', function() {
		
			var selectedInitiated = $("#INITIATED_BY_ID").val();
			
			for( var i = 0, len = employeeSuperintendentMappingArray.length; i < len; i++ ) {
				var currentVal = employeeSuperintendentMappingArray[i];	
				var empId = currentVal[0];
				var superIntend = currentVal[1];
					
				if( empId == selectedInitiated && superIntend > 0) {
					$("#SUPERINTENDENT_ID").val(superIntend);					
					break;
				}
			}	
		});

		var selectedPRIMARYPERSONText = $("#PRIMARYPERSONID option:selected").text();
		if(selectedPRIMARYPERSONText == 'Visitor, Contractor')
		{
			$("#row_PRIMARY_PERSON_CONTRACTOR_NAME").show();
		}else
		{
			$("#PRIMARY_PERSON_CONTRACTOR_NAME").val("");
			$("#row_PRIMARY_PERSON_CONTRACTOR_NAME").hide();
		}		
			
		$("#PRIMARYPERSONID").on('change', function() {
		
			selectedPRIMARYPERSONText = $("#PRIMARYPERSONID option:selected").text();
			if(selectedPRIMARYPERSONText == 'Visitor, Contractor')
			{
				$("#row_PRIMARY_PERSON_CONTRACTOR_NAME").show();
			}else
			{
				$("#PRIMARY_PERSON_CONTRACTOR_NAME").val("");
				$("#row_PRIMARY_PERSON_CONTRACTOR_NAME").hide();
			}
		});
		
		
		
		$("#GOLDENRULE").on('change', function() {
		
			if(!goldenRulePermission)
			{
				alert("Need higher user permission to set/change Golden Rule.");
				$("#GOLDENRULE").val(selectedGoldenRule);
				return -1;
			}
		
			var selectedGoldenRule = $("#GOLDENRULE").val();
			var goldRuleEl = document.getElementById("GOLDENRULE");
			if(goldRuleEl != null)
			{
				var goldText = goldRuleEl.options[goldRuleEl.selectedIndex].innerHTML;
				//alert('goldText: ' + goldText);
				if(goldText == 'Other Activities / Equipment')
				{
					$("#row_GOLDENRULEOTHER").show();
				}
				else
				{
					$("#row_GOLDENRULEOTHER").hide();
					$("#GOLDENRULEOTHER").val("");
				}
			}
		});
		
		var investigationNr = '<?PHP echo $investigationNumber;?>';
		//alert('investigationNr: ' + investigationNr);
		if(investigationNr > 0)
		{
			$("#btnAddICAM").hide();
		}
		else
		{
			$("#btnEditICAM").hide();
		}
		
		<?php
		if($p=='view'){ 
		?>
			$("#BtnLikelihoodActualOpen").hide();
			$("#BtnConsequenceActualOpen").hide();
			$("#BtnLikelihoodPotentialOpen").hide();
			$("#BtnConsequencePotentialOpen").hide();
		<?php
		} 
		?>		
		
		
		var forwardPersonId1 = $("#FORWARD_PERSON_ID_1").val();
		var forwardPersonId2 = $("#FORWARD_PERSON_ID_2").val();
		var forwardPersonId3 = $("#FORWARD_PERSON_ID_3").val();
		var forwardPersonId4 = $("#FORWARD_PERSON_ID_INC_4").val();
		var forwardPersonId5 = $("#FORWARD_PERSON_ID_INC_5").val();
		if(forwardPersonId1 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_2").hide();
			$("#row_row_incident_signoff2").hide();
			$("#SIGNOFF_PERSON_ID_2_text").hide();
			$("#FORWARD_PERSON_ID_2").hide();
			$("#FORWARD_PERSON_ID_2_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_2").hide();
			$("#btnReject_FORWARD_PERSON_ID_2").hide();
			$("#btnClear_FORWARD_PERSON_ID_2").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_2").hide();
			$("#row_SIGNOFF_DATE_2").hide();
			$("#SIGNOFF_DATE_2_d").hide();
			$("#STATUS_2").hide();
			$("#row_MESSAGE_2").hide();
			$("#MESSAGE_2").hide();
		}
		if(forwardPersonId2 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_3").hide();
			$("#row_row_incident_signoff3").hide();
			$("#SIGNOFF_PERSON_ID_3_text").hide();
			$("#FORWARD_PERSON_ID_3").hide();
			$("#FORWARD_PERSON_ID_3_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_3").hide();
			$("#btnReject_FORWARD_PERSON_ID_3").hide();
			$("#btnClear_FORWARD_PERSON_ID_3").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_3").hide();
			$("#row_SIGNOFF_DATE_3").hide();
			$("#SIGNOFF_DATE_3_d").hide();
			$("#STATUS_3").hide();
			$("#row_MESSAGE_3").hide();
			$("#MESSAGE_3").hide();
		}
		if(forwardPersonId3 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_INC_4").hide();
			$("#row_row_incident_signoff4").hide();
			$("#SIGNOFF_PERSON_ID_INC_4_text").hide();
			$("#FORWARD_PERSON_ID_INC_4").hide();
			$("#FORWARD_PERSON_ID_INC_4_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_INC_4").hide();
			$("#btnReject_FORWARD_PERSON_ID_INC_4").hide();
			$("#btnClear_FORWARD_PERSON_ID_INC_4").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_INC_4").hide();
			$("#row_SIGNOFF_DATE_INC_4").hide();
			$("#SIGNOFF_DATE_INC_4_d").hide();
			$("#STATUS_INC_4").hide();
			$("#row_MESSAGE_INC_4").hide();
			$("#MESSAGE_INC_4").hide();
		}
		if(forwardPersonId4 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_INC_5").hide();
			$("#row_row_incident_signoff5").hide();
			$("#SIGNOFF_PERSON_ID_INC_5_text").hide();
			$("#FORWARD_PERSON_ID_INC_5").hide();
			$("#FORWARD_PERSON_ID_INC_5_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_INC_5").hide();
			$("#btnReject_FORWARD_PERSON_ID_INC_5").hide();
			$("#btnClear_FORWARD_PERSON_ID_INC_5").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_INC_5").hide();
			$("#row_SIGNOFF_DATE_INC_5").hide();
			$("#SIGNOFF_DATE_INC_5_d").hide();
			$("#STATUS_INC_5").hide();
			$("#row_MESSAGE_INC_5").hide();
			$("#MESSAGE_INC_5").hide();
		}
		if(forwardPersonId5 == '')
		{
			$("#row_SIGNOFF_PERSON_ID_INC_6").hide();
			$("#row_row_incident_signoff6").hide();
			$("#SIGNOFF_PERSON_ID_INC_6_text").hide();
			$("#FORWARD_PERSON_ID_INC_6").hide();
			$("#FORWARD_PERSON_ID_INC_6_text").hide();
			$("#btnApprove_FORWARD_PERSON_ID_INC_6").hide();
			$("#btnReject_FORWARD_PERSON_ID_INC_6").hide();
			$("#btnClear_FORWARD_PERSON_ID_INC_6").hide();
			$("#btnSaveSend_FORWARD_PERSON_ID_INC_6").hide();
			$("#row_SIGNOFF_DATE_INC_6").hide();
			$("#SIGNOFF_DATE_INC_6_d").hide();
			$("#STATUS_INC_6").hide();
			$("#row_MESSAGE_INC_6").hide();
			$("#MESSAGE_INC_6").hide();
		}
		
	});		

	function disableForwardFields(level)
	{
		if(level == 1)
		{
			$("#FORWARD_PERSON_ID_1_text").attr("disabled","true"); 
			$("#FORWARD_PERSON_ID_1_btn").attr("disabled","true"); 
			$("#MESSAGE_1").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_1_text").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_1_btn").attr("disabled","true"); 
			
			$("#btnApprove_FORWARD_PERSON_ID_1").attr("disabled","true"); 
			$("#btnReject_FORWARD_PERSON_ID_1").attr("disabled","true"); 
			
			$("#SIGNOFF_DATE_1_d").attr("disabled","true");
		}
		else if(level == 2)
		{
			$("#FORWARD_PERSON_ID_2_text").attr("disabled","true"); 
			$("#FORWARD_PERSON_ID_2_btn").attr("disabled","true"); 
			$("#MESSAGE_2").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_2_text").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_2_btn").attr("disabled","true"); 
			
			$("#btnApprove_FORWARD_PERSON_ID_2").attr("disabled","true"); 
			$("#btnReject_FORWARD_PERSON_ID_2").attr("disabled","true"); 
			$("#SIGNOFF_DATE_2_d").attr("disabled","true");
		}			
		else if(level == 3)
		{
			$("#FORWARD_PERSON_ID_3_text").attr("disabled","true"); 
			$("#FORWARD_PERSON_ID_3_btn").attr("disabled","true"); 
			$("#MESSAGE_3").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_3_text").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_3_btn").attr("disabled","true"); 
			
			$("#btnApprove_FORWARD_PERSON_ID_3").attr("disabled","true"); 
			$("#btnReject_FORWARD_PERSON_ID_3").attr("disabled","true"); 
			$("#SIGNOFF_DATE_3_d").attr("disabled","true");
		}			
		else if(level == 4)
		{
			$("#FORWARD_PERSON_ID_INC_4_text").attr("disabled","true"); 
			$("#FORWARD_PERSON_ID_INC_4_btn").attr("disabled","true"); 
			$("#MESSAGE_INC_4").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_INC_4_text").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_INC_4_btn").attr("disabled","true"); 
			
			$("#btnApprove_FORWARD_PERSON_ID_INC_4").attr("disabled","true"); 
			$("#btnReject_FORWARD_PERSON_ID_INC_4").attr("disabled","true"); 
			$("#SIGNOFF_DATE_INC_4_d").attr("disabled","true");
		}			
		else if(level == 5)
		{
			$("#FORWARD_PERSON_ID_INC_5_text").attr("disabled","true"); 
			$("#FORWARD_PERSON_ID_INC_5_btn").attr("disabled","true"); 
			$("#MESSAGE_INC_5").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_INC_5_text").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_INC_5_btn").attr("disabled","true"); 
			
			$("#btnApprove_FORWARD_PERSON_ID_INC_5").attr("disabled","true"); 
			$("#btnReject_FORWARD_PERSON_ID_INC_5").attr("disabled","true"); 
			$("#SIGNOFF_DATE_INC_5_d").attr("disabled","true");
		}			
		else if(level == 6)
		{
			$("#FORWARD_PERSON_ID_INC_6_text").attr("disabled","true"); 
			$("#FORWARD_PERSON_ID_INC_6_btn").attr("disabled","true"); 
			$("#MESSAGE_INC_6").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_INC_6_text").attr("disabled","true"); 
			$("#SIGNOFF_PERSON_ID_INC_6_btn").attr("disabled","true"); 
			
			$("#btnApprove_FORWARD_PERSON_ID_INC_6").attr("disabled","true"); 
			$("#btnReject_FORWARD_PERSON_ID_INC_6").attr("disabled","true"); 
			$("#SIGNOFF_DATE_INC_6_d").attr("disabled","true");
		}			

		
	}	

	function setLikelihoodActual(likelihood)
	{
		//alert('Likelihood: ' + estimation); RISK_LIKELIHOOD_ACTUAL
		
		$("#RISK_LIKELIHOOD_ACTUAL").val(likelihood);
		setRiskRating(likelihood, $("#RISK_CONSEQUENCE_ACTUAL").val(), "ACTUAL");
		$("#BthLikelihoodActualClose").click();
	}

	function setConsequenceActual(consequence)
	{
		//alert('setConsequenceActual: ' + consequence);
		$("#RISK_CONSEQUENCE_ACTUAL").val(consequence);
		setRiskRating($("#RISK_LIKELIHOOD_ACTUAL").val(), consequence, "ACTUAL");
		$("#BthConsequenceCloseActual").click();
	}
	
	function setLikelihoodPotential(likelihood)
	{
		//alert('Likelihood: ' + estimation); 
		
		$("#RISK_LIKELIHOOD_POTENTIAL").val(likelihood);
		setRiskRating(likelihood, $("#RISK_CONSEQUENCE_POTENTIAL").val(), "POTENTIAL");
		$("#BthLikelihoodPotentialClose").click();
	}

	function setConsequencePotential(consequence)
	{
		//alert('setConsequencePotential: ' + consequence);
		$("#RISK_CONSEQUENCE_POTENTIAL").val(consequence);
		setRiskRating($("#RISK_LIKELIHOOD_POTENTIAL").val(), consequence, "POTENTIAL");
		$("#BthConsequenceClosePotential").click();
	}
	
	
	function setRiskRating(likelihood, consequence, type)
	{
		//alert('setRiskRating: ' + likelihood + ' ' + consequence + ' ' + type);
		if(likelihood != "" && likelihood != null && consequence != "" && consequence != null)
		{
			if(likelihood == "A - Almost Certain")
			{ 
				if(consequence == "5 - Catastrophic")
					setVeryHigh(type, 'A5');
				if(consequence == "4 - Major")
					setVeryHigh(type, 'A4');
				if(consequence == "3 - Moderate")
					setHigh(type, 'A3');
				if(consequence == "2 - Minor")
					setHigh(type, 'A2');
				if(consequence == "1 - Insignificant")
					setMedium(type, 'A1');
			}
			else if(likelihood == "B - Likely")
			{ 
				if(consequence == "5 - Catastrophic")
					setVeryHigh(type, 'B5');
				if(consequence == "4 - Major")
					setHigh(type, 'B4');
				if(consequence == "3 - Moderate")
					setHigh(type, 'B3');
				if(consequence == "2 - Minor")
					setMedium(type, 'B2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'B1');
			}
			else if(likelihood == "C - Moderate")
			{ 
				if(consequence == "5 - Catastrophic")
					setHigh(type, 'C5');
				if(consequence == "4 - Major")
					setHigh(type, 'C4');
				if(consequence == "3 - Moderate")
					setMedium(type, 'C3');
				if(consequence == "2 - Minor")
					setMedium(type, 'C2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'C1');
			}
			else if(likelihood == "D - Unlikely")
			{ 
				if(consequence == "5 - Catastrophic")
					setHigh(type, 'D5');
				if(consequence == "4 - Major")
					setMedium(type, 'D4');
				if(consequence == "3 - Moderate")
					setMedium(type, 'D3');
				if(consequence == "2 - Minor")
					setLow(type, 'D2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'D1');
			}
			else if(likelihood == "E - Rare")
			{ 
				if(consequence == "5 - Catastrophic")
					setMedium(type, 'E5');
				if(consequence == "4 - Major")
					setMedium(type, 'E4', '');
				if(consequence == "3 - Moderate")
					setMedium(type, 'E3');
				if(consequence == "2 - Minor")
					setLow(type, 'E2');
				if(consequence == "1 - Insignificant")
					setLow(type, 'E1');
			}
		
		}
	}
	
	function setVeryHigh(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - Very High");
			$("#RISK_RATING_ACTUAL").removeClass("textHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textMedium");
			$("#RISK_RATING_ACTUAL").removeClass("textLow");
			$("#RISK_RATING_ACTUAL").addClass("textVeryHigh");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - Very High");
			$("#RISK_RATING_POTENTIAL").removeClass("textHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textMedium");
			$("#RISK_RATING_POTENTIAL").removeClass("textLow");
			$("#RISK_RATING_POTENTIAL").addClass("textVeryHigh");
			$("#INITIALRISKLEVEL").val("446"); // Code value
		}
	}
	
	function setHigh(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - High");
			$("#RISK_RATING_ACTUAL").removeClass("textVeryHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textMedium");
			$("#RISK_RATING_ACTUAL").removeClass("textLow");
			$("#RISK_RATING_ACTUAL").addClass("textHigh");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - High");
			$("#RISK_RATING_POTENTIAL").removeClass("textVeryHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textMedium");
			$("#RISK_RATING_POTENTIAL").removeClass("textLow");
			$("#RISK_RATING_POTENTIAL").addClass("textHigh");
			$("#INITIALRISKLEVEL").val("445"); // Code value
		}
	}
	
	function setMedium(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - Medium");
			$("#RISK_RATING_ACTUAL").removeClass("textVeryHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textLow");
			$("#RISK_RATING_ACTUAL").addClass("textMedium");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - Medium");
			$("#RISK_RATING_POTENTIAL").removeClass("textVeryHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textLow");
			$("#RISK_RATING_POTENTIAL").addClass("textMedium");
			$("#INITIALRISKLEVEL").val("444"); // Code value
			//alert('INITIALRISKLEVEL: ' + $("#INITIALRISKLEVEL").val());
		}
	}
	
	function setLow(type, level)
	{
		if(type == "ACTUAL")
		{
			$("#RISK_RATING_ACTUAL").val(level + " - Low");
			$("#RISK_RATING_ACTUAL").removeClass("textVeryHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textHigh");
			$("#RISK_RATING_ACTUAL").removeClass("textMedium");
			$("#RISK_RATING_ACTUAL").addClass("textLow");
		}
		else if(type == "POTENTIAL")
		{
			$("#RISK_RATING_POTENTIAL").val(level + " - Low");
			$("#RISK_RATING_POTENTIAL").removeClass("textVeryHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textHigh");
			$("#RISK_RATING_POTENTIAL").removeClass("textMedium");
			$("#RISK_RATING_POTENTIAL").addClass("textLow");
			$("#INITIALRISKLEVEL").val("443"); // Code value
		}
	}
	
	function deletePhoto(photoNumber)           
	{
		// 1. Set form values for FormDeletePhoto 
		$("#PHOTO_NUMBER").val(photoNumber);  
		//alert('Delete photoNumber: ' + $("#PHOTO_NUMBER").val());
		// 2. Post to deletephoto.php and come back to this page
		$("#FormDeletePhoto").submit();
		
	}
	
	function preSaveForm()
	{
		//Consequence Ranking---------------------------
		$("#RISK_LIKELIHOOD_ACTUAL").prop('disabled', false);
		$("#RISK_LIKELIHOOD_POTENTIAL").prop('disabled', false);
		$("#RISK_CONSEQUENCE_ACTUAL").prop('disabled', false);
		$("#RISK_CONSEQUENCE_POTENTIAL").prop('disabled', false);
		$("#RISK_RATING_ACTUAL").prop('disabled', false);
		$("#RISK_RATING_POTENTIAL").prop('disabled', false);
		
		var selectedPRIMARYPERSONText = $("#PRIMARYPERSONID option:selected").text();
		
		if(selectedPRIMARYPERSONText == 'Visitor, Contractor' && $("#PRIMARY_PERSON_CONTRACTOR_NAME").val() == "")
		{
			alert("Please set Visitor / Contractors name for the Person Involved.");
			return;
		}
		
		var forwardPersonId1Current = $("#FORWARD_PERSON_ID_1").val();
		var forwardPersonId2Current = $("#FORWARD_PERSON_ID_2").val();
		var forwardPersonId3Current = $("#FORWARD_PERSON_ID_3").val();
		var forwardPersonId4Current = $("#FORWARD_PERSON_ID_INC_4").val();
		var forwardPersonId5Current = $("#FORWARD_PERSON_ID_INC_5").val();
		var forwardPersonId6Current = $("#FORWARD_PERSON_ID_INC_6").val();
		
		var hasForward1Changed = (forwardPersonId1Current != forwardPersonId1Onload);
		var hasForward2Changed = (forwardPersonId2Current != forwardPersonId2Onload);
		var hasForward3Changed = (forwardPersonId3Current != forwardPersonId3Onload);
		var hasForward4Changed = (forwardPersonId4Current != forwardPersonId4Onload);
		var hasForward5Changed = (forwardPersonId5Current != forwardPersonId5Onload);
		var hasForward6Changed = (forwardPersonId6Current != forwardPersonId6Onload);
		var signoffBy = "";
		var signoffDate = "";
		var forwardTo = "";
		var msg = "";
		
		//alert("changes: " + hasForward1Changed + " " + hasForward2Changed + " " + hasForward3Changed);
		
		if(hasForward1Changed)
		{
			forwardTo = $("#FORWARD_PERSON_ID_1").val();
			if(forwardTo != "")
			{
				signoffBy = $("#SIGNOFF_PERSON_ID_1").val();
				$("#DO_EMAIL_FLAG_1").val("True");
				$("#WFL_STAGE").val("1");			
				signoffDate = $("#SIGNOFF_DATE_1").val();
				msg = $("#MESSAGE_1").val(); 
			}				
		}
		if(hasForward2Changed)
		{
			forwardTo = $("#FORWARD_PERSON_ID_2").val();
			if(forwardTo != "")
			{
				$("#DO_EMAIL_FLAG_2").val("True");
				$("#WFL_STAGE").val("2");			
				signoffBy = $("#SIGNOFF_PERSON_ID_2").val();
				signoffDate = $("#SIGNOFF_DATE_2").val();
				msg = $("#MESSAGE_2").val(); 	
			}				
		}
		if(hasForward3Changed)
		{
			forwardTo = $("#FORWARD_PERSON_ID_3").val();
			if(forwardTo != "")
			{
				$("#DO_EMAIL_FLAG_3").val("True");
				$("#WFL_STAGE").val("3");
				signoffBy = $("#SIGNOFF_PERSON_ID_3").val();
				signoffDate = $("#SIGNOFF_DATE_3").val();
				msg = $("#MESSAGE_3").val(); 			
			}
		}
		if(hasForward4Changed)
		{
			forwardTo = $("#FORWARD_PERSON_ID_INC_4").val();
			if(forwardTo != "")
			{
				$("#DO_EMAIL_FLAG_INC_4").val("True");
				$("#WFL_STAGE").val("4");
				signoffBy = $("#SIGNOFF_PERSON_ID_INC_4").val();
				signoffDate = $("#SIGNOFF_DATE_INC_4").val();
				msg = $("#MESSAGE_INC_4").val(); 			
			}
		}
		if(hasForward5Changed)
		{
			forwardTo = $("#FORWARD_PERSON_ID_INC_5").val();
			if(forwardTo != "")
			{
				$("#DO_EMAIL_FLAG_INC_5").val("True");
				$("#WFL_STAGE").val("5");
				signoffBy = $("#SIGNOFF_PERSON_ID_INC_5").val();
				signoffDate = $("#SIGNOFF_DATE_INC_5").val();
				msg = $("#MESSAGE_INC_5").val(); 			
			}
		}
		if(hasForward6Changed)
		{
			forwardTo = $("#FORWARD_PERSON_ID_INC_6").val();
			if(forwardTo != "")
			{
				$("#DO_EMAIL_FLAG_INC_6").val("True");
				$("#WFL_STAGE").val("6");
				signoffBy = $("#SIGNOFF_PERSON_ID_INC_6").val();
				signoffDate = $("#SIGNOFF_DATE_INC_6").val();
				msg = $("#MESSAGE_INC_6").val(); 			
			}
		}
        
        if(forwardTo != "")
		{
			$("#SIGN_OFF_BY").val(signoffBy);
			$("#SIGN_OFF_DATE_TIME").val(signoffDate);
			$("#FORWARD_TO").val(forwardTo);
			$("#MESSAGE").val(msg);		
		}		
		
		
		setHiddenDateTime("INCIDENT_DATE");
		setHiddenDateTime("REPORT_DATE");
		setHiddenDateTime("SIGNOFF_DATE_1");
		setHiddenDateTime("SIGNOFF_DATE_2");
		setHiddenDateTime("SIGNOFF_DATE_3");
		setHiddenDateTime("SIGNOFF_DATE_INC_4");
		setHiddenDateTime("SIGNOFF_DATE_INC_5");
		setHiddenDateTime("SIGNOFF_DATE_INC_6");
		
		// Blank out the date if the workflow is not set.
		if($("#STATUS_1").val() == '')
			$("#SIGNOFF_DATE_1").val('');	
		if($("#STATUS_2").val() == '')
			$("#SIGNOFF_DATE_2").val('');	
		if($("#STATUS_3").val() == '')
			$("#SIGNOFF_DATE_3").val('');	
		if($("#STATUS_INC_4").val() == '')
			$("#SIGNOFF_DATE_INC_4").val('');	
		if($("#STATUS_INC_5").val() == '')
			$("#SIGNOFF_DATE_INC_5").val('');	
		if($("#STATUS_INC_6").val() == '')
			$("#SIGNOFF_DATE_INC_6").val('');	
	
	
		document.forms["editIncidentForm"].submit();
		
		
	}	
	
	function doUploadPhoto()
	{
		document.forms["FormPhoto"].submit();
	}	
	
	function showInReport(photoNumber, checkbox)
	{	
		if(checkbox.checked)
		{
			countOfSelectedImagesInReport ++;
		}
		else
		{
			countOfSelectedImagesInReport --;
		}
		if(countOfSelectedImagesInReport > 2)
		{
			alert("CATS has been defined to show two reports in the Incident Report. Please un-select another report before selecting more.");
			checkbox.checked = false;
			countOfSelectedImagesInReport --;
		}
	}	

	function saveAndSend()
	{
		$("#saveButton").click();
	}
	
	function clearWFLfields(field)
	{
		IS_FIELD_CHANGED = true;
		
		if(field == 'FORWARD_PERSON_ID_1')
		{
			
			
			$("#SIGNOFF_PERSON_ID_1").val("");
			$("#SIGNOFF_PERSON_ID_1_text").val("");
			$("#SIGNOFF_DATE_1").val("");
			$("#FORWARD_PERSON_ID_1_text").val("");
			$("#FORWARD_PERSON_ID_1").val("");
			$("#MESSAGE_1").val("");
			$("#STATUS_1").val("");
			$("#STATUS_1").removeClass("textLow");
			$("#STATUS_1").removeClass("textVeryHigh");

			$("#FORWARD_PERSON_ID_1_text").prop("disabled",false); 
			$("#FORWARD_PERSON_ID_1_btn").prop("disabled",false);
			$("#MESSAGE_1").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_1_text").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_1_btn").prop("disabled",false);
			$("#btnApprove_FORWARD_PERSON_ID_1").prop("disabled",false); 
			$("#btnReject_FORWARD_PERSON_ID_1").prop("disabled",false);	
			

		} 
		else if(field == 'FORWARD_PERSON_ID_2')
		{
			$("#SIGNOFF_PERSON_ID_2").val("");
			$("#SIGNOFF_PERSON_ID_2_text").val("");
			$("#SIGNOFF_DATE_2").val("");
			$("#FORWARD_PERSON_ID_2_text").val("");
			$("#FORWARD_PERSON_ID_2").val("");
			$("#MESSAGE_2").val("");
			$("#STATUS_2").val("");
			$("#STATUS_2").removeClass("textLow");
			$("#STATUS_2").removeClass("textVeryHigh");
			
			$("#FORWARD_PERSON_ID_2_text").prop("disabled",false); 
			$("#FORWARD_PERSON_ID_2_btn").prop("disabled",false);
			$("#MESSAGE_2").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_2_text").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_2_btn").prop("disabled",false);
			$("#btnApprove_FORWARD_PERSON_ID_2").prop("disabled",false); 
			$("#btnReject_FORWARD_PERSON_ID_2").prop("disabled",false);	
			
		} 
		else if(field == 'FORWARD_PERSON_ID_3')
		{
			$("#SIGNOFF_PERSON_ID_3").val("");
			$("#SIGNOFF_PERSON_ID_3_text").val("");
			$("#SIGNOFF_DATE_3").val("");
			$("#FORWARD_PERSON_ID_3_text").val("");
			$("#FORWARD_PERSON_ID_3").val("");
			$("#MESSAGE_3").val("");
			$("#STATUS_3").val("");
			$("#STATUS_3").removeClass("textLow");
			$("#STATUS_3").removeClass("textVeryHigh");
			
			$("#FORWARD_PERSON_ID_3_text").prop("disabled",false); 
			$("#FORWARD_PERSON_ID_3_btn").prop("disabled",false);
			$("#MESSAGE_3").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_3_text").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_3_btn").prop("disabled",false);
			$("#btnApprove_FORWARD_PERSON_ID_3").prop("disabled",false); 
			$("#btnReject_FORWARD_PERSON_ID_3").prop("disabled",false);	
			
		} 
		else if(field == 'FORWARD_PERSON_ID_INC_4')
		{
			$("#SIGNOFF_PERSON_ID_INC_4").val("");
			$("#SIGNOFF_PERSON_ID_INC_4_text").val("");
			$("#SIGNOFF_DATE_INC_4").val("");
			$("#FORWARD_PERSON_ID_INC_4_text").val("");
			$("#FORWARD_PERSON_ID_INC_4").val("");
			$("#MESSAGE_INC_4").val("");
			$("#STATUS_INC_4").val("");
			$("#STATUS_INC_4").removeClass("textLow");
			$("#STATUS_INC_4").removeClass("textVeryHigh");
			
			$("#FORWARD_PERSON_ID_INC_4_text").prop("disabled",false); 
			$("#FORWARD_PERSON_ID_INC_4_btn").prop("disabled",false);
			$("#MESSAGE_INC_4").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_INC_4_text").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_INC_4_btn").prop("disabled",false);
			$("#btnApprove_FORWARD_PERSON_ID_INC_4").prop("disabled",false); 
			$("#btnReject_FORWARD_PERSON_ID_INC_4").prop("disabled",false);	
			
		} 
		else if(field == 'FORWARD_PERSON_ID_INC_5')
		{
			$("#SIGNOFF_PERSON_ID_INC_5").val("");
			$("#SIGNOFF_PERSON_ID_INC_5_text").val("");
			$("#SIGNOFF_DATE_INC_5").val("");
			$("#FORWARD_PERSON_ID_INC_5_text").val("");
			$("#FORWARD_PERSON_ID_INC_5").val("");
			$("#MESSAGE_INC_5").val("");
			$("#STATUS_INC_5").val("");
			$("#STATUS_INC_5").removeClass("textLow");
			$("#STATUS_INC_5").removeClass("textVeryHigh");
			
			$("#FORWARD_PERSON_ID_INC_5_text").prop("disabled",false); 
			$("#FORWARD_PERSON_ID_INC_5_btn").prop("disabled",false);
			$("#MESSAGE_INC_5").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_INC_5_text").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_INC_5_btn").prop("disabled",false);
			$("#btnApprove_FORWARD_PERSON_ID_INC_5").prop("disabled",false); 
			$("#btnReject_FORWARD_PERSON_ID_INC_5").prop("disabled",false);	
			
		} 
		else if(field == 'FORWARD_PERSON_ID_INC_6')
		{
			$("#SIGNOFF_PERSON_ID_INC_6").val("");
			$("#SIGNOFF_PERSON_ID_INC_6_text").val("");
			$("#SIGNOFF_DATE_INC_6").val("");
			$("#FORWARD_PERSON_ID_INC_6_text").val("");
			$("#FORWARD_PERSON_ID_INC_6").val("");
			$("#MESSAGE_INC_6").val("");
			$("#STATUS_INC_6").val("");
			$("#STATUS_INC_6").removeClass("textLow");
			$("#STATUS_INC_6").removeClass("textVeryHigh");
			
			$("#FORWARD_PERSON_ID_INC_6_text").prop("disabled",false); 
			$("#FORWARD_PERSON_ID_INC_6_btn").prop("disabled",false);
			$("#MESSAGE_INC_6").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_INC_6_text").prop("disabled",false);
			$("#SIGNOFF_PERSON_ID_INC_6_btn").prop("disabled",false);
			$("#btnApprove_FORWARD_PERSON_ID_INC_6").prop("disabled",false); 
			$("#btnReject_FORWARD_PERSON_ID_INC_6").prop("disabled",false);	
			
		} 

	}

	function isWorkflowStepValid(signoffBy, forwardTo, msg)
	{
		if(signoffBy == "")
		{
			alert("Please set Sign-off Incident Report By");
			return false;
		}
		if(forwardTo == "")
		{
			alert("Please set Forward to");
			return false;
		}
		if(msg == "")
		{
			alert("Please set the Approval/Rejection message");
			return false;
		}
		return true;
	}

	function submitReject(field)
	{
	
		var signoffBy = "";
		var signoffDate = "";
		var forwardTo = "";
		var msg = "";
		
		if(field == 'FORWARD_PERSON_ID_1')
		{
			$("#WFL_STAGE").val("1");
			signoffBy = $("#SIGNOFF_PERSON_ID_1").val();
			signoffDate = $("#SIGNOFF_DATE_1").val();
			forwardTo = $("#FORWARD_PERSON_ID_1").val();
			msg = $("#MESSAGE_1").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_1").val("REJECTED");
				$("#STATUS_1").removeClass("textLow");
				$("#STATUS_1").addClass("textVeryHigh");
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_1").val("True");
//				}
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_2')
		{
			$("#WFL_STAGE").val("2");
			signoffBy = $("#SIGNOFF_PERSON_ID_2").val();
			signoffDate = $("#SIGNOFF_DATE_2").val();
			forwardTo = $("#FORWARD_PERSON_ID_2").val();
			msg = $("#MESSAGE_2").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_2").val("REJECTED");
				$("#STATUS_2").removeClass("textLow");
				$("#STATUS_2").addClass("textVeryHigh");				
//				if(forwardTo != "")
//					$("#DO_EMAIL_FLAG_2").val("True");
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_3')
		{
			$("#WFL_STAGE").val("3");
			signoffBy = $("#SIGNOFF_PERSON_ID_3").val();
			signoffDate = $("#SIGNOFF_DATE_3").val();
			forwardTo = $("#FORWARD_PERSON_ID_3").val();
			msg = $("#MESSAGE_3").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_3").val("REJECTED");
				$("#STATUS_3").removeClass("textLow");
				$("#STATUS_3").addClass("textVeryHigh");				
//				if(forwardTo != "")
//					$("#DO_EMAIL_FLAG_3").val("True");
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_INC_4')
		{
			$("#WFL_STAGE").val("4");
			signoffBy = $("#SIGNOFF_PERSON_ID_INC_4").val();
			signoffDate = $("#SIGNOFF_DATE_INC_4").val();
			forwardTo = $("#FORWARD_PERSON_ID_INC_4").val();
			msg = $("#MESSAGE_INC_4").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_INC_4").val("REJECTED");
				$("#STATUS_INC_4").removeClass("textLow");
				$("#STATUS_INC_4").addClass("textVeryHigh");				
//				if(forwardTo != "")
//					$("#DO_EMAIL_FLAG_4").val("True");
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_INC_5')
		{
			$("#WFL_STAGE").val("5");
			signoffBy = $("#SIGNOFF_PERSON_ID_INC_5").val();
			signoffDate = $("#SIGNOFF_DATE_INC_5").val();
			forwardTo = $("#FORWARD_PERSON_ID_INC_5").val();
			msg = $("#MESSAGE_INC_5").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_INC_5").val("REJECTED");
				$("#STATUS_INC_5").removeClass("textLow");
				$("#STATUS_INC_5").addClass("textVeryHigh");				
//				if(forwardTo != "")
//					$("#DO_EMAIL_FLAG_5").val("True");
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_INC_6')
		{
			$("#WFL_STAGE").val("6");
			signoffBy = $("#SIGNOFF_PERSON_ID_INC_6").val();
			signoffDate = $("#SIGNOFF_DATE_INC_6").val();
			forwardTo = $("#FORWARD_PERSON_ID_INC_6").val();
			msg = $("#MESSAGE_INC_6").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_INC_6").val("REJECTED");
				$("#STATUS_INC_6").removeClass("textLow");
				$("#STATUS_INC_6").addClass("textVeryHigh");				
//				if(forwardTo != "")
//					$("#DO_EMAIL_FLAG_6").val("True");
			}
		}
		$("#APPROVAL_STATUS").val("REJECTED");
		$("#SIGN_OFF_BY").val(signoffBy);
		$("#SIGN_OFF_DATE_TIME").val(signoffDate);
		$("#FORWARD_TO").val(forwardTo);
		$("#MESSAGE").val(msg);


	}
	
	
	function submitApprove(field)
	{
	
		var signoffBy = "";
		var signoffDate = "";
		var forwardTo = "";
		var msg = "";
		
		if(field == 'FORWARD_PERSON_ID_1')
		{
			$("#WFL_STAGE").val("1");
			signoffBy = $("#SIGNOFF_PERSON_ID_1").val();
			signoffDate = $("#SIGNOFF_DATE_1").val();
			forwardTo = $("#FORWARD_PERSON_ID_1").val();
			msg = $("#MESSAGE_1").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_1").val("APPROVED");
				$("#STATUS_1").removeClass("textVeryHigh");
				$("#STATUS_1").addClass("textLow");				
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_1").val("True");
//				}
				
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_2')
		{
			$("#WFL_STAGE").val("2");
			signoffBy = $("#SIGNOFF_PERSON_ID_2").val();
			signoffDate = $("#SIGNOFF_DATE_2").val();
			forwardTo = $("#FORWARD_PERSON_ID_2").val();
			msg = $("#MESSAGE_2").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_2").val("APPROVED");
				$("#STATUS_2").removeClass("textVeryHigh");
				$("#STATUS_2").addClass("textLow");				
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_2").val("True");
//				}
			}
		} 
		else if(field == 'FORWARD_PERSON_ID_3')
		{
			$("#WFL_STAGE").val("3");
			signoffBy = $("#SIGNOFF_PERSON_ID_3").val();
			signoffDate = $("#SIGNOFF_DATE_3").val();
			forwardTo = $("#FORWARD_PERSON_ID_3").val();
			msg = $("#MESSAGE_3").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_3").val("APPROVED");
				$("#STATUS_3").removeClass("textVeryHigh");
				$("#STATUS_3").addClass("textLow");
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_3").val("True");
//				}
			}
		}
		else if(field == 'FORWARD_PERSON_ID_INC_4')
		{
			$("#WFL_STAGE").val("4");
			signoffBy = $("#SIGNOFF_PERSON_ID_INC_4").val();
			signoffDate = $("#SIGNOFF_DATE_INC_4").val();
			forwardTo = $("#FORWARD_PERSON_ID_INC_4").val();
			msg = $("#MESSAGE_INC_4").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_INC_4").val("APPROVED");
				$("#STATUS_INC_4").removeClass("textVeryHigh");
				$("#STATUS_INC_4").addClass("textLow");
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_INC_4").val("True");
//				}
			}
		}
		else if(field == 'FORWARD_PERSON_ID_INC_5')
		{
			$("#WFL_STAGE").val("5");
			signoffBy = $("#SIGNOFF_PERSON_ID_INC_5").val();
			signoffDate = $("#SIGNOFF_DATE_INC_5").val();
			forwardTo = $("#FORWARD_PERSON_ID_INC_5").val();
			msg = $("#MESSAGE_INC_5").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_INC_5").val("APPROVED");
				$("#STATUS_INC_5").removeClass("textVeryHigh");
				$("#STATUS_INC_5").addClass("textLow");
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_INC_5").val("True");
//				}
			}
		}
		else if(field == 'FORWARD_PERSON_ID_INC_6')
		{
			$("#WFL_STAGE").val("6");
			signoffBy = $("#SIGNOFF_PERSON_ID_INC_6").val();
			signoffDate = $("#SIGNOFF_DATE_INC_6").val();
			forwardTo = $("#FORWARD_PERSON_ID_INC_6").val();
			msg = $("#MESSAGE_INC_6").val();
			if(isWorkflowStepValid(signoffBy, forwardTo, msg))
			{
				$("#STATUS_INC_6").val("APPROVED");
				$("#STATUS_INC_6").removeClass("textVeryHigh");
				$("#STATUS_INC_6").addClass("textLow");
//				if(forwardTo != "")
//				{
//					$("#DO_EMAIL_FLAG_INC_6").val("True");
//				}
			}
		}
		
		$("#APPROVAL_STATUS").val("APPROVED");
		$("#SIGN_OFF_BY").val(signoffBy);
		$("#SIGN_OFF_DATE_TIME").val(signoffDate);
		$("#FORWARD_TO").val(forwardTo);
		$("#MESSAGE").val(msg);

		// TEST:	
		var wflstage = $("#WFL_STAGE").val()
		var wflstatus = $("#APPROVAL_STATUS").val()
		signoffBy = $("#SIGN_OFF_BY").val()
		signoffDate = $("#SIGN_OFF_DATE_TIME").val()
		forwardTo = $("#FORWARD_TO").val();
		msg = $("#MESSAGE").val();
		
		//alert('Reject: stage: ' + wflstage + ' wflstatus: ' + wflstatus + ' signoffBy: ' + signoffBy + ' Date: ' + signoffDate + ' ForwardTo: ' + forwardTo + ' msg: ' + msg);
		
		//$("#ApproveRejectForm").submit();
		
	}	

	
<?php

	$sql = "Select count(1) From Tblincident_Photos Where Investigation_Number = " . $id . " and show_in_report='Y' ";
	$countOfSelectedImagesInReport = db_get_one($sql);
	
?>	

	var countOfSelectedImagesInReport = '<?php echo($countOfSelectedImagesInReport);?>';
	
	function showDate()
	{
		var repDate1 = $("#INCIDENT_DATE").val();
		var repDate2 = $("#INCIDENT_DATE_d").val();
		alert("ReportDates: " + repDate1 + " " + repDate2);
		if(isset($_SESSION['SQL_DEBUG']))alert("SQL: <?php echo($_SESSION['SQL_DEBUG']); ?>");
	}
	
	function selectTab(theTab)
	{
		if(IS_FIELD_CHANGED)
		{
			
			$( function() {
				$( "#dialog-confirm" ).dialog({
				  resizable: false,
				  height: "auto",
				  width: 250,
				  modal: true,
				  buttons: {
					"Yes": function() {
					  preSaveForm();
					},
					"No": function() {
					  IS_FIELD_CHANGED = false;
					  tab_onclick(theTab);
					  $( this ).dialog( "close" );
					},
					Cancel: function() {
					  $( this ).dialog( "close" );
					}
				  }
				});
			  } );			
		}
		else
		{
			tab_onclick(theTab);
		}
		
	}
	
</script>


