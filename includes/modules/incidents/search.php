<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$iTabs=0;
?>
<link rel="stylesheet" href="<?php echo WS_STYLE_PATH;?>forms.css" type="text/css" />
<link rel="stylesheet" href="<?php echo WS_STYLE_PATH;?>tabs.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/incidents.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
var PAGE = "<?php echo $p;?>";
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
window.onload = init;
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

function get_incidents(itm, value) {
	//only do if something is entered in the filter box
	if(value != "") {
		var sNumber = value.substring(0,value.len);
		//go and get the incidents
		var obj={
			'a':'get_alpha_incidents_array',
			'p':sNumber
		};
		var e = remote.get_sync("incidents", obj);
		var arr = e.options;
		var opt, name, val;
		var g = arr.length;
		itm.options.length = 0; //remove options
		if (g<2) { //no data so we add a blank option
			opt = new Option("","");
			itm.options[0] = opt;
		} else { //only do this is we have some incidents
			var len = value.length;
			var search_data = value.substring(0,len);
			var selected = false;
			var i=0;
			for (i=0; i<g; i++) {
				// add the incidents to the drop down box
				txt = arr[i][1];
				val = arr[i][0];
				opt = new Option(txt, val);
				itm.options[i] = opt;
				//highlight the required option
				if (selected == false) {
					len = value.length;
					search_data = txt.substring(0,len);
					if (value == search_data) {
						itm.options[i].selected = true;
						selected = true;
					}
				}
			}
		}
	}
}

function change_incident(itm,to){
	to.value = itm.options[itm.selectedIndex].value;
}

</script>
</head>
<body><form action="index.php?m=<?php echo $m;?>&p=results" target="<?php echo $results_iframe_target;?>" method="post">	
<?php if(cats_user_is_incident_analysis() || cats_user_is_editor(true) || cats_user_is_editor(false)){ ?>
<input type="button" class="button" name="cats::new" value="New Incident" onClick="_m.newModule()">
<?php } ?>

<?php echo html_draw_hidden_field('cats::report_fields',''); ?>
<table width="100%">
<tr valign="top">
	<td width="100%">		

<!-- BEGIN:: Search Table -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" class="search" onClick="preSeachForm(); toggle_search()"><?php echo $GENERAL['name'];?></a></caption>
<tr>
	<td>
		<!-- <div  id="search_form" style="display:<?php echo $search_display;?>"> -->
			<!-- <div  id="search_form" style="display:<?php  echo isset($search_display)?$search_display:'block';?>"> -->
			<div  id="search_form" ><!--  after discussion  -->
			<fieldset class="tbar" style="text-align:right; "><a name="todo" style="display:block;float:left;font: bold 13px Tahoma, Verdana; padding: 3px;">General - Welcome to the general search/filter page. Use the options below to refine your search or for all records click on the search button below.</a></fieldset>	

<?php
// Create the search form
$ed=new Editor("edit","admin");
echo($ed->buildForm($FIELDS));
?>

<fieldset class="tbar" style="text-align:right; ">
<?php
if(cats_user_is_incident_privileged() || cats_user_is_incident_analysis()){
?>
<a name="todo" style="display:block;float:left;font: bold 13px Tahoma, Verdana; padding: 3px;">Incident Analysis</a>
<?php
}
?>
<input type="reset" class="reset" value="Reset" /><input type="submit" class="submit" name="cats::search" value="Search" onClick="preSeachForm(); return _m.search(this)">
<input type="button" class="submit" name="cats::report" value="Export To Excel" onClick="return _m.report(this)">
		<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>
	
<?php
if(cats_user_is_incident_privileged() || cats_user_is_incident_analysis()){
?>
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<tr>
	<td>
		<div id="tabs">
			<div class="tab_off" id="tab_all_incidents" onClick="tab_toggle(this,'checkboxes','Reported_To','Systemic_Cause','Behaviour_Cause');" >All Incidents</div>
			<div class="tab_off" id="tab_injuries" onClick="tab_toggle(this,'checkboxes','Injury_Classification','Injury_Location','Injury_Mechanism','Injury_Type','Injury_Agency');" >Injury</div>
			<div class="tab_off" id="tab_environment" onClick="tab_toggle(this,'checkboxes','Environment_Classification','Environment_ImpactTo','Environment_ImpactBy');" >Environment</div>
			<div class="tab_off" id="tab_community" onClick="tab_toggle(this,'checkboxes','Community_Classification','Contact_Source');" >Community</div>
			<div class="tab_off" id="tab_quality" onClick="tab_toggle(this,'checkboxes','Quality_Classification');" >Quality</div>
			<div class="tab_off" id="tab_economic" onClick="tab_toggle(this,'checkboxes','Economic');" >Economic</div>
		</div>
	</td>
</tr>
<tr>
	<td>
		<div class="div_off" id="all_incidents" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_all_incidents">All Incidents Details</a></caption>
			<tr>
				<td class="label">
					<label>Severity</label>
				</td>
				<td>
					<input type="radio" id="Serverity_1" name="SEVERITY_RATING" value="Minor"><label for="Serverity_1" class="normal">Minor</label>
					<input type="radio" id="Serverity_2" name="SEVERITY_RATING" value="Externally Reportable"><label for="Serverity_2" class="normal">Externally Reportable</label>
					<input type="radio" id="Serverity_3" name="SEVERITY_RATING" value="Significant" ><label for="Serverity_3" class="normal">Significant</label>
				</td>
			</tr>
			<tr>
				<td class="label">
					<label>Externally Reported To</label>
				</td>
				<td><div id="divReported_To">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Systemic</label>
				</td>
				<td><div id="divSystemic_Cause">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Root Cause - Behaviour</label>
				</td>
				<td><div id="divBehaviour_Cause">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="injuries" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_injury">Injury Details</a></caption>
			<tr>
				<td class="label">
					<label>Injured Person Name</label>
				</td>
				<td><?php echo html_draw_employee_helper_search('INJURED_PERSON_ID','');?></td>
			</tr>
			<tr>
				<td class="label">
					<label>Injury Classification</label>
				</td>
				<td><div id="divInjury_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Location of Injury</label>
				</td>
				<td><div id="divInjury_Location">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Mechanism of Injury</label>
				</td>
				<td><div id="divInjury_Mechanism">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Injury Type</label>
				</td>
				<td><div id="divInjury_Type">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Agency of Injury</label>
				</td>
				<td><div id="divInjury_Agency">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="environment" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_environment">Environment Details</a></caption>
			<tr>
				<td class="label">
					<label>Environment Classification</label>
				</td>
				<td><div id="divEnvironment_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact To (Environment)</label>
				</td>
				<td><div id="divEnvironment_ImpactTo">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Impact By (Agent)</label>
				</td>
				<td><div id="divEnvironment_ImpactBy">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="community" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_community">Community Details</a></caption>
			<tr>
				<td class="label">
					<label>Community Classification</label>
				</td>
				<td><div id="divCommunity_Classification">Please wait while loading...</div></td>
			</tr>
			<tr>
				<td class="label">
					<label>Contact Source</label>
				</td>
				<td><div id="divContact_Source">Please wait while loading...</div></td>
			</tr>			
			<tr>
				<td class="label">
					<label>Substantiated?</label>
				</td>
				<td>
					<input type="radio" name="SUBSTANTIATED" id="Sub_1" value="Substantiated" /><label for="Sub_1" class="chk">Substantiated</label>
					<input type="radio" name="SUBSTANTIATED" id="Sub_2" value="Unsubstantiated" /><label for="Sub_2" class="chk">Unsubstantiated</label>
				</td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="quality" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_quality">Quality Details</a></caption>
			<tr>
				<td class="label">
					<label>Quality Classification</label>
				</td>
				<td><div id="divQuality_Classification">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
		<div class="div_off" id="economic" >
			<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
			<caption id="tog"><a name="anc_economic">Economic Details</a></caption>
			<tr>
				<td class="label">
					<label>Economic Classification</label>
				</td>
				<td><div id="divEconomic">Please wait while loading...</div></td>
			</tr>
			</table>
		</div>
	</td>
</tr>
</table>

		<fieldset class="tbar" style="text-align:right; ">
		<input type="reset" class="reset" value="Reset" />
		<input type="button" class="submit" name="cats::search2" value="Search" onClick="this.form.elements['cats::search'].click();">
		<input type="button" class="button" name="cats::new" value="New Incident" onClick="_m.newModule()">
		<input type="button" class="submit" name="cats::report" value="Export To Excel" onClick="return _m.report(this)">
		<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
		</fieldset>		
<?php
} // is a privileged user
?>
		</div>
	</td>
</tr>
</table>

<!-- END:: Search Table -->
		
	</td>
</tr>
</table>
</form>
<?php
include (CATS_INCLUDE_PATH . 'results_iframe.inc.php');
?>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>

<script language="javascript">

	function preSeachForm()
	{
		if($("#REALDATE_FROM_d").val() != "")
			setHiddenDate("REALDATE_FROM");
		if($("#REALDATE_TO_d").val() != "")
			setHiddenDate("REALDATE_TO");
	}

</script>