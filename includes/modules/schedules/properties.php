<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Schedules',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Schedule',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Schedules',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLSCHEDULER',
					'id'=>'SCHEDULER_ID',
					'view'=>'VIEW_SCHEDULE'
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLSCHEDULER',
					'id'=>'SCHEDULER_ID',
					'view'=>'VIEW_SCHEDULE',
					'filter'=>' 1=2 '
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLSCHEDULER',
					'id'=>'SCHEDULER_ID',
					'view'=>'VIEW_SCHEDULE',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){
	global $id;
	$ret = array();
	if(isset($_GET['rm'])){
		$ret = array(
			'rm' => array('label'=>'Return module', 'value'=>$_GET['rm'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
			'rp' => array('label'=>'Return page', 'value'=>$_GET['rp'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
			'rid' => array('label'=>'Return REPORT_ID', 'value'=>$_GET['rid'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
		);
	}
	switch($page){
		case 'edit':
			$ret = array_merge($ret,
				array(
					'REPORT_ID' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'REGISTER_ORIGIN' => array('label'=>'Register Origin', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SCHEDULER_DESCRIPTION' => array('label'=>'Scheduler Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'COMPLETION_DATE' => array('label'=>'Action Due Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					/*
					'MANAGED_BY_ID' => array('label'=>'Managed By Id', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
					*/
					'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>2),
					'ALLOCATED_TO_ID' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_site_linked', 'params2'=>'"DEPARTMENT_ID","SECTION_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1),
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>2),
					'ACTION_RAISED_DATE' => array('label'=>'Date Action to be Raised<br>(if left blank no action will be raised)', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					'FREQUENCY' => array('label'=>'Frequency (months)', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'NO_OF_DAYS_NOTICE' => array('label'=>'No of Days Notice before Action Raised', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_draw_on_off_radioset', 'col'=>1, 'col_span'=>2),
					'ACTIONS_CREATED' => array('label'=>'No. of Actions Created', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTIONS_CLOSED' => array('label'=>'No. of Actions Close', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search':
		case 'results': //explode("|","Site_Id|none,none,NULL|Department_Id|none,none,NULL|MANAGED_BY_ID|none,none,NULL|Completion_Date_from|',none,NULL,DATE_FROM|Completion_Date_to|',none,NULL,DATE_TO|Section_Id|none,none,NULL|Status|',none,''");
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1),
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
				'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
				'ALLOCATED_TO_ID' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1), 
				'STATUS' => array('label'=>'Schedule Status', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_on_off_dd', 'col'=>1, 'col_span'=>2),
				'COMPLETION_DATE_FROM' => array('label'=>'Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1),
				'COMPLETION_DATE_TO' => array('label'=>'Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			);
			break;
		case 'new':
			$ret = array_merge($ret,
				array(
					'REPORT_ID' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'REGISTER_ORIGIN' => array('label'=>'Register Origin', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SCHEDULER_DESCRIPTION' => array('label'=>'Scheduler Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'COMPLETION_DATE' => array('label'=>'Action Due Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					/*
					'MANAGED_BY_ID' => array('label'=>'Managed By Id', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
					*/
					'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>2),
					'ALLOCATED_TO_ID' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_site_linked', 'params2'=>'"DEPARTMENT_ID","SECTION_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1),
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>2),
					'ACTION_RAISED_DATE' => array('label'=>'Date Action to be Raised<br>(if left blank no action will be raised)', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
					'FREQUENCY' => array('label'=>'Frequency (months)', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'NO_OF_DAYS_NOTICE' => array('label'=>'No of Days Notice before Action Raised', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_draw_on_off_radioset', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'view':
			$ret = array_merge($ret,
				array(
					//'REPORT_ID' => array('label'=>'Report Id', 'value'=>'', 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0),
					'REPORT_ID' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2),
					'REGISTER_ORIGIN' => array('label'=>'Register Origin', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'SCHEDULER_DESCRIPTION' => array('label'=>'Scheduler Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'COMPLETION_DATE' => array('label'=>'Action Due Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					/*
					'MANAGED_BY_ID' => array('label'=>'Managed By Id', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
					*/
					'MANAGED_BY' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>2),
					'ALLOCATED_TO' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"DEPARTMENT_ID","SECTION_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),

					'ACTION_RAISED_DATE' => array('label'=>'Date Action to be Raised<br>(if left blank no action will be raised)', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'FREQUENCY' => array('label'=>'Frequency (months)', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'NO_OF_DAYS_NOTICE' => array('label'=>'No of Days Notice before Action Raised', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTIONS_CREATED' => array('label'=>'No. of Actions Created', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTIONS_CLOSED' => array('label'=>'No. of Actions Close', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'ACTION_TITLE',"text" => 'ACTION_TITLE');
	$ret[] = array("id" => 'SITE_DESCRIPTION',"text" => 'SITE_DESCRIPTION');
	$ret[] = array("id" => 'DEPARTMENT_DESCRIPTION',"text" => 'DEPARTMENT_DESCRIPTION');
	$ret[] = array("id" => 'MANAGED_BY',"text" => 'MANAGED_BY');
	$ret[] = array("id" => 'COMPLETION_DATE',"text" => 'COMPLETION_DATE');
	$ret[] = array("id" => 'STATUS',"text" => 'STATUS');
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>