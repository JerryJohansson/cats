<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));


// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST)>0){
	$_SESSION[$m.'_'.$p] = $_POST;
}else if(isset($_SESSION[$m.'_'.$p])){
	$_POST=$_SESSION[$m.'_'.$p];
}

if(isset($_POST['cats::search'])||isset($_POST['cats::report'])){
	// create the Select String
	$sql = "SELECT DISTINCT SCHEDULER_ID, ACTION_TITLE, SITE_DESCRIPTION, DEPARTMENT_DESCRIPTION, MANAGED_BY, ALLOCATED_TO,  COMPLETION_DATE, STATUS";
	
	// create action controls array to hold our buttons
	$action_controls = array();
	if(isset($_POST['cats::report'])){
		if(isset($_POST['cats::report_fields']) && !empty($_POST['cats::report_fields']))
			$sql = "select ".$_POST['cats::report_fields']." ";
		else
			$sql = "Select * ";
	}else{
		if(cats_user_is_editor(true)) {
			// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
			$edit_button = "edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:{$TABLES['id']}:\")";
			// put it into an array so we can merge with column headers later
			$action_controls[] = $edit_button;
			$sql .= ", '$edit_button' as edit ";
		}
		if(cats_user_is_super_administrator()) {
			// delete button string can be a button or an action checkbox for each row...
			// the checkboxes also have a dropdown which is displayed to the right of the grid navigation
			// The button control sql looks like this:
			// delete:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=post&a=del&id=:{$TABLES['id']}:\")";
			$delete_button = "delete:::{$TABLES['id']}:";
			$action_controls[] = $delete_button;
			$sql .= ", '$delete_button' as del ";
		}
	}
	$sql .= " FROM {$TABLES['view']} ";
	
	// specify columns to ignore when drawing the table
	$col_ignore = array($TABLES['id']);
	// specify links for column values on each row
	$col_links = array("","js|top.view('$m',|{$TABLES['id']}","","","","","");
	
	// column headers and attributes
	// these are used when rendering the search results
	// col_attributes - use this array to attach attributes to the header columns
	$col_attributes = array(' width="3%" ', ' width="26%" ',' width="10%" ',' width="15%" ',' width="13%" ',' width="13%" ',' width="10%" ',' width="4%" ','','');
	// col_headers - used to label our heading columns
	$col_headers = explode("|","ID|Action Title|Site|Department|Managed By|Allocated To|Completion Date|Status");
	$col_headers = array_merge($col_headers,$action_controls); // append edit and delete buttons if they exist
	
	// the order by column name used for the default sorting criteria
	$order_by_col = "SITE_DESCRIPTION";
	
	$sql_where = "";
	// Create Where String and append it to the Select String
	foreach($FIELDS as $column_name => $props){
		// the criterias operator I.E operator of <= will look like COLUMN_NAME <= 'SEARCH VALUE'
		$operator = isset($props['operator']) ? " {$props['operator']} " : " = ";
		// the values delimiter I.E a delim of ' will look like 'SEARCH VALUE'
		//$delim = $props['delim'];comment  dev
		$delim = isset($props['delim'])?$props['delim']:'';
		// create the user specific sites criteria and add to the where string
		if($column_name == "SITE_ID" && (!isset($_POST[$column_name]) || empty($_POST[$column_name]))){
			$sql_where .= (($sql_where == "") ? " WHERE ":" AND ") . db_get_user_sites_where_string();
		}else{
			switch($column_name){
				default:
					// sql_where is passed by reference
					db_get_where_clause($column_name,$sql_where,$operator,$delim);
					break;
			}
		}
	}
	$search_sql = $sql . $sql_where;
	
	$db->debug = false;
	if($db->debug == true)
	{
		print_r($_POST);
		echo('<br>'.$search_sql);
	}
	
	require_once(CATS_ADODB_PATH . 'cats_csv.php');
	
}
?>
<script>
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	// resize result iframe to consume the parent page
	init_resize_results_container();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------
</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><form name="results" action="index.php" method="post"><table width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
	<td width="100%">

<!-- BEGIN:: Results Table -->
<?php
if(isset($search_sql)){
	// Search Results
	//db_render_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], true, $order_by_col, 'ASC');
	cats_form_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], $m, $order_by_col, 'ASC', $col_links, $col_ignore);
}else{
	echo("<h2>No Search results</h2>");
}
?>
<!-- END:: Results table -->

	</td>
</tr>
</table>
</form>