<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
$report_id = isset($REQUEST['report_id'])?$_REQUEST['report_id']:0;

$iTabs=0;
$iTabButtons=0;
$RS=array(); 
$RECORD=false;

// Security Check
if(!cats_user_is_editor()  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New Schedule");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

if($action=='edit'){
	if($id==0){
		$_SESSION['messageStack']->add("The ID was missing from the request. The ID of the record must be greater than 0.");
		include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
		exit;
	}
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	
	/*
	Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!
	*/
	//print($RECORD['SITE_ID']);
	$user_access = cats_check_user_site_access($RECORD['SITE_ID']);
	
	if($user_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}else{

	$ORIGIN_TABLES = array(
		'major_hazards'=>array('view'=>'Major Hazard Register','title'=>'Major Hazard Register'),
		'site_specific'=>array('view'=>'Site Specific Obligation','title'=>'Site Specific Obligation')
	);

	// set the originating types according to the return module
	if(isset($_GET['rm'])){
		cats_set_fields_array_value('REPORT_ID',$_GET['rid']);
		cats_set_fields_array_value('REGISTER_ORIGIN',$ORIGIN_TABLES[$_GET['rm']]['view']);
		cats_set_fields_array_value('ACTION_TITLE','');
	}else{
		// To record the parent doc id if this Action was not created from the Actions filter form
		cats_set_fields_array_value('REPORT_ID',0);
		cats_set_fields_array_value('REGISTER_ORIGIN','Schedule');
		cats_set_fields_array_value('ACTION_TITLE','');
	}
}
$RS = array();//add dev for load formdata
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// initialise the tabbing object
	init_tabs();
	set_textarea_maxlength();
	//alert("after set_textarea_maxlength")
	// create document.mousedown handlers
	init_document_handlers();
	//alert("after init_document_handlers")
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

function _validate_extra(){
	var f=document.forms[0];
	var errors = "";
	
	var o = f.elements["ACTION_RAISED_DATE"];
	if (o.value == "") {
		errors += "- Action Raised Date is required.\n";
	}
	var e = f.elements["FREQUENCY"];
	if(e.value.trim()=="") {
		errors += "- Frequency is required.";
	}else{
		if(isNaN(e.value)) errors += "- Frequency must be a valid number.";
	}
	return errors;
}
</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onSubmit="return CATS_validateForm(this, 'ACTION_TITLE','','R','SCHEDULER_DESCRIPTION','','R','COMPLETION_DATE','','R','MANAGED_BY_ID','','R','NO_OF_DAYS_NOTICE','','NisNum');"<?php
}else{
	?>onSubmit="return CATS_validateForm(this, 'ACTION_TITLE','','R','SCHEDULER_DESCRIPTION','','R','COMPLETION_DATE','','R','MANAGED_BY_ID','','R','NO_OF_DAYS_NOTICE','','NisNum');"<?php
}
?>>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<?php
if($action=='edit'){
?>
<a
	title="View Originating Records"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">Originating Documents</a>
<a
	title="View Follow-up Actions"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/followup.gif);">Followup Actions</a>
<?php
}
?>
</fieldset>
<fieldset class="tool_bar">
<?php
if($p=="edit"){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php }elseif(cats_user_is_editor(true)){ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
	<a
	title="Print current <?PHP echo $module_name_txt;?> item"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>	
<?php
if($p=='edit' && cats_user_is_administrator()){
?>
<!-- <a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a> -->
<?php
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed = new Editor($m,$p,$a);
if($id>0){
	// echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));comment dev
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,isset($RS[$id])?$RS[$id]: false));
}else{
	echo($ed->buildForm($FIELDS));
}
?>
	<fieldset class="tbar" style="text-align:right; " id="button_panel">
<?php if($p!="view"){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()">
<?php }elseif(cats_user_is_editor(true) || cats_user_is_super_administrator()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
<?php if($p=="edit"){ ?>
<input type="button" class="button" name="cats::New" value="New <?php echo $module_name_txt;?>" onClick="_m.newModule();" />
<?php } ?>
	<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
	</fieldset>		
</fieldset>
<?php
if($p!='new'){
?>
<!-- Originiating Documents -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();"><?php echo $GENERAL['name'];?></a></caption>
<tr>
	<td>
<?php

// Originiating Documents
$report_id = $RECORD['REPORT_ID'];
switch($RECORD['REGISTER_ORIGIN']){
	case 'Site Specific Obligation':
		$originiating_documents_sql="SELECT OBLIGATION_ID, 'Site Specific Obligation' as type, IssueString, Key_Requirement, Legislation1";
		if(cats_user_is_editor(true)) {
			$edit_button = "edit:javascript:top.show_edit_screen(\"site_specific\",\"index.php?m=site_specific&p=edit&id=:OBLIGATION_ID:\")";
			$originiating_documents_sql .= ", '$edit_button' as edit ";
		}
		$originiating_documents_sql .= " FROM VIEW_SITE_SPECIFIC_OBLIGATION WHERE Obligation_Id = $report_id";
		$col_1 = "Issue";
		$col_2 = "Key Requirement";
		$col_3 = "Legislation";
		break;
	case 'Major Hazard Register':
		$originiating_documents_sql="SELECT Hazard_Id, 'Major Hazard Register' as type, Hazard_Type, Description, DateAnalysed";
		if(cats_user_is_editor(true)) {
			$edit_button = "edit:javascript:top.show_edit_screen(\"major_hazards\",\"index.php?m=major_hazards&p=edit&id=:HAZARD_ID:\")";
			$originiating_documents_sql .= ", '$edit_button' as edit ";
		}
		$originiating_documents_sql .= " FROM View_HazardRegister_Details WHERE Hazard_Id = $report_id";
		$col_1 = "Hazard";
		$col_2 = "Detailed Description";
		$col_3 = "Date Analysed";
		break;
	default:
		// show no results ... in fact let just put a message there instead
		// be friendly now ;)
		break;
}

if(isset($originiating_documents_sql) && !empty($originiating_documents_sql)){
	$col_attributes = array(' width="5%" ',' width="15%" ',' width="20%" ',' width="40%" ',' width="15%" ',' width="5%" ');
	$col_headers=array( 'Report Id' , 'Origin' , $col_1 , $col_2 , $col_3  );
	$col_headers[] = $edit_button;
	db_render_pager($originiating_documents_sql,$col_headers,$col_attributes,'',false,'','',1000);
}else{
	$_SESSION['messageStack']->reset();
	$_SESSION['messageStack']->add("No Originating Document results for this record.","info");
	echo $_SESSION['messageStack']->output();
}
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>

<!-- Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td>
<?php
// Follow-up Actions

// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","","");

$sql = "SELECT Action_Id, Action_Title, Managed_By, Allocated_To, Action_Type, Status, Scheduled_Date";
$col_headers=array('ID','Action Title','Managed By','Allocated To','Action Type','Status','Scheduled Date');
if(cats_user_is_editor(true)) {
	$edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
	$col_headers[] = $edit_button;
	$sql .= ", '$edit_button' as edit ";
}

$col_attributes = array(' width="3%" ',' width="32%" ',' width="10%" ',' width="10%" ',' width="10%" ',' width="20%" ',' width="10%" ');
$sql .= " FROM View_Action_Details  WHERE Report_Id = $id AND Origin_Table = 'Originating_Schedule'  ORDER BY Action_Title ASC";
//db_render_pager_links($sql,$col_headers,$col_attributes,$col_links,'',false,'','',1000);
cats_form_pager($sql,$col_headers,$col_attributes,'', '', '', 'ASC', $col_links, false, 1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<?php
}
?>
</div>
</form>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">

	function preSubmitForm()
	{
		if($("#COMPLETION_DATE_d").val() != "")
			setHiddenDate("COMPLETION_DATE");
		if($("#ACTION_RAISED_DATE_d").val() != "")
			setHiddenDate("ACTION_RAISED_DATE");
	}

</script>
