<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Checkboxes',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Checkbox',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Checkboxes',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENTFORM_CHECKBOXES', 
					'id'=>'CHECKBOX_ID',
					'view'=>'VIEW_CHECKBOXTYPES'
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENTFORM_CHECKBOXES', 
					'id'=>'CHECKBOX_ID',
					'view'=>'VIEW_CHECKBOXTYPES',
					'filter'=>' 1=2 '
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENTFORM_CHECKBOXES', 
					'id'=>'CHECKBOX_ID',
					'view'=>'VIEW_CHECKBOXTYPES',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();
	switch($page){
		case 'new': 
			$ret = array_merge($ret, 
				array(
					'CHECKBOX_TYPE' => array('label'=>'Checkbox Type', 'value'=>'', 'func'=>'html_form_draw_checkbox_types_dd', 'params'=>'onchange=\"get_dropdown_display(this,this.form.DROPDOWN_DISPLAY);\"', 'col'=>1, 'col_span'=>2),
					'CHECKBOX_DESC' => array('label'=>'Checkbox Description', 'value'=>'', 'func'=>'html_draw_input_field', 'params'=>'size=\"50\" maxlength=\"40\"', 'col'=>1, 'col_span'=>2), 
					'CHECKBOX_COMMENT' => array('label'=>'Checkbox Comment', 'value'=>'', 'func'=>'html_draw_textarea_field', 'params'=>'maxlength=\"1000\"', 'col'=>1, 'col_span'=>2),
					'DROPDOWN_DISPLAY' => array('label'=>'', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'CHECKBOX_TYPE' => array('label'=>'Checkbox Type', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'CHECKBOX_DESC' => array('label'=>'Checkbox Description', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'CHECKBOX_COMMENT' => array('label'=>'Checkbox Comment', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)					
				)
			);
			break;
		case 'search': 
		case 'results':
			$ret = array(
				'CHECKBOX_TYPE' => array('label'=>'Checkbox Type', 'delim'=>"'",  'value'=>'', 'func'=>'html_form_draw_checkbox_types_dd', 'col'=>1, 'col_span'=>2),
				'CHECKBOX_DESC' => array('label'=>'Checkbox Description', 'value'=>'', 'delim'=>"'", 'func'=>'html_form_draw_checkbox_description_dd', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}
if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>
