<?PHP
//require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
//require_once(CATS_CLASSES_PATH . 'editor.php');
//require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$iTabs=0;
$iTabButtons=0;
$page_title = "Cron Jobs";
$info_image = "info";
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
window.onload = init;
</script>
</head>
<body>
<?php
switch($host_name) {
	case 'catsdev31' :
		echo "<h1> Cronjobs in DEV31 </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev31/');
		break;
	case 'catsdev' :
		echo "<h1> Cronjobs in DEV </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/dev/');
		break;
	case 'catstst' :
		echo "<h1> Cronjobs in TST </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/tst/');
		break;
	case 'catsprd' :
		echo "<h1> Cronjobs in PRD </h1>";
		define('CATS_ROOT_PATH','/apps2/CATS/prd/');
		break;
	default :
		echo "<p> Unknown Host : ".$host_name."</p>";
		echo "</body></html>";
		die;
	}
?>

<h2>Output To Browser</h2>
<p><a href="/js/cronjobs/EmailOverdueActions.php" target="_blank">Email Overdue Actions</a></p>
<p><a href="/js/cronjobs/CreateScheduledActions.php" target="_blank">Create Scheduled Actions</a></p>
<p><a href="/js/cronjobs/CreateScheduledPCRActions.php" target="_blank">Create Scheduled PCR Actions</a></p>
<p><a href="/js/cronjobs/EmailReminder.php" target="_blank">Email Reminder</a></p>

<h2>Output To Log File</h2>
<p><a href="/js/cronjobs/Log.php?cronjob=EmailOverdueActions" target="_blank">Email Overdue Actions</a></p>
<p><a href="/js/cronjobs/Log.php?cronjob=CreateScheduledActions" target="_blank">Create Scheduled Actions</a></p>
<p><a href="/js/cronjobs/Log.php?cronjob=CreateScheduledPCRActions" target="_blank">Create Scheduled PCR Actions</a></p>
<p><a href="/js/cronjobs/Log.php?cronjob=EmailReminder" target="_blank">Email Reminder</a></p>

<h2>Log Files</h2>
<table border="5" align="left" cellpadding="5" cellspacing="5">
<thead>
<th>File</th>
<th>Size (bytes)</th>
<th>Changed Date</th>
</thead>
<?php
$dir = CATS_ROOT_PATH."/js/cronjobs/logs/";

// Open a known directory, and proceed to read its contents
if (is_dir($dir)) {
	if ($dh = opendir($dir)) {
		while (($file = readdir($dh)) !== false) {
			//echo "<p>filename: $file : filetype: " . filetype($dir . $file) . "</p>";
			if (filetype($dir.$file) == 'file') {
				echo '<tr>';
				echo '<td><a href="/js/cronjobs/logs/'.$file.'" target="_blank">'.$file.'</a></td>';
				echo '<td>'.filesize($dir.$file).'</td>';
				echo '<td>'.date("d F Y H:i:s.", filectime($dir.$file)).'</td>';
				echo '<tr>';
			}
		}
	closedir($dh);
	}
}
?>
</table>
<?php
include (CATS_INCLUDE_PATH . 'results_iframe.inc.php');
?>