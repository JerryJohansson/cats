<?php
/*
modified by Craig Wood SRA IT, 06/02/2015 
Added Golden Rule array in the new/edit page
Change 2.2 SRA functional Requirements Doc 1.0 Jan 2015

*/
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Statistic',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Statistic',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Statistic',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'TBLINCIDENT_DETAILS',
					'CHECKBOXES'=> array('table'=>'TBLINCIDENT_CHECKBOXVALUES','id'=>'INCIDENTID', 'fields'=>'INCIDENTID,CHECKBOXID,CHECKBOXTYPE'),
						'DOCUMENTLOCATION'=>array(
						'table'=>'TBLMULTIDOCUMENTS',
						'id'=>'REPORT_ID'
						)
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'TBLINCIDENT_DETAILS',
					'filter'=>' 1=2 ',
					'CHECKBOXES'=> array('table'=>'TBLINCIDENT_CHECKBOXVALUES','id'=>'INCIDENTID', 'fields'=>'INCIDENTID,CHECKBOXID,CHECKBOXTYPE'),
						'DOCUMENTLOCATION'=>array(
						'table'=>'TBLMULTIDOCUMENTS',
						'id'=>'REPORT_ID'
						)
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INCIDENT_PRINTVIEW',
					'filter'=>' 1=2 ',
					'CHECKBOXES'=> array('table'=>'TBLINCIDENT_CHECKBOXVALUES','id'=>'INCIDENTID', 'fields'=>'INCIDENTID,CHECKBOXID,CHECKBOXTYPE'),
						'DOCUMENTLOCATION'=>array(
						'table'=>'TBLMULTIDOCUMENTS',
						'id'=>'REPORT_ID'
						)
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLINCIDENT_DETAILS',
					'id'=>'INCIDENT_NUMBER',
					'view'=>'VIEW_INCIDENT_SEARCHVIEW',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page = 'search'){
	global $id, $p;

	$ret = array();
	switch($page){
		case 'edit': case 'new':
			$ret = array_merge($ret,
				array(
					'NOTE' => array('group'=>'row_title', 'label'=>'NOTE: The purpose of the incident investigation is NOT to fix blame but to obtain the relevant facts and prevent a recurrence.', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'NOTE2' => array('group'=>'row_title', 'label'=>'* Denotes this is a Mandatory field.', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_report_details' => array('label'=>'Report Details', 'group'=>'row_header'),
					'INCIDENT_TYPE' => array('label'=>'Incident Type', 'value'=>'1', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>50),
					'INCDISPLYID' => array('label'=>'* Incident Report No.', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1, 'maxlength'=>50),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'value'=>'', 'func'=>'html_get_incident_status_radioset', 'col'=>2, 'col_span'=>1),
					'SITE_ID' => array('label'=>'* Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					//'DEPARTMENT_ID' => array('label'=>'* Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'* Department', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1),
					//'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd_linked', 'params2'=>'"SHIFT_ID"', 'col'=>1, 'col_span'=>1),
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_filtered_section_dd_linked', 'params2'=>'"SHIFT_ID"', 'col'=>1, 'col_span'=>1),
					'SHIFT_ID' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_form_draw_shift_dd_linked', 'col'=>2, 'col_span'=>1),
					//'SHIFT_ID' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_form_draw_filtered_shift_dd', 'col'=>2, 'col_span'=>1),
					'INITIATED_BY_ID' => array('label'=>'* Report Initiated By', 'value'=>NULL, 'func'=>'html_form_draw_employee_dd', 'col'=>1, 'col_span'=>1),
					'REPORTEDCOMPANYID' => array('label'=>'* Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>2, 'col_span'=>1),
					'INCIDENT_DATE' => array('label'=>'Incident Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field', 'col'=>1, 'col_span'=>1),
					//'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'REPORT_DATE' => array('label'=>'Report Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field', 'col'=>2, 'col_span'=>1),
					//'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'REPORTED_TO' => array('label'=>'* Reported To', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					'SUPERINTENDENT_ID' => array('label'=>'* Responsible Superintendent', 'value'=>NULL, 'func'=>'html_form_draw_employee_dd', 'col'=>2, 'col_span'=>1),
					'LOCATION' => array('label'=>'* Location', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					'AREA' => array('label'=>'* Area', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					'EQUIPMENT_NO' => array('label'=>'Equipment', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					//'GOLDENRULEARRAY' => array('label'=>'Golden Rule', 'value'=>NULL, 'func'=>'html_form_draw_golden_rule_dd', 'col'=>1, 'col_span'=>1),
					'row_incident_type' => array('label'=>'Incident Type', 'group'=>'row_header'),
					'Incident_Category' => array('label'=>'Incident Category', 'value'=>'', 'func'=>'html_draw_incident_category_checkboxset', 'col'=>1, 'col_span'=>2),
					'Report_Type' => array('label'=>'Type of Report', 'value'=>'', 'func'=>'html_draw_incident_report_type_checkboxset', 'col'=>1, 'col_span'=>2),
					'row_person_involved' => array('label'=>'Persons Involved', 'group'=>'row_header'),
					'PRIMARYPERSONID' => array('label'=>'* Primary Person Involved', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					'PRIMARYCOMPANYID' => array('label'=>'* Primary Person Involved Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>2, 'col_span'=>1),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Other Persons Involved & Company Name', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON_ID' => array('label'=>'Injured Persons Name', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1),
					'INJUREDCOMPANYID' => array('label'=>'Injured Persons Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>2, 'col_span'=>1),
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_gender_radioset', 'col'=>1, 'col_span'=>2),
					'row_incident_details' => array('label'=>'Incident Details', 'group'=>'row_header'),
					'INCIDENT_TITLE' => array('label'=>'* Incident Title', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DESCRIPTION' => array('label'=>'* Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					//'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'row_assessment_risk' => array('label'=>'Assessment of the Risk of Recurrence', 'group'=>'row_header'),
					'INITIALRISKCATEGORY' => array('label'=>'* Risk Category', 'value'=>NULL, 'func'=>'html_form_draw_risk_category_dd', 'col'=>1, 'col_span'=>1),
					'INITIALRISKLEVEL' => array('label'=>'* Risk Level', 'value'=>NULL, 'func'=>'html_form_draw_risk_level_dd', 'col'=>2, 'col_span'=>1),
					'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_field')
				   
						),
						'sql'=>"SELECT DocumentLocation, 'delete:javascript:remove_sequence_row(this, ::)' from tblMultiDocuments  where FormType = 'incident_details' AND Report_Id = $id"
					)
					//'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),

				)
			);
			if($p=='post'){
				$ret = array_merge($ret,
					array(
						'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
					)
				);
			}
			break;
		case 'view':
/*
"INCDISPLYID","PRIMARYPERSONID","PRIMARYCOMPANYID","REPORTEDCOMPANYID","INJUREDCOMPANYID","INCIDENTSTATUS",
"AREA","BODY_PART","CAUSE_OF_INJURY","COMPANY","CURRENT_LOCATION","DEPARTMENT_ID","ESTIMATED_COST",
"IMMEDIATE_CORRECTIVE_ACTIONS","INCIDENT_DESCRIPTION","INCIDENT_NUMBER","INCIDENT_REPORT_STATUS_ID",
"INCIDENT_TIME","INCIDENT_TITLE","INCIDENT_TYPE","INITIALRISKCATEGORY","INITIALRISKLEVEL","INITIATED_BY_ID",
"INJURED_PERSON_COMPANY","INJURED_PERSON_GENDER","INJURED_PERSON_ID","INJURY_STATUS",
"INVESTIGATION_COMPLETION_DATE","INVESTIGATION_REQUIRED","LOCATION","MECHANISM_OF_INJURY",
"PERSONS_INVOLVED_DESCRIPTION","RECALCRISKCATEGORY","RECALCRISKLEVEL","RECALC_RISK_RESULT",
"REPORTED_TO","INCIDENT_DATE","REPORT_DATE","REPORT_TIME","SEVERITY_RATING","SITE_ID","SUBSTANTIATED",
"SUPERINTENDENT_ID","INCIDENT_CATEGORIES","INCIDENT_REPORTTYPE","INCIDENT_IMPACTBY","INCIDENT_IMPACTTO",
"INCIDENT_INJURYTYPE","INCIDENT_INJURYMECHANISM","INCIDENT_INJURYAGENCY","INCIDENT_INJURYLOCATION",
"INCIDENT_INJURYCLASS","INCIDENT_ENVIROCLASS","INCIDENT_COMMUNITYCLASS","INCIDENT_QUALITYCLASS",
"INCIDENT_REPORTEDTO","INCIDENT_SYSTEMICCAUSE","INCIDENT_BEHAVIOURCAUSE","INCIDENT_ECONOMIC",
"DEPARTMENT","SITE","INJURED_PERSON","SUPERINTENDENT","INITIATED_BY","TYPE_OF_EMPLOYEE","REPORTEDTO",
"PRIMARYPERSON","RECALC_RISKCAT","RECALC_RISKLEVEL","INIT_RISKCAT","INIT_RISKLEVEL","REPORTEDBYCOMPANY",
"INJUREDCOMPANY","PRIMARYCOMPANY","CREATEDACTIONS","CLOSEDACTIONS"
*/
			$ret = array_merge($ret,
				array(
					'NOTE' => array('group'=>'row_title', 'label'=>'NOTE: The purpose of the incident investigation is NOT to fix blame but to obtain the relevant facts and prevent a recurrence.', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_report_details' => array('label'=>'Report Details', 'group'=>'row_header'),
					'INCDISPLYID' => array('label'=>'Incident Report No.', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1, 'maxlength'=>50),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'value'=>'', 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),

					'SITE' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),

					'DEPARTMENT' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1),

					'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"SHIFT_ID"', 'col'=>1, 'col_span'=>1),

					'SHIFT_DESCRIPTION' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'INITIATED_BY' => array('label'=>'Report Initiated By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'REPORTEDBYCOMPANY' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'REPORTEDTO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'SUPERINTENDENT' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'EQUIPMENT_NO' => array('label'=>'Equipment', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_incident_type' => array('label'=>'Incident Type', 'group'=>'row_header'),
					'Incident_Category' => array('label'=>'Incident Category', 'value'=>'', 'func'=>'html_draw_incident_category_display', 'col'=>1, 'col_span'=>2),
					'Report_Type' => array('label'=>'Type of Report', 'value'=>'', 'func'=>'html_draw_incident_report_type_display', 'col'=>1, 'col_span'=>2),
					'row_person_involved' => array('label'=>'Persons Involved', 'group'=>'row_header'),
					'PRIMARYPERSON' => array('label'=>'Primary Person Involved', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'PRIMARYCOMPANY' => array('label'=>'Primary Person Involved Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Other Persons Involved & Company Name', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON' => array('label'=>'Injured Persons Name', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'INJUREDCOMPANY' => array('label'=>'Injured Persons Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'row_incident_details' => array('label'=>'Incident Details', 'group'=>'row_header'),
					'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					//'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'row_assessment_risk' => array('label'=>'Assessment of the Risk of Recurrence', 'group'=>'row_header'),
					'INIT_RISKCAT' => array('label'=>'Risk Category', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'INIT_RISKLEVEL' => array('label'=>'Risk Level', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'Document' => array('label'=>'Supporting Documents', 'group' => 'row_sequence', 'func'=>'$this->buildSequentialForm', 'value'=>array(
							'DOCUMENTLOCATION' => array('label'=>'Document Location', 'value'=>NULL, 'func'=>'html_get_file_location_value')
						),
						'sql'=>"SELECT DocumentLocation from tblMultiDocuments where FormType = 'incident_details' AND Report_Id = $id",
						'params2'=>"READONLY"
					)
					//'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					//'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),

				)
			);
			break;
		case 'new_old':
			$ret = array_merge($ret,
				array(
					'INCDISPLYID' => array('label'=>'IncDisplyId', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2),
					'DEPARTMENT_ID' => array('label'=>'Department Id', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd', 'col'=>1, 'col_span'=>2),
					'INITIATED_BY_ID' => array('label'=>'Initiated By Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
					'REPORTEDCOMPANYID' => array('label'=>'ReportedCompanyId', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DATE' => array('label'=>'Incident Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_TIME' => array('label'=>'Incident Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'REPORT_DATE' => array('label'=>'Report Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2),
					'REPORT_TIME' => array('label'=>'Report Time', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SUPERINTENDENT_ID' => array('label'=>'Superintendent Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
					'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'AREA' => array('label'=>'Area', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Persons Involved Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON_ID' => array('label'=>'Injured Person Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
					'INJUREDCOMPANYID' => array('label'=>'InjuredCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INJURED_PERSON_GENDER' => array('label'=>'Injured Person Gender', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_TITLE' => array('label'=>'Incident Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENT_DESCRIPTION' => array('label'=>'Incident Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'IMMEDIATE_CORRECTIVE_ACTIONS' => array('label'=>'Immediate Corrective Actions', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALRISKCATEGORY' => array('label'=>'InitialRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INITIALRISKLEVEL' => array('label'=>'InitialRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PRIMARYPERSONID' => array('label'=>'PrimaryPersonId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'PRIMARYCOMPANYID' => array('label'=>'PrimaryCompanyId', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
					'INCIDENTSTATUS' => array('label'=>'IncidentStatus', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
				)
			);
			if($p=='post'){
				$ret = array_merge($ret,
					array(
						'SEVERITY_RATING' => array('label'=>'Severity Rating', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'SUBSTANTIATED' => array('label'=>'Substantiated', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKCATEGORY' => array('label'=>'RecalcRiskCategory', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),
						'RECALCRISKLEVEL' => array('label'=>'RecalcRiskLevel', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
					)
				);
			}
			break;
		case 'search':
		case 'results':
			if(cats_user_is_incident_privileged()){
				$ret = array(
					'INCDISPLYID' => array('label'=>'Incident No.', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'delim'=>"'", 'value'=>NULL, 'func'=>'html_get_incident_status_dd', 'col'=>2, 'col_span'=>1),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd', 'col'=>2, 'col_span'=>1),
					'SUPERINTENDENT_ID' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1),
					'TYPE_OF_EMPLOYEE' => array('label'=>'Type Of Employee', 'value'=>'', 'func'=>'html_form_draw_employee_type_dd', 'col'=>2, 'col_span'=>1),
					'INCIDENT_CATEGORY' => array('label'=>'Incident Category', 'value'=>NULL, 'func'=>'html_form_draw_incident_categories_dd', 'col'=>1, 'col_span'=>1),
					'REPORT_TYPE' => array('label'=>'Report Type', 'value'=>NULL, 'func'=>'html_form_draw_incident_report_types_dd', 'col'=>2, 'col_span'=>1),
					'INJUREDCOMPANYID' => array('label'=>'Injured Persons Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>1, 'col_span'=>1),
					'ESTIMATED_COST' => array('label'=>'Estimated Cost', 'value'=>NULL, 'func'=>'html_form_draw_estimated_costs_dd', 'col'=>2, 'col_span'=>1),
					'REALDATE_FROM' => array('label'=>'Incident Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1),
					'REALDATE_TO' => array('label'=>'Incident Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'INITIALRISKCATEGORY' => array('label'=>'Assessment of Risk (Category)', 'value'=>NULL, 'func'=>'html_form_draw_risk_category_dd', 'col'=>1, 'col_span'=>1),
					'INITIALRISKLEVEL' => array('label'=>'Assessment of Risk (Level)', 'value'=>NULL, 'func'=>'html_form_draw_risk_level_dd', 'col'=>2, 'col_span'=>1),
					'RECALCRISKCATEGORY' => array('label'=>'Re-assessment of Risk (Category)', 'value'=>NULL, 'func'=>'html_form_draw_risk_category_dd', 'col'=>1, 'col_span'=>1),
					'RECALCRISKLEVEL' => array('label'=>'Re-assessment of Risk (Level)', 'value'=>NULL, 'func'=>'html_form_draw_risk_level_dd', 'col'=>2, 'col_span'=>1),
					'INITIATED_BY_ID' => array('label'=>'Initiated By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1),
					'REPORTED_TO' => array('label'=>'Reported To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1),
					'PRIMARYPERSONID' => array('label'=>'Primary Person', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1),
					'PERSONS_INVOLVED_DESCRIPTION' => array('label'=>'Other Persons', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1)
					//'cats::report_fields' => array('label'=>'Chosen Reporting Fields', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
					//new golden rule array
                   
				);
			}else{
				$ret = array(
					'INCDISPLYID' => array('label'=>'Incident No.', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1),
					'SUPERINTENDENT_ID' => array('label'=>'Responsible Superintendent', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2),
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1),
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_filtered_department_dd', 'col'=>2, 'col_span'=>1),
					'INCIDENT_CATEGORY' => array('label'=>'Incident Category', 'value'=>NULL, 'func'=>'html_form_draw_incident_categories_dd', 'col'=>1, 'col_span'=>1),
					'REPORT_TYPE' => array('label'=>'Report Type', 'value'=>NULL, 'func'=>'html_form_draw_incident_report_types_dd', 'col'=>2, 'col_span'=>1),
					'REALDATE_FROM' => array('label'=>'Incident Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1),
					'REALDATE_TO' => array('label'=>'Incident Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1),
					'INCIDENTSTATUS' => array('label'=>'Incident Status', 'delim'=>"'", 'value'=>NULL, 'func'=>'html_get_incident_status_dd', 'col'=>1, 'col_span'=>2)
				);
			}
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'INCDISPLYID',"text" => 'INCDISPLYID');
	$ret[] = array("id" => 'INCIDENT_DATE',"text" => 'INCIDENT_DATE');
	$ret[] = array("id" => 'INCIDENT_CATEGORIES',"text" => 'INCIDENT_CATEGORIES');
	$ret[] = array("id" => 'INCIDENT_TITLE',"text" => 'INCIDENT_TITLE');
	$ret[] = array("id" => 'INCIDENTSTATUS',"text" => 'INCIDENTSTATUS');
	$ret[] = array("id" => 'CREATEDACTIONS',"text" => 'CREATEDACTIONS');
	$ret[] = array("id" => 'CLOSEDACTIONS',"text" => 'CLOSEDACTIONS');
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);

//foreach($FIELDS as $key => $value) echo("$key, ");
?>