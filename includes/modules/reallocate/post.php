<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$from_id = isset($_POST['from_id'])?$_POST['from_id']:false;
$to_id = isset($_POST['to_id'])?$_POST['to_id']:false;
$from_name = isset($_POST['from_id_text'])?$_POST['from_id_text']:'no-one';
$to_name = isset($_POST['to_id_text'])?$_POST['to_id_text']:'no-one';

$site_id = (isset($_POST['SITE_ID']) && (!empty($_POST['SITE_ID'])))?$_POST['SITE_ID']:'NULL';
$dept_id = (isset($_POST['DEPARTMENT_ID']) && (!empty($_POST['DEPARTMENT_ID'])))?$_POST['DEPARTMENT_ID']:'NULL';
$sect_id = (isset($_POST['SECTION_ID']) && (!empty($_POST['SECTION_ID'])))?$_POST['SECTION_ID']:'NULL';

if($from_id!=false && $to_id!=false){
	//exit("action = $a and id = $id");
	$ret = true;
	switch($action){
		case "edit": // Update node
			// create sql for Updating Managed By
			$sql_1 = "update tblAction_Details set managed_by_id = $to_id, Site_Id = $site_id, Department_Id = $dept_id, Section_Id = $sect_id where Status='Open' AND managed_by_id = $from_id ";

			// create sql for Updating Allocated To
			$sql_2 = "update tblAction_Details set Allocated_To_Id = $to_id where Status='Open' AND Allocated_To_Id = $from_id ";
		
			// create sql for Updating tblScheduler
			$sql_3 = "update tblscheduler set managed_by_id = $to_id, Site_Id = $site_id, Department_Id = $dept_id, Section_Id = $sect_id where managed_by_id = $from_id ";

			// create sql for Updating PCR Actions Managed By
			$sql_4 = "update tblPCR_Actions set managed_by_id = $to_id, Site_Id = $site_id where (Status='Open' OR Status='pending') AND managed_by_id = $from_id ";

			// create sql for Updating PCR Actions Allocated To
			$sql_5 = "update tblPCR_Actions set Allocated_To_Id = $to_id where (Status='Open' OR Status='pending') AND Allocated_To_Id = $from_id ";

			// create sql for Updating Default PCR Action Managed By
			$sql_6 = "update tblPCR_Action set managed_by_id = $to_id, Site_Id = $site_id where managed_by_id = $from_id ";
			
			$ret = db_query($sql_1);
			if($ret===false){
				$_SESSION['messageStack']->add("Error occured while updating Managed By Person in Actions - From '$from_name ($from_id)' To '$to_name ($to_id)'.<br>Error: ".$db->ErrorMsg()."<hr/>$sql_1<hr/>");
				break;
			}else $_SESSION['messageStack']->add("Managed By Person in Actions - From '{$from_name}', updated to '{$to_name}'.","success");
			
			$ret = db_query($sql_2);
			if($ret===false){
				$_SESSION['messageStack']->add("Error occured while updating Allocated To Person in Actions - From '$from_name ($from_id)' To '$to_name ($to_id)'.<br>Error: ".$db->ErrorMsg());
				break;
			}else $_SESSION['messageStack']->add("Allocated To Person in Actions - From '{$from_name}', updated to '{$to_name}'.","success");
			
			$ret = db_query($sql_3);
			if($ret===false){
				$_SESSION['messageStack']->add("Error occured while updating Managed By Person in scheduler - From '$from_name ($from_id)' To '$to_name ($to_id)'.<br>Error: ".$db->ErrorMsg());
				break;
			}else $_SESSION['messageStack']->add("Managed By Person in scheduler - From '{$from_name}', updated to '{$to_name}'.","success");
			
			$ret = db_query($sql_4);
			if($ret===false){
				$_SESSION['messageStack']->add("Error occured while updating Managed By Person in PCR Actions - From '$from_name ($from_id)' To '$to_name ($to_id)'.<br>Error: ".$db->ErrorMsg()."<hr/>$sql_1<hr/>");
				break;
			}else $_SESSION['messageStack']->add("Managed By Person in PCR Actions - From '{$from_name}', updated to '{$to_name}'.","success");
			
			$ret = db_query($sql_5);
			if($ret===false){
				$_SESSION['messageStack']->add("Error occured while updating Allocated To Person in PCR Actions - From '$from_name ($from_id)' To '$to_name ($to_id)'.<br>Error: ".$db->ErrorMsg());
				break;
			}else $_SESSION['messageStack']->add("Allocated To Person in PCR Actions - From '{$from_name}', updated to '{$to_name}'.","success");	
			
			$ret = db_query($sql_6);
			if($ret===false){
				$_SESSION['messageStack']->add("Error occured while updating Default PCR Action Managed By - From '$from_name ($from_id)' To '$to_name ($to_id)'.<br>Error: ".$db->ErrorMsg());
				break;
			}else $_SESSION['messageStack']->add("Default Managed By for a PCR Action - From '{$from_name}', updated to '{$to_name}'.","success");	
			
			break;
		default:
			$_SESSION['messageStack']->add("Action Parameter does not exist.");
			$ret = false;
			break;
	}
	
	if($ret) {
		$success_return_module = 'dashboard';
		$success_remove_page = 'search';
		include(CATS_INCLUDE_PATH . 'success.inc.php');
	} else {
		$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
		include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
		exit;
	}
}else{
	$_SESSION['messageStack']->add("The client-side validation obviously did not work. Both 'Re-allocate From' and 'Re-allocate To' fields must be selected. Please try again.");
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
	//main_redirect("index.php?m=reallocate&p=edit");
}

$db=db_close();
//exit("<br>DONE_POST");
?>