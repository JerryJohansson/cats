<?php


function getPropertiesTables(){
	return array(
		'table'=>'dual', 
		'id'=>'id',
		'view'=>'dual'
	);
}

function getPropertiesFields(){
	return array(
		'from_id' => array('label'=>'Re-allocate From', 'value'=>NULL, 'func'=>'html_draw_employee_all_helper', 'params'=>'title=\"Re-allocate From\"', 'col'=>1, 'col_span'=>2), 
		'to_id' => array('label'=>'Re-allocate To', 'value'=>'', 'func'=>'html_draw_employee_helper_linked', 'params2'=>'"SITE_ID"', 'params'=>'title=\"Re-allocate To\"', 'col'=>1, 'col_span'=>2),
		'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0), 
		'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0), 
		'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
	);
}


$TABLES=getPropertiesTables();
$FIELDS=getPropertiesFields();
?>