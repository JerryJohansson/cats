<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

$search_filter = array(
	'name'=>'Re-Allocate Responsibilty',
	'filter'=>" 1 = 2 ",
	'type' => "reallocate"
);
$module_name = ucwords($search_filter['type']);
$module_name_txt = $search_filter['name'];
$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:0;
// init the tabs index for tab iteration
$iTabs=0;
$iTabButtons = 0;
$RS=array();
?>
<?php  

if(!cats_user_is_super_administrator() && !cats_user_is_pcr_administrator() && !cats_user_is_action_administrator() && !cats_user_is_editor(true)){
	$_SESSION['messageStack']->add("You do not have access to view Re-Allocate Responsibilty");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return CATS_validateForm(this, 'from_id','','R','to_id','','R');">
<?php
// set hidden variables used when sending notification email
// nothing to set yet
?>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
</fieldset>
<fieldset class="tool_bar">
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo ucwords((string)$module_name_txt);?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed=new Editor($m, $p, $a);
//$ed->create_recordset = false;
echo($ed->buildForm($FIELDS));
?>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" class="submit" name="cats::Save" value="Save" >
<input type="button" class="button" name="cats::Cancel" value="Cancel" onclick="doCancel();">
</fieldset>		
</fieldset>

</div>
</form><?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
