<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH . 'sql/actions.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_edit";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
$currentTab=(isset($_REQUEST['t']))?$_REQUEST['t']:0;
$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;

// Security Check
//if(!cats_user_is_editor()  && ($p=='new')){
//	$_SESSION['messageStack']->add("You do not have access to Create a New Incident");
//	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
//	exit;
//}

if($id>0){
	/*Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!*/
	$user_site_id = $_SESSION['user_details']['site_id'];
	$user_site_access = $_SESSION['user_details']['site_access'];
	// have to explicitly select fields because oracle doesn't return time unless you ask it to :(
	if($p=='view'){
		$sql_fields = " * ";
	}else{
		$sql_fields = "INCIDENT_NUMBER, INCDISPLYID, INCIDENTSTATUS, SITE_ID, DEPARTMENT_ID, INITIATED_BY_ID, REPORTEDCOMPANYID, ";
		$sql_fields .= "TO_CHAR(INCIDENT_DATE,'YYYY-MM-DD HH24:MI') INCIDENT_DATE, TO_CHAR(REPORT_DATE,'YYYY-MM-DD HH24:MI') REPORT_DATE, ";
		$sql_fields .= "REPORTED_TO, SUPERINTENDENT_ID, LOCATION, AREA, EQUIPMENT_NO, PRIMARYPERSONID, RELATED_SUPERINTENDENT, PRIMARYCOMPANYID, PERSONS_INVOLVED_DESCRIPTION, ";
		$sql_fields .= "INJURED_PERSON_ID, INJUREDCOMPANYID, INJURED_PERSON_GENDER, INCIDENT_TITLE, INCIDENT_DESCRIPTION, IMMEDIATE_CORRECTIVE_ACTIONS, ";
		$sql_fields .= "INITIALRISKCATEGORY, INITIALRISKLEVEL, SEVERITY_RATING, SUBSTANTIATED, ESTIMATED_COST, RECALCRISKCATEGORY, RECALCRISKLEVEL, ";
		$sql_fields .= "SECTION_ID, SHIFT_ID, ";
		$sql_fields .= "ACTIONS_CREATED(INCIDENT_NUMBER, 'Originating_Incident') CREATEDACTIONS, ACTIONS_CLOSED(INCIDENT_NUMBER, 'Originating_Incident') CLOSEDACTIONS, ";
		$sql_fields .= "INJURIES_CHECKED(INCIDENT_NUMBER) INJURIES, ENVIRONMENT_CHECKED(INCIDENT_NUMBER) ENVIRONMENT, COMMUNITY_CHECKED(INCIDENT_NUMBER) COMMUNITY, QUALITY_CHECKED(INCIDENT_NUMBER) QUALITY, ECONOMIC_CHECKED(INCIDENT_NUMBER) ECONOMIC";
		
	}
	$sql = "SELECT $sql_fields FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	//echo 'SQL: ' . $sql;
	$action_access = true;
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	if($p!='view')
		// see if the user has access to this action by checking if he belongs to any one of the sites from the view
		
		$action_access = cats_check_user_site_access($RECORD['SITE_ID']);
		
	if($action_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}
$RS = array();//add dev for load formdata
?>

<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>tabs.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/incidents.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
//var PAGE = "<?php echo $p;?>";
var INCIDENT_ID = <?php echo $id;?>;
top.gui.edit_checkboxes = new Object();
function init(){
	// initialise the tabbing object
	init_tabs(<?php echo $currentTab;?>);
	// resize editing section
	init_resize_editor();
	// set the maxlength for textareas
	set_textarea_maxlength();
	
	SHOW_ANCHOR=false;
<?php
if($p!='view'){
	echo("	tab_toggle(element('tab_all_incidents'),'edit_checkboxes','Reported_To','Systemic_Cause','Behaviour_Cause');\n");
	if($RECORD['INJURIES']>0) echo("	element('tab_injuries').onclick();\n");
	if($RECORD['ENVIRONMENT']>0) echo("	element('tab_environment').onclick();\n");
	if($RECORD['COMMUNITY']>0) echo("	element('tab_community').onclick();\n");
	if($RECORD['QUALITY']>0) echo("	element('tab_quality').onclick();\n");
	if($RECORD['ECONOMIC']>0) echo("	element('tab_economic').onclick();\n");
}
?>
	
	
	// create document.mousedown handlers
	init_document_handlers();
	
	return true;
}
window.onload = init;
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------
</script>
<style type="text/css">
.tab_off, .tab_on {
	float:none;
	font: bold 11px verdana;
	color: #333333;
}
</style>
</head>
<body class="edit" >
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onSubmit="return CATS_validateForm(this, 'INCDISPLYID','','R','SITE_ID','','R','DEPARTMENT_ID','','R','INITIATED_BY_ID','','R','REPORTEDCOMPANYID','','R','INCIDENT_DATE','','R','REPORT_DATE','','R','REPORTED_TO','','R','SUPERINTENDENT_ID','','R','LOCATION','','R','AREA','','R','PRIMARYPERSONID','','','PRIMARYCOMPANYID','','','INCIDENT_TITLE','','R','INCIDENT_DESCRIPTION','','R','INITIALRISKCATEGORY','','','INITIALRISKLEVEL','','');"<?php
}else{
	?>onSubmit="return CATS_validateForm(this, 'INCDISPLYID','','R','SITE_ID','','R','DEPARTMENT_ID','','R','INITIATED_BY_ID','','R','REPORTEDCOMPANYID','','R','INCIDENT_DATE','','R','REPORT_DATE','','R','REPORTED_TO','','R','SUPERINTENDENT_ID','','R','LOCATION','','R','AREA','','R','PRIMARYPERSONID','','','PRIMARYCOMPANYID','','','INCIDENT_TITLE','','R','INCIDENT_DESCRIPTION','','R','INITIALRISKCATEGORY','','','INITIALRISKLEVEL','','');"<?php
}
?>>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
</fieldset>
<fieldset class="tool_bar">
<?php
if($p=='edit'){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<?php if($p != 'view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php
}else{
	if(cats_user_is_editor()){
?>
<!--<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();"
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>-->
<?php
	}//if(cats_user_is_editor()
}
?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php if($p != 'view'){ ?>

<?php if($p == 'edit'){ ?>
<a
	title="Print Preview"
	href="javascript:_m.view();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>
<?php } ?>
<?php
}else{
?>
<a
	title="Print"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>
<?php
}
?>
<?php
if(cats_user_is_administrator() && $p=='edit'){
?>
<!-- <a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a> -->
<?php
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
//print_r($RS);
$ed = new Editor($m,$p,$a);
if($id>0){
	// echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));comment dev
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,isset($RS[$id])?$RS[$id]:false));
}else{
			echo($ed->buildForm($FIELDS));
}
?>


<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
?>
<input type="button" class="button" name="cats::New" value="New Early Advice Incident" onClick="_m.newModule();" >
<?php	
}
?>
<?php if($p != 'view'){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onclick="preSubmitForm();" >
<?php if($p == 'edit'){ ?>
<!-- <input type="button" class="button" name="cats::View" value="Print Preview" onClick="_m.view();" > -->
<?php } ?>
<!--input type="button" class="button" name="cats::viewnotes" value="View Case Notes" onclick="top.show_edit_screen('injury_case_notes','index.php?INCIDENT_NUMBER=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&INCDISPLYID=<?php echo($RECORD['INCDISPLYID']); ?>&m=injury_case_notes&p=edit&a=edit&rm=incidents&rp=edit&rt=2');" -->
<?php
}else{
	if(cats_user_is_editor()){
		
?>
<!--<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >-->
<!-- <input type="button" class="button" name="cats::Print" value="Print" onClick="printPage();" > -->
<?php
	}
}
?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>
<?php
//if($p=='edit'||$p=='view'){
if($p=='something'){ //this if statement never get executed here. 
?>
<!-- BEGIN:: Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Follow-up Actions for this Incident</a></caption>
<tr id="row_ACTIONS_CREATED">
 <td class="label"><label title="">No. of Actions Created</label></td>
	<td valign="middle"><span id="ACTIONS_CREATED" ><?php echo($RECORD['CREATEDACTIONS']);?></span></td>
 <td class="label"><label title="">No. of Actions Closed</label></td>
	<td valign="middle"><span id="ACTIONS_CLOSED" ><?php echo($RECORD['CLOSEDACTIONS']);?></span></td>
</tr>
<tr>
	<td colspan="4">
<?php
// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","","");

// Follow-up Actions
$action_edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
$col_attributes = array(' width="2%" ','','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date',$action_edit_button);
$sql = "SELECT ACTION_ID,Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date, '$action_edit_button' as edit  FROM VIEW_ACTION_DETAILS  WHERE Report_Id = $id  AND Origin_Table='Originating_Incident'  ORDER BY Action_Title ASC";
//db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
cats_form_pager($sql,$col_headers,$col_attributes,'', '', '', 'ASC', $col_links, false, 1000);
?>
	</td>
</tr>
<?php
if($p=='view'){ // ***************   VIEW   ******************* //
?>
<tr id="row_RECALCRISKCATEGORY">
	<td class="label"><label title="">Re-assessment of the 'Risk of Recurrence' (Risk Category)</label></td>
	<td>
		<?php echo html_display_value('RECALCRISKCATEGORY', $RECORD['RECALC_RISKCAT'], ' title="Re-assessment of Risk (Category)" ');?>
	</td>
	<td class="label"><label title="">Risk Level</label></td>
	<td>
		<?php echo html_display_value('RECALCRISKLEVEL', $RECORD['RECALC_RISKLEVEL'], ' title="Re-assessment of Risk (Level)" ');?>
	</td>
</tr>
<?php
}else{ // ***************   EDIT   ******************* //
?>
<tr id="row_RECALCRISKCATEGORY">
	<td class="label"><label title="">Re-assessment of the 'Risk of Recurrence' (Risk Category)</label></td>
	<td>
		<?php echo html_form_draw_risk_category_dd('RECALCRISKCATEGORY', $RECORD['RECALCRISKCATEGORY'], ' title="Re-assessment of Risk (Category)" ');?>
	</td>
	<td class="label"><label title="">Risk Level</label></td>
	<td>
		<?php echo html_form_draw_risk_level_dd('RECALCRISKLEVEL', $RECORD['RECALCRISKLEVEL'], ' title="Re-assessment of Risk (Level)" ');?>
	</td>
</tr>
<?php
}
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
?>
<input type="button" class="button" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php
}
?>
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Followup Actions -->

<!-- BEGIN:: Lost Days for this Incident -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Lost Days</a></caption>
<tr>
	<td>
<?php
// Lost Days
$lost_days_edit_button = "edit:javascript:top.show_edit_screen(\"lost_days\",\"index.php?m=lost_days&p=edit&id=:LOST_DAYS_ID:\")";
//$lost_days_edit_button = "";
$col_attributes = array(' width="5%" ', ' width="20%" ',' width="20%" ',' width="25%" ',' width="25%" ', '');
$col_headers=explode("|","ID|Month/Year|Lost Calendar Days|Lost Rostered Days|Restricted Calendar Days|Injured Person|$lost_days_edit_button");
$sql = "SELECT Lost_Days_Id, Months||'/'||Years, Lost_Days, Rostered_Days, Restricted_Days, Emp_Name, '$lost_days_edit_button' as edit  FROM VIEW_LOST_DAYS  WHERE Incident_Number = $id  ORDER BY Year_Month ASC";
db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php
if($p=='edit'){
	if($RECORD['INJURED_PERSON_ID'] > 0){
?>
<input type="button" class="button" name="cats::new_lostdays" value="New Lost Days" onClick="top.show_edit_screen('lost_days','index.php?INCIDENT_NUMBER=<?php echo($RECORD['INCIDENT_NUMBER']); ?>&INCDISPLYID=<?php echo($RECORD['INCDISPLYID']); ?>&EMP_ID=<?php echo($RECORD['INJURED_PERSON_ID']); ?>&m=lost_days&p=new&a=add&rm=incidents&rp=edit&rid=<?php echo($id); ?>&rt=2');" >
<?php
	}
}
?>
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Lost Days for this Incident -->
<?php
}

if($p=='view'){
	require_once('view.adv.php');
}else{
	require_once('edit.adv.php');
}
?>

<?php if($id==0){ ?>

<SCRIPT  type="text/javascript" >
	
	LoadDept();
	
	function LoadDept()
		{
		
		get_site_department(document.getElementById("SITE_ID"),document.getElementById("DEPARTMENT_ID")); 
		//this delay used because get_site-department need to fill the value .
		setTimeout('setDepartment()',500)
		} 
	
	function setDepartment()
		{
			var i ;
			var DEPARTMENT =document.getElementById("DEPARTMENT_ID");
			var s= <?php $data = $_SESSION['user_details'];echo($data['department_id']); ?>;
		
		for(i=0; i<DEPARTMENT.length;i++)
			{
			 		 
			if(s==Number(DEPARTMENT.options[i].value) )
				{
				DEPARTMENT.options[i].selected = true;
				break;
				}
			}	
		get_department_section(document.getElementById("DEPARTMENT_ID"),document.getElementById("SECTION_ID"));
		}	
		
		

		
</SCRIPT>
<?php } ?>
</div>






<script type='text/javascript'>

<?php

	$sql = "select employee_number as ID, nvl(Related_Superintendent, 0) as TEXT from tblemployee_details ";
	$val = html_db_options($sql, 'ID', 'TEXT', true);

	echo('var employeeSuperintendentMappingArray = [');
	foreach($val as $row){
		echo('[' . $row['id'] . ',' . $row['text'] . '],'); 
	}
	echo('[[ 0 ], [ 0]]];');
	
?>	
	

	$(document).ready(function() {
		$( ".button" ).addClass("btn btn-default btn-sm active");
		$( ".button" ).removeClass("button");
		
		$( ".submit" ).addClass("btn btn-primary btn-sm active");
		$( ".submit" ).removeClass("submit");

		$( ".reset" ).addClass("btn btn-warning btn-sm active");
		$( ".reset" ).removeClass("reset");
		
		
		$("#INITIATED_BY_ID").on('change', function() {
			var selectedInitiated = $("#INITIATED_BY_ID").val();
			for( var i = 0, len = employeeSuperintendentMappingArray.length; i < len; i++ ) {
				var currentVal = employeeSuperintendentMappingArray[i];	
				var empId = currentVal[0];
				var superIntend = currentVal[1];
					
				if( empId == selectedInitiated && superIntend > 0) {
					$("#SUPERINTENDENT_ID").val(superIntend);					
					break;
				}
			}	
		});
		

		$("#PRIMARYPERSONID").on('change', function() {
		
			var selectedInitiated = $("#PRIMARYPERSONID").val();
			
			for( var i = 0, len = employeeSuperintendentMappingArray.length; i < len; i++ ) {
				var currentVal = employeeSuperintendentMappingArray[i];	
				var empId = currentVal[0];
				var superIntend = currentVal[1];
					
				if( empId == selectedInitiated && superIntend > 0) {
					$("#RELATED_SUPERINTENDENT").val(superIntend);	
					setFlagForEmailTronoxSupervisor();						
					break;
				}
			}	
		});
		
		$("#RELATED_SUPERINTENDENT").on('change', function() {
			setFlagForEmailTronoxSupervisor();	
		});
		
		
		
	});		
	
	function setFlagForEmailTronoxSupervisor()
	{
		var empIdToEmail = $("#RELATED_SUPERINTENDENT").val();	
		$("#EMAIL_TRONOX_SUPERINTENDENT_EMP_ID").val(empIdToEmail);	
	}
	
	
</script>


<script language="javascript">

	function preSubmitForm()
	{
		if($("#INCIDENT_DATE_d").val() != "")
			setHiddenDateTime("INCIDENT_DATE");
		if($("#REPORT_DATE_d").val() != "")
			setHiddenDateTime("REPORT_DATE");
	}

</script>


</form>

  
