<?PHP

require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST)>0){
	$_SESSION[$m.'_'.$p] = $_POST;
}else if(isset($_SESSION[$m.'_'.$p])){
	$_POST=$_SESSION[$m.'_'.$p];
}

if(isset($_POST['cats::search'])||isset($_POST['cats::report'])){
	// create the Select String first and check if user can edit or delete
	//$sql = "Select Incdisplyid, Incident_Number, Incident_Date, Incident_Categories, Incident_Title, IncidentStatus, CreatedActions, ClosedActions ";
	$sql = "Select Incdisplyid, Incident_Number, Incident_Date, Incident_Categories, Incident_Title ";
	// create action controls array to hold our buttons
	$action_controls = array();
	if(isset($_POST['cats::report'])){
		if(isset($_POST['cats::report_fields']) && !empty($_POST['cats::report_fields']))
			$sql = "select ".$_POST['cats::report_fields']." ";
		else
			$sql = "Select * ";
	}else{
		if(cats_user_is_editor(true)) {
			// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
			$edit_button = "edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:{$TABLES['id']}:\")";
			// put it into an array so we can merge with column headers later
			$action_controls[] = $edit_button;
			$sql .= ", '$edit_button' as edit ";
		}
//		if(cats_user_is_super_administrator()) {
//			// delete button string can be a button or an action checkbox for each row...
//			// the checkboxes also have a dropdown which is displayed to the right of the grid navigation
//			// The button control sql looks like this:
//			// delete:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=post&a=del&id=:{$TABLES['id']}:\")";
//			$delete_button = "delete:::{$TABLES['id']}:";
//			$action_controls[] = $delete_button;
//			$sql .= ", '$delete_button' as del ";
//		}
	}
	$sql .= " FROM {$TABLES['view']} ";
	
	// specify columns to ignore when drawing the table
	$col_ignore = array('INCIDENT_NUMBER');
	// specify links for column values on each row
	$col_links = array("js|top.view('$m',|{$TABLES['id']}","","","","","");
	
	// column headers and attributes
	// these are used when rendering the search results
	// col_attributes - use this array to attach attributes to the header columns
	//$col_attributes = array('', ' width="10%" ',' width="5%" ',' width="10%" ', ' width="60%" ', ' width="5%" ', ' width="5%" ', ' width="5%" ', '', '');
	$col_attributes = array('', ' width="10%" ',' width="5%" ',' width="15%" ', ' width="70%" ', '', '');
	// col_headers - used to label our heading columns
	// standard incidents results view
	//$col_headers = explode("|","Incident No.||Incident Date|Incident Category/s|Title|Incident Status|Actions Created|Actions Closed");
	$col_headers = explode("|","Incident No.||Incident Date|Incident Category/s|Title");
	
	
	$col_headers = array_merge($col_headers,$action_controls); // append edit and delete buttons if they exist
	
	// the order by column name used for the default sorting criteria
	$order_by_col = "INCIDENT_NUMBER";

	// init the where string used below to append the where clause criteria
	$sql_where = "";
	
	$extra_fields = array();
	$checkboxtype_names = array();
	
	// make an array where keys=posted name and values that match checkboxtype value - would be much easier if they were the same :(
	$checkboxtypes = explode("|","BehaviourCause|CommunityClassification|ContactSource|Economic|EnviroClassification|EnviroImpactBy|EnviroImpactTo|IncidentCategory|IncidentReportType|InjuryClassification|InjuryLocation|InjuryMechanism|InjuryType|InjuryAgency|QualityClassification|ReportedTo|SystemicCause");
	$checkbox_types = explode("|","Behaviour_Cause|Community_Classification|Contact_Source|Economic|Environment_Classification|Environment_ImpactBy|Environment_ImpactTo|Incident_Category|Report_Type|Injury_Classification|Injury_Location|Injury_Mechanism|Injury_Type|Injury_Agency|Quality_Classification|Reported_To|Systemic_Cause");
	// use UPPERCASE if we have to
	//$checkbox_types = explode("|","BEHAVIOUR_CAUSE|COMMUNITY_CLASSIFICATION|ECONOMIC|ENVIRONMENT_CLASSIFICATION|ENVIRONMENT_IMPACTBY|ENVIRONMENT_IMPACTTO|INCIDENT_CATEGORY|INJURY_AGENCY|INJURY_CLASSIFICATION|INJURY_LOCATION|INJURY_MECHANISM|INJURY_TYPE|QUALITY_CLASSIFICATION|REPORT_TYPE|REPORTED_TO|SYSTEMIC_CAUSE");
	//$checkboxtype_names = array_combine($checkbox_types,$checkboxtypes);

	$i=0;
	foreach($checkbox_types as $key){
		$extra_fields[$key]=array('delim'=>"",'operator'=>"=");
		$checkboxtype_names[$key]=$checkboxtypes[$i++];
	}
	$FIELDS = array_merge($FIELDS, $extra_fields );
	$FIELDS['SEVERITY_RATING'] = array('delim'=>"'",'operator'=>"=");
	$FIELDS['INCDISPLYID'] = array('delim'=>"'",'operator'=>"LIKE");
	// Create Where String and append it to the Select String
	foreach($FIELDS as $column_name => $props){
		// the criterias operator I.E operator of <= will look like COLUMN_NAME <= 'SEARCH VALUE'
		$operator = isset($props['operator']) ? " {$props['operator']} " : " = ";
		// the values delimiter I.E a delim of ' will look like 'SEARCH VALUE'
		//$delim = $props['delim'];comment  dev
		$delim = isset($props['delim'])?$props['delim']:'';
		// create the user specific sites criteria and add to the where string
		if($column_name == "SITE_ID" && (!isset($_POST[$column_name]) || empty($_POST[$column_name]))){
			$sql_where .= (($sql_where == "") ? " WHERE ":" AND ") . db_get_user_sites_where_string();
		}else{
			switch($column_name){
				case 'INCIDENT_CATEGORY':
					if((isset($_POST[$column_name]) && !empty($_POST[$column_name]))){
						$column_value = $_POST[$column_name];
						$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
						$sql_where .=  " Incident_Number in (select IC.IncidentID from tblIncident_CheckboxValues IC where IC.checkboxtype = 'IncidentCategory' and IC.CheckboxId in ($column_value)) ";
					}
					break;
				case 'REPORT_TYPE':
					if((isset($_POST[$column_name]) && !empty($_POST[$column_name]))){
						$column_value = $_POST[$column_name];
						$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
						$sql_where .=  " Incident_Number in (select IRT.IncidentID from tblIncident_CheckboxValues IRT where IRT.checkboxtype = 'IncidentReportType' and IRT.CheckboxId in ($column_value)) ";
					}
					break;
				/* use UPPERCASE if we have to
				case 'BEHAVIOUR_CAUSE': case 'COMMUNITY_CLASSIFICATION': case 'ECONOMIC': case 'ENVIRONMENT_CLASSIFICATION': case 'ENVIRONMENT_IMPACTBY':
				case 'ENVIRONMENT_IMPACTTO': case 'INCIDENT_CATEGORY': case 'INJURY_AGENCY': case 'INJURY_CLASSIFICATION': case 'INJURY_LOCATION': 
				case 'INJURY_MECHANISM': case 'INJURY_TYPE': case 'QUALITY_CLASSIFICATION': case 'REPORT_TYPE': case 'REPORTED_TO': case 'SYSTEMIC_CAUSE':
				*/
				case 'Behaviour_Cause': case 'Community_Classification': case 'Contact_Source': case 'Economic': case 'Environment_Classification': case 'Environment_ImpactBy': case 'Environment_ImpactTo': case 'Incident_Category': case 'Injury_Agency': case 'Injury_Classification': case 'Injury_Location': case 'Injury_Mechanism': case 'Injury_Type': case 'Quality_Classification': case 'Report_Type': case 'Reported_To': case 'Systemic_Cause':
				
					if((isset($_POST[$column_name]) && !empty($_POST[$column_name]))){
						$column_value = $_POST[$column_name];
						// create the in clause using an array or comma delimited string
						if(!is_array($column_value)) {
							// using comma delimited string
							// remove single quotes(if any) and make an array using the value
							$column_value = explode( "," , preg_replace("/'/","",$column_value) );
						}
						// create a string with delimiters omitting the outer delimiters as this is added when we
						// finish the in clause off...
						$column_value = implode("$delim,$delim",$column_value);
						// get the checkboxtype name from the checkboxtype_names array created above
						$column_name = $checkboxtype_names[$column_name];
						$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
						$sql_where .=  " Incident_Number in (select B.IncidentID from tblIncident_CheckboxValues B where B.checkboxtype = '$column_name' and B.CheckboxId in ($column_value)) ";
					}
					break;
				case 'cats::report_fields': break;
				case 'TYPE_OF_EMPLOYEE':
					db_get_where_clause($column_name,$sql_where,$operator,"'");
					break;
				default:
					// sql_where is passed by reference
					db_get_where_clause($column_name,$sql_where,$operator,$delim);
					break;
			}
		}
	}
	$search_sql = $sql . $sql_where;
	
	// debug if needed
	//$db->debug=true;
	if($db->debug==true){
		print_r($_POST);
		echo('<br>'.$search_sql);
	}else require_once(CATS_ADODB_PATH . 'cats_csv.php');
	
	
}
?>
<script>
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	// resize result iframe to consume the parent page
	init_resize_results_container();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><form name="results" action="index.php" method="post"><table width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
	<td width="100%">

<!-- BEGIN:: Results Table -->
<?php
if(isset($search_sql)){
	// Search Results
	//db_render_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], true, $order_by_col, 'ASC');
	cats_form_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], $m, $order_by_col, 'ASC', $col_links, $col_ignore);
}else{
	echo("<h2>No Search results</h2>");
}
?>
<!-- END:: Results table -->

	</td>
</tr>
</table>
</form>