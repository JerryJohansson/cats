<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');

//$db->debug=true;
if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;

switch($action){
	case "add": // Add record

		//$db->debug=true;
		echo $action;
		
		// check overlapping Display ID
		$sql = "SELECT COUNT(*) FROM tblincident_details WHERE incdisplyid = '".$_POST['INCDISPLYID']."'";
		if (db_get_one($sql) == 0) {
			// Do the Insert
			$editor = new Editor($m, $p, $a);
			$ret = $editor->insert();

			$id=$db->Insert_ID("incident_details_seq");
			echo("<br><b>id=".$id."</b><br>");

			// Now Update the Timestamp Fields
			if($ret == true){
				//print_r($_POST);
				$ret = update_timestamps("tblincident_details", $id);
				update_audit("tblincident_details_audit", $id);
			}

			} else {
			$ret = false;
			$_SESSION['messageStack']->add("An Early Advice Incident with Incident Number ".$_POST['INCDISPLYID']." already exists.");
		}

/*		if(empty($_POST['INCDISPLYID']) && $id > 0){
			// update INCDISPLYID if it wasn't specified
			$sql = "update tblincident_details set INCDISPLYID = '{$id}/".date('Y')."' where INCIDENT_NUMBER = $id ";
			$db->Execute($sql);
		}
*/

			$empIdToSend = $_REQUEST['EMAIL_TRONOX_SUPERINTENDENT_EMP_ID'];
			echo '$empIdToSend: ' . $empIdToSend;
			if($empIdToSend != null && $empIdToSend != 'null' && $empIdToSend != '1')
			{
				echo '$SENDING: ' . $empIdToSend; // TODO BELOW FUNCTION IS NOT WORKING, JERRY
				EarlyAdviceEmailSend($id, $_POST['PRIMARYPERSONID'], $_POST['EMAIL_TRONOX_SUPERINTENDENT_EMP_ID'], $to,$subject,$message,$attachments=false,$priority=1,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false);
			}


		if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
			$ret = update_documents_sequence($id,"incident_details");
				if($ret == false){
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				}


		if($ret)
			{
				mail_send_this($id);
			$return_js_function = "location.href='index.php?m=$m&p=edit&id=$id';";
				//$return_js_function = "location.href='new.php';"
			}

		break;
	case "edit": // Update node
		//$db->debug=true;
		if($id>0){

			$sql = "SELECT INCIDENT_NUMBER FROM tblincident_details WHERE incdisplyid = '".$_POST['INCDISPLYID']."'";
			if (db_get_one($sql) == $id) {
				$editor = new Editor($m, $p, $a);
				$ret = ($editor->update($id) > 0);

				// Now Update the Timestamp Fields
				if($ret == true){
					//print_r($_POST);
					$ret = update_timestamps("tblincident_details", $id);
				}
			} else {
				$ret = false;
				$_SESSION['messageStack']->add("Another Early Advice Incident with Incident Number ".$_POST['INCDISPLYID']." already exists.");
			}
		}else{
			$ret = false;
			$_SESSION['messageStack']->add("The Operation failed because there was no ID.");
		}
		
		$empIdToSend = $_REQUEST['EMAIL_TRONOX_SUPERINTENDENT_EMP_ID'];
		echo '$empIdToSend: ' . $empIdToSend;
		if($empIdToSend != null && $empIdToSend != 'null' && $empIdToSend != '1')
		{
			echo '$SENDING: ' . $empIdToSend;  //TODO FIX JERRY
			EarlyAdviceEmailSend($id, $_POST['PRIMARYPERSONID'], $_POST['EMAIL_TRONOX_SUPERINTENDENT_EMP_ID'], $to,$subject,$message,$attachments=false,$priority=1,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false);
		}
		
		if($ret)
			$ret = update_checkbox_values_sequence($TABLES['CHECKBOXES'], $id);
			$ret = update_documents_sequence($id,"incident_details");
			if($ret == false)
			{
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
			}
				
		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		if($ret = $editor->delete($id)){
			$sql_del="delete from " . $TABLES['CHECKBOXES']['table'] . " where " . $TABLES['CHECKBOXES']['id'] . " = $id ";
			$ret = db_query($sql_del);
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					$arr = array();
					foreach($to_delete as $val){
						$arr[] = array($val); // array must be in this format to bind to our compiled statement
					}

					
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$arr)){
						$_SESSION['messageStack']->add("Early Advice Incident records deleted successfully.","success");
						// now delete the action category checkbox values using the same delete array
						
						if($ret = db_bulk_delete($TABLES['CHECKBOXES']['table'],$TABLES['CHECKBOXES']['id'],$arr)){
							$_SESSION['messageStack']->add("Incident checkbox values deleted successfully.","success");
						}
					}
					if(!$ret) $_SESSION['messageStack']->add("Errors occurred while deleting incidents.<br>".$db->ErrorMsg());
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");
		$ret = false;
		break;
}
if($db->debug==true) exit;
if($ret) {
	if(isset($_POST['rm']) && !empty($_POST['rm'])){
		main_redirect("index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}");
	}
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
/*
+------------------------------------------------------------------------
|  Functions
+------------------------------------------------------------------------
*/
function update_timestamps($table, $id){
	global $db;
	$ret = true;



	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET incident_date=TO_TIMESTAMP('".$_POST['INCIDENT_DATE']."','YYYY-MM-DD HH24:MI '), report_date=TO_TIMESTAMP('".$_POST['REPORT_DATE']."','YYYY-MM-DD HH24:MI ')";
	$update_sql .= " WHERE incident_number=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}
function update_audit($table, $id){
	global $db;
	$ret = true;



	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE incident_number=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

function update_documents_sequence($id,$type){
	global $TABLES,$db;
	$ret = true;
	// if action category exists then replace the checkbox values
	if(isset($_POST['DOCUMENTLOCATION'])){
		// replace linked records
		$sql_del="delete from " . $TABLES['DOCUMENTLOCATION']['table'] . " where FORMTYPE = '$type' and " . $TABLES['DOCUMENTLOCATION']['id'] . " = $id ";
		$success = db_query($sql_del);
	
		$arr=array();
		$act_cat = $_POST['DOCUMENTLOCATION'];
		$update = false;
		$docerr = false;
		foreach($act_cat as $key => $val){
			if(!empty($val)){
								
				if($val{0}!='\\' or $val{1}!='\\'){			
					$_SESSION['messageStack']->add("Document Location contains path '".$val."' which is an incorrectly formatted network path. The document must be located on the network.");
					$docerr = true;
				}else{
					$arr[] = array($id, $val, $type);//comment dev
					// $mappedValues = array();
					// $mappedValues[strtolower('REPORT_ID')] = $id;
					// $mappedValues[strtolower('DOCUMENTLOCATION')] = $val;
					// $mappedValues[strtolower('FORMTYPE')] = $type;
					// array_push($arr, $mappedValues) ;
					$update = true;
				}
			}
		}
		if($update){
			// insert new checkbox values
			if($ret = db_bulk_insert($TABLES['DOCUMENTLOCATION']['table'],"REPORT_ID,DOCUMENTLOCATION,FORMTYPE",$arr,$db->debug)){//comment dev
			// if($ret = db_bulk_insert_dev($TABLES['DOCUMENTLOCATION']['table'],"REPORT_ID,DOCUMENTLOCATION,FORMTYPE",$arr,$db->debug)){
				$_SESSION['messageStack']->add("Supporting Documents were successfully updated.","success");
			}else{
				$_SESSION['messageStack']->add("An error occured while updating Supporting Document records.<br>".$db->ErrorMsg());
			}
		} // update records is true
		
		// If document path error then set return to false even though some records may have been updated
		if($docerr==true){
			$ret=false;
		}
		
	}
	return $ret;
}

function update_checkbox_values_sequence($tables, $id){
	global $db;
	$ret = true;

	//add dev
	// $mappedFields = array();
	// if(isset($tables['fields'])){
	// 	$flds = explode(",",$tables['fields']);
	// 	for ($i = 0; $i < count($flds); $i++) {
	// 		array_push($mappedFields, strtolower($flds[$i])) ;
	// 	}
	// }
	//end dev

	// get checkbox values array so we can map
	// tblIncidentForm_Checkboxes.Checkbox_Type to tblIncident_CheckboxValues.CheckboxType
	$types = cats_get_checkboxes_values_array();
	// delete * checkbox values and insert the new values if they exist
	$ret = db_query("delete from {$tables['table']} where {$tables['id']}=$id ");
	// loop through types array and check if we have a matching array in the POST
	foreach($types as $key => $chktype){

		if(isset($_POST[$key]) && is_array($_POST[$key])){
			$checkbox = $_POST[$key];

			$arr=array();
			$update = false;
			// only add value to the array if it is not empty
			foreach($checkbox as $k => $chkid){
				if(!empty($chkid)){
					$update = true;
					$arr[] = array($id, $chkid, $chktype);//comment dev
					// $mappedValues = array();
					// $helparr= array($id, $chkid, $chktype);
					// if(!empty($mappedFields)){
					// 	for ($i = 0; $i < count($mappedFields); $i++) {
					// 		$mappedValues[strtolower($mappedFields[$i])] =  $helparr[$i];
							
					// 	}
					// }
					// array_push($arr, $mappedValues);
				}
			}
			if($update){
				// delete the checkbox values by type
				//$ret = db_query("delete from {$tables['table']} where CHECKBOXTYPE='$chktype' and {$tables['id']}=$id ");

				// insert new checkbox values
				if($ret = db_bulk_insert($tables['table'],$tables['fields'],$arr,$db->debug)){//comment dev
				// if($ret = db_bulk_insert_dev($tables['table'],$tables['fields'],$arr,$db->debug)){
					$_SESSION['messageStack']->add("Checkbox ($key) values were successfully updated.","success");
				}else{
					$_SESSION['messageStack']->add("An error occured while updating ($key) checkbox value records.<br>".$db->ErrorMsg());
				}
			} // update records is true
		}
	} // loop: checkbox values array
	return (bool)$ret;
}

			

function mail_send_this($id)
{
	require_once(CATS_CLASSES_PATH . 'mail.class.php');
	//require_once(CATS_FUNCTIONS_PATH . 'sql/actions.php');
	
	$person = $_SESSION['user_details'];
	
	
	$from = "catshelpdesk.v3@tronox.com";
	$subject = "Confirmation of Early Advice Form";
	
	 
	$sql = "SELECT INCDISPLYID,INCIDENT_TITLE,LOCATION, AREA,SITE_ID, EQUIPMENT_NO,INCIDENT_DATE FROM VIEW_INCIDENT_PRINTVIEW WHERE INCIDENT_NUMBER = $id";
	
	
	$RS = db_query($sql);
	$RECORD = db_get_array($RS);
	
	if($RECORD['SITE_ID'] == 3)
		{
			$to = $person['email'] ;
			$cc = "KwinanaSecurity@tronox.com";
			
			//$to = "Dialog1@tronox.com" ;
			//$cc = "Dialog1@tronox.com";
		}	
	else
		{
		$to = $person['email'];
		//$to = "Dialog1@tronox.com";
		}	
	$message = "The following early advice form has been entered into CATS.\n\n";
    $message .= "IR Number:          " . $RECORD['INCDISPLYID'] . "\n";
    $message .= "Incident Title:     " . $RECORD['INCIDENT_TITLE'] . "\n";
    $message .= "Location:           " . $RECORD['LOCATION'] . "\n";
    $message .= "Area/Equip:         " . $RECORD['AREA'] . "/" . $RECORD['EQUIPMENT_NO'] . "\n";
    $message .= "Incident Date:      " . $RECORD['INCIDENT_DATE'] . "\n\n";
	$message .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n\n";
	$message .= " " . WS_PATH . "index.php?m=early_advice&p=view&id=" . $id. "\n";

	// Get the email Configuration
	$sqlx = "SELECT * FROM tblEmailOptions";
	$email_config = db_get_array($sqlx);

//	if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
//		$to = $email_config["EMAIL_TESTADDRESS"];
//		
//		$cc = $email_config["EMAIL_TESTADDRESS"];
//	}
 
		$m= new Mail; // create the mail
		$m->From($from);
		$m->To($to);
		if($RECORD['SITE_ID'] == 3)
			{
			$m->Cc($cc);
			}
		$m->Subject($subject);
		$m->Body($message);
		$m->Priority(4);

		$ret = $m->Send();	// send the mail
		
	
}



// house cleaning to be done
$db=db_close();


?>