<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH . 'mail_functions.php');

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

//$isAllocatedByIdChanged = is_allocated_by_id_changed($id);
	
$isAllocatedByIdChanged = "false";	
if($_POST['REGISTER_ORIGIN'] == 'Investigation Events')
{
	$is_investigation_event = "true";	
	//email_to_jerry($POST, "DEBUG", 'EVENT: ' . $_POST['EVENT_DATE'] . ' ' . $_POST['ACTION_TITLE'] . ' is_investigation_event: ' . $is_investigation_event);
}
else
{
	if($action == "edit")
		$isAllocatedByIdChanged = is_allocated_by_id_changed($id);
}



$ret = false;
switch($action){
	case "add": // Add record
		// $db->debug = true;
		$editor = new Editor($m, $p, $a);
		$ret = $editor->insert();

		
		$id=$db->Insert_ID("tblaction_details_seq");
		// $id= 63572;
		// insert action categories

		if($ret && isset($_POST['REPORT_ID'])){
			$_POST['REASON_FOR_CHANGE'] = 'Follow-up Action added';
			$ret = insert_action_history($_POST['REPORT_ID']);
			update_audit_new("tblaction_details_audit", $id);
			update_timestamps("tblaction_details", $id);
			
		}
		// insert action categories
		if($ret){
			$ret = insert_action_categories($id);
			if($isAllocatedByIdChanged == "true")
				updateAllocatedSetbyId($id);
		}
		// send email
		//if($ret){
		
		//if($is_investigation_event == "false")
			$ret = email_action_send_notification($id,$_POST);
		//}
		break;
	case "edit": // Update node
		//$db->debug=true;
		if($id>0){
			// Remove the Date_Created from the list of variables
			if(isset($_POST['DATE_CREATED']))
				unset($_POST['DATE_CREATED']);

			// Closed date not to be set if status is open
			if($_POST['STATUS']=='Open'){
					$_POST['CLOSING_DATE'] = '';
			}
				
			$editor = new Editor($m, $p, $a);
			$ret = ($editor->update($id) > 0);
			if($isAllocatedByIdChanged == "true")
				updateAllocatedSetbyId($id);
			update_audit_edit("tblaction_details_audit", $id);
			update_timestamps("tblaction_details", $id);
			
			
		}else{
			$ret = false;
			$_SESSION['messageStack']->add("The Operation failed because there was no ID.");	
		}
		
		
		// insert/update action history and categories
		if($ret){
			$ret = insert_action_history($id);
			$ret = insert_action_categories($id);
		}
		
		email_action_send_notification($id,$_POST);
		
		// send email
		if($ret){
			//if($is_investigation_event == "false")
				//$ret = email_action_send_notification($id,$_POST);
			
			$ret = update_pcr_action($id);
		}
		
		
		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		if($ret = $editor->delete($id)){
			$sql_del="delete from " . $TABLES['ACTION_CATEGORY']['table'] . " where " . $TABLES['ACTION_CATEGORY']['id'] . " = $id ";
			$ret = db_query($sql_del);
		}
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				
				$delete = array();
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Action records deleted successfully.","success");
						// now delete the action category checkbox values using the same delete array
						if($ret = db_bulk_delete($TABLES['ACTION_CATEGORY']['table'],$TABLES['ACTION_CATEGORY']['id'],$delete)){
							$_SESSION['messageStack']->add("Action Category records deleted successfully.","success");
						}
					}
					if(!$ret) $_SESSION['messageStack']->add("Errors occurred while deleting  actions.<br>".$db->ErrorMsg());
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");	
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");	
		$ret = false;
		break;
}
if($db->debug==true) exit('<br>debugging done.');
if($ret) {
	if(isset($_POST['rm']) && !empty($_POST['rm'])){
		//$return_js_function = "top._edit_success('{$_POST['rm']}','{$m}','edit','edit','index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}&t={$_POST['rt']}')";
		$return_js_function = "location.href='index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}&t={$_POST['rt']}'";
		//main_redirect("index.php?m={$_POST['rm']}&p={$_POST['rp']}&id={$_POST['rid']}&t={$_POST['rt']}");
		//echo($return_js_function);
		//exit;
	}else{
		$path_info = parse_url($_SERVER['HTTP_REFERER']);
		
		if(is_array($path_info)){
			//print_r($path_info);
			parse_str($path_info['query'],$query);
			//print_r($query);
			$return_js_function = "top._edit_success('actions')";
			//$return_js_function = "top._edit_success('{$query['m']}','{$m}','edit','{$query['p']}','index.php?m={$query['m']}&p={$query['p']}&id={$query['id']}&t={$_POST['rt']}')";
			//echo $return_js_function;
			//exit;
		}
	}
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
/*
+------------------------------------------------------------------------
|  Functions
+------------------------------------------------------------------------
*/
function update_timestamps($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET event_date=TO_TIMESTAMP('".$_POST['EVENT_DATE']."','YYYY-MM-DD HH24:MI ') ";
	$update_sql .= " WHERE action_id=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

function is_allocated_by_id_changed($id){

	$subject  = "DEBUG";
	$issettest = "";
	if(isset($_POST['ALLOCATED_TO_ID'])){
		$issettest = "TRUE";
	}
	$allocatedToId = "";
	$rs = db_query("select * from TBLACTION_DETAILS where ACTION_ID=" . $id);
	while($tab = $rs->FetchRow()){

		$allocatedToIdTmp = $tab['ALLOCATED_TO_ID'];
		$allocatedToId = $allocatedToIdTmp . '';
	}
	$isSame = ($_POST['ALLOCATED_TO_ID'] == $allocatedToId);	
	
	if($isSame == 1)
		return "false";
	else
		return "true";
	
	//$emailMessage = 'ALLOCATED_TO_ID: ' . $_POST['ALLOCATED_TO_ID'] . ' ALLOCATED_SET_BY_ID: ' .$_POST['ALLOCATED_SET_BY_ID'] . ' $issettest: ' . $issettest . ' allocatedToId: ' . $allocatedToId . ' $isSame: ' . $isSame;
	//email_to_jerry($POST, "DEBUG", 'is_allocated_by_id_changed: ' . is_allocated_by_id_changed($id));
}

function insert_action_categories($id){
	global $db;
	global $TABLES;
	
	$ret = true;
	// if action category exists then replace the checkbox values
	if(isset($_POST['ACTION_CATEGORY'])){
		// replace linked records
		$sql_del="delete from " . $TABLES['ACTION_CATEGORY']['table'] . " where " . $TABLES['ACTION_CATEGORY']['id'] . " = $id ";
		$success = db_query($sql_del);

	
		$arr=array();
		$act_cat = $_POST['ACTION_CATEGORY'];
		foreach($act_cat as $key => $val){
			//'ActionId','CheckBoxId','CheckBoxType'='ActionCategory'
			$type='ActionCategory';
			
			array_push($arr, array($id, $val, $type)) ;//comment dev
			// $mappedValues = array();
			// $mappedValues[strtolower('ACTIONID')] = $id;
			// $mappedValues[strtolower('CHECKBOXID')] = $val;
			// $mappedValues[strtolower('CHECKBOXTYPE')] = $type;
			// array_push($arr, $mappedValues) ;

		}

		$success = db_bulk_insert($TABLES['ACTION_CATEGORY']['table'],"ACTIONID,CHECKBOXID,CHECKBOXTYPE",$arr);//comment dev
		// $success = db_bulk_insert_dev($TABLES['ACTION_CATEGORY']['table'],"ACTIONID,CHECKBOXID,CHECKBOXTYPE",$arr);
		if($success){
			$ret = true;
		}else{
			$_SESSION['messageStack']->add("An error occured while updating action category records.<br>".$db->ErrorMsg());
			$ret = false;
		} 
	}
	return $ret;
}

function insert_action_history($id){
	global $db;
	$ret = true;
	// insert action history stuff
	if(isset($_POST['REASON_FOR_CHANGE'])){
		$sql_data = array(
			'ACTION_ID'=>$id,
			'EMPLOYEE_NUMBER'=>$_SESSION['user_details']['user_id'],
			'COMMENTS'=>$_POST['REASON_FOR_CHANGE'],
			'STATUS'=>$_POST['STATUS'],
			'DATE_CHANGED'=>$_POST['DATE_CHANGED']
		);
		$sql = "select * from action_history where 1=2";
		$rs = $db->Execute($sql);
		$sql = $db->GetInsertSQL($rs,$sql_data);
		if($ret = $db->Execute($sql)){
			$_SESSION['messageStack']->add("Action history was successfully added.","success");
		}else{
			$_SESSION['messageStack']->add("An error occured while adding action history.<br>".$db->ErrorMsg());
		}
	}
	return $ret;
}

function updateAllocatedSetbyId($id){
	global $db;
	$sql = "UPDATE tblaction_details SET allocated_set_by_id=". $_SESSION['user_details']['user_id'] . " WHERE action_id=".$id;
	if($ret = $db->Execute($sql)){
	}else{
		$_SESSION['messageStack']->add("An error occured while updating Action details, for Alloceded Set By Id.<br>".$db->ErrorMsg());
	}
	//email_to_jerry($POST, "DEBUG", 'updateAllocatedSetbyId: $sql' . $sql . ' $ret: ' . $ret );
}

function update_audit_new($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE action_id=".$id;

	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}
function update_audit_edit($table, $id){
	global $db;
	$ret = true;

	// set the Update SQL
	$update_sql = "UPDATE ".$table;
	$update_sql .= " SET AUDIT_BY_USER_ID = ".$_SESSION['user_details']['user_id'];
	$update_sql .= " WHERE audit_id >= (select max(audit_id) -1 from ".$table.") and action_id=".$id;
	// Do the Update
	$ret = $db->Execute($update_sql);

	// Return to the caller
	return $ret;
}

function update_pcr_action($id){
	global $db;
	$ret = true;
	// insert action history stuff
	if(isset($_POST['REGISTER_ORIGIN'])){
		// If this is a PCR Action
		if($_POST['REGISTER_ORIGIN'] == 'PCR Action'){
			//if($_POST['STATUS'] == 'Open'){
				//$pcr_status = 'Processed';
			//} else {
				$pcr_status = $_POST['STATUS'];
				// Escape single quotes in PCR Description to avoid upsetting SQL
				//$pcr_description = $_POST['ACTION_TITLE'];
				$pcr_description = preg_replace("/'/", "''", $_POST['ACTION_TITLE']);
				//
				$pcr_managedby = $_POST['MANAGED_BY_ID'];
				$pcr_allocatedto = $_POST['ALLOCATED_TO_ID'];
				$pcr_duedate = $_POST['SCHEDULED_DATE'];
			//}
			
			// build the sql to update the pcr status
			$sql = "UPDATE tblpcr_actions SET status='".$pcr_status."', action_description='".$pcr_description."', managed_by_id=".$pcr_managedby.", allocated_to_id=".$pcr_allocatedto.", due_date='".$pcr_duedate."' WHERE action_register_id=".$id;
			if($ret = $db->Execute($sql)){
				$_SESSION['messageStack']->add("PCR Action details were successfully updated.","success");
			}else{
				$_SESSION['messageStack']->add("An error occured while updating PCR Action details.<br>".$db->ErrorMsg());
			}
		}
	}
	return $ret;
}


// house cleaning to be done
$db=db_close();
?>