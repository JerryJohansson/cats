<?php

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Actions Register',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Action Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'Action Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Actions Register',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLACTION_DETAILS', 
					'id'=>'ACTION_ID',
					'view'=>'VIEW_ACTION_DETAILS',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLACTION_DETAILS', 
					'id'=>'ACTION_ID',
					'view'=>'VIEW_ACTION_DETAILS',
					'filter'=>' 1=2 ',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLACTION_DETAILS', 
					'id'=>'ACTION_ID',
					'view'=>'VIEW_ACTION_DETAILS',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();
	// set default request form values used to store return values to redirect user back to orginating documents etc...
	// this array will merge with the other page fields(edit/new)

	//comment dev
	// if(isset($_GET['rm'])){
	// 	$ret = array(
	// 		'rm' => array('label'=>'Return module', 'value'=>$_GET['rm'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
	// 		'rp' => array('label'=>'Return page', 'value'=>$_GET['rp'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
	// 		'rid' => array('label'=>'Return REPORT_ID', 'value'=>$_GET['rid'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
	// 		'rt' => array('label'=>'Return tab', 'value'=>$_GET['rt'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
	// 	);
	// }

	if(isset($_GET['rm'])){
		$ret = array(
			'rm' => array('label'=>'Return module', 'value'=>$_GET['rm'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
			'rp' => array('label'=>'Return page', 'value'=>$_GET['rp'], 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
			'rid' => array('label'=>'Return REPORT_ID', 'value'=>isset($_GET['rid'])?$_GET['rid']:null, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
			'rt' => array('label'=>'Return tab', 'value'=>isset($_GET['rt'])?$_GET['rt']:null, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0)
		);
	}


	$ret = array_merge($ret, 
		array('rurl' => array('label'=>'Referrer', 'value'=>array_key_exists('HTTP_REFERER', $_SERVER)?$_SERVER['HTTP_REFERER']:'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0))
	);
	switch($page){
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'ORIGIN_TABLE' => array('label'=>'Origin Table', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'REPORT_ID' => array('label'=>'Report Id', 'value'=>'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'ACTION_ID' => array('label'=>'Action No.', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'DATE_CREATED' => array('label'=>'Date Created', 'value'=>'', 'func'=>'html_draw_disabled_field', 'col'=>2, 'col_span'=>1),
					'CRITICAL' => array('label'=>'Critical Action', 'value'=>'1', 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>1),
					'REGISTER_ORIGIN' => array('label'=>'Origin Type', 'value'=>'', 'func'=>'html_form_hidden_display_value', 'col'=>1, 'col_span'=>1), 
					'ACTION_TYPE' => array('label'=>'Action Type', 'value'=>'', 'func'=>'html_form_hidden_display_value', 'col'=>2, 'col_span'=>1), 
					'ACTION_CATEGORY' => array('label'=>'Action Category', 'table'=>'TBLACTION_CHECKBOXVALUES', 'value'=>'', 'func'=>'html_draw_action_category_checkboxset', 'col'=>1, 'col_span'=>2), 
					'EVENT_DATE' => array('label'=>'Timeline Event Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field', 'params2'=>'true', 'col'=>1, 'col_span'=>1), 
					'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
					'ACTION_DESCRIPTION' => array('label'=>'Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'SCHEDULED_DATE' => array('label'=>'Scheduled Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'params2'=>'true', 'col'=>1, 'col_span'=>1), 
					'REMINDER_DATE' => array('label'=>'Reminder Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'params2'=>'true', 'col'=>2, 'col_span'=>1), 
					//'CHANGE_COMMENTS' => array('label'=>'Reason for Change', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'REASON_FOR_CHANGE' => array('label'=>'Reason for Change', 'value'=>'', 'maxlength'=>'2000', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
					'NOTIFY_MANAGED_EMAIL' => array('label'=>'Notify Managed By Person By Email', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>2, 'col_span'=>1), 
					'ALLOCATED_TO_ID' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked_action_confirm', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1), 
					'NOTIFY_ALLOCATED_EMAIL' => array('label'=>'Notify Allocated To Person By Email', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>2, 'col_span'=>1), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_site_linked', 'params2'=>'"DEPARTMENT_ID","SECTION_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1), 
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>2), 
					'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_get_action_status_radio_fields', 'col'=>1, 'col_span'=>2), 
					'CLOSED_BY' => array('label'=>'Closed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
					'CLOSING_DATE' => array('label'=>'Closing Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'params2'=>'true', 'col'=>2, 'col_span'=>1), 
					'RECORD_LOCATION' => array('label'=>'Location Additional Records Filed', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
					'RECORD_COMMENTS' => array('label'=>'Comments', 'value'=>'', 'maxlength'=>'2000', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'ACTION_REFERENCE' => array('label'=>'Action Reference', 'value'=>NULL, 'func'=>'html_form_draw_action_reference_dd', 'col'=>1, 'col_span'=>1) 
					//'DATE_DUE_CHANGED_HISTORY' => array('label'=>'Date Due Changed History', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'new':
			global $m, $p, $a;
			//set isset property as '' dev
			$ret = array_merge($ret, 
				array(
					'ORIGIN_TABLE' => array('label'=>'Origin Table', 'value'=>isset($_REQUEST['ORIGIN_TABLE'])?$_REQUEST['ORIGIN_TABLE']:'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'REPORT_ID' => array('label'=>'Report Id', 'value'=>isset($_REQUEST['REPORT_ID'])?$_REQUEST['REPORT_ID']:'', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'CRITICAL' => array('label'=>'Critical Action', 'value'=>'0', 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2),
					'REGISTER_ORIGIN' => array('label'=>'Origin Type', 'value'=>isset($_REQUEST['REGISTER_ORIGIN'])?$_REQUEST['REGISTER_ORIGIN']:'', 'func'=>'html_form_hidden_display_value', 'col'=>1, 'col_span'=>1), 
					'ACTION_TYPE' => array('label'=>'Action Type', 'value'=>'Follow-up', 'func'=>'html_form_hidden_display_value', 'col'=>2, 'col_span'=>1), 
					'ACTION_CATEGORY' => array('label'=>'Action Category', 'table'=>'TBLACTION_CHECKBOXVALUES', 'value'=>'', 'func'=>'html_draw_action_category_checkboxset', 'col'=>1, 'col_span'=>2), 
					'EVENT_DATE' => array('label'=>'Timeline Event Date/Time', 'value'=>NULL, 'func'=>'html_get_calendar_date_time_field', 'col'=>1, 'col_span'=>1), 
					'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
					'ACTION_DESCRIPTION' => array('label'=>'Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'SCHEDULED_DATE' => array('label'=>'Scheduled Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
					'REMINDER_DATE' => array('label'=>'Reminder Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1), 
					//'CHANGE_COMMENTS' => array('label'=>'Reason for Change', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					//'REASON_FOR_CHANGE' => array('label'=>'Reason for Change', 'value'=>'', 'maxlength'=>'1000', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
					'NOTIFY_MANAGED_EMAIL' => array('label'=>'Notify Managed By Person By Email', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>2, 'col_span'=>1), 
					'ALLOCATED_TO_ID' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_linked_action_confirm', 'params2'=>'"SITE_ID"', 'col'=>1, 'col_span'=>1), 
					'ALLOCATED_SET_BY_ID' => array('label'=>'Allocated Set By UserId', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'NOTIFY_ALLOCATED_EMAIL' => array('label'=>'Notify Allocated To Person By Email', 'value'=>1, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>2, 'col_span'=>1), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_draw_site_linked', 'params2'=>'"DEPARTMENT_ID","SECTION_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department', 'col'=>2, 'col_span'=>1), 
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section', 'col'=>1, 'col_span'=>2), 
					'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_get_action_status_radio_fields', 'col'=>1, 'col_span'=>2), 
					'CLOSED_BY' => array('label'=>'Closed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>1), 
					'CLOSING_DATE' => array('label'=>'Closing Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1), 
					'RECORD_LOCATION' => array('label'=>'Location Additional Records Filed', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
					'RECORD_COMMENTS' => array('label'=>'Comments', 'value'=>'', 'maxlength'=>'2000', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
					'ACTION_REFERENCE' => array('label'=>'Action Reference', 'value'=>NULL, 'func'=>'html_form_draw_action_reference_dd', 'col'=>1, 'col_span'=>1)
					
					//'DATE_DUE_CHANGED_HISTORY' => array('label'=>'Date Due Changed History', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'view':
			$ret = array_merge($ret, 
				array(
					'ACTION_ID' => array('label'=>'Action No.', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
					'DATE_CREATED' => array('label'=>'Date Created', 'value'=>'', 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1),
					'CRITICAL' => array('label'=>'Critical Action', 'value'=>'1', 'func'=>'html_display_yesno_value', 'col'=>2, 'col_span'=>1),
					'REGISTER_ORIGIN' => array('label'=>'Origin Type', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'ACTION_TYPE' => array('label'=>'Action Type', 'value'=>'', 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1), 
					'ACTIONCATEGORY' => array('label'=>'Action Category', 'table'=>'TBLACTION_CHECKBOXVALUES', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'EVENT_DATE' => array('label'=>'Event Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'params2'=>'true', 'col'=>1, 'col_span'=>1), 
					'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'ACTION_DESCRIPTION' => array('label'=>'Action Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'SCHEDULED_DATE' => array('label'=>'Scheduled Date', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'true', 'col'=>1, 'col_span'=>1), 
					'REMINDER_DATE' => array('label'=>'Reminder Date', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'true', 'col'=>2, 'col_span'=>1), 
					'MANAGED_BY' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'ALLOCATED_TO' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1), 
					'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'CLOSED_BY' => array('label'=>'Closed By', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'col'=>1, 'col_span'=>1), 
					'CLOSING_DATE' => array('label'=>'Closing Date', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'true', 'col'=>2, 'col_span'=>1), 
					'RECORD_LOCATION' => array('label'=>'Location Additional Records Filed', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'RECORD_COMMENTS' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTION_REFERENCE' => array('label'=>'Action Reference', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1)
					
					//'DATE_DUE_CHANGED_HISTORY' => array('label'=>'Date Due Changed History', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search': 
		case 'results':
			$ret = array(
				'ACTION_CATEGORY' => array('label'=>'Category', 'delim'=>"'", 'table'=>'', 'value'=>NULL, 'func'=>'html_action_category_dd', 'col'=>1, 'col_span'=>1), 
				'ACTION_TYPE' => array('label'=>'Action Type', 'delim'=>"'", 'value'=>'', 'func'=>'html_action_type_dd', 'col'=>2, 'col_span'=>1), 
				'REGISTER_ORIGIN' => array('label'=>'Origin Type', 'delim'=>"'", 'value'=>'', 'func'=>'html_action_register_origin_dd', 'col'=>1, 'col_span'=>1), 
				'CRITICAL' => array('label'=>'Critical Action', 'value'=>'', 'func'=>'html_yes_no_number_dd', 'col'=>2, 'col_span'=>2),
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=>'"SECTION_ID"', 'col'=>2, 'col_span'=>1), 
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
				'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>1), 
				'ALLOCATED_TO_ID' => array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>2, 'col_span'=>1), 
				'MANBYORALLOCTO' => array('label'=>'Managed By or Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2), 
				'STATUS' => array('label'=>'Status', 'delim'=>"'", 'value'=>'', 'func'=>'html_get_action_status_dd', 'col'=>1, 'col_span'=>2), 
				'SCHEDULED_DATE_FROM' => array('label'=>'From Scheduled Date', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'search_operator'=>'>=', 'col'=>1, 'col_span'=>1), 
				'SCHEDULED_DATE_TO' => array('label'=>'To Scheduled Date', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'search_operator'=>'<=', 'col'=>2, 'col_span'=>1), 
				'CLOSING_DATE_FROM' => array('label'=>'From Closing Date', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'search_operator'=>'>=', 'col'=>1, 'col_span'=>1), 
				'CLOSING_DATE_TO' => array('label'=>'To Closing Date', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'search_operator'=>'<=', 'col'=>2, 'col_span'=>1), 
				'ACTION_ID' => array('label'=>'Action Id', 'operator'=>'IN', 'value'=>NULL, 'func'=>'', 'col'=>1, 'col_span'=>2),
				'INCDISPLYID' => array('label'=>'Incident No.', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1),
				
				
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'ACTION_TITLE',"text" => 'ACTION_TITLE');
	$ret[] = array("id" => 'REGISTER_ORIGIN',"text" => 'REGISTER_ORIGIN');
	$ret[] = array("id" => 'DEPARTMENT_DESCRIPTION',"text" => 'DEPARTMENT_DESCRIPTION');
	$ret[] = array("id" => 'MANAGED_BY',"text" => 'MANAGED_BY');
	$ret[] = array("id" => 'ALLOCATED_TO',"text" => 'ALLOCATED_TO');
	$ret[] = array("id" => 'ACTION_TYPE',"text" => 'ACTION_TYPE');
	$ret[] = array("id" => 'STATUS',"text" => 'STATUS');
	$ret[] = array("id" => 'SCHEDULED_DATE',"text" => 'SCHEDULED_DATE');
	$ret[] = array("id" => 'CLOSING_DATE',"text" => 'CLOSING_DATE');
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>