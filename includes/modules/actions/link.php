<?php
$default_page_url = "index.php";
if(isset($_GET['rdir'])) $rdir_page_url = urldecode($_GET['rdir']);
?>
<link type="text/css" rel="stylesheet" href="<?PHP echo WS_STYLE_PATH;?>top.css"/>

<style>
.trans {
	filter:alpha(opacity=60);
  -moz-opacity:0.60;
  opacity: 0.60;
}
</style>
<style type="text/css" media="print">
#loading_message,
.toolbar,
#top_logo,
#top_personal
{display:none}
</style>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script src="js/jquery.min.js"></script>

<!-- sra -->
<?php include "includes/menu.php"; ?> 


<script> 
/*******************************
Set Global Variables
*******************************/
var HelperObjects=new Object(); // references to elements stored by a helper object
var IFrameHelpers=new Object(); // reference to helper iframes
var IFrameMenus=new Object(); // references to menu iframes
var IFrameDialogs=new Object(); // references to any message dialogs we have created
//
// delay in milliseconds
//
var cjDelay=1200;
//

// Positional variables

// Screen coords
var CATS_SCREEN_OFFSET=82;//105;
var CATS_SCREEN_TOP=77;
var CATS_SCREEN_HEIGHT=0;
var CATS_CURRENT_SCREEN=null; // current iframe element
// toolbars coords
var TOOLBAR_TOP=54;
var TOOLBAR_HEIGHT=25;
var TOOLBAR_Y=TOOLBAR_TOP; //increment by TOOLBAR_HEIGHT after we create toolbar

// Dialog names
var LOADER_DIALOG_ID = 'cover_elements_that_show_through';
var MESSAGE_DIALOG_ID = 'message_dialog';
var SESSION_DIALOG_ID = 'session_dialog';
var ALERT_DIALOG_ID = 'alert_message';

// Dialog z-indexes -
var SESSION_DIALOG_ZINDEX=500001;
var LOADER_DIALOG_ZINDEX=499998;
var MESSAGE_DIALOG_ZINDEX=500002;
var MENU_DIALOG_ZINDEX=500000;
var HELPER_DIALOG_ZINDEX=499997;

function getDocumentElement(o){
	while(o.nodeName!='HTML') o=o.parentNode;
	return o.parentNode;
}

/*
+------------------------------------------------------------------------
|  BEGIN:: Objects and Classes
+------------------------------------------------------------------------
*/

function _session(){
// create a session object which holds session id and also
// provide a timeout function when session times out...popup a message dialog 5 minutes before
// session timeout with a button to press that will do a very quick request and close (this is to keep the session alive)
// if the user does not click the button in time I.E they are not near the machine at the time...
// the message dialog should turn into a login dialog so when the user logs in using this popup
// they will not loose there place. This is good when the user has been working on a large form which could take some time
// and they need more info or something...we don't want them to loose all there hard work so we try and keep them where they are
// to avoid this from happening.
	// session and user details
	this.SID='<?php echo(session_id());?>';
	this.www_root = '<?php echo(WS_BASE_PATH);?>/cats';
	this.groups=parseInt('<?php echo $_SESSION['user_details']['groups'];?>');
	this.login='<?php echo $_SESSION['user_details']['login'];?>';
	this.close_parent=<?php echo (defined('CATS_REUSE_LOGIN') && CATS_REUSE_LOGIN==true)?"false":"true";?>;
	this.user_id=parseInt('<?php echo $_SESSION['user_details']['user_id'];?>');
	this.site_id=parseInt('<?php echo $_SESSION['user_details']['site_id'];?>');
	this.site_access='<?php echo $_SESSION['user_details']['site_access'];?>';
	this.skins_path='<?php echo WS_STYLE_PATH;?>';
	this.initial_directory='<?php echo str_replace("\\","\\\\",$_SESSION['user_details']['site_folder']);?>';
	this.changed=false;
	//
	this.history = new Object();
	this.history.ref = this;
	this.history.items = new Object(); // unique history items
	this.history.all = []; // array of all history items

	this.timeout_offset = (5*60000); // 5 minute buffer so user can request something to keep the session alive;//5000;//
	this.timeout = (<?php echo session_cache_expire();?>*60000)-this.timeout_offset;;//10000-this.timeout_offset;//15000;//
	this.interval=null;
	this.pulse = function(){
		this.interval = top.setInterval(this.message, this.timeout);
	}
	// every page should call this method with the onload
	// it will reset the timer so we don't prematurely popup the timeout message
	this.reset = function(){
		this.clear();
		this.pulse();
	}
	this.clear = function(){
		if(this.interval!=null)top.clearInterval(this.interval);
	}
	this.message = function(){
		// Show a dialog box with a "session is about to end" message in it
		top.gui.session.clear();
		top.gui.session.interval = top.setInterval(top.gui.session.logon, top.gui.session.timeout_offset);
		var msgs = ["Your session is about to end. Please click the OK button below to keep your session alive."];
		top.gui.session.dialog(SESSION_DIALOG_ID,'Session Warning', msgs, false, true);
	}
	this.logon = function(){
		// Show a dialog box with a login form
		top.gui.session.clear();
		var msgs = ["Your session has timed out. Please use the form below to re-submit your \"Login\" and \"Password\".",
							"If you close this window without logging in, you will automatically be logged out and all un-saved work will be lost."];
		top.gui.session.dialog(SESSION_DIALOG_ID,'Login',msgs,true,true);
	}
	this.dialog = function(dialog_id, title,msgs,showLogonForm,msg_p){
		// Show dialog
		var fra = top.IFrameDialogs[dialog_id];
		var win = fra.contentWindow.window;
		var doc = win.document;
		var d=document.body;
		var msg = "";
		var i=0;
		var m_delim1=(msg_p)?"<p>":"";
		var m_delim2=(msg_p)?"</p>":"";
		for( i=0;i<msgs.length;i++) msg+=m_delim1+msgs[i]+m_delim2;
		var ot = doc.getElementById('dialog_title').innerHTML = title;
		var ob = doc.getElementById('dialog_message').innerHTML = msg;
		if(win.show_hide_login) win.show_hide_login(showLogonForm);
		var w=500;
		var x=(d.offsetWidth/2)-(w/2);
		fra.style.left = x;
		fra.style.width = w;
		fra.style.height=win.get_height();
		fra.style.display = 'block';
	}
	this.history.add = function(name,page,url){
		var e,add=true;
		var id=name+"_"+page;
		for(e in this.items){
			if((e)==(id)){
				add=false;
				break;
			}
		}
		this.all.push([name,page,url]);
		if(add) {
			this.items[id]=new Object();
			this.items[id].module = name;
			this.items[id].page = page;
			this.items[id].url = url;
		}
	}
	this.history.back = function(){
		var args=arguments;
		var hist=null;
		var curr=this.all.pop();
		var cnt = this.all.length-1;
		if(args.length>0){
			//alert(args[0]+":"+cnt)
			for(var i=1;i<args[0];i++) this.all.pop();
			//var cnt=((this.all.length-args[0])>=0)?this.all.length-args[0]:0;
			cnt = this.all.length-1;
			//alert(args[0]+":"+cnt)
			hist = this.all[cnt];
		}else hist = this.all[cnt];
		//this.all.push(curr);
		//this.ignore=true;
		//alert(hist);
		//if(typeof(hist[2])=='undefined') top.doMenu(hist[0],hist[1]);
		//else top.doMenu(hist[0],hist[1],hist[2]);
		//alert(this.all.length+"\n"+hist + "\n" + curr);
		if(hist[2]!=''){
			if(
			hist[2]==gui.screens["screen_"+curr[0]+"_"+curr[1]].page.contentWindow.window.location.href
			||
			hist[2]==curr[2]
			){
				hist=this.all.pop();
				hist=this.all.pop();
			}
		}
		if(hist[2]!='') top.doMenu(hist[0],hist[1],hist[2]);
		else top.doMenu(hist[0],hist[1]);
	}
	this.history.display = function(){
		var msgs = [];
		for(e in this.items){
			msgs.push('<a href="javascript:top.show_screen(\''+this.items[e].module+'\',\''+this.items[e].page+'\');_close();" >'+this.items[e].page+" "+this.items[e].module+'</a>');
		}
		/*for(e in this.items){
			msgs.push('<a href="javascript:top.show_screen(\''+this.items[e].module+'\',\''+this.items[e].page+'\');_close();" >'+this.items[e].page+" "+this.items[e].module+'</a>');
		}*/
		top.gui.session.dialog(MESSAGE_DIALOG_ID,'History',msgs,false,false);
	}
	this.reset();
	return this;
}

/*
+------------------------------------------------------------------------
|  Options object used to control SELECT options
+------------------------------------------------------------------------
*/

function _option(name){
	this.name = name;
	this._array = new Array();
	this._options = new Array();
	this._html = "";
	this.options = new Array();

	this.to_array = function(e){
		var i;
		var o=e.options;
		var x=o.length;
		var a=new Array();
		for(i=0;i<x;i++){
			if(o[i].text!="") a.push([o[i].text , o[i].value]);
		}
		return a;
	}
	this.addgroup=function(oSelect, oData, selected){
		var i = 0;
		var value=0;
		var text=1;
		var disable=2;
		var doc = getDocumentElement(oSelect);
		if(oSelect){
			var groups = [];
			var g = 0;
			if(oData[0].length == 1 && typeof(oData[0][0])=='string'){
				for(i=0;i<oData.length;i+=2)
					groups.push([oData[i],oData[i+1]]);
			}else groups.push([false,oData]);

			for(g = 0; g < groups.length; g++){
				//alert(groups[g][0]+":"+groups[g][1])
				var is_g=typeof(groups[g][0][0])=="string";
				if(is_g){
					var o=doc.createElement('OPTGROUP');
					o.label = groups[g][0];
					oSelect.appendChild(o);
				}else{
					var o=oSelect;
				}

				data = groups[g][1];
				for(i = 0; i < data.length; i++){
					oOption = doc.createElement('OPTION');
					o.appendChild(oOption);
					oOption.text = data[i][text];
					oOption.value = data[i][value];
					oOption.disabled = data[i][disable];
					if(oOption.disabled) oOption.style.color = "graytext";
					oOption.selected = (oOption.value==selected);
				}
			}
		}
	}
	this.add = function(cached,el,arr,selected){
		var value=0;
		var text=1;
		var i=0;
		var len=arr.length;

		this._array=this.to_array(el);
		var atop=this._array;
		var olen,add;

		for(i=0;i<len;i++){
			atop=this._array;
			olen=atop.length;
			add = true;
			if(olen==0){
				add = true;
			}else{
				JLOOP:for(j=0;j<olen;j++){
					if(String(atop[j][value])==String(arr[i][value])){
						add=false;
						break JLOOP;
					}
				}
			}
			//dalert(add);
			if(add){
				var o=new Option(arr[i][text],arr[i][value]);
				this.options.push(o);
				if(!cached) this._options.push([arr[i][value],arr[i][text]]);
			}
		}
		el.innerHTML="";
		this.addgroup(el, arr, selected);

		if(typeof(arguments[3])!="undefined" && arguments[3]!=null){
			var id=arguments[3];
			if(typeof(id)=="object")
				id=id.value;
			return this.select(el,id);
		}
	}
	this.compare = function(x,y){
		return x-y;
	}
	this.select = function(el,id){
		var ret='';
		var i=0;
		var opts=el.options;
		var len=opts.length;
		for(i=0;i<len;i++){
			if(String(id)==String(opts[i].value)){
				ret=opts[i].value;
				// Try to Select the option
				try {
					opts[i].selected=true;
				} catch(e) {
					//alert(id+":"+opts[i].value+":"+opts[i].text);
				}
				break;
			}
		}
		return ret;
	}
	this.clear = function(el){
		el.options.length = 0;
		this.options.length = 0;
	}
	this.replace = function(cached,el,arr,selected){
		this.clear(el);
		this.add(cached,el,arr,selected);
	}
}

/*
+------------------------------------------------------------------------
|  Creates the GUI Object which holds screens/toolbars/and the session
+------------------------------------------------------------------------
*/

function _gui(){
	try {
		this.options=new Object(); // holds _option objects for use with helper dropdown object
		this.screens=new Object(); // holds screen objects like the page iframe and toolbars
		this.tabs=new Object(); // holds tab objects for use with tabbed pages
		this.session=new _session(); // holds the current session info
	} catch(e) {
		alert(e);
	}

	try {
		this.shell = new ActiveXObject("Shell.Application"); // global shell object for general shellexecute stuff - BrowseForFolder, ShellExecute etc...
	} catch(e) {
		alert('Folder Popups will not be available as the Shell.Application ActiveX Object could not be loaded.');
	}

//	try {
//		this.dlg = new ActiveXObject("UserAccounts.CommonDialog"); // used for file browsing - we can set the initial directory for this ;)
//	} catch(e) {
//		try {
//			this.dlg = new ActiveXObject("MSComDlg.CommonDialog"); // used for file browsing - we can set the initial directory for this ;)
//		} catch(f) {
//			alert('File Popups will not be available as the Microsoft Common Dialog Control ActiveX Object could not be loaded.');
//		}
//	}
	return this;
}

var gui=new _gui();
top.gui.options.add = function(name){
	if(typeof(top.gui.options[name])!="object")
		top.gui.options[name] = new _option(name);
}
top.gui.options.replace = function(name){
	top.gui.options.remove(name);
	top.gui.options.add(name);
	if(typeof(top.gui.options[name])!="object"){
		top.gui.options[name] = null;
		top.gui.options[name] = new _option(name);
	}
}
top.gui.options.remove = function(name){
	if(top.gui.options[name]) top.gui.options[name] = null;
}

// END:: Objects and Classes
//--------------------------

// Functions that return paths to commonly used areas
function cats_make_path(path,module){
	return path+module+".php";
}

function cats_get_remote_path(m){
	return cats_make_path(CATS_REMOTE_PATH,m);
}

function cats_get_helper_path(m){
	return cats_make_path(CATS_HELPER_PATH,m);
}

function cats_get_path(m){
	return cats_make_path(CATS_PATH,m);
}

//----------------------------------

function get_cats_screen_height(){
	return CATS_SCREEN_HEIGHT;
}

function set_cats_screen_height(){
	CATS_SCREEN_HEIGHT=getAvailableScreenHeight(CATS_SCREEN_OFFSET);
}

// return the available screen space of the page and offset it by the only parameter
function getAvailableScreenHeight(offset){
	var h=(!document.all)?window.innerHeight:document.body.offsetHeight;
	return h-((typeof(offset)=="number")?offset:0);
}

function cats_set_dialog_pos(){
	var a=arguments;
	var o=new Object();
	var d=document.body;
	o.w=(a[0])?a[0]:(d.offsetWidth*.7);
	o.h=(a[1])?a[1]:0;
	o.x=(d.offsetWidth/2)-(o.w/2);
	o.y=70;
	return o;
}

function helper_show_menu(func, module, e, d){
	hideHelpers();
	try{
		var value="";
		if(e!=null) value=e.value;
		var dvalue = "";
		if(d!=null){
			dvalue=d.value;
			if(dvalue.indexOf(",")!=-1) dvalue=dvalue.split(",")[0];
		}
		var doc=document.body;
		w=(doc.offsetWidth*.6);
		h=200;
		x=(doc.offsetWidth/2)-(w/2);
		y=100;
		var obj={
			'a':func,
			'm':module,
			'n':func,
			'p':dvalue,
			'id':value
		};
		var q = remote._query(obj);
		var url=cats_get_helper_path(module)+"?"+q;
		var o=IFrameHelpers[func];
		if(o==null){
			IFrameHelpers[func] = create_iframe(func,url,w,h,x,y,HELPER_DIALOG_ZINDEX);
			if(dvalue!='') HelperObjects[func].initial_search_value = dvalue;
		}else{
			var doit=false;
			if(dvalue!='') {
				doit=(HelperObjects[func].initial_search_value && (HelperObjects[func].initial_search_value.toLowerCase() != dvalue.toLowerCase()));
				HelperObjects[func].initial_search_value = dvalue;
			}
			// force refresh with last argument
			if(arguments.length>4) doit=arguments[4];
			if(doit) o.contentWindow.window.document.location.href=url;
			o.style.display = 'block';
		}
	}catch(e){alert(e);}
}

HelperObjects.set=function(id,o){
	var h=this[id];
	this[id].return_value=o;
	if(h){
		var s="id="+id+"\n";
		for(e in o){
			s+="e="+e+":h="+o[e]+":";
			if(h[e]){
				s+="name="+h[e].type+":h="+h[e]+":";//+h[e].getAttribute('name')
				if(o[e]) {
					if(setFormElement(h[e],o[e]))
						continue;
						//if(h[e].onchange) h[e].onchange();
				} else {
					var name=h[e].getAttribute('name');
					if(name.indexOf("_text")==(name.length-5)) h[e].value='N/A';
					else h[e].value='';
				}
			}
			s+="\n";
		}
		if(this[id].trigger_function) {
			this[id].trigger_function();
			delete this[id].trigger_function;
		}
		//alert(s);
		//this[id]=null;
		hideHelpers();
	}
}

// set the value of a form element or innerHTML of block element
function setFormElement(){
	var args = setFormElement.arguments;
	var value = (args.length>1)?args[1]:"";
	var ret = false;

	if( typeof( args[0] ) == "object" ){
		if((typeof(args[0].type) == 'undefined') && args[0].length){ // assume we have a radio/checkbox set
			if(typeof(args[0][0])=="object"){
				for(i=0;i<args[0].length;i++){
					if( args[0][i].value == value ) {
						args[0][i].checked = true;
						ret = true;
						break;
					}
				}
			}
		}else{
			if( args[0].nodeName.match(/INPUT|TEXTAREA|SELECT|BUTTON/gi) == null ) {
				try{
					args[0].innerHTML=value;
					return true;
				}catch(e){
					alert("The object is not a form element");
					return false;
				}
			}
			switch( args[0].type ){
				case "text": case "textarea": case "hidden": case "password":
					args[0].value = value;
					break;
				case "radio": case "checkbox" :
					if( args[0].value == value )
						args[0].checked;
					break;
				case "button": case "submit": case "reset":
					args[0].value = value;
					break;
				case "select-one":
					for(i=0;i<args[0].options.length;i++){
						if( args[0][i].value == value ) {
							ret = (i!=args[0].selectedIndex);
							args[0][i].selected = true;
							break;
						}
					}
					break;
				default:
					if( args[0].nodeName.toLowerCase() == "select" ){
						for(i=0;i<args[0].options.length;i++){
							if( args[0][i].value == value ) {args[0][i].selected = true;break;}
						}
					}else if( args[0].type.match(/[button|submit|reset]/) ){
						if(args[0].type!="password") if(confirm("are u sure u want to change the value of the button "+args[0].getAttribute("name") +" from "+args[0].getAttribute("title") )) args[0].value = value;
					}
					break;
			}
		}
	}
	return ret;
}

function check_search_module(name){
	return name.match(/index|dashboard|reallocate|email_configuration|form/)==null;
}

function resize_menu_height(){
	for(e in IFrameMenus)
		IFrameMenus[e].style.height = alertMe(get_menu_height(e));
}

function get_menu_height(name){
	return IFrameMenus[name].contentWindow.window.get_height();
}

var MENUS_LOADING=null;

function set_menus_loaded(){
	MENUS_LOADING=null;
}

function showMenu(id){

	pausecomp(cjDelay);
	var name="menu["+id+"]";
	if(MENUS_LOADING) return;
	//hideMenus();
	if( !isset(IFrameMenus[name]) ){
		MENUS_LOADING=true;
		var o=cats_set_dialog_pos();
		var h=0;
		var url = cats_get_helper_path('menus');
		url += "?a=get_menu&id="+id;
		IFrameMenus[name] = create_iframe(name,url,o.w,h,o.x,o.y,MENU_DIALOG_ZINDEX);
	}else
		IFrameMenus[name].style.display = 'block';
}

function show_screen(m,p){

	var args=arguments;
	pausecomp(cjDelay);

	if(p=='back'){
		gui.session.history.back();
		return;
	}

	if(m=='dashboard'){
		//alert(top.gui.session.changed)
		if(top.gui.session.changed){
			doMenu('index','dashboard',{"refresh":true});
			top.gui.session.changed=false;
		}else doMenu('index','dashboard');
	}else{
	    check_access(m);
        if ( DO_TMP_ACTION=="Y" ) {
    		if((args.length>2)){// && (typeof(args[2])=="object")){
    			doMenu(m,p,args[2]);
    		}else{
    			doMenu(m,p);
    		}
        } else {
            if ( site_name=="Bentley" ) {
                alert('This CATS menu '+action_desc+'is no longer available in '+site_name+' - if an incident for '+site_name+' has occurred please contact '+site_name+' Reception');
            }else{
                alert('This CATS menu '+action_desc+'is no longer available in '+site_name+' - please use Incontrol');
            }
        }
	}
}

function show_edit_screen(m,url){
	pausecomp(cjDelay);

	var args=arguments;
    check_access(m);
    if ( DO_TMP_ACTION=="Y" ) {
    	if(args.length>1){
    		doMenu(m,'edit',url);
    	}else{
    		doMenu(m,'search');
    	}
    } else {
        if ( site_name=="Bentley" ) {
            alert('This CATS menu '+action_desc+'is no longer available in '+site_name+' - if an incident for '+site_name+' has occurred please contact '+site_name+' Reception');
        }else{
            alert('This CATS menu '+action_desc+'is no longer available in '+site_name+' - please use Incontrol');
        }
    }

}
function pausecomp(ms) {
ms += new Date().getTime();
while (new Date() < ms){}
}
function check_access (m) {
    site_id=parseInt('<?php echo $_SESSION['user_details']['site_id'];?>');
    site_name=('<?php echo $_SESSION['user_details']['site_name'];?>');
    user_id=parseInt('<?php echo $_SESSION['user_details']['user_id'];?>');
    login='<?php echo $_SESSION['user_details']['login'];?>';

    DO_TMP_ACTION="Y";
    action_desc="";

    // 2:Bentley 1:Cooljarloo 3:Kwinana 5:Chandala
    if ( site_id == 2 ) {
        DO_TMP_ACTION="Y";

		switch( m ){
            case "actions":
                action_desc="(Actions) ";
                 break;
            case "audits_and_inspections":
                action_desc="(Audits & Inspections) ";
                DO_TMP_ACTION="N";
                 break;
            case "early_advice":
                action_desc="(Early Advice Form) ";
                DO_TMP_ACTION="N";
                 break;
            case "statistical_analysis":
                action_desc="(Statistical Analysis Form) ";
                DO_TMP_ACTION="N";
                 break;
            case "government_requirements":
                action_desc="(Government Inspections) ";
                DO_TMP_ACTION="N";
                 break;
            case "major_hazards":
                action_desc="(High Level Business Risks) ";
                DO_TMP_ACTION="N";
                 break;
            case "incidents":
                action_desc="(Incidents) ";
                 break;
            case "investigation":
                action_desc="(Investigation) ";
                 break;
            case "lost_days":
                action_desc="(Lost Days) ";
                DO_TMP_ACTION="N";
                 break;
            case "work_hours":
                action_desc="(Work Hours) ";
                DO_TMP_ACTION="N";
                 break;
            case "meeting_minutes":
                action_desc="(Meeting Minutes) ";
                DO_TMP_ACTION="N";
                 break;
            case "other_reports":
                action_desc="(Other Records) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_maintain_actions":
                action_desc="(PCR Maintain Actions) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_maintain_email":
                action_desc="(PCR Maintain Email) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_maintain_reviewers":
                action_desc="(PCR Maintain Reviewers) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_meetings":
                action_desc="(PCR Meetings) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_search":
                action_desc="(Search PCRS) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_site_areas":
                action_desc="(Maintain PCR Site Areas) ";
                DO_TMP_ACTION="N";
                 break;
            case "schedules":
                action_desc="(Schedules) ";
                DO_TMP_ACTION="N";
                 break;
            case "site_specific":
                action_desc="(Site Specific Obligations) ";
                DO_TMP_ACTION="N";
                 break;
    		default:
                 break;
		}
	}
    // 2:Bentley 1:Cooljarloo 3:Kwinana 5:Chandala
    if ( site_id == 3 ) {
		switch( m ){
            case "actions":
                action_desc="(Actions) ";
                DO_TMP_ACTION="N";
                 break;
            case "audits_and_inspections":
                action_desc="(Audits & Inspections) ";
                DO_TMP_ACTION="N";
                 break;
            case "early_advice":
                action_desc="(Early Advice Form) ";
                DO_TMP_ACTION="N";
                 break;
            case "statistical_analysis":
                action_desc="(Statistical Analysis Form) ";
                DO_TMP_ACTION="N";
                 break;
            case "government_requirements":
                action_desc="(Government Inspections) ";
                DO_TMP_ACTION="N";
                 break;
            case "major_hazards":
                action_desc="(High Level Business Risks) ";
                DO_TMP_ACTION="N";
                 break;
            case "incidents":
                action_desc="(Incidents) ";
                DO_TMP_ACTION="N";
                 break;
            case "investigation":
                action_desc="(Investigation) ";
                DO_TMP_ACTION="N";
                 break;
            case "lost_days":
                action_desc="(Lost Days) ";
                DO_TMP_ACTION="N";
                 break;
            case "work_hours":
                action_desc="(Work Hours) ";
                DO_TMP_ACTION="N";
                 break;
            case "meeting_minutes":
                action_desc="(Meeting Minutes) ";
                DO_TMP_ACTION="N";
                 break;
            case "other_reports":
                action_desc="(Other Records) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_maintain_actions":
                action_desc="(PCR Maintain Actions) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_maintain_email":
                action_desc="(PCR Maintain Email) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_maintain_reviewers":
                action_desc="(PCR Maintain Reviewers) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_meetings":
                action_desc="(PCR Meetings) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_search":
                action_desc="(Search PCRS) ";
                DO_TMP_ACTION="N";
                 break;
            case "pcr_site_areas":
                action_desc="(Maintain PCR Site Areas) ";
                DO_TMP_ACTION="N";
                 break;
            case "schedules":
                action_desc="(Schedules) ";
                DO_TMP_ACTION="N";
                 break;
            case "site_specific":
                action_desc="(Site Specific Obligations) ";
                DO_TMP_ACTION="N";
                 break;
    		default:
                 break;
		}
	}
    if ( DO_TMP_ACTION=="N" ) {
		switch( login.toLowerCase() ){
            case "k_lilje": case "s_hill": case "c_boston":case "p_demiden":
                if ( site_name=="Bentley" ) {
                    alert('This CATS menu '+action_desc+'is no longer available in '+site_name+' - if an incident for '+site_name+' has occurred please contact '+site_name+' Reception');
                }else{
                    alert('This CATS menu '+action_desc+'is no longer available in '+site_name+' - please use Incontrol');
                }
                alert('Access override for '+login.toUpperCase());
                DO_TMP_ACTION="Y";
                break;
            default:
                break;
        }
    }
}
function _edit_success(name){
	var args=arguments;
	
	if(args[1]) _remove_screen(args[1]+"_"+args[2]);
	if(args.length>3){
		show_screen(name,args[3],args[4]);
	}else{
		show_screen(name,'search',{"refresh":true});
	}
}

function _remove_screen(id){
	remove_element("screen_"+id);
	remove_element("tb_btn_"+id);
	delete top.gui.screens["screen_"+id];
}

function edit(m,id){
	var args=arguments;
	var p=(args.length>2)?args[2]:'edit';
	var params=(args.length>3)?'&'+args[3]:'';
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function edit_parent(m,id){
	var args=arguments;
	var p=(args.length>2)?args[2]:'edit';
	var params=(args.length>3)?'&'+args[3]:'';
	//params+="&rchild="+
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function edit_child(m,id){
	var args=arguments;
	var p=(args.length>2)?args[2]:'edit';
	var params=(args.length>3)?'&'+args[3]:'';
	//params+="&rparent="+
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function view(m,id){
	pausecomp(cjDelay);
	var args=arguments;
	var p=(args.length>2)?args[2]:'view';
	var params=(args.length>3)?'&'+args[3]:'';
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function printpreview(m,id){
	var args=arguments;
	var p=(args.length>2)?args[2]:'printpreview';
	var params=(args.length>3)?'&'+args[3]:'';
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function doMenu() {
	pausecomp(cjDelay);

// args=module_name, page, toolbars list delimited by |, url
  hideMenu();
	if (!document.createElement) {return true};
  var url = '';
	var w=h=x=y=0;
	if(arguments[0]){
		var name=arguments[0];
		var page=(arguments[1])?arguments[1]:"search";
		var screen_name = name+"_"+page;
		var toolbar_name = name+"_tools";
		var el_id = "screen_"+screen_name;
		//if(gui.session.history.ignore) gui.session.history.ignore=false; // last link clicked was a back history item
		//else gui.session.history.add(name,page);
		hideScreens(el_id);
		if(top.gui.screens[el_id] && typeof(top.gui.screens[el_id].page)!='undefined'){
			var o=top.gui.screens[el_id].page;
			var otools=top.gui.screens[el_id].toolbars[el_id];
			if(page=='search' && o.style.display!='none')
				o.contentWindow.window.toggle_search();

			h=get_cats_screen_height();
			o.style.height=h;
			o.style.display='block';
			otools.style.display='block';
			//alert(top.gui.session.changed + ":"+ name)
			if(name=="index" && top.gui.session.changed){
				o.contentWindow.window.location.reload();
				top.gui.session.changed=false;
			}
			if(typeof(arguments[2]) == "object"){
				if(arguments[2].refresh==true && page=='search') o.contentWindow.window.document.forms[0].elements['cats::search'].click();
			}
		}else{
			set_cats_screen_height();
			url=cats_get_path('index')+"?m="+name+"&p="+page;
			//url="index.php?m="+name+"&p="+page;
			w="100%";
			h=get_cats_screen_height();
			y=CATS_SCREEN_TOP;
			x=0;
			var change_refresh='';
			if(check_search_module(name)){
				var a=['back','search'];
				var i=0;
				var d,e;
				var s = "";
				if(page!="search") a="back|search".split("|");
				else if (typeof(top.gui.screens['screen_'+name+'_edit'])!='undefined') a.push("edit");//if(element('screen_'+name+'_edit')!=null)
				var len=a.length;

				for(i=0;i<len;i++) s+='<a id="tb_btn_'+name+'_'+a[i]+'" href="javascript:show_screen(\''+arguments[0]+'\',\''+a[i]+'\');" class="'+a[i]+'">'+a[i]+'</a>';
				/*
				if(name=='pcr_search'){
					a=['search','edit','pcr_initial_review','pcr_evaluation','pcr_area_review','pcr_pcrt_review','pcr_extension','pcr_actions','pcr_sign_off','pcr_post_audit'];
					var len=a.length;
					for(i=0;i<len;i++) s+='<a id="tb_btn_'+name+'_'+a[i]+'" href="javascript:show_screen(\''+arguments[0]+'\',\''+a[i]+'\');" class="'+a[i]+'">'+(a[i].replace(/(pcr_)/g,"").replace(/_/g," ").toPropperCase())+'</a>';
				}else{
				}*/
			}else{
				//alert(top.gui.session.changed)
				if(top.gui.session.changed){
					change_refresh = {"refresh":true};
					top.gui.session.changed=false;
				}
			}

			var obj=(typeof(arguments[2]) == "object")?arguments[2]:change_refresh;
			var o = new Object();
			o.page = create_iframe(el_id,url,w,h,x,y);
			o.toolbars = new Object();
			o.toolbars[el_id]=create_toolbar(el_id,s);
			top.gui.screens[el_id] = o;

			if(typeof(obj) == "object"){
				if(obj.refresh==true) o.contentWindow.window.location.reload();
			}
			
		}

		if(top.gui.screens[el_id]) CATS_CURRENT_SCREEN=top.gui.screens[el_id].page;
		if(arguments[2]){
			if(typeof(arguments[2])!="object"){
				if(arguments[2]!='') url=arguments[2];
			}
		}

		if(url!=""){
			f_url=gui.session.www_root+'/'+url;
			if(f_url!=top.gui.screens[el_id].page.contentWindow.window.location.href){
				//alert(top.gui.screens[el_id].page.contentWindow.window.location.href + "\n" + url)
				top._show_message();
				top.gui.screens[el_id].page.contentWindow.window.location.replace(url);
			}
		}
		// add history item
		if(gui.session.history.ignore) gui.session.history.ignore=false; // last link clicked was a back history item
		else// if(el_id != "screen_pcr_search_edit")
		{
			gui.session.history.add(name,page,url);
		}	
	}
}

/**************************************
HIDE REFERENCED ELEMENTS CONTAINED
IN OBJECTS
	This is a helper function which is called
	by other methoeds/functions passing the
	container object that holds reference/s
	to elements you wish to hide
Parameters:
	o{object} = Object which holds reference/s to the elements
	exception{string} = exclude this element from being hidden
***************************************/

function _hide_ref_objects(o,exception){
	_show_hide_ref_objects(o,exception);
}

function _show_ref_objects(o,exception){
	_show_hide_ref_objects(o,exception,true);
}

_show_ref_object=_hide_ref_objects;

function _show_hide_ref_objects(o,exception,show){
	var i=null;
	for(i in o)
		if(exception!=o[i].id)
			o[i].style.display=show?'block':'none';
}

// hides any menu objects displayed
function hideMenu(){
	hideMenus();
}

function hideMenus(){
	if(MENUS_LOADING) return;
	var exception=(arguments[0])?arguments[0]:'';
	_hide_ref_objects(IFrameMenus,exception);
}

// hides any helper objects displayed
function hideHelpers(){
	var exception=(arguments[0])?arguments[0]:'';
	_hide_ref_objects(IFrameHelpers,exception);
}

function hideDialogs(){
	_hide_ref_objects(IFrameDialogs);
}

// hides the iframe/toolbar elements that hold the pages and page action buttons
function hideScreens(){
	var exception=(arguments[0])?arguments[0]:'';
	_hide_screens(exception)
}

function _hide_screens(exception,show){
	var e=null;
	var display = show?'block':'none';
	for(e in top.gui.screens){
		if(exception!=e){
			top.gui.screens[e].page.style.display=display;
			var i=null,x=top.gui.screens[e].toolbars;
			for(i in x) x[i].style.display=display;
		}
	}
}

var popup=null;
window.onload = function(){
	// Create the default Screen Object (Dashboard)
	//doMenu('index','dashboard','<?PHP echo $default_page_url;?>');
	//show_top_header();
	//return; // SRA change, new menu. 30.01.2015 Jerry
	
	// Are we to be redirected
	<?PHP
	
	if(false)
	{
		if(isset($rdir_page_url)){
			// Set the Default Module and Page
			$rdir_module = "index";
			$rdir_page = "search";
			$rdir_length = strlen($rdir_page_url);

			// Get the Redireced Module
			$rdir_start = strpos($rdir_page_url,"m=");
			if($rdir_start > 0) {
				$rdir_start += 2;
				$rdir_end = strpos($rdir_page_url,"&",$rdir_start);
				if($rdir_end === false){
					$rdir_end = $rdir_length - $rdir_start;
				} else {
					$rdir_end = $rdir_end - $rdir_start;
				}
				$rdir_module = substr($rdir_page_url,$rdir_start,$rdir_end);
			}

			// Get the Redirected Page
			$rdir_start = strpos($rdir_page_url,"p=");
			if($rdir_start > 0) {
				$rdir_start += 2;
				$rdir_end = strpos($rdir_page_url,"&",$rdir_start + 2);
				if($rdir_end === false){
					$rdir_end = $rdir_length - $rdir_start;
				} else {
					$rdir_end = $rdir_end - $rdir_start;
				}
				$rdir_page = substr($rdir_page_url,$rdir_start,$rdir_end);
			}
			// Load the Page (which creates the Screen Object)
			echo "doMenu('".$rdir_module."','".$rdir_page."','".$rdir_page_url.".');";
		}
	}
	?>
	//doMenu('index','dashboard','<?PHP echo $default_page_url;?>');

	var o=cats_set_dialog_pos();
	w=document.body.offsetWidth;
	x=0;
	// load the session dialog
	IFrameDialogs[IFRAME_SESSION]=create_iframe(SESSION_DIALOG_ID,'index.php?m=login&p=dialog', 500, 200, o.x, o.y, SESSION_DIALOG_ZINDEX, false);
	IFrameDialogs[IFRAME_MESSAGE]=create_iframe(MESSAGE_DIALOG_ID,'index.php?m=index&p=dialog', 500, 200, o.x, o.y, MESSAGE_DIALOG_ZINDEX, false);
	IFrameDialogs[IFRAME_ALERT]=create_iframe(ALERT_DIALOG_ID,'index.php?m=index&p=message', 500, 200, o.x, o.y, MESSAGE_DIALOG_ZINDEX+1, false);
	IFrameDialogs[IFRAME_LOADER]=create_iframe(LOADER_DIALOG_ID,'about:blank', w, o.h, x, o.y, LOADER_DIALOG_ZINDEX, false);
	IFrameDialogs[IFRAME_LOADER].className = "trans";
	popup = new _alert(IFRAME_ALERT);
	init_document_handlers(false);
	doresize();
	show_top_header();
}

window.onresize = doresize;

function doresize(){
	set_cats_screen_height();
	var h=get_cats_screen_height();
	try{
		if(CATS_CURRENT_SCREEN!=null) CATS_CURRENT_SCREEN.style.height=h;
		if(IFrameDialogs[IFRAME_LOADER]){
			var o=element('loading_message');
			var f=IFrameDialogs[IFRAME_LOADER];
			f.style.top = o.style.top = CATS_SCREEN_TOP;
			f.style.height = o.style.height = h;
		}
	}catch(e){}
}

function show_top_header(){
	element("top_logo").style.display = 'none';
	element("top_personal").style.display = 'block';
	element("page_title").style.visibilty = 'block';
}

function _alert(frame){
	this.fra = IFrameDialogs[frame];
	this.win = this.fra.contentWindow.window;
	this.width = 500;
	this.height = 200;
	this.x = 100;
	this.y = 100;

	this.show = function(s){
		var d=document.body;
		this.height = this.win.get_height();
		this.x=(d.offsetWidth/2)-(this.width/2);
		this.fra.style.left = this.x;
		this.fra.style.width = this.width;
		this.fra.style.height = this.height;
		this.message(s);
		this.fra.style.display = 'block';
	}
	this.message = function(s){
		this.win.element('loading_message_msg').innerHTML = (s!='')?s:'Loading...';
	}
	this.hide = function(){
		this.win.element('loading_message_msg').innerHTML = 'Loading...';
		this.fra.style.display = 'none';
	}
	return this;
}

function _show_message(s){
//return;
	if(IFrameDialogs[IFRAME_LOADER]){
		IFrameDialogs[IFRAME_LOADER].style.display = 'block';
	}
	var o=element('loading_message');
	o.style.visibility = '';
	if(s){
		d=element('loading_message_msg');
		d.innerHTML = s;
	}
}

function _hide_message(){
	element('loading_message_msg').innerHTML = 'Loading...';
	element('loading_message').style.visibility='hidden';
	if(IFrameDialogs[IFRAME_LOADER]){
		IFrameDialogs[IFRAME_LOADER].style.display = 'none';
	}
}

function loaded(){
	_hide_message();
}

function loading(){
	var o=IFrameDialogs[MESSAGE_DIALOG_ID];
	var win=o.contentWindow.window;
	var doc=win.document;
	doc.getElementById('dialog_title').innerHTML="Loading...";
	doc.getElementById('dialog_message').innerHTML="Please wait while document is loading"
	o.style.display = 'block';
}

function _account(){
	doMenu('employees','edit','index.php?m=employees&p=edit&id=<?php echo $_SESSION['user_details']['user_id'];?>');
}

onunload = function(){
	if(window.opener){
		window.opener.location.href="index.php?m=login&p=logout";
		try{
			if(gui.session.close_parent){
				window.opener.focus();
			}else{
				window.opener.focus();
			}
		}catch(e){
			//window.opener.setFocus();
		}
	}
}

function open_bugz_tracker(){
	var f=document.forms[0];
	var w=window.open("","bugz");
	f.submit();
	if(w.focus) w.focus();
}
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="top" scroll="no">
<?php include "includes/loading_message.inc.php"; ?>
<div id="top_personal" class="bar">
<a
	title="Logout"
	class="top_right"
	href="javascript:window.close();"
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_close_bg.gif);"></a>
<a
	title="My Employee Details"
	class="top_right"
	href="javascript:_account();"
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_my_employee_details_bg.gif);"></a>
<a
	title="My History"
	class="top_right"
	href="javascript:gui.session.history.display();"
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_my_history_bg.gif);"></a>
<a
	title="Submit a Bug/Feature Request"
	class="top_right"
	href="javascript:open_bugz_tracker();"
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_bug_bg.gif);"></a>
<a
	href="javascript:_hide_message();"
	class="top_right"
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_corner_bg.gif);"></a>
</div>
<div id="top_logo" style="display:none;">

</div>
<div style="display: none">
<form method="post" action="https://tronox.topdesk.net/" target="bugz">
</form>
</div>
</div>

<script>
<?php
	echo("var actionId = '") . $_GET['id'] . "';";
?>

	$(document).ready(function() {
			top.show_edit_screen("actions","index.php?m=actions&p=edit&id=" + actionId);
			_hide_message();
	});		
</script>