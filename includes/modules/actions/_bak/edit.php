<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_edit";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;
$currentTab=(isset($_REQUEST['t']))?$_REQUEST['t']:0;
$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;
if($id>0){
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	if(cats_user_is_reader() && $p=='view')
		$action_access =true;
	if($p=='edit' && cats_user_is_reader()){
		if(!($action_access = ($RECORD['MANAGED_BY_ID']==$_SESSION['user_details']['user_id'])||($RECORD['ALLOCATED_TO_ID']==$_SESSION['user_details']['user_id'])))
			// see if the user has access to this action by checking if he belongs to any one of the sites from the view
			$action_access = cats_check_user_site_access(array($RECORD['SITE_ID'],$RECORD['C_SITE_ID'],$RECORD['M_SITE_ID'],$RECORD['A_SITE_ID']));
	}
	if($action_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}else{
	if($a=='add'){
		$_GET["report_id"] = isset($_REQUEST['rid'])?$_REQUEST['rid']:0;
	}
	// To record the parent doc id if this Action was not created from the Actions filter form
	$_POST['REPORT_ID'] = $id;
	if(isset($_GET["report_id"])) { 
		$_POST['REPORT_ID'] = $_GET["report_id"];
	}
	// To record the parent database if this Action was not created from the Actions filter form
	$_POST['ORIGIN_TABLE'] = "Originating_Action";
	if(isset($_GET["origin_table"])) { 
		$_POST['ORIGIN_TABLE'] = $_GET["origin_table"];
	}
	
	$_POST['REGISTER_ORIGIN'] = "Action";
	if(isset($_GET["register_origin"])) { 
		$_POST['REGISTER_ORIGIN'] = $_GET["register_origin"];
	}
	
	$_POST['ACTION_TYPE'] = "Follow-up";
	if(isset($_GET["action_type"])) { 
		$_POST['ACTION_TYPE'] = $_GET["action_type"];
	}
	$FIELDS['REGISTER_ORIGIN']['value'] = 'Action';
	$FIELDS['REGISTER_ORIGIN']['value'] = 'Action';
	
}
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs(<?php echo $currentTab;?>);
	// resize editing section
	init_resize_editor();
	<?php
	if($p=='edit') echo 'PrivilegeLevel();';
	?>
	<?php if($p!="view"){ ?>
	_show_rows();
	set_textarea_maxlength();
	<?php } ?>
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
window.onload = init;
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

function _form_items_disable(form, disable){
	_elements_disable('INPUT',disable);
	_elements_disable('TEXTAREA',disable);
	_elements_disable('SELECT',disable);
	_elements_disable('IMG',disable, 'type');
}

//show or hide certain fields depending on the users privilege level
function PrivilegeLevel(){
	var f = document.forms[0];
	var Group = top.gui.session.groups;//"<?php echo $_SESSION['user_details']['groups']; ?>";
	var EmpNo = top.gui.session.user_id;//"<?php echo $_SESSION['user_details']['user_id']; ?>";
	var Allocated = "<?php echo $RECORD['ALLOCATED_TO_ID']; ?>";
	var Managed = "<?php echo $RECORD['MANAGED_BY_ID']; ?>";
	var Edit = true;
	var s ='';
	Group = parseInt(Group);
	//Lets see if this user can edit the action details
	// Permission is granted to edit if you are in the following groups:
	// Administrators/Super Administrators/Privileged
	// and if you are 
	if(
			((Group & CATS_SUPER_ADMINISTRATOR)==0) && // user is NOT part of super admin group
			((Group & CATS_ADMINISTRATOR)==0) && // user is NOT part of admin group
			(EmpNo != Managed)){ // user is not the manager of this action
		//disable the form fields
		_form_items_disable(f, true);
		f.elements['cats::New'].disabled = false;
		//f.Print_Preview.disabled = false;
		f.elements['cats::Cancel'].disabled = false;
		Edit = false;
	}

	//lets see if this user can close the action
	if( EmpNo == Managed || EmpNo == Allocated || (Group & (CATS_SUPER_ADMINISTRATOR+CATS_ADMINISTRATOR))>0 ){
		if(Edit == false){//enable the closed fields
			f.elements['cats::Save'].disabled = false;
			f.STATUS[0].disabled = false;
			f.STATUS[1].disabled = false;
			f.STATUS[2].disabled = false;
			f.CLOSING_DATE_d.disabled = false;
			f.CLOSING_DATE.disabled = false;
			f.CLOSED_BY.disabled = false;
			f.RECORD_LOCATION.disabled = false;
			f.RECORD_COMMENTS.disabled = false;
		}
	}
<?php
$origin = isset($RECORD['ORIGIN_TABLE'])?$RECORD['ORIGIN_TABLE']:$_POST['ORIGIN_TABLE'];
if(!cats_actions_closed($origin)){
	
	echo ('
			f.STATUS[1].disabled = true;
			f.STATUS[2].disabled = true;
			
			var d=document.createElement("DIV");
			d.innerHTML = "<b>NOTE:</b> There are <a href=\'javascript:init_tabs(2);\'>follow-up actions</a> to close before you can close this action";
			d.className = "status";
			d.style.cssText = "color: red; border: 1px dotted;";
			f.STATUS[2].parentNode.appendChild(d);
	');
}
?>
}

function ValidateChangeDate() {
	if((top.gui.session.groups & CATS_ACTION_ADMINISTRATOR)>0) {
		alert("Please Remember to enter Date Required Changed Comments");
		var oChangeComments = MM_findObj("CHANGE_COMMENTS");
		oChangeComments.focus();
	} else {
		alert("You do not have permission to change Scheduled Completion Date");
		var oDateField = MM_findObj("SCHEDULED_DATE");
		var oOldDateField = MM_findObj("OLD_SCHEDULED_DATE");
		oDateField.value = oOldDateField.value;
	}
}

function timeout_remind_admin(itm){
	if(itm.getAttribute("olddate")!=itm.value) {
		itm.setAttribute("olddate",itm.value);
		alert("Please Remember to enter Date Required Changed Comments");
		var oChangeComments = MM_findObj("CHANGE_COMMENTS");
		oChangeComments.focus();
	}
}
function return_remind_admin(itm){
	if((top.gui.session.groups & CATS_ACTION_ADMINISTRATOR)>0) {
		setTimeout('timeout_remind_admin(document.forms.module["'+itm.name+'"]);',100);
	}
	return true;
}
function return_check_admin(){
	if((top.gui.session.groups & CATS_ACTION_ADMINISTRATOR)==0) {
		alert("You do not have permission to change Scheduled Completion Date");
		return false;
	}else return true;
}

function enable_fields(){
	var f = document.forms[0];
	var x = f.elements.length;
	for(i=0; i<x; i++){
		var name=f.elements[i].getAttribute('name');
		if(name) if(name.indexOf("_text")!=(name.length-5)) f.elements[i].disabled = false;
	}
}


//=====================================================================================================================
//
//=====================================================================================================================



// Action functions
function _close_action() {
	var oStatusClosedP = MM_findObj("STATUS[1]");
	var oStatusClosedNP = MM_findObj("STATUS[2]");
	var r1 = MM_findObj("row_CLOSED_BY");
	var r2 = MM_findObj("row_RECORD_LOCATION");
	var r3 = MM_findObj("row_RECORD_COMMENTS");

	if((oStatusClosedP.checked==true) || (oStatusClosedNP.checked==true)) {
		r1.style.display = "";
		r2.style.display = "";
		r3.style.display = "";
		var oClosingDate = MM_findObj("CLOSING_DATE");
		var oClosingDate_d = MM_findObj("CLOSING_DATE_d");
		var oClosedBy = MM_findObj("CLOSED_BY");
		var oClosedBy_d = MM_findObj("CLOSED_BY_text");
		var todayDate = new Date();
		var strTodayDate = todayDate.Format();

		oClosingDate.value = strTodayDate;
		oClosingDate_d.value = todayDate.Format('dd-mmm-yyyy');
		
		oClosedBy.value = "<?php echo $_SESSION['user_details']['user_id']; ?>";
		oClosedBy_d.value = "<?php echo $_SESSION['user_details']['last_name'] .', '. $_SESSION['user_details']['first_name']; ?>";

		var oComments = MM_findObj("RECORD_COMMENTS");
		oComments.value = "Close Action";
		oComments.select();
	}
	var Open = MM_findObj("STATUS[0]");
	if(Open.checked == true){
		r1.style.display = "none";
		r2.style.display = "none";
		r3.style.display = "none";
	}
}
function _show_rows(){
	var o1 = MM_findObj("STATUS[1]");
	var o2 = MM_findObj("STATUS[2]");
	var r1 = MM_findObj("row_CLOSED_BY");
	var r2 = MM_findObj("row_RECORD_LOCATION");
	var r3 = MM_findObj("row_RECORD_COMMENTS");
	var display = ((o1.checked==true) || (o2.checked==true))?'':'none';
	r1.style.display = display;
	r2.style.display = display;
	r3.style.display = display;
}

function _validate_extra(){
	var errors = '';
	var f=document.forms[0];
	var e=f.elements;
	var c=e['ACTION_CATEGORY[]'];
	var i=0;
	var x=c.length;
	var ret=false;
	for(i=0;i<x;i++){
		if(c[i].checked==true){
			ret=true;
			break;
		}
	}
	if(!ret) errors += '- Action Category is required.\n';
<?php
if($action=='edit'){
?>
	var user_login = "<?php echo $_SESSION['user_details']['login'] ;?>";
	var Sched_Date = e["SCHEDULED_DATE"];
	var Old_Sched_Date = '<?php echo db_getdate($RECORD['SCHEDULED_DATE'],false,'j-M-Y');?>';
	var Change_Comments = e["REASON_FOR_CHANGE"];
	var oStatusClosedP = e["STATUS[1]"];
	var oStatusClosedNP = e["STATUS[2]"];
	var Changed = false;
	var Closed = false;
	if (!(Sched_Date.value == Old_Sched_Date.value)) Changed = true;
	if ((oStatusClosedP.checked==true) || (oStatusClosedNP.checked==true)) {
		Closed = true;
		if(empty(e['CLOSING_DATE'].value)) errors += '- Closing Date is required.\n';
	}
	if (oStatusClosedNP.checked==true) {
		Closed = true;
		if(empty(e['RECORD_COMMENTS'].value)) errors += '- Comments is required.\n';
	}
	if(empty(Change_Comments.value)){
		if(Closed) Change_Comments.value = "Action Closed";
		else errors += '- Reason for Change is required';
	}
<?php
}
?>
	return errors;
}

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=actions&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onsubmit="enable_fields(); return CATS_validateForm(this, 'ACTION_TITLE','','R','SCHEDULED_DATE','','R','ACTION_DESCRIPTION','','R','REMINDER_DATE','','R','MANAGED_BY_ID','','R','SITE_ID','','R','DEPARTMENT_ID','','R');"<?php
}else{
	?>onsubmit="return CATS_validateForm(this, 'ACTION_TITLE','','R','SCHEDULED_DATE','','R','ACTION_DESCRIPTION','','R','REMINDER_DATE','','R','MANAGED_BY_ID','','R','SITE_ID','','R','DEPARTMENT_ID','','R');"<?php
}
?>>
<?php
// set action(a) parameter so we don't
// set hidden variables used when sending notification email
echo html_draw_hidden_field('MANAGED_BY_ID_OLD',$RECORD['MANAGED_BY_ID']);
echo html_draw_hidden_field('ALLOCATED_TO_ID_OLD',$RECORD['ALLOCATED_TO_ID']);
echo html_draw_hidden_field('STATUS_OLD',$RECORD['STATUS']);
?>
<h3 id="print"><?php echo($GENERAL['name']); ?></h3>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<?php
if($action=='edit'){
?>
<a
	title="View Originating Records"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/originating.gif);">Originating Documents</a>
<a
	title="View Follow-up Actions"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/followup.gif);">Followup Actions</a>
<a
	title="View Action Histroy"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/history.gif);">Action History</a>
<?php
}
?>
</fieldset>
<fieldset class="tool_bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newAction();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php
if(cats_user_is_administrator()){
?>
<a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<?php
}
?>
<a
	title="Print current <?PHP echo $module_name_txt;?> item"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>	
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_refresh"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed = new Editor($m,$p,$a);
if($id>0){
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
}else{
	echo($ed->buildForm($FIELDS));
}

?>
<fieldset class="tbar" id="hide" style="text-align:right; ">
<?php if($p=='new') echo(html_filters_templates_dd());?>
<?php if($p!="view"){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" >
<?php }elseif(cats_user_is_editor()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onclick="_m.edit();" >
<?php } ?>
<?php
if($action=="edit"){
?>
<input type="button" class="button" name="cats::New" value="New Action" onclick="_m.newAction();" >
<?php
}
?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onclick="_m.cancel();">
</fieldset>		
</fieldset>
<?php
if($p!='new'){
?>
<!-- BEGIN:: Originiating Documents -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<tr>
	<td>
<?php
//echo($RECORD['ORIGIN_TABLE']);
// Originiating Documents
$ORIGIN_MODULES = array(
	'ORIGINATING_PCR_ACTION'=>array('pcr_search','main','t=pcr_actions'), // goto to pcr module and select actions tab
	'Originating_Action'=>array('actions'),
	'Originating_Audit'=>array('audits_and_inspections'),
	'Originating_Government'=>array('government_requirements'),
	'Originating_HazardRegister'=>array('major_hazards'),
	'Originating_Incident'=>array('incidents'),
	'Originating_Meeting'=>array('meeting_minutes'),
	'Originating_Other'=>array('other_reports'),
	'Originating_Schedule'=>array('schedules')
);
$sql="SELECT Type, Report_Id, Site, Department, Report_Date, Employee, Details ";
$col_attributes = array(' width="50" ',' width="40" ',' width="100" ',' width="100" ','','',' width="40%" ',' width="20" ');
$col_headers = array('Type','Report Id','Site','Department','Report Date','Employee','Details');
$arr_mod=$ORIGIN_MODULES[$RECORD['ORIGIN_TABLE']];
$origin_module = $arr_mod[0];
$origin_params = (empty($arr_mod[1]))?'':',"'.$arr_mod[1].'"';
$origin_params .= (empty($arr_mod[2]))?'':',"'.$arr_mod[2].'"';
if(cats_user_is_editor(true)) {
	// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
	//$origin_edit_button = 'edit:javascript:top.show_edit_screen("'.$origin_module.'","index.php?m='.$origin_module.'&p=edit&id=:REPORT_ID:")';
	$origin_edit_button = 'edit:javascript:_m.edit_parent("'.$origin_module.'",":REPORT_ID:"'.$origin_params.')';
	// put it into an array so we can merge with column headers later
	$col_headers[] = $origin_edit_button;
	$sql .= ", '$origin_edit_button' as edit ";
}
// specify links for column values on each row
$col_links = array("js|top.view('$origin_module',|REPORT_ID","","","","","");

$sql .= " FROM ".$RECORD['ORIGIN_TABLE']." WHERE Report_Id = ".$RECORD['REPORT_ID'];
cats_form_pager($sql,$col_headers,$col_attributes,$GENERAL['results'], '', '', '', $col_links, $col_ignore);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" id="hide" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Originiating Documents -->

<!-- BEGIN:: Followup Actions -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td>
<?php
// Follow-up Actions
$sql = "SELECT Action_Id, Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date ";
$col_attributes = array('','','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date');
$col_ignore = array("ACTION_ID");
if(cats_user_is_editor(true)) {
	// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
	//$origin_edit_button = 'edit:javascript:top.show_edit_screen("'.$origin_module.'","index.php?m='.$origin_module.'&p=edit&id=:REPORT_ID:")';
	$edit_button = 'edit:javascript:_m.edit_child("actions",":ACTION_ID:")';
	// put it into an array so we can merge with column headers later
	$col_headers[] = $edit_button;
	$sql .= ", '$edit_button' as edit ";
}
// specify links for column values on each row
$col_links = array("","js|top.view('$m',|ACTION_ID","","","","","");

$sql .= " FROM {$TABLES['view']}  WHERE Report_Id = $id  AND Origin_Table='Originating_Action'  ORDER BY Action_Title ASC";

cats_form_pager($sql,$col_headers,$col_attributes,$GENERAL['results'], '', '', '', $col_links, $col_ignore);
//db_render_pager($sql,$col_headers,$col_attributes);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" id="hide" style="text-align:right; ">
<input type="button" class="button" name="cats::New" value="New..." title="New Follow-up Action"  onclick="_m.newAction();" >
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Followup Actions -->

<!-- BEGIN:: Action History -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td>
<?php
// Action history
$col_attributes = array(' width="20%" ',' width="15%" ',' width="55%" ',' width="10%" ');
$col_headers=array('Employee','Date','Comment','Status');
$sql = "SELECT e.last_name||', '||e.first_name as employee, ".$db->SQLDate(CATS_SQL_DATE_FORMAT_LONG,'h.date_time').", h.comments, h.status FROM action_history h, tblemployee_details e  WHERE e.employee_number = h.employee_number and h.action_id = $id  ORDER BY h.date_time DESC";
db_render_pager($sql,$col_headers,$col_attributes);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" id="hide" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Action History -->
<?php
}
?>
</div>
</form>