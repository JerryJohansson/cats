<?php

require_once ("../../conf.php");

require_once (CATS_FUNCTIONS_PATH . 'main_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'db_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'js_functions.php');

require_once (CATS_FUNCTIONS_PATH . 'mail_functions.php');

require_once (CATS_CLASSES_PATH.'users.php');

require_once (CATS_ADODB_PATH.'adodb.inc.php');


  echo 'postApprovalRejection:' . '<br/>';

  echo 'baseDir: ' . $baseDir . '<br/>';
  echo 'CATS_FUNCTIONS_PATH: ' . CATS_FUNCTIONS_PATH  . '<br/>';
  
  
  echo 'ACTION_ID: ' . $_POST["ACTION_ID"]  . '<br/>';
  echo 'APPROVAL_STATUS: ' . $_POST["APPROVAL_STATUS"]  . '<br/>';
  
  echo 'REJECTION_MESSAGE: ' . $_POST["REJECTION_MESSAGE"]  . '<br/>';
  echo 'REJECTED_BY_USER_ID: ' . $_POST["REJECTED_BY_USER_ID"]  . '<br/>';
  echo 'ALLOCATED_SET_BY_ID: ' . $_POST["ALLOCATED_SET_BY_ID"]  . '<br/>';
  echo 'REF_URL: ' . $_POST["REF_URL"]  . '<br/>';

  

	$config = parse_ini_file("../../conf/".$_SERVER['SERVER_NAME']."/cats_admin.ini");

	foreach ($config as $key => $value) {

		if(!defined($key))

			define($key, $value);

	}

    $conn = OCILogon(DB_USERNAME, DB_PASSWORD, DB_HOSTNAME . "/" . DB_DATABASE);

	$approval_status = $_POST["APPROVAL_STATUS"];
	$allocatedSetById = $_POST["ALLOCATED_SET_BY_ID"];
	$rejectedByUserId = $_POST["REJECTED_BY_USER_ID"];
	if($allocatedSetById == null)
		$allocatedSetById = 0;
	if($rejectedByUserId == null)
		$rejectedByUserId = 0;	
	///////////////////////
	$emailToAlert = "";
	$firstNameToAlert = "";
	$lastNameToAlert = "";
	$firstNameFrom = "";
	$lastNameFrom = "";
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $allocatedSetById);
	while($tab = $rs->FetchRow()){

		$emailToAlert = $tab['EMAIL_ADDRESS'];	
		$firstNameToAlert = $tab['FIRST_NAME'];
		$lastNameToAlert = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $rejectedByUserId);
	while($tab = $rs->FetchRow()){

		$firstNameFrom = $tab['FIRST_NAME'];
		$lastNameFrom = $tab['LAST_NAME'];
	}
	
	
	
	echo 'Dear ' . $emailToAlert . ' ' . $firstNameToAlert . ' ' . $lastNameToAlert . '. ' . $firstNameFrom . ' ' . $lastNameFrom . ' has REJECTED an ACTION assigned by you.';
	///////////////////////
	
	  echo 'APPROVAL_STATUS: ' . $_POST["APPROVAL_STATUS"]  . '<br/>';
  
  echo 'REJECTION_MESSAGE: ' . $_POST["REJECTION_MESSAGE"]  . '<br/>';
  echo 'REJECTED_BY_USER_ID: ' . $_POST["REJECTED_BY_USER_ID"]  . '<br/>';
  echo 'ALLOCATED_SET_BY_ID: ' . $_POST["ALLOCATED_SET_BY_ID"]  . '<br/>';
	
	// Update action
	if($approval_status == "REJECTED")
		$sql = "update CATSDBA.TBLACTION_DETAILS set APPROVAL_STATUS" . " = '" . $_POST["APPROVAL_STATUS"] . "', APPROVAL_REJECT_MESSAGE" . " = '" . $_POST["REJECTION_MESSAGE"] . "' WHERE ACTION_ID = " . $_POST["ACTION_ID"];
	else
		$sql = "update CATSDBA.TBLACTION_DETAILS set APPROVAL_STATUS" . " = 'APPROVED' WHERE ACTION_ID = " . $_POST["ACTION_ID"];
		
    echo "SQL: " . $sql;
	$stmt = OCIParse($conn, $sql);
    OCIExecute($stmt);

	
    OCIFreeStatement($stmt);  

    OCILogoff($conn);	
	
	if($approval_status == "REJECTED")
		email_action_rejection($_POST, $_POST["ACTION_ID"]);
	
	sleep(1);
	
	
?>

<script>
	//top.show_screen('dashboard','search');
	//window.location.href = '<?php echo $_POST["REF_URL"]; ?>';
	window.location.replace("http://" + window.location.hostname + "/index.php");
</script>