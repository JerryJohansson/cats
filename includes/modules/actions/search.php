<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$iTabs=0;
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

window.onload = init;
</script>
</head>
<body>
<table width="100%">
<tr valign="top">
	<td width="100%">		
<form action="index.php?m=<?php echo $m;?>&p=results" target="<?php echo $results_iframe_target;?>" method="post">	
<?php echo html_draw_hidden_field('cats::report_fields',''); ?>
<!-- BEGIN:: Search Table -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="cats::todo" class="search" onClick="preSeachForm(); toggle_search()"><?php 
echo $GENERAL['name'];
if(isset($show_search_results) && $show_search_results){
	echo ' (Click here to show Search Parameters)';
}
?></a></caption>
<tr>
	<td>
		<!-- <div  id="search_form" style="display:<?php  echo isset($search_display)?$search_display:'none';?>"> -->
			<div  id="search_form" ><!--  after discussion  -->
<?php
// Create the search form
$ed=new Editor("edit","admin");
echo($ed->buildForm($FIELDS));
?>
		<fieldset class="tbar" style="text-align:right; ">
<?php 
// uncomment the next line of code to add filter saving feature
// this will allow a user to save user defined filters for this module
//echo(html_filters_templates_dd());

?>
		
		<input type="reset" class="reset" value="Reset" />
		<input type="submit" class="submit" name="cats::search" value="Search" onClick="preSeachForm(); _m.search(this)">
		<!-- <input type="button" class="button" name="cats::new" value="New <?php //echo $module_name_txt;?>" onClick="_m.newModule()"> -->
		<input type="button" class="submit" name="cats::report" value="Export To Excel" onClick="return _m.report(this);">
		<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
		</fieldset>		
		</div>
	</td>
</tr>
</table>

<!-- END:: Search Table -->
</form>		
	</td>
</tr>


<?php
include (CATS_INCLUDE_PATH . 'results_iframe.inc.php');
?>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>

<script language="javascript">
 
	function preSeachForm()
	{
		if($("#SCHEDULED_DATE_FROM_d").val() != "")
			setHiddenDate("SCHEDULED_DATE_FROM");
		if($("#SCHEDULED_DATE_TO_d").val() != "")
			setHiddenDate("SCHEDULED_DATE_TO");
		if($("#CLOSING_DATE_FROM_d").val() != "")
			setHiddenDate("CLOSING_DATE_FROM");
		if($("#CLOSING_DATE_TO_d").val() != "")
			setHiddenDate("CLOSING_DATE_TO");
	}

</script>