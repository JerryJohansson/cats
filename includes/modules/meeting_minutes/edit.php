<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;

// Security Check
if(!cats_user_is_editor()  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create New Meeting Minutes");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

if($id>0){
	/*Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!*/
	$user_site_id = $_SESSION['user_details']['site_id'];
	$user_site_access = $_SESSION['user_details']['site_access'];

	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	$site_id = $RECORD['SITE_ID'];
	$action_access = false;
	if(!empty($user_site_access)){
		$a_sites = explode(",",$user_site_access);
		foreach($a_sites as $site){
			$n = 0+$site;
			if($n == $site_id){
				$action_access = true;
				break;
			}
		}
	}else{
		if($user_site_id == $site_id){
			$action_access = true;
		}
	}
	/*
	if($action_access == false){
		$_SESSION['messageStack']->add("You do not have access to this actions details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}*/
	
	$actions_created = $db->GetOne("SELECT Count(Action_Id) Actions_Created  FROM tblAction_Details  WHERE Report_Id = $id AND Origin_Table = 'Originating_Meeting'");
	$actions_closed = $db->GetOne("SELECT Count(Action_Id) Actions_Closed  FROM tblAction_Details  WHERE Report_Id = $id AND Status != 'Open' AND Origin_Table = 'Originating_Meeting' ");
}
$RS = array();//add dev load data
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = '<?php echo $m;?>';
function init(){
	// initialise the tabbing object
	init_tabs();
	set_textarea_maxlength();
	// create document.mousedown handlers
	init_document_handlers();
	init_resize_editor();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){
	?>onsubmit="return CATS_validateForm(this, 'TYPE_OF_MEETING','','R','DATE_MEETING','','R','SITE_ID','','R','DEPARTMENT_ID','','R','ATTENDEES','','R','LOCATION_OF_MINUTES', '', 'filePath');"<?php
}else{
	?>onsubmit="return CATS_validateForm(this, 'TYPE_OF_MEETING','','R','DATE_MEETING','','R','SITE_ID','','R','DEPARTMENT_ID','','R','ATTENDEES','','R','LOCATION_OF_MINUTES', '', 'filePath');"<?php
}
?>>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<?php
if($action=='edit'){
?>
<a
	title="View Actions for this meeting"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/followup.gif);">Actions For This Meeting</a>
<?php
}
?>
</fieldset>
<fieldset class="tool_bar">
<?php if($p=="edit"){ ?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php } ?>
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php }elseif(cats_user_is_editor()){ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Print current <?PHP echo $module_name_txt;?> item"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>	
<?php
if(cats_user_is_administrator()){
/*
?>
 <a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<?php
*/
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed = new Editor($m,$p,$a);
if($id>0){
	// echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));comment dev
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,isset($RS[$id])?$RS[$id]: false));
}else{
	echo($ed->buildForm($FIELDS));
}
?>

<fieldset class="tbar" id="hide" style="text-align:right; ">
<?php if($p!="view"){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()">
<?php }elseif(cats_user_is_editor() || cats_user_is_super_administrator()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
<?php if($p=="edit"){ ?>
<input type="button" class="button" name="cats::New" value="New <?php echo $module_name_txt;?>" onClick="_m.newModule();" />
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>	
</fieldset>
<?php
if($action=='edit'){
?>

<!-- BEGIN:: Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">

<?php
// Follow-up Actions
$edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&rm=$m&rp=$p&id=:ACTION_ID:\")";
$col_attributes = array('',' width="30%" ',' width="10%" ',' width="10%" ',' width="10%" ',' width="10%" ',' width="10%" ',' width="10%" ','');
$col_headers = array('ID','Action Title','Managed By','Allocated To','Action Type','Status','Scheduled Date','Closing Date',$edit_button);
$sql = "SELECT Action_Id, Action_Title, Managed_By, Allocated_To, Action_Type, Status, Scheduled_Date, Closing_Date, '$edit_button' as edit  FROM View_Action_Details  WHERE Report_Id = $id AND Origin_Table = 'Originating_Meeting'  ORDER BY Action_Title ASC";
db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);

$sql="SELECT Action_Id, Action_Title, Managed_By, Allocated_To, Action_Type, Status, Scheduled_Date, Closing_Date ";
$col_attributes = array(' width="50" ',' width="40" ',' width="100" ',' width="100" ','','',' width="40%" ',' width="20" ');
$col_headers = array('Action Title','Managed By','Allocated To','Action Type','Status','Scheduled Date','Closing Date');
$cols = array(
	'ACTION_ID'		=> array(
		'label'			=>	'Action Id',
		'ignore'		=> true
	),
	'ACTION_TITLE'	=>	array(
		'label'				=>	'Title',
		'attributes'	=>	' width="40%" '
	),
	'MANAGED_BY'		=>	array(
		'label'				=>	'Managed By',
		'attributes'	=>	' width="10%" '
	),
	'ALLOCATED_TO'	=>	array(
		'label'				=>	'Allocated To',
		'attributes'	=>	' width="10%" '
	),
	'ACTION_TYPE'		=>	array(
		'label'				=>	'Type',
		'attributes'	=>	' width="10%" '
	),
	'STATUS'				=>	array(
		'label'				=>	'Status',
		'attributes'	=>	' width="10%" '
	),
	'SCHEDULED_DATE'	=>	array(
		'label'					=>	'Scheduled Date',
		'attributes'		=>	' width="10%" '
	),
	'CLOSING_DATE'	=>	array(
		'label'				=>	'Closing Date',
		'attributes'	=>	' width="10%" '
	)
);
function _render_pager($sql, $cols){
	$module = $arr_mod[0];
	$params = (empty($arr_mod[1]))?'':',"'.$arr_mod[1].'"';
	$params .= (empty($arr_mod[2]))?'':',"'.$arr_mod[2].'"';
	if(cats_user_is_editor(true)) {
		// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
		//$origin_edit_button = 'edit:javascript:top.show_edit_screen("'.$origin_module.'","index.php?m='.$origin_module.'&p=edit&id=:REPORT_ID:")';
		$edit_button = 'edit:javascript:top.edit("'.$module.'",":REPORT_ID:"'.$params.')';
		// put it into an array so we can merge with column headers later
		$col_headers[] = $edit_button;
		$sql .= ", '$edit_button' as edit ";
	}
	// specify links for column values on each row
	$col_links = array("js|top.view('$module',|REPORT_ID","","","","","");
	
	$sql .= " FROM ".$RECORD['ORIGIN_TABLE']." WHERE Report_Id = ".$RECORD['REPORT_ID'];
	cats_form_pager($sql,$col_headers,$col_attributes,$GENERAL['results'], '', '', '', $col_links, $col_ignore, 1000);
}
?>

<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::New" value="New..." onClick="_m.newAction();" >
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>

<!-- BEGIN:: Followup Actions -->

<?php
}
?>
</div>
</form>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">

	function preSubmitForm()
	{
		if($("#DATE_MEETING_d").val() != "")
			setHiddenDate("DATE_MEETING");
	}

</script>