<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Meeting Minutes',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Meeting',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Meeting Minutes',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLMEETING_MINUTES', 
					'id'=>'REPORT_ID',
					'view'=>'VIEW_MEETING_MINUTES',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLMEETING_MINUTES', 
					'id'=>'REPORT_ID',
					'view'=>'VIEW_MEETING_MINUTES',
					'filter'=>' 1=2 ',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLMEETING_MINUTES', 
					'id'=>'REPORT_ID',
					'view'=>'VIEW_MEETING_MINUTES',
					'filter'=>' 1=2 ',
					'ACTION_CATEGORY'=> array('table'=>'TBLACTION_CHECKBOXVALUES','id'=>'ACTIONID')
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();
	switch($page){
		case 'view':
			$ret = array_merge($ret, 
				array(
					//'REPORT_ID' => array('label'=>'Report Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					//'ORIGIN' => array('label'=>'Origin', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'TYPE_OF_MEETING' => array('label'=>'Type Of Meeting', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'DATE_MEETING' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1), 
					'ATTENDEES' => array('label'=>'Attendees', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'OTHER_ATTENDEES' => array('label'=>'Other Attendees', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'ABSENTEES' => array('label'=>'Absentees', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'NOTES' => array('label'=>'Notes', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'LOCATION_OF_MINUTES' => array('label'=>'Location Of Minutes', 'value'=>'', 'func'=>'html_get_file_location_value', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					//'REPORT_ID' => array('label'=>'Report Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
					//'ORIGIN' => array('label'=>'Origin', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'TYPE_OF_MEETING' => array('label'=>'Type Of Meeting', 'value'=>'', 'func'=>'html_form_draw_reftable_meeting_minutes_dd', 'col'=>1, 'col_span'=>2), 
					'DATE_MEETING' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd', 'col'=>2, 'col_span'=>1), 
					'ATTENDEES' => array('label'=>'Attendees', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'OTHER_ATTENDEES' => array('label'=>'Other Attendees', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'ABSENTEES' => array('label'=>'Absentees', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'NOTES' => array('label'=>'Notes', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'LOCATION_OF_MINUTES' => array('label'=>'Location Of Minutes', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'ORIGIN' => array('label'=>'Origin', 'value'=>'Meeting', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'TYPE_OF_MEETING' => array('label'=>'Type Of Meeting', 'value'=>'', 'func'=>'html_form_draw_reftable_meeting_minutes_dd', 'col'=>1, 'col_span'=>2), 
					'DATE_MEETING' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd', 'col'=>2, 'col_span'=>1), 
					'ATTENDEES' => array('label'=>'Attendees', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'OTHER_ATTENDEES' => array('label'=>'Other Attendees', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'ABSENTEES' => array('label'=>'Absentees', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'NOTES' => array('label'=>'Notes', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
					'LOCATION_OF_MINUTES' => array('label'=>'Location Of Minutes', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search': 
		case 'results': //explode("|","Type_of_Meeting|',none,''|Date_Meeting_from|',none,NULL,DATE_FROM|Date_Meeting_to|',none,NULL,DATE_TO|Site_Id|none,none,NULL|Department_Id|none,none,NULL");
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd', 'col'=>2, 'col_span'=>1), 
				'TYPE_OF_MEETING' => array('label'=>'Type Of Meeting', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_reftable_meeting_minutes_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_MEETING_FROM' => array('label'=>'Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'DATE_MEETING_TO' => array('label'=>'Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'TYPE_OF_MEETING',"text" => 'TYPE_OF_MEETING');
	$ret[] = array("id" => 'DATE_MEETING',"text" => 'DATE_MEETING');
	$ret[] = array("id" => 'SITE_DESCRIPTION',"text" => 'SITE_DESCRIPTION');
	$ret[] = array("id" => 'DEPARTMENT_DESCRIPTION',"text" => 'DEPARTMENT_DESCRIPTION');
	$ret[] = array("id" => 'LOCATION_OF_MINUTES',"text" => 'LOCATION_OF_MINUTES');
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>