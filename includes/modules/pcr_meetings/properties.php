<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit PCRT Meeting Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New PCRT Meeting',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'View PCRT Meeting Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search PCRT Meetings',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables(){
	$ret = array();
	$ret = array_merge(
		array(
			'table'=> 'TBLPCR_MEETINGS',
			'id'=> 'MEETING_ID',
			'view'=> 'VIEW_PCR_MEETINGS',
			'PCR'=> array(
				'table'=>'TBLPCR_MEETINGS_PCR_DETAILS',
				'view'=>'VIEW_PCR_MEETINGS_PCR_DETAILS',
				'fields'=>'PCR,MEETING_ID'
			),
			'EMPLOYEE_NUMBER'=> array(
				'table'=>'TBLPCR_MEETINGS_EMP_DETAILS',
				'fields'=>'EMPLOYEE_NUMBER,MEETING_ID'
			)
		)
	);
	return $ret;
}

function getPropertiesFields($page='search'){
	switch($page){
		case 'edit': case 'new':
			$ret = array(
				//'MEETING_ID' => array('label'=>'Meeting ID', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'OTHER_INVITEES' => array('label'=>'Other Invitees', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'ADDITIONAL_COMMENTS' => array('label'=>'Additional Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'MINUTES' => array('label'=>'Minutes of Meetings', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
				'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_draw_pcr_meetings_status_radioset', 'col'=>1, 'col_span'=>2), 
				'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
			);
			break;
		case 'view':
			$ret = array(
				//'MEETING_ID' => array('label'=>'Meeting ID', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'OTHER_INVITEES' => array('label'=>'Other Invitees', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'ADDITIONAL_COMMENTS' => array('label'=>'Additional Comments', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'MINUTES' => array('label'=>'Minutes of Meetings', 'value'=>'', 'func'=>'html_get_file_location_value', 'col'=>1, 'col_span'=>2), 
				'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'MEETING_DATE' => array('label'=>'Meeting Date', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
			);
			break;
		case 'search': case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'PCR_DISPLAY_ID' => array('label'=>'PCR ID', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2),  
				'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_draw_pcr_meeting_status_dd', 'col'=>1, 'col_span'=>2), 
				'MEETING_DATE_FROM' => array('label'=>'Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'MEETING_DATE_TO' => array('label'=>'Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>