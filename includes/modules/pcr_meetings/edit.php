<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = "Maintain PCRT Meetings";
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

// init the tabs and tab buttons index for tab iteration
$iTabs = 0;
$iTabButtons=0;
$RS = array();
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// resize editing section
	init_resize_editor();
	<?php if($p!="view"){ ?>
	// Set the Counters and max restriction on the textarea boxes
	set_textarea_maxlength();
	<?php } ?>
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
onload = function(){
	init();
}
window.onresize = init_resize_editor;

/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return CATS_validateForm(this, 'MEETING_ID','','R','SITE_ID','','R','MINUTES','','filePath','MEETING_DATE','','R');">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?php echo ''.($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<?php if($p!='view'){ ?>
<a
	title="Edit PCRs Reviewed at Meeting"
	id="tab_button[<?php echo ''.($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);"
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);">PCR Details</a>
<a
	title="Edit PCR Team Members and Invited Employees"
	id="tab_button[<?php echo ''.($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);"
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);">Employee Details</a>	
<?php } ?>	
</fieldset>
<fieldset class="tool_bar">
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed = new Editor("edit");

// Check whether to edit or add a new record
if($id > 0)
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
else
	echo($ed->buildForm($FIELDS));
?>
<fieldset class="tbar_btns" style="text-align:right; ">
<?php if($p!='view'){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()"/>
<?php }elseif(cats_user_is_editor() || cats_user_is_super_administrator() || cats_user_is_pcr_administrator()){ ?>
<input type="button" class="button" name="cats::Save" value="Edit" onClick="_m.edit();" />
<?php } ?>
<?php if($action=='edit' || cats_user_is_super_administrator() || cats_user_is_pcr_administrator()){ ?>
<input type="button" class="button" name="cats::New" value="New PCRT Meeting" onClick="_m.newModule();" />
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();" />
</fieldset>		
</fieldset>

<?php if($p!='view'){ ?>
<!-- BEGIN:: PCR Details -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Add/Remove PCRs to be Reviewed</a></caption>
<tr>
	<td>

<?php
//$ed=new Editor($m,$p,$a);
$seq_fields = array(
	'PCR_DISPLAYID' => array('label'=>'PCR', 'value'=>NULL, 'func'=>'html_draw_upper_input_field', 'col'=>0, 'col_span'=>0)
);

$edit_button = "delete:javascript:remove_sequence_row(this, ::)";
$details_sql="SELECT pcr_displayid, '$edit_button' FROM view_pcr_meetings_pcr_details WHERE meeting_id = $id";
//echo (html_get_sequential_control('config', '', $details_sql));
echo ($ed->buildSequentialForm('PCRS', $seq_fields, $details_sql));

?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: PCR Details -->

<!-- BEGIN:: Employee Details -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Add/Remove PCR Team Members and Invited Employees</a></caption>
<tr>
	<td>

<?php
if($p=='new'){
	$seq_fields = array(
		'EMPLOYEE_NUMBER' => array('label'=>'Employee', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>0, 'col_span'=>0)
	);
	$edit_button = "delete:javascript:remove_sequence_row(this, ::)";
	$details_sql="SELECT employee_number, '$edit_button' FROM tblemployee_details WHERE (BITAND(group_mask, 256) != 0) AND site_id=".$_SESSION['user_details']['site_id'];
}else{
	$seq_fields = array(
		'EMPLOYEE_NUMBER' => array('label'=>'Employee', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>0, 'col_span'=>0)
	);

	$edit_button = "delete:javascript:remove_sequence_row(this, ::)";
	$details_sql="SELECT employee_number, '$edit_button' FROM tblpcr_meetings_emp_details WHERE meeting_id = $id";
}
//echo (html_get_sequential_control('config', '', $details_sql));
echo ($ed->buildSequentialForm('Employees', $seq_fields, $details_sql));

?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Employee Details -->

<?php } ?>

</div>
</form>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>

<script language="javascript">

	function preSubmitForm()
	{
		if($("#MEETING_DATE_d").val() != "")
			setHiddenDate("MEETING_DATE");
	}

</script>