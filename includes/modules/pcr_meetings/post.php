<?php

/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id = isset($_REQUEST['id'])?$_REQUEST['id']:false;

$ret = false;
switch($action){
	case "add": // Add record
		$editor = new Editor($m, $p, $a);
		$ret = $editor->insert();
		if($ret) $id=$db->Insert_ID("tblpcr_meetings_seq");
		// Add PCRs
		$ret = set_meeting_pcr_details($id);
		// Add Employees
		$ret = set_meeting_emp_details($id);
		break;
	case "edit": // Update record
		$editor = new Editor($m, $p, $a);
		$ret = ($editor->update($id) > 0);
		// Add PCRs
		$ret = set_meeting_pcr_details($id);
		// Add Employees
		$ret = set_meeting_emp_details($id);

		break;
	case "delete":
		$editor = new Editor($m, $p, $a);
		$ret = $editor->delete($id);
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Records deleted successfully.","success");
					}else{
						$_SESSION['messageStack']->add("There was an error while deleting the records.");
					}
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");	
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");	
		$ret = false;
		break;
}

if($ret) {
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}

function set_meeting_pcr_details($id){
	global $TABLES;
	$ret = true;
	//print_r($_POST);
	if($id>0){
		if(isset($_POST['PCR_DISPLAYID']) && $_POST['PCR_DISPLAYID']!=''){
			$keys = $_POST['PCR_DISPLAYID'];
			
			if(is_array($keys)){ // lets make sure we have an array
				if (sizeof($keys) > 1) { // have any Employees been selected
					// make the array for binding to our compiled delete statement
					for($i=0; $i < sizeof($keys); $i++){			
						if(!empty($keys[$i])) {
							// Need to Get PCRID from TBLPCR_DETAILS File
							$pcr_id = db_get_one("SELECT pcr_id FROM tblpcr_details where pcr_displayid='".$keys[$i]."'");
							// Check the Returned Value
							if ($pcr_id != 0) {
								// array must be in this format to bind to our compiled statement
								$add[] = array( $pcr_id, $id ); 
							} else {
								$_SESSION['messageStack']->add("PCR ".$keys[$i]." does not exist.");
								//$ret = false;
							}
						}
					}
					
					if($ret == true) {
						// delete the records and re-insert with new values
						$ret = db_query("DELETE FROM tblpcr_meetings_pcr_details WHERE meeting_id = $id ");
						// do the bulk delete on the main table using the id as the delete criteria
						$ret = db_bulk_insert($TABLES['PCR']['table'],$TABLES['PCR']['fields'],$add,true);			
					}
					if($ret == true)
						$_SESSION['messageStack']->add("PCRs to be Reviewed updated successfully.","success");
					else
						$_SESSION['messageStack']->add("PCRs to be Reviewed updated failed.");
				}else {
					// delete the records 
					$ret = db_query("DELETE FROM tblpcr_meetings_pcr_details WHERE meeting_id = $id ");
					if($ret == true)
						$_SESSION['messageStack']->add("PCRs to be Reviewed updated successfully.","success");
					else
						$_SESSION['messageStack']->add("PCRs to be Reviewed updated failed.");
				}
			}else{
				// let me know how you got here if it ever happens
				$_SESSION['messageStack']->add("Problems updating application details.");
			}
		}else{
			$_SESSION['messageStack']->add("The PCR ID was not supplied. So I can't create 'PCRs to be Reviewed'");
		}
	}else{
		$_SESSION['messageStack']->add("The Meeting ID was not supplied. So I can't create 'PCRs to be Reviewed'");
	}
	return $ret;
}

function set_meeting_emp_details($id){
	global $TABLES;
	$ret = true;
	//print_r($_POST);
	if($id>0){
		if(isset($_POST['EMPLOYEE_NUMBER']) && $_POST['EMPLOYEE_NUMBER']!=''){
			$keys = $_POST['EMPLOYEE_NUMBER'];
			
			if(is_array($keys)){ // lets make sure we have an array
				// delete the records and re-insert with new values
				$ret = db_query("DELETE FROM tblpcr_meetings_emp_details WHERE meeting_id = $id ");
				
				if (sizeof($keys) > 1) { // have any Employees been selected
					// make the array for binding to our compiled delete statement
					for($i=0; $i < sizeof($keys); $i++){			
						if(!empty($keys[$i]))
							$add[] = array( $keys[$i], $id ); // array must be in this format to bind to our compiled statement
					}
								
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_insert($TABLES['EMPLOYEE_NUMBER']['table'],$TABLES['EMPLOYEE_NUMBER']['fields'],$add,true)){
						$_SESSION['messageStack']->add("PCR Team Members and Invited Employees updated successfully.","success");
					}else{
						$_SESSION['messageStack']->add("PCR Team Members and Invited Employees updated failed.");
					}
				}else{
					$_SESSION['messageStack']->add("PCR Team Members and Invited Employees updated successfully.","success");
				}
			}else{
				// let me know how you got here if it ever happens
				$_SESSION['messageStack']->add("Problems updating application details.");
			}
		}else{
			$_SESSION['messageStack']->add("The Employee Number was not supplied. So I can't create 'PCR Team Members and Invited Employees'");
		}
	}else{
		$_SESSION['messageStack']->add("The Meeting ID was not supplied. So I can't create 'PCR Team Members and Invited Employees'");
	}
	return $ret;
}


// house cleaning to be done
$db=db_close();
?>