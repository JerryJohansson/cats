<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');

$search_filter = array(
	'name'=>'Open Actions Managed By or Allocated To me',
	'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "
);
?>
<link rel="stylesheet" href="style/forms.css" type="text/css" />
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script language="JavaScript" src="js/classes/menu/popup.js"></script>
<script language="JavaScript" src="js/classes/helper/helper.js"></script>
<script language="JavaScript" src="js/validation.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
document.onmousedown = function(evt){
	if(ACTIVE_HELPER && !_in(ACTIVE_HELPER.parentNode.parentNode,_target(evt))) ACTIVE_HELPER.style.display='none';
	
	top.doMenu();
}
function _target(e){
	return (document.all)?event.srcElement:e.target;
}
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}
//=====================================================================================================================
//
//=====================================================================================================================
//
//=====================================================================================================================
/*REF_EMPLOYEES = 'employees';
var CATS_MENU=null;
var bWRITE_MODE=true;*/
function get_employees(name){
	var s=_get_dd_select({'name':name,'onchange':"alert('what the????')"},"");
	document.write(s);
	var id = (arguments[1])?arguments[1]:1;
	var el=element(name);
	remote.get_options_add({'name':name,'type':'text','value':''},el,'get_employee_array','users',id);
}

function _get_dd_select(o,soptions){
	var name=o.name;
	var id=(typeof(o.id)=="string")?o.id:o.name;
	var onchange=(typeof(o.onchange)=="string")?' onchange="'+o.onchange+'" ':"";
	var onfocus=(typeof(o.onfocus)=="string")?' onfocus="'+o.onfocus+'" ':"";
	var onblur=(typeof(o.onblur)=="string")?' onblur="'+o.onblur+'" ':"";
	var attribs=(typeof(o.attributes)=="string")?' '+o.attributes+' ':"";
	return '<select name="'+name+'" id="'+id+'" '+onchange+onfocus+onblur+attribs+' >\n<option></option>\n'+soptions+'</select>\n';
}

function _get_dd_options(adata,selected,oatt){
	var i,checked,s="";
	for(i=0;i<adata.length;i+=2){
		checked = (selected == adata[i])?"selected":"";
		s+="<option value=\""+adata[i]+"\" "+checked+" >"+adata[i+1]+"</option>\n";
	}
	//alert(s);
	return s;
}



var win=window;
function dummy(){}
function init(){
	// start up script for each page
	//
	// Execute some start up script
	return true;
}


</script>
</head>
<body>
<form><table>
<tr valign="top">
	<td width="60%">
		
		<fieldset class="tbar" id="right_panel">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">My Details</a></caption>
		<tr>
			<td class="label"><label for="id_manager_id_txt">Manager:</label></td>
			<td><input class="sfield_sml" type="text" name="manager_id_txt" id="id_manager_id_txt" value="" onchange="remote.get_options_add(this, this.form.manager_id, 'get_employee_array', 'users');" />
			<?PHP echo '<script>get_employees("manager_id",90999,true);</script>';?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_allocated_id_txt">Allocated To:</label></td>
			<td><input class="sfield_sml" type="text" name="allocated_id_txt" id="id_allocated_id_txt" value="" onchange="remote.get_options_add(this, this.form.allocated_id, 'get_employee_array', 'users');" />
			<?PHP echo '<script>get_employees("allocated_id",20013,true);</script>';?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_lname">My Manager:</label></td>
			<td>
				<table>
					<tr><td><script>get_helper_dd('my_manager_id[0]',['Haywood, Merideth',10804],true);</script></td><td><script>get_helper_dd('my_allocated_id[0]',10804,true);</script></td></tr>
					<tr><td><script>get_helper_dd('my_manager_id[1]',10804,true);</script></td><td><script>get_helper_dd('my_allocated_id[1]',10804,true);</script></td></tr>
				</table>
				
				
			</td>
		</tr>
		<tr>
			<td class="label"><label for="id_employees_to_txt">Employees To:</label></td>
			<td><input class="sfield_sml" type="text" name="employees_to_txt" id="id_employees_to_txt" value="" onchange="remote.get_options_add(this, this.form.employees_to, 'get_employee_array', 'users');" />
			<?PHP echo '<script>get_employees("employees_to",20208,true);</script>';?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_employees_from_txt">Employees From:</label></td>
			<td><input class="sfield_sml" type="text" name="employees_from_txt" id="id_employees_from_txt" value="" onchange="remote.get_options_add(this, this.form.employees_from, 'get_employee_array', 'users');" />
			<?PHP echo '<script>get_employees("employees_from",10595,true);</script>';?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_date_from_txt">Employees From:</label></td>
			<td><?PHP echo html_get_calendar_date_field("date_from");?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_date_to_txt">Employees From:</label></td>
			<td><?PHP echo html_get_calendar_date_time_field("date_to");?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_emp_id">My Manager:</label></td>
			<td>
				<script>
				get_helper_dd('emp_id',90999,true);
				get_helper_dd('emp_id2',10862,true);
				get_helper_dd('emp_id3',10866,true);
				</script>
			</td>
		</tr>
		<tr>
			<td class="label"><label for="id_userid">User ID:</label></td>
			<td><input class="sfield" type="text" name="FIELD_PID_DISABLED" value="<?PHP echo $_SESSION['user_details']['uid'];?>"  disabled=true /></td>
		</tr>
		<tr>
			<td class="label"><label for="id_username">Username:</label></td>
			<td><?PHP echo $_SESSION['user_details']['user_name'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_email">Email:</label></td>
			<td><?PHP echo $_SESSION['user_details']['email'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_fname">Given Name:</label></td>
			<td><?PHP echo $_SESSION['user_details']['first_name'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_lname">Surname:</label></td>
			<td><?PHP echo $_SESSION['user_details']['last_name'];?></td>
		</tr>
		</table>
		
		<?php
		$employee_number = 90999;//$_SESSION['user_details']['user_id'];
		$col_attributes = array('',' width="60" ',' width="180" ',' width="150" ',' width="20" ');
		$col_headers=array('Action Title','Action No.','Scheduled Date','Action Origin','edit:index.php?m=edit&id=:ACTION_ID');
		$sql="SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, 'edit:index.php?m=edit&id=:ACTION_ID' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id,Allocated_To_Id) ORDER BY Scheduled_Date ASC";
		db_render_pager($sql,$col_headers,$col_attributes);
				$s='
<div id="login">
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
<tr valign="middle">
	<td>
		<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="box">
		<tr>
			<td align="center" valign="middle">
				<form method="POST" onSubmit="return _login(this);">
		
				<div class="maincontentheader">
					 <h4>Welcome to CATS</h4>
					 <p>Please enter a valid login and password.</p>
					 <p><!-- message --></p>
				</div>
				<table>
				<tr valign="top">
					<td><img src="icons/login.gif" alt=""/></td>
					<td>
						<div class="block">
						<label for="id1">Login</label>
						<input class="halfbox" type="text" size="10" name="uid" id="id1" value="" />
						</div>
						
						<div class="block">
						<label for="id2">Password</label>
						<input class="halfbox" type="password" size="10" name="pwd" id="id2" value="" />
						</div>
						
						<div class="buttonblock">
						<input type="submit" name="login" value="Login" />
						</div>
					</td>
				</tr>
				</table>
				</form>	
			</td>
		</tr>
		</table>
	</td>
</tr>
<!--<tr>
	<td class="copyright" height="40">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</td>
</tr>-->
</table>
</div>
		';
		$s=preg_replace("/\r\n/","",$s);
		echo $s;
		
		?>
		
		<!-- extra_panels -->
		</fieldset>
		<fieldset class="tbar">
		<div align="right" class="bar">
			<a
			title="Edit My Account Details"
			href="users.php?action=edit&id=<?php echo $_SESSION['user_details']['uid'];?>"
			style="background-image:url(icons/nav_my_account.gif);float:right;">My Account</a>
		</div>
		</fieldset>
		
<?php
if(!isset($_SESSION['user_details']['show_welcome_message'])){
	$_SESSION['user_details']['show_welcome_message']='done';
?>
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">Welcome to CATS</a></caption>
		<tr>
			<td>
				<p>Welcome back <?PHP echo $_SESSION['user_details']['first_name'];?>, you last logged into the system on <?php echo $_SESSION['user_details']['lastlogdate'];?> at <?php echo $_SESSION['user_details']['lastlogtime'];?>hrs. If this time is incorrect please contact your <a href="mailto:<? echo $_SESSION['user_details']['email'];?>">website administrator</a>. You have logged into the system 22 times.</p>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
		</fieldset>
<?php
	$num = 6;
} // END:: if REQUEST['show_welcome_message'] == 'yes' 
else{
	$num = 10;
}
?>
		<!-- xtra_templates -->
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();"><?php echo $search_filter['name'];?></a></caption>
		<tr>
			<th>Action Title</th><th>Action No.</th><th>Scheduled Date</th><th>Action Origin</th><th>Edit</th>
		</tr>
		<?php echo get_filter_rows('',$num); ?>
		</table>
		</fieldset>
		
	</td>
	<td width="40%">
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">Message of the Day</a></caption>
		<tr>
			<td>
				<p>Workflow configurations control who an issue can be assigned to, the valid values for
					the resolution and status fields, and basic data entry validations. RMTrack installs
					with a default workflow configuration that can easily be changed or replaced entirely.</p>
			</td>
		</tr>
		</table>
		</fieldset>
		
		<!-- extra_column_right -->
		
		
	</td>
</tr>
</table>
<iframe style="width:0px;height:0px" src="" id="ezw_buffer"></iframe>
<div id="html_content"></div>
<?php

function get_filter_rows($filter='',$x=10)
{
	$action_title = array('Scheduled Actions: Reminder to document code','Create client-side html object code library',
		'Document code library','Create code library for database layer','Create page prototype',
		'Approval of page prototype','Create form entry pages','Create functional pages',
		'Create cron jobs for back end processing','Testing goes on');
	$action_no = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30);
	$scheduled_date = array('17/12/2004','18/12/2004','19/12/2004','22/02/2004','13/12/2004','14/11/2004','22/06/2004','24/12/2004','11/12/2004','12/09/2004');
	$action_origin = array('Schedule','Action','Email','PCR','Meeting','Email','PCR','Meeting','Schedule','Action');
	//$x=10;
	$s="";
	for($i=0;$i<$x;$i++){
		$s .= '
		<tr onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);">
			<td>
				<a href="index.php?m=edit&c=action&id=1">'.$action_title[rand(0,9)].'</a>
			</td>
			<td>'.$action_no[$i].'</td>
			<td>'.$scheduled_date[rand(0,9)].'</td>
			<td>'.$action_origin[rand(0,9)].'</td>
			<td><img src="style/icons/btn_show_bg.gif" border="0" /></td>
		</tr>
		';
	}
	return $s;
}
?>
<script>

function _pg(){
	var args=arguments;
	this.html = (args.length>0)?args[0]:"";
	this.container = (args.length>1)?(typeof(args[1])=="string"?element(args[1]):args[1]):_default;
	this.container.innerHTML = this.html;
}

function get_fields(page){
	var url = "_remote/get_fields.php";
	var data=null;
	if(top.tabs[page]&&typeof(top.tabs[page]['search'])=="string"){
		data=top.tabs[page];
	}else{
		try{
			var return_value = RSExecute(url,"getfields", page);
		}catch(e){
			var return_value = {'return_value':"{'search':'"+top.tabs[page]['search']+"','int':"+top.tabs[page]['int']+"}"};
		}
		return_value_dump=return_value.return_value;
		eval("data="+return_value.return_value+";");//.split("|");
		top.tabs[page]=data;
		// *
		var s = "";
		for(e in return_value) if(typeof return_value[e] != "function") s+=e+":"+return_value[e]+"\n";
		alert("get_fields()\n\n"+page+"\n\n"+data+"\n\n"+return_value.return_value);
		//alert(data.string)
		//*/
	}
	return data;
}
return_value_dump = "";
function get_tables(page){
	var url = "_remote/get_fields.php";
	var sql = "select * from form_fields";
	var data=null;
	if(top.tabs[page]&&typeof(top.tabs[page]['search'])=="string"){
		data=top.tabs[page];
	}else{
		try{
			var return_value = RSExecute(url,"getfieldvalues", sql.replace(/\+/,"#"));
		}catch(e){
			var return_value = {'return_value':"{'search':'"+top.tabs[page]['search']+"','int':"+top.tabs[page]['int']+"}"};
		}
		return_value_dump=return_value.return_value;
		eval("data="+return_value.return_value+";");//.split("|");
		top.tabs[page]=data;
		
	}
	return data;
}
function render_pg(){
	alert("hello:\n\n"+arguments[0]);
}

function render(obj_string){
	IDX_DEFAULT=1;
	var obj_string_name = string_to_id(obj_string);
	var a = null;
	//if(obj_string!="workload review")
		a = get_fields(obj_string);
	//else
		//a = get_tables(obj_string);
	alert("a=get_fields('"+obj_string+"')\n"+a['search'])
	var o=null;
	var obj=(typeof(a['array'])=="object")?a['array']:a['search'].split("|");
	var interval=a['int'];
	var type=a['type'];
	//alert(obj+":"+interval+":"+type)
	if(typeof(top.tabs[obj_string])!="object") top.tabs[obj_string] = a;
	top.tabs[obj_string]._html=new _html( obj, interval, "", type );
	var s=top.tabs[obj_string]._html.gen();
	try{
		o=parent.element(_menu_prefix+obj_string_name);
		menu=o.innerHTML;
	}catch(e){
		var tmp=top.document.createElement("DIV");
		tmp.setAttribute("id",_menu_prefix+obj_string_name);
		o=top.document.body.appendChild(tmp);
		tmp=null;
		tmp=top.document.createElement("A");
		tmp.className = "def";
		tmp.setAttribute("href","javascript:showMenu('"+obj_string_name+"','"+obj_string+"');");
		tmp=top.element("menu_history_list").appendChild(tmp);
		tmp.innerHTML = obj_string;
	}
	o.innerHTML = s;
	o.style.display = 'none';
	top.showMenu(obj_string_name);
	//alert(s);
	//alert(return_value_dump);
}
if(!document.all){
top.tabs['splash']={'type':0,'int':6,'search':'Splash_Goes_Here|s|3|3|1|0'};
top.tabs['actions']={'type':0,'int':3,'search':'Site|s|3|Department|s|64|Section|s|64|Action_Managed_By|n|16|Allocated_To|n|16|Managed_By_Or_Allocated_To|n|16|Type|s|50|Origin|s|50|Category|s|50|Status|s|50|Action_Number|n|16|Due_Date_From|d|10|Due_Date_To|d|10|Closed_Date_From|d|10|Closed_Date_To|d|10'};
top.tabs['audits and inspections']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|2|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|2|1|0|Audit_Or_Inspection_Type|s|64|2|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['government inspections']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Inspection_Type|s|64|2|1|0|Government_Departments|s|64|2|1|0|Location|s|128|Kwinana|1|0|Date_Completed_From|d|10|21/02/2005|2|0|Date_Completed_To|d|10|23/02/2005|2|0'};
top.tabs['incidents']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|3|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['major hazards register']={'type':1,'int':6,'search':'Site|s|3|Kwinana|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|1|0|Date_To|d|10|23/02/2005|1|0','obj':{'fields':['Name','Site','Department'],'names':['Vernon','Kwinana','Software']}};
top.tabs['meeting minutes']={'type':0,'int':3,'search':'Site|s|3|Department|s|64|Section|s|64|Action_Managed_By|n|16|Allocated_To|n|16|Managed_By_Or_Allocated_To|n|16|Type|s|50|Origin|s|50|Category|s|50|Status|s|50|Action_Number|n|16|Due_Date_From|d|10|Due_Date_To|d|10|Closed_Date_From|d|10|Closed_Date_To|d|10'};
top.tabs['other records']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|2|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|2|1|0|Audit_Or_Inspection_Type|s|64|2|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['schedules']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Inspection_Type|s|64|2|1|0|Government_Departments|s|64|2|1|0|Location|s|128|Kwinana|1|0|Date_Completed_From|d|10|21/02/2005|2|0|Date_Completed_To|d|10|23/02/2005|2|0'};
top.tabs['site specific obligations']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|3|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['workload review']={'type':1,'int':6,'search':'Site|s|3|Kwinana|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|1|0|Date_To|d|10|23/02/2005|1|0','obj':{'fields':['Name','Site','Department'],'names':['Vernon','Kwinana','Software']}};


//,Date_From,d,10,31/12/2004,1,0,Date_To,d,10,31/12/2005,1,0|1|0
}
</script>
</form>