<script language="javascript">





</script> 


<?PHP

require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');

$search_filter = array(
	'name'=> array('Open Actions Managed By me','Open Actions Allocated To me'),
	'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "
);
?>



<script language="JavaScript" src="js/prototype.js"></script>
<script>



function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m = new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
</script>
<style>
.motd_title {
	font: bold 12px Tahoma, Verdana;
	color: #660000;
	padding: 3px 2px 5px 2px;
}
.motd_body {
	font: 11px Verdana, Arial;
	padding: 3px 2px 5px 2px;
}
.motd_date {
	font: 10px Tahoma, Verdana;
	color: #999999;
	padding: 3px 2px 5px 2px;
}
</style>
</head>
<body>


<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10">
        <div class="row">
            <div class="col-sm">
				<fieldset class="tbar" style="margin-bottom:10px;">
					<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
					<caption id="tog"><a name="todo" onClick="this.blur();">Welcome to CATS</a></caption>
					<tr>
						<td>  
							<p>Welcome back   <?PHP echo $_SESSION['user_details']['first_name']; ?>.</p> 
							<p>&nbsp;</p>
						</td>
					</tr>
					</table>
				</fieldset>
            </div>
        </div>

        <div class="row">
            <div class="col-sm">
				<fieldset class="tbar" style="margin-bottom:10px;">
					<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
						<tr>
							<td>  

								<?PHP
									require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');

									$search_filter = array(
										'name'=> array('Open Actions Managed By me','Open Actions Allocated To me'),
										'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "
									);
								?>
								<?php
								// Display workload review items

								$employee_number = $_SESSION['user_details']['user_id'];
								$col_attributes = array('',' width="60" ',' width="180" ',' width="150" ',' width="150" ',' width="20" ');
								$col_headers=array('Action Title','Action No.','Scheduled Date','Action Origin','Originating ID','edit:javascript:top.show_edit_screen("actions","index.php?m=actions&p=edit&id=:ACTION_ID:")');
								//$asql=array(
								//	"SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, REPORT_ID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id) ORDER BY Scheduled_Date ASC",
								//	"SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, REPORT_ID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Allocated_To_Id) ORDER BY Scheduled_Date ASC"
								$asql=array(
									"(SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, to_char(REPORT_ID) as REPORT_ID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id) and ORIGIN_TABLE <> 'Originating_Incident' and ORIGIN_TABLE <> 'ORIGINATING_PCR_ACTION') UNION ALL (SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, t2.INCDISPLYID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details t1 JOIN Originating_Incident t2 on t1.Report_id = t2.Report_id  WHERE Status='Open' AND $employee_number in (Managed_By_Id) and ORIGIN_TABLE = 'Originating_Incident') UNION ALL (SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, t2.DISPLAY_ID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details t1 JOIN ORIGINATING_PCR_ACTION t2 on t1.Report_id = t2.Report_id WHERE Status='Open' AND $employee_number in (Managed_By_Id) and ORIGIN_TABLE = 'ORIGINATING_PCR_ACTION') ORDER BY Scheduled_Date ASC",
									"(SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, to_char(REPORT_ID) as REPORT_ID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Allocated_To_Id) and ORIGIN_TABLE <> 'Originating_Incident' and ORIGIN_TABLE <> 'ORIGINATING_PCR_ACTION') UNION ALL (SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, t2.INCDISPLYID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details t1 JOIN Originating_Incident t2 on t1.Report_id = t2.Report_id  WHERE Status='Open' AND $employee_number in (Allocated_To_Id) and ORIGIN_TABLE = 'Originating_Incident') UNION ALL (SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, t2.DISPLAY_ID, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")' as edit FROM View_Action_Details t1 JOIN ORIGINATING_PCR_ACTION t2 on t1.Report_id = t2.Report_id WHERE Status='Open' AND $employee_number in (Allocated_To_Id) and ORIGIN_TABLE = 'ORIGINATING_PCR_ACTION') ORDER BY Scheduled_Date ASC"
								);
								$idx=0;
								foreach($asql as $sql) db_render_pager($sql,$col_headers,$col_attributes,$search_filter['name'][$idx++], false, '', '', 100);
								?>
	
							</td>
						</tr>
					</table>
				</fieldset>
            </div>
        </div>


        <div class="row">
            <div class="col-sm">
                


									<?php
									$site_name = $_SESSION['user_details']['site_name'];
									$user_department_name = $_SESSION['user_details']['department'];

									?>


									<tr>
										<td>
											<fieldset class="tbar" id="left_panel">


																<!-- Incident Table -->
														<?php
														
														$search_filter = array(
															'name'=> array('Open Incidents: Assigned To Me or With Open Actions Assigned To Me'),
															'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "	
														);
														
														
														// Open Incidents: Assigned To Me or With Open Actions Assigned To Me

														$employee_number = $_SESSION['user_details']['user_id'];
														$site_id = $_SESSION['user_details']['site_id'];
														$col_attributes = array(' width="1" ',' width="400" ',' width=50',' width="50" ',' width="30" ',' width="60" ', ' width="65" ', ' width="60" ', ' width="100" ', ' width="30" ', ' width="30" ', ' width="10" ');

														$col_headers=array('ID','Incident Title','Inc Status', 'Responsible Superintendent', 'Incident Id', 'Incident Date', 'Review Status', 'Date of Last Review', 'Forward To', '# Act Raised', '# Act Open', 'edit:javascript:top.show_edit_screen("incidents","index.php?m=incidents&p=edit&id=:INCIDENT_NUMBER:")');
														$asql=array(
																"select distinct INCIDENT_NUMBER, INCIDENT_TITLE, INCIDENTSTATUS, SUPERINTENDENT, INCDISPLYID, INCIDENTDATE, REVIEWSTATUS, 
																DATEOFLASTREVIEW, FORWARDEDTO, ACTRAISED, ACTOPEN, 
																'edit:javascript:top.show_edit_screen(\"incidents\",\"index.php?m=incidents&p=edit&id=:INCIDENT_NUMBER:\")' as edit 

																FROM (
																	select * from (
																		select i.*,
																		(select count(1) from tblaction_details where report_id = i.incident_number and allocated_to_id = $employee_number) as ActRaised,
																		(select count(1) from tblaction_details where report_id = i.incident_number and status = 'Open' and allocated_to_id = $employee_number) as ActOpen
																		from VIEW_DASHBOARD_3_1_OPEN i
																		where  ((i.forward_person_id_1 = $employee_number and i.forward_person_id_2 is null and i.forward_person_id_3 is null and
																			i.forward_person_id_inc_4 is null and i.forward_person_id_inc_5 is null and i.forward_person_id_inc_6 is null)
																			or
																			(i.forward_person_id_2 = $employee_number and i.forward_person_id_3 is null and
																			i.forward_person_id_inc_4 is null and i.forward_person_id_inc_5 is null and i.forward_person_id_inc_6 is null)
																			or
																			(i.forward_person_id_3 = $employee_number and
																			i.forward_person_id_inc_4 is null and i.forward_person_id_inc_5 is null and i.forward_person_id_inc_6 is null)
																			or
																			(i.forward_person_id_inc_4 = $employee_number and i.forward_person_id_inc_5 is null and i.forward_person_id_inc_6 is null)
																			or
																			(i.forward_person_id_inc_5 = $employee_number and i.forward_person_id_inc_6 is null)
																			or
																			(i.forward_person_id_inc_6 = $employee_number)
																		) 
																	) aaaa      
																	
																	UNION
																	
																	select * from (
																	select i.*, 
																		(select count(1) from tblaction_details where report_id = i.incident_number and allocated_to_id = $employee_number) as ActRaised,
																		(select count(1) from tblaction_details where report_id = i.incident_number and status = 'Open' and allocated_to_id = $employee_number) as ActOpen
																	from VIEW_DASHBOARD_3_2_CLOSED i  
																	where i.allocated_to_id = $employee_number
																	) bbbb
																) XXX  order by incidentdate desc", 	
															
															
																					
															
														);
														
														$idx=0;
														foreach($asql as $sql) db_render_pager($sql,$col_headers,$col_attributes,$search_filter['name'][$idx++], false, '', '', 100);
														
														$sortFilter = isset($_REQUEST['sortFilter'])?$_REQUEST['sortFilter']:null;
														$orderByFilter = isset($_REQUEST['orderByFilter'])?$_REQUEST['orderByFilter']:null;

														if($sortFilter == null || $sortFilter == "")
															$sortFilter = "INCIDENT_TITLE";
														if($orderByFilter == null || $orderByFilter == "")
															$orderByFilter = "ASC";
														
														
														?>
											</fieldset>						
											
											<fieldset class="tbar" id="left_panel">
											<div id="leftDIV2">
										
												<div id="msgDiv2" style="padding-left: 20px;">
															
															
														<button type='button' onclick='sort("INCIDENT_TITLE")' id='btnsearch1' class='btn btn-info btn-xs' style='color:white; width:120px;'>Incident Title</button>
														<button type='button' onclick='sort("INCIDENTSTATUS")' id='btnsearch2' class='btn btn-info btn-xs' style='color:white; width:120px;'>Incident Status</button>
														<button type='button' onclick='sort("SUPERINTENDENT")' id='btnsearch3' class='btn btn-info btn-xs' style='color:white; width:120px;'>Superintendent</button>
														<button type='button' onclick='sort("INCDISPLYID")' id='btnsearch4' class='btn btn-info btn-xs' style='color:white; width:120px;'>Incident Id</button>
														<button type='button' onclick='sort("INCIDENTDATE")' id='btnsearch5' class='btn btn-info btn-xs' style='color:white; width:120px;'>Incident Date</button>
														<button type='button' onclick='sort("REVIEWSTATUS")' id='btnsearch6' class='btn btn-info btn-xs' style='color:white; width:120px;'>Review Status</button>
														<button type='button' onclick='sort("DATEOFLASTREVIEW")' id='btnsearch7' class='btn btn-info btn-xs' style='color:white; width:120px;'>Last Review Date</button>
														<button type='button' onclick='sort("FORWARDEDTO")' id='btnsearch8' class='btn btn-info btn-xs' style='color:white; width:120px;'>Forwarded To</button>
															
															
																
																<script language="javascript">
																	var orderBy = "ASC";
																	var lastSort = "<?php echo $sortFilter?>";
																	var lastOrderBy = "<?php echo $orderByFilter?>";								
																	function sort(column){
																

																		
																		if(column == lastSort)
																		{
																			if(lastOrderBy == "ASC")
																			{
																				orderBy = "DESC";
																			}
																			else
																			{
																				orderBy = "ASC";
																			}
																			
																		}
																		
																		//alert('sort: ' + column + ' ' + orderBy);
																		window.location = "<?php echo WS_PATH;?>/index.php?sortFilter=" + column + '&orderByFilter=' + orderBy;
																	}
																	
																	//alert('lastSort: ' + lastSort);
																	if(lastSort == "INCIDENT_TITLE")
																	{
																		$("#btnsearch1").removeClass('btn-info');
																		$("#btnsearch1").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch1").html('Incident Title v');
																		else
																			$("#btnsearch1").html('Incident Title ^');
																	}

																	if(lastSort == "INCIDENTSTATUS")
																	{
																		$("#btnsearch2").removeClass('btn-info');
																		$("#btnsearch2").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch2").html('Incident Status v');
																		else
																			$("#btnsearch2").html('Incident Status ^');
																	}

																	if(lastSort == "SUPERINTENDENT")
																	{
																		$("#btnsearch3").removeClass('btn-info');
																		$("#btnsearch3").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch3").html('Superintendent v');
																		else
																			$("#btnsearch3").html('Superintendent ^');
																	}

																	if(lastSort == "INCDISPLYID")
																	{
																		$("#btnsearch4").removeClass('btn-info');
																		$("#btnsearch4").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch4").html('Incident Id v');
																		else
																			$("#btnsearch4").html('Incident Id ^');
																	}

																	if(lastSort == "INCIDENTDATE")
																	{
																		$("#btnsearch5").removeClass('btn-info');
																		$("#btnsearch5").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch5").html('Incident Date v');
																		else
																			$("#btnsearch5").html('Incident Date ^');
																	}

																	if(lastSort == "REVIEWSTATUS")
																	{
																		$("#btnsearch6").removeClass('btn-info');
																		$("#btnsearch6").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch6").html('Review Status v');
																		else
																			$("#btnsearch6").html('Review Status ^');
																	}

																	if(lastSort == "DATEOFLASTREVIEW")
																	{
																		$("#btnsearch7").removeClass('btn-info');
																		$("#btnsearch7").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch7").html('Date Review v');
																		else
																			$("#btnsearch7").html('Date Review ^');
																	}

																	if(lastSort == "FORWARDEDTO")
																	{
																		$("#btnsearch8").removeClass('btn-info');
																		$("#btnsearch8").addClass('btn-warning');
																		if(lastOrderBy == "ASC")
																			$("#btnsearch8").html('Forwarded To v');
																		else
																			$("#btnsearch8").html('Forwarded To ^');
																	}

																	
																	function sortFilter()
																	{
																		window.location = "<?php echo WS_PATH;?>/index.php";
																	}
																</script>
															
													
															
														</div> 
													</div>






            </div>
        </div>

        <div class="row">
            <div class="col-sm">



						<fieldset class="tbar" id="left_panel">




								<!-- Incident Table -->
								<?php


								//////////////////////////////////

								// All Open Incidents or Incidents with Open Actions in my Department:

								$search_filter = array(
								'name'=> array('All Open Incidents or Incidents with Open Actions in my Department: ' . $user_department_name),
								'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "	
								);



								$col_headers=array('ID','Incident Title','Inc Status', 'Responsible Superintendent', 'Incident Id', 'Incident Date', 'Review Status', 'Date of Last Review', 'Forward To', '# Act Raised', '# Act Open', 'edit:javascript:top.show_edit_screen("incidents","index.php?m=incidents&p=edit&id=:INCIDENT_NUMBER:")');
								$asql=array(


								"select INCIDENT_NUMBER, INCIDENT_TITLE, INCIDENTSTATUS, SUPERINTENDENT, INCDISPLYID, INCIDENTDATE,  REVIEWSTATUS, DATEOFLASTREVIEW, FORWARDEDTO, ACTRAISED, ACTOPEN, 
								'edit:javascript:top.show_edit_screen(\"incidents\",\"index.php?m=incidents&p=edit&id=:INCIDENT_NUMBER:\")' as edit 
								FROM (
									select i.*, 
									(select count(1) from tblaction_details where report_id = i.incident_number) as ActRaised,
									(select count(1) from tblaction_details where report_id = i.incident_number and status = 'Open') as ActOpen
									from VIEW_DASHBOARD_4_1_OPEN i
									where i.department_id = (select department_id from tblemployee_details where employee_number = $employee_number) 
									
								UNION
									
									select i.*, 
									(select count(1) from tblaction_details where report_id = i.incident_number) as ActRaised,
									(select count(1) from tblaction_details where report_id = i.incident_number and status = 'Open') as ActOpen
									from VIEW_DASHBOARD_4_2_CLOSED i
									where i.department_id = (select department_id from tblemployee_details where employee_number = $employee_number) 
									

								) XXX order by $sortFilter $orderByFilter "  

								);





								//echo $asql[0];

								$idx=0;
								foreach($asql as $sql) db_render_pager($sql,$col_headers,$col_attributes,$search_filter['name'][$idx++], false, '', '', 100);




								?>
					</fieldset>			





            </div>
        </div>

    </div>
    <div class="col-sm-2">
        <div class="row">
            <div class="col-sm">




											<?php
											// Message of the Day
											$sql = "SELECT * FROM View_MOTD_Current WHERE MOTD_Type = 0 AND MOTD_Site LIKE '%" . $_SESSION['user_details']['site_id'] . "%'";
											$rs = $db->Execute($sql);
											if($rs->RecordCount()){
											?>		
													<!-- Message Of The Day -->
													
													<fieldset class="tbar" style="margin-bottom:10px;">
													<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
													<caption id="tog"><a name="todo" onClick="this.blur();">Message of the Day</a></caption>
											<?php
												// Message of the day body table
												$idx = 0;
												while ($arr = $rs->FetchRow()) {
													$link = (!empty($arr['MOTD_URL']))?"<div align=\"left\">Link:<a href=\"".$arr['MOTD_URL']."\" target=\"_blank\">".$arr['MOTD_SHORT']."</a></div>":"";
													$motd_title = $arr['MOTD_SHORT'];
													$motd = (!empty($arr['MOTD_DESC']))?$arr['MOTD_DESC']:"No message today";
													$date = $arr['MOTD_STARTDATE'];
													$tr_hilite = ($idx % 2)?' bgcolor="#eeeeee" ':' bgcolor="#ffffff" ';
													$idx++;
											?>
													<tr <?php echo $tr_hilite;?>>
														<td>
															<div class="motd_title"><?php echo $motd_title; ?></div>
															<div class="motd_body" align="justify"><?php echo $motd; ?></div>
															<?php echo $link;?>
															<div class="motd_date" align="left"><?php echo $date;?></div>
														</td>
													</tr>
											<?php
												} // END:: rs while loop
											?>
													<tr>
												<td>
											
													<!-- Modal sra-->
													<div id="myModalMessageOfTheDay" tabindex="-1" aria-labelledby="myModalLabel" style="width:100%; left-padding:100; Display:none">
														<table>
														<tr>
															<td>
															<div id="leftDIV">
														
																<div id="msgDiv" style="padding-left: 20px;">
																	<input type="button" class="btn btn-info btn-xs active" name="cats::New" value="New Message" onclick="javascript:rightDIV.style.display=''"></button>
																	<!-- Button to trigger modal -->
																	
																	
																	
															
																	
																</div> 
															</div>
															</td>
															<td>  
															<div id="rightDIV" style="padding-left: 20px; display:none">
															

																	<form action="http://<?PHP echo $_SERVER['SERVER_NAME']; ?>/includes/modules/dashboard/saveMsg4Today.php" id="FormMessage4Today" method="post">  
																		<table>
																		<tr>
																			<td>
																				<label>Headline</label>
																			</td>
																			<td>  
																				<input type="text" name="headline" style="width:400px">
																			</td>
																		</tr> 
																		<tr>
																			<td>
																			<label>Message</label>
																			</td>
																			<td> 
																		
																			<textarea rows="2" name="message" width="400px"></textarea>
																			<button class="btn btn-warning btn-xs" onclick="javascript:rightDIV.style.display='none'">Cancel</button>&nbsp;
																			<button class="btn btn-primary btn-xs" type="submit" rightDIV.style.display='none';">Save</button>
																			

																		
																			
																			
																			
																			</td>
																		</tr> 
																		</table> 
																	</form>						 
															</div>
															</td>      
														</table>   
													</div>          
													

													
													
												</td>
												</tr>  
												<tr>
												<td>
													&nbsp;
												</td>
												</tr>
												</table>
													</fieldset>
													
											<?php
											} // END:: rs->RecordCount()
											?>




            </div>
        </div>
        <div class="row">
            <div class="col-sm">


					<?php

					// Links of Interest
					$sql = "SELECT * FROM View_MOTD_Current WHERE MOTD_Type = 1 AND MOTD_Site LIKE '%" . $_SESSION['user_details']['site_id'] . "%'";
					$rs = $db->Execute($sql);
					if($rs->RecordCount()){
					?>
							<!-- links of interest -->
							
							<fieldset class="tbar" style="margin-bottom:10px;">
							<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
							<caption id="tog"><a name="todo" onClick="this.blur();">Links of Interest</a></caption>
							<?php
								// Links of Interest table body
								$idx = 0;
								while ($arr = $rs->FetchRow()) {
									$link = (!empty($arr['MOTD_URL']))?"<div align=\"left\">Link:<a href=\"".$arr['MOTD_URL']."\" target=\"_blank\">".$arr['MOTD_SHORT']."</a></div>":"";
									$motd_title = $arr['MOTD_SHORT'];
									$motd = (!empty($arr['MOTD_DESC']))?$arr['MOTD_DESC']:"No links today";
									$date = $arr['MOTD_STARTDATE'];
									$tr_hilite = ($idx % 2)?' bgcolor="#eeeeee" ':' bgcolor="#ffffff" ';
									$idx++;
							?>
									<tr <?php echo $tr_hilite;?>>
										<td>
											<div class="motd_title"><?php echo $motd_title; ?></div>
											<div class="motd_body" align="justify"><?php echo $motd; ?></div>
											<?php echo $link;?>
											<div class="motd_date" align="left"><?php echo $date;?></div>
										</td>
									</tr>
							<?php
								} // END:: rs while loop
							?>
							</table>
						
						
							</fieldset>


						<?php
						} // END:: rs->RecordCount()
						?>		
							






            </div>
        </div>
        <div class="row">
            <div class="col-sm">
  

					<!-- User Details Table -->
						
					<fieldset class="tbar" id="right_panel" style="position: absolute; top: 300px; width:100%">
						<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
						<caption id="tog"><a name="todo" onClick="this.blur();">My Details</a></caption>
						<tr>
							<td class="label"><label for="id_userid">ID:</label></td>
							<td><?PHP echo $_SESSION['user_details']['user_id'];?></td>
						</tr>
						<tr>
							<td class="label"><label for="id_username">Name:</label></td>
							<td><?PHP echo $_SESSION['user_details']['first_name']." ".$_SESSION['user_details']['last_name'];?></td>
						</tr>
						<tr>
							<td class="label"><label for="id_email">Email:</label></td>
							<td><?PHP echo $_SESSION['user_details']['email'];?></td>
						</tr>
						<tr>
							<td class="label"><label for="id_site">Site:</label></td>
							<td><?PHP echo $_SESSION['user_details']['site_name'];?></td>
						</tr>
						<tr>
							<td class="label"><label for="id_dep">Department:</label></td>
							<td><?PHP echo $_SESSION['user_details']['department'];?></td>
						</tr>
						<tr>
							<td class="label"><label for="id_dep">Section:</label></td>
							<td><?PHP echo $_SESSION['user_details']['section'];?></td>
						</tr>
						<tr>
							<td class="label"><label for="id_pos">Position:</label></td>
							<td><?PHP echo $_SESSION['user_details']['position'];?></td>
						</tr>
						<tr>
							<td colspan="2">
								<fieldset class="tbar" style="margin-bottom:10px;">
								<div align="right" class="bar">
									<a
									title="Edit My Account Details"
									href="javascript:top._account();"
									style="background-image:url(<?php echo WS_ICONS_PATH;?>nav_my_account.gif);float:right;">My Account</a>
								</div>
								</fieldset>
							
							</td>
						</tr>
						</table>
						</fieldset>

            </div>
        </div>
    </div>
  </div>
</div>

		
		
		
		
<?php
//} // END:: rs->RecordCount()
?>		
	
		



<script type='text/javascript'>

	var cats_user_is_administrator = '<?PHP echo cats_user_is_administrator();?>';
	var cats_user_is_admin_group = '<?PHP echo cats_user_is_admin_group();?>';
	var cats_user_is_super_administrator = '<?PHP echo cats_user_is_super_administrator();?>';
	var cats_user_is_root_administrator = '<?PHP echo cats_user_is_root_administrator();?>';
	var cats_user_is_editor = '<?PHP echo cats_user_is_editor();?>';

	var showMessageOfTodayButton = false;
 
	if (cats_user_is_administrator == '1' ||
		 cats_user_is_admin_group == '1' ||
		 cats_user_is_super_administrator == '1' ||
		 cats_user_is_root_administrator == '1' ||
		 cats_user_is_editor == '1' ){
		 
		 showMessageOfTodayButton = true;
		 
	}

	$(document).ready(function() {
		if(showMessageOfTodayButton)
		{
			$("#myModalMessageOfTheDay").show();
		}
		
	});		
</script>

<?php  

if(!cats_user_is_reader(false) && !cats_user_is_reader(true) && !cats_user_is_editor(true) && !cats_user_is_editor(false) && !cats_user_is_incident_analysis() && !cats_user_is_incident_privileged() && !cats_user_is_pcr_administrator() && !cats_user_is_super_administrator()){
	$_SESSION['messageStack']->add("You do not have access to the Landing page / Workload Review");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

?>