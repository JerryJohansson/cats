<?php
 // echo 'hello world!' . $_POST["headline"] . " " . $_POST["message"]  ;


$config = parse_ini_file("../../conf/" . $_SERVER['SERVER_NAME'] . "/cats_admin.ini");

foreach ($config as $key => $value) {

	if(!defined($key))

		define($key, $value);

}

    $conn = OCILogon(DB_USERNAME, DB_PASSWORD, DB_HOSTNAME . '/' . DB_DATABASE);

	// Update previous message obsolete
	$sql = "update CATSDBA.TBLMOTD set motd_enddate = sysdate WHERE MOTD_Type = 0 and MOTD_EndDate >= SysDate";
    $stmt = OCIParse($conn, $sql);
    OCIExecute($stmt);

	// Insert new message
	$sql = "insert into CATSDBA.TBLMOTD
	(MOTD_TYPE,
		MOTD_URL,
		MOTD_SHORT,
		MOTD_DESC,
	   MOTD_STARTDATE,
		MOTD_ENDDATE,
		MOTD_FREQUENCY,
		MOTD_REPEATPERIOD,
		MOTD_DELETE,
	   MOTD_DATEOPTION_1,
		MOTD_DATEOPTION_2,
		MOTD_DATEOPTION_3,
		MOTD_SITE,
		TIMESTAMP)
	VALUES
	( 0, null, '" . $_POST["headline"] ."', '" . $_POST["message"]  . "', sysdate, '31-MAY-20', 1, 1, 0, 0, 0, 0, '2,3,5,1', null)";
	
    $stmt = OCIParse($conn, $sql);
    OCIExecute($stmt);
	
    OCIFreeStatement($stmt);  

    OCILogoff($conn);	
	
	sleep(1);
	
?>

<script>
	//top.show_screen('dashboard','search');
	//window.location.href = "/"; 
	window.location.replace("http://" + window.location.hostname + "/index.php");
</script>
