<?php
$username="";
/* 
// The following doesn't work anyway (don't know why ??)
if(defined('CATS_REUSE_LOGIN') && (bool)CATS_REUSE_LOGIN == true) {
	$username = $_SESSION['user_details']['login'];
}
*/
?>
<script src="js/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>

<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script>
//================================================================================//
//	XML Requests/Responses
//================================================================================//
function alertMe(s){alert(s);return s;}
var oXML=oXML2=null;
function cats_get_path(path,module){
	return path+module+".php";
}
function _login(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	var s="";
	var i=0;
	for(i=0;i<f.length;i++){
		s+=""+f[i].name+"="+escape(f[i].value)+"&";
	}
	oXML=_HTTPRequest(url,"post",return_login,s.replace(/&$/,""));
	enable_disable_form(f,true);
	return false;
}
function return_login(o){
	try{
		eval('var e='+o);
		if(e){
			for(a in e) e[a]=unescape(e[a]);
		}
		if(e.success=='true'){
			show_hide_login();
			top.hideDialogs();
			top.gui.session.reset();
		}else{
			if(e.message) _error_message(e.message);
			else _error_message("Username or Password was incorrect. Please try again.");
		}
		var f=document.forms.frmLogin;
		enable_disable_form(f,false);
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);}
}
function enable_disable_form(f,b){
	for(i=0;i<f.length;i++){
		f[i].disabled=b;
	}
}
function show_hide_login(){
	element('dialog_login_form').style.display=arguments[0]?'block':'none';
	element('dialog_actions').style.display=arguments[0]?'none':'block';
	document.forms['frmLogin'].elements['pwd'].value='';
}

function get_height(){
	var h=document.body.firstChild.offsetHeight+20;
	if(h<60) h=200;
	return h;
}
function _close(){
	if(element('dialog_login_form').style.display!='none'){
		top.window.close();
	}else top.hideDialogs();
}
onload = function(){
	try{
	//var f=document.forms.frmLogin[<?php $idx=(($username!="")?1:0);echo $idx;?>];
	var f=document.forms.frmLogin[0];
	f.value = top.gui.session.login;
	f=document.forms.frmLogin[1];
	f.focus();
	}catch(e){}
}
function _keep_alive(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	oXML=_HTTPRequest(url,"post",return_session,"action=renew");
	enable_disable_form(f,true);
	return false;
}
function return_session(o){
	try{
		eval('var e='+o);
		if(e.success){
			top.hideDialogs();
			top.gui.session.reset();
			var f=document.forms.frmSession;
			enable_disable_form(f,false);
		}else{
			if(e.message) _error_message(e.message);
			else _error_message("An error occurred while refreshing your session. You will automatically be logged out.\nSorry, but, all unsaved work will be lost.");
			top.close();
		}
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);}
}
</script>
<link type="text/css" rel="stylesheet" href="<?php echo(WS_STYLE_PATH."dialog.css"); ?>" />
<link type="text/css" rel="stylesheet" href="<?php echo(WS_STYLE_PATH."$m.css"); ?>" />
</head>
<body class="login"><div class="dialog">
<div class="dialog_title_bg">
	<div class="dialog_title">
		<img src="<?php echo WS_ICONS_PATH;?>btn_close_bg.gif" onClick="_close();" align="right" />
		<span id="dialog_title"></span>
	</div>
</div>
<div id="dialog_message" class="box"></div>
<div id="dialog_actions"><form name="frmSession" method="POST" onSubmit="return _keep_alive(this);">
	<div class="buttonblock">
		<input type="submit" class="submit" name="cats::OK" value="OK" />
	</div></form>
</div>
<div id="dialog_login_form" style="display:none;">
	<div id="form">
		<form id="frmLogin" onSubmit="return _login(this);">
			
			<div class="block">
			<label for="uid">Login</label>
			<input readonly class="sfield" type="text" size="10" name="uid" id="uid" value="<?php echo $username;?>" />
			</div>
			
			<div class="block">
			<label for="pwd">Password</label>
			<input class="sfield" type="password" size="10" name="pwd" id="pwd" value="" />
			</div>
			
			<div class="buttonblock">
			<input type="submit" class="submit" name="action" value="Login" />
			<?php if(isset($redirect)){ $redirect_url=$redirect; }else{ $redirect_url = "/" ;} ?>
 			<input type="hidden" name="rurl" value="<?php echo($redirect_url);?>" /> 
			</div>
			
		</form>
	</div>
</div>
<div id="copyright" class="copyright">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</div>
</div>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
