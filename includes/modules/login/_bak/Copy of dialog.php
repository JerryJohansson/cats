<?php
//echo CATS_MAINTENANCE;
if(defined('CATS_MAINTENANCE') && CATS_MAINTENANCE=="yes" && !isset($_GET['cats_maintenance'])){
	require_once(CATS_INCLUDE_PATH."page_header.php");
	require_once(CATS_MODULES_PATH."index/maintenance.php");
	require_once(CATS_INCLUDE_PATH."page_footer.php");
	exit;
}
$username="";
if(defined('CATS_REUSE_LOGIN') && (bool)CATS_REUSE_LOGIN == true) {
	$username = $_SESSION['user_details']['login'];
}
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<style type="text/css">
body{
	overflow: hidden;
	margin: 0px;
	border: none;
}
div.block, div.step
{
	display: block;
	clear: both;
	padding: 0px;
	margin-top: 0.5ex;
	margin-bottom: 0.5ex;
}
div.step
{
	background-color: #f0f0f0;
	margin: 0ex;
	border-bottom: dashed 2px #808080;
}

div.buttonblock
{
	margin-top: 1ex;
	margin-bottom: 1ex;
	text-align:center;
}
input.halfbox {
	width: 230px;
}

.block label
{
	font-weight: bold;
	padding-right: 1ex;
	white-space: nowrap;
	display: block;
}
.box {
	border: 1px dashed #666;
}
div.box {
	margin: 15px;
	padding: 0px 10px;
	font: bold 12px Verdana;
}
</style>
<script>
//================================================================================//
//	XML Requests/Responses
//================================================================================//
function alertMe(s){alert(s);return s;}
var oXML=oXML2=null;
function cats_get_path(path,module){
	return path+module+".php";
}
function _login(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	var s="";
	var i=0;
	for(i=0;i<f.length;i++){
		s+=""+f[i].name+"="+escape(f[i].value)+"&";
	}
	oXML=_HTTPRequest(url,"post",return_login,s.replace(/&$/,""));
	enable_disable_form(f,true);
	return false;
}
function return_login(o){
	try{
		eval('var e='+o);
		if(e){
			for(a in e) e[a]=unescape(e[a]);
		}
		if(e.success=='true'){
			show_hide_login();
			top.hideDialogs();
			top.gui.session.reset();
		}else{
			if(e.message) _error_message(e.message);
			else _error_message("Username or Password was incorrect. Please try again.");
		}
		var f=document.forms.frmLogin;
		enable_disable_form(f,false);
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);}
}
function enable_disable_form(f,b){
	for(i=0;i<f.length;i++){
		f[i].disabled=b;
	}
}
function show_hide_login(){
	element('login_form').style.display=arguments[0]?'block':'none';
	element('dialog_actions').style.display=arguments[0]?'none':'block';
	document.forms['frmLogin'].elements['pwd'].value='';
}

function get_height(){
	var h=document.body.firstChild.offsetHeight+20;
	if(h<60) h=200;
	return h;
}
function _close(){
	if(element('login_form').style.display!='none'){
		top.window.close();
	}else top.hideDialogs();
}
onload = function(){
	try{var f=document.forms.frmLogin[<?php $idx=(($username!="")?1:0);echo $idx;?>];
	f.focus();
	}catch(e){}
}
function _keep_alive(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	oXML=_HTTPRequest(url,"post",return_session,"action=renew");
	enable_disable_form(f,true);
	return false;
}
function return_session(o){
	try{
		eval('var e='+o);
		if(e.success){
			top.hideDialogs();
			top.gui.session.reset();
			var f=document.forms.frmSession;
			enable_disable_form(f,false);
		}else{
			if(e.message) _error_message(e.message);
			else _error_message("An error occurred while refreshing your session. You will automatically be logged out.\nSorry, but, all unsaved work will be lost.");
			top.close();
		}
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);}
}
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="login"><div>
<div class="dialog_title_bg">
	<div class="dialog_title">
		<img src="<?php echo WS_ICONS_PATH;?>btn_close_bg.gif" onclick="_close();" align="right" />
		<span id="dialog_title"></span>
	</div>
</div>
<div id="dialog_message" class="box"></div>
<div id="dialog_actions"><form name="frmSession" method="POST" onSubmit="return _keep_alive(this);">
	<div class="buttonblock">
		<input type="submit" name="cats::OK" value="OK" />
	</div></form>
</div>
<div id="login_form" style="display:none;">
	<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="box">
	<tr>
		<td align="center" valign="middle">
			<form name="frmLogin" method="POST" onSubmit="return _login(this);">
			
			<table>
			<tr valign="top">
				<td><img src="<?php echo WS_ICONS_PATH;?>login.gif" alt=""/></td>
				<td>
					<div class="block">
					<label for="id1">Login</label>
					<input class="halfbox" type="text" size="10" name="uid" id="id1" value="<?php echo $username;?>" />
					</div>
					
					<div class="block">
					<label for="id2">Password</label>
					<input class="halfbox" type="password" size="10" name="pwd" id="id2" value="" />
					</div>
					
					<div class="buttonblock">
					<input type="submit" name="action" value="Login" />
					</div>
				</td>
			</tr>
			</table>
			
			<input type="hidden" name="rurl" value="<?php echo($redirect);?>" />
			
			</form>	
		</td>
	</tr>
	</table>
</div></div>