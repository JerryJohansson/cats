<?php
//echo CATS_MAINTENANCE;
if(defined('CATS_MAINTENANCE') && CATS_MAINTENANCE=="yes" && !isset($_GET['cats_maintenance'])){
	require_once(CATS_INCLUDE_PATH."page_header.php");
	require_once(CATS_MODULES_PATH."index/maintenance.php");
	require_once(CATS_INCLUDE_PATH."page_footer.php");
	exit;
}
$redirect = "";//"index.php?top=true";
$username="";
if(isset($_GET['rdir'])) $redirect = urldecode($_GET['rdir']);
if(isset($_GET['uid'])) $uid=$_GET['uid'];
if(isset($uid) && $uid!="") $username=$uid;

switch(strtolower($server_name)){
	case 'catsdev':
		$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
		$msg .= '<p align="left" style="padding:10px"><b>Note:</b> You are about to login to the development version of CATS</p>';
		$window_name="CATS_DEV_WINDOW";
		break;
	case 'catstst':
		$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
		$msg .= '<p align="left" style="padding:10px"><b>Note:</b> You are about to login to the testing version of CATS</p>';		
		$window_name="CATS_TST_WINDOW";
		break;
	default:
		$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
		$window_name="CATS_LOCAL_WINDOW";
		break;
}

?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<style type="text/css">
div.block, div.step
{
    display: block;
    clear: both;
    padding: 0px;
    margin-top: 0.5ex;
    margin-bottom: 0.5ex;
}
div.step
{
	background-color: #f0f0f0;
	margin: 0ex;
	border-bottom: dashed 2px #808080;
}

div.buttonblock
{
	margin-top: 1ex;
	margin-bottom: 1ex;
	text-align:center;
}
input.halfbox {
	width: 230px;
}

.block label
{
	font-weight: bold;
	padding-right: 1ex;
	white-space: nowrap;
	display: block;
}
table.box {
	border: 1px dashed #666;
}

</style>
<script>
//================================================================================//
//	XML Requests/Responses
//================================================================================//
function alertMe(s){alert(s);return s;}
var oXML=oXML2=null;
function cats_get_path(path,module){
	return path+module+".php";
}
function _login(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	var s="";
	var i=0;
	for(i=0;i<f.length;i++){
		s+=""+f[i].name+"="+escape(f[i].value)+"&";
	}
	//alert(s+"\n"+url)
	oXML=_HTTPRequest(url,"post",return_login,s.replace(/&$/,""));
	for(i=0;i<f.length;i++){
		f[i].disabled=true;
	}
	//oXML2=_HTTPRequest(url,"post",return_window,"action=dash");
	return false;
}
var CATS_MAIN_WINDOW=null;
function open_cats_main_window(url){
	return show_window(url,"<?php echo($window_name);?>",getPopupFeatures(screen.availWidth,screen.availHeight,screen.availLeft,screen.availTop));//+",fullscreen=1");
}
function return_login(o){
	try{
		eval('var e='+o);
		if(e){
			for(a in e) 
				if(typeof(e[a])=="string") e[a]=unescape(e[a]);
		}
		if(e.success){
			if(e.url && e.url!=""){
				var url=e.url;
				if(e.redirect && e.redirect!="")
					url+="&rdir="+escape(e.redirect);
				CATS_MAIN_WINDOW = open_cats_main_window(url);
				CATS_MAIN_WINDOW.resizeTo(screen.availWidth,screen.availHeight);
				CATS_MAIN_WINDOW.moveTo(screen.availLeft,screen.availTop);
			}else{
				_error_message("An unspecified error occured: Please note the steps you took to get to this message and report it to catsadmin@tiwest.com.au");
			}
		}else{
			var s = "";
			if((dup=e.duplicates)!=null) {
				var x = dup.length;
				for(i=0;i<x;i++){
					s+="\n"+dup[i][0] + " : " + dup[i][1];
				}
				s+="\nA message has been sent to the IT department but you should confirm that they got it.";
			}
			if(e.message) _error_message(e.message+s);
			else _error_message("Username or Password was incorrect. Please try again.");
			var f=document.forms[0];
			for(i=0;i<f.length;i++){
				f[i].disabled=false;
			}
		}
	}catch(e){_error_message(e+"\n"+e.number+"\n"+e.description+"\n"+o);}
}

onload = function(){
	var f=document.forms[0][<?php $idx=(($username!="")?1:0);echo $idx;?>];
	f.focus();
}
window.onfocus=function(){
	if(CATS_MAIN_WINDOW!=null){
		CATS_MAIN_WINDOW.focus();
	}
}
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="login">
<table id="mainHeader" cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
<tr valign="middle">
	<td>
		<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="box">
		<tr>
			<td align="center" valign="middle">
				<form method="POST" onSubmit="return _login(this);">
		
				<div class="maincontentheader">
					 <?php echo($msg);?>
					 <p>Please enter a valid login and password.</p>
					 <p><!-- message --></p>
				</div>
				<table>
				<tr valign="top">
					<td><img src="<?php echo WS_ICONS_PATH;?>login.gif" alt=""/></td>
					<td>
						<div class="block">
						<label for="id1">Login</label>
						<input class="halfbox" type="text" size="10" name="uid" id="id1" value="<?php echo $username;?>" />
						</div>
						
						<div class="block">
						<label for="id2">Password</label>
						<input class="halfbox" type="password" size="10" name="pwd" id="id2" value="" />
						</div>
						
						<div class="buttonblock">
						<input type="submit" name="action" value="Login" />
						</div>
					</td>
				</tr>
				</table>
				
				<input type="hidden" name="rurl" value="<?php echo($redirect);?>" />
				
				</form>	
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="copyright" height="40">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</td>
</tr>
</table>
