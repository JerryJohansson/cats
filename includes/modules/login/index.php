<?php
$redirect = "";
$username="";
$msg = "";
if(isset($_GET['rdir'])) $redirect = urldecode($_GET['rdir']);
if(isset($_GET['uid'])) $uid=$_GET['uid'];
if(isset($uid) && $uid!="") $username=$uid;

//echo CATS_MAINTENANCE;
if(defined('CATS_MAINTENANCE') && CATS_MAINTENANCE=="yes" && !isset($_GET['cats_maintenance'])){
	switch(strtolower($server_name)){
		case 'catsdev':
			$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
			$msg .= '<p align="left" style="padding:10px"><b>Note:</b> You are trying to login to the development version of CATS</p>';
			$msg .= '
					<h4 style="color:red">Sorry, <b>'.strtoupper($server_name).'</b> is currently closed for maintenance. Please check back soon</h4>
			';
			break;
		case 'catstst':
			$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
			$msg .= '<p align="left" style="padding:10px"><b>Note:</b> You are trying to login to the testing version of CATS</p>';		
			$msg .= '
			<h4 style="color:red">Sorry, <b>'.strtoupper($server_name).'</b> is currently closed for maintenance. Please check back soon</h4>
			';
			break;
		default:
			$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
			$msg .= '
			<h4 style="color:red">Sorry, <b>'.strtoupper($server_name).'</b> is currently closed for maintenance. Please check back soon</h4>
			';
			break;
	}
	if(isset($_SESSION['bypass_cats_maintenance']) && $_SESSION['bypass_cats_maintenance']=='yes'){
		$show_login_screen = true;
	}else{
		$show_login_screen = false;
	}
}else{
	$show_login_screen = true;
	switch(strtolower($server_name)){
		case 'catsdev':
			$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
			$msg .= '<p align="left" style="padding:10px"><b>Note:</b> You are about to login to the development version of CATS</p>';
			$window_name="CATS_DEV_WINDOW";
			break;
		case 'catstst':
			$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
			$msg .= '<p align="left" style="padding:10px"><b>Note:</b> You are about to login to the testing version of CATS</p>';		
			$window_name="CATS_TST_WINDOW";
			break;
		default:
			$msg .= '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
			$window_name="CATS_LOCAL_WINDOW";
			break;
	}
}
?>
<script src="js/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>

<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/<?php echo("$m.js"); ?>"></script>
<script>
function open_cats_main_window(url){
	//return show_window(url,"<?php echo($window_name);?>",getPopupFeatures(screen.availWidth,screen.availHeight,screen.availLeft,screen.availTop));//+",fullscreen=1");
	window.location.href = url;
	return window;
}
</script>
<link type="text/css" rel="stylesheet" href="<?php echo(WS_STYLE_PATH."$m.css"); ?>" />
</head>
<body class="login">
<div id="login_form">
	<?php 
	echo($msg);
	if($show_login_screen){
	?>
	<p>Please enter your network username and password.<br><small><font color=blue >Not Ellipse</font></small></p>
	<div id="form">
		<form id="login" name="frmLogin" onSubmit="return _login(this);">
			
			<div class="block">
			<label for="uid">Login</label>
			<input class="sfield" type="text" size="10" name="uid" id="uid" value="<?php echo $username;?>" />
			</div>
			
			<div class="block">
			<label for="pwd">Password</label>
			<input class="sfield" type="password" size="10" name="pwd" id="pwd" value="" />
			</div>
			
			<div class="buttonblock">
			<input type="submit" class="submit" name="action" value="Login" />
			<input type="hidden" name="rurl" value="<?php echo($redirect);?>" />
			</div>
			
		</form>
	</div>
	<?php 
	}
	?>
</div>
<div id="copyright" class="copyright">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</div>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>