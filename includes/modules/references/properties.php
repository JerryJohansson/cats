<?php

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search'){
	$ret = array();
	switch($action){
		case 'search':
		case 'results':		
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Reference Table',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Reference Table',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Reference Table',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables()
{
	return array(
		'table'=>'TBLREFTABLE', 
		'id'=>'REFTABLE_ID',
		'view'=> 'TBLREFTABLE'
	);
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{

	$ret = array();

	switch($action)
	{	
		case 'results':
		case 'search':
			$ret = array(
			  'REFTABLE_TYPE' => array('label'=>'Reference Type', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_reference_table_types_dd', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'REFTABLE_DESCRIPTION' => array('label'=>'Description', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_reference_table_descriptions_dd', 'col'=>1, 'col_span'=>2)
			);
			break;
	  case 'new':
			$ret =  array(
			  'REFTABLE_TYPE' => array('label'=>'Reference Type', 'value'=>'', 'func'=>'html_form_draw_reference_table_types_dd', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'REFTABLE_DESCRIPTION' => array('label'=>'Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'REFTABLE_COMMENT' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
			);
		     break;
		case 'edit':
			$ret =  array(
			  'REFTABLE_TYPE' => array('label'=>'Reference Type', 'value'=>'', 'func'=>'html_form_draw_reference_table_types_dd', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'REFTABLE_DESCRIPTION' => array('label'=>'Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'REFTABLE_COMMENT' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
			);
		  break;
	}
	return $ret;
}


// Fix page parameter
if($p=='post')
	$pg=(isset($a) && $a=='add')?'new':'edit';
else
    $pg=$p;


// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($pg);
$TABLES  = getPropertiesTables($pg);
$FIELDS  = getPropertiesFields($pg);

?>