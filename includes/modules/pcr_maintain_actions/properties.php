<?php

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Default PCR Actions',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New PCR Action',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'View PCR Actions',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search PCR Actions',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		default:
			$ret = array_merge(
				array(
					'table'=>'TBLPCR_ACTION', 
					'id'=>'ACTION_ID',
					'view'=>'VIEW_PCR_DEFAULT_ACTIONS',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();

	switch($page){
		case 'edit': case 'new':
			$ret = array_merge($ret, 
				array(
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2),
					'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2),
					'DAYS_AFTER_PREV_STAGE' => array('label'=>'Days After Previous Stage', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2),
					'ACTION_DESCRIPTION' => array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'view': 
			$ret = array_merge($ret, 
				array(
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'MANAGED_BY' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'DAYS_AFTER_PREV_STAGE' => array('label'=>'Days After Previous Stage', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ACTION_DESCRIPTION' => array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
				)
			);
			break;
		case 'search': case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2),
				'MANAGED_BY_ID' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2),
				'DAYS_AFTER_PREV_STAGE' => array('label'=>'Days After Previous Stage', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}
if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>