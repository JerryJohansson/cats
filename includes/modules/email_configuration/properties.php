<?php


function getPropertiesTables(){
	return array('table'=>'TBLEMAILOPTIONS', 'view'=>'TBLEMAILOPTIONS','id'=>'EMAIL_ID');
}

function getPropertiesFields(){
	return array(
		'EMAIL_STATUS' => array('label'=>'Email Status', 'value'=>NULL, 'func'=>'html_get_email_status_dd', 'col'=>1, 'col_span'=>2), 
		'EMAIL_SERVER' => array('label'=>'Email Server', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
		'EMAIL_FROMNAME' => array('label'=>'Email From Name', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
		'EMAIL_FROMADDRESS' => array('label'=>'Email From Address', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
		'EMAIL_TESTNAME' => array('label'=>'Email Test Name', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
		'EMAIL_TESTADDRESS' => array('label'=>'Email Test Address', 'value'=>NULL, 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2)
	);
}


$TABLES=getPropertiesTables();
$FIELDS=getPropertiesFields();
?>