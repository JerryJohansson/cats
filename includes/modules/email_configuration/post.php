<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;
if($id!=false){
	//exit("action = $a and id = $id");
	$ret = true;
	switch($action){
		case "add": // Add record
			$editor = new Editor($action,"admin");
			$ret = $editor->insert();
			break;
		case "edit": // Update node
			//print_r($_POST);
			$editor = new Editor($action,"admin");
			$ret = ($editor->update($id) > 0);
			break;
		default:
			$ret = false;
			break;
	}
	
	if($ret) {
		$success_return_module = 'dashboard';
		$success_remove_page = 'search';
		include(CATS_INCLUDE_PATH . 'success.inc.php');
	} else {
		dprint(__FILE__,__LINE__,0,"Error occured while editing");
	}
}else{
	main_redirect("index.php?m=$m&p=edit");
}

$db=db_close();
//exit("<br>DONE_POST");
?>