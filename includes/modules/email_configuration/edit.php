<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
// init the tabs index for tab iteration
$iTabs=0;
$iTabButtons = 0;
$RS=array();
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	//initTabs();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return CATS_validateForm(this, 'EMAIL_STATUS','','R','EMAIL_SERVER','','R','EMAIL_FROMNAME','','R','EMAIL_FROMADDRESS','','RisEmail','EMAIL_TESTNAME','','R','EMAIL_TESTADDRESS','','RisEmail');">
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
</fieldset>
<fieldset class="tool_bar">
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModue();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo ucwords((string)$module_name_txt);?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed=new Editor($m, $p, $a);
//echo("\$id=$id");
if($id>0){
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id));
}else{
	echo($ed->buildForm($FIELDS));
}
?>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" class="submit" name="cats::save" value="Save" >
<input type="button" class="button" name="cats::cancel" value="Cancel" onclick="_m.cancel();">
</fieldset>		
</fieldset>

</div>
</form><?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>