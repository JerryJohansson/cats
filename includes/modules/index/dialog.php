<?php
//echo CATS_MAINTENANCE;
if(defined('CATS_MAINTENANCE') && CATS_MAINTENANCE=="yes" && !isset($_GET['cats_maintenance'])){
	require_once(CATS_INCLUDE_PATH."page_header.php");
	require_once(CATS_MODULES_PATH."index/maintenance.php");
	require_once(CATS_INCLUDE_PATH."page_footer.php");
	exit;
}
$username="";
if(defined('CATS_REUSE_LOGIN') && (bool)CATS_REUSE_LOGIN == true) {
	$username = $_SESSION['user_details']['login'];
}
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<style type="text/css">
body{
	overflow: hidden;
	margin: 0px;
	border: none;
}

.box {
	border: 1px dashed #666;
}
div.box {
	margin: 15px;
	padding: 0px 10px;
	font: bold 12px Verdana;
}
div.box a, div.box a:link, div.box a:visited{
	display:block;
	padding: 3px 3px 3px 30px;
	background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_results_bg.gif);
	background-position:left;
	background-repeat:no-repeat;
}
div.box a:hover{
	background-color: #CCCCCC;
}

</style>
<script>
function _close(){
	top.hideDialogs();
}
function get_height(){
	var h=document.body.firstChild.offsetHeight+20;
	if(h<60) h=200;
	return h;
}
//onload = resize;
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="login"><div class="dialog">
<div class="dialog_title_bg">
	<div class="dialog_title">
		<img src="<?php echo WS_ICONS_PATH;?>btn_close_bg.gif" onclick="_close();" align="right" />
		<span id="dialog_title"></span>
	</div>
</div>
<div id="dialog_message" class="box" style="height:150px; overflow:auto;">Messages</div>
<div id="form[1]" style="display:none;"></div>
</div>