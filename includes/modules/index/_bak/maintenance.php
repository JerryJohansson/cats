<?php
$redirect = "";//"index.php?top=true";
$username="";
if(isset($_GET['rdir'])) $redirect = urldecode($_GET['rdir']);
if(isset($_GET['uid'])) $uid=$_GET['uid'];
if(isset($uid) && $uid!="") $username=$uid;
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<style type="text/css">
div.block, div.step
{
    display: block;
    clear: both;
    padding: 0px;
    margin-top: 0.5ex;
    margin-bottom: 0.5ex;
}
div.step
{
	background-color: #f0f0f0;
	margin: 0ex;
	border-bottom: dashed 2px #808080;
}

div.buttonblock
{
	margin-top: 1ex;
	margin-bottom: 1ex;
	text-align:center;
}
input.halfbox {
	width: 230px;
}

.block label
{
	font-weight: bold;
	padding-right: 1ex;
	white-space: nowrap;
	display: block;
}
table.box {
	border: 1px dashed #666;
}

</style>
<script>
//================================================================================//
//	XML Requests/Responses
//================================================================================//
function alertMe(s){alert(s);return s;}
var oXML=oXML2=null;
function cats_get_path(path,module){
	return path+module+".php";
}
function _login(f){
	var url = cats_get_path(CATS_REMOTE_PATH,"users");
	var s="";
	var i=0;
	for(i=0;i<f.length;i++){
			s+=""+f[i].name+"="+escape(f[i].value)+"&";
	}
	//alert(s+"\n"+url)
	oXML=_HTTPRequest(url,"post",return_login,s.replace(/&$/,""));
	//oXML2=_HTTPRequest(url,"post",return_window,"action=dash");
	return false;
}
onload = function(){
	var f=document.forms[0][<?php $idx=(($username!="")?1:0);echo $idx;?>];
	f.focus();
}
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="login">
<table id="mainHeader" cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
<tr valign="middle">
	<td>
		<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="box">
		<tr>
			<td align="center" valign="middle">
				<form method="POST" onSubmit="return _login(this);">
		
				<div class="maincontentheader">
					 <?php
switch(strtolower($server_name)){
	case 'catsdev':
		echo '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
		echo '<p align="left" style="padding:10px"><b>Note:</b> You are trying to login to the development version of CATS</p>';
		$message = '
				<h4 style="color:red">Sorry, <b>'.strtoupper($server_name).'</b> is currently closed for maintenance. Please check back soon</h4>
		';
		$disabled = ' disabled="true" ';
		break;
	case 'catstst':
		echo '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
		echo '<p align="left" style="padding:10px"><b>Note:</b> You are trying to login to the testing version of CATS</p>';		
		$message = '
		<h4 style="color:red">Sorry, <b>'.strtoupper($server_name).'</b> is currently closed for maintenance. Please check back soon</h4>
		';
		$disabled = ' disabled="true" ';
		break;
	default:
		echo '<h4>Welcome to ' . strtoupper($server_name) . '</h4>';
		$message = '
		<h4 style="color:red">Sorry, <b>'.strtoupper($server_name).'</b> is currently closed for maintenance. Please check back soon</h4>
		';
		$disabled = ' disabled="true" ';
		break;
}
					 ?>
					 <p><?php echo $message;?></p>
				</div>
				<table>
				<tr valign="top">
					<td><img src="<?php echo WS_ICONS_PATH;?>login.gif" alt=""/></td>
					<td>
						<div class="block">
						<label for="id1">Login</label>
						<input class="halfbox" type="text" size="10" name="uid" id="id1" value="<?php echo $username;?>" <?php echo $disabled;?> />
						</div>
						
						<div class="block">
						<label for="id2">Password</label>
						<input class="halfbox" type="password" size="10" name="pwd" id="id2" value="" <?php echo $disabled;?> />
						</div>
						
						<div class="buttonblock">
						<input type="submit" name="action" value="Login" <?php echo $disabled;?> />
						</div>
					</td>
				</tr>
				</table>
				
				<input type="hidden" name="rurl" value="<?php echo($redirect);?>" />
				
				</form>	
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="copyright" height="40">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</td>
</tr>
</table>
