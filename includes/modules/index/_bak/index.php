<?php
$default_page_url = "index.php";
if(isset($_GET['rdir'])) $default_page_url = urldecode($_GET['rdir']);
?>
<link type="text/css" rel="stylesheet" href="<?PHP echo WS_STYLE_PATH;?>top.css"/>
<style>
.trans {
	filter:alpha(opacity=60);
  -moz-opacity:0.60;
  opacity: 0.60;
}
</style>
<style type="text/css" media="print">
#loading_message,
.toolbar,
#top_logo,
#top_personal
{display:none}
</style>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script>
/*******************************
Set Global Variables
*******************************/
var HelperObjects=new Object(); // references to elements stored by a helper object
var IFrameHelpers=new Object(); // reference to helper iframes
var IFrameMenus=new Object(); // references to menu iframes
var IFrameDialogs=new Object(); // references to any message dialogs we have created

// Positional variables
// Screen coords
var CATS_SCREEN_OFFSET=82;//105;
var CATS_SCREEN_TOP=77;
var CATS_SCREEN_HEIGHT=0;
var CATS_CURRENT_SCREEN=null; // current iframe element
// toolbars coords
var TOOLBAR_TOP=54;
var TOOLBAR_HEIGHT=25;
var TOOLBAR_Y=TOOLBAR_TOP; //increment by TOOLBAR_HEIGHT after we create toolbar

// Dialog names
var LOADER_DIALOG_ID = 'cover_elements_that_show_through';
var MESSAGE_DIALOG_ID = 'message_dialog';
var SESSION_DIALOG_ID = 'session_dialog';
var ALERT_DIALOG_ID = 'alert_message';
// Dialog z-indexes - 
var SESSION_DIALOG_ZINDEX=500001;
var LOADER_DIALOG_ZINDEX=499998;
var MESSAGE_DIALOG_ZINDEX=500002;
var MENU_DIALOG_ZINDEX=500000;
var HELPER_DIALOG_ZINDEX=499997;

/*
+------------------------------------------------------------------------
|  BEGIN:: Objects and Classes
+------------------------------------------------------------------------
*/
function _session(){
// create a session object which holds session id and also
// provide a timeout function when session times out...popup a message dialog 5 minutes before
// session timeout with a button to press that will do a very quick request and close (this is to keep the session alive)
// if the user does not click the button in time I.E they are not near the machine at the time... 
// the message dialog should turn into a login dialog so when the user logs in using this popup
// they will not loose there place. This is good when the user has been working on a large form which could take some time
// and they need more info or something...we don't want them to loose all there hard work so we try and keep them where they are
// to avoid this from happening.
	// session and user details 
	this.SID='<?php echo(session_id());?>';
	this.groups=parseInt('<?php echo $_SESSION['user_details']['groups'];?>');
	this.login='<?php echo $_SESSION['user_details']['login'];?>';
	this.close_parent=<?php echo (defined('CATS_REUSE_LOGIN') && CATS_REUSE_LOGIN==true)?"false":"true";?>;
	this.user_id=parseInt('<?php echo $_SESSION['user_details']['user_id'];?>');
	this.site_id=parseInt('<?php echo $_SESSION['user_details']['site_id'];?>');
	this.site_access='<?php echo $_SESSION['user_details']['site_access'];?>';
	this.skins_path='<?php echo WS_STYLE_PATH;?>';
	this.initial_directory='<?php echo str_replace("\\","\\\\",$_SESSION['user_details']['site_folder']);?>';
	//
	this.history = new Object();
	this.history.ref = this;
	this.history.items = new Object(); // unique history items
	this.history.all = []; // array of all history items
	
	this.timeout_offset = (5*60000); // 5 minute buffer so user can request something to keep the session alive;//5000;//
	this.timeout = (<?php echo session_cache_expire();?>*60000)-this.timeout_offset;;//10000-this.timeout_offset;//
	this.interval=null;
	this.pulse = function(){
		this.interval = top.setInterval(this.message, this.timeout);
	}
	// every page should call this method with the onload
	// it will reset the timer so we don't prematurely popup the timeout message
	this.reset = function(){
		this.clear();
		this.pulse();
	}
	this.clear = function(){
		if(this.interval!=null)top.clearInterval(this.interval);
	}
	this.message = function(){
		// Show a dialog box with a "session is about to end" message in it
		top.gui.session.clear();
		top.gui.session.interval = top.setInterval(top.gui.session.logon, top.gui.session.timeout_offset);
		var msgs = ["Your session is about to end. Please click the OK button below to keep your session alive."];
		top.gui.session.dialog(SESSION_DIALOG_ID,'Session Warning', msgs, false, true);
	}
	this.logon = function(){
		// Show a dialog box with a login form
		top.gui.session.clear();
		var msgs = ["Your session has timed out. Please use the form below to re-submit your \"Login\" and \"Password\".",
							"If you close this window without logging in, you will automatically be logged out and all un-saved work will be lost."];
		top.gui.session.dialog(SESSION_DIALOG_ID,'Login',msgs,true,true);
	}
	this.dialog = function(dialog_id, title,msgs,showLogonForm,msg_p){
		// Show dialog 
		var fra = top.IFrameDialogs[dialog_id];
		var win = fra.contentWindow.window;
		var doc = win.document;
		var d=document.body;
		var msg = "";
		var i=0;
		var m_delim1=(msg_p)?"<p>":"";
		var m_delim2=(msg_p)?"</p>":"";
		for( i=0;i<msgs.length;i++) msg+=m_delim1+msgs[i]+m_delim2;
		var ot = doc.getElementById('dialog_title').innerHTML = title;
		var ob = doc.getElementById('dialog_message').innerHTML = msg;
		if(win.show_hide_login) win.show_hide_login(showLogonForm);
		var w=500;
		var x=(d.offsetWidth/2)-(w/2);
		fra.style.left = x;
		fra.style.width = w;
		fra.style.height=win.get_height();
		fra.style.display = 'block';
	}
	this.history.add = function(name,page,url){
		var e,add=true;
		var id=name+"_"+page;
		for(e in this.items){
			if((e)==(id)){
				add=false;
				break;
			}
		}
		this.all.push([name,page,url]);
		if(add) {
			this.items[id]=new Object();
			this.items[id].module = name;
			this.items[id].page = page;
			this.items[id].url = url;
		}
	}
	this.history.back = function(){
		var args=arguments;
		var hist=null;
		var curr=this.all.pop();
		if(args>0) hist = this.all[args[0]];
		else hist = this.all[this.all.length-1];
		//this.all.push(curr);
		//this.ignore=true;
		//alert(hist);
		//if(typeof(hist[2])=='undefined') top.doMenu(hist[0],hist[1]);
		//else top.doMenu(hist[0],hist[1],hist[2]);
		top.doMenu(hist[0],hist[1]);
	}
	this.history.display = function(){
		var msgs = [];
		for(e in this.items){
			msgs.push('<a href="javascript:top.show_screen(\''+this.items[e].module+'\',\''+this.items[e].page+'\');_close();" >'+this.items[e].page+" "+this.items[e].module+'</a>');
		}
		top.gui.session.dialog(MESSAGE_DIALOG_ID,'History',msgs,false,false);
	}
	this.reset();
	return this;
}
/*
+------------------------------------------------------------------------
|  Options object used to control SELECT options
+------------------------------------------------------------------------
*/
function _option(name){
	this.name = name;
	this._array = new Array();
	this._options = new Array();
	this._html = "";
	this.options = new Array();
	
	this.to_array = function(e){
		var i;
		var o=e.options;
		var x=o.length;
		var a=new Array();
		for(i=0;i<x;i++){
			if(o[i].text!="") a.push([o[i].text , o[i].value]);
		}
		return a;
	}
	this.add = function(cached,el,arr,selected){
		var value=0;
		var text=1;
		var i=0;
		var len=arr.length;
		
		this._array=this.to_array(el);
		var atop=this._array;
		var olen,add;
		
		for(i=0;i<len;i++){
			atop=this._array;
			olen=atop.length;
			add = true;
			if(olen==0){
				add = true;
			}else{
				JLOOP:for(j=0;j<olen;j++){
					if(String(atop[j][value])==String(arr[i][value])){
						add=false;
						break JLOOP;
					}
				}
			}
			//dalert(add);
			if(add){
				var o=new Option(arr[i][text],arr[i][value]);
				this.options.push(o);
				if(!cached) this._options.push([arr[i][value],arr[i][text]]);
			}
		}
		el.options.length=0;
		var x=this.options.length;
		//alert(x);
		for(i=0;i<x;i++) el.options[el.options.length]=new Option(this.options[i].text,this.options[i].value, false,(this.options[i].value==selected));
		//alert(arguments[3]);
		if(typeof(arguments[3])!="undefined" && arguments[3]!=null){
			var id=arguments[3];
			//alert("ID From options["+name+"].add="+id);
			if(typeof(id)=="object")
				id=id.value;
			return this.select(el,id);
		}
	}
	this.compare = function(x,y){
		return x-y;
	}
	this.select = function(el,id){
		var ret='';
		var i=0;
		var opts=el.options;
		var len=opts.length;
		for(i=0;i<len;i++){
			if(String(id)==String(opts[i].value)){
				ret=opts[i].value;
				opts[i].selected=true;
				break;
			}
		}
		return ret;
	}
	this.clear = function(el){
		el.options.length = 0;
		this.options.length = 0;
	}
	this.replace = function(cached,el,arr,selected){
		this.clear(el);
		this.add(cached,el,arr,selected);
	}
}
/*
+------------------------------------------------------------------------
|  Creates the GUI Object which holds screens/toolbars/and the session
+------------------------------------------------------------------------
*/
function _gui(){
	try{
		this.options=new Object(); // holds _option objects for use with helper dropdown object
		this.screens=new Object(); // holds screen objects like the page iframe and toolbars
		this.tabs=new Object(); // holds tab objects for use with tabbed pages
		this.session=new _session(); // holds the current session info
		this.shell = new ActiveXObject("Shell.Application"); // global shell object for general shellexecute stuff - BrowseForFolder, ShellExecute etc...
		this.dlg = new ActiveXObject("UserAccounts.CommonDialog"); // used for file browsing - we can set the initial directory for this ;)
	}catch(e){alert(e)}
	return this;
}
var gui=new _gui();
top.gui.options.add = function(name){
	if(typeof(top.gui.options[name])!="object")
		top.gui.options[name] = new _option(name);
}
top.gui.options.replace = function(name){
	top.gui.options.remove(name);
	top.gui.options.add(name);
	if(typeof(top.gui.options[name])!="object"){
		top.gui.options[name] = null;
		top.gui.options[name] = new _option(name);
	}
}
top.gui.options.remove = function(name){
	if(top.gui.options[name]) top.gui.options[name] = null;
}
// END:: Objects and Classes
//--------------------------

// Functions that return paths to commonly used areas
function cats_make_path(path,module){
	return path+module+".php";
}
function cats_get_remote_path(m){
	return cats_make_path(CATS_REMOTE_PATH,m);
}
function cats_get_helper_path(m){
	return cats_make_path(CATS_HELPER_PATH,m);
}
function cats_get_path(m){
	return cats_make_path(CATS_PATH,m);
}
//----------------------------------


function get_cats_screen_height(){
	return CATS_SCREEN_HEIGHT;
}
function set_cats_screen_height(){
	CATS_SCREEN_HEIGHT=getAvailableScreenHeight(CATS_SCREEN_OFFSET);
}
// return the available screen space of the page and offset it by the only parameter
function getAvailableScreenHeight(offset){
	var h=(!document.all)?window.innerHeight:document.body.offsetHeight;
	return h-((typeof(offset)=="number")?offset:0);
}

function cats_set_dialog_pos(){
	var a=arguments;
	var o=new Object();
	var d=document.body;
	o.w=(a[0])?a[0]:(d.offsetWidth*.7);
	o.h=(a[1])?a[1]:0;
	o.x=(d.offsetWidth/2)-(o.w/2);
	o.y=70;
	return o;
}


function helper_show_menu(func, module, e, d){
	hideHelpers();
	try{
		var value="";
		if(e!=null) value=e.value;
		var dvalue = "";
		if(d!=null){
			dvalue=d.value;
			if(dvalue.indexOf(",")!=-1) dvalue=dvalue.split(",")[0];
		}
		var doc=document.body;
		w=(doc.offsetWidth*.6);
		h=200;
		x=(doc.offsetWidth/2)-(w/2);
		y=100;
		var obj={
			'a':func,
			'm':module,
			'n':func,
			'p':dvalue,
			'id':value
		};
		var q = remote._query(obj);
		var url=cats_get_helper_path(module)+"?"+q;
		var o=IFrameHelpers[func];
		if(o==null){
			IFrameHelpers[func] = create_iframe(func,url,w,h,x,y,HELPER_DIALOG_ZINDEX);
			if(dvalue!='') HelperObjects[func].initial_search_value = dvalue;
		}else{
			var doit=false;
			if(dvalue!='') {
				doit=(HelperObjects[func].initial_search_value && (HelperObjects[func].initial_search_value.toLowerCase() != dvalue.toLowerCase()));
				HelperObjects[func].initial_search_value = dvalue;
			}
			// force refresh with last argument
			if(arguments.length>4) doit=arguments[4];
			if(doit) o.contentWindow.window.document.location.href=url;
			o.style.display = 'block';
		}
	}catch(e){alert(e);}
}
HelperObjects.set=function(id,o){
	var h=this[id];
	this[id].return_value=o;
	if(h){
		var s="id="+id+"\n";
		for(e in o){
			s+="e="+e+":h="+o[e]+":";
			if(h[e]){
				s+="name="+h[e].type+":h="+h[e]+":";//+h[e].getAttribute('name')
				if(o[e]) {
					if(setFormElement(h[e],o[e]))
						continue;
						//if(h[e].onchange) h[e].onchange();
				} else {
					var name=h[e].getAttribute('name');
					if(name.indexOf("_text")==(name.length-5)) h[e].value='N/A';
					else h[e].value='';
				}
			}
			s+="\n";
		}
		if(this[id].trigger_function) {
			this[id].trigger_function();
			delete this[id].trigger_function;
		}
		//alert(s);
		//this[id]=null;
		hideHelpers();
	}
}
// set the value of a form element or innerHTML of block element
function setFormElement(){
	var args = setFormElement.arguments;
	var value = (args.length>1)?args[1]:"";
	var ret = false;
	if( typeof( args[0] ) == "object" ){
		if((typeof(args[0].type) == 'undefined') && args[0].length){ // assume we have a radio/checkbox set
			if(typeof(args[0][0])=="object"){
				for(i=0;i<args[0].length;i++){
					if( args[0][i].value == value ) {
						args[0][i].checked = true;
						ret = true;
						break;
					}
				}
			}
		}else{
			if( args[0].nodeName.match(/INPUT|TEXTAREA|SELECT|BUTTON/gi) == null ) {
				try{
					args[0].innerHTML=value;
					return true;
				}catch(e){
					alert("The object is not a form element");
					return false;
				}
				
			}
			switch( args[0].type ){
				case "text": case "textarea": case "hidden": case "password":
					args[0].value = value;
					break;
				case "radio": case "checkbox" :
					if( args[0].value == value ) 
						args[0].checked;
					break;
				case "button": case "submit": case "reset":
					args[0].value = value;
					break;
				case "select-one":
					for(i=0;i<args[0].options.length;i++){
						if( args[0][i].value == value ) {
							ret = (i!=args[0].selectedIndex);
							args[0][i].selected = true;
							break;
						}
					}
					break;
				default:
					if( args[0].nodeName.toLowerCase() == "select" ){
						for(i=0;i<args[0].options.length;i++){
							if( args[0][i].value == value ) {args[0][i].selected = true;break;}
						}
					}else if( args[0].type.match(/[button|submit|reset]/) ){
						if(args[0].type!="password") if(confirm("are u sure u want to change the value of the button "+args[0].getAttribute("name") +" from "+args[0].getAttribute("title") )) args[0].value = value;
					}
					break;
			}
		}
	}
	return ret;
}


function check_search_module(name){
	return name.match(/index|dashboard|reallocate|email_configuration|form/)==null;
}
function resize_menu_height(){
	for(e in IFrameMenus)
		IFrameMenus[e].style.height = alertMe(get_menu_height(e));
}
function get_menu_height(name){
	return IFrameMenus[name].contentWindow.window.get_height();
}
var MENUS_LOADING=null;
function set_menus_loaded(){
	MENUS_LOADING=null;
}
function showMenu(id){
	var name="menu["+id+"]";
	if(MENUS_LOADING) return;
	//hideMenus();
	if( !isset(IFrameMenus[name]) ){
		MENUS_LOADING=true;
		var o=cats_set_dialog_pos();
		var h=0;
		var url = cats_get_helper_path('menus');
		url += "?a=get_menu&id="+id;
		IFrameMenus[name] = create_iframe(name,url,o.w,h,o.x,o.y,MENU_DIALOG_ZINDEX);
	}else
		IFrameMenus[name].style.display = 'block';
}

function show_screen(m,p){
	var args=arguments;
	if(m=='dashboard'){
		doMenu('index','dashboard');
	}else{
		if((args.length>2) && (typeof(args[2])=="object")){
			doMenu(m,p,args[2]);
		}else{
			doMenu(m,p);
		}
	}
}

function show_edit_screen(m,url){
	var args=arguments;
	if(args.length>1){
		doMenu(m,'edit',url);
	}else{
		doMenu(m,'search');
	}
}

function _edit_success(name){
	var args=arguments;
	if(args[1]) _remove_screen(args[1]+"_"+args[2]);
	show_screen(name,'search',{"refresh":true});
}
function _remove_screen(id){
	remove_element("screen_"+id);
	remove_element("tb_btn_"+id);
	delete top.gui.screens["screen_"+id];
}
function edit(m,id){
	var args=arguments;
	var p=(args.length>2)?args[2]:'edit';
	var params=(args.length>3)?'&'+args[3]:'';
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function view(m,id){
	var args=arguments;
	var p=(args.length>2)?args[2]:'view';
	var params=(args.length>3)?'&'+args[3]:'';
	doMenu(m,'edit','index.php?m='+m+'&p='+p+'&id='+id+params);
}

function doMenu() {
// args=module_name, page, toolbars list delimited by |, url
  hideMenu();
	if (!document.createElement) {return true};
  var url = '';
	var w=h=x=y=0;
	if(arguments[0]){
		var name=arguments[0];
		var page=(arguments[1])?arguments[1]:"search";
		var screen_name = name+"_"+page;
		var toolbar_name = name+"_tools";
		var el_id = "screen_"+screen_name;
		//if(gui.session.history.ignore) gui.session.history.ignore=false; // last link clicked was a back history item
		//else gui.session.history.add(name,page);
		hideScreens(el_id);
		if(top.gui.screens[el_id] && typeof(top.gui.screens[el_id].page)!='undefined'){
			var o=top.gui.screens[el_id].page;
			var otools=top.gui.screens[el_id].toolbars[el_id];
			if(page=='search' && o.style.display!='none')
				o.contentWindow.window.toggle_search();
			
			h=get_cats_screen_height();
			o.style.height=h;
			o.style.display='block';
			otools.style.display='block';
			if(typeof(arguments[2]) == "object"){
				if(arguments[2].refresh==true && page=='search') o.contentWindow.window.document.forms[0].elements['cats::search'].click();
			}
		}else{
			set_cats_screen_height();
			url=cats_get_path('index')+"?m="+name+"&p="+page;
			w="100%";
			h=get_cats_screen_height();
			y=CATS_SCREEN_TOP;
			x=0;
			if(check_search_module(name)){
				var a=['search'];
				var i=0;
				var d,e;
				var s = "";
				if(page!="search") a="search|edit".split("|");
				else if (typeof(top.gui.screens['screen_'+name+'_edit'])!='undefined') a.push("edit");//if(element('screen_'+name+'_edit')!=null)
				var len=a.length;
				for(i=0;i<len;i++) s+='<a id="tb_btn_'+name+'_'+a[i]+'" href="javascript:show_screen(\''+arguments[0]+'\',\''+a[i]+'\');" class="'+a[i]+'">'+a[i]+'</a>';
				/*
				if(name=='pcr_search'){
					a=['search','edit','pcr_initial_review','pcr_evaluation','pcr_area_review','pcr_pcrt_review','pcr_extension','pcr_actions','pcr_sign_off','pcr_post_audit'];
					var len=a.length;
					for(i=0;i<len;i++) s+='<a id="tb_btn_'+name+'_'+a[i]+'" href="javascript:show_screen(\''+arguments[0]+'\',\''+a[i]+'\');" class="'+a[i]+'">'+(a[i].replace(/(pcr_)/g,"").replace(/_/g," ").toPropperCase())+'</a>';
				}else{
				}*/
			}
			
			var o = new Object();
			o.page = create_iframe(el_id,url,w,h,x,y);
			o.toolbars = new Object();
			o.toolbars[el_id]=create_toolbar(el_id,s);
			top.gui.screens[el_id] = o;
		}
		
		if(top.gui.screens[el_id]) CATS_CURRENT_SCREEN=top.gui.screens[el_id].page;
		if(arguments[2]){
			if(typeof(arguments[2])!="object"){
				if(arguments[2]!='') url=arguments[2];
			}
		}
		// add history item
		if(gui.session.history.ignore) gui.session.history.ignore=false; // last link clicked was a back history item
		else// if(el_id != "screen_pcr_search_edit") 
			gui.session.history.add(name,page,url);
		if(url!=""){
			top._show_message();
			top.gui.screens[el_id].page.contentWindow.window.location.replace(url);
		}
	}
}


/**************************************
HIDE REFERENCED ELEMENTS CONTAINED
IN OBJECTS
	This is a helper function which is called
	by other methoeds/functions passing the 
	container object that holds reference/s
	to elements you wish to hide
Parameters: 
	o{object} = Object which holds reference/s to the elements
	exception{string} = exclude this element from being hidden
***************************************/
function _hide_ref_objects(o,exception){
	_show_hide_ref_objects(o,exception);
}
function _show_ref_objects(o,exception){
	_show_hide_ref_objects(o,exception,true);
}
_show_ref_object=_hide_ref_objects;
function _show_hide_ref_objects(o,exception,show){
	var i=null;
	for(i in o)
		if(exception!=o[i].id)
			o[i].style.display=show?'block':'none';
}

// hides any menu objects displayed
function hideMenu(){
	hideMenus();
}
function hideMenus(){
	if(MENUS_LOADING) return;
	var exception=(arguments[0])?arguments[0]:'';
	_hide_ref_objects(IFrameMenus,exception);
}
// hides any helper objects displayed
function hideHelpers(){
	var exception=(arguments[0])?arguments[0]:'';
	_hide_ref_objects(IFrameHelpers,exception);
}
function hideDialogs(){
	_hide_ref_objects(IFrameDialogs);
}
// hides the iframe/toolbar elements that hold the pages and page action buttons
function hideScreens(){
	var exception=(arguments[0])?arguments[0]:'';
	_hide_screens(exception)
}
function _hide_screens(exception,show){
	var e=null;
	var display = show?'block':'none';
	for(e in top.gui.screens){
		if(exception!=e){
			top.gui.screens[e].page.style.display=display;
			var i=null,x=top.gui.screens[e].toolbars;
			for(i in x) x[i].style.display=display;
		}
	}
}


var popup=null;
window.onload = function(){
	doMenu('index','dashboard','<?PHP echo $default_page_url;?>');
	
	var o=cats_set_dialog_pos();
	w=document.body.offsetWidth;
	x=0;
	// load the session dialog
	IFrameDialogs[IFRAME_SESSION]=create_iframe(SESSION_DIALOG_ID,'index.php?m=login&p=dialog', 500, 200, o.x, o.y, SESSION_DIALOG_ZINDEX, false);
	IFrameDialogs[IFRAME_MESSAGE]=create_iframe(MESSAGE_DIALOG_ID,'index.php?m=index&p=dialog', 500, 200, o.x, o.y, MESSAGE_DIALOG_ZINDEX, false);
	IFrameDialogs[IFRAME_ALERT]=create_iframe(ALERT_DIALOG_ID,'index.php?m=index&p=message', 500, 200, o.x, o.y, MESSAGE_DIALOG_ZINDEX+1, false);
	IFrameDialogs[IFRAME_LOADER]=create_iframe(LOADER_DIALOG_ID,'about:blank', w, o.h, x, o.y, LOADER_DIALOG_ZINDEX, false);
	IFrameDialogs[IFRAME_LOADER].className = "trans";
	popup = new _alert(IFRAME_ALERT);
	init_document_handlers(false);
	doresize();
	show_top_header();
}
window.onresize = doresize;
function doresize(){
	set_cats_screen_height();
	var h=get_cats_screen_height();
	try{
		if(CATS_CURRENT_SCREEN!=null) CATS_CURRENT_SCREEN.style.height=h;
		if(IFrameDialogs[IFRAME_LOADER]){
			var o=element('loading_message');
			var f=IFrameDialogs[IFRAME_LOADER];
			f.style.top = o.style.top = CATS_SCREEN_TOP;
			f.style.height = o.style.height = h;
		}
	}catch(e){}
}
function show_top_header(){
	element("top_logo").style.display = 'block';
	element("top_personal").style.display = 'block';
	element("page_title").style.visibilty = 'block';
}
function _alert(frame){
	this.fra = IFrameDialogs[frame];
	this.win = this.fra.contentWindow.window;
	this.width = 500;
	this.height = 200;
	this.x = 100;
	this.y = 100;
	
	this.show = function(s){
		var d=document.body;
		this.height = this.win.get_height();
		this.x=(d.offsetWidth/2)-(this.width/2);
		this.fra.style.left = this.x;
		this.fra.style.width = this.width;
		this.fra.style.height = this.height; 
		this.message(s);
		this.fra.style.display = 'block';
	}
	this.message = function(s){
		this.win.element('loading_message_msg').innerHTML = (s!='')?s:'Loading...';
	}
	this.hide = function(){
		this.win.element('loading_message_msg').innerHTML = 'Loading...';
		this.fra.style.display = 'none';
	}
	return this;
}


function _show_message(s){
//return;
	if(IFrameDialogs[IFRAME_LOADER]){
		IFrameDialogs[IFRAME_LOADER].style.display = 'block';
	}
	var o=element('loading_message');
	o.style.visibility = '';
	if(s){
		d=element('loading_message_msg');
		d.innerHTML = s;
	}
}
function _hide_message(){
	element('loading_message_msg').innerHTML = 'Loading...';
	element('loading_message').style.visibility='hidden';
	if(IFrameDialogs[IFRAME_LOADER]){
		IFrameDialogs[IFRAME_LOADER].style.display = 'none';
	}
}

function loaded(){
	_hide_message();
}

function loading(){
	var o=IFrameDialogs[MESSAGE_DIALOG_ID];
	var win=o.contentWindow.window;
	var doc=win.document;
	doc.getElementById('dialog_title').innerHTML="Loading...";
	doc.getElementById('dialog_message').innerHTML="Please wait while document is loading"
	o.style.display = 'block';
}

function _account(){
	doMenu('employees','edit','index.php?m=employees&p=edit&id=<?php echo $_SESSION['user_details']['user_id'];?>');
}

onunload = function(){
	if(window.opener){
		window.opener.location.href="index.php?m=login&p=logout";
		try{
			if(gui.session.close_parent){
				window.opener.close();
			}else{
				window.opener.focus();
			}
		}catch(e){
			//window.opener.setFocus();
		}
	}
}
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="top" scroll="no">
<?php include "includes/loading_message.inc.php"; ?>
<div id="top_personal" class="bar">
<a
	title="Logout" 
	class="top_right"
	href="javascript:window.close();" 
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_close_bg.gif);"></a>
<a
	title="My Employee Details" 
	class="top_right"
	href="javascript:_account();" 
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_my_employee_details_bg.gif);"></a>
<a
	title="My History" 
	class="top_right"
	href="javascript:gui.session.history.display();" 
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_results_bg.gif);"></a>
<a
	href="javascript:_hide_message();" 
	class="top_right" 
	style="background-image: url(<?PHP echo WS_ICONS_PATH;?>btn_corner_bg.gif);"></a>
</div>
<div id="top_logo" style="display:none;">
<a href="javascript:doMenu('index','dashboard');" class="cats_home"></a>
<?php
$NO_CONFIG=true;
$IFRAMES="";
require_once(CATS_REMOTE_PATH . 'menu.php');
print_menu(0, '');
$title_link = "{};";
if(cats_user_is_root_administrator()) $title_link = "show_screen('form','search');";
?>
<a href="javascript:<?php echo $title_link;?>" name="title" style="border: none;
	width:auto;
	padding: 15px;
	font: bold 17px Verdana;" id="page_title">Welcome to CATS</a>
</div>
</div>