<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
$db->debug=false;
if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:0;

$ret = false;

switch($action){
	case "add": // Add record
		$editor = new Editor($m,$p,$a);
		// manipulate site access
		if(isset($_POST['SITE_ACCESS'])){
			if(is_array($_POST['SITE_ACCESS']))
				$_POST['SITE_ACCESS'] = implode(",",$_POST['SITE_ACCESS']);
		}else{
			$_POST['SITE_ACCESS'] = 'NULL';
		}
		
		// manipulate group mask
		if(isset($_POST['GROUP_MASK'])){
			$arr_list = (!is_array($_POST['GROUP_MASK']))?array($_POST['GROUP_MASK']):$_POST['GROUP_MASK'];
			$_POST['GROUP_MASK'] = main_arr2mask($arr_list);
		}
		
		// get encrypted password
		if(isset($_POST['PASSWORD'])){
			$pwd=$_POST['PASSWORD'];
			$_POST['PASSWORD'] = db_get_one("SELECT Encrypt_password('$pwd') AS NewPassword FROM dual");
		}
		
		// get Employee Number
		if(isset($_POST['EMPLOYEE_NUMBER']) && ($_POST['EMPLOYEE_NUMBER'] == 0)){
			$_POST['EMPLOYEE_NUMBER'] = db_get_one("SELECT catsdba.tblemployee_details_seq.nextval AS EmployeeNumber FROM dual");
		}
		
		// $db->debug=true;
		$ret = $editor->insert();
		$id=$db->Insert_ID("tblemployee_details_seq");
		
		// set users application configuration details
		set_user_configuration_details($id);
		
		break;
	case "edit": // Update node
		$editor = new Editor($m,$p,$a);
		
		// manipulate site access
		if(isset($_POST['SITE_ACCESS'])){
			if(is_array($_POST['SITE_ACCESS']))
				$_POST['SITE_ACCESS'] = implode(",",$_POST['SITE_ACCESS']);
		}else{
			$_POST['SITE_ACCESS'] = 'NULL';
		}
		// manipulate group mask
		if(isset($_POST['GROUP_MASK'])){
			$arr_list = (!is_array($_POST['GROUP_MASK']))?array($_POST['GROUP_MASK']):$_POST['GROUP_MASK'];
			$_POST['GROUP_MASK'] = main_arr2mask($arr_list);
		}
		
		// get encrypted password
		if(isset($_POST['PASSWORD_OLD']) && isset($_POST['PASSWORD']) &&
				$_POST['PASSWORD_OLD']!=$_POST['PASSWORD']){
			$pwd=$_POST['PASSWORD'];
			$_POST['PASSWORD'] = db_get_one("SELECT Encrypt_password('$pwd') AS NewPassword FROM dual");
		}
		
		$ret = ($editor->update($id) > 0);

		// set users application configuration details
		$ret = set_user_configuration_details($id);

		$ret = true;		
		
		//exit;
		break;
	case "del":
		$editor = new Editor($m,$p,$a);
		$ret = $editor->delete($id);
		break;
	case CATS_FORM_ACTION_SELECT:
		switch($_POST[CATS_FORM_ACTION_SELECT.'_act']){
			case 'del':
				// get the checkbox array
				$to_delete = $_POST[CATS_FORM_ACTION_CHECKBOX_NAME];
				if(is_array($to_delete)){ // lets make we have an array
					// make the array for binding to our compiled delete statement
					foreach($to_delete as $val){
						$delete[] = array($val); // array must be in this format to bind to our compiled statement
					}
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_delete($TABLES['table'],$TABLES['id'],$delete)){
						$_SESSION['messageStack']->add("Records deleted successfully.","success");
					}
				}else{
					// let me know how you got here if it ever happens
					$_SESSION['messageStack']->add("Could not identify the form values posted.");
				}
				// these must be set to resize iframe and return the user to the last page
				$results_iframe_target = "screen_{$m}_results";
				$return_js_function = "location.href='{$_SERVER['HTTP_REFERER']}'";
				break;
			default:
				// print out some garble because i don't know what else to say
				$_SESSION['messageStack']->add("The Operation failed to fall in the expected category.");	
				break;
		}
		break;
	default:
		// print out some garble because i don't know what else to say
		$_SESSION['messageStack']->add("The Operation failed to fall in the expected category...");	
		$ret = false;
		break;
}

if($ret) {
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}


function set_user_configuration_details($id){
	global $TABLES;
	$ret = false;

	//add dev
	// $mappedFields = array();
	// if(isset($TABLES['CONFIGURATION']['fields'])){
	// 	$flds = explode(",",$TABLES['CONFIGURATION']['fields']);
	// 	for ($i = 0; $i < count($flds); $i++) {
	// 		array_push($mappedFields, strtolower($flds[$i])) ;
	// 	}
	// }
	//end dev
	if(isset($_POST['CONFIGURATION_KEY']) && $_POST['CONFIGURATION_KEY']!=''){
		$keys = $_POST['CONFIGURATION_KEY'];
		$values = $_POST['CONFIGURATION_VALUE'];
		if(is_array($keys)){ // lets make we have an array
			// delete the records and re-insert with new values
			$ret = db_query("delete from configuration where employee_number = $id ");
			$add=array();//add dev
			// make the array for binding to our compiled delete statement
			for($i=0; $i < sizeof($keys); $i++){
				if(!empty($keys[$i])){
					$add[] = array( $keys[$i], $values[$i], $id ); //comment dev // array must be in this format to bind to our compiled statement
					// $mappedValues = array();
					// $helparr= array($keys[$i], $values[$i], $id);
					// if(!empty($mappedFields)){
					// 	for ($i = 0; $i < count($mappedFields); $i++) {
					// 		$mappedValues[strtolower($mappedFields[$i])] =  $helparr[$i];
							
					// 	}
					// }
					// array_push($add, $mappedValues);
				}
			}
			// do the bulk delete on the main table using the id as the delete criteria
			//print_r($TABLES['CONFIGURATION']);
			if($ret = db_bulk_insert($TABLES['CONFIGURATION']['table'],$TABLES['CONFIGURATION']['fields'],$add)){//comment dev
			// if($ret = db_bulk_insert_dev($TABLES['CONFIGURATION']['table'],$TABLES['CONFIGURATION']['fields'],$add)){
				$_SESSION['messageStack']->add("Users Application Settings updated successfully.","success");
			}
		}else{
			// let me know how you got here if it ever happens
			$_SESSION['messageStack']->add("Problems updating application details.");
		}
	}
	return $ret;
}
// house cleaning to be done
$db=db_close();
?>