<?php

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit Employee Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Employee Details',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Employee Register',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLEMPLOYEE_DETAILS', 
					'id'=>'EMPLOYEE_NUMBER',
					'view'=>'VIEW_EMPLOYEE_DETAILS',
					'CONFIGURATION'=>array('table'=>'CONFIGURATION', 'fields'=>'CONFIGURATION_KEY,CONFIGURATION_VALUE,EMPLOYEE_NUMBER', 'id'=>'EMPLOYEE_NUMBER')
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'table'=>'TBLEMPLOYEE_DETAILS', 
					'id'=>'EMPLOYEE_NUMBER',
					'view'=>'VIEW_EMPLOYEE_DETAILS',
					'filter'=>' 1=2 ',
					'CONFIGURATION'=>array('table'=>'CONFIGURATION', 'fields'=>'CONFIGURATION_KEY,CONFIGURATION_VALUE,EMPLOYEE_NUMBER', 'id'=>'EMPLOYEE_NUMBER')
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'TBLEMPLOYEE_DETAILS', 
					'id'=>'EMPLOYEE_NUMBER',
					'view'=>'VIEW_EMPLOYEE_DETAILS',
					'filter'=>' 1=2 '
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
*/
function getPropertiesFields($page='search'){	
	$ret = array();
	switch($page){
		case 'new':
			$ret = array_merge($ret, 
				array(
					'TYPE_OF_EMPLOYEE' => array('label'=>'Type Of Employee', 'value'=>'', 'func'=>'html_draw_employee_type_radioset', 'params'=>' onclick=\"contractor(this);\" ', 'col'=>1, 'col_span'=>1),
					'EMPLOYEE_NUMBER' => array('label'=>'Induction Number', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>2), 
					'FIRST_NAME' => array('label'=>'Given Name', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 
					'LAST_NAME' => array('label'=>'Surname', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1), 
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
					'COMPANY_DD' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_form_draw_company_filter_dd', 'params'=>' onchange=\"change_company(this,this.form.COMPANYID);\" ', 'col'=>1, 'col_span'=>1), 
					'COMPANYID' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
					'MIMS' => array('label'=>'MIMS', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0), 
					'POSITION' => array('label'=>'Position', 'value'=>'', 'func'=>'html_form_draw_employee_position_dd', 'col'=>2, 'col_span'=>1), 
					'SITE_ACCESS' => array('label'=>'Other Sites This User Can View', 'value'=>'', 'func'=>'html_draw_sites_list', 'col'=>1, 'col_span'=>2), 
					'INDUCTION_DATE' => array('label'=>'Induction Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'EMAIL_ADDRESS' => array('label'=>'Email Address', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'TERMINATION_DATE' => array('group_mask'=>128,'label'=>'Termination Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'ACCESS_ID' => array('label'=>'Login', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PASSWORD' => array('label'=>'Password', 'value'=>'', 'func'=>'html_draw_password_field', 'col'=>1, 'col_span'=>2), 
					//'GROUP_NAME' => array('label'=>'Group Name', 'value'=>'', 'func'=>'html_form_draw_group_name_dd', 'col'=>1, 'col_span'=>2), 
					'GROUP_MASK' => array('label'=>'Group Name', 'value'=>'', 'func'=>'html_user_groups_list', 'col'=>1, 'col_span'=>2), 
					//'CAN_CHANGE_DUE_DATE' => array('label'=>'Can Change Due Date', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					//'INCIDENT_PRIVILEGED_USER' => array('label'=>'Incident Privileged User', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					//'ACTION_PRIVILEGED_USER' => array('label'=>'Action Privileged User', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					'USER_STATUS' => array('label'=>'User Status', 'value'=>'Active', 'func'=>'html_draw_employee_status_radioset', 'col'=>1, 'col_span'=>2, 'group_mask'=>9965568),
					'SUPERINTENDENT' => array('label'=>'PCR Superintendent', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),
					'ENGINEER' => array('label'=>'PCR Engineer', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),
					 'ADD_EMPLOYEE' => array('label'=>'Allow to add Employee', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),//--------------allow to add employee------------///---------------
				    'RELATED_SUPERINTENDENT' => array('label'=>'Superintendent for Employee', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2) //--------------------------related super intendent-----------------
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'EMPLOYEE_NUMBER' => array('label'=>'Employee Number', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					//'INDUCTION_NUMBER' => array('label'=>'Induction Number', 'value'=>NULL, 'func'=>'html_draw_number_field', 'col'=>1, 'col_span'=>1), 
					'TYPE_OF_EMPLOYEE' => array('label'=>'Type Of Employee', 'value'=>'', 'func'=>'html_draw_employee_type_radioset', 'col'=>2, 'col_span'=>1), 
					'FIRST_NAME' => array('label'=>'Given Name', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 
					'LAST_NAME' => array('label'=>'Surname', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1), 
					'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1), 
					'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
					'COMPANY_DD' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_form_draw_company_filter_dd', 'params'=>' onchange=\"change_company(this,this.form.COMPANYID);\" ', 'col'=>1, 'col_span'=>1), 
					'COMPANYID' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0), 
					'MIMS' => array('label'=>'MIMS', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0), 
					'POSITION' => array('label'=>'Position', 'value'=>'', 'func'=>'html_form_draw_employee_position_dd', 'col'=>2, 'col_span'=>1), 
					'SITE_ACCESS' => array('label'=>'Other Sites This User Can View', 'value'=>'', 'func'=>'html_draw_sites_list', 'col'=>1, 'col_span'=>2), 
					'INDUCTION_DATE' => array('label'=>'Induction Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'EMAIL_ADDRESS' => array('label'=>'Email Address', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'TERMINATION_DATE' => array('group_mask'=>'MASK_ADMINISTRATOR','label'=>'Termination Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
					'ACCESS_ID' => array('label'=>'Login', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
					'PASSWORD' => array('label'=>'Password', 'value'=>'', 'func'=>'html_draw_password_field', 'col'=>1, 'col_span'=>2), 
					//'GROUP_NAME' => array('label'=>'Group Name', 'value'=>'', 'func'=>'html_form_draw_group_name_dd', 'col'=>1, 'col_span'=>2), 
					'GROUP_MASK' => array('label'=>'User Groups', 'value'=>'', 'func'=>'html_user_groups_list', 'col'=>1, 'col_span'=>2), 
					//'CAN_CHANGE_DUE_DATE' => array('label'=>'Can Change Due Date', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					//'INCIDENT_PRIVILEGED_USER' => array('label'=>'Incident Privileged User', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					//'ACTION_PRIVILEGED_USER' => array('label'=>'Action Privileged User', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					'USER_STATUS' => array('label'=>'User Status', 'value'=>'Active', 'func'=>'html_draw_employee_status_radioset', 'col'=>1, 'col_span'=>2, 'group_mask'=>9965568),
					'SUPERINTENDENT' => array('label'=>'PCR Superintendent', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),
					'ENGINEER' => array('label'=>'PCR Engineer', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),
					 'ADD_EMPLOYEE' => array('label'=>'Allow to add Employee', 'value'=>NULL, 'func'=>'html_draw_yes_no_radioset', 'col'=>1, 'col_span'=>2),//--------------allow to add employee------------///---------------
				    'RELATED_SUPERINTENDENT' => array('label'=>'Superintendent for Employee', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2) //------------------------------------related super intendant
 
				)
			);
			break;
		case 'view':
			$ret = array_merge($ret, 
				array(
					'EMPLOYEE_NUMBER' => array('label'=>'Employee Number', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					//'INDUCTION_NUMBER' => array('label'=>'Induction Number', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'TYPE_OF_EMPLOYEE' => array('label'=>'Type Of Employee', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'FIRST_NAME' => array('label'=>'Given Name', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'LAST_NAME' => array('label'=>'Surname', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
					'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=> '"SECTION_ID"', 'col'=>1, 'col_span'=>1), 
					'SECTION_DESCRIPTION' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					//'COMPANY_DD' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>' onchange=\"change_company(this,this.form.COMPANYID);\" ', 'col'=>1, 'col_span'=>1), 
					'COMPANYNAME' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'POSITION_NAME' => array('label'=>'Position', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1), 
					'SITE_ACCESS' => array('label'=>'Other Sites This User Can View', 'value'=>'', 'func'=>'html_display_site_access_value', 'col'=>1, 'col_span'=>2), 
					'INDUCTION_DATE' => array('label'=>'Induction Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'EMAIL_ADDRESS' => array('label'=>'Email Address', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'TERMINATION_DATE' => array('group_mask'=>MASK_ADMINISTRATOR,'label'=>'Termination Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					'ACCESS_ID' => array('label'=>'Login', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					//'PASSWORD' => array('label'=>'Password', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
					//'GROUP_NAME' => array('label'=>'Group Name', 'value'=>'', 'func'=>'html_form_draw_group_name_dd', 'col'=>1, 'col_span'=>2), 
					'GROUP_MASK' => array('label'=>'User Groups', 'value'=>'', 'func'=>'html_display_user_groups_value', 'col'=>1, 'col_span'=>2), 
					//'CAN_CHANGE_DUE_DATE' => array('label'=>'Can Change Due Date', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					//'INCIDENT_PRIVILEGED_USER' => array('label'=>'Incident Privileged User', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					//'ACTION_PRIVILEGED_USER' => array('label'=>'Action Privileged User', 'value'=>0, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2), 
					'USER_STATUS' => array('label'=>'User Status', 'value'=>'Active', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'SUPERINTENDENT' => array('label'=>'PCR Superintendent', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ENGINEER' => array('label'=>'PCR Engineer', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
					'ADD_EMPLOYEE' => array('label'=>'Allow to add Employee', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),//--------------allow to add employee------------///---------------
				    'RELATED_SUPERINTENDENT' => array('label'=>'Superintendent for Employee', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2) //--------------------------------------related super intendent-----------
				)
			);
			break;
		case 'search':
		case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=> '"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_form_draw_department_dd_linked', 'params2'=> '"SECTION_ID"', 'col'=>2, 'col_span'=>1), 
				'SECTION_ID' => array('label'=>'Section', 'value'=>NULL, 'func'=>'html_form_draw_section_dd', 'col'=>1, 'col_span'=>2),
				'COMPANYID' => array('label'=>'Company', 'value'=>NULL, 'func'=>'html_form_draw_company_dd', 'col'=>1, 'col_span'=>2), 
				'TYPE_OF_EMPLOYEE' => array('label'=>'Type Of Employee', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_employee_type_dd', 'col'=>1, 'col_span'=>1), 
				'EMP_NAME' => array('label'=>'Employee Name', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1), 
				'FIRST_NAME' => array('label'=>'Employee Given Name', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>1), 
				'LAST_NAME' => array('label'=>'Employee Surname', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>NULL, 'func'=>'html_draw_input_field', 'col'=>2, 'col_span'=>1), 
				'EMPLOYEE_NUMBER' => array('label'=>'Induction Number', 'value'=>NULL, 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2), 
				'INDUCTION_DATE_FROM' => array('label'=>'Induction Date From', 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'INDUCTION_DATE_TO' => array('label'=>'Induction Date To', 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1), 
				'GROUP_MASK' => array('label'=>'Group Name', 'value'=>'', 'operator'=>'MASK', 'func'=>'html_user_groups_list', 'col'=>1, 'col_span'=>2), 
				'USER_STATUS' => array('label'=>'User Status', 'delim'=>"'", 'value'=>'', 'func'=>'html_get_employee_status_dd', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}
if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>