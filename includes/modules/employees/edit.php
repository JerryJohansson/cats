<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');

$canUserAddUser = $_SESSION['user_details']['add_employee'];

 
// If user is not an administrator or UserCanAdd==Yes, only View Allowed
if(!cats_user_is_administrator())
{
	if($canUserAddUser == 'Yes')
	{
		// Allow
	}
	else
	{
		$p = 'view';
	}
}
 
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));

$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:0;
$pid=isset($_REQUEST['pid'])?$_REQUEST['pid']:0;
$iTabs=0;
$iTabButtons=0;
$RS=array();
$RECORD=false;


if($id>0){
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	

	if(!($page_access = $RECORD['EMPLOYEE_NUMBER']==$_SESSION['user_details']['user_id'])){
		$page_access = (cats_user_is_administrator() || ($canUserAddUser == 'Yes'));
	}
	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this employees details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
	$can_delete = (db_get_one("select count(*) as cnt from tblaction_details where managed_by_id = $id or allocated_to_id = $id")==0);
	//print_r($RS[$id]);
}else{ // must be a new employee since there is no ID
	// add stuff for new item
	$RECORD=array(
		'ACCESS_ID'=>'',
		'PASSWORD'=>'',
		'LAST_NAME'=>'',
		'FIRST_NAME'=>'',
		'EMPLOYEE_NUMBER'=>''
	);
}
$RS = array();//add dev for load formdata
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
/***********************
 start up script for each page
***********************/
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// create document.mousedown handlers
	init_document_handlers();
	// resize editing section
	init_resize_editor();
	
	<?php 
	if($p!='view') echo 'set_company_id();'
	?>
	
	return true;
}
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
window.onload = init;
window.onresize = init_resize_editor;

	function preSubmitForm()
	{
		if($("#INDUCTION_DATE_d").val() != "")
			setHiddenDate("INDUCTION_DATE");
		if($("#TERMINATION_DATE_d").val() != "")
			setHiddenDate("TERMINATION_DATE");
	}
	
//=====================================================================================================================
//
//=====================================================================================================================

function set_company_id(){
	var f=document.forms.module;
	var value=f.elements['COMPANYID'].value;
	var itm=f.elements['COMPANY_DD'];
	itm.value = value;
}
//check that the login and password make a unique combination that no other user has
function check_login_new(){
	//top.popup.show("Please wait while checking login details...");
	//check the login combination doesn't exist
	var f=document.forms.module;
	var els=f.elements;
	var login = els["ACCESS_ID"];
	var pwd = els["PASSWORD"];
	var obj;
	
	if(login.value != els['ACCESS_ID_OLD'].value || pwd.value != els['PASSWORD_OLD'].value){
		if(login.value != "" && pwd.value != ""){
			//top.popup.message("Checking login information...");
			obj={
				'a':'check_login',
				'uid':login.value,
				'pwd':pwd.value
			};
			if(remote.get_sync("users", obj)){
				//the combination is already being used so we enforce the validation
				els['valLogin'].value = "";
			}else{
				els['valLogin'].value = "OK";
			}
		}
	}else{
		els['valLogin'].value = "OK";
	}
	 
	//now check that the name combination doesn't exist
	var fname = els['FIRST_NAME'];
	var lname = els['LAST_NAME'];
	if(fname.value != els['FIRST_NAME_OLD'].value || lname.value != els['LAST_NAME_OLD'].value){
		if(fname.value != "" && lname.value != ""){
			//top.popup.message("Checking employee name...");
			obj={
				'a':'check_employee_name',
				'fname':fname.value,
				'lname':lname.value
			};
			if(remote.get_sync("users", obj)){
				//the combination is already being used so we enforce the validation
				els['valNames'].value = "";
			}else{
				els['valNames'].value = "OK";
			}
		}
	}else{
		els['valNames'].value = "OK";
	}
	
	//now check that the employee number doesn't exist
	var emp_id = els['EMPLOYEE_NUMBER'];
	if(emp_id.value != els['EMPLOYEE_NUMBER_OLD'].value){
		if(emp_id.value != ""){
			//top.popup.message("Checking induction number...");
			obj={
				'a':'check_employee_number',
				'enum':emp_id.value
			};
			if(remote.get_sync("users", obj)){
				//the combination is already being used so we enforce the validation
				els['valEmp'].value = "";
			}else{
				els['valEmp'].value = "OK";
			}
		}
	}else{
		els['valEmp'].value = "OK";
	}
	//top.popup.hide();
}
//check that the login and password make a unique combination that no other user has
function check_login_edit(){
	//top.popup.show("Please wait while checking login details...");
	//check the login combination doesn't exist
	var f=document.forms.module;
	var els=f.elements;
	var login = els["ACCESS_ID"];
	var pwd = els["PASSWORD"];
	var obj;
	
	if(login.value != els['ACCESS_ID_OLD'].value || pwd.value != els['PASSWORD_OLD'].value){
		if(login.value != "" && pwd.value != ""){
			//top.popup.message("Checking login information...");
			obj={
				'a':'check_login',
				'uid':login.value,
				'pwd':pwd.value
			};
			if(remote.get_sync("users", obj)){
				//the combination is already being used so we enforce the validation
				els['valLogin'].value = "";
			}else{
				els['valLogin'].value = "OK";
			}
		}
	}else{
		els['valLogin'].value = "OK";
	}
	 
	//now check that the name combination doesn't exist
	var fname = els['FIRST_NAME'];
	var lname = els['LAST_NAME'];
	if(fname.value != els['FIRST_NAME_OLD'].value || lname.value != els['LAST_NAME_OLD'].value){
		if(fname.value != "" && lname.value != ""){
			//top.popup.message("Checking employee name...");
			obj={
				'a':'check_employee_name',
				'fname':fname.value,
				'lname':lname.value
			};
			if(remote.get_sync("users", obj)){
				//the combination is already being used so we enforce the validation
				els['valNames'].value = "";
			}else{
				els['valNames'].value = "OK";
			}
		}
	}else{
		els['valNames'].value = "OK";
	}
	//top.popup.hide();
}
//remotely go and get the companies for the chosen letter
function get_companies(itm, value){
	//only do it if something is entered in the filter box
	if(value != ""){
		// Set the MIMS Company Flag
		document.forms.module.elements['MIMS'].value = "true";
		var sLetter = value.charAt(0);
		sLetter = sLetter.toUpperCase();
		//go and get the names
		var obj={
			'a':'get_alpha_companies_array',
			'p':sLetter
		};
		var e = remote.get_sync("admin", obj);
		//alert(e.options[1]);
		var arr = e.options;
		var opt, name, val;
		var g = arr.length;
		itm.options.length = 0; // remove options
		if(g<2){//no data so we add a blank option
			opt = new Option("","");
			itm.options[0] = opt;
		}else{//only do this is we have some employees
			value = value.toLowerCase();
			var len = value.length;
			var search_data = value.substring(0,len);
			var selected = false;
			var i=0;
			for(i=0;i<g;i++){
				//add the names to the drop down box
				txt = arr[i][1];
				val = arr[i][0];
				opt = new Option(txt,val);
				itm.options[i] = opt;
				//highlight the required option
				if(selected == false){
					len = value.length;
					search_data = txt.substring(0,len).toLowerCase();
					if(value == search_data){
						itm.options[i].selected = true; 
						selected = true;
						//now update the hidden field
						document.forms.module.elements['COMPANYID'].value = val;
					}
				}
			}
		}
	}//end if(value != "")
}
//remotely go and get the companies for the chosen letter
function check_for_company(){
	var f = document.forms.module;
	var itm = f.elements['COMPANY_DD'];
	var to = f.elements['COMPANYID'];
	var x = itm.selectedIndex;
	var id = itm.options[x].value;
	var name = itm.options[x].text;
	var obj;
	//go and get the names
	if(document.forms.module.elements['MIMS'].value == "true"){
		obj={
			'a':'check_mims_companies',
			'p':name,
			'id':id
		};
	}else{
		obj={
			'a':'check_companies',
			'p':name,
			'id':id
		};
	}
	var e = remote.get_sync("admin", obj);
	if(parseInt(e.company_id) != 0){
	  to.value = e.company_id;
	}else{
		to.value = id;
	}
}
/*
+------------------------------------------------------------------------
|  BEGIN::Override default site and department remoting functions with these sync remoting functions
+------------------------------------------------------------------------
*/
function get_site_department(itm,to){
	get_remote_dd(itm, to, 'admin', 'get_departments_array');
}
function get_department_section(itm,to){
	get_remote_dd(itm, to, 'admin', 'get_sections_array');
}
function get_remote_dd(itm, to, module, action){
	var id=itm.options[itm.selectedIndex].value;
	var top_options_name = module+"__"+action;
	var option_id = top_options_name+"__"+id
	var obj={
		'a':action,
		'p':id,
		'id':id
	};
	var e = null;
	var cached = false;
	if(typeof(top.gui.options[option_id])!="object") {
		top.gui.options.add(option_id);
		e = remote.get_sync(module,obj);
	} else {
		e = new Object();
		e.options = top.gui.options[option_id]._options;
		cached = true;
	}
	var selected_value=top.gui.options[option_id].replace(cached, to, e.options);
}
function change_company(itm,to){
	to.value = itm.options[itm.selectedIndex].value;
}
<?php
if($action=='add'){
?>
function contractor(itm){
	var f=itm.form;
	if(f.TYPE_OF_EMPLOYEE[0].checked == true){//employee
		f.COMPANYID.value = 0;
	}else{//contractor
		f.POSITION.value = 10;
		f.EMAIL_ADDRESS.value = "<?php echo CATS_ADMIN_EMAIL;?>";
		var id=null;
		var o = f.elements['GROUP_MASK[]'];
		var x = o.options.length;
		for(i=0;i<x;i++){
			if(o.options[i].text == "No Access") id=i;
			o.options[i].selected = false;
		}
		if(id!=null) o.options[id].selected = true;
	}
}
var HELPER_ID='';
function _helper_show_mimms_employee(func,module){
	var a=arguments;
	var f=document.forms[0];
	var e=(f!=null)?f.elements:{}; 
	//create it once only
	HELPER_ID=func;
	if(!top.HelperObjects[func])
		top.HelperObjects[func]=new Object();
	//'INDUCTION_NUMBER','LAST_NAME','FIRST_NAME','EMPLOYEE_TYPE','SITE_ID','SITE_ID_text','POSITION','POSITION_text','ACCESS_ID','PASSWORD','EMAIL_ADDRESS'
	var i=2;
	if(a.length>(i+5)){
		top.HelperObjects[func].id=a[i++];
		top.HelperObjects[func].val=a[i++];
		top.HelperObjects[func].emp_id=e[a[i++]];
		top.HelperObjects[func].emp_lname=e[a[i++]];
		top.HelperObjects[func].emp_fname=e[a[i++]];
		top.HelperObjects[func].emp_type=e[a[i++]];
	}
	
	if(a.length>(i+1)){
		top.HelperObjects[func].site_id=e[a[i++]];
		top.HelperObjects[func].site_val=e[a[i++]];
	}
	
	if(a.length>(i+1)){
		// reset the departments dd
		if(top.HelperObjects[func].site_id.selectedIndex>0){
			top.HelperObjects[func].site_id.options[0].selected = true;
			top.HelperObjects[func].site_id.onchange();
		}
		top.HelperObjects[func].dept_id=e[a[i++]];
		top.HelperObjects[func].dept_val=e[a[i++]];
	}
	
	if(a.length>(i+1)){
		top.HelperObjects[func].pos_id=e[a[i++]];
		top.HelperObjects[func].pos_val=e[a[i++]];
	}
	
	if(a.length>(i+2)){
		top.HelperObjects[func].uid=e[a[i++]];
		top.HelperObjects[func].pwd=e[a[i++]];
		top.HelperObjects[func].email=e[a[i++]];
	}
	top.HelperObjects[func].trigger_function=set_department;
	top.helper_show_menu(func, module, null);
}
function set_department_timeout(){
	setTimeout(set_department, 500);
}
function set_department(){
	top.HelperObjects[HELPER_ID].dept_id.onchange();
}
// new validation

<?php
}
?>
function MM_validateForm() { //v4.0
  var args = new Array(MM_validateForm.arguments.length);
  for (i=0; i<args.length; i++) {args[i]=MM_validateForm.arguments[i];}
  var i,p,q,nm,test,num,min,max,errors='';
  for (i=0; i<(args.length-2); i+=3) { 
  	test=args[i+2]; val=MM_findObj(args[i]);
    if(val){ 
		nm=val.getAttribute("title"); 
		if((val=val.value)!=""){
      		if(test.indexOf('isEmail')!=-1){ 
				p=val.indexOf('@');
        		if(p<1 || p==(val.length-1)){
					errors+='- '+nm+' must contain an e-mail address.\n';
				}
      		}else if(test!='R' && test!='L' && test!='P' && test!='E'){
        		if(isNaN(val)){
					errors+='- '+nm+' must contain a number.\n';
				}
        		if(test.indexOf('inRange') != -1){ 
					p=test.indexOf(':');
         			min=test.substring(8,p); max=test.substring(p+1);
          			if(val<min || max<val){
						errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
					}
    			}
	 		} 
		}else if(test.charAt(0) == 'R'){
			errors += '- '+nm+' is required.\n';
		}else if(test.charAt(0) == 'L'){
			errors += '- The combination of Login and Password you entered is already being used.\n   Please change the Login or Password to make this a unique combination.';
		}else if(test.charAt(0) == 'P'){
			errors += '- The combination of Surname and Given Name(s) you entered is already being used.\n   Please change the Surname or Given Name(s) to make this a unique combination.\n';
		}else if(test.charAt(0) == 'E'){
			errors += '- The Induction Number you entered is already being used.\n   Please enter a different one.\n';
		}
	}//end if(val)
  }//end for (i=0; i<(args.length-2); i+=3)
  
  if (errors){
    alert('The following error(s) occurred:\n'+errors);
  }else{
    check_for_company();
  }
  document.MM_returnValue = (errors == '');
}
function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" <?php 
if($action=='edit'){ // validation is the same for now
	?>onSubmit="check_login_edit(); MM_validateForm('LAST_NAME','','R','FIRST_NAME','','R','valNames','','P','SITE_ID','','R','DEPARTMENT_ID','','R','POSITION','','R','INDUCTION_DATE','','R','EMAIL_ADDRESS','','NisEmail','ACCESS_ID','','R','PASSWORD','','R','GROUP_MASK','','R','valLogin','','L','valEmp','','E'); return document.MM_returnValue"<?php
}else{
	?>onSubmit="check_login_new(); MM_validateForm('LAST_NAME','','R','FIRST_NAME','','R','valNames','','P','SITE_ID','','R','DEPARTMENT_ID','','R','POSITION','','R','INDUCTION_DATE','','R','EMAIL_ADDRESS','','NisEmail','ACCESS_ID','','R','PASSWORD','','R','GROUP_MASK','','R','valLogin','','L','valEmp','','E'); return document.MM_returnValue"<?php
}
?>>
<?php
// set action(a) parameter so we don't
// set hidden variables used when sending notification email
echo html_draw_hidden_field('ACCESS_ID_OLD',$RECORD['ACCESS_ID']);
echo html_draw_hidden_field('PASSWORD_OLD',$RECORD['PASSWORD']);
echo html_draw_hidden_field('LAST_NAME_OLD',$RECORD['LAST_NAME']);
echo html_draw_hidden_field('FIRST_NAME_OLD',$RECORD['FIRST_NAME']);
echo html_draw_hidden_field('EMPLOYEE_NUMBER_OLD',$RECORD['EMPLOYEE_NUMBER']);
echo html_draw_hidden_field('valNames','OK');
echo html_draw_hidden_field('valLogin','OK');
echo html_draw_hidden_field('valEmp','OK');
?>
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<?php
if($p=='edit'){
	if((is_array($RECORD) && $RECORD['EMPLOYEE_NUMBER']==$_SESSION['user_details']['user_id']) || (cats_user_is_super_administrator())){
?>
<a
	title="Edit Application Settings for this employee"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);">Application Settings</a>
<?php
	}
}
?>
</fieldset>
<fieldset class="tool_bar">
<?php
if($p=="edit"){
?>
<a id="linkAddNewUser"
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php }elseif(cats_user_is_administrator()){ ?>
<a
	title="Edit"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit</a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php
if($p=="edit"){
	if(cats_user_is_administrator() && $can_delete){
?>
<a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<?php
	}
}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<!-- Edit Main Fields test -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed=new Editor($m,$p,$a);

if($id>0){
	//echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]);//comment dev
	echo($ed->buildForm($FIELDS, $TABLES['view'], $TABLES['id'], $id, isset($RS[$id])?$RS[$id]:false));
}else{
	echo($ed->buildForm($FIELDS));
}
?>
	<fieldset class="tbar" style="text-align:right; ">
<?php //if($p=='new') echo(html_filters_templates_dd());?>
<?php if($p!="view"){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()">
<?php }elseif(cats_user_is_administrator()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
		<?php if($p=='edit'){ ?>
		<input type="button" id="btnAddNewEmployeeOrContractor" class="button" name="cats::New" value="New Employee or Contractor" onClick="_m.newModule();" >
		<?php } ?>
		<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
	</fieldset>
</fieldset>

<?php
if((is_array($RECORD) && $RECORD['EMPLOYEE_NUMBER']==$_SESSION['user_details']['user_id']) || (cats_user_is_super_administrator())){
?>
<!-- BEGIN:: Settings/Details -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Employee Application Settings</a></caption>
<tr>
	<td class="info">
	</td>
	<td>
		Application configuration settings specific to an employee. These settings control the way the pages are displayed, the graphical style of the site etc...
		<br>The settings available and their associated values are as follows:
		<table width="100%">
		<tr>
			<th>Chosen Key</th><th>Expected Value</th>
		</tr>
		<tr>
			<td>Number of search results to display</td><td>A number less than 1000</td>
		</tr>
		<tr bgcolor="#eeeeee">
			<td>Re-enter login automatically into login screen when you logout</td><td>true or false</td>
		</tr>
		<tr>
			<td>Default skin and styles to use</td><td>cats or default (more can be added upon request)</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">

<?php
//$ed=new Editor($m,$p,$a);

$seq_fields = array(
	'CONFIGURATION_KEY' => array('label'=>'Key', 'value'=>NULL, 'func'=>'html_draw_configuration_keys_dd', 'params'=>' onchange="get_config_value_control(this);" ', 'col'=>0, 'col_span'=>0), 
	'CONFIGURATION_VALUE' => array('label'=>'Value', 'value'=>NULL, 'func'=>'html_config_loader', 'col'=>0, 'col_span'=>0)
);

$edit_button = "delete:javascript:remove_sequence_row(this, ::)";
$details_sql="select configuration_key, configuration_value, '$edit_button' from configuration where employee_number = ".$RECORD['EMPLOYEE_NUMBER'];
//echo (html_get_sequential_control('config', '', $details_sql));
echo ($ed->buildSequentialForm('Configuration', $seq_fields, $details_sql));

?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>

<!-- END:: Settings/Details -->
<?php
}
?>
</div>
</form>


<script language="javascript">

	$(document).ready(function() {
		$( ".button" ).addClass("btn btn-default btn-sm active");
		$( ".button" ).removeClass("button");
		
		$( ".submit" ).addClass("btn btn-primary btn-sm active");
		$( ".submit" ).removeClass("submit");

		$( ".reset" ).addClass("btn btn-warning btn-sm active");
		$( ".reset" ).removeClass("reset");	

		if('<?PHP echo $canUserAddUser; ?>' != 'Yes')
		{
			$("#btnAddNewEmployeeOrContractor").hide();
			$("#linkAddNewUser").hide();
			
			
		}
	});		
		
</script>

<script language="javascript">



</script>


