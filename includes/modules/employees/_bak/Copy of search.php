<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));


$where = "";
$multi_sites = $_SESSION['user_details']['site_access'];
$users_site = $_SESSION['user_Details']['site_id'];
if(is_array($_SESSION['user_details']) && $multi_sites!=''){
	$where = $multi_sites;
}else{
	$where = $users_site;
}

// create the Select String
$sql = "SELECT Employee_Number, Emp_Name, Site_Description, Department_Description, Section_Description, CompanyName, Induction_Date, 'edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:{$TABLES['id']}:\")' as edit FROM " . $TABLES['view'] . " ";

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST)>0){
	$_SESSION[$m.'_'.$p] = $_POST;
}else if(isset($_SESSION[$m.'_'.$p])){
	$_POST=$_SESSION[$m.'_'.$p];
}
if(isset($_POST['cats::search'])){
	$columns=explode("|","Site_Id|none,none,NULL|Department_Id|none,none,NULL|CompanyId|none,none,NULL|Employee_Number|none,none,NULL|Induction_Date_From|',none,NULL,>=|Induction_Date_to|',none,NULL,<=|Type_Of_Employee|',none,''|Employee_Number|none,none,NULL|Group_Name|',none,''|Section_Id|none,none,NULL");
	$fields=array();
	$order_by_col = "Emp_Name ASC";
	//$sql = "";
	$sql_where = "";
	$sql_order_by = "";
	// create the MM_FormFields and MM_TableFields arrays
	$x=count($columns);
	// Create Where String and append it to the Select String
	for ($i=0; $i < $x; $i+=2) {
		$types = explode(",",$columns[$i+1]);
		$delim =  ($types[0] != "none") ? $types[0] : "";
		$operator = ($types[3] == ">=" || $types[3] == "<=") ? $types[3] : " = ";
		$column_name=strtoupper($columns[$i]);
		if($column_name == "SITE_ID"){//add the users sites to the where string
			if(!isset($column_name)){
				if($sql_where == "") { 
					$sql_where = " WHERE " . $where;
				}else{
					$sql_where .= " AND " . $where;
				}
			}
		}//end if "site_id")
		
		switch($column_name){
			case 'INDUCTION_DATE_FROM': //case 'CLOSING_DATE_FROM':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-5);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			case 'INDUCTION_DATE_TO': //case 'CLOSING_DATE_TO':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-3);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			default:
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					if($operator=="LIKE"){
						$sql_where .=  $column_name . $operator . "'%" . $_POST[$column_name] . "%'"; 
					}else{
						$sql_where .=  $column_name . $operator . $delim . $_POST[$column_name] . $delim; 
					}
				}
				break;
		}
	}
	$search_sql = $sql . $sql_where;
	
	$search_display='none';
	$results_display='block';
	$show_search_results = true;

}else{
	$search_display='block';
	$results_display='none';
	$search_sql = $sql . " WHERE " . $TABLES['filter'];
	$show_search_results = false;
}
$db->debug=true;
if($db->debug==true){
	print_r($_POST);
	echo('<br>'.$search_sql);
}
?>
<link rel="stylesheet" href="<?php echo WS_STYLE_PATH;?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
var FORM_TARGET="<?php echo $results_iframe_target;?>";
function search_click(btn){
	var search_type = "";
	if(btn.value=="Search") search_type = "submit";
	var f=btn.form;
	toggle_search();
	try{
		var rwin=element(FORM_TARGET);
		var rdoc=rwin.contentWindow.window.document;
		rdoc.body.firstChild.style.display='none';
	}catch(e){}
	top._show_message("Please wait while loading search results...");
	return true;
}
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $id;?>");

window.onload = init;
</script>
</head>
<body>
<?PHP $iTabs=0; ?>
<form action="index.php?m=<?php echo $m;?>&p=results" target="screen_<?php echo $m;?>_results" method="post">	
<table width="100%">
<tr valign="top">
	<td width="100%">		
	
		<!-- Actions Search Table -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" class="search" onclick="var e=element('search_form');e.style.display=(e.style.display=='none')?'block':'none';"><?php 
echo $GENERAL['name'];
if($show_search_results){
	echo ' (Click here to show Search Parameters)';
}
?></a></caption>
<tr>
	<td>
		<div  id="search_form" style="display:<?php echo $search_display;?>">
<?php
$ed=new Editor("edit","admin");

if($id>0){
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id));
}else{
	echo($ed->buildForm($FIELDS));
}
?>

<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="cats::search" value="Search">
<input type="button" name="cats::new" value="New Employee or Contractor" onclick="newEmployee()">
<input type="button" name="Cancel" value="Cancel" onclick="doCancel();">
</fieldset>		
</div>
	</td>
</tr>
</table>

<?php
if($show_search_results){
?>
<!-- Results Table -->
		

<?php
// Search Results
// define table headers
//$db->debug=true;
$colattribs = array(' width="40" ',' width="100" ',' width="100" ',' width="100" ',' width="100" ',' width="100" ',' width="80" ',' width="20" ');
$colarr = explode("|","Emp No.|Name|Site|Department|Section|Company|Induction Date|edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:{$TABLES['id']}:\")");
db_render_pager($search_sql,$colarr,$colattribs,$GENERAL['results']);
?>
</fieldset>
<?php
}
?>
		
	</td>
</tr>
</table>
</form>