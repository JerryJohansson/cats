<?php


/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search'){
	$ret = array();
	switch($action){
		case 'search':
		case 'results':				
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Site Specific Obligation',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Site Specific Obligation',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Site Specific Obligation',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}



/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables($page = 'search'){
	$ret = array();
	switch($page){
		case 'new': 
		case 'edit':
			$ret = array_merge(
				array(
					'table'=>'TBLOBLIGATIONS', 
					'id'=>'OBLIGATION_ID',
					'view'=> 'TBLOBLIGATIONS',
					'POSITION_ID'=> array(
						'table'=>'TBLOBLIGATIONPOSITIONS',
						'fields'=>'POSITION_ID,OBLIGATION_ID'
					)
				)
			);
			break;
		case 'printpreview':
			$ret = array_merge(
				array(
					'table'=>'VIEW_SITE_OBLIGATIONS', 
					'id'=>'OBLIGATION_ID',
					'view'=> 'VIEW_SITE_OBLIGATIONS',
					'POSITION_ID'=> array(
						'table'=>'TBLOBLIGATIONPOSITIONS',
						'fields'=>'POSITION_ID,OBLIGATION_ID'
					)
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'table'=>'VIEW_SITE_OBLIGATIONS', 
					'id'=>'OBLIGATION_ID',
					'view'=> 'VIEW_SITE_OBLIGATIONS'
				)
			);
			break;
	}
	return $ret;
}



/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/


function getPropertiesFields($page='search')
{
	$ret = array();

	switch($page)
	{
		case 'search':
		case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'SUBJECT' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_form_draw_reftable_subject_dd', 'col'=>1, 'col_span'=>2), 
				'ISSUE' => array('label'=>'Issue', 'value'=>'', 'func'=>'html_form_draw_reftable_issue_dd', 'col'=>1, 'col_span'=>2), 
				//'POSITION_ID' => array('label'=>'Responsible Position', 'value'=>'', 'func'=>'html_form_draw_employee_positions_dd', 'col'=>1, 'col_span'=>2)
				'POSITION_ID' => array('label'=>'Responsible Position', 'value'=>'', 'func'=>'html_get_obligation_responsibilities_helper', 'col'=>1, 'col_span'=>2)
			);
			break;
	    case 'new':
			$ret =  array(
				'SUBJECT' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_form_draw_reftable_subject_dd', 'col'=>1, 'col_span'=>2),
				'ISSUE' => array('label'=>'Issue', 'value'=>'', 'func'=>'html_form_draw_reftable_issue_dd', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'KEY_REQUIREMENT' => array('label'=>'Key Requirements', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'LEGISLATION1' => array('label'=>'Reference Legislation', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'ALLOCATED_TO_POSITION_ID' => array('label'=>'Allocated To (Position)', 'value'=>'', 'func'=>'html_get_obligation_responsibilities_helper', 'col'=>1, 'col_span'=>2),
				'VERIFICATION' => array('label'=>'Verification', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2)
			);
		    break;
		case 'edit':
			$ret =  array(
				'SUBJECT' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_form_draw_reftable_subject_dd', 'col'=>1, 'col_span'=>2),
				'ISSUE' => array('label'=>'Issue', 'value'=>'', 'func'=>'html_form_draw_reftable_issue_dd', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'KEY_REQUIREMENT' => array('label'=>'Key Requirements', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'LEGISLATION1' => array('label'=>'Reference Legislation', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'ALLOCATED_TO_POSITION_ID' => array('label'=>'Allocated To (Position)', 'value'=>'', 'func'=>'html_get_obligation_responsibilities_helper', 'col'=>1, 'col_span'=>2),
				'VERIFICATION' => array('label'=>'Verification', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2)
			);
		  break;
		case 'view':
		case 'printpreview':
			$ret =  array(
				'SUBJECTSTRING' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'ISSUESTRING' => array('label'=>'Issue', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'KEY_REQUIREMENT' => array('label'=>'Key Requirements', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'LEGISLATION1' => array('label'=>'Reference Legislation', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'POSITIONS' => array('label'=>'Allocated To (Position)', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'VERIFICATION' => array('label'=>'Verification', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
		  break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'SUBJECTSTRING',"text" => 'SUBJECTSTRING');
	$ret[] = array("id" => 'ISSUESTRING',"text" => 'ISSUESTRING');
	$ret[] = array("id" => 'KEY_REQUIREMENT',"text" => 'KEY_REQUIREMENT');
	$ret[] = array("id" => 'LEGISLATION1',"text" => 'LEGISLATION1');
	$ret[] = array("id" => 'POSITIONS',"text" => 'POSITIONS');
	return $ret;
}


// Fix page parameter
if($p=='post')
	$pg=(isset($a) && $a=='add')?'new':'edit';
else
  $pg=$p;


// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($pg);
$TABLES  = getPropertiesTables($pg);
$FIELDS  = getPropertiesFields($pg);


?>