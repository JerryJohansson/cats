<?php

/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id = isset($_REQUEST['id'])?$_REQUEST['id']:false;


//-----------------------------------------------
// Retrieve the Obligation Position POST values
//-----------------------------------------------

$ret = false;
switch($action){
	case "add": // Add record
			
		//$db->debug=true;
		$editor = new Editor($m,$p,$a);
		
		$ret = $editor->insert();
		$id=$db->Insert_ID("TBLOBLIGATIONS_SEQ");

		// set users application configuration details
		$ret = set_responsibility_position_details($id);
					
		break;
	case "edit": // Update record
		//$db->debug=true;
		$editor = new Editor($m,$p,$a);
		
		$ret = ($editor->update($id) > 0);
		
		// set users application configuration details
		$ret = set_responsibility_position_details($id);
		
		break;
	default:
		$_SESSION['messageStack']->add("Action parameter was not supplied. No action was taken.",'warning');
		$ret = false;
		break;
}

if($ret) {
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}


function set_responsibility_position_details($id){
	global $TABLES;
	$ret = true;
	//print_r($_POST);

	//add dev
	// $mappedFields = array();
	// if(isset($TABLES['POSITION_ID']['fields'])){
	// 	$flds = explode(",",$TABLES['POSITION_ID']['fields']);
	// 	for ($i = 0; $i < count($flds); $i++) {
	// 		array_push($mappedFields, strtolower($flds[$i])) ;
	// 	}
	// }
	//end dev
	if($id>0){
		if(isset($_POST['POSITION_ID']) && $_POST['POSITION_ID']!=''){
			$keys = $_POST['POSITION_ID'];
			
			if(is_array($keys)){ // lets make sure we have an array
				// delete the records and re-insert with new values
				$ret = db_query("delete from tblObligationPositions where Obligation_ID = $id ");
				$add=array();//add dev
				
				if (sizeof($keys) > 1) { // have any Responsible Positions been selected
					// make the array for binding to our compiled delete statement
					for($i=0; $i < sizeof($keys); $i++){			
						if(!empty($keys[$i]))
							$add[] = array( $keys[$i], $id );//comment dev // array must be in this format to bind to our compiled statement
							// $mappedValues = array();
							// $helparr= array($keys[$i], $id);
							// if(!empty($mappedFields)){
							// for ($i = 0; $i < count($mappedFields); $i++) {
							// $mappedValues[strtolower($mappedFields[$i])] =  $helparr[$i];

							// }
							// }
							// array_push($add, $mappedValues);
					}
								
					// do the bulk delete on the main table using the id as the delete criteria
					if($ret = db_bulk_insert($TABLES['POSITION_ID']['table'],$TABLES['POSITION_ID']['fields'],$add,$db->debug)){ //comment dev
					// if($ret = db_bulk_insert_dev($TABLES['POSITION_ID']['table'],$TABLES['POSITION_ID']['fields'],$add,false)){
						$_SESSION['messageStack']->add("Obligation Responsible Positions updated successfully.","success");
					}else{
						$_SESSION['messageStack']->add("Obligation Responsible Positions updated failed.");
					}
				}else{
					$_SESSION['messageStack']->add("Obligation Responsible Positions updated successfully.","success");
				}
			}else{
				// let me know how you got here if it ever happens
				$_SESSION['messageStack']->add("Problems updating application details.");
			}
		}else{
			$_SESSION['messageStack']->add("The Position ID was not supplied. So I can't create 'Obligation Responsible Positions'");
		}
	}else{
		$_SESSION['messageStack']->add("The ID was not supplied. So I can't create 'Obligation Responsible Positions'");
	}
	return $ret;
}
$db = db_close();
?>