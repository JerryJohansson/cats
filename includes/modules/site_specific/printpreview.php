<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
//require_once(CATS_FUNCTIONS_PATH. 'dave_sql.php');

$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
$RECORD=false;

if($id>0){
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	/*
	$page_access = cats_check_user_site_access($RECORD['SITE_ID']);
	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this employees details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
	*/
	//print_r($RS[$id]);
}
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/<?php echo $m;?>.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// create document.mousedown handlers
	init_document_handlers();
	set_textarea_maxlength();
	init_resize_editor();
	return true;
}
window.onload = init;
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
</fieldset>

<fieldset class="tool_bar">
<?php if(cats_user_is_editor()){ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Site Specific Obligation</a></caption>
</table>
<?php

// Instantiate a new editor
$ed = new Editor($m,$p,$a);
if($id > 0)
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
else
	echo($ed->buildForm($FIELDS));

    // Write the drop down boxes to the page
?>

<!-- BEGIN:: Responsible Positions -->
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Responsible Positions</a></caption>
<tr>
	<td>

<?php
//$ed=new Editor($m,$p,$a);
$seq_fields = array(
	'POSITION_DESCRIPTION' => array('label'=>'Position', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0)
);

$edit_button = "";
$details_sql="SELECT tblposition.position_description FROM tblObligationPositions,tblposition WHERE tblObligationPositions.Obligation_ID = $id AND tblObligationPositions.position_id = tblposition.position_id";
echo ($ed->buildSequentialForm('Responsibilties', $seq_fields, $details_sql));

?>
	</td>
</tr>
</table>
<!-- END:: Responsible Positions -->

<!-- BEGIN:: Schedules -->
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Schedules Created for this Site Obligation</a></caption>
<tr>
	<td>
<?php
// Schedules
$seq_fields = array(
	'SCHEDULER_ID' => array('label'=>'ID', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0),
	'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0),
	'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0),
	'ACTION_TITLE' => array('label'=>'Action', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0),
	'MANAGED_BY' => array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0),
	'COMPLETION_DATE' => array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>0, 'col_span'=>0)
);

$edit_button = "";
$details_sql = "SELECT Scheduler_Id, Site_Description, Department_Description, Action_Title, Managed_By, Completion_Date ";
$details_sql .= "FROM view_schedule ";
$details_sql .= "WHERE Register_Origin = 'Site Specific Obligation' AND REPORT_ID = $id ";
$details_sql .= "ORDER BY Site_Description ASC";
echo ($ed->buildSequentialForm('Responsibilties', $seq_fields, $details_sql));

//$sql = "SELECT Scheduler_Id, Site_Description, Department_Description, Action_Title, Managed_By, Completion_Date ";
//$col_attributes = array('','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
//$col_headers=array('ID','Site','Department','Action','Managed By','Due Date');
//$sql .= "FROM VIEW_SCHEDULE  WHERE Register_Origin = 'Site Specific Obligation' AND REPORT_ID = $id  ORDER BY Site_Description ASC";
//db_render_pager($sql,$col_headers,$col_attributes);
?>
	</td>
</tr>
</table>
<!-- END:: Schedules -->

<fieldset class="tbar" style="text-align:right; " id="button_panel">
<input type="button" class="button" name="cats::Print" value="Print" onClick="_m.print(this);" >
<?php if(cats_user_is_editor()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>

</div>
</form>