<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_sql.php');

$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();




/*	
|-----------------------------------------------
| Name: employee_form_draw_dd
|-----------------------------------------------
| Description: Draws the job types in the as a drop down list
|
| Parameters: $index: -  numeric value
|             $position: - position
|-----------------------------------------------
*/	

	function job_type_form_draw_dd($html, $index, $position)
	{
      return $html;
	}


?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/validation_helpers.js"></script>
<script>

function init(){
	// initialise the tabbing object
	init_tabs();
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
onload = function(){
	init();
}

/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $id;?>");


/*
|--------------------------------------
| ValidateForm
|--------------------------------------
| Parameters: 
|           :- form: HTML form
|
| Returns:
|        :- boolean result
|--------------------------------------
*/
function ValidateForm(form)
{

    // initialize global values
    errorFlag = true;    
    messageString = 'The following fields must be entered\n\n';

	// form details
    SUBJECT                = form.SUBJECT.value;
    ISSUE                  = form.ISSUE.value;
    SITE_ID                = form.SITE_ID.value;
    KEY_REQUIREMENT        = form.KEY_REQUIREMENT.value;
    LEGISLATION1           = form.LEGISLATION1.value;	
    POSITION_ID1           = form.POSITION_ID1.value;	

    checkRequiredField(SUBJECT,                 1, 'Subject missing \n');
    checkRequiredField(ISSUE,                   1, 'Issue missing \n');
    checkRequiredField(SITE_ID,                 1, 'Site missing \n');
    checkRequiredField(KEY_REQUIREMENT,         1, 'Key Requirements missing \n');
    checkRequiredField(LEGISLATION1,            1, 'Reference Legislation missing \n');
    checkRequiredField(POSITION_ID1,            1, 'Position of Responsibility missing \n');

    // display the message if there were problems with the form
    if (errorFlag == false)
        alert(messageString)
		 
	return errorFlag;
}


</script>

</head>
<body class="tb">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return ValidateForm(this)">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
</fieldset>
<fieldset class="tool_bar">
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed = new Editor("edit");

// Initialize 5 values for the array
$Positions = array ( '', '', '', '', '');

// Prefill array values
for ($i = 1; $i <= 5; $i++)
    $Positions[$i] = html_form_draw_employee_positions_dd('POSITION_ID'.$i,'','');

if($id > 0)
{
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id));

    // Get the obligation positions
    $sql = Select_ObligationPositions_FROM_ID_SQL($id);

    // Refill array with existing selected value
    $i = 1;
	  $rs = db_query($sql);
    while($row = $rs->FetchRow())
    {
        $Positions[$i] = html_form_draw_employee_positions_dd('POSITION_ID'.$i,$row["POSITION_ID"],'');
		    $i++;
	  }
}
else
	echo($ed->buildForm($FIELDS));

    // Write the drop down boxes to the page
?>

    <table cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"> 
	    <tr id="row_POSITION_ID1">
        <td class="label"><label title="Name: POSITION_ID1">Positions Of Responsibility</label></td>
        <td colspan="3"><?PHP echo($Positions[1]) ?></td>
      </tr>
	    <tr id="row_POSITION_ID2">
        <td class="label"><label title="Name: POSITION_ID2"></label></td>
        <td colspan="3"><?PHP echo($Positions[2]) ?></td>
      </tr>
	    <tr id="row_POSITION_ID3">
        <td class="label"><label title="Name: POSITION_ID3"></label></td>
        <td colspan="3"><?PHP echo($Positions[3]) ?></td>
      </tr>
	    <tr id="row_POSITION_ID4">
        <td class="label"><label title="Name: POSITION_ID4<?PHP echo($i) ?>"></label></td>
        <td colspan="3"><?PHP echo($Positions[4]) ?></td>
      </tr>
	    <tr id="row_POSITION_ID5">
        <td class="label"><label title="Name: POSITION_ID5"></label></td>
        <td colspan="3"><?PHP echo($Positions[5]) ?></td>
      </tr>												
    </table>

<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="cats::Save" value="Save" >
<input type="button" name="New_Action" value="New Site Specific Obligation" onClick="_m.newModule();" >
<input type="button" name="Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">

<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
</div>
</form>