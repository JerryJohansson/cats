<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
//require_once(CATS_FUNCTIONS_PATH. 'dave_sql.php');

$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
$RECORD=false;

// Security Check
if(!cats_user_is_editor()  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New Site Specific Obligation");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

if($id>0){
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	// var_dump($RECORD);
	// ;die();
	/*
	$page_access = cats_check_user_site_access($RECORD['SITE_ID']);
	if($page_access == false){
		$_SESSION['messageStack']->add("You do not have access to this employees details");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
	*/
	//print_r($RS[$id]);
}
$RS = array();//add dev load data
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/<?php echo $m;?>.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// create document.mousedown handlers
	init_document_handlers();
	set_textarea_maxlength();
	init_resize_editor();
	return true;
}
window.onload = init;
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);

</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
<?php if($action == 'edit'){ ?>
	onSubmit="return CATS_validateForm(this, 'SITE_ID','','R','KEY_REQUIREMENT','','R');" onshite="return ValidateForm(this)">
<?php }else{ ?>
	onSubmit="return CATS_validateForm(this, 'SUBJECT','','R','ISSUE','','R','SITE_ID','','R','KEY REQUIREMENT','','R','LEGISLATION1','','R');" >
<?php } ?>

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<?php if($p!='view'){ ?>
<a
	title="Edit Positions of Responsibilty Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);">Responsibilties Details</a>
<?php } ?>
<?php if($p != 'new'){ ?>
<a
	title="View Schedules Created for this Site Obligation"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);">Schedule Details</a>
<?php } ?>
</fieldset>
<fieldset class="tool_bar">
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php }elseif(cats_user_is_editor(true)){ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Print current <?PHP echo $module_name_txt;?> item"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>	
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed = new Editor($m,$p,$a);
if($id > 0){
	// echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));comment dev
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,isset($RS[$id])?$RS[$id]: false));
}
else
	echo($ed->buildForm($FIELDS));

    // Write the drop down boxes to the page
?>
<fieldset class="tbar" style="text-align:right; ">
<!-- <input type="button" class="button" name="cats::Preview" value="Print Preview" onClick="_m.printpreview();" > -->
<?php if($p!="view"){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" >
<?php }elseif(cats_user_is_editor(true) || cats_user_is_super_administrator()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
<?php if($p=="edit"){ ?>
<input type="button" class="button" name="cats::New" value="New <?php echo $module_name_txt;?>" onClick="_m.newModule();" />
<input type="button" class="button" name="cats::Schedule" value="New Schedule" onClick="_m.newSchedule();" >
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>
<?php if($p!="view"){ ?>
<!-- BEGIN:: Responsible Positions -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Add/Remove Responsible Positions</a></caption>
<tr>
	<td>

<?php
//$ed=new Editor($m,$p,$a);
$seq_fields = array(
	'POSITION_ID' => array('label'=>'Position', 'value'=>NULL, 'func'=>'html_get_obligation_responsibilities_helper', 'col'=>0, 'col_span'=>0)
);

$edit_button = "delete:javascript:remove_sequence_row(this, ::)";
$details_sql="select position_id, '$edit_button' from tblObligationPositions where Obligation_ID = $id";
//echo (html_get_sequential_control('config', '', $details_sql));
echo ($ed->buildSequentialForm('Responsibilties', $seq_fields, $details_sql));

?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Responsible Positions -->
<?php } ?>
<!-- BEGIN:: Schedules -->
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Schedules Created for this Site Obligation</a></caption>
<tr>
	<td>
<?php
// Schedules
$sql = "SELECT Scheduler_Id, Site_Description, Department_Description, Action_Title, Managed_By, Completion_Date";
$col_attributes = array('','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Site','Department','Action','Managed By','Due Date');
if(cats_user_is_editor(true)) {
	// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
	$schedule_edit_button = "edit:javascript:top.show_edit_screen(\"schedules\",\"index.php?m=schedules&p=edit&rm=$m&rp=$p&rid=$id&id=:SCHEDULER_ID:\")";
	// put it into an array so we can merge with column headers later
	$col_headers[] = $schedule_edit_button;
	$sql .= ", '$schedule_edit_button' as edit ";
}
$sql .= " FROM VIEW_SCHEDULE  WHERE Register_Origin = 'Site Specific Obligation' AND REPORT_ID = $id  ORDER BY Site_Description ASC";
db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="cats::SaveProperties" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<!-- END:: Schedules -->


</div>
</form>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
