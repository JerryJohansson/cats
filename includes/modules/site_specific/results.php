<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$where = "";
$multi_sites = $_SESSION['user_details']['site_access'];
$users_site  = $_SESSION['user_details']['site_id'];


if(is_array($_SESSION['user_details']) && $multi_sites!='')
	$where = $multi_sites;
else
	$where = $users_site;

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST) > 0)
	$_SESSION[$m.'_'.$p] = $_POST;
else if(isset($_SESSION[$m.'_'.$p]))
	$_POST=$_SESSION[$m.'_'.$p];


// If the post is equal to search then perform the following
if(isset($_POST['cats::search'])||isset($_POST['cats::report'])){

	$sql             = "SELECT DISTINCT Obligation_ID, SubjectString, IssueString, Key_Requirement, Legislation1, Positions";
	$action_controls = array();
	if(isset($_POST['cats::report'])){
		if(isset($_POST['cats::report_fields']) && !empty($_POST['cats::report_fields']))
			$sql = "select ".$_POST['cats::report_fields']." ";
		else
			$sql = "Select * ";
	}else{
		if(cats_user_is_editor(true)) {
			// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
			$edit_button = "edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:{$TABLES['id']}:\")";
			// put it into an array so we can merge with column headers later
			$action_controls[] = $edit_button;
			$sql .= ", '$edit_button' as edit ";
		}
		if(cats_user_is_super_administrator()) {
			// delete button string can be a button or an action checkbox for each row...
			// the checkboxes also have a dropdown which is displayed to the right of the grid navigation
			// The button control sql looks like this:
			// delete:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=post&a=del&id=:{$TABLES['id']}:\")";
			$delete_button = "delete:::{$TABLES['id']}:";
			$action_controls[] = $delete_button;
			$sql .= ", '$delete_button' as del ";
		}
	}
	$sql .= " FROM {$TABLES['view']} ";
	
	/*
	|------------------------------------------------------------
	| Description: Modify colattribs alongside colarr
	|              these are passed into the db_render_page control
	|------------------------------------------------------------
	*/
	// specify columns to ignore when drawing the table
	$col_ignore = array($TABLES['id']);
	// specify links for column values on each row
	$col_links = array("","js|top.view('$m',|{$TABLES['id']}","","","","");
	
	$col_attributes	= array(' width="10" ',' width="200" ',' width="100" ',' width="" ',' width="200" ',' width="150" ');
	$col_headers		= explode("|","ID|Subject|Issue|Key Requirement|Legislation|Responsible Positions");
	$col_headers		= array_merge($col_headers,$action_controls); // append edit and delete buttons if they exist

	$order_by_col = "SUBJECTSTRING";

	$sql_where = "";
	
	// Create Where String and append it to the Select String
	foreach($FIELDS as $column_name => $props){
		// the criterias operator I.E operator of <= will look like COLUMN_NAME <= 'SEARCH VALUE'
		$operator = isset($props['operator']) ? " {$props['operator']} " : " = ";
		// the values delimiter I.E a delim of ' will look like 'SEARCH VALUE'
		$delim = isset($props['delim']) ? $props['delim'] : '';
		// create the user specific sites criteria and add to the where string
		if($column_name == "SITE_ID" && (!isset($_POST[$column_name]) || empty($_POST[$column_name]))){
			$sql_where .= (($sql_where == "") ? " WHERE ":" AND ") . db_get_user_sites_where_string("SITE_ID");
		}else{
			switch($column_name){
				default:
					// sql_where is passed by reference
					db_get_where_clause($column_name,$sql_where,$operator,$delim);
					break;
			}
		}
	}
	$search_sql = $sql . $sql_where;
	
	$db->debug = false;
	if($db->debug == true)
	{
		print_r($_POST);
		echo('<br>'.$search_sql);
	}
	
	require_once(CATS_ADODB_PATH . 'cats_csv.php');
	
}

?>
<script>
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	// resize result iframe to consume the parent page
	init_resize_results_container();
	return true;
}
window.onload = init;
window.onresize = init_resize_results_container;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><form action="index.php?m=<?php echo $m;?>" method="post">
<table width="100%">
<tr valign="top">
	<td width="100%">	
<?php

// Search Results
if(isset($search_sql))
{
    //db_render_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], true, $order_by_col, 'DESC');
		cats_form_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], $m, $order_by_col, 'ASC', $col_links, $col_ignore);
}
else
	echo("<h2>No Search results</h2>");
?>
<!-- END:: Results table -->
	</td>
</tr>
</table>
</form>