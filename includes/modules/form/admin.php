<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
require_once ("../includes/config.php");
authenticate('admin');
require(DIR_WS_CLASSES."ezweb.php");
require(DIR_WS_CLASSES."template.php");
require(DIR_WS_CLASSES."all.php");
$ezweb = new Ezweb;

$id = $_REQUEST['id'];
$action = $_REQUEST['action'];
$mode = $_REQUEST['mode'];
$refresh_tree = $_REQUEST['refresh_tree'];

if($action=="edit") // View the page or category
{
	exit("edit");
}
elseif($action=="add") // Show the delete confirmation
{
	exit("add");
}
elseif($action=="email") // Show the delete confirmation
{
	if(isset($id))
		$ezweb->displayEmail($id);
	else
		echo("No page to display");
}
else // Show the default admin page
{
	require_once(DIR_WS_CLASSES.'users.php');
	$user=new User;
	$hdr = "templates/no_hdr_main.htm";
	$data = $_SESSION['user_details'];
	switch($mode)
	{
		case 'content':case 'edit':case 'admin':
			$tpl = new Template("templates/admin_todolist.htm");
			$values = array(
				"todo_list" => $user->get_user_admin_todo_list()
			);
			$tpl->parse($values);
			$xtra_templates = $tpl->template;
			
			$xtra_content .= "<div><b>Instructions:</b><ul><li>Click the Site Settings button on the right to change any site-wide configuration settings</li><li>Click the My Account button to change your details</li><li>Click on any one of the main links in the top right of the screen to leap to these sections:<ul><li><b>Admin:</b> This page</li><li><b>Content:</b> Content maintenance</li><li><b>System:</b> Site Configuration settings</li><li><b>Users:</b> Maintain your customer/members/Administrator list</li></ul></li></ul></div>";
			$values = array(
				"name"						=> $data['user_nick'],
				"email"						=> $data['email'],
				"user_id"					=> $data['user_id'],
				"firstname"     	=> $data['firstname'],
				"lastname"				=> $data['lastname'],
				"lognum"					=> $data['lognum'],
				"lastlogdate"			=> date('l dS of F Y',strtotime($data['lastlogdate'])),
				"lastlogtime"			=> date('h:i:s A',strtotime($data['lastlogdate'])),
				"admin_email"			=> (defined('ADMIN_EMAIL') ? ADMIN_EMAIL : "me@ezwebmaker.com"),
				"xtra_content"		=> $xtra_content,
				"xtra_templates"	=> $xtra_templates,
				"xtra_panels"			=> $xtra_panels
			);
			$admin_template = "templates/admin.htm";
			break;
		default:
			//$xtra_xtra = '';//'<p><img src="http://www.buzmedia.com.au/images/www-index.jpg" width="180"></p>';
			//$xtra_content .= "<div><b>Instructions:</b><ul><li><b>Admin:</b> Change site wide settings and configuration details</li><li><b>Content:</b> Content maintenance, create/edit web pages</li><li><b>Orders:</b> Order maintenance - check out all orders for products</li><li><b>Users:</b> Maintain your customer/members/Administrator list</li></ul></div>";
			$values = array(
				//"xtra_content"		=> $xtra_content,
				//"xtra_xtra"				=> $xtra_xtra,
				"recent_pages_list"	=> $user->get_user_history_list($data['user_id']),
				"create_pages_list"			=> '		 	<div id="sectionLinks">
			<a style="padding-left:40px;background:url(../js/tree/icons/article.gif) left no-repeat;" href="editor.php?action=add&type=page&pid=2">Feature</a>
			<a style="padding-left:40px;background:url(../js/tree/icons/newsarticle.gif) left no-repeat;" href="editor.php?action=add&type=news&pid=3">News</a>
			<a style="padding-left:40px;background:url(../js/tree/icons/schedule.gif) left no-repeat;" href="editor.php?action=add&type=schedule&pid=4">Event</a>
			<a style="padding-left:40px;background:url(../js/tree/icons/testimonial.gif) left no-repeat;" href="editor.php?action=add&type=testimonial&pid=6">Testimonial</a>
			<a style="padding-left:40px;background:url(../js/tree/icons/campaign.gif) left no-repeat;" href="editor.php?action=add&type=campaign&pid=7">Campaign</a>
		</div>
'//$user->get_user_new_item_list($data['user_id'])
			);
			$admin_template = "templates/splash.htm";
			//$hdr = "templates/main_splash.htm";
			break;
	}

	$admintpl = new Template($admin_template);
	$admintpl->parse($values);
	$main .= $admintpl->template;
}
if(isset($refresh_tree)) $onload = 'onload=function(){updateTree('.$refresh_tree.')}';
$tpl = new Template($hdr);
$values = compact('main', 'onload');
$tpl->parse($values);
$tpl->output();
exit();
?>
