<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once ("../../conf.php");
require_once(CATS_CLASSES_PATH . "template.php");
require_once(CATS_CLASSES_PATH . "navigation.php");
$menu = new Navigation("refreshtree", "admin");

if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
if(isset($_REQUEST['pid'])) $pid = $_REQUEST['pid'];
if(isset($_REQUEST['action'])) $action = $_REQUEST['action'];
if(isset($_REQUEST['mode'])) $mode = $_REQUEST['mode'];
if(isset($_REQUEST['dir'])) $dir = $_REQUEST['dir'];
if(isset($_REQUEST['id_list'])) $id_list = $_REQUEST['id_list'];
if(isset($_REQUEST['root'])) $root = $_REQUEST['root'];

if($action=="move") // Move node
{
	$menu->reorder($id,($action.$dir));
	//header("Location: explore.php");
}
elseif($action=="paste")
{
	$menu->paste($mode,$id_list,$id);
	//header("Location: explore.php");
}
elseif($action=="delete")
{
	$menu->delete($id);
	//header("Location: explore.php");
}
//echo('pid='.$pid.'::id='.$id.'::root='.$root.'<br>');
if(!isset($root)) $root = $pid;
$menu->setMenus($root,$id);
echo $menu->getMenuJS();

$db=db_close();

?>