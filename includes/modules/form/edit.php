<?php
require_once(CATS_ADODB_PATH . "tohtml.inc.php");
require_once(CATS_CLASSES_PATH . "template.php");
require_once(CATS_CLASSES_PATH . "users.php");
require_once(CATS_CLASSES_PATH . "navigation.php");
require_once(CATS_CLASSES_PATH . "edit.php");
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
/*
+------------------------------------------------------------------------
|  Comment: Show type selection if we are in a new page and the parent page item allows different types as children
+------------------------------------------------------------------------
*/
$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
$pid=isset($_REQUEST['pid'])?$_REQUEST['pid']:0;
if($pid>0 && $id==1) $id = $pid;
$type = (isset($_REQUEST['c']) && !empty($_REQUEST['c']))?$_REQUEST['c']:'';
$class_pagetype=($type=='')?'Edit':ucwords($type);

if(empty($type)){
	switch($action){
		case 'add':
			if(isset($pid)){
				$class_pagetype=ucwords(db_get_parent_pagetype($pid));
			}
			break;
		case 'edit':
			if(isset($id)){
				$class_pagetype=ucwords(db_get_parent_pagetype($id));
			}
			break;
		default:
			main_redirect("admin.php?mode=$action");
			break;
	}
}

if(empty($id)) $id=0;
$user_group_mask=$_SESSION['user_details']['groups'];
$user_role_mask=$_SESSION['user_details']['roles'];
$extras = array("No results");
$mode = "admin";
$RS = array();
if($action=="add")
{
	$container = new Edit($pid,$mode);
	//$container->load($pid,$mode);
	if(empty($pid) || $pid == 0)
	{
		$pid = 0;
	}
	$extras = $container->getFormEdit();
}
elseif($action=="edit")
{
	$container = new Edit($id,$mode);
	//$container->load($id,$mode);
	$extras = $container->getFormEdit();
}
elseif($action=="del") // Show the delete confirmation
{
	$nav = new Navigation;
	$nav->delete($id);
}
else
{
	main_redirect("index.php?m=admin&p=splash");
}

$iTabs = 0; // Tab counter
$iTabButtons=0;
$status = $container->getStatus();
if(!((bool)$status)) $status=0;
$page_title = "Editing:";
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script type="text/javascript">
/***********************
 start up script for each page
***********************/
var MODULE = '<?php echo $m;?>';
var PAGE = 'search';
function init(){
	init_tabs();
	// create document.mousedown handlers
	init_document_handlers();
	// resize editing section
	init_resize_editor();
	return true;
}
window.onload = init;
window.onresize = function(){
	init_resize_editor();
	resize_editor();
}

function resize_editor(){
	var ed=element('HELP_USER_GUIDE___Frame');
	if(ed){
		var pos=getPos(ed);
		var name='screen_'+MODULE+"_"+((PAGE)?PAGE:'edit');
		var bh=top.gui.screens[name].page.offsetHeight;
		ed.style.height=(bh-(pos.y+(tb.offsetHeight+4)));
	}
}

function set_edit_mode(itm){
	// This is a hack for Gecko... it stops editing when the editor is hidden.
	resize_editor();
	if (!document.all )
	{
		var oEditor = FCKeditorAPI.GetInstance('HELP_USER_GUIDE') ;
		if (  oEditor.EditMode == FCK_EDITMODE_WYSIWYG )
			oEditor.MakeEditable() ;
	}
}

/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
</script>

</head>
<body class="edit">
<form name="module" action="?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?php echo $id;?>" method="POST" enctype="multipart/form-data"
	onsubmit="return CATS_validateForm(this, 'FIELD_NAME','','R','FIELD_LABEL','','R','FIELD_GROUP_TYPE','','R','FIELD_TYPE_ID','','R','SORT_ORDER','','RisNum');">
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:tab_onclick(0);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<a
	title="Edit Extended Properties"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="javascript:tab_onclick(1);set_edit_mode(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/help.gif);">Help/User Guide</a>
</fieldset>
<fieldset class="tool_bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php
if($action=="edit"){if(cats_user_is_administrator()){
?>
<a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<?php
}}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<?PHP $container->drawForm($extras); ?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" class="submit" name="Submit" value="Save" />
<input type="button" class="button" name="Cancel" value="Cancel" onclick="doCancel();">
</fieldset>
</fieldset>


<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc;">
		<table>
		<tr>
			<td class="info"></td>
			<td>
				<p>Enter help information and documentation for the user guide:</p>
				<ul>
					<li><b>Enter Help:</b> Describe the item and its purpose breifly.</li>
					<li><b>Enter User Guide :</b> Describe the item in detail making sure to describe it's relationship with other items etc.</li>
				</ul>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label>Help Tip</label></td><td><textarea class="sfield" name="HELP_TIP"><?php echo( $RS->fields['HELP_TIP'] );?></textarea></td>
</tr>
</table>
<fieldset class="bar">
<?php 
// crashing IE ATM so revert to heavy HTML editor FKEditor
//echo html_draw_ezedit_field('FIELD_USER_GUIDE','<p>help goes here</p>');

include(CATS_ROOT_PATH."js/ezedit/fckeditor.php") ;
$sBasePath = WS_PATH . 'js/ezedit/';
//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "_samples" ) ) ;

$oFCKeditor							= new FCKeditor('HELP_USER_GUIDE') ;
$oFCKeditor->BasePath		= $sBasePath ;
$oFCKeditor->ToolbarSet	= 'CATS' ;
$oFCKeditor->SID	= strip_tags(session_id()) ;
$oFCKeditor->Value			= $RS->fields['HELP_USER_GUIDE'];
$oFCKeditor->Create();

?>
</fieldset>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" class="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
</div>
</form>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
