<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
//require_once ('../../conf.php');
require_once (CATS_CLASSES_PATH . 'edit.php');

//print_r($_GET);
//print_r($_POST);
//echo("<br><h1>id=$id</h1>");
if(isset($a) && !empty($a)) $action = $a;
$ret = false;
$pid = 0;
switch($action){
	case "add": // Add record
		$pid = $id;
		$editor = new Edit($id,"save");
		$ret = $editor->insert();
		if($ret) $id=$db->Insert_ID("FORM_FIELDS_SEQ");
		if($id>0){
			// insert help and user guide
			// returns an array used for the update/insert
			$sql_data = array(
				'FIELD_ID'=>$id,
				'HELP_TIP'=>$_POST['HELP_TIP'],
				'HELP_USER_GUIDE'=>$_POST['HELP_USER_GUIDE']
			);
			
			// get a recordset to use as the base of the insert statement
			$rs = $db->Execute("select * from HELP_FIELDS where 1=2");
			
			// Create an update statement based on the recordset($rs) and
			// sql data array($sql_data)
			$sql = $db->GetInsertSQL($rs,$sql_data);
			$_SESSION['messageStack']->add("SQL:<br>".htmlspecialchars($sql),"info");
			if(!empty($sql)){
				if($ret = $db->Execute($sql)){
					$_SESSION['messageStack']->add("Help and User Guide details created.","success");
				}else{
					$_SESSION['messageStack']->add("An error occured while inserting the help and user guide.<br>Error: ".$db->ErrorMsg());
					$ret = false;
				}
			}
		}else{
			$_SESSION['messageStack']->add("An error occured while inserting form field details.<br>Error: ".$db->ErrorMsg());
			$ret = false;
		}
		
		break;
	case "edit": // Update node
		//echo("<br>id=$id<br>");
		//print_r($_POST);
		//$db->debug=true;
		$editor = new Edit($id,"save");
		$ret = ($editor->update($id) > 0);
		if($ret){
			// update help and user guide
			// returns an array used for the update/insert
			$sql_data = array(
				'FIELD_ID'=>$id,
				'HELP_TIP'=>$_POST['HELP_TIP'],
				'HELP_USER_GUIDE'=>$_POST['HELP_USER_GUIDE']
			);
			
			// get a recordset to use as the base of the insert statement
			$rs = $db->Execute("select * from HELP_FIELDS where field_id = $id");
			
			// Create an update statement based on the recordset($rs) and
			// sql data array($sql_data)
			$sql = $db->GetUpdateSQL($rs,$sql_data);
			$_SESSION['messageStack']->add("SQL:<br>".htmlspecialchars($sql),"info");
			if(!empty($sql)){
				if($ret = $db->Execute($sql)){
					$_SESSION['messageStack']->add("Help and User Guide details updated.","success");
				}else{
					$_SESSION['messageStack']->add("An error occured while updating help and user guide details.<br>Error: ".$db->ErrorMsg());
				}
			}
		}else{
			$_SESSION['messageStack']->add("An error occured while updating form field details.<br>Error: ".$db->ErrorMsg());
			$ret = false;
		}
		
		break;
	default:
		$ret = false;
		break;
}

if($db->debug==true) exit;
if($ret) {
	//$url = "index.php?m=form&p=edit&a=edit&id=$id";
	$return_js_function = "";
	if($pid>0){
		$return_js_function .= "parent.window.frames['frmNav'].window.action('".WS_MODULES_PATH."form/actions.php?pid={$pid}');";
	}
	$return_js_function .= "location.href='index.php?m=form&p=edit&a=edit&id=$id'";
	//isset($success_return_module)?"top._edit_success('$success_return_module','$m','$success_remove_page')":"top._edit_success('$m')";
	include(CATS_INCLUDE_PATH . 'success.inc.php');
} else {
	$_SESSION['messageStack']->add("If you think this is an urgent error then please notify Tiwest IT department of this message. The error has been logged and will be invesigated as soon as one of the technical staff is available.",'warning');
	include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
	exit;
}
// clean up
$db=db_close();
?>