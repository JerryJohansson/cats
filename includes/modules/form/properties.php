<?php

function getPropertiesTables()
{
	return array(
		'table'=>'FORM_FIELDS', 
		'id'=>'FIELD_ID',
		'view'=> 'FORM_FIELDS'
	);
}


function getPropertiesFields(){
	return array(
		'FIELD_ID' => array('label'=>'Field Id', 'value'=>'1', 'func'=>'html_form_show_hidden(', 'col'=>1, 'col_span'=>2), 
		'FIELD_PID' => array('label'=>'Field Pid', 'value'=>'0', 'func'=>'html_form_show_hidden(', 'col'=>1, 'col_span'=>2), 
		'FIELD_GROUP_TYPE' => array('label'=>'Field Group Type', 'value'=>'menu', 'func'=>'html_group_type_dd(', 'col'=>1, 'col_span'=>2), 
		'FIELD_NAME' => array('label'=>'Field Name', 'value'=>'CATS', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FIELD_LABEL' => array('label'=>'Field Label', 'value'=>'CATS - Main Menu', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FIELD_TYPE' => array('label'=>'Field Type', 'value'=>'Menu', 'func'=>'html_form_types_dd(', 'col'=>1, 'col_span'=>2), 
		'FIELD_LENGTH' => array('label'=>'Field Length', 'value'=>'0', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FIELD_VALUE' => array('label'=>'Field Value', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FIELD_ATTRIBUTES' => array('label'=>'Field Attributes', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FIELD_TABLE_NAME' => array('label'=>'Field Table Name', 'value'=>'', 'func'=>'html_form_tables_helper('FIELD_COLUMN_NAME',', 'col'=>1, 'col_span'=>2), 
		'FIELD_COLUMN_NAME' => array('label'=>'Field Column Name', 'value'=>'', 'func'=>'html_form_columns_helper($props['FIELD_TABLE_NAME'],', 'col'=>1, 'col_span'=>2), 
		'FIELD_DISPLAY_FUNC' => array('label'=>'Field Display Func', 'value'=>'', 'func'=>'html_form_functions_chooser('db_functions.php',', 'col'=>1, 'col_span'=>2), 
		'FIELD_DISPLAY_PARAMS' => array('label'=>'Field Display Params', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'FIELD_SET_FUNC' => array('label'=>'Field Set Func', 'value'=>'', 'func'=>'html_form_functions_chooser('db_functions.php',', 'col'=>1, 'col_span'=>2), 
		'FIELD_SET_PARAMS' => array('label'=>'Field Set Params', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'COL_NUM' => array('label'=>'Col Num', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'COL_SPAN' => array('label'=>'Col Span', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'ROW_SPAN' => array('label'=>'Row Span', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'COL_COUNT' => array('label'=>'Col Count', 'value'=>'1', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'MANDATORY' => array('label'=>'Mandatory', 'value'=>'0', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'OPTIONS' => array('label'=>'Options', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'SORT_ORDER' => array('label'=>'Sort Order', 'value'=>'1', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'ROLE_MASK' => array('label'=>'Role Mask', 'value'=>'254', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'GROUP_MASK' => array('label'=>'Group Mask', 'value'=>'254', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'STATUS' => array('label'=>'Status', 'value'=>'0', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
	);
}


$TABLES=getPropertiesTables();
$FIELDS=getPropertiesFields();
?>