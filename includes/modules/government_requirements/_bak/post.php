<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_sql.php');
require_once('properties.php');




if(isset($a) && !empty($a)) $action = $a;
if(!isset($action)) $action = $_REQUEST['a'];
$id=isset($_REQUEST['id'])?$_REQUEST['id']:false;

if($action == 'add' || $id)
{

	$ret = true;
	switch($action){
		case "add": // Add record
		    $sql = Create_GovernmentDetails_SQL_InsertString($_POST, $_GET);	
			$editor = new Editor($action, "admin");
			$ret = $editor->sql_insert($sql);

			break;
		case "edit": // Update node
		    $sql = Create_GovernmentDetails_SQL_UpdateString($_POST, $_GET);	
			$editor = new Editor($action, "admin");
			$ret = $editor->sql_insert($sql);

			break;
		default:
			$ret = false;
			break;
	}

	if($ret)
		include(CATS_INCLUDE_PATH . 'success.inc.php');
	else
	{
		dprint(__FILE__,__LINE__,0,"Error occured while editing");
		
		// Output an arror whether it was insert or update related
		if ($action = 'add')
		    echo('There was an error performing an insert to the database');
		else
		    echo('There was an error performing an update to the database');

	}
}
else
	main_redirect("index.php?m=$m&p=search");

$db = db_close();

?>