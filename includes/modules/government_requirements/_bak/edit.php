<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:1;


// init the tabs index for tab iteration
$iTabs = 0;
$RS = array();
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/classes/helper/helper.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/validation_helpers.js"></script>
<script>

function _target(e){
	return (document.all)?event.srcElement:e.target;
}
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}

var win=window;
function init(){
	// start up script for each page

	// Execute some start up script
	initTabs();

	document.onmousedown=function(evt){
		if(typeof(_hide_calendar)=="function") _hide_calendar();
		if(top.hideMenu) top.hideMenu();
	}
	setTimeout(_hide_message,1000);
	
	
	
	return true;
}

function printPage() { print(document); }

function _show_message(s){
	var o=element('loading_message');
	o.innerHTML='<table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2>'+s+'</h2></td></tr></table></td></tr></table>'
	//o.style.display='block';
	o.style.visibility = '';
	_show_hide_elements('select',false);
}

function _hide_message(){
	element('loading_message').style.visibility='hidden';
	_show_hide_elements('select',true);
}


function _show_hide_elements(tag_name, b_show){
	var sel=document.getElementsByTagName(tag_name);
	var i=0;
	var len=sel.length;
	var display = (b_show)?'':'none';
	for(i=0;i<len;i++){
		sel[i].style.display = display;
	}
}

function new<?PHP echo $module_name;?>(){
	top.show_edit_screen("<?php echo $m;?>","index.php?m=<?php echo $m;?>&p=new&a=add");
}

function save<?PHP echo $module_name;?>(){
	var f = document.forms.form;
	f.Submit.click();
}


function doCancel(){
	top.show_screen('<?php echo $m;?>','search');
}

onload = function(){
	init();
}


function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}
function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.id; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') {
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (val<min || max<val) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
  
/*
|--------------------------------------
| ValidateForm
|--------------------------------------
| Parameters: 
|           :- form: HTML form
|
| Returns:
|        :- boolean result
|--------------------------------------
*/
function ValidateForm(form)
{


    // initialize global values
    errorFlag = true;    
    messageString = 'The following fields must be entered\n\n';
	
	// form details
    REQUIREMENT_TYPE      = form.REQUIREMENT_TYPE.value;
    DATE_COMPLETED        = form.DATE_COMPLETED_d.value;
	SITE_ID               = form.SITE_ID.value;
	GOVT_DEPARTMENT       = form.GOVT_DEPARTMENT.value;
	LOCATION              = form.LOCATION.value;	
	CONDUCTED_BY_ID       = form.CONDUCTED_BY_ID.value;	
	TIWEST_CONTACT_ID     = form.TIWEST_CONTACT_ID_text.value;			
	LOCATION_OF_DOCUMENTS = form.LOCATION_OF_DOCUMENTS.value;	
	SHIFT_ID              = form.SHIFT_ID.value;

    checkRequiredField(REQUIREMENT_TYPE,      1, 'Inspection Type missing \n');
    checkRequiredField(DATE_COMPLETED,        1, 'Date Completed missing \n');
    checkRequiredField(SITE_ID,               1, 'Site missing \n');
    checkRequiredField(GOVT_DEPARTMENT,       1, 'Government Department missing \n');
    checkRequiredField(LOCATION,              1, 'Location Of Inspection missing \n');
    checkRequiredField(CONDUCTED_BY_ID,       1, 'Government Department missing \n');
    checkRequiredField(TIWEST_CONTACT_ID,     1, 'Tiwest Contact missing \n');
    checkRequiredField(LOCATION_OF_DOCUMENTS, 1, 'Location Of Additional Documents missing \n');
    checkRequiredField(SHIFT_ID,              1, 'Shift missing \n');

    // display the message if there were problems with the form
    if (errorFlag == false)
        alert(messageString)
		 
	return errorFlag;
}
  

</script>
<style type="text/css">
textarea { background-color: #fff; border: 1px solid #000000; width: 100% !important; }
a.indent { border: inset; background-color: #999999; font-weight:bold; }
</style>
</head>
<body class="tb">
<?php
// Displays the loading screen
require_once(CATS_INCLUDE_PATH."loading_message.php");
?>
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return ValidateForm(document.form)">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="Editor" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<a
	title=""
	id="Followup" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);">Follow-up Actions</a>
</fieldset>
<fieldset class="tool_bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:new<?PHP echo $module_name;?>();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:save<?PHP echo $module_name;?>();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:doCancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo ucwords((string)$module_name_txt);?> item"
	href="javascript:document.location.reload();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody" style="padding-top:60px;">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed=new Editor("edit");

// Check whether to edit or add a new record
if($id > 0)
	echo($ed->buildForm($FIELDS,$TABLES['edit_view']['name'],$TABLES['edit_view']['id'],$id));
else
	echo($ed->buildForm($FIELDS));

?>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="Submit" value="Save" >
<input type="button" name="Cancel" value="Cancel" onClick="doCancel();">
</fieldset>		
</fieldset>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td>
<?php
// Follow up actions for record
$col_attributes = array('','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date','edit:javascript:top.show_edit_screen("actions","index.php?m=actions&p=edit&id=:REPORT_ID:")');
//print_r($RS);
$sql = "SELECT Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date, 'edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:REPORT_ID:\")' as edit  FROM View_Action_Details  WHERE Report_Id = $id  AND Origin_Table='Originating_Government'  ORDER BY Action_Title ASC";
//print $sql;
db_render_pager($sql,$col_headers,$col_attributes);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
</div>
</form>