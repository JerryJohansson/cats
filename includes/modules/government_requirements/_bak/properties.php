<?php


/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search'){
	$ret = array();
	switch($action){
		case 'search':
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Government Inspections',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Government Inspection & Correspondence',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Government Inspection & Correspondence',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables()
{
	return array(
		'edit'=> array('name'=>'TBLGOVERNMENT_DETAILS', 'id'=>'GOVERNMENT_ID'),
		'search'=> array('name'=>'VIEW_GOVERNMENT_DETAILS','id'=>'GOVERNMENT_ID'),
		'edit_view'=> array('name'=>'VIEW_GOVERNMENT_DETAILS','id'=>'GOVERNMENT_ID')
	);
}



/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{
	$ret = array();

	switch($action)
	{
		case 'search':
			$ret = array(
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'REFTABLE_DESCRIPTION' => array('label'=>'Inspection Type', 'value'=>'', 'func'=>'html_form_draw_inspection_type_dd', 'col'=>1, 'col_span'=>2), 
				'GOVT_DEPARTMENT' => array('label'=>'Government Department', 'value'=>'', 'func'=>'html_form_draw_government_departments_dd', 'col'=>1, 'col_span'=>2), 
				'LOCATION' => array('label'=>'Location', 'value'=>'', 'func'=>'html_form_draw_government_locations_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_COMPLETED_FROM' => array('label'=>'Date Completed From', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1), 
				'DATE_COMPLETED_TO' => array('label'=>'Date Completed To', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1)
			);
			break;
		case 'new':
			$ret =  array(
				'REQUIREMENT_TYPE' => array('label'=>'Inspection Type', 'value'=>'', 'func'=>'html_form_draw_inspection_type_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_COMPLETED' => array('label'=>'Date Completed', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'GOVT_DEPARTMENT' => array('label'=>'Government Department', 'value'=>'', 'func'=>'html_form_draw_government_departments_dd', 'col'=>1, 'col_span'=>2), 
				'LOCATION' => array('label'=>'Location Of Inspection', 'value'=>'', 'func'=>'html_form_draw_government_locations_dd', 'col'=>1, 'col_span'=>2), 
				'CONDUCTED_BY_ID' => array('label'=>'Government Representative', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'TIWEST_CONTACT_ID' => array('label'=>'Tiwest Contact', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
				'LOCATION_OF_DOCUMENTS' => array('label'=>'Location Of Additional Documents', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
				'SHIFT_ID' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_form_draw_reftable_shift_dd', 'col'=>1, 'col_span'=>2), 
				'COMMENTS' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'edit':
			$ret =  array(
				'REFTABLE_DESCRIPTION' => array('label'=>'Incident Type', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 	
				'DATE_COMPLETED' => array('label'=>'Date Completed', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'GOVT_DEPARTMENT' => array('label'=>'Government Department', 'value'=>'', 'func'=>'html_form_draw_government_departments_dd', 'col'=>1, 'col_span'=>2), 
				'LOCATION' => array('label'=>'Location Of Inspection', 'value'=>'', 'func'=>'html_form_draw_government_locations_dd', 'col'=>1, 'col_span'=>2), 
				'CONDUCTED_BY_ID' => array('label'=>'Government Representative', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
				'TIWEST_CONTACT_ID' => array('label'=>'Tiwest Contact', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
				'LOCATION_OF_DOCUMENTS' => array('label'=>'Location Of Additional Documents', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 
				'SHIFT_ID' => array('label'=>'Shift', 'value'=>NULL, 'func'=>'html_form_draw_reftable_shift_dd', 'col'=>1, 'col_span'=>2), 
				'COMMENTS' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
			);
			break;

	}
	return $ret;
}


// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($p);
$TABLES  = getPropertiesTables($p);
$FIELDS  = getPropertiesFields($p);

?>