<?php
//SUBJECT = '#SUBJECT#', MESSAGE = '#MESSAGE#' where PERSON_DESCRIPTION='#PERSON_DESCRIPTION#' and SCREEN_ID=#SCREEN_ID# and SITE_ID

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit PCR Email Message',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New Email Message',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'View PCR Email Message',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search PCR Email Messages',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables(){
	$ret = array(
		'table'=> 'TBLPCR_EMAIL',
		'id'=> 'EMAIL_ID',
		'view'=> 'VIEW_PCR_EMAIL'
	);
	return $ret;
}

function getPropertiesFields($page='search'){
	
	switch($page){
		case 'edit':
			$ret = array(
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
				'SCREEN_ID' => array('label'=>'Screen Id', 'value'=>NULL, 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>0),
				'SCREEN_NAME' => array('label'=>'Screen', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2),
				'PERSON_DESCRIPTION' => array('label'=>'Person', 'value'=>'', 'func'=>'html_form_show_hidden', 'params'=>'style=\"width:100%\"', 'col'=>1, 'col_span'=>2), 
				'SUBJECT' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
				'MESSAGE' => array('label'=>'Message', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'new':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2),
				'SCREEN_ID' => array('label'=>'Screen', 'value'=>NULL, 'func'=>'html_draw_pcr_tabs_search_dd', 'col'=>1, 'col_span'=>2),
				'PERSON_DESCRIPTION' => array('label'=>'Person', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
				'SUBJECT' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
				'MESSAGE' => array('label'=>'Message', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'view':
			$ret = array(
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'SCREEN_NAME' => array('label'=>'Screen', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'PERSON_DESCRIPTION' => array('label'=>'Person', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'SUBJECT' => array('label'=>'Subject', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'MESSAGE' => array('label'=>'Message', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'search': case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2),
				'SCREEN_ID' => array('label'=>'Screen', 'value'=>NULL, 'func'=>'html_draw_pcr_tabs_search_dd', 'col'=>1, 'col_span'=>2),
				'PERSON_DESCRIPTION' => array('label'=>'Person', 'delim'=>"'", 'value'=>'', 'func'=>'html_draw_pcr_email_person_search_dd', 'col'=>1, 'col_span'=>2), 
				'SUBJECT' => array('label'=>'Subject', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
				'MESSAGE' => array('label'=>'Message', 'delim'=>"'", 'operator'=>'LIKE', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>