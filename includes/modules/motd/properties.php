<?php



/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search'){
	$ret = array();
	switch($action){
		case 'search':
		case 'results':			
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Message Of The Day',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Message Of The Day',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Message Of The Day',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


function getPropertiesTables()
{
	return array(
		'table'=>'TBLMOTD', 
		'id'=>'MOTDID',
		'view'=> 'VIEW_MOTD'
	);
}


/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/



function getPropertiesFields($action='search')
{
	$ret = array();

    //exit($action);

	switch($action)
	{
		case 'results':
		case 'search':
			$ret = array(
				'MOTD_TYPE' => array('label'=>'Type', 'value'=>NULL, 'func'=>'html_motd_type_dd', 'col'=>1, 'col_span'=>2), 
				'MOTD_SITE' => array('label'=>'Site', 'value'=>'', 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2),
				'MOTD_STARTDATE_FROM' => array('label'=>'Start From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1), 
				'MOTD_STARTDATE_TO' => array('label'=>'Start To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1)							
			);
			break;
		case 'new':
		case 'edit':
			$ret = array(
				array(
					'MOTD_TYPE' => array('label'=>'Type', 'value'=>NULL, 'func'=>'html_motd_type_dd', 'col'=>1, 'col_span'=>2), 
					'MOTD_SHORT' => array('label'=>'Short Title', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
					'MOTD_URL' => array('label'=>'URL (Optional)', 'value'=>'', 'func'=>'html_draw_lge_input_field', 'col'=>1, 'col_span'=>2), 
					'MOTD_SITE' => array('label'=>'Site(s) which can view this Message', 'value'=>'', 'func'=>'html_draw_sites_list', 'col'=>1, 'col_span'=>2), 
					'MOTD_DESC' => array('label'=>'Full Description', 'value'=>'', 'func'=>'html_get_motd_wysiwyg_editor', 'group'=>'default', 'col'=>1, 'col_span'=>2)
				),
				array(
					'MOTD_STARTDATE' => array('label'=>'Start Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>1), 
					'MOTD_ENDDATE' => array('label'=>'End Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>2, 'col_span'=>1), 
					'MOTD_FREQUENCY' => array('label'=>'Repeat Every', 'value'=>NULL, 'func'=>'html_motd_frequency_dd', 'params'=>'onchange=\"change_repeat(this);\" style=\"width:auto;min-width:auto\"', 'col'=>1, 'col_span'=>3), 
					'MOTD_REPEATPERIOD' => array('label'=>'Repeat Period', 'value'=>NULL, 'func'=>'html_motd_repeat_period_dd', 'params'=>'onchange=\"change_repeat(this);\"', 'col'=>3, 'col_span'=>2), 
					'MOTD_DELETE' => array('label'=>'Delete on complete', 'value'=>NULL, 'func'=>'html_draw_yes_no_number_radioset', 'col'=>1, 'col_span'=>2)
				),
				array(
					'MOTD_DATEOPTION_2' => array('label'=>'Occurs', 'value'=>NULL, 'func'=>'html_draw_motd_date_option2_dd', 'params'=>'onchange=\"calc_dates(this);\"', 'col'=>1, 'col_span'=>2), 
					'MOTD_DATEOPTION_1' => array('label'=>'Day Of Week', 'value'=>NULL, 'func'=>'html_draw_motd_date_option1_dd', 'params'=>'onchange=\"calc_dates(this);\"', 'col'=>1, 'col_span'=>2), 
					'MOTD_DATEOPTION_3' => array('label'=>'Month', 'value'=>NULL, 'func'=>'html_draw_motd_date_option3_dd', 'params'=>'onchange=\"calc_dates(this);\"', 'col'=>1, 'col_span'=>2)
				)
			);
			break;



	}
	return $ret;
}

if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);


?>