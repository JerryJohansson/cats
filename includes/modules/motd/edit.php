<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_sql.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

// init the tabs index for tab iteration
$iTabs = 0;
$iTabButtons = 0;
$RS = array();
$RECORD = array();
if($id>0){
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	// see if the user has access to this action by checking if he belongs to any one of the sites from the view
	
	if(!cats_user_is_administrator()){
		$_SESSION['messageStack']->add("You do not have access to this screen");
		include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
		exit;
	}
}


?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>

var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// resize editing section
	init_resize_editor();
	resize_editor();
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
window.onload = init;
window.onresize = function(){
	init_resize_editor();
	resize_editor();
}

function resize_editor(){
	var ed=element('MOTD_DESC___Frame');
	if(ed){
		var pos=getPos(ed);
		var name='screen_'+MODULE+"_"+(typeof(PAGE)!='undefined'?PAGE:'edit');
		var bh=top.gui.screens[name].page.offsetHeight;
		ed.style.height=(bh-(pos.y+(tb.offsetHeight+4)));
	}
}
/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);


/*
|---------------------------------------------------------
| Function: ChangeRepeat
|---------------------------------------------------------
*/
function change_repeat() 
{
	var e=document.forms.module.elements;
	var f = e["MOTD_FREQUENCY"];
	var r = e["MOTD_REPEATPERIOD"];
	// turn delete off if epeat every is valid
	if ((f.selectedIndex > 0 )||(r.selectedIndex > 0))
		e["MOTD_DELETE"][1].checked = true;
	
	// reset advanced
	var d = e["MOTD_DATEOPTION_1"];
	var w = e["MOTD_DATEOPTION_2"];
	var m = e["MOTD_DATEOPTION_3"];
	d.selectedIndex = m.selectedIndex = w.selectedIndex = 0;
}


/*
|---------------------------------------------------------
| Function: CalcDates
|---------------------------------------------------------
*/
function calc_dates() 
{
	
	var e=document.forms.module.elements;
	
	var today = new Date();
	var seeddate = 1;
	var seedmonth = (today.getMonth()+1);
	var seedyear = today.getFullYear();
	
	var d = e["MOTD_DATEOPTION_1"];
	var day = d.options[d.selectedIndex].value;
	var w = e["MOTD_DATEOPTION_2"];
	var week = w.options[w.selectedIndex].value;
	var m = e["MOTD_DATEOPTION_3"];
	var month = m.options[m.selectedIndex].value;
	
	// If month is selected
	if (month > 0)
		seedmonth = month;
	
	// if occurs is selected
	if (week > 0) 
		seeddate = ((week-1)*7)+1;
	
	var thedate = new Date(seedyear,seedmonth-1,seeddate);
	// day to start at
	var seedday = thedate.getDay();
	
	// Calcs diff in days between named days. 
	// eg between a Monday and Friday (7 - 1 + 5) = 11
	var thisday = 7 - parseInt(seedday) + parseInt(day);
	
	// if more than 7 days then reduce by seven.
	if (thisday >= 7)
		thisday = thisday - 7;
	
	thisday = thisday + seeddate;
	
	var newdate =  new Date(seedyear,seedmonth-1,thisday);
	
	e["MOTD_STARTDATE"].value = newdate.Format("yyyy-mm-dd");
	e["MOTD_STARTDATE_d"].value = newdate.Format("dd-mmm-yyyy");
	
	e["MOTD_ENDDATE"].value = newdate.Format("yyyy-mm-dd");
	e["MOTD_ENDDATE_d"].value = newdate.Format("dd-mmm-yyyy");
	
	d.value = day;
	w.value = week;
	m.value = month;
	
	// reset basic
	var f  = e["MOTD_FREQUENCY"];
	var r = e["MOTD_REPEATPERIOD"];
	f.selectedIndex = r.selectedIndex = 0; 
	 
}

function _validate_extra(){
	var e=document.forms.module.elements;
	var errors = '';
	var url = e['MOTD_URL'];
	
	
	if(empty(url.value)){
		// Get the editor instance that we want to interact with.
		var oEditor = FCKeditorAPI.GetInstance('MOTD_DESC') ;
	
		// Get the editor contents in XHTML.
		if ( oEditor.GetXHTML( false ).length == 0 ){
			errors += '- Full Description is required\n';
		}
	}else{
		var pat = /^((http:\/\/)|(https:\/\/))([A-Za-z0-9]+)((\.|-)([A-Za-z0-9]+))*\.([A-Za-z0-9]+)/;
		if((m=url.value.match(pat))==null){
			errors += '- URL is invalid\n';
		}
	}
	var f=e["MOTD_FREQUENCY"];
	var r=e["MOTD_REPEATPERIOD"];

	if((r.selectedIndex>0 && f.selectedIndex==0) || (r.selectedIndex==0 && f.selectedIndex>0)){
		errors += '- Both fields of "Repeat Every" must have a selection or both must be empty\n';
	}
	return errors;
}

function set_edit_mode(itm){
	// This is a hack for Gecko... it stops editing when the editor is hidden.
	if (itm.className == 'indent' && !document.all )
	{
		var oEditor = FCKeditorAPI.GetInstance('MOTD_DESC') ;
		if (  oEditor.EditMode == FCK_EDITMODE_WYSIWYG )
			oEditor.MakeEditable() ;
	}
}

</script>

</head><body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return CATS_validateForm(this, 'MOTD_STARTDATE','','R','MOTD_ENDDATE','','R','MOTD_SHORT','','R','MOTD_SITE','','R');">
<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="tab_onclick(this);set_edit_mode(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<a
	title="Edit Basic Message Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);">Basic Details</a>
<a
	title="Edit Advanced Message Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);">Advanced Details</a>
</fieldset>
<fieldset class="tool_bar">
<a
	title="Refresh editor"
	href="javascript:set_edit_mode(this);" 
	class="indent"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Edit Mode</a>
<?php if($p=='edit'){ ?>
<a
	title="Create New Message of the Day"
	href="javascript:_m.newModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->
<div id="Lcontentbody">
<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php
$ed=new Editor($m,$p,$a);

if($id>0){
	echo($ed->buildForm($FIELDS[0],$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
}else{
	echo($ed->buildForm($FIELDS[0]));
}
?>
	<fieldset class="tbar" style="text-align:right; ">
		<input type="submit" class="submit" name="cats::Save" value="Save">
    <?php if($p=='edit'){ ?>
		<input type="button" class="button" name="cats::New" value="New Message" onClick="_m.newModule();" >
    <?php } ?>
		<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
	</fieldset>
</fieldset>


<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<?php
if($id>0){
	echo($ed->buildForm($FIELDS[1],$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
}else{
	echo($ed->buildForm($FIELDS[1]));
}
?>
<fieldset class="tbar" style="text-align:right; ">
	<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<?php
if($id>0){
	echo($ed->buildForm($FIELDS[2],$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
}else{
	echo($ed->buildForm($FIELDS[2]));
}
?>
<fieldset class="tbar" style="text-align:right; ">
	<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
</div>
</form>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
