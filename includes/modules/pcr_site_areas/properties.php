<?php
/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
*/
function getPropertiesGeneral($page = 'search'){
	$ret = array();
	switch($page){
		case 'edit':
			$ret = array_merge(
				array(
					'name'=>'Edit PCR Site Areas',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge(
				array(
					'name'=>'New PCR Site Area',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge(
				array(
					'name'=>'View PCR Site Area',
					'filter'=>" 1=2 "
				)
			);
			break;
		default: // case 'search':
			$ret = array_merge(
				array(
					'name'=>'Search Site Areas',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}


/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
*/
function getPropertiesTables(){
	$ret = array(
		'table'=> 'TBLPCR_AREA',
		'id'=> 'AREA_ID',
		'view'=> 'VIEW_PCR_AREA'
	);
	return $ret;
}

function getPropertiesFields($page='search'){
	switch($page){
		case 'new':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2),  
				'AREA_DESCRIPTION' => array('label'=>'Area Description', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'edit':
			$ret = array(
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'AREA_DESCRIPTION' => array('label'=>'Area Description', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'view':
			$ret = array(
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'AREA_DESCRIPTION' => array('label'=>'Area Description', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'search': case 'results':
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_filtered_site_dd', 'col'=>1, 'col_span'=>2)
			);
			break;
		default:
			$_SESSION['messageStack']->add("Page parameter is not defined.");
			$_SESSION['messageStack']->add("Please notify Tiwest IT department of this message.",'warning');
			include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
			break;
	}
	return $ret;
}


if($p=='post'){
	$pg=(isset($a) && $a=='add')?'new':'edit';
}else{
	$pg=$p;
}

$GENERAL=getPropertiesGeneral($pg);
$TABLES=getPropertiesTables($pg);
$FIELDS=getPropertiesFields($pg);
?>