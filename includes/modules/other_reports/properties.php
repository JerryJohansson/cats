<?php

/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search'){
	$ret = array();
	switch($action){
		case 'search':
		case 'results':			
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Other Records',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Other Records',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Other Records',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}



/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables()
{
	return array(
		'table'=>'TBLOTHER_REPORTS', 
		'id'=>'REPORT_ID',
		'view'=> 'VIEW_OTHER_REPORTS'
	);
}


/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{
	$ret = array();
	global $id;
	switch($action)
	{
		case 'results':
		case 'search': // search
			$ret = array(
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>1), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>2, 'col_span'=>1), 
				'RECORD_TYPE' => array('label'=>'Record Type', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_reftable_other_reports_dd', 'col'=>1, 'col_span'=>2), 				
				'REPORTED_BY_ID' => array('label'=>'Reported By', 'value'=>'', 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2), 
				'DATE_COMPLETED_FROM' => array('label'=>'Date From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>'', 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'DATE_COMPLETED_TO' => array('label'=>'Date To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>'', 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			);
			break;	
	    case 'new':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Record Type', 'value'=>'', 'func'=>'html_form_draw_reftable_other_reports_dd', 'col'=>1, 'col_span'=>2), 
				'REGISTER_ORIGIN' => array('label'=>'Register Origin', 'value'=>'Audit', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>50), 
				'DATE_COMPLETED' => array('label'=>'Date Completed', 'value'=>'', 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'REPORTED_BY_ID' => array('label'=>'Reported By', 'value'=>'', 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2), 	
				'REPORT_NOTES' => array('label'=>'Report Notes', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 	
				'LOCATION_DOCS' => array('label'=>'Document Location', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 		
				'LOCATION_PHOTO' => array('label'=>'Photo Location', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 		
				'COMMENTS' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2)											
			);
		break;
		case 'edit':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Record Type', 'value'=>'', 'func'=>'html_form_draw_reftable_other_reports_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_COMPLETED' => array('label'=>'Date Completed', 'value'=>'', 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2),
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'REPORTED_BY_ID' => array('label'=>'Reported By', 'value'=>'', 'func'=>'html_draw_employee_helper_search', 'col'=>1, 'col_span'=>2), 	
				'REPORT_NOTES' => array('label'=>'Report Notes', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 	
				'LOCATION_DOCS' => array('label'=>'Document Location', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2), 		
				'COMMENTS' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2),
				'ACTIONS_CREATED' => array('label'=>'No. of Actions Created', 'value'=>'', 'func'=>'html_display_actions_created', 'params2'=> $id . ',"Originating_Other"', 'col'=>1, 'col_span'=>2),
				'ACTIONS_CLOSED' => array('label'=>'No. of Actions Close', 'value'=>'', 'func'=>'html_display_actions_closed', 'params2'=> $id .',"Originating_Other"', 'col'=>1, 'col_span'=>2)
			);
		break;
		case 'view':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Record Type', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'DATE_COMPLETED' => array('label'=>'Date Completed', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'REPORTED_BY' => array('label'=>'Reported By', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 	
				'REPORT_NOTES' => array('label'=>'Report Notes', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 	
				'LOCATION_DOCS' => array('label'=>'Document Location', 'value'=>'', 'func'=>'html_get_file_location_value', 'col'=>1, 'col_span'=>2), 		
				'COMMENTS' => array('label'=>'Comments', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'ACTIONS_CREATED' => array('label'=>'No. of Actions Created', 'value'=>'', 'func'=>'html_display_actions_created', 'params2'=> $id . ',"Originating_Other"', 'col'=>1, 'col_span'=>2),
				'ACTIONS_CLOSED' => array('label'=>'No. of Actions Close', 'value'=>'', 'func'=>'html_display_actions_closed', 'params2'=> $id .',"Originating_Other"', 'col'=>1, 'col_span'=>2)
			);
		break;
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'RECORD_TYPE',"text" => 'RECORD_TYPE');
	$ret[] = array("id" => 'DATE_COMPLETED',"text" => 'DATE_COMPLETED');
	$ret[] = array("id" => 'SITE_DESCRIPTION',"text" => 'SITE_DESCRIPTION');
	$ret[] = array("id" => 'DEPARTMENT_DESCRIPTION',"text" => 'DEPARTMENT_DESCRIPTION');
	$ret[] = array("id" => 'REPORTED_BY',"text" => 'REPORTED_BY');
	$ret[] = array("id" => 'LOCATION_DOCS',"text" => 'LOCATION_DOCS');
	return $ret;
}


// Fix page parameter
if($p=='post')
	$pg=(isset($a) && $a=='add')?'new':'edit';
else
    $pg=$p;


// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($pg);
$TABLES  = getPropertiesTables($pg);
$FIELDS  = getPropertiesFields($pg);

?>