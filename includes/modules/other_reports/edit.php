<?PHP
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once(CATS_FUNCTIONS_PATH. 'dave_sql.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;


// init the tabs and tab buttons index for tab iteration
$iTabs = 0;
$iTabButtons=0;
$RS = array();

// Security Check
if(!cats_user_is_editor()  && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New Other Record");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}
?>
<link rel="stylesheet" href="<?php echo(WS_STYLE_PATH);?>forms.css" type="text/css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/validation_helpers.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// resize editing section
	init_resize_editor();
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
onload = function(){
	init();
}
window.onresize = init_resize_editor;
/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);



/*
|--------------------------------------
| ValidateForm
|--------------------------------------
| Parameters: 
|           :- form: HTML form
|
| Returns:
|        :- boolean result
|--------------------------------------
*/
function ValidateForm(form)
{


    // initialize global values
    errorFlag = true;    
    messageString = 'The following fields must be entered\n\n';
	
	// form details
    RECORD_TYPE         = form.RECORD_TYPE.value;
    DATE_COMPLETED      = form.DATE_COMPLETED_d.value;
	SITE_ID             = form.SITE_ID.value;
	DEPARTMENT_ID       = form.DEPARTMENT_ID.value;
	REPORTED_BY         = form.REPORTED_BY.value;	
	LOCATION_DOCS       = form.LOCATION_DOCS.value;

    checkRequiredField(RECORD_TYPE,         1, 'Record Type missing \n');
    checkRequiredField(DATE_COMPLETED,      1, 'Date Completed missing \n');
    checkRequiredField(SITE_ID,             1, 'Site missing \n');
    checkRequiredField(DEPARTMENT_ID,       1, 'Department missing \n');
    checkRequiredField(REPORTED_BY,         1, 'Location Of Inspection missing \n');
    checkRequiredField(LOCATION_DOCS,       1, 'Document Location missing \n');
    
    // display the message if there were problems with the form
    if (errorFlag == false)
        alert(messageString)
		 
	return errorFlag;
}


</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return CATS_validateForm(this, 'RECORD_TYPE','','R','DATE_COMPLETED','','R','SITE_ID','','R','DEPARTMENT_ID','','R','REPORTED_BY','','R','LOCATION_DOCS','','R','LOCATION_DOCS','','filePath');">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?php echo ($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<a
	title="View Follow-up Actions"
	id="tab_button[<?php echo ($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/followup.gif);">Follow-up Actions</a>
</fieldset>
<fieldset class="tool_bar">
<?php if($p=="edit"){ ?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php } ?>
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php }elseif(cats_user_is_editor(true)){ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Print current <?PHP echo $module_name_txt;?> item"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>	
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>		
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed = new Editor("edit");

// Check whether to edit or add a new record
if($id > 0)
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id));
else
	echo($ed->buildForm($FIELDS));

if($id < 0)
{
?>
    <table cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1;">
      <tr id="row_created_actions">
        <td class="label"><label title="Name: Sites">Actions Created: </label></td>
        <td colspan="3"><?PHP echo(Get_Created_Actions($id, "Originating_Other")); ?></td>
      </tr>
      <tr id="row_open_actions">
        <td class="label"><label title="Name: Sites">Actions Closed: </label></td>
        <td colspan="3"><?PHP echo(Get_Closed_Actions($id, "Originating_Other")); ?></td>
      </tr>	  
    </table>
<?PHP
}
?>
<fieldset class="tbar" style="text-align:right; ">
<?php if($p!="view"){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onClick="preSubmitForm()">
<?php }elseif(cats_user_is_editor(true) || cats_user_is_super_administrator()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
<?php if($p=="edit"){ ?>
<input type="button" class="button" name="cats::New" value="New <?php echo $module_name_txt;?>" onClick="_m.newModule();" />
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();">
</fieldset>		
</fieldset>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();"><?php echo $GENERAL['name'];?></a></caption>
<tr>
	<td>
<?php

// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","","");

// Follow-up Actions
$action_edit_button = "edit:javascript:top.show_edit_screen(\"actions\",\"index.php?m=actions&p=edit&id=:ACTION_ID:\")";
$col_attributes = array('','','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('ID','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date',$action_edit_button);
$sql = "SELECT ACTION_ID, Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date, '$action_edit_button' as edit  FROM VIEW_ACTION_DETAILS  WHERE Report_Id = $id  AND Origin_Table='Originating_Other'  ORDER BY Action_Title ASC";
//db_render_pager($sql,$col_headers,$col_attributes,'',false,'','',1000);
cats_form_pager($sql,$col_headers,$col_attributes,'', 'actions', '', 'ASC', $col_links, false, 1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<?php if($p=='edit' || cats_user_is_administrator() || cats_user_is_action_administrator()){?>
<input type="button" class="button" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php } ?>
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
</div>
</form>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
<script language="javascript">
 
	function preSubmitForm()
	{
		if($("#DATE_COMPLETED_d").val() != "")
			setHiddenDate("DATE_COMPLETED");

	}

</script>
