<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";

$where = "";
$multi_sites = $_SESSION['user_details']['site_access'];
$users_site  = $_SESSION['user_Details']['site_id'];


if(is_array($_SESSION['user_details']) && $multi_sites!='')
	$where = $multi_sites;
else
	$where = $users_site;

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST) > 0)
	$_SESSION[$m.'_'.$p] = $_POST;
else if(isset($_SESSION[$m.'_'.$p]))
	$_POST=$_SESSION[$m.'_'.$p];


// If the post is equal to search then perform the following
if(isset($_POST['cats::search']))
{
	$edit_button     = "edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:{$TABLES['id']}:\")";
	$delete_button   = "delete:::{$TABLES['id']}:";//:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=post&a=del&id=:{$TABLES['id']}:\")";
	
	
	/*
	|------------------------------------------------------------
	| Description: Modify colattribs alongside colarr
	|              these are passed into the db_render_page control
	|------------------------------------------------------------
	*/
	$col_attributes  = array(' width="40" ',' width="" ',' width="120" ',' width="150" ',' width="150" ',' width="150" ',' width="20" ');
	$col_headers     = explode("|","ID|Type|Date_Completed|Site_Description|Department|Reported By|Documents|$edit_button|$delete_button");
	$sql             = "SELECT DISTINCT Report_Id, Record_Type, Date_Completed, Site_Description, Department_Description, Reported_by, Location_Docs, '$edit_button' as edit, '$delete_button' as del  FROM {$TABLES['view']} ";

	$columns = explode("|","Record_Type|',none,''|Date_Completed_From|',none,NULL,>=|Date_Completed_To|',none,NULL,<=|Site_Id|none,none,NULL|Department_Id|none,none,NULL|Reported_by|',none,''");
	$fields = array();
	$order_by_col = "Record_Type";

	$sql_where = "";
	$sql_order_by = "";
	
	// create the FormFields and TableFields arrays
	$x = count($columns);
	

	// Cycle the columns / field headers to create the SELECT string
	// Create WHERE String and append it to the SELECT String
	for ($i=0; $i < $x; $i+=2)
	{
		$types = explode(",",$columns[$i+1]);
		$delim =  ($types[0] != "none") ? $types[0] : "";
		$operator = ($types[3] == ">=" || $types[3] == "<=") ? $types[3] : " = ";
		$column_name=strtoupper($columns[$i]);
		
		//add the users sites to the where string
		if($column_name == "SITE_ID")
		{
			if(!isset($column_name))
			{
				if($sql_where == "")  
					$sql_where = " WHERE " . $where;
				else
					$sql_where .= " AND " . $where;
				
			}
		}
		
		switch($column_name){
			case 'DATE_COMPLETED_FROM': //case 'CLOSING_DATE_FROM':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-5);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			case 'DATE_COMPLETED_TO': //case 'CLOSING_DATE_TO':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-3);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			default:
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					if($operator=="LIKE"){
						$sql_where .=  $column_name . $operator . "'%" . $_POST[$column_name] . "%'"; 
					}else{
						$sql_where .=  $column_name . $operator . $delim . $_POST[$column_name] . $delim; 
					}
				}
				break;
		}
	}
	$search_sql = $sql . $sql_where;
	
	$db->debug = false;
	if($db->debug == true)
	{
		print_r($_POST);
		echo('<br>'.$search_sql);
	}
}

?>
<script>
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	// resize result iframe to consume the parent page
	init_resize_results_container();
	return true;
}
window.onload = init;
/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
//------------------------------

</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><form name="results" action="index.php" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr valign="top">
	<td width="100%">
		
<!-- BEGIN:: Results Table -->	
<?php
// Search Results
if(isset($search_sql))
{
    db_render_pager($search_sql,$col_headers,$col_attributes,$GENERAL['results'], true, $order_by_col, 'ASC');
}
else
	echo("<h2>No Search results</h2>");
?>
<!-- END:: Results table -->
	</td>
</tr>
</table>
</form>