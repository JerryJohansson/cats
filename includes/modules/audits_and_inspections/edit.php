<?php
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');


$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
$action = isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

// Security Check
if((!cats_user_is_editor(true) && !cats_user_is_editor(false) && !cats_user_is_incident_analysis() && !cats_user_is_super_administrator()) && ($p=='new')){
	$_SESSION['messageStack']->add("You do not have access to Create a New Audit or Inspection");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}

if(!cats_user_is_reader(true) && !cats_user_is_editor(true) && !cats_user_is_editor(false) && !cats_user_is_incident_analysis() && !cats_user_is_super_administrator()){
	$_SESSION['messageStack']->add("You do not have access to Audit or Inspection");
	include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
	exit;
}


if($action=='edit'){
	if($id==0){
		$_SESSION['messageStack']->add("The ID was missing from the request. The ID of the record must be greater than 0.");
		include(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
		exit;
	}
	
	$sql = "SELECT * FROM {$TABLES['view']} WHERE {$TABLES['id']} = $id";
	$RS[$id] = db_query($sql);
	$RECORD = db_get_array($RS[$id]);
	
	/*
	Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
	If it doesn't then we kick them out.!
	*/
	//print($RECORD['SITE_ID']);
	if($p=='edit'){
		$user_access = cats_check_user_site_access($RECORD['SITE_ID']);
		
		if($user_access == false){
			$_SESSION['messageStack']->add("You do not have access to this actions details");
			include(CATS_INCLUDE_PATH . 'access_denied.inc.php');
			exit;
		}
	}
}
// init the tabs and tab buttons index for tab iteration
$iTabs = 0;
$iTabButtons=0;
$RS = array();
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script language="JavaScript" src="js/calendarwin8Fix.js"></script>
<script>
var MODULE = "<?php echo $m;?>";
function init(){
	// initialise the tabbing object
	init_tabs();
	// resize editing section
	init_resize_editor();
	// create document.mousedown handlers
	init_document_handlers();
	return true;
}
onload = function(){
	init();
}
window.onresize = init_resize_editor;

/*******************************
+ Editing functions
1. Create new module
2. Save current module
3. Cancel and return to search screen
4. Delete current record shown by this module
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
</script>

</head>
<body class="edit">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>&id=<?PHP echo $id; ?>" method="POST" enctype="multipart/form-data" 
	onSubmit="return CATS_validateForm(this, 'RECORD_TYPE','','R','DATE_INSPECTION','','R','SITE_ID','','R','DEPARTMENT_ID','','R','LOCATION_DOCS','','filePath');">

<!-- BEGIN:: Of Toolbar Buttons -->
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?php echo ($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/edit.gif);"><?PHP echo $module_name_txt;?> Details</a>
<?php if($action=='edit'){ ?>
<a
	title="View Follow-up actions for this record"
	id="tab_button[<?php echo ($iTabButtons++);?>]" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/followup.gif);">Follow-up Actions</a>	
<?php } ?>
</fieldset>
<fieldset class="tool_bar">
<?php if($p=='edit'){ ?>
<a
	title="New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php } ?>
<?php if($p!='view'){ ?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:preSubmitForm(); _m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<?php }elseif(cats_user_is_editor(true)){ ?>
<a
	title="Edit <?PHP echo $module_name_txt;?>"
	href="javascript:_m.edit();" 
	class="main_edit"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/edit.gif);">Edit <?PHP echo $module_name_txt;?></a>
<?php } ?>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Print current <?PHP echo $module_name_txt;?> item"
	href="javascript:printPage();" 
	class="main_print"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/print.gif);">Print</a>	
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>
<!-- END:: Of Toolbar Buttons -->

<div id="Lcontentbody">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<?php

// Instantiate a new editor
$ed = new Editor("edit");

// Check whether to edit or add a new record
if($id > 0)
{
	//comment by dev
	// echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,$RS[$id]));
	echo($ed->buildForm($FIELDS,$TABLES['view'],$TABLES['id'],$id,(isset($RS[$id]))?$RS[$id]:false));

}
else
	echo($ed->buildForm($FIELDS));
?>
<fieldset class="tbar_btns" style="text-align:right; ">
<?php if($p!='view'){ ?>
<input type="submit" class="submit" name="cats::Save" value="Save" onclick="preSubmitForm();"/>
<?php }elseif(cats_user_is_editor(true) || cats_user_is_super_administrator()){ ?>
<input type="button" class="button" name="cats::Edit" value="Edit" onClick="_m.edit();" >
<?php } ?>
<?php if($p=='edit'){ ?>
<input type="button" class="button" name="cats::New" value="New Audit and Inspection" onClick="_m.newModule();" />
<?php } ?>
<input type="button" class="button" name="cats::Cancel" value="Cancel" onClick="_m.cancel();" />
</fieldset>		
</fieldset>
<?php if($action=='edit'){ ?>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();">Actions Created from this Audit or Inspection</a></caption>
<tr>
	<td>
<?php
// Follow-up actions for record
$sql = "SELECT ACTION_ID, Action_Title, Managed_By, Allocated_To, Status, Scheduled_Date, Closing_Date";
$col_attributes = array('','','',' width="40" ',' width="40" ',' width="40" ',' width="20" ');
$col_headers=array('','Action Title','Managed By','Allocated To','Status','Scheduled Date','Closing Date');
if(cats_user_is_editor(true)) {
	// edit and delete strings used in the sql query and col_headers array to display the edit buttons and action checkboxes for each row
	$actions_edit_button = 'edit:javascript:top.show_edit_screen("actions","index.php?m=actions&p=edit&id=:ACTION_ID:")';
	// put it into an array so we can merge with column headers later
	$col_headers[] = $actions_edit_button;
	$sql .= ", '$actions_edit_button' as edit ";
}
// specify columns to ignore when drawing the table
$col_ignore = array('ACTION_ID');
// specify links for column values on each row
$col_links = array("","js|top.view('actions',|ACTION_ID","","","","");

$sql .= " FROM View_Action_Details  WHERE Report_Id = $id  AND Origin_Table='Originating_Audit'  ORDER BY ACTION_TITLE ASC";
//db_render_pager($sql,$colarr,$colattribs,'',false,'','',1000);
//comment by dev
// cats_form_pager($sql,$col_headers,$col_attributes,$GENERAL['results'], '', '', '', $col_links, $col_ignore, 1000);
cats_form_pager($sql,$col_headers,$col_attributes,isset($GENERAL['results'])?$GENERAL['results']:'', '', '', '', $col_links, $col_ignore, 1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar_btns" style="text-align:right; ">
<?php if($p=='edit'){?>
<input type="button" class="button" name="cats::new_action" value="New Action" onClick="_m.newAction();" >
<?php } ?>
<input type="button" class="button" name="cats::SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<?php } ?>
</div>
</form>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>

<script language="javascript">

	function preSubmitForm()
	{
		if($("#DATE_INSPECTION_d").val() != "")
			setHiddenDate("DATE_INSPECTION");
	}

</script>