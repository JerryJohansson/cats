<?php


/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search')
{
	$ret = array();
	switch($action){
		case 'search':
		case 'results':
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Audits and Inspections',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New Audits and Inspections',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'Edit Audits and Inspections',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'view':
			$ret = array_merge($ret, 
				array(
					'name'=>'Audits and Inspections',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}






/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables()
{
	return array(
		'table'=>'TBLAUDITS', 
		'id'=>'REPORT_ID',
		'view'=> 'VIEW_AUDITS'
	);
}


/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{
	$ret = array();
	global $id;
	switch($action)
	{	
		case 'results':
		case 'search':  // search
			$ret = array(
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'RECORD_TYPE' => array('label'=>'Audit or Inspection Type', 'delim'=>"'", 'value'=>'', 'func'=>'html_form_draw_audit_type_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_INSPECTION_FROM' => array('label'=>'Date Inspection From', 'delim'=>"'", 'operator'=>'DATE_FROM', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>1), 
				'DATE_INSPECTION_TO' => array('label'=>'Date Inspection To', 'delim'=>"'", 'operator'=>'DATE_TO', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>2, 'col_span'=>1)
			);
			break;	
	  case 'new':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Audit or Inspection Type', 'value'=>NULL, 'func'=>'html_form_draw_audit_type_dd', 'col'=>1, 'col_span'=>2), 
				'REGISTER_ORIGIN' => array('label'=>'Register Origin', 'value'=>'Audit', 'func'=>'html_draw_hidden_field', 'col'=>0, 'col_span'=>1, 'maxlength'=>50), 
				'DATE_INSPECTION' => array('label'=>'Inspection Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'AUDIT_NOTES' => array('label'=>'Audit Notes', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'LOCATION_DOCS' => array('label'=>'Location Of Documents', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2)
			);
			break;
		case 'edit':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Audit or Inspection Type', 'value'=>NULL, 'func'=>'html_form_draw_audit_type_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_INSPECTION' => array('label'=>'Inspection Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field_notime_ver2', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd_linked', 'params2'=>'"DEPARTMENT_ID"', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'AUDIT_NOTES' => array('label'=>'Audit Notes', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'LOCATION_DOCS' => array('label'=>'Location Of Documents', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2)	,						
				'ACTIONS_CREATED' => array('label'=>'No. of Actions Created', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>1),
				'ACTIONS_CLOSED' => array('label'=>'No. of Actions Closed', 'value'=>'', 'func'=>'html_display_value', 'col'=>2, 'col_span'=>1)
			);
			break;
		case 'view':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Audit or Inspection Type', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'DATE_INSPECTION' => array('label'=>'Inspection Date', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'SITE_DESCRIPTION' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_DESCRIPTION' => array('label'=>'Department', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'AUDIT_NOTES' => array('label'=>'Audit Notes', 'value'=>NULL, 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2), 
				'LOCATION_DOCS' => array('label'=>'Location Of Documents', 'value'=>'', 'func'=>'html_get_file_location_value', 'col'=>1, 'col_span'=>2)	,						
				'ACTIONS_CREATED' => array('label'=>'No. of Actions Created', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2),
				'ACTIONS_CLOSED' => array('label'=>'No. of Actions Closed', 'value'=>'', 'func'=>'html_display_value', 'col'=>1, 'col_span'=>2)
			);
			break;

	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|  Get Default Fields for Export
+---------------------------------------------------------------------------
*/
function getDefaultExportFields(){
	$ret = array();
	$ret[] = array("id" => 'REPORT_ID',"text" => 'REPORT_ID');
	$ret[] = array("id" => 'RECORD_TYPE',"text" => 'RECORD_TYPE');
	$ret[] = array("id" => 'DATE_INSPECTION',"text" => 'DATE_INSPECTION');
	$ret[] = array("id" => 'SITE_DESCRIPTION',"text" => 'SITE_DESCRIPTION');
	$ret[] = array("id" => 'DEPARTMENT_DESCRIPTION',"text" => 'DEPARTMENT_DESCRIPTION');
	$ret[] = array("id" => 'LOCATION_DOCS',"text" => 'LOCATION_DOCS');
	return $ret;
}


// Fix page parameter
if($p=='post')
	$pg=(isset($a) && $a=='add')?'new':'edit';
else
	$pg=$p;

// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($pg);
$TABLES  = getPropertiesTables($pg);
$FIELDS  = getPropertiesFields($pg);


?>