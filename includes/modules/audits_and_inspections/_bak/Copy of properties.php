<?php



/*
+---------------------------------------------------------------------------
|  Get General Properties
+---------------------------------------------------------------------------
|  Description: Retrieves general properties regarding the page
|               :- name - contains the title to display on the button or within the page
|               :- filter - contains the value to filter for by default
|---------------------------------------------------------------------------
*/

function getPropertiesGeneral($action = 'search')
{
	$ret = array();
	switch($action){
		case 'search':
			$ret = array_merge($ret, 
				array(
					'name'=>'Search Audits and Inspections',
					'results'=>'Search Results',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'new':
			$ret = array_merge($ret, 
				array(
					'name'=>'New AUDITS AND INSPECTIONS',
					'filter'=>" 1=2 "
				)
			);
			break;
		case 'edit':
			$ret = array_merge($ret, 
				array(
					'name'=>'EDIT AUDITS AND INSPECTIONS',
					'filter'=>" 1=2 "
				)
			);
			break;
	}
	return $ret;
}






/*
+---------------------------------------------------------------------------
|  Get Table Properties
+---------------------------------------------------------------------------
|  Description: Retrieves an array with recrods
|               :- Edit - stores the name of the Table that is being updated and the key value to update
|               :- Search - stores the name of the view to query 
|               :- Edit_View - stores the name of the view to query
|---------------------------------------------------------------------------
*/

function getPropertiesTables()
{
	return array(
		'edit'=> array('name'=>'TBLAUDITS', 'id'=>'REPORT_ID'),
		'search'=> array('name'=>'VIEW_AUDITS','id'=>'REPORT_ID'),
		'edit_view'=> array('name'=>'VIEW_AUDITS','id'=>'REPORT_ID')
	);
}







/*
+---------------------------------------------------------------------------
|  Get Field Properties
+---------------------------------------------------------------------------
|  Description: Retrieve the field properties of the module
|               :- Search: Return the array for the Search fields
|               :- Edit or New: Return the array for the Edit or New fields
|---------------------------------------------------------------------------
*/

function getPropertiesFields($action='search')
{
	$ret = array();


	switch($action)
	{	
	
		case 'search': // search
			$ret = array(
				'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'TYPE' => array('label'=>'Audit or Inspection Type', 'value'=>'', 'func'=>'html_form_draw_inspection_type_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_TO' => array('label'=>'Date To', 'value'=>'', 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2)
			);
			break;	
	    case 'new':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Audit or Inspection Type', 'value'=>NULL, 'func'=>'html_form_draw_reference_table_audit_inspection_types_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_INSPECTION' => array('label'=>'Inspection Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'AUDIT_NOTES' => array('label'=>'Audit Notes', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'LOCATION_DOCS' => array('label'=>'Location Of Documents', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2)
			);
		break;
		case 'edit':
			$ret =  array(
				'RECORD_TYPE' => array('label'=>'Audit or Inspection Type', 'value'=>NULL, 'func'=>'html_form_draw_reference_table_audit_inspection_types_dd', 'col'=>1, 'col_span'=>2), 
				'DATE_INSPECTION' => array('label'=>'Inspection Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
				'SITE_ID' => array('label'=>'Site', 'value'=>NULL, 'func'=>'html_form_draw_site_dd', 'col'=>1, 'col_span'=>2), 
				'DEPARTMENT_ID' => array('label'=>'Department', 'value'=>'', 'func'=>'html_form_draw_department_dd', 'col'=>1, 'col_span'=>2), 
				'AUDIT_NOTES' => array('label'=>'Audit Notes', 'value'=>NULL, 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
				'LOCATION_DOCS' => array('label'=>'Location Of Documents', 'value'=>'', 'func'=>'html_get_file_location_field', 'col'=>1, 'col_span'=>2)							
			);
		break;

	}
	return $ret;
}


// Call the functions and store the results in the following variables
$GENERAL = getPropertiesGeneral($p);
$TABLES  = getPropertiesTables($p);
$FIELDS  = getPropertiesFields($p);

?>