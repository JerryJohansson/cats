<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

$FIELDS = getPropertiesFields($p);

$where = "";
$multi_sites = $_SESSION['user_details']['site_access'];
$users_site = $_SESSION['user_Details']['site_id'];


if(is_array($_SESSION['user_details']) && $multi_sites!='')
	$where = $multi_sites;
else
	$where = $users_site;


// Create the SELECT String
$sql = "SELECT DISTINCT Report_Id, Record_Type, Date_Inspection, Site_Description, Department_Description, Location_Docs, 'edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:".$TABLES[$p]['id'].":\")' as edit  FROM " . $TABLES[$p]['name'] . " ";

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST) > 0)
	$_SESSION[$m.'_'.$p] = $_POST;
else if(isset($_SESSION[$m.'_'.$p]))
	$_POST=$_SESSION[$m.'_'.$p];


// If the post is equal to search then perform the following
if(isset($_POST['cats::search']))
{
	$columns = explode("|","Record_Type|',none,''|Date_Inspection|',none,NULL,>=|Date_Inspection|',none,NULL,<=|Site_Id|none,none,NULL|Department_Id|',none,''");
	$fields = array();
	$order_by_col = "Record_Type ASC";


	$sql_where = "";
	$sql_order_by = "";
	
	// create the FormFields and TableFields arrays
	$x=count($columns);
	
	
	// Cycle the columns / field headers to create the SELECT string
	// Create WHERE String and append it to the SELECT String
	for ($i=0; $i < $x; $i+=2)
	{
		$types = explode(",",$columns[$i+1]);
		$delim =  ($types[0] != "none") ? $types[0] : "";
		$operator = ($types[3] == ">=" || $types[3] == "<=") ? $types[3] : " = ";
		$column_name=strtoupper($columns[$i]);
		
		//add the users sites to the where string
		if($column_name == "SITE_ID")
		{
			if(!isset($column_name))
			{
				if($sql_where == "")  
					$sql_where = " WHERE " . $where;
				else
					$sql_where .= " AND " . $where;
				
			}
		}
		
		switch($column_name){
			case 'DATE_COMPLETED_FROM': //case 'CLOSING_DATE_FROM':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-5);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			case 'DATE_COMPLETED_TO': //case 'CLOSING_DATE_TO':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-3);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			default:
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					if($operator=="LIKE"){
						$sql_where .=  $column_name . $operator . "'%" . $_POST[$column_name] . "%'"; 
					}else{
						$sql_where .=  $column_name . $operator . $delim . $_POST[$column_name] . $delim; 
					}
				}
				break;
		}
	}
	$search_sql = $sql . $sql_where;
	
	$search_display='none';
	$results_display='block';
	$show_search_results = true;

}
else
{
	$search_display='block';
	$results_display='none';
	$search_sql = $sql . " WHERE 1=2";
	$show_search_results = false;

}

$db->debug = false;
if($db->debug == true)
{
	print_r($_POST);
	echo('<br>'.$search_sql);
}


?>
<link rel="stylesheet" href="<?php echo WS_STYLE_PATH;?>forms.css" type="text/css" />
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>

document.onmousedown = function(evt){
	if(top.hideMenu) top.hideMenu();
	if(_hide_calendar) _hide_calendar();
}

function init(){
	// start up script for each page
	//
	// Execute some start up script
	return true;
}

function pager(q){
	location.href = "index.php?m=<?php echo $m;?>&p=search&"+q;
}


function newAuditInspection()
{
	top.show_edit_screen("<?php echo $m;?>","index.php?m=<?php echo $m;?>&p=new&a=add");
}

function doCancel(){
	top.doMenu('index','dashboard');
}
</script>
</head>
<body>
<?PHP $iTabs=0; ?>
<form action="index.php?m=<?php echo $m;?>&p=search" method="post">
<table width="100%">
    <tr valign="top">
	    <td width="100%">		
		<!-- Actions Search Table -->
        <fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
        <table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
        <caption id="tog"><a name="todo" style="cursor:pointer;" onClick="var e=element('search_form');e.style.display=(e.style.display=='none')?'block':'none';">
<?php 

echo $GENERAL['name'];
if($show_search_results)
{
	echo ' (Click here to show Search Parameters)';
}

?></a></caption>
<tr>
	<td>
		<div  id="search_form" style="display:<?php echo $search_display;?>">
<?php

$ed = new Editor("edit","admin");

if($id > 0)
	echo($ed->buildForm($FIELDS,$TABLES['search']['name'],$TABLES['search']['id'],$id));
else
	echo($ed->buildForm($FIELDS));


?>

<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="cats::search" value="Search">
<input type="button" name="cats::new" value="New Audit or Inspection" onClick="newAuditInspection()">
<input type="button" name="Cancel" value="Cancel" onClick="doCancel();">
</fieldset>
</div>
	</td>
</tr>
</table>

<?php
if($show_search_results){
?>
<!-- Results Table -->
		

<?php
// Search Results
// define table headers
//$db->debug=true;



/*
|------------------------------------------------------------
| Description: Modify colattribs alongside colarr
|              these are passed into the db_render_page control
|------------------------------------------------------------
*/
$colattribs = array(' width="40" ',' width="200" ',' width="120" ',' width="100" ',' width="" ',' width="150" ',' width="20" ');
$colarr = explode("|","ID|Type|Date|Site|Department|Location|edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:".$TABLES[$p]['id'].":\")");
db_render_pager($search_sql,$colarr,$colattribs,$GENERAL['results']);


?>
</fieldset>
<?php
}
?>
	</td>
</tr>
</table>
</form>