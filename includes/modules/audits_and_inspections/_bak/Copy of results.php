<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('properties.php');

/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";


$where = "";
$multi_sites = $_SESSION['user_details']['site_access'];
$users_site = $_SESSION['user_Details']['site_id'];


if(is_array($_SESSION['user_details']) && $multi_sites!='')
	$where = $multi_sites;
else
	$where = $users_site;


// Create the SELECT String
$sql = "SELECT DISTINCT Report_Id, Record_Type, Date_Inspection, Site_Description, Department_Description, Location_Docs, 'edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:".$TABLES[$p]['id'].":\")' as edit  FROM " . $TABLES[$p]['name'] . " ";

// set session var when there is no post
// this is used to store the search params while scrolling through recordset
if(count($_POST) > 0)
	$_SESSION[$m.'_'.$p] = $_POST;
else if(isset($_SESSION[$m.'_'.$p]))
	$_POST=$_SESSION[$m.'_'.$p];


// If the post is equal to search then perform the following
if(isset($_POST['cats::search']))
{
	$columns = explode("|","Record_Type|',none,''|Date_Inspection|',none,NULL,>=|Date_Inspection|',none,NULL,<=|Site_Id|none,none,NULL|Department_Id|',none,''");
	$fields = array();
	$order_by_col = "Record_Type ASC";


	$sql_where = "";
	$sql_order_by = "";
	
	// create the FormFields and TableFields arrays
	$x=count($columns);
	
	
	// Cycle the columns / field headers to create the SELECT string
	// Create WHERE String and append it to the SELECT String
	for ($i=0; $i < $x; $i+=2)
	{
		$types = explode(",",$columns[$i+1]);
		$delim =  ($types[0] != "none") ? $types[0] : "";
		$operator = ($types[3] == ">=" || $types[3] == "<=") ? $types[3] : " = ";
		$column_name=strtoupper($columns[$i]);
		
		//add the users sites to the where string
		if($column_name == "SITE_ID")
		{
			if(!isset($column_name))
			{
				if($sql_where == "")  
					$sql_where = " WHERE " . $where;
				else
					$sql_where .= " AND " . $where;
				
			}
		}
		
		switch($column_name){
			case 'DATE_COMPLETED_FROM': //case 'CLOSING_DATE_FROM':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-5);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			case 'DATE_COMPLETED_TO': //case 'CLOSING_DATE_TO':
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					$col_name = substr($column_name,0,strlen($column_name)-3);
					$delim='';
					$sql_where .=  $col_name . $operator . $delim . $db->DBDate(trim($_POST[$column_name])) . $delim; 
				}
				break;
			default:
				if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
					$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
					if($operator=="LIKE"){
						$sql_where .=  $column_name . $operator . "'%" . $_POST[$column_name] . "%'"; 
					}else{
						$sql_where .=  $column_name . $operator . $delim . $_POST[$column_name] . $delim; 
					}
				}
				break;
		}
	}
	$search_sql = $sql . $sql_where;
	
	$search_display='none';
	$results_display='block';
	$show_search_results = true;

}
else
{
	$search_display='block';
	$results_display='none';
	$search_sql = $sql . " WHERE 1=2";
	$show_search_results = false;

}

$db->debug = false;
if($db->debug == true)
{
	print_r($_POST);
	echo('<br>'.$search_sql);
}


?>
<link rel="stylesheet" href="<?php echo WS_STYLE_PATH;?>forms.css" type="text/css" />
<script>
var FORM_TARGET="<?php echo $results_iframe_target;?>";
/***********************
 start up script for each page
***********************/
function init(){
	// create document.mousedown handlers
	init_document_handlers();
	// create document.mousedown handlers
	init_resize_results_container();
	
	return true;
}


function pager(q){
	location.href = "index.php?m=<?php echo $m;?>&p=results&"+q;
	top._show_message("Please wait while loading search results...");
}

window.onload = init;
</script>
<style>
html, body {
	width:100%;
	height:100%;
}
body {
	border:none;
	margin:0px;
	overflow:hidden;
}
</style>
</head>
<body><table width="100%">
<tr valign="top">
	<td width="100%">		
<!-- Results Table -->
<?php
/*
|------------------------------------------------------------
| Description: Modify colattribs alongside colarr
|              these are passed into the db_render_page control
|------------------------------------------------------------
*/
$colattribs = array(' width="40" ',' width="200" ',' width="120" ',' width="100" ',' width="" ',' width="150" ',' width="20" ');
$colarr = explode("|","ID|Type|Date|Site|Department|Location|edit:javascript:top.show_edit_screen(\"$m\",\"index.php?m=$m&p=edit&id=:".$TABLES[$p]['id'].":\")");
db_render_pager($search_sql,$colarr,$colattribs,$GENERAL['results']);
?>
	</td>
</tr>
</table>
