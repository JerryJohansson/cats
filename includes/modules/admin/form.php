<?PHP
require_once(CATS_CLASSES_PATH.'template.php');
require_once(CATS_CLASSES_PATH.'users.php');
require_once(CATS_CLASSES_PATH."mail.php");
require_once(CATS_CLASSES_PATH."mail_plus.php");

define('EZW_PREFIX','ezw::');
define('EZW_MAIL_SUBMIT',EZW_PREFIX.'submit');
define('EZW_MAIL_TYPE',EZW_PREFIX.'mid');
$mail_type = $_REQUEST[EZW_MAIL_TYPE] + 0;
$title = SITE_NAME.'|Forms Processor';
$br = "\n";

if(isset($_POST[EZW_MAIL_SUBMIT]) && $_POST[EZW_MAIL_SUBMIT]=='Send')
{
	foreach($_POST as $key => $value)
	{
		$bypass = strstr($key,EZW_PREFIX);
		if($bypass && $bypass==0)
		{
			$ezw_msg .= parseFormRow($key, $value);
			$ezw_msg_txt .= parseFormRow($key, $value, 'txt');
		}
		else
		{
			$ezw_form .= parseFormRow($key, $value);
			$ezw_form_txt .= parseFormRow($key, $value, 'txt');
		}
	}
	$form_table = '<table>'.$ezw_form.'</table><p>&nbsp;</p>';
	$ezw_msg = '<table border=1>'.$ezw_msg.'</table>';
	
	$tpl = new Template(DIR_WS_TEMPLATES."email.htm",DIR_WS_PUBLIC);
	$values = array( 
		"title"							=> $title,
		"main"							=> $form_table.$ezw_msg
	);
	$tpl->parse($values);
	$html_message = $tpl->get();
	
	// Process the final result
	$mail = new MailPlus;
	$mail_user = new User;
	$user_id = 5;
	$cc = 'fran@comverj.com';
	switch($mail_type)
	{
		case 1:
			$mail_user_details = $mail_user->getFields($user_id);
			$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
			$mail->addRecipient($mail_user_details['firstname'].' '.$mail_user_details['lastname'], $mail_user_details['email']);
			$mail->addRecipient($mail_user_details['firstname'].' '.$mail_user_details['lastname'], $cc);
			$mail->setSubject('Request more information on Current Graduate');
			$mail->setHTMLMessage($html_message);
			$mail->setTextMessage($ezw_mail_title.$br.$ezw_form_txt.$ezw_msg_txt);
			$mail->send();
			
			$content = "<h2>Thank You for your interest</h2><p>The following information has been sent to a ".SITE_NAME." staff member and will be attended to ASAP.</p>";
			break;
		case 2:
			$mail_user_details = $mail_user->getFields($user_id);
			$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
			$mail->addRecipient($mail_user_details['firstname'].' '.$mail_user_details['lastname'], $mail_user_details['email']);
			$mail->addRecipient($mail_user_details['firstname'].' '.$mail_user_details['lastname'], $cc);
			$mail->setSubject('Request more information on Your Job Requirements');
			$mail->setHTMLMessage($html_message);
			$mail->setTextMessage($ezw_mail_title.$br.$ezw_form_txt.$ezw_msg_txt);
			$mail->send();
			
			$content = "<h2>Thank You for your interest</h2><p>The following information has been sent to a ".SITE_NAME." staff member and will be attended to ASAP.</p>";
			break;
		default:
			
	}
	require(DIR_WS_CLASSES."navigation.php");
	if(!isset($mt)) $mt = DEFAULT_MENU_SYSTEM;
	$menu = new Navigation($mt);
	$menu->setMenus();
	
	$tpl = new Template("templates/main.htm");
	$values = array( 
		"title"						=> "Thank You",
		"main"						=> $content.$form_table , 
		"menu"						=> $menu->getMenuHTML(), 
		"menujs"					=> $menu->getMenuJS()
	);
	$tpl->parse($values);
	$tpl->output();
	exit;
}
elseif(isset($_REQUEST[EZW_MAIL_TYPE]))
{
	
	switch($mail_type)
	{
		case 1:
			if(isset($_REQUEST['Student_Name'])) $student_name = $_REQUEST['Student_Name'];
			$arr_fields = array(
				array('Students Name',$student_name,'text'),
				array('Employers Name','','text'),
				array('Contact Name','','text'),
				array('Contact Phone Number','','text'),
				array('Email','','text'),
				array('Message','','textarea')
			);
			break;
		case 2:
			$arr_fields = array(
				array('Employers Name','','text'),
				array('Contact Name','','text'),
				array('Contact Phone Number','','text'),
				array('Email','','text'),
				array('Job Requirements','','row'),
				array('Job Description','','textarea'),
				array('Qualities Wanted','','textarea'),
				array('Qualifications Wanted','','textarea')
			);
			break;
		default:
			$arr_fields = array(array('Information','No mail type was selected.'));
	}
	foreach($arr_fields as $arr_params)
	{
		$form_rows .= getFormRow($arr_params[0], $arr_params[1], $arr_params[2], $arr_params[3]);
	}
	$form_rows .= getFormRow(EZW_MAIL_SUBMIT,'Send','ezw');
	$form_table .= '<table width="90%" border="0" cellspacing="2" cellpadding="4">'.$form_rows.'</table>';
}
function parseFormRow($label='label', $value='value', $format='html')
{
	if($format=='html')
	{
		return '<tr valign="top"><td align="right"><b>'.str_replace('_',' ',$label).':</b></td><td>'.$value.'</td></tr>';
	}
	else
	{
		return "$label:  $value".$br;
	}
}
function getFormRow($label='label', $value_in='', $type='', $selected='')
{
	$sz = 50;
	$rows = 5;
	$name = str_replace(' ','_',$label);
	$value = ($reinsert_field) ? $_REQUEST[$name] : $value_in ;
	switch($type)
	{
		case 'text':
			$field = '<input type="text" class="sfield" size="'.$sz.'" name="'.$name.'" value="'.$value.'" />';
			break;
		case 'hidden':
			$field = '<input type="'.$type.'" class="sfield" size="'.$sz.'" name="'.$name.'" value="'.$value.'" />';
			break;
		case 'radio':case 'checkbox':
			$field = '<input type="'.$type.'" class="bfield" name="'.$name.'" value="'.$value.'" '.$selected.' />';
			break;
		case 'textarea':
			$field = '<textarea class="sfield" name="'.$name.'" rows="'.$rows.'" cols="'.$sz.'">'.$value.'</textarea>';
			break;
		case 'select':
			$field = '';
			break;
		case 'row':
			$field = '';
			break;
		case 'ezw':
			$field = '<input type="submit" name="'.$label.'" value="'.$value.'">';
			$label = '';
			break;
		default:
			$field = $value;
			break;
	}
	if($label!='') $label .= ':';
	if($type!='row')
	{
		return '<tr valign="top"><td align="right"><b>'.$label.'</b></td><td>'.$field.'</td></tr>';
	}
	else
	{
		return '<tr valign="top"><td colspan="2"><b>'.$label.'</b></td></tr>';
	}
}
?>
<html>
<head>
<title><?PHP echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/style.css" type="text/css">
<script language="javascript1.2" type="text/javascript" src="js/validation.js"></script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
<tr valign="top">
	<td height="1">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tr valign="top">
	<td rowspan="2"><img src="templates/images/tpl-t-l.gif" width="200" height="90" border="0" alt="HandsOn Computer Training - Hocti.com" /></td>
	<td bgcolor="#CCCC00" height="60" width="100%"><img src="templates/images/layout/clear.gif" width="1" height="60" align="left" /></td>
</tr>
<tr>
	<td bgcolor="#660066" height="30" colspan="2"><div id="headerTabNav"><img src="templates/images/layout/clear.gif" width="1" height="30"  align="left" /></div></td>
</tr>
<tr>
	<td colspan="2"><div class="headerNavigation"></div></td>
</tr>
</table>
	</td>
</tr>
<tr valign="top">
	<td width="100%">
	<div id="column_body">
<table width="100%" border="0" cellspacing="0" cellpadding="4">
<tr> 
	<td width="100%" valign="top">
		<form name="form1" method="post" action="forms.php">
		<input type="hidden" name="<?PHP echo EZW_MAIL_TYPE; ?>" value="<?PHP echo $mail_type;?>" />
			<?PHP echo $form_table; ?>
		</form>
		<p>&nbsp; </p>
	</td>
</tr>
</table>
	<p>&nbsp;</p>
	</div>
	</td>
</tr>
<tr height="1">
	<td class="footer" style="border-top:1px solid #ccc">
		<!-- footer -->
		<img src="templates/images/tpl-partners-logos.gif" width="600" height="70"><br />
		&nbsp;&nbsp;Copyright	&copy; 2004 HandsOn Computer Training International Pty Ltd. All rights reserved.	</td>
</tr>
</table>
</body>
</html><?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
