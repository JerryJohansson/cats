<?php
require_once(CATS_ADODB_PATH . "tohtml.inc.php");
require_once(CATS_CLASSES_PATH . "template.php");
require_once(CATS_CLASSES_PATH . "users.php");
require_once(CATS_CLASSES_PATH . "navigation.php");
require_once(CATS_CLASSES_PATH . "edit.php");
/*
+---------------------------------------------------------------------------
|  Start processing the page now
+---------------------------------------------------------------------------
*/
// name of results iframe. we use this name as a target to post our
// search criteria to or as a js element id to create a window handle
$results_iframe_target = "screen_{$m}_results";
$module_name = ucwords($m);
$module_name_txt = ucwords(preg_replace("/_/"," ",$m));
/*
+------------------------------------------------------------------------
|  Comment: Show type selection if we are in a new page and the parent page item allows different types as children
+------------------------------------------------------------------------
*/
$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
$pid=isset($_REQUEST['pid'])?$_REQUEST['pid']:0;
$type = (isset($_REQUEST['c']) && !empty($_REQUEST['c']))?$_REQUEST['c']:'';
$class_pagetype=($type=='')?'Edit':ucwords($type);

if(empty($type)){
	switch($action){
		case 'add':
			if(isset($pid)){
				$allowed_types = db_get_allowed_pagetype($pid);
				exit($allowed_types.":what the...");
			}
			break;
		case 'edit':
			if(isset($id)){
				$class_pagetype=ucwords(db_get_parent_pagetype($id));
			}
			break;
		default:
			main_redirect("index.php?m=$m&p=editor&a=$action&pid=0");
			break;
	}
}

if(empty($id)) $id=0;
$user_group_mask=$_SESSION['user_details']['groups'];
$user_role_mask=$_SESSION['user_details']['roles'];
$extras = array("No results");
$mode = "admin";

if($action=="add")
{
	$container = new Edit($pid,$mode);
	//$container->load($pid,$mode);
	if(empty($pid) || $pid == 0)
	{
		$pid = 0;
	}
	$extras = $container->getFormAdd();
}
elseif($action=="edit")
{
	$container = new Edit($id,$mode);
	//$container->load($id,$mode);
	$extras = $container->getFormEdit();
}
elseif($action=="del") // Show the delete confirmation
{
	$nav = new Navigation;
	$nav->delete($id);
}
else
{
	main_redirect("index.php?m=$m&p=splash");
}

$iTabs = 0; // Tab counter
$iTabButtons=0;
$status = $container->getStatus();
if(!((bool)$status)) $status=0;
$page_title = "Editing:";
?>
<link rel="stylesheet" type="text/css" href="style/forms.css" />
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script type="text/javascript">
/***********************
 start up script for each page
***********************/
var MODULE = '<?php echo $m;?>';
var PAGE = 'index';
function init(){
	init_tabs();
	// create document.mousedown handlers
	init_document_handlers();
	// resize editing section
	init_resize_editor();
	// resize editor
	resize_editor();
	return true;
}
window.onload = init;
window.onresize = function(){
	init_resize_editor();
	resize_editor();
}

function resize_editor(){
	var ed=element('FIELD_USER_GUIDE___Frame');
	if(ed){
		var pos=getPos(ed);
		var name='screen_'+MODULE+"_"+((PAGE)?PAGE:'edit');
		var bh=top.gui.screens[name].page.offsetHeight;
		ed.style.height=(bh-(pos.y+(tb.offsetHeight+4)));
	}
}

/*******************************
+ Editing functions
	_m.newModule, _m.saveModule, _m.deleteModule, _m.cancel, _m.refresh, _m.pager, _m.pager_sort
*******************************/
var _m=new _module("<?php echo $m;?>","<?php echo $p;?>","<?php echo $id;?>",document);
</script>
<style type="text/css">
textarea { background-color: #fff; border: 1px solid #000000; width: 100% !important; }
a.indent { border: inset; background-color: #999999; font-weight:bold; }
</style>
</head>
<body class="tb">
<form name="module" action="index.php?m=<?php echo $m;?>&p=post&a=<?PHP echo $action; ?>" method="POST" enctype="multipart/form-data">
<div id="tool_bar_c">
<fieldset class="bar" id="tab_buttons">
<a
	title="Edit <?PHP echo $module_name_txt;?> Details"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH;?>images/icons/properties.gif);"><?PHP echo $module_name_txt;?> Properties</a>
<a
	title="Edit Permissions For this page"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/security.gif);">Security</a>
<a
	title="Edit Extended Properties"
	id="tab_button[<?PHP echo ''.($iTabButtons++); ?>]" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/help.gif);">Help/User Guide</a>
</fieldset>
<fieldset class="tool_bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $module_name_txt;?>"
	href="javascript:_m.newModule();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $module_name_txt;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $module_name_txt;?>"
	href="javascript:_m.saveModule();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $module_name_txt;?></a>
<a
	title="Cancel operation"
	href="javascript:_m.cancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<?php
if($action=="edit"){if(cats_user_is_administrator()){
?>
<a
	title="Delete current <?PHP echo $module_name_txt;?>"
	href="javascript:_m.deleteModule();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<?php
}}
?>
<a
	title="Refresh current <?PHP echo $module_name_txt;?> item"
	href="javascript:_m.refresh();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
</div>

<div id="Lcontentbody">

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<?PHP $container->drawForm($extras); ?>
</table>
<fieldset class="tbar" style="text-align:right; ">
	<input type="submit" name="cats::Save" value="Save" >
	<input type="button" name="cats::Cancel" value="Cancel" onclick="_m.cancel();">
</fieldset>
</fieldset>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc; border-top:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="<?php echo(WS_ICONS_PATH); ?>info.gif" /></td>
			<td>
				<p><b>Viewing Security:</b> - If you would like to make this item accessible only to logged in members or certain User Groups from the following list, then select 1 or more User Groups from the list using ctrl+click to select multiple items.</p>
				<?php if($_SESSION['user_details']['groups'] & CATS_SA_MASK) { ?>
				<p><b>Editing Security:</b> - If you would like to restrict who edits this item, then select 1 or more User Roles from the list using ctrl+click to select multiple items.</p>
				<? } ?>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label for="id_user_group" title="Select the user groups allowed to view this item">Viewing Permissions:</label></td>
	<td><?PHP 
	$sql="select group_mask from form_fields where field_id=".$id;
	$node_groups = db_get_one($sql);
	echo html_user_groups_list('groups',$node_groups); ?></td>
</tr>
<?php if($_SESSION['user_details']['groups'] & CATS_SA_MASK) { ?>
<tr>
	<td class="label"><label for="id_user_role" title="Select the user roles this page is allowed to be edited by">Editing Permissions:</label></td>
	<td><?PHP 
	$sql = "select role_mask from form_fields where field_id=$id";
	$node_roles = db_get_one($sql);
	echo html_user_roles_list('roles',$node_roles);
	//db_close();
	?></td>
</tr>
<?php
}
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveSecurity" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="<?php echo(WS_ICONS_PATH); ?>info.gif" /></td>
			<td>
				<p>Enter help information and documentation for the user guide:</p>
				<ul>
					<li><b>Enter Help:</b> Describe the item and its purpose breifly.</li>
					<li><b>Enter User Guide :</b> Describe the item in detail making sure to describe it's relationship with other items etc.</li>
				</ul>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label>Help Tip</label></td><td><textarea class="sfield" name="FIELD_HELP"><?php echo strip_tags(session_id());?></textarea></td>
</tr>
</table>
<fieldset class="bar">
<?php 
// crashing IE ATM so revert to heavy HTML editor FKEditor
//echo html_draw_ezedit_field('FIELD_USER_GUIDE','<p>help goes here</p>');

include(CATS_ROOT_PATH."js/ezedit/fckeditor.php") ;
$sBasePath = WS_PATH . 'js/ezedit/';
//$sBasePath = substr( $sBasePath, 0, strpos( $sBasePath, "_samples" ) ) ;

$oFCKeditor							= new FCKeditor('FIELD_USER_GUIDE') ;
$oFCKeditor->BasePath		= $sBasePath ;
$oFCKeditor->ToolbarSet	= 'CATS' ;
$oFCKeditor->SID	= strip_tags(session_id()) ;
$oFCKeditor->Value			= $RS['FIELD_USER_GUIDE'];
$oFCKeditor->Create();

?>
</fieldset>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
</div>
</form><?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>