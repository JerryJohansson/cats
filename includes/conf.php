<?PHP
/*
+---------------------------------------------------------------------------
| CONFIG FILE - Always gets loaded first
+---------------------------------------------------------------------------
*/
// Set session handlers etc-----------------
//ob_start();
ini_set('output_buffering','On');
//ini_set('sendmail_from','admin@buzmedia.com.au');
$oldSessionName=session_name("cats");
session_cache_expire(30);
session_start();
function getmicrotime() 
{ 
	list($usec, $sec) = explode(" ", microtime()); 
	return ((float)$usec + (float)$sec); 
} 
//define('APP_SHOW_EXECUTION_TIME',true);
define('APP_START_TIME',getmicrotime());
// we will do our own error handling
// error_reporting(0);
error_reporting(E_DEPRECATED);// add dev

// user defined error handling function
function cats_error_handler($errno, $errmsg, $filename, $linenum, $vars) 
{
    // timestamp for the error entry 
	
    $dt = date("Y-m-d H:i:s (T)");

    // define an assoc array of error string
    // in reality the only entries we should
    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
    // E_USER_WARNING and E_USER_NOTICE
    $errortype = array (
                E_ERROR           => "Error",
                E_WARNING         => "Warning",
                E_PARSE           => "Parsing Error",
                E_NOTICE          => "Notice",
                E_CORE_ERROR      => "Core Error",
                E_CORE_WARNING    => "Core Warning",
                E_COMPILE_ERROR   => "Compile Error",
                E_COMPILE_WARNING => "Compile Warning",
                E_USER_ERROR      => "User Error",
                E_USER_WARNING    => "User Warning",
                E_USER_NOTICE     => "User Notice",
                E_STRICT          => "Runtime Notice"
                );
    // set of errors for which a var trace will be saved
    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);
    
    $err = "<errorentry>\n";
    $err .= "\t<datetime>" . $dt . "</datetime>\n";
    $err .= "\t<errornum>" . $errno . "</errornum>\n";
    $err .= "\t<errortype>" . $errortype[$errno] . "</errortype>\n";
    $err .= "\t<errormsg>" . $errmsg . "</errormsg>\n";
    $err .= "\t<scriptname>" . $filename . "</scriptname>\n";
    $err .= "\t<scriptlinenum>" . $linenum . "</scriptlinenum>\n";

    if (in_array($errno, $user_errors))
        $err .= "\t<vartrace>" . wddx_serialize_value($vars, "Variables") . "</vartrace>\n";
    $err .= "</errorentry>\n\n";
    
    // for testing
    // echo $err;

    // save to the error log, and e-mail me if there is a critical user error
    error_log($err, 3, "/usr/local/php4/error.log");
    if ($errno == E_USER_ERROR) {
        mail("cats.admin@tronox.com", "Critical CATS User Error", $err);
    }
}
//putenv('TZ=Australia/Perth');
if(isset($_GET['show_env'])){
	switch($_GET['show_env']){ // show phpinfo and exit if requested
		case 'php_info':
			phpinfo();
			exit;
			break;
		case 'const':
			$show_defined_constants=true;
			break;
		default:
			break;
	}
}
//------------------------------------------
// Define all mask values used within the site
define('MASK_NORMAL', 0);					// Normal system object. No attributes are set.
define('MASK_READ_ONLY', 1);			// Read-only object.
define('MASK_HIDDEN', 2);					// This is a sa - admin feature only and is hidden or FSO(Hidden file).
define('MASK_SYSTEM', 4);					// System file.
define('MASK_DIRECTORY', 16);			// Folder or directory.
define('MASK_ARCHIVE', 32);				// This object has been archived or FSO(File has changed since last backup.)
define('MASK_HISTORY', 64);				// This object has been put in its parents history list
define('MASK_PUBLISHED', 128);		// This object is published and and has been backed up
define('MASK_ALIAS', 1024);				// Link or shortcut.
define('MASK_COMPRESSED', 2048);	// Compressed object.
define('MASK_DELETED', 4096);			// The Deleted flag.
/* USER GROUP MASKS
select user_group_mask, user_group_name from user_group;
Admin Assistants, Administrators, Edit (Privileged), Edit (Restricted), Incident
 Analysis, Reader (Privileged), Super Administrator
8388608+1048576+524288+1024+512+32+16+8+4
USER_GROUP_MASK USER_GROUP_NAME
--------------- ----------------------------------------------------------------
        8388608 Master Administrator OR the developers of CATS
        1048576 Root Administrators
         524288 Super Administrators
           8192 Incidents Privileged User
           4096 Actions Administrator
           2048 Actions Privileged User
           1024 Administrators
            512 Admin Assistant
            256 PCR Team Member
            128 PCR Super Intendant
             64 PCR Engineer
             32 Incident Analysis
             16 Editor (Privileged)
              8 Editor (Restricted)
              4 Reader (Privileged)
              2 Reader (Restricted)
              0 No Access
*/
// add root admin and master admin masks to all constants
// Root Administrators should be able to see everything
// Master Administrators OWN this application and are equal to GODS ;)
// Use these bit masks to control access to pages, controls etc
// The following statement will equal true if the user belongs to the CATS_ADMINISTRATOR group
// e.g. ($users_group_mask & CATS_ADMINISTRATOR)>0
// The following statement will equal true if the user belongs to the following groups:
//		CATS_ADMINISTRATOR, CATS_INCIDENT_ANALYSIS and CATS_EDITOR
// e.g. ($users_group_mask & (CATS_ADMINISTRATOR+CATS_INCIDENT_ANALYSIS+CATS_EDITOR))>0
define('CATS_ROOT_ADMINISTRATOR',9437184); 
define('CATS_SUPER_ADMINISTRATOR',524288+9437184);
define('CATS_PCR_ADMINISTRATOR',16384+9437184);
define('CATS_INCIDENT_PRIVILEGED',8192+9437184); // user gets privileged info on incidents
define('CATS_ACTION_ADMINISTRATOR',4096+9437184); // user can change due date on actions
define('CATS_ACTION_PRIVILEGED',2048+9437184); 
define('CATS_ADMINISTRATOR',1024+9437184);
define('CATS_ADMIN_ASSISTANT',512+9437184);
define('CATS_PCR_TEAM_MEMBER',256+9437184);
define('CATS_INCIDENT_ANALYSIS',32+9437184);
define('CATS_EDITOR',16+8+9437184);
define('CATS_EDITOR_PRIVILEGED',16+9437184);
define('CATS_EDITOR_RESTRICTED',8+9437184);
define('CATS_READER',2+4+9437184);
define('CATS_READER_PRIVILEGED',4+9437184);
define('CATS_READER_RESTRICTED',2+9437184);
/* TO SELECT USER GROUP AND BELOW RUN THE FOLLOWING (IF NEEDED)
-- get Super Administrators level and below
select sum(user_group_mask) as super_administrator from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Super Administrators');

-- get Incidents Privileged User level and below
select sum(user_group_mask) as inicident_priv from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Incidents Privileged User');

-- get Actions Administrator level and below
select sum(user_group_mask) as actions_admin from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Actions Administrator');

-- get Actions Privileged User level and below
select sum(user_group_mask) as actions_priv from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Actions Privileged User');

-- get Administrators level and below
select sum(user_group_mask) as administrator from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Administrators');

-- get Admin Assistant level and below
select sum(user_group_mask) as admin_asist from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Admin Assistant');

-- get PCR Team Member level and below
select sum(user_group_mask) as PCRT from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'PCR Team Member');

-- get PCR Super Intendant level and below
select sum(user_group_mask) as PCR_Super from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'PCR Super Intendant');

-- get PCR Engineer level and below
select sum(user_group_mask) as PCR_Engineer from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'PCR Engineer');

-- get Incident Analysis level and below
select sum(user_group_mask) as Incident_Analysis from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Incident Analysis');

-- get Editor (Privileged) level and below
select sum(user_group_mask) as Editor_Priv from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Editor (Privileged)');

-- get Editor (Restricted) level and below
select sum(user_group_mask) as Editor_Restrict from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Editor (Restricted)');

-- get Reader (Privileged) level and below
select sum(user_group_mask) as Reader_Priv from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Reader (Privileged)');

-- get Reader (Restricted) level and below
select sum(user_group_mask) as Reader_Restrict from user_group where user_group_mask <= 
(select u.user_group_mask from user_group u where u.user_group_name = 'Reader (Restricted)');
*/

// Define form constants
define('CATS_FORM_PREFIX','cats::');
define('CATS_FORM_MAIL_SUBMIT',CATS_FORM_PREFIX.'submit');
define('CATS_FORM_MAIL_TYPE',CATS_FORM_PREFIX.'mid');
define('CATS_FORM_FUNCTION', CATS_FORM_PREFIX.'func');
define('CATS_FORM_RETURN_URL', CATS_FORM_PREFIX.'rurl');

// use these as button names to identify what to leave out of the processing part of the form
define('CATS_FORM_SEARCH', CATS_FORM_PREFIX.'search');
define('CATS_FORM_EDIT', CATS_FORM_PREFIX.'edit');
define('CATS_FORM_EDIT_SAVE', CATS_FORM_PREFIX.'save');

// checkbox name in the results pages used for actions such as delete or change status etc
define('CATS_FORM_ACTION_SELECT', CATS_FORM_PREFIX.'op');
define('CATS_FORM_ACTION_CHECKBOX_NAME', CATS_FORM_PREFIX.'act_chk'); // use this when getting the value from the $_POST
define('CATS_FORM_ACTION_CHECKBOX', CATS_FORM_ACTION_CHECKBOX_NAME.'[]'); // use this to name the elements

// Set main request vars
// Change _REQUEST to _GET
// m=module
// p=page
// a=action
$m=(isset($_GET['m']))?$_GET['m']:(isset($_SESSION['m'])?$_SESSION['m']:'dashboard');
$p=(isset($_GET['p']))?$_GET['p']:(isset($_SESSION['p'])?$_SESSION['p']:'index');
$a=(isset($_GET['a']))?$_GET['a']:(isset($_SESSION['a'])?$_SESSION['a']:'search');
$id=(isset($_GET['id']))?$_GET['id']:(isset($_SESSION['id'])?$_SESSION['id']:0);

// If we experience a 'white screen of death' or other problems,
// uncomment the following line of code:
//error_reporting( E_ALL );

$loginFromPage = 'index.php';
$baseDir = dirname(__FILE__);


// automatically define the base url
$baseUri = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
$server_name = str_replace(".tiwest.com.au","",$server_name);
$baseUri .= $host_name;

define('WS_BASE_PATH',$baseUri);
$baseDir=substr($baseDir,0,strpos($baseDir,"includes"));

if(strpos(WS_BASE_PATH,"localhost")){//isset($_ENV["COMPUTERNAME"])){
	$baseUrl=substr($baseUri,0,strpos($baseUri,"/cats"))."/cats";
	$domain_split=explode("/",$_SERVER['SCRIPT_NAME']);

	$pathInfo = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : getenv('PATH_INFO');
	if (@$pathInfo) {
		$baseUri .= dirname($pathInfo);
	} else {
		$pathInfo="N/A";
		$baseUri .= isset($_SERVER['SCRIPT_NAME']) ? dirname($_SERVER['SCRIPT_NAME']) : dirname(getenv('SCRIPT_NAME'));
	}

}else{
	$pathInfo="N/A";
	$baseUrl=$baseUri;
	$domain_split=array(0,$_SERVER['SERVER_NAME'],'/');
}


//echo($server_name.''.$host_name);
//echo("baseUrl=$baseUrl:::baseDir=$baseDir:::pathInfo=$pathInfo:::baseUri=$baseUri");


define('SERVER_DOMAIN',$domain_split[1]);
define('SITE_IS_LIVE',false);	

define('CATS_ROOT_PATH',$baseDir); 
define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
define('CATS_CLASSES_PATH',CATS_INCLUDE_PATH.'classes/');
define('CATS_FUNCTIONS_PATH',CATS_INCLUDE_PATH.'functions/');
define('CATS_ADODB_PATH',CATS_INCLUDE_PATH.'adodb/');
define('CATS_MODULES_PATH',CATS_INCLUDE_PATH.'modules/');// $m/classes/$m.php
define('CATS_REMOTE_PATH',CATS_INCLUDE_PATH.'remote/');

define('WS_PATH',$baseUrl.'/');//'/'.SERVER_DOMAIN.'/');
define('WS_MODULES_PATH',WS_PATH . 'includes/modules/');
define('WS_REMOTE_PATH',WS_PATH . 'includes/remote/');
define('WS_HELPER_PATH',WS_PATH . 'includes/helpers/');
define('WS_CLASSES_PATH',WS_PATH . 'includes/classes/');

// load db stuff
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/cats_admin.ini");
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}
//--------------------------------------------------------------
$includes_ok=false; 
//print_r(get_defined_constants());
// include the database functions - table definitions are kept in the default.ini file
require_once(CATS_FUNCTIONS_PATH . 'db_functions.php');

// if we want the main configuration details only, ignore definitions
if(!(isset($config_details_only) && $config_details_only==true)){ 
	require_once(CATS_FUNCTIONS_PATH . 'main_functions.php');
	
	require_once(CATS_FUNCTIONS_PATH . 'cats_functions.php');
	require_once(CATS_FUNCTIONS_PATH . 'js_functions.php');
	require_once(CATS_FUNCTIONS_PATH . 'html_functions.php');
	//require_once(CATS_FUNCTIONS_PATH . 'session_functions.php');
	// TODO: maybe remove this funciton
	require_once(CATS_FUNCTIONS_PATH . 'security_functions.php');
	
	require_once(CATS_CLASSES_PATH . 'message_stack.php');
	
	if($m!="login"){
		// If the user session is not set kick them out to login page
		if(!isset($_SESSION['user_details']) || !is_array($_SESSION['user_details'])){
			$user_authenticated = false;
		}else{
			// Now that all the functions are loaded lets authenticate the user
			// against the requested module/page
			if(isset($_SESSION['user_details']['user_id']) && ($_SESSION['user_details']['user_id']+0)>0){
				//$users = new User
				//$user_authenticated = $users->authenticate();
				$user_authenticated = true;
			}else{
				$user_authenticated = false;
			}
		}
		
		// Now lets kick the user out to login if they were not authenticated
		if($user_authenticated == false){
			if(!isset($_SESSION['bypass_cats_maintenance']) && isset($_GET['cats_maintenance'])){
				$_SESSION['bypass_cats_maintenance'] = 'yes';
			}else if(isset($_SESSION['bypass_cats_maintenance'])){
				//$_SESSION['bypass_cats_maintenance']='yes';
			}else{
				require_once(CATS_INCLUDE_PATH."top_redirect.php");
				exit;
			}
		}
	}
	
	//if(!isset($_SESSION['messageStack'])) 
	$_SESSION['messageStack'] = new messageStack;
	
	if($m != "" && $p != "") {
		if(file_exists(CATS_MODULES_PATH."$m/$p.php")){
			$includes_ok=true;
		}else{
			$_SESSION['messageStack']->add("Either Module or Page does not exist. Make sure the Module and Page are defined and that they exist.");
		}
	} else {
		$_SESSION['messageStack']->add("Either Module or Page was not declared. Make sure the Module and Page are defined and that they exist.");
	}
}

// set configuration constants with user options over-writing the default constants
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$server_name/default.ini");
if(function_exists("db_get_key_value_array") && isset($_SESSION['user_details'])){
	if($_SESSION['user_details']){
	$user_id = $_SESSION['user_details']['user_id'];
	$arr = db_get_key_value_array("select configuration_key as akey, configuration_value as avalue from configuration where employee_number = $user_id");
	$config = array_merge($config, $arr);
}
}
foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}
//print_r(get_defined_constants());
if(!defined('DEFAULT_STYLE')){
	define('DEFAULT_STYLE','cats');
}
define('CATS_STYLE_PATH', CATS_ROOT_PATH . 'style/' . DEFAULT_STYLE . '/');
define('CATS_TEMPLATE_PATH',CATS_STYLE_PATH.'templates/');

define('WS_STYLE_PATH', WS_PATH . 'style/' . DEFAULT_STYLE . '/');
define('WS_ICONS_PATH',WS_STYLE_PATH . 'icons/');
define('WS_IMAGES_PATH',WS_STYLE_PATH . 'images/');
//print(CATS_MAINTENANCE . ':' . ((bool)CATS_MAINTENANCE));
//print_r($_SESSION);
// display vars if asked to ;)
if(isset($show_defined_constants)){
	print_r(get_defined_constants());
	if($show_defined_constants===true) exit;
}

?>