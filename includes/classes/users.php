<?php

class User {

	var $db;

	var $fields=array();

	var $site_id = '';

	function __construct()
	{

		global $db;

		$this->db = $db;

	}

	

	function login($uid='',$pwd='',$action='Logout',$rurl='')

	{

		global $db;

		

		$ws_path=defined('CATS_ROOT_PATH')?CATS_ROOT_PATH:'/apps/CATS/dev/';

		//echo($uid.':'.$pwd.':'.$action);

		if($action=='Logout')

		{

			unset($_SESSION['user_details']);

		}

		elseif($action=='Login')

		{

			// main auth flag

			$authenticated = false;

			// ldap auth flag -set $ldap_auth to false if username/password does not authenticate against ldap

			// if ldap doesn't authenticate the user then we assume the user is a contractor and doesn't have an

			// entry in ldap and in this case we check the username/password match in CATS db before we authenticate.

			// Otherwise we only check the username and get their details...the password will be different in CATS db

			// as it is to ldap because ldap passwords get changed often.

			$ldap_auth = false;

			/*

			if we are using localhost then don't worry about ldap ;)

			otherwise authenticate user against the ldap server

			*/

			if(strpos(WS_BASE_PATH,"localhost")===false){

				$cmd = $ws_path . 'includes/cgi/AuthSRA.pl -u '. $uid . ' -p "' . $pwd .'"';

				//echo '<hr>' . $cmd . '<hr>';
				
				$handle = popen($cmd, 'r');
				
				// add by dev $filesize = filesize($ws_path . 'includes/cgi/AuthSRA.pl');

				//sleep(1);
				// add dev $read = fread($handle, $filesize);
			 	$read = fread($handle, 2096);
				flush();
				//sleep(1);

				//echo "<br>Returned -- read: " . $read . " | " . $handle . "<br>";
				
				pclose($handle);
				// add dev echo "here $read test";				

				if ( $read == "True" ) { 

					//if(isset($_SESSION['messageStack'])) $_SESSION['messageStack']->add( $uid . ' successfully authenticated.<br>', 'success');

					//echo "true are we";

					$authenticated = true;

					$ldap_auth = true;

				} else {

					//if(isset($_SESSION['messageStack'])) $_SESSION['messageStack']->add( $uid . ' NOT authenticated.<br>' , 'error');

					//echo "false are we";

					$authenticated = false;

				}

			}else{

				$authenticated = true;

				$ldap_auth = true;

			}

			

			$s = "{'message':";

			if( $authenticated==true || ($ldap_auth==false && $authenticated==false) ){

				//$sql = "select e.*, s.site_description, s.site_code, d.department_description, s1.section_description, p.position_description from tblemployee_details e, tblsite s, tbldepartment d, tblsection s1, tblposition p where e.site_id=s.site_id and e.department_id=d.department_id and e.section_id=s1.section_id and e.position = p.position_id and upper(e.access_id) = upper('$uid') and e.user_status = 'Active'";

				$sql = "select e.*, s.site_description, s.site_code, s.site_folder, d.department_description, s1.section_description, p.position_description from tblemployee_details e, tblsite s, tbldepartment d, tblsection s1, tblposition p where s.site_id=e.site_id and d.department_id=e.department_id and s1.section_id(+)=e.section_id and p.position_id=e.position and upper(e.access_id) = upper('$uid') and e.user_status = 'Active'";

				// add the password check for users who don't have an entry in ldap

				if($ldap_auth===false) {
				
					$sql .= " and e.password = Encrypt_password(upper('$pwd'))";
					
				}

				$rs = db_query($sql);

				if($rs->RecordCount()>1) {

					$authenticated = false;

					$s .= "'You have multiple entries of your access id and for this reason I am having difficulty authenticating you...Please contact catsadmin@tronox.com and notify them that you need to remove the duplicate user account entries in your name.'";

					$s .= ",'success':false";

					// get duplicate entries to notify user

					// TODO: email CATSADMIN to notify them of duplicate employee entries and tell them to do something about it

					$susers = '';

					while($arr = $rs->FetchRow()) {

						$susers .= "[{$arr['EMPLOYEE_NUMBER']}, '{$arr['FIRST_NAME']} {$arr['LAST_NAME']}'],";

					}

					if($susers!='') {
						
						$s .= ",'duplicates':[" . preg_replace("/,$/i","",$susers) . "]";
						
					}

				} elseif(is_array($this->get_user_details($rs))) { // set the users session details

					$authenticated = true;

					$s .= "'You have been authenticated'";//"'Good Morning ".$_SESSION['user_details']['first_name'].", You have successfully logged into the system.\\nPlease wait while you are transferred to your dashboard.'";

					$s .= ",'success':true";

					$s .= ",'url':'index.php?top=true'";

					$s .= ",'redirect':'$rurl'";

				} else {
					
					if(isset($uid) && empty($uid)) {
						
						// Check for Blank UserID

						$s .= "'You forgot to enter your login and/or password.\\n\\nPlease check it and try again.'";
						
					} elseif(isset($pwd) && empty($pwd)) {
						
						// Check for Blank Password

						$s .= "'You forgot to enter your password.\\n\\nPlease check it and try again.'";
						
					} else {
						
						// Does the user exist in the Database ?
						
						$sql = "select e.*, s.site_description, s.site_code, s.site_folder, d.department_description, s1.section_description, p.position_description from tblemployee_details e, tblsite s, tbldepartment d, tblsection s1, tblposition p where s.site_id=e.site_id and d.department_id=e.department_id and s1.section_id(+)=e.section_id and p.position_id=e.position and upper(e.access_id) = upper('$uid') and e.user_status = 'Active'";
						
						$rs = db_query($sql);

						if($rs->RecordCount()>=1) {
							
							// User exists, so password must be incorrect
							
							$s .= "'Your password is incorrect.\\n\\nPlease check it and try again.'";
							
						} else {
							
							// User does not exist
							
							$s .= "'Your login is incorrect.\\n\\nPlease check it and try again.'";
							
						}
						
					}

					$s .= ",'success':false";

				}

			} elseif($authenticated==false) {

				$s .= "'Your login and/or password are incorrect.\\n\\nPlease check it and try again.'";

				$s .= ",'success':false";

			}

			$s .= "}";

		}

		return $s;

	}

	

	function get_user_details(&$rs)

	{	

		global $db;

		

		if($arr = $rs->FetchRow()){

			

			// get the users id first so we can get and set logs

			$uid = $arr['EMPLOYEE_NUMBER'];

			$log_type = 'login';

			$date_format = defined('CATS_DATE_FORMAT_LONG')?CATS_DATE_FORMAT_LONG:"DD-MON-YYYY HH24:MI:SS";

			// get the last time this user logged in

			$sql = "select * from (select to_char(date_time,'$date_format') as date_time from user_history_log where employee_number = $uid and log_type = '$log_type' order by date_time desc) where rownum=1";

			$rslog = $db->Execute($sql);

			//print_r($rslog->GetAll());

			$last_log_date = "now";

			if($log = $rslog->FetchRow()){

				$last_log_date = $log['DATE_TIME'];

			}else{

				//echo( "<b>SQL:</b><br>".$sql );

			}

			// set log date and time etc for this user

			db_insert_history_log( $log_type, $uid );

			

			// set the user details array for easy retreival of common info

			$_SESSION['user_details'] = array(

				"user_id" => $uid,

				"login" => $arr['ACCESS_ID'],

				"first_name" => $arr['FIRST_NAME'],

				"last_name" => $arr['LAST_NAME'],

				"email" => $arr['EMAIL_ADDRESS'],

				"lastlogdate"	=> date('l dS of F Y',strtotime($last_log_date)), 

				"lastlogtime"=> date('h:i:s A',strtotime($last_log_date)), 

				"HISTORY_MIN_LENGTH" => 5,

				"groups" => $arr['GROUP_MASK'], 

				"roles" => $arr['ROLE_MASK'], 

				"site_id" => $arr['SITE_ID'],

				"site_name" => $arr['SITE_DESCRIPTION'],

				"site_folder" => $arr['SITE_FOLDER'],

				"site_code" => $arr['SITE_CODE'],

				"site_access" => ($arr['SITE_ACCESS']=='NULL')?'':$arr['SITE_ACCESS'], //$arr['VIEWABLE_SITES'],

				"department_id" => $arr['DEPARTMENT_ID'],

				"section_id" => $arr['SECTION_ID'],

				"position_id" => $arr['POSITION'],

				"department" => $arr['DEPARTMENT_DESCRIPTION'],

				"section" => $arr['SECTION_DESCRIPTION'],

				"position" => $arr['POSITION_DESCRIPTION'],
				
				"add_employee" => $arr['ADD_EMPLOYEE']
				

			);

		}else{

			$_SESSION['user_details'] = "";

		}

		//print_r($_SESSION['user_details']);

		return $_SESSION['user_details'];

	}

	

	function renew_session(){

		$sql = "select e.*, s.site_description, s.site_code, s.site_folder, d.department_description, s1.section_description, p.position_description from tblemployee_details e, tblsite s, tbldepartment d, tblsection s1, tblposition p where e.site_id=s.site_id and e.department_id=d.department_id and s1.section_id(+) = e.section_id and e.position = p.position_id and employee_number = {$_SESSION['user_details']['user_id']}";

		$rs = db_query($sql);

		$s = "";

		if(is_array($this->get_user_details($rs))){ // set the users session details

			$s .= "'success':true";

		}else{

			$s .= "'success':false";

		}

		return $s;

	}

	

	

	function getEmployeesArray($id, $filter, $all_employees = false){

		global $db;

		

		$s="";

		if($id>0){

			$s .= " where e.employee_number = $id ";

		}

		if($filter!=""){

			if(empty($s)){

				$s .= " where ";

			}else{

				$s .= " and ";

			}

			$last_name = db_sanitize('%'.$filter.'%');

			$s .= " ((lower(e.last_name) like lower($last_name)) ";

			$s .= ')';

			if(!$all_employees) $s .= " and e.user_status = 'Active' ";

		}

		if(empty($s)){

			$s .= " where ";

		}else{

			$s .= " and ";

		}

		$s .= " (s1.site_id=e.site_id) and (d.department_id = e.department_id) and (s.section_id(+) = e.section_id and (p.position_id(+) = e.position)) ";

		if($this->site_id!=''){

			if(empty($s)){

				$s .= " where ";

			}else{

				$s .= " and ";

			}

			$s .= " e.site_id in ( {$this->site_id} ) ";

		}

		$sql = "select e.employee_number as id, e.last_name||', '||e.first_name as text, s1.site_id, s1.site_description as site, d.department_id, d.department_description as department, s.section_id, s.section_description as section, p.position_id, p.position_description as position  from tblemployee_details e, tblsite s1, tbldepartment d, tblsection s, tblposition p $s";

		$sql .= " order by e.last_name, s1.site_description";

		//$db->debug=true;

		$rs = $db->Execute($sql);

		if($ret=$rs->GetAll()){

			return $ret;

		}else if($ret=$rs->GetArray()){

			return $ret;

		}else{

			return false;

		}

	}


	function sanitize($s,$dir=0)

	{

		if($dir==0){ // Going into db

			return preg_replace("/'/","''",$s);

		}else{ // Coming out of db

			return preg_replace("/'/","\\'",$s);

		}

	}

}

//$users=new User;

			

/*

tblcompanies;

 Name                                      Null?    Type

 ----------------------------------------- -------- ----------------------------

 COMPANYID                                 NOT NULL NUMBER

 MIMS_ID                                            VARCHAR2(20)

 COMPANYNAME                                        VARCHAR2(200)

 COMPANYSTATUS                                      VARCHAR2(9)



tbldepartment;

 Name                                      Null?    Type

 ----------------------------------------- -------- ----------------------------

 SITE_ID                                   NOT NULL NUMBER

 DEPARTMENT_ID                             NOT NULL NUMBER

 DEPARTMENT_DESCRIPTION                    NOT NULL VARCHAR2(100)

 DEPARTMENT_NAME                                    VARCHAR2(20)



tblposition

 Name                                      Null?    Type

 ----------------------------------------- -------- ----------------------------

 POSITION_ID                               NOT NULL NUMBER

 POSITION_DESCRIPTION                      NOT NULL VARCHAR2(40)



tblsection

 Name                                      Null?    Type

 ----------------------------------------- -------- ----------------------------

 SECTION_ID                                NOT NULL NUMBER

 SITE_ID                                   NOT NULL NUMBER

 DEPARTMENT_ID                                      NUMBER

 SECTION_DESCRIPTION                       NOT NULL VARCHAR2(100)

 SECTION_NAME                              NOT NULL VARCHAR2(20)

*/

?>
