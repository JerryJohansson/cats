<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Templating System
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
class Template
{
	var $replace_pattern;						// String used to replace tpl tags - DO NOT TOUCH THIS!!
	var $template;									// Location to the currently loaded template file
	var $loops;											// Temporary variable to hold loop data
	var $data;											// Array of block values
	var $extra_replace;							// Internal extra values for parse array. 
	
	function Template($tpl='main.htm', $use_string_as_tpl=false)
	{
		$baseDir=CATS_TEMPLATE_PATH;
		$baseUri=WS_TEMPLATE_PATH;
		if($use_string_as_tpl==false){
			if(!file_exists($tpl)) {
				$temp	= implode('',file("$baseDir/".basename($tpl)));
			} else {
				$temp = implode('', file($tpl) );
			}
		}else{
			$temp = $tpl;
		}
		$temp													= preg_replace("/(movie\" value|src|background|<link href)=\"(?!http)/mi","\$1=\"$baseUri/",$temp);
		$temp													= preg_replace("/(:url\((?!http))/mi","$1$baseUri/",$temp);
		$this->replace_pattern				= "|<!--\s+([a-zA-Z_\[?\]?\x7f-\xff][a-zA-Z0-9_\[?\]?\x7f-\xff]*)\s+-->|me";
		$this->template								= $temp;
		$this->loops									= array();
		$this->data["DOCUMENT_URL"]		= ((getenv("HTTPS")=='on')?"https://":"http://").$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		$this->init();
	}
	
	function init()
	{
		$this->template = preg_replace("/<!-- box_start -->/",ezw_get_box_start(),$this->template);
		$this->template = preg_replace("/<!-- box_body -->/",ezw_get_box_body(),$this->template);
		$this->template = preg_replace("/<!-- box_end -->/",ezw_get_box_end(),$this->template);
	}
	
	function setExtraReplace($a){
		$this->extra_replace = $a;
	}
	function getExtraReplace(){
		if(!is_array($this->extra_replace)) $this->setExtraReplace(get_defined_constants());
		return $this->extra_replace;//array("SERVER_DOMAIN"=>SERVER_DOMAIN);array("SERVER_DOMAIN"=>SERVER_DOMAIN);//
	}
	
	function set_param($name, $value)
	{
		$this->data[$name] = $value;
	}
	
	function set_block($name, $block)
	{
		$this->data[$name][] = $block;
	}

	function parse_blocks()
	{
		$body = $this->template;
		if (count($this->data)) foreach ($this->data as $key => $value) {
			if (is_array($value)) {
				$blocks = array();
				foreach ($value as $block) {
					if (preg_match("/<!-- BEGIN ".$block->name." -->(.*)<!-- END ".$block->name." -->/s", $body, $matches)) {
						$blocks[] = $block->parse($matches[1]);
					}
				}
				if (count($blocks)) {
					$body = preg_replace("/\{(".$key.")\}/", join("", $blocks), $body);
				}
			} else {
				$body = preg_replace("/\{(".$key.")\}/", $value, $body);
			}
		}
		$body = preg_replace("/<!-- BEGIN (\w+) -->(.*)<!-- END \\1 -->/s", "", $body);
		return $body;
	}
	
	function prepare_blocks()
	{
		$this->template = $this->parse_blocks();
	}
	
	// Replaces <!-- tagname --> with the contents of $values['tagname']
	function parse($values)
	{
		extract(array_merge($values,$this->getExtraReplace()));
		$this->template = preg_replace($this->replace_pattern, '$$1;', $this->template);
	}
	
	function parse_vars($names, $values)
	{
		$this->template = preg_replace($names, $values, $this->template);
	}
	
	function parse_loop($loop)
	{
		$tpl = explode('<!-- '.$loop.' -->', $this->template);
		$tempArray = array($loop => $tpl[1]);
		$this->loops = array_merge($this->loops, $tempArray);
		$this->template = $tpl[0].'<!-- '.$loop.' -->'.$tpl[2];
	}
	
	function get_loop($loop, $values)
	{
		extract($values);
		$tpl = preg_replace($this->replace_pattern, '$$1;', $this->loops[$loop]);
		return $tpl;
	}
	
	function get()
	{
		return $this->template;
	}
	
	function output($blocks=false)
	{
		if($blocks) $this->template = $this->parse_blocks();
		echo($this->template);
	}
}

class Block {
	var $name;
	var $data;
	
	function Block($name, $data = NULL) {
		$this->name = $name;
		$this->data = $data;
	}
	
	function set_param($name, $value) {
		$this->data[$name] = $value;
	}
	
	function set_block($name, $block) {
		$this->data[$name][] = $block;
	}
	
	function parse($template) {
		if (count($this->data)) foreach ($this->data as $key => $value) {
			if (is_array($value)) {
				$blocks = array();
				foreach ($value as $block) {
					if (preg_match("/<!-- BEGIN ".$block->name." -->(.*)<!-- END ".$block->name." -->/s", $template, $matches)) {
						$blocks[] = $block->parse($matches[1]);
					} else {
						echo $block->name." not found in <pre>".$template."</pre>";
					}
				}
				$template = preg_replace("/\{(".$key.")\}/", join("", $blocks), $template);
				$template = preg_replace("/<!-- BEGIN (\w+) -->(.*)<!-- END \\1 -->/s", "", $template);
			} else {
				$template = preg_replace("/\{(".$key.")\}/", $value, $template);
			}
		}
		return $template;
	}
}
?>
