<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Date class
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

class Date {
	function Date()
	{
		
	}


/*
+---------------------------------------------------------------------------
|	DATEADD
|	 Returns a new Unix timestamp value based on adding an interval to the specified date.
|	 @param  string  $datepart is the parameter that specifies on which part of the date to return a new value.
|	 @param  integer $number   is the value used to increment datepart. If you specify a value that is not an integer, the fractional part of the value is discarded.
|	 @param  integer $date     a Unix timestamp value.     
|	 @return integer a Unix timestamp.
+---------------------------------------------------------------------------
*/
	function dateadd($datepart, $number, $date)
	{
		$number = intval($number);
		switch (strtolower($datepart)) {
			case 'yy':
			case 'yyyy':
			case 'year':
				$d = getdate($date);
				$d['year'] += $number;
				if (($d['mday'] == 29) && ($d['mon'] == 2) && (date('L', mktime(0, 0, 0, 1, 1, $d['year'])) == 0)) $d['mday'] = 28;
				return mktime($d['hours'], $d['minutes'], $d['seconds'], $d['mon'], $d['mday'], $d['year']);
				break;
			case 'm':
			case 'mm':
			case 'month':
				$d = getdate($date);
				$d['mon'] += $number;
				while($d['mon'] > 12) {
					$d['mon'] -= 12;
					$d['year']++;
				}
				while($d['mon'] < 1) {
					$d['mon'] += 12;
					$d['year']--;
				}
				$l = date('t', mktime(0,0,0,$d['mon'],1,$d['year']));
				if ($d['mday'] > $l) $d['mday'] = $l;
				return mktime($d['hours'], $d['minutes'], $d['seconds'], $d['mon'], $d['mday'], $d['year']);
				break;
			case 'd':
			case 'dd':
			case 'day':
				return ($date + $number * 86400); 
				break;
			default:
				return $date;//die("Unsupported operation");
		}
	}
/*
+---------------------------------------------------------------------------
|	DATEDIFF
|	 Returns the number of date and time boundaries crossed between two specified dates.
|	 @param  string  $datepart  is the parameter that specifies on which part of the date to calculate the difference.
|	 @param  integer $startdate is the beginning date (Unix timestamp) for the calculation.
|	 @param  integer $enddate   is the ending date (Unix timestamp) for the calculation.     
|	 @return integer the number between the two dates.
+---------------------------------------------------------------------------
*/
	function datediff($datepart, $startdate, $enddate)
	{
		switch (strtolower($datepart)) {
			case 'yy':
			case 'yyyy':
			case 'year':
				$di = getdate($startdate);
				$df = getdate($enddate);
				return $df['year'] - $di['year'];
				break;
			case 'q':
			case 'qq':
			case 'quarter':
				die("Unsupported operation");
				break;
			case 'n':
			case 'mi':
			case 'minute':
				return ceil(($enddate - $startdate) / 60); 
				break;
			case 'hh':
			case 'hour':
				return ceil(($enddate - $startdate) / 3600); 
				break;
			case 'd':
			case 'dd':
			case 'day':
				return ceil(($enddate - $startdate) / 86400); 
				break;
			case 'wk':
			case 'ww':
			case 'week':
				return ceil(($enddate - $startdate) / 604800); 
				break;
			case 'm':
			case 'mm':
			case 'month':
				$di = getdate($startdate);
				$df = getdate($enddate);
				return ($df['year'] - $di['year']) * 12 + ($df['mon'] - $di['mon']);
				break;
			default:
				die("Unsupported operation");
		}
	}
/*
+---------------------------------------------------------------------------
|	RE-OCCURRING DATE
|	 Returns an array of re-occurring dates
|		getReoccurringDatesArray('d',7,'d-m-Y','6:00P\M l dS of F Y')
+---------------------------------------------------------------------------
*/
	function getReoccurringDatesArray($datepart='d',$addamount=7,$formatkey='Y-m-d H:i:s',$formatval='Y-m-d H:i:s')
	{
		$ret = array();
		$dt = mktime(0,0,0,9,1,2004);
		while($dt < mktime())
		{
			$dt = $this->dateadd($datepart,$addamount,$dt);
		}
		$dt = $this->dateadd($datepart,-$addamount,$dt);
		for($i=0;$i<10;$i++)
		{
			$dt = $this->dateadd($datepart,$addamount,$dt);
			$value = date($formatval,$dt);
			$key = date($formatkey,$dt);
			$ret[$key] = $value;
		}
		return $ret;
	}
}
?>
