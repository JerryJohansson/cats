<?php
/*
+--------------------------------------------------------------------------
|   Container Database Wrapper
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to access the page information. Part of the ezwebmaker CMS.
+---------------------------------------------------------------------------
*/

class Edit
{
	var $error;
	var $id;
	var $pid;
	var $mode;
	
	var $date_published;
	var $notify_published;
	var $email_owner;
	var $email_site_owner;
	var $email_editor;
	var $email_publisher;
	
	var $field_properties;
	var $edit_properties;
	var $search_properties;
	
	var $form_properties;
	var $properties;
	var $elements;
	var $status;
	
	function Edit($id=0,$mode='admin')
	{
		$this->mode=$mode;
		if($id != 0)
		{
			$this->setId($id);
			if($mode=='admin') $this->load($id,$mode);
		}
		else
		{
			$this->setId("");
			$this->setPid("");
			$this->setStatus(0);
		}
	}

	/*
	+---------------------------------------------------------------------------
	|  Load an object from the database 
	|		id of node
	|		edit flag to get unpublished document
	+---------------------------------------------------------------------------
	*/
	function _load($id,$mode='form',$action='search'){ // default to search form
		// global ADOBD object
		global $db, $RS;
		//$db->debug = true;
		$ret = false;
		
		switch($mode){
			case "admin":
				if($id == 0)
					$id = $this->getId();
				require_once(CATS_CLASSES_PATH . "navigation.php");
				$nav = new Navigation("edit",$mode);
				$sql = $nav->getFormSQL($id); // get sql to return node
				
				// get the editing page
				$rs = $db->Execute($sql);
				$RS = $rs;
				// Set data array before setting properties
				// if successful go and set default class properties
				if($data = $rs->GetArray())
				{
					// Set the properties
					$this->properties = $data[0];
					
					// 
					if(($this->properties['GROUP_MASK']+0)>0)
					{
						if($_REQUEST['id']==$id) auth_groups($this->properties['GROUP_MASK']);
					}
					// set default class properties
					$this->setId($this->properties['FIELD_ID']);
					$this->setPid($this->properties['FIELD_PID']);
					$this->setStatus($this->properties['STATUS']);
					$ret = true;
				}
				else
				{
					dprint(__FILE__,__LINE__,10,"<b>No matching row in DB Container::load()</b><br> field_id = ".$id);
					$ret = false;
				}
				break;
			// experimental
			// Sets the form properties according to the values set in the form_fields table
			// 
			case "form": 
				if($id == 0)
					$id = $this->getId();
				require_once(CATS_CLASSES_PATH . "navigation.php");
				
				// first we get the node details as this is the form itself
				$nav = new Navigation("edit","admin");
				$sql = $nav->getFormSQL($id);
				$rs = $db->Execute($sql);
				
				// if we have a form then lets go and get some
				// more details from its children
				if($this->form_properties = $rs->GetArray())
				{
					// Get the forms element records
					$sql = $nav->getSQL($id);
					$rs = $db->Execute($sql);
					
					// Set the properties to a multi dimensional array
					// holding all elements properties etc...
					if($this->properties = $rs->GetAll()){
						
						// create a properties array that is compatible with the form builder
						// Loop thru the array and create another one for the elements by using the 
						// values found in the form_fields (or properties) array
						foreach($this->properties as $key => $val){
							$this->elements[strtoupper($val['FIELD_NAME'])]=array(
								'label' => $val['FIELD_LABEL'], // label of the element
								'delim' =>"{$val['DB_DELIMITER']}", // the delimiter to use when building sql strings
								'operator' => $val['DB_OPERATOR'], // operator to use when building search vriteria string
								'value' => $val['FIELD_VALUE'], // default value for the element
								'func' => $val['FIELD_DISPLAY_FUNC'], // the function to use to display the element
								'set_func' => $val['FIELD_SET_FUNC'], // function to use to update elements value to db
								'params' => $val['FIELD_DISPLAY_PARAMS'], // parameters 
								'params2' => $val['OPTIONS'], // Xtra parameters for functions
								'table' => $val['FIELD_TABLE_NAME'], // table to use to populate child element values
								'field_id' => $val['FIELD_COLUMN_NAME'], // The field identifier of the table to use
								'mandatory' => $val['MANDATORY'], // The field identifier of the table to use
								'col' => $val['COL_NUM'], // what column to display this element in - 1 or 2
								'col_span' => $val['COL_SPAN']
							);
							
							//switch($val['FIELD_TYPE_ID']){
								//default:
									if($val['FIELD_DISPLAY_FUNC']!='' && function_exists($val['FIELD_DISPLAY_FUNC']))
										echo('$this->properties[' . $key . ']["element"] = ' . $val['FIELD_DISPLAY_FUNC'] . '("' . $val['FIELD_NAME'] . '","' . $val['FIELD_VALUE'] . '"' . (empty($val['FIELD_DISPLAY_PARAMS'])?'':',' . $val['FIELD_DISPLAY_PARAMS']) . ');');
									else
										echo('$this->properties[' . $key . ']["element"] = ' . $val['DISPLAY_FUNC'] . '(\$val' . (empty($val['FIELD_DISPLAY_PARAMS'])?'':',' . $val['FIELD_DISPLAY_PARAMS']) . ');');
										//$this->properties[$key]['element'] = html_draw_input_field( $val['FIELD_NAME'], db_escape_string($val['FIELD_VALUE']) );
									//break;
									echo("<br>");
							//}
						}
						
						dprint(__FILE__,__LINE__,10,"Success: properties retreived now lets try and load them");
						$ret = true;
					}else{
						dprint(__FILE__,__LINE__,0,"Error occured while trying to retreive properties array rs->GetAll");
						$ret = false;
					}
				} else {
					dprint(__FILE__,__LINE__,10,"<b>No matching row in DB Container::load()</b><br> field_id = ".$id);
					$ret = false;
				}
				break;
		}
		return $ret;
	}
	function load($id=0, $mode='form')
	{
		return $this->_load($id, $mode);
	}

/*
+---------------------------------------------------------------------------
|   Editor Fields Display
|		!NOTE FOR EXTRACT:
|			Make sure you use one of the non-overwriting extract_type values such
|			as EXTR_SKIP and be aware that you should extract $_SERVER, $_SESSION,
|			$_COOKIE, $_POST and $_GET in that order. 
|		!Example: extract($properties, EXTR_PREFIX_SAME, "cats_");
+---------------------------------------------------------------------------
*/
	/*
	+---------------------------------------------------------------------------
	|   drawForm
	|		Draws a table given a two dimensional array - uses tohtml library from ADODB
	|			Use this method to return a form using the class properties to populate
	|		Params:	
	|			$arr = Two dimensional array of values
	|		!Example: 
	|			$myarray[]=array($val1,$val2,...);
	|			$this->drawForm($myarray); // print html table
	+---------------------------------------------------------------------------
	*/
	function drawForm($arr){
		$out=array();
		foreach($arr as $key => $value) $out[]=array($key, $value);
		arr2html_form($out,'cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;"');
	}
	/*
	+---------------------------------------------------------------------------
	|  Set Properties Function for all child classes
	+---------------------------------------------------------------------------
	*/
	function setProperties($properties_in='',$prop_names='',$set_funcs='', $input_type='POST'){
		// eval the input type: $_GET, $_POST, $_SESSION
		eval('$input_values = $_'.addslashes($input_type).';');
		if(!is_array($input_values)) $input_values = $_POST; // fall back to post if input_type is incorrect
		$binaryImageLocation = $_FILES['PHOTO_OF_INCIDENT']['TMP_FILE']; //this just is a folder structure pointing to the file like a url.            if ( file_exists( $binaryImageLocation ) ) {                 echo "file exists";            }            else {             echo  "file doesn't exist";            }				   $handle = fopen( $binaryImageLocation, "rb"); // The "r" on the end means "open this file for reading only".fopen( ) doesn't actually read the contents of a file. All it does is to set a pointer to the file you want to open. It then returns what's call a file handle. All you're doing is telling PHP to remember the location of the file.	   //fread($handle,filesize($binaryImageLocation)); // I dont want to read it. I want the existing code to take care of getting it and putting it in the database	   array_push ($input_values, $handle); // 	   fclose($handle);
		// properties used to fill form fields etc...
		$properties=(isset($this->properties) && is_array($this->properties)) ? $this->properties : array();
		
		// get property dislay helper functions
		if(!is_array($prop_names)) $prop_names=$this->getPropertyNames();
		
		// get property set helper functions
		if(!is_array($set_funcs)) $set_funcs=$this->getPropertySetFunctions();
		
		// merge properties with properties_in
		if(is_array($properties_in)) $properties = array_merge($properties, $properties_in);
		
		// loop through expected property names and set their values using the $_POST
		// TODO: make other globals available i.e. $_GET, $_POST, or $_SESSION
		$len=count($prop_names);
		for($i=0;$i<$len;$i++) {
			$name=$prop_names[$i];
			$value=(is_string($input_values[$name]))?htmlspecialchars($input_values[$name]):$input_values[$name];
			if(isset($set_funcs[$i]) && $set_funcs[$i]!="") {
				// if there is a function set, use it!
				// if the function is not ended correctly we could get proplems with eval
				//  functions must end with a ( or , so we can properly finish off the function statement
				// example function db value: html_display_dropdown("param1","param2",  
				// example when compiled: html_display_dropdown("param1","param2", "$value");
				//  NOTE: the addition to the end of the function call: , "$value");
				//  the line below constructs the function call and inserts the value automatically as the last parameter
				//
				$value = $set_funcs[$i]($value);
			}
			// no function to use so insert default value
			$properties = array_merge($properties, array($name => $value));
		}
		// set the properties property to new value
		$this->properties = $properties;
		
	}
	
	function getProperties(){
		return $this->properties;
	}
	function getFieldPropertiesAdminEdit(){
		//return explode("|","FIELD_NAME|FIELD_LABEL|FIELD_GROUP_TYPE|FIELD_TYPE_ID|FIELD_LENGTH|FIELD_VALUE|FIELD_ATTRIBUTES|FIELD_TABLE_NAME|FIELD_COLUMN_NAME|FIELD_DISPLAY_FUNC|FIELD_DISPLAY_PARAMS|FIELD_SET_FUNC|FIELD_SET_PARAMS|COL_NUM|COL_SPAN|ROW_SPAN|COL_COUNT|MANDATORY|OPTIONS|SORT_ORDER|ROLE_MASK|GROUP_MASK|STATUS");
		return explode("|","FIELD_NAME|FIELD_LABEL|FIELD_GROUP_TYPE|FIELD_TYPE_ID|FIELD_LENGTH|FIELD_VALUE|ORIGIN_TABLE|REGISTER_ORIGIN|SORT_ORDER|ROLE_MASK|GROUP_MASK|STATUS");
	}
	function getFieldPropertiesAdminSearch(){
		return explode("|","FIELD_NAME|FIELD_LABEL|FIELD_GROUP_TYPE|FIELD_TYPE_ID|FIELD_LENGTH|FIELD_VALUE|FIELD_ATTRIBUTES|FIELD_TABLE_NAME|FIELD_COLUMN_NAME|FIELD_DISPLAY_FUNC|FIELD_DISPLAY_PARAMS|FIELD_SET_FUNC|FIELD_SET_PARAMS|COL_NUM|COL_SPAN|ROW_SPAN|COL_COUNT|MANDATORY|OPTIONS|SORT_ORDER|ROLE_MASK|GROUP_MASK|STATUS");
	}
	function getFieldPropertiesEdit(){
		$arr=$this->field_properties;
		if(is_array($arr)){
			$out=array('names'=>array(),'tables'=>array(),'columns'=>array(),'types'=>array(),'display_funcs'=>array(),'set_funcs'=>array());
			foreach($arr as $key => $val){
				$out['names'][]=$val['FIELD_NAME'];
				$out['tables'][]=$val['FIELD_TABLE_NAME'];
				$out['columns'][]=$val['FIELD_COLUMN_NAME'];
				$out['types'][]=$val['FIELD_GROUP_TYPE'];
				$out['display_funcs'][]=$val['FIELD_DISPLAY_FUNC'];
				$out['set_funcs'][]=$val['FIELD_SET_FUNC'];
			}
			return $out;
		}else{
			return false;
		}
	}
	function getFieldPropertiesSearch(){
		return $this->getFieldPropertiesEdit();
	}
	function getPropertyNames($type='edit', $mode='admin'){
		// return an array of display functions for each property
		$ret = false;
		switch($type){
			case 'edit':
				if($mode=='admin') // edit the fields themselves
					$ret = $this->getFieldPropertiesAdminEdit();
				else // get field names from properties
					$ret = $this->getFieldPropertiesEdit();
				break;
			case 'search':
				if($mode=='admin')
					$ret = $this->getFieldPropertiesAdminSearch();
				else
					$ret = $this->getFieldPropertiesSearch();
				break;
			default:
				$ret = $this->getFieldPropertiesAdminEdit();
				break;
		}
		return $ret;
	}
	function getPropertyDisplayFunctions($type='edit', $mode='form'){
		// return an array of display functions for each property
		switch($type){
			case 'edit': 
				if($mode=='admin')
					//explode("|","FIELD_NAME|FIELD_LABEL|FIELD_GROUP_TYPE|FIELD_TYPE_ID|FIELD_LENGTH|FIELD_VALUE|ORIGIN_TABLE|REGISTER_ORIGIN|SORT_ORDER|ROLE_MASK|GROUP_MASK|STATUS");
					return explode("|","||html_group_type_dd(|html_form_types_dd(|||html_action_origin_table_dd(|html_action_register_origin_dd(||html_user_roles_list(|html_user_groups_list(|html_form_fields_status_dd(");
					//return explode("|","||html_group_type_dd(|html_form_types_dd(||||html_form_tables_helper('FIELD_COLUMN_NAME',|html_form_columns_helper(\$props['FIELD_TABLE_NAME'],|html_form_functions_chooser('db_functions.php',||html_form_functions_chooser('db_functions.php',|||||||||html_user_roles_list(|html_user_groups_list(|");
				else
					return $this->fields['display_funcs'];
				break;
			case 'add':
				if($mode=='admin')
					return explode("|","||html_group_type_dd(|html_form_types_dd(|||html_form_origin_tables_dd(|html_form_register_origin_dd(||html_user_roles_list(|html_user_groups_list(|html_form_fields_status_dd(");
				else
					return explode("|","||||||||||||||||||||||");
				break;
			default:
				return explode("|","|||||||||||||||||||||||");
				break;
		}
	}
	function getPropertySetFunctions($type='edit', $mode='admin'){
		switch($type){
			case 'edit': case 'add':
				if($mode=='admin')
					return explode("|","|||||||||main_arr2mask|main_arr2mask|");
					//return explode("|","||||||||||||||||||||main_arr2mask|main_arr2mask|");
				else
					return explode("|","|||||||||||||||||||||||");
				break;
			case 'search':
				if($mode=='admin')
					return explode("|","|||||||||||||||||||||||");
				else
					return explode("|","|||||||||||||||||||||||");
				break;
			default:
				return explode("|","||||||||||||||||||||||||");
				break;
		}
	}
	
	
	function getFormAdd()
	{
		// set return value to false incase we have no props
		$values=false;
		$prop_names=$this->getPropertyNames("add","admin");
		$prop_funcs=$this->getPropertyDisplayFunctions("add","admin");
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$values=array();
			// empty the property list
			for($i=0;$i<$len;$i++) {
				$this->properties[$prop_names[$i]] = "" ;
			}
			if(is_array($this->properties)){
				$len=count($prop_names);
				for($i=0;$i<$len;$i++) {
					$name=$prop_names[$i];
					if($prop_funcs[$i]!=""){
						$func = $this->$set_funcs[$i];
						$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'("';
						eval('$values = array_merge($values, array($name => ' . $func . '"' . $this->properties[$name] . '");');
					}else{
						$values=array_merge($values,array($name => $this->properties[$name]));
					}
				}
			}
		}
		return $values;
	}

	function getFormEdit()
	{
		global $db;
		$func_return="";
		// set return value to false incase we have no props
		$values=false;
		$prop_names=$this->getPropertyNames("edit","admin");
		$prop_funcs=$this->getPropertyDisplayFunctions("edit","admin");
		
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$values=array();
			if(is_array($this->properties)){
				$len=count($prop_names);
				$props = $this->properties;
				$i=0;
				for($i=0;$i<$len;$i++) {
					$pname=$prop_names[$i];
					$value=$props[$pname];
					if($prop_funcs[$i]!=""){
						$func = $prop_funcs[$i];
						$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'("';
						//echo('$values = array_merge($values, array("'.$pname.'" => ' . $func . '"' . $pname . '","' . db_escape_string($value) . '")));');
						eval('$values = array_merge($values, array("'.$pname.'" => ' . $func . '"' . $pname . '","' . db_escape_string($value) . '")));');
					}else{
						//echo("'$pname' => array('label'=>'$lbl', 'value'=>'$value', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), \n");
						$values=array_merge($values,array($pname => html_draw_input_field($pname,db_escape_string($value))));
					}
				}
			}
		}
		return $values;
	}
	
	function getMask($in){
		return $this->addMask($this->getStatus(),$in);
	}
	function addMask($int_mask,$in){
		return ((int)($int_mask+0) & (int)$in)==0?((int)$int_mask+$in):((int)$int_mask+0);
	}
	function removeMask($int_mask,$out){
		return ((int)($int_mask+0) & (int)$out)==0?((int)$int_mask-$out):(int)$int_mask+0;
	}
	
	function update($id)
	{
		
		// global ADOBD object
		global $db;
		
		$this->setProperties();
		$sql_data = $this->getProperties();
		
		// Get the record to compare our update statement against
		$rs = $db->Execute("select * from form_fields where field_id = $id");
		
		// Create an update sql query string by comparing the fields
		// in the $sql_data array...can use the $_POST data directly here but
		// we should pass it through a validator and also set any special
		// variables before we insert/update
		$sql = $db->GetUpdateSQL($rs,$sql_data);
		//$_SESSION['messageStack']->add("SQL:<br>$sql","info");
		// Finally execute the generated update sql
		if(!empty($sql)){
			if($ok = $db->Execute($sql)){
				// return the id incase we don't know it???? DUH
				$_SESSION['messageStack']->add("Form Field details updated successfully.","success");
				return $id;
			}else{
				// return 0 which can't be a primary key value
				// TODO: add error logging
				$_SESSION['messageStack']->add("An error occured while updating Form Field details.<br>Error: ".$db->ErrorMsg());
				return 0;
			}
		}else{
			$_SESSION['messageStack']->add("No update to main record was neccessary", "success");
		}
		return $id;
	}
	
	
	function insert()
	{
		
		// global ADOBD object
		global $db;
		
		$this->setProperties();
		$sql_data = $this->getProperties();
		
		// Get the record to compare our update statement against
		$rs = $db->Execute("select * from form_fields where 1=2");
		
		// add the parent id to sql data
		$sql_data = array_merge($sql_data, array('FIELD_PID'=>$this->id));
		
		// Create an update sql query string by comparing the fields
		// in the $sql_data array...can use the $_POST data directly here but
		// we should pass it through a validator and also set any special
		// variables before we insert/update
		$sql = $db->GetInsertSQL($rs,$sql_data);				//echo "$sql";		//echo $sql;//----------------------------------------------------------------------------testcraigwood
		//$_SESSION['messageStack']->add("SQL:<br>$sql","info");		//echo "$_SESSION['messageStack']";		//echo $_SESSION['messageStack'];//---------------------------------------------------------testcraigwood		        
		// just doit already
		if($ok = $db->Execute($sql)){
			// return the value returned from db execute
			$_SESSION['messageStack']->add("Form Field details created successfully.","success");
			return $ok;
		}else{
			// this didn't work
			// TODO: add error logging
			$_SESSION['messageStack']->add("An error occured while adding Form Field details.<br>Error: ".$db->ErrorMsg());
			return false;
		}
	}
/*
+---------------------------------------------------------------------------
|   Accessors / Mutators
+---------------------------------------------------------------------------
*/

	function setId($val)
	{
		$this->id = $val;
	}
	
	function getId()
	{
		return $this->id;
	}
	
	function setPid($val)
	{
		$this->pid = $val;
	}
	
	function getPid()
	{
		return $this->pid;
	}
	
	function setStatus($val)
	{
		$this->status = $val;
	}
	
	function getStatus()
	{
		return $this->status;
	}
}
?>
