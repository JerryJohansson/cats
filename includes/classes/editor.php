<?php
if(defined('CATS_MAINTENANCE') && CATS_MAINTENANCE=="yes" && !isset($_GET['cats_maintenance'])){
	require_once(CATS_INCLUDE_PATH."top_redirect.php");
	exit;
}
?>

<?php
/*
+--------------------------------------------------------------------------
|   Container Database Wrapper
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to access the page information.
+---------------------------------------------------------------------------
*/

class Editor
{
	var $error;
	var $id;
	var $pid;
	var $module;
	var $page;
	var $action;
	
	function __construct($m, $p='search',$a='')
	{
		// set the module/page/action parameters
		// these usually come from the URL's query string (m,p,a)
		$this->module=$m;
		$this->page=$p;
		$this->action=$a;
	}

	/*
	+---------------------------------------------------------------------------
	|  Set Properties and return an array with the values populated from the $_POST
	|  or other global array
	+---------------------------------------------------------------------------
	*/
	
	function setPropertiesArray($table_name,$props_in, $input_type='POST'){
		
		// eval the input type: $_GET, $_POST, $_SESSION or $CUSTOM
		eval('$input_values = $_'.addslashes($input_type).';');
		
		// set return value to false incase we have no props
		$values=false;		
		if(is_array($props_in))
		{
			$values=array();
			// iterate through properties and create an array of
			// name/value pairs
			foreach($props_in as $name => $props){
				// only do it if the table property does not exist of if the
				// table property is the current table
				if(isset($input_values[$name]) && (!isset($props['table']) || $props['table']==$table_name) ){
					// get the value from the $_POST array or what ever global
					// array is used as $input_type. This defaults to use the $_POST var
					$value=$input_values[$name];
					
					// merge name/value array to return array
					$values=array_merge($values,array($name=>$value));//db_escape_string($value)));
				}
			}
		}
		//echo("<hr>Values=");
		//print_r($values);
		//echo("<hr>");
		// return the values array otherwise return false
		return $values;
	}

	/*
	+---------------------------------------------------------------------------
	|   drawForm
	|		Draws a table given a two dimensional array - uses tohtml library from ADODB
	|			Use this method to return a form using the class properties to populate
	|		Params:	
	|			$arr = Two dimensional array of values
	|		!Example: 
	|			$myarray[]=array($val1,$val2,...);
	|			$this->drawForm($myarray); // print html table
	+---------------------------------------------------------------------------
	*/
	function drawForm($arr){
		require_once(CATS_ADODB_PATH . 'tohtml.inc.php');
		//print_r($arr);
		$out=array();
		foreach($arr as $key => $value) $out[]=array($key, $value);
		arr2html_form($out,'cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"');
		//<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
	}

	/*
	+---------------------------------------------------------------------------
	|   buildForm
	|		Draws a table given a two dimensional array - uses tohtml library from ADODB
	|			Use this method to return a form using the class properties to populate
	|		Params:	
	|			(array)$props_in = Multi dimensional array of name/value pairs
	|     (string)$table = Table name used in sql to retrieve data from db
	|     (string)$field_id = The column name of the primary key
	|     (int)$id = me thinks this should be the actual ID value of the primary key ;)
	|     (array)$column_headers(defaults to false) = array of column headers to use...very unlikely but there newayz
	|     (string)$parameters = table attributes. Defaults to...
	| <TABLE cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"></TABLE>
	|		!Example: 
	|			$myarray = array($name1 => $value1, $name2 => $value2);
	|			$this->buildForm($myarray,'my_table','my_table_id',1); // print html form table
	+---------------------------------------------------------------------------
	*/
	function buildForm($props_in, $table = '', $field_id = '', $id = '', $sql_in = false, $column_headers = false, $parameters = 'cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"' )
	{
		global $TABLES;
		// set to false if we want a new form and not an edit form
		$use_rs_values = true;
		
				// show fatal error screen if props_in is not an array
		if( !is_array($props_in) ) {
			global $m, $p;
			$_SESSION['messageStack']->add("The properties parameter in editor->buildForm is not an array. Please check the properties file of the module '<u>$m</u>' for the '<u>$p</u>' page and correct the problem.");
			include_once(CATS_INCLUDE_PATH . 'fatal_error.inc.php');
			exit;
		}
		//print_r($TABLES);
		//print_r($sql_in);
		// if table is empty then we create a recordset using the global TABLES var...
		// get a row so we can get field types etc... if we do this then we assume that this 
		// is a new form and in which case we ignore the values in recordset and assign the default 
		// values from the props_in array
		if( (is_bool($sql_in)) && $table=="" ){
			$sql = "select * from {$TABLES['view']} where rownum = 1";
			$rs = db_query($sql);
			$use_rs_values = false;
		}else{
			// build default sql and get recordset or assume sql_in is a recordset and assign it to rs var
			if(empty($sql_in)){ 
				$sql = "select * from $table where $field_id = $id";
				$rs = db_query($sql);
			}else{
				$sql = "";
				$rs = $sql_in;
			}
		}
		// var_dump($rs);die();
		// create array to hold the transformed values
		$arr = array();
		
		// iterate through recordset...should only be one row
		// if there are more then something has gone wrong
		// TODO: add error logging if recordset contains more than one row
		

		while($row = $rs->FetchRow()){
			// get field count and iterate through the fields to get there 
			// types, length and set additional parameters/attributes etc
			$fcnt=$rs->FieldCount();
			
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);

				// get the field type...types shown below
				$type = $rs->MetaType($fld->type);
				//print_r($fld);
				
				//echo($row[$fld->name]."<BR>");
/* Types::
# C: Character fields that should be shown in a <input type="text"> tag.
# X: Clob (character large objects), or large text fields that should be shown in a <textarea>
# D: Date field
# T: Timestamp field
# L: Logical field (boolean or bit-field)
# N: Numeric field. Includes decimal, numeric, floating point, and real.
# I: Integer field.
# R: Counter or Autoincrement field. Must be numeric.
# B: Blob, or binary large objects.
*/
				switch($type){
					case 'D': // format the date value
						// $arr[$fld->name] = cats_date_short($row[$fld->name]);
						$arr[$fld->name] = str_replace('/', '-', $row[$fld->name]);
						
						break;
					case 'T': // format the date value
						// $arr[$fld->name] = cats_date_short($row[$fld->name]);
					
					  
						if(!is_null($row[$fld->name])){
							$row[$fld->name] = str_replace('/', '-', $row[$fld->name]);
						    $row[$fld->name] = str_replace(':', '.', $row[$fld->name]);

							$date = date_create_from_format('d-M-y h.i.s.u A', $row[$fld->name]);
							$arr[$fld->name] = $date->format('d-M-y h:i');
						}
						else
						{
							 $arr[$fld->name] = $row[$fld->name];
						}
						
					break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						// if maxlength does not appear in the properties array then
						// we add an element to the properties array called maxlength.
						// This value will be used as the maxlength for the text input field
						//echo 'text value here - '.$fld->name.':'.$fld->max_length.'<br>';
						if(isset($props_in[$fld->name]) && !isset($props_in[$fld->name]['maxlength']))
							$props_in[$fld->name]['maxlength'] = $fld->max_length;
						
						// remove unwanted characters so it will display correctly
						// in the input field control
						//print($row[$fld->name]."<br>");addslashes(($row[$fld->name]));//
						$arr[$fld->name] = htmlentities(addslashes($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
		}
			
		if(count($arr)!=0)
		{
			// only fill props_in if we have an edit form
			if($use_rs_values){
				// add the values to the properties array by iterating through
				// the properties array and merging values with properties
				foreach($props_in as $name => $props){
					// if($arr[$name]!='')comment dev
					if(isset($arr[$name])&&$arr[$name]!='')
					{
						// FIXED 12th March 2006, cameron@sandboxsw.com
						// Do not strip slashes if the output contains a file path
						// Otherwise we lose \\UNC paths						
						if(stristr($props["func"], "file") || stristr($props["func"], "folder"))
						{
							$arr[$name] = str_replace("\\\\", "\\\\\\\\", $arr[$name]);
						}						
						$props_in[$name]=array_merge($props_in[$name],array("value" => stripslashes($arr[$name]) ) );
					}
				}
				//print_r($props_in);
			}
		}

		$s = "<table $parameters>";//';print_r($arr);
		// add column headers if desired...silly idea really but just in case
		if ($column_headers) {
			$s .= "<tr>\n";
			for ($i=0; $i<sizeof($column_headers); $i++) {
				$s .= "	<th>{$column_headers[$i]}</th>\n";
			}
			$s .= "</tr>\n";
		}

		foreach($props_in as $name => $props){
			
			// get the string used for the label
			$label = $props["label"];
			
			// get default value of element
			//comment  dev
			// $value = $props["value"];	
			$value = isset($props["value"])?$props["value"]:null;		
			
			// draw a control or not
			$draw_control = true;
			
			// draw a control spanning the whole row
			$render_row_control = false;
			
			// use sequence function of this class
			$render_sequence = false;
			
			// if we have a group type of row_header then draw a heading row
			if(array_key_exists('group', $props)){
				
				switch($props['group']){
					case 'row_title':
						$s .= "<tr id=\"row_$name\">\n";
						$s .= " <th class=\"row_title\" colspan=\"4\"><span >$label</span></th>\n";
						$s .= "</tr>\n";
						$draw_control = false;
						break;
					case 'row_header':
						$s .= "<tr id=\"row_$name\">\n";
						$s .= " <th class=\"row_header\" colspan=\"4\"><span >$label</span></th>\n";
						$s .= "</tr>\n";
						$draw_control = false;
						break;
					case 'row_sequence':
						$s .= "<tr id=\"row_$name\">\n";
						$s .= " <th class=\"row_sequence\" colspan=\"4\"><span >$label</span></th>\n";
						$s .= "</tr>\n";
						$render_sequence = true;
						$render_row_control = true;
						break;
					default:
						$s .= "<tr id=\"row_$name\">\n";
						$s .= " <th class=\"row_header\" colspan=\"4\"><span >$label</span></th>\n";
						$s .= "</tr>\n";
						$render_row_control = true;
						break;
				}
			}
			//print_r($props);
			if($draw_control){
				// get the function name to be used to create the element
				$func = $props["func"];
				
				
				// get the string used for the label
				$help_tip = (array_key_exists('help', $props))? $props["help"]:'';
				
				// get the column number:
				// Which column this element belongs to
				
				// $col = $props["col"];comment dev
				$col = isset($props["col"])?$props["col"]:false;
				
				// get the column span
				// Used to span a label/element pair across two columns(label/element pair columns that is)
				// $col_span = $props["col_span"];comment dev
				$col_span = isset($props["col_span"])?$props["col_span"]:false;

				
				// get security group mask
				// Used to limit who sees what controls etc
				if(isset($props["group_mask"])){
					$group_mask = $props["group_mask"];
					if (is_numeric($group_mask) && is_numeric($_SESSION['user_details']['groups']))//comment dev check php 7.2 
					if((($group_mask+0) & ($_SESSION['user_details']['groups']+0)) == 0) continue;
				}


				
				// if a function exists then use it
				if($func!=""){
					
					// If this is a text field or to be exact, a varchar/char/clob db field
					// then the max length of the field is used to limit overflow of text entry.
					$max_length = (isset($props['maxlength']))?$props['maxlength']:false;
					
					// get parameters from properties:
					// These are parameters passed into the function and are used
					// to populate the elements attributes
					if(isset($props["params"])) {
						$params = $props["params"];
						
					}else $params = "";


					if(!preg_match("/title=/",$params)){
						$params .= ' title=\"'.$label.'\" ';
					}
					
					
					// check if we have a max length available and add it to the params
					// parameter. This will be used in as the third parameter of the `func` function
					if($max_length!=false) $params .= ' maxlength=\"'.$max_length.'\" ';
					
					// get extra parameters for function:
					// This is a string to be used as the forth/nth parameter/s of
					// the `func` function
					if(isset($props["params2"])) $xtra_params = $props["params2"];
					else $xtra_params = "";
					
					
					
					// start building the parameters string
					$param = ($params!="")?',"'.$params.'"':"";
					
					// if params2 is empty, skip it otherwise
					// add the xtra parameters used for the fourth parameter of `func` function
					if($xtra_params!=""){
						// if params is empty then add the third parameter as an empty string
						// which should be the default for all html output functions
						// otherwise just append the extra parameter
						$param .= ($params!="")?','.$xtra_params:',"",'.$xtra_params;
					}
					


					if($render_sequence){
						// if the value is an array then there are special rules in place.
						// 1. params2 can't be used
						//$value = array($label => $func($name,$value,$props['sql']));
						//print_r($value);
						if (array_key_exists('ext', $props) && $props['ext']==true){
						
							
							$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'(';
							//echo('$value = array("'.$label.'" => ' . $func . '"' . $name . '","' . $value . '"'.$param.'));');
							eval('$value = array("'.$label.'" => ' . $func . '"' . $name . '","' . $value . '"'.$param.'));');
						}else
						{
						
							 $value = array($label => $this->buildSequentialForm($name,$value,$props['sql'],$xtra_params));
						}
					}else{
						
						// fix the `func` name if neccessary
						$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'(';
						// evaluate the whole thing, cross your fingers ;) and hopefully 
						// it will evaluate to something real
						eval('$value = array("'.$label.'" => ' . $func . '"' . $name . '","' . $value . '"'.$param.'));');

					}
				}else{ 

					// use default input field if no display function exists
					$value=array($label => html_draw_input_field($name,$value));
				}
				
				if($render_row_control){
					$s .= "<tr id=\"row_$name\">\n";
					$s .= "	<td colspan=\"4\">\n";
					$s .= "		{$value[$label]}\n";
					$s .= "	</td>\n";
					$s .= "</tr>";
				}else{

					
					// render label/field pair column
					switch($col){
						// used for hidden fields - do not render hidden fields in a cell with label etc
						// hide the hidden field between rows
						case 0:
							$s .= $value[$label];
							break;
						case 1:
							switch($col_span){
								case 1:
									$s .= "<tr id=\"row_$name\">\n";
									$s .= "	<td class=\"label\"><label title1=\"$help_tip\" for=\"{$name}\">$label</label></td>\n";
									$s .= "	<td>{$value[$label]}</td>\n";
									break;
								case 2:
									$s .= "<tr id=\"row_$name\">\n";
									$s .= "	<td class=\"label\"><label title=\"$help_tip\" for=\"{$name}\">$label</label></td>\n";
									$s .= "	<td colspan=\"3\">{$value[$label]}</td>\n";
									$s .= "</tr>\n";
									break;
								// shared column: shares label with another control
								case 3:
									$s .= "<tr id=\"row_$name\">\n";
									$s .= "	<td class=\"label\"><label title=\"$help_tip\" for=\"{$name}\">$label</label></td>\n";
									$s .= "	<td colspan=\"3\">\n";
									$s .= "		{$value[$label]}\n";
									break;
							}
							break;
						// use this param to render label/field pair in second label/field column
						case 2:
							$s .= "	<td class=\"label\"><label title=\"$help_tip\" for=\"{$name}\">$label</label></td>\n";
							$s .= "	<td>{$value[$label]}</td>\n";
							$s .= "</tr>\n";
							break;
						// end shared label
						case 3:
							$s .= "		{$value[$label]}\n";
							$s .= "	</td>\n";
							$s .= "</tr>\n";
							break;
					}
				} // render row control
			} // draw control
		} // for loop
		$s .= '</table>';
		return $s;
	}
	
	
	function update($id=0)
	{
		
		// global ADOBD object
		global $db, $TABLES, $FIELDS;
		// turn debug on if neccessary
		//$db->debug = true;
		
		// set fields array
		$fields = array();
		// if we have tab array then merge all tabs to one array
		if(isset($FIELDS[0])){
			foreach($FIELDS as $arr) $fields = array_merge($fields, $arr);
		}else	$fields = $FIELDS;
		
		if($id==0){
			$_SESSION['messageStack']->add("Unable to update record with an invalid identifier. The records identifier must be greater than 0.");
			return 0;
		}
		// returns an array used for the update/insert
		$sql_data = $this->setPropertiesArray($TABLES['table'],$fields);
		// print_r($sql_data);
		// sql to be used for recordset object when comparing current record to posted data.
		// The GetUpdateSQL will figure this out for us. lucky us ;)
		$sql = "select * from ".$TABLES['table']." where ".$TABLES['id']." = $id";
		
		// get a recordset to use as the base of the insert statement
		$rs = $db->Execute($sql);
		
		// Create an update statement based on the recordset($rs) and
		// sql data array($sql_data)
		$sql = $db->GetUpdateSQL($rs,$sql_data);
		
		// Finally execute the generated update sql
		if(!empty($sql)){
			if($ok = $db->Execute($sql)){
				// return the id incase we don't know it???? DUH
				$_SESSION['messageStack']->add("The Record update was a success. " ,"success");
				return $id;
			}else{
				// return 0 which can't be a primary key value
				// TODO: add error logging
				$_SESSION['messageStack']->add("An error occured while updating record for {$this->module}.<br>Error: ".$db->ErrorMsg());
				return 0;
			}
		}else{
			$_SESSION['messageStack']->add("No update to main record was neccessary", "success");
		}
		return $id;
	}
	
	
	function insert()
	{
		// global ADOBD object
		global $db, $TABLES, $FIELDS;
		//$db->debug = true;
		
		// set fields array
		$fields = array();
		// if we have tab array then merge all tabs to one array
		if(isset($FIELDS[0])){
			foreach($FIELDS as $arr) $fields = array_merge($fields, $arr);
		}else	$fields = $FIELDS;
		
		// set the table type to retrieve table name from global $TABLES array
		$tbl_type = 'edit';
		
		// returns an array used for the update/insert
		$sql_data = $this->setPropertiesArray($TABLES['table'],$fields);
		
		// sql used for dummy recordset object used in GetInsertSQL
		$sql = "select * from ".$TABLES['table']." where 1=2";//.$TABLES[$tbl_type]['id']." = 1";
		
		// get a recordset to use as the base of the insert statement
		$rs = $db->Execute($sql);
		
		// Create an insert statement based on the recordset($rs) and
		// sql data array($sql_data)
		$sql = $db->GetInsertSQL($rs,$sql_data);
		
		// just doit already
		if($ok = $db->Execute($sql)){
			
			// return the value returned from db execute
			$_SESSION['messageStack']->add("The Record was successfully added to {$this->module}. ","success");
			return $ok;
		}else{
			
			// this didn't work
			// TODO: add error logging
			$_SESSION['messageStack']->add("An error occured while adding record to {$this->module}.<br>Error: ".$db->ErrorMsg()." Incident_number: ".$_POST['INCIDENT_NUMBER'] . "SQL: " .$sql);
			return false;
		}
	}
	
	function delete($id){
		global $TABLES, $db;
		if($id>0){
			$sql = "delete from {$TABLES['table']} where {$TABLES['id']} = $id";
			//echo $sql;
			if($ret = $db->Execute($sql)){
				// return the value returned from db execute
				$_SESSION['messageStack']->add("The Record was successfully deleted from {$this->module}.","success");
				return $ret;
			}else{
				// this didn't work
				// TODO: add error logging
				$_SESSION['messageStack']->add("An error occured while deleting record from {$this->module}.<br>Error: ".$db->ErrorMsg());
				return false;
			}
		}else{
			$_SESSION['messageStack']->add("The Record was not deleted because of an invalid identifier value. Expecting a value greater than 0.","warning");
			return false;
		}
	}
	
	
	/*
	+---------------------------------------------------------------------------
	|   buildSequentialForm
	|		Draws a table given a two dimensional array
	|			Use this method to return a form using the class properties to populate
	|		Params:	
	|			(string)$name = Name of sequence
	|     (array)$props_in = Multi dimensional array of name/value pairs
	|     (string)$sql_in = sql used to build sequence
	|     (string)$parameters = dunno yet :(
	|     (array)$table_parameters = main tables attributes
	|		!Example: 
	|			$myarray = array($name1 => $value1, $name2 => $value2);
	|			$this->buildForm('name',$myarray,'select field1, field2 from table_name where f_key_id = 1'); // print html form table
	+---------------------------------------------------------------------------
	*/
	function buildSequentialForm($name, $props_in, $sql_in, $parameters = '', $table_parameters = 'cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"' )
	{
		global $RS;
		//$db->debug = true;
		$func_return="";
		
		if( is_array($props_in) ){

			// create record set
			$rs = db_query($sql_in);

			// begin the return string with a container div and table
			$s = "<table id=\"{$name}_table\" $table_parameters><thead><tr>";//';print_r($arr);
			$ncols = $rs->FieldCount();
			for ($i=0; $i < $ncols; $i++) {	
		
				$field = $rs->FetchField($i);

				$field_match = preg_replace("/^'(EDIT|DELETE):(.*)/", "$1", $field->name);
				//echo("field_match = $field_match <br>fname = {$field->name}<br>");
				switch($field_match){
					case 'EDIT':
						//$fname = $props_in[$field->name]['label'];
						$col_attribs = ' width="5" ';
						//echo $field->name."<br>";
						$edit_button=explode(":",strtolower($field->name));
						$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
						$fname = '';//$edit_button[0];
						$edit_url = $edit_button[2];
						$edit_id = $edit_button[3];
						if(count($edit_button)>3)
							$edit_close = $edit_button[4];
						else
							$edit_close = '';
						//print_r($edit_button);
						$fname='';
						break;
					case 'DEL': case 'DELETE':
						//$fname = $props_in[$field->name]['label'];
						//print_r($edit_button);
						$col_attribs = ' width="5" ';
						$delete_button=explode(":",strtolower($field->name));
						$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
						$delete_url = $delete_button[2];
						$delete_id = $delete_button[3];
						if(count($delete_button)>3)
							$delete_close = $delete_button[4];
						else
							$delete_close = '';
					
						$fname='';
						break;
					default:
						$fname = $props_in[$field->name]['label'];
						break;
				} // end::switch fname
				if (strlen($fname)==0) $fname = '&nbsp;';
				
				$thattribs=isset($col_attribs)?($col_attribs!='')?$col_attribs:'':'';
				
				
				$s .= "<th$thattribs>$fname</th>";
			}
			$s .= "</tr></thead>";
			
			$row_attribs='';//' onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';
			$numoffset = isset($rs->fields[0]) ||isset($rs->fields[1]) || isset($rs->fields[2]);
			$i_hilite = 0;
			
			$s .= "<tbody>";
			$srow = "";
			$s_add_row = "";
			$add_row = "";
			$add_row_only = false;
			
			// if we have no records then create sql to get one row so we can build the add item row
			// for dynamic creation via javascript
			if($rs->EOF){
				// strip the where clause from sql_in
				// and create new query to get the first row
				$sql_tmp = preg_replace("/( where )(.*)/i", "$1 rownum = 1 ", $sql_in);
				//echo ($sql_tmp);
				$rs = db_query($sql_tmp);
				$add_row_only = true;
			}
			
			while (!$rs->EOF) {
				$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
				$i_hilite++;
				$irows = ($i_hilite-1);
				
				$srow = "<TR valign=top $row_hilite $row_attribs>";
				
				for ($i=0; $i < $ncols; $i++) {
					if ($i===0) $v=($numoffset) ? $rs->fields[0] : reset($rs->fields);
					else $v = ($numoffset) ? $rs->fields[$i] : next($rs->fields);
					$field = $rs->FetchField($i);
					
					
					$field_match = preg_replace("/^'(EDIT|DELETE):(.*)/", "$1", $field->name);
					//echo("field_match = $field_match <br>fname = {$field->name}<br>");
					switch($field_match){
						case 'EDIT':
							//$fname = $props_in[$field->name]['label'];
							$col_attribs = ' width="5" ';
							$edit_button=explode(":",$v);
							$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
							$edit_url = $edit_button[2];
							$edit_id = $edit_button[3];
							if(count($edit_button)>3)
								$edit_close = $edit_button[4];
							else
								$edit_close = '';
							$srow .= "	<TD width=5><a href='" . $edit_protocol . $edit_url . $rs->Fields($edit_id) . $edit_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[0]) . "_bg.gif' border='0' /></a></TD>\n";
							break;
						case 'DEL': case 'DELETE':
							//$fname = $props_in[$field->name]['label'];
							$col_attribs = ' width="5" ';
							$delete_button=explode(":",$v);
							$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
							$delete_url = $delete_button[2];
							$delete_id = $delete_button[3];
							if(count($delete_button)>3)
								$delete_close = $delete_button[4];
							else
								$delete_close = '';
							if($delete_protocol!=''){
								//$s .= "	<TD width=5><a href='" . $delete_protocol . $delete_url . $irows . $delete_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($delete_button[0]) . "_bg.gif' border='0' /></a></TD>\n";
								$srow .= "<TD width=5><div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div></TD>";
								if($add_row=='') $s_add_row .= "<TD width=5><div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div></TD>";
							}else{
								$srow .= "<TD width=5><input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" /></TD>";
							}
							break;
						default:
							$v = trim($v);
							if (array_key_exists('func', $props_in[$field->name])) 
								$func = $props_in[$field->name]['func'];
							else
								$func = "html_draw_input_field";
							
							$subparameters = '';//add dev
							if (array_key_exists('params', $props_in[$field->name])) 
								$subparameters = $props_in[$field->name]['params'];
							// if the style attribute isn't set then set the control to 100% width
							// if(strpos($params," style=")===false) $subparameters .= ' style="width:100%;" ';comment dev
							if(isset($params)&&strpos($params," style=")===false) $subparameters .= ' style="width:100%;" ';
							$srow .= "<TD>". $func($field->name.'[]', $v, $subparameters) ."</TD>";
							if($add_row=='') $s_add_row .= "<TD>". $func($field->name.'[]', '', $subparameters) ."</TD>";
							break;
					} // end::switch fname
					
				} // for
				$srow .= "</TR>";
				$rows = 0;//add dev
				if($add_row=='') $add_row .= $s_add_row . "</TR>";
				if(!$add_row_only) $s .= $srow;
				$rows += 1;
		
				$rs->MoveNext();
			
			} // while
			//$add_row = $s_add_row;
			$s .= "</tbody></table>";
			$s .= "<table id=\"{$name}_add\" style='display:none;'>";
			$s .= $add_row;
			$s .= "</table>";
			if (($this->page != 'printpreview') && ($parameters != 'READONLY')) {
				$s .= '<div id="'.$name.'_controller" style="background-image:url('.WS_STYLE_PATH.'images/toolbar_bg.jpg)"><input type="button" name="cats::btn_'.$name.'_add" value="Add '.$name.'" onclick="add_sequence_row(this, \''.$name.'\');" /></div>';
			}
			//$s .= '</td></tr></tfoot></table>';
			
		} // props_in is array
		
		return $s;
	}
	
	
	
	
	
	
	
	// -----------------------------------
	// PERFORM AN INSERT WITH AN SQL STATEMENT
	// -----------------------------------
	function sql_insert($my_sql)
	{
		// global ADOBD object
		global $db;

		if($ok = $db->Execute($my_sql))
			return $ok;
		else
			return false;
	}
	
	
	
	function getMask($in){
		return $this->addMask($this->getStatus(),$in);
	}
	function addMask($int_mask,$in){
		return ((int)($int_mask+0) & (int)$in)==0?((int)$int_mask+$in):((int)$int_mask+0);
	}
	function removeMask($int_mask,$out){
		return ((int)($int_mask+0) & (int)$out)==0?((int)$int_mask-$out):(int)$int_mask+0;
	}

/*
+---------------------------------------------------------------------------
|   Accessors / Mutators
+---------------------------------------------------------------------------
*/
	
	function setError($val)
	{
		$this->error = $val;
	}
	
	function getError()
	{
		return $this->error;
	}
/*
+------------------------------------------------------------------------
|  Comment: 	
	function setId($val)
	{
		$this->id = $val;
	}
	
	function getId()
	{
		return $this->id;
	}
	
	function setPid($val)
	{
		$this->pid = $val;
	}
	
	function getPid()
	{
		return $this->pid;
	}
		
	function setMode($val)
	{
		$this->mode = $val;
	}
	
	function getMode()
	{
		return $this->mode;
	}
	
	function setType($val)
	{
		$this->type = $val;
	}
	
	function getType()
	{
		return $this->type;
	}

+------------------------------------------------------------------------
*/}
?>
