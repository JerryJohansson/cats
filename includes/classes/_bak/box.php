<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Box class
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------

  Example usage:

  $heading = array();
  $heading[] = array('params' => 'class="menuBoxHeading"',
                     'text'  => BOX_HEADING_TOOLS,
                     'link'  => tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('selected_box')) . 'selected_box=tools'));

  $contents = array();
  $contents[] = array('text'  => SOME_TEXT);

  $box = new Box;
  echo $box->infoBox($heading, $contents);
*/

class Box extends Table {
	function box() {
		$this->heading = array();
		$this->contents = array();
	}

	function infoBox($heading, $contents) {
		$this->table_parameters = 'class="admin" style="background:#ccc;"';
		$this->table_row_parameters = 'class="infoBoxHeading"';
		$this->table_data_parameters = 'class="infoBoxHeading"';
		$this->heading = $this->Table($heading);

		$this->table_row_parameters = '';
		$this->table_data_parameters = 'class="infoBoxContent"';
		$this->contents = $this->Table($contents);
		
		return $this->heading . $this->contents;
	}

	function menuBox($heading, $contents) {
		$this->table_cellspacing = '0';
		$this->table_cellpadding = '0';
		$this->table_parameters = 'class="admin" style="background:#fff;"';
		$icon = (empty($contents))?'icon_plus.gif':'icon_minus.gif';
		$caption = array('name'=>preg_replace("/\s/","_",$heading[0]['text']),'link'=>$heading[0]['link'],'text'=>$heading[0]['text'],'icon'=>DIR_WS_IMAGES.$icon);
		$this->contents = $this->Table($contents,$caption);
		return $this->contents;
	}
}
?>
