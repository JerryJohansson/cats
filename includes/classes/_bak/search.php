<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Search Class
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

class Search
{
    function Search()
    {
    }

    function endRowCheck($pagecount, $numimages)
    {
        if($numimages==$pagecount)
				{
            $colspan = (int)SEARCH_THUMB_COLS - $pagecount % (int)SEARCH_THUMB_COLS;
            return '<td colspan="$colspan"></td></tr>';
        }
        else
        {
            return '';
        }
    }
    
    function adjustSearchTerms($string)
    {
        $stringArray = explode(' ', $string);
        foreach($stringArray as $key => $value)
        {
            // If it ends in s, remove the s and add a *
            if( substr($value, strlen($value)-1, 1) == "s" )
            {
                $stringArray[$key] = substr($value, 0, strlen($value)-1);
            }
            $stringArray[$key] = $stringArray[$key].'*';
        }
        $string = implode($stringArray, ' ');
        return $string;
    }
    
    function buildThumb($container)
    {
        if(SEARCH_THUMB_ON == true)
        {
            // Do a quick check to see if a thumbnail is set and exists
            if($container->getThumb() != '' && file_exists(DIR_FS_THUMBS.$container->getThumb()))
                $thumb = '<img src="'.DIR_WS_THUMBS.$container->getThumb().'" border="0">';
            else
                $thumb = '';
        }
        else
        {
            $thumb = '';
        }
        return $thumb;
    }

    function getSearchThumbs($string, $pagenum)
    {
        require_once(DIR_WS_CLASSES."template.php");
        $tpl = new Template("templates/search_results_thumbs.htm");

        $adjustedString = $this->adjustSearchTerms($string);
        
        // Database wrappers
        require_once(DIR_WS_CLASSES."container.php");
        $container = new Container;

        $rows = '';
        $pagelinks = '';
        // Set the counter to keep track of which image we are upto
        $imagecount=1;
        // Number of thumbs allowed per page
        $cells = (int)SEARCH_THUMB_COLS * (int)SEARCH_THUMB_ROWS;
        $offset=$cells * ($pagenum-1);
        // Loop through each page and fill the loop
        $result = $container->getSearchLimited($adjustedString, $offset, $cells, "");
        $totalnumimages = $container->numSearch($adjustedString, "");
        $numimages = sizeof($result);

        foreach($result as $searchId)
        {
            $container->load($searchId);
            $link = DIR_WS_PUBLIC.strtolower(get_class($container)).'.php?id='.$container->getId();
            $thumb = $this->buildThumb($container);

            // Check if we want a summary with the title
            if(SEARCH_SUMMARY_ON == true)
                $summary = substr( stripslashes( strip_tags( $container->getContent() ) ), 0, (int)SEARCH_SUMMARY_LENGTH );
            else
                $summary = '';


                /*********** Do Thumbnail Display ************/
                if( ($imagecount % (int)SEARCH_THUMB_COLS) == 1 ){ // First <td> in the row
                    $rows .= '<tr valign="bottom"><td valign="bottom" align="center"><a href="'.$link.'">'.$thumb.'<br>'.$container->getTitle().'</a></td>';
                    $rows .= $this->endRowCheck($imagecount, $numimages);
                }elseif( ($imagecount % (int)SEARCH_THUMB_COLS) == 0 ){ // End of the row
                    $rows .= '<td valign="bottom" align="center"><a href="'.$link.'">'.$thumb.'<br>'.$container->getTitle().'</a></td></tr>';
                }else{ // Middle <td>
                    $rows .= '<td valign="bottom" align="center"><a href="'.$link.'">'.$thumb.'<br>'.$container->getTitle().'</a></td>';
                    $rows .= $this->endRowCheck($imagecount, $numimages);
                }
                $imagecount++;
                /*********** Do Thumbnail Display ************/
        }

            /********** Next Container Link Generation *************/
            if(($totalnumimages/$cells) > 1)
            {
                $previous=$pagenum-1;
                if($pagenum > 1)
                    $pagelinks .= '<a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$previous.'">&lt;-- Previous</a>';
                $i=1;
                while($i < $pagenum)
                {
                    $pagelinks .= '| <a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$i.'">'.$i.'</a>';
                    $i++;
                }
                $pagelinks .= '| '.$pagenum.' ';
                $i++;
                while(($i-1) < ($totalnumimages/$cells))
                {
                    $pagelinks .= '| <a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$i.'">'.$i.'</a>';
                    $i++;
                }
                if($totalnumimages >= ($imagecount + $offset)){
                    $next=$pagenum+1;
                    $pagelinks .= '| <a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$next.'">Next--&gt;</a>';
                }
            }
            /********** Next Container Link Generation *************/

        if($numimages == 0)
            $rows = '<br>&nbsp;&nbsp;<b>No Results found for:</b> '.urldecode($string);

        $values = array(
                    "rows"        => $rows,
                    "pagelink"    => $pagelinks
                        );
        $tpl->parse($values);
        return $tpl->template;
    }


    function getSearchList($string, $pagenum)
    {
        // Load up the site definition and template
        require_once(DIR_WS_CLASSES."template.php");
        $tpl = new Template("templates/search_results_rows.htm");
        $tpl->parse_loop("searchloop");

        $adjustedString = $this->adjustSearchTerms($string);
        
        // Database wrappers
        require_once(DIR_WS_CLASSES."container.php");
        $container = new Container;

        $rows = '';
        $pagelinks = '';
        // Set the counter to keep track of which page we are upto
        $searchcount=1;
        // Number of thumbs allowed per page
        $offset=(int)SEARCH_NUM_ROWS * ($pagenum-1);
        // Loop through each page and fill the loop
        $result = $container->getSearchLimited($adjustedString, $offset, (int)SEARCH_NUM_ROWS, "");
        $totalnumpages = $container->numSearch($adjustedString, "");
        $numpages = sizeof($result);

        foreach($result as $searchId)
        {
            $container->load($searchId);
            $link = DIR_WS_PUBLIC.strtolower(get_class($container)).'.php?id='.$container->getId();
            $thumb = '<a href="'.$link.'">'.$this->buildThumb($container).'</a>';

            // Check if we want a summary with the title
            if(SEARCH_SUMMARY_ON == true)
            {
                $summary = substr( stripslashes( strip_tags( $container->getContent() ) ), 0, (int)SEARCH_SUMMARY_LENGTH );
                $summary .= '... <a href="'.$link.'">read more</a>';
                $title = $container->getTitle();
            }
            else
            {
                $summary = '';
                $title = '<a href="'.$link.'">'.$container->getTitle().'</a>';
            }

                $values = array(
									"thumb"    => $thumb,
									"title"    => $title,
									"summary"  => $summary
								);
                $rows .= $tpl->get_loop("searchloop", $values);
                $searchcount++;
        }

            /********** Next Container Link Generation *************/
            if(($totalnumpages/((int)SEARCH_NUM_ROWS)) > 1)
            {
                $previous=$pagenum-1;
                if($pagenum > 1)
                    $pagelinks .= '<a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$previous.'">&lt;-- Previous</a>';
                $i=1;
                while($i < $pagenum)
                {
                    $pagelinks .= '| <a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$i.'">'.$i.'</a>';
                    $i++;
                }
                $pagelinks .= '| '.$pagenum.' ';
                $i++;
                while(($i-1) < ($totalnumpages/((int)SEARCH_NUM_ROWS)))
                {
                    $pagelinks .= '| <a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$i.'">'.$i.'</a>';
                    $i++;
                }
                if($totalnumpages >= ($searchcount + $offset)){
                    $next=$pagenum+1;
                    $pagelinks .= '| <a href="'.DIR_WS_PUBLIC.'search.php?search='.urlencode($string).'&page='.$next.'">Next--&gt;</a>';
                }
            }
            /********** Next Container Link Generation *************/

        if($numpages == 0)
            $rows = '<br>&nbsp;&nbsp;<b>No Results found for:</b> '.urldecode($string);

        $values = array(
					"rows"        => $rows,
					"pagelinks"   => $pagelinks
				);
        $tpl->parse($values);
        return $tpl->template;
    }
    
    
    function getSearch($string, $pagenum)
    {
        // Load up the site definition
        if(SEARCH_RESULT_TYPE == "thumbs")
            $ret = $this->getSearchThumbs($string, $pagenum);
        elseif(SEARCH_RESULT_TYPE == "list")
            $ret = $this->getSearchList($string, $pagenum);
        else
            die("Error in Search::getSearch - SEARCH_RESULT_TYPE has not been defined");
            
        return $ret;
    }
}
?>
