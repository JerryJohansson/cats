<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

class Ezweb
{
    function Ezweb()
    {
    }
		
		function displayFrameSet($mode='', $id=0)
		{
			// Load up the required includes
      require_once(DIR_WS_CLASSES."template.php");
			//<base href="http://localhost:8080/hocti.com/" >
			/*
				explore_page => "explore.php?id=$id",
				editor_page => "editor.php?action=$action&id=$id",
				header_page => "editor.php?topframe=true",
				maintain_page => "index.php?mode=editor&action=$action&id=$id&pid=$pid",
			*/
			
			$base_ref = '<base href="'.HTTP_SERVER.'" >';
			$header_page = 'editor.php?topframe=true';
			$maintain_page = 'index.php?mode=editor&id='.$id.'&pid='.$_REQUEST['pid'];
			
			$explore_page = 'explore.php?id='.$id;
			$editor_page = 'editor.php?action='.$_REQUEST['action'].'&id='.$id;
			//echo($mode);
			if($mode=="admin")
			{
			//print_r($_SESSION);
				$tpl = new Template("templates/frameset_main.htm");
				$values = array(
					"header_page" => $header_page,
					"maintain_page" => $maintain_page
				);
				//print_r($values);
        $tpl->parse($values);
				$tpl->output();
			}
			elseif($mode=="content")
			{
				
				$tpl = new Template("templates/frameset_main.htm");
				$values = array(
					"header_page" => $header_page,
					"maintain_page" => $maintain_page.'&action='.$_REQUEST['action']
				);
        $tpl->parse($values);
				$tpl->output();
			}
			elseif($mode=="editor")
			{
				$tpl = new Template("templates/frameset_edit.htm");
				$values = array(
					"explore_page" => $explore_page,
					"editor_page" => $editor_page
				);
        $tpl->parse($values);
				$tpl->output();
			}
		}

    function displayEditor($id, $action)
    {
			if($action == "edit")
			{
				header("Location: ".DIR_WS_ADMIN."editor.php?action=edit&id=".$id);
			}
			else
			{
				header("Location: ".DIR_WS_ADMIN."editor.php?action=add&pid=".$id);
			}
    }

    function displayDelete($id)
    {
			require_once(DIR_WS_CLASSES."template.php");
			$main = new Template("templates/main.htm");
			$tpl = new Template("templates/delete.htm");
			$container = new Container;
			$container->load($id);

			// Pull this pages entry from the pages database and load up an associative array to pass to the template
			$values = array(
				"title"       => $container->getTitle(),
				"type"        => get_class($container),
				"id"          => $id
			);
			$tpl->parse($values);
			$values = array("main" => $tpl->template);
			$main->parse($values);
			$main->output();
    }

    function displayEmail($id)
    {
			require_once(DIR_WS_CLASSES."template.php");
			$main = new Template("templates/main.htm");
			$tpl = new Template("templates/email.htm");
			$container = new Container;
			$container->load($id);

			$values = array(
				"content"			=> $container->getContent(),
				"title"       => $container->getTitle(),
				"type"        => get_class($container),
				"id"          => $id
			);
			$tpl->parse($values);
			$values = array("main" => $tpl->template);
			$main->parse($values);
			$main->output();
    }

    function checkImageUpload($POST, $id)
    {
			// If there are any images that need uploading, do it
			if( strstr($POST['content'], 'file://' ) != false )
			{
				$itags = '';
				require_once(DIR_WS_CLASSES."template.php");
				$main = new Template("templates/no_hdr_main.htm");
				$tpl = new Template("templates/upload.htm");
				// Once we have updated the database, take the content and build the list of input tags
				require("upload.php");

				$rows = preg_replace("/<(.*?)>/e", "process_tag(StripSlashes('\\1'))", stripslashes($POST['content']));
				$values = array(
					"id"            => $id,
					"type"          => $POST['type'],
					"uploadtags"    => $itags
				);
				$tpl->parse($values);

				$values = array( "main" => $tpl->template );
				$main->parse($values);
				$main->output();
				return true;
			}
			else
			{
				return false;
			}
    }
    
    function checkPageSecurity2($POST, $id)
    {
			if(!isset($POST['groups']) && is_array($POST['groups']))
				$POST['groups'] = implode(",",$POST['groups']);
			else
				$POST['groups'] = '';
			// Grab the pages database wrapper
			$container = new Container($id);
			$container->setGroupSecurity($POST['groups']);
			$container->update();
    }
		
		function checkFileName($filename, $filepath, $index=0)
		{
			//$filename = preg_replace(array_values(get_html_translation_table(HTML_ENTITIES)), "_", $filename);
			//ezw_dbg($filename."<br>");
			/*
			if(file_exists($filepath.$filename))
			{
				$filename = ''.$index.'_'.$filename;
				$filename = preg_replace("/\s|#|\?|@|\||!|\^|%|\*|\+/", "_", $filename);
				$filename = $this->checkFileName($filename ,$filepath, $index++);
			}else */
			$filename = preg_replace("/\s|#|\?|@|\||!|\^|%|\+|\*/", "_", $filename);
			//ezw_dbg($filename."<br>");
			return $filename;
		}
		
		function getFileUpload($POST)
    {
			//exit("count=".count($_FILES['userfile']['tmp_name']).$_FILES['userfile']['tmp_name'][0]);
			$files = $POST['file'];
			if($_FILES['userfile']['tmp_name'][0])//($userfile && $userfile_name)
			{
				$filenames = array();
				$afilenames = array();
				
				$class_pagetype=(!isset($POST['type']))?'container':ucwords($POST['type']);
				
				$container = new $class_pagetype($POST['id']);
				$filecount = count($_FILES['userfile']['tmp_name']);
				
				//ezw_dbg("checkFileUpload::container->properties=");
				//ezw_dbg($container->properties);
				//ezw_dbg("<br>");
				
				for($i=0; $i<$filecount; $i++)
				{
					// Upload data
					$tempname = $_FILES['userfile']['tmp_name'][$i];
					$filename = $this->checkFileName($_FILES['userfile']['name'][$i], DIR_FS_FILES);
					$filepath = DIR_FS_FILES.$filename;
					
					// Move file
					if($tempname != '')
					{
						move_uploaded_file($tempname, $filepath);
					}
					@chmod($filepath, 0777);
					$filenames[] = $filename;
				}// end::for($i=0; $i<count($_FILES['userfile']['tmp_name']); $i++)
				//ezw_dbg($container->properties);
				//ezw_dbg("<br>container->getFile=".$container->getFile()."<br>");
				if($container->getFile()!=''){
					
					$afiles=explode("|",$container->getFile());
					foreach($afiles as $file){
						foreach($filenames as $filename){
							if($file!=$filename) $afilenames[]=$filename;
						}
					}
					if(!empty($afilenames)) $files = $container->getFile().'|'.implode('|',$afilenames);
					else $files = $container->getFile();
					
				}else
					$files = implode('|',$filenames);
				//$POST['file'] = $files;
				//$_POST['file'] = $files;
				//$container->setProperties($POST);
				//$container->setFile($files);
				//$container->update();
				//ezw_dbg($container->properties);
				//ezw_dbg("<br>POST=<br>");
				//ezw_dbg($POST);
				//ezw_dbg("<br>_POST=<br>");
				//ezw_dbg($_POST);
				//exit;
			}
			return $files;
    }
		
    function getFileDelete($POST)
    {
			$files = explode("|",$POST['file']);
			if(is_array($POST['delfile']))
			{
				// Grab the pages database wrapper
				require_once(DIR_WS_CLASSES."container.php");
				$container = new Container($POST['id']);
				$files = explode("|", $container->getFile());

				foreach($POST['delfile'] as $value)
				{
					if($value!='')
					{
						// If there is MORE than one container linking to this file, then do not delete it
						if( $container->checkFile($value) <= 1 )
						{
							// delete the currently linked file
							@chmod(DIR_FS_FILES.$value, 0777);
							unlink(DIR_FS_FILES.$value);
						}
						// remove from array
						$i = 0;
						foreach($files as $file)
						{
							if($files[$i]==$value) array_splice($files, $i, 1);
							$i++;
						}
					}
				}
				// Reset the file field
				//$container->setFile(implode("|",$files));
				//$container->update();
				//exit;
			}
			return implode("|",$files);
    }

    // We need the parent category to insert this under, the type and the post data
    function postAdd($POST, $userfile, $userfile_name)
    {
			/*$POST = array_merge( $POST, array( "date" => strtotime( $POST['date'] )) );
			$POST = array_merge( $POST, array( "edate" => strtotime( $POST['edate'] )) );
			
			$_POST = array_merge( $_POST, array( "date" => strtotime( $_POST['date'] )) );
			$_POST = array_merge( $_POST, array( "edate" => strtotime( $_POST['edate'] )) );
			*/
			// Check if we are updating the attached file
			$_POST = array_merge( $_POST, array("file" => $this->getFileUpload($_POST)) );
			
			// Add a new node by eval'ing the type and executing it's Add method
			$type = (isset($_POST['type']))?$_POST['type']:'container';
			require_once(DIR_WS_CLASSES.$type.".php");
			$class = ucwords($type);
			$node = new $class;
			$nodeid = $node->postAdd();

			$_POST['id'] = $nodeid;
			if(!isset($_POST['refresh_pid'])) $_POST['refresh_pid'] = $nodeid;
			//exit("<br><b>END::ADD</b><br>");
			$pid = $node->getPid();
			// Check if any images need uploading, if not go back to the index
			//if( $this->checkImageUpload($POST, $nodeid) == false )
			ezw_redirect("admin.php?refresh_tree=$pid");
				//header("Location: editor.php");
    }

    // We want the id of the container, the type and the post data
    function postEdit($POST, $userfile, $userfile_name)
    {
			/*
			$POST = array_merge( $POST, array( "date" => strtotime( $POST['date'] )) );
			$POST = array_merge( $POST, array( "edate" => strtotime( $POST['edate'] )) );
			*/
			// Check if we are updating the file
			$_POST['file'] = $this->getFileUpload($_POST);//, $userfile, $userfile_name);
			// Check if we are deleting the file
			$_POST['file'] = $this->getFileDelete($_POST);
			
			$type = (isset($_POST['type']))?$_POST['type']:'container';
			require_once(DIR_WS_CLASSES.$type.".php");
			$class = ucwords($type);
			$node = new $class;
			$pid = $node->postEdit();
			
			// Check if this container is protected and update the database accordingly
			//$this->checkPageSecurity($POST, $POST['id']);
			//ezw_dbg("<br>ezweb::postEdit::POST=<br>");
			//ezw_dbg($POST);
			exit("<br><b>END::EDIT</b><br>");
			// Check if any images need uploading, if not go back to the admin
			//if( $this->checkImageUpload($POST, $POST['id']) == false )
			ezw_redirect("admin.php?refresh_tree=$pid");
    }

    // This function takes the container id supplied, merges it with the email template and then emails every user who is marked as subscribed
    function postEmail($id)
    {
			require(DIR_WS_CLASSES."users.php");
			require_once(DIR_WS_CLASSES."template.php");
			$container = new Container($id);
			$user = new User;
			$tpl = new Template("../templates/email.htm");

			$emails = $user->getSubscribed();
			$values = array("main" => $container->getContent(), "title" => SITE_NAME . " - Newsletter");
			$tpl->parse($values);

			foreach( $emails as $email )
			{
				mail($email, SITE_NAME . " - Newsletter", $tpl->template,
						 "MIME-Version: 1.0\r\n".
						 "Content-type: text/html; charset=iso-8859-1\r\n".
						 "From: webmaster@{$_SERVER['SERVER_NAME']}\r\n" .
						 "Reply-To: webmaster@{$_SERVER['SERVER_NAME']}\r\n" .
						 "X-Mailer: PHP/" . phpversion());
			}

			header("Location: admin.php");
    }

    function deldir($dir){
			if(file_exists($dir) == true)
			{
				$current_dir = opendir($dir);
				while($entryname = readdir($current_dir))
				{
					if(is_dir("$dir/$entryname") and ($entryname != "." and $entryname!=".."))
					{
						deldir("${dir}/${entryname}");
					}
					elseif($entryname != "." and $entryname!="..")
					{
						unlink("${dir}/${entryname}");
					}
				}
				closedir($current_dir);
				rmdir(${dir});
			}
    }

    function postDelete($id)
    {
			$container = new Container;
			// Delete the container
			$container->load($id);
			$pid = $container->getPid();
			$container->delete($id);
			
			header("Location: admin.php?refresh_tree=$pid");
    }

    // We need to take the post data, move all the files and replace all the image tags
    function postUpload($POST, $userfile, $userfile_name, $width, $height, $attr, $tag)
    {
			require_once (DIR_WS_CLASSES.'image_toolbox.php');
			
			$container = new Container($POST['id']);
			$data = $container->getContent();
			define('DIR_FS_UPLOAD',DIR_FS_GALLERY.'upload/');
			// Setup permission to upload
			@chmod(DIR_FS_UPLOAD, 0777);
			@chmod(DIR_FS_GALLERY, 0777);
			@chmod(DIR_FS_THUMBS, 0777);
			
			$image_types = array('','gif', 'jpg', 'png', 'swf');
			// Loop through the images being uploaded
			foreach($userfile as $key => $value)
			{
				// We need to format the file path
				//$image_upload_orig = $this->checkFileName($userfile_name[$key], DIR_FS_GALLERY.'upload/');
				$image_original_name = $userfile_name[$key];
				$image_upload_name = $this->checkFileName($image_original_name, DIR_FS_UPLOAD);
				$image_upload_path = DIR_FS_UPLOAD.$image_upload_name;
				$image_save_path = DIR_FS_GALLERY.$image_upload_name;
				
				$attr = getimagesize($userfile[$key]);
				$imagetype = $image_types[$attr[2]];
				if($imagetype=='gif' || $imagetype=='swf') // This is a gif file
				{
					$image_upload_name = $this->checkFileName($image_original_name, DIR_FS_GALLERY);
					$image_save_path = DIR_FS_GALLERY.$image_upload_name;
					copy($userfile[$key], $image_save_path);
				}
				else
				{
					$image_upload_path = (($width[$key]!= "" ) && ($height[$key] != "" )) ? $image_upload_path : $image_save_path;
					if(copy($userfile[$key], $image_upload_path))
					{
						$img = new Image_Toolbox($image_upload_path);
						//$attr = getimagesize($image_upload_path);
						if( $imagetype=='jpg' || $imagetype=='png' )
						{
							if ( ($width[$key]!= "" ) && ($height[$key] != "" ))
							{
								$img_width = (int)$width[$key];
								$img->newOutputSize($img_width, 0);
							}
						}
						
						$img->save($image_save_path, $imagetype);
						// Save thumbnail if enabled
						if($this->isThumbnailType($container->getPageType()))
						{
							$thumb_new_size = (defined('THUMB_SIZE') && (int)THUMB_SIZE > 0) ? THUMB_SIZE : 100 ;
							$thumb_save_path = DIR_FS_THUMBS.$image_upload_name;
							$img = new Image_Toolbox($image_upload_path);
							if($attr[0] < $thumb_new_size) // width < new width
							{
								if($attr[1] > $thumb_new_size) // height > new height
								{
									$img->newOutputSize(0, $thumb_new_size);
								}
							}
							else
							{
								$img->newOutputSize($thumb_new_size, 0);
							}
							$img->save($thumb_save_path, $imagetype);
							if(!isset($thumb)) $thumb = $image_upload_name;
						}
					}
					else
					{
						echo("There was an error: ".$php_errormsg." <br>Please contact Buzmedia for assistance<br>");
					}
				}
				$img_tag = preg_replace('/src="file:\/\/(.*?)'.rawurlencode($image_original_name).'"/','src="'.DIR_WS_GALLERY.$image_upload_name.'"', base64_decode($tag[$key]));
				// Search out the old img tags and replace the src
				//echo base64_decode($tag[$key]).'<br>';
				//echo $img_tag.'<br>';
				//echo '/src="file:\/\/(.*?)'.rawurlencode($image_original_name).'"/'.'<br>';
				//$data = ereg_replace( base64_decode($tag[$key]), $img_tag, $data);
				//exit($data);
				
				$data = str_replace( base64_decode($tag[$key]), $img_tag, $data);
			}
			
			// Update the data
			$container->setThumb($thumb);
			$container->setContent($data);
			$container->update();
			
			ezw_redirect("editor.php?refresh_tree=".$POST['pid']);
	}
	function isThumbnailType($type='')
	{
		return true;
		$ret = false;
		switch($type)
		{
			case 'product':
			case 'page':
			case 'course':
				$ret = true;
				break;
			default:
				$ret = false;
		}
		return $ret;
	}
	
}
?>
