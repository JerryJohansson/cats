<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Table class
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
class Table {
	var $table_border = '0';
	var $table_width = '100%';
	var $table_cellspacing = '0';
	var $table_cellpadding = '2';
	var $table_parameters = '';
	var $table_row_parameters = '';
	var $table_data_parameters = '';

	function Table($contents,$caption='') {
		$tableBox_string = '';

		$form_set = false;
		if (isset($contents['form'])) {
			$tableBox_string .= $contents['form'] . "\n";
			$form_set = true;
			array_shift($contents);
		}

		if(is_array($caption)){
			//$tableBox_string .= '<fieldset id="tbar">' . "\n";
		}
		
		$tableBox_string .= '<table border="' . $this->table_border . '" width="' . $this->table_width . '" cellspacing="' . $this->table_cellspacing . '" cellpadding="' . $this->table_cellpadding . '"';
		if (main_not_null($this->table_parameters)) $tableBox_string .= ' ' . $this->table_parameters;
		$tableBox_string .= '>' . "\n";
		
		if (is_array($caption)) {
			$tableBox_string .= '<caption id="tog"><a name="'.$caption['name'].'"';
			if(!empty($caption['link'])) $tableBox_string .= ' href="'.$caption['link'].'"';
			if(!empty($caption['icon'])) $tableBox_string .= ' style="background-image:url('.$caption['icon'].');padding-left:24px;background-position:left;"';
			$tableBox_string .= '>' . $caption['text'] . "</a></caption>\n";
		}

		for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
			$tableBox_string .= '  <tr';
			if (main_not_null($this->table_row_parameters)) $tableBox_string .= ' ' . $this->table_row_parameters;
			if (isset($contents[$i]['params']) && main_not_null($contents[$i]['params'])) $tableBox_string .= ' ' . $contents[$i]['params'];
			$tableBox_string .= '>' . "\n";

			if (isset($contents[$i][0]) && is_array($contents[$i][0])) {
				for ($x=0, $y=sizeof($contents[$i]); $x<$y; $x++) {
					if (isset($contents[$i][$x]['text']) && main_not_null(isset($contents[$i][$x]['text']))) {
						$tableBox_string .= '    <td';
						if (isset($contents[$i][$x]['align']) && main_not_null($contents[$i][$x]['align'])) $tableBox_string .= ' align="' . $contents[$i][$x]['align'] . '"';
						if (isset($contents[$i][$x]['params']) && main_not_null(isset($contents[$i][$x]['params']))) {
							$tableBox_string .= ' ' . $contents[$i][$x]['params'];
						} elseif (main_not_null($this->table_data_parameters)) {
							$tableBox_string .= ' ' . $this->table_data_parameters;
						}
						$tableBox_string .= '>';
						if (isset($contents[$i][$x]['form']) && main_not_null($contents[$i][$x]['form'])) $tableBox_string .= $contents[$i][$x]['form'];
						$tableBox_string .= $contents[$i][$x]['text'];
						if (isset($contents[$i][$x]['form']) && main_not_null($contents[$i][$x]['form'])) $tableBox_string .= '</form>';
						$tableBox_string .= '</td>' . "\n";
					}
				}
			} else {
				$tableBox_string .= '    <td';
				if (isset($contents[$i]['align']) && main_not_null($contents[$i]['align'])) $tableBox_string .= ' align="' . $contents[$i]['align'] . '"';
				if (isset($contents[$i]['params']) && main_not_null($contents[$i]['params'])) {
					$tableBox_string .= ' ' . $contents[$i]['params'];
				} elseif (main_not_null($this->table_data_parameters)) {
					$tableBox_string .= ' ' . $this->table_data_parameters;
				}
				$tableBox_string .= '>' . $contents[$i]['text'] . '</td>' . "\n";
			}

			$tableBox_string .= '  </tr>' . "\n";
		}

		$tableBox_string .= '</table>' . "\n";
		
		if(is_array($caption)){
			//$tableBox_string .= '</fieldset>' . "\n";
		}

		if ($form_set == true) $tableBox_string .= '</form>' . "\n";

		return $tableBox_string;
	}
}
?>
