<?PHP
class Form
{
	var $return_url;
	var $formset = false; //Flag to tell if the formtags have been set or not
	var $name; // Name of form (name)
	var $realname; // Just the name part of $name
	var $action; // Action of form (action)
	var $encode; // Encode type of form (encType)
	var $method; // Forms method GET/POST
	var $form=array(); // Array of stored rows of label field values
	var $formtag=array(); // Array of form tag and extra stuff like validation script etc
	var $hidden=array(); // Array of stored hidden fields
	var $formhtml; // Store the finished form html
	var $formrows = ''; // Store the html rows from form array
	var $validate_var;
	var $validate_fields;
	var $validate;
	var $script = '
<script src="js/usableforms.js" language="javascript1.2" type="text/javascript"></script>
<script src="js/validation.js" language="javascript1.2" type="text/javascript"></script>'; // store the embeded scripts and any other addhoc scripts
	var $validation = ''; // store the script string for validation
	var $flagtag;
	var $onload_script = '
<table><tbody id="waitingRoom"></tbody></table>
<script language="javascript1.2" type="text/javascript">
prepareForm();
</script>';
	
	var $posthtml = '';
	var $posttxt = '';
	var $posthtml_msg = '';
	var $posttxt_msg = '';
	var $title;
	var $help;
	
	var $reinsert_field = true;
	var $field_size = 50;
	var $field_maxlength = 128;
	
	function Form($name='main',$id=0,$type='')
	{
		$this->setName($name);
		$this->setMethod('POST');
		switch($type)
		{
			case EZW_FORM_CAMPAIGN_TYPE:
				$this->addHidden($this->getHidden(EZW_FORM_CAMPAIGN_TYPE,$id));
				break;
			case EZW_FORM_MAIL_TYPE:
				$this->addHidden($this->getHidden(EZW_FORM_MAIL_TYPE,$id));
				break;
			default:
				$this->form = $this->getHidden($type,$id);
				break;
		}
	}
/*
+---------------------------------------------------------------------------
|	Add an element or html to the form array
+---------------------------------------------------------------------------
*/
	function add($html)
	{
		$this->form[] = $html;
	}
	function addHidden($html)
	{
		$this->hidden[] = $html;
	}
	
	function setTitle($val)
	{
		$this->title = $val;
	}
	function setHelp()
	{
		$this->help = $val;
	}
	function setName($val)
	{
		$this->realname = $val;
		$this->name = ' name="'.$val.'" ';
	}
	function setAction($val)
	{
		$this->action = ' action="'.$val.'" ';
	}
	function setEncode($val)
	{
		$this->encode = ' enctype="'.$val.'" ';
	}
	function setMethod($val)
	{
		$this->method = ' method="'.$val.'" ';
	}
	function setValidateVariable()
	{
		$this->validate_var = 'FORM_'.str_replace(' ','_',strtoupper($this->realname)).'_VALIDATE';
	}
	function addValidateField($name,$label,$validate=array('type'=>'empty'))
	{
	/*****************************************
	*		Validate Fields from list
	**	List = "sFieldName[,sFieldName2[,n..]];validateType[,param1[,param2[,n..]]];sFriendlyName";
	***	validateTypes: 
				empty = empty field ( name;empty;label name )
				alertonly = no focus ( name;alertonly;label name )
				number = check number ( name;number;label name )
				email = email address ( name;email;label name )
				length = check length of a number ( name;length,80;label name )
				date = check date ( name;date;label name )
				daterange = check to and from dates ( name1,name2;daterange;label name )
				same = check the values are the same ( name1,name2;same;label name )
	******************************************/
		$label = str_replace("_"," ",$label);
		$ret = '';
		$validate_types = explode("|",$validate['type']);
		$len = count($validate_types);
		$events=array(
			"onchange" => '',
			"onclick" => '', 
			"onkeypress" => '',
			"onblur" => ''
		);
		for($i=0;$i<$len;$i++){
			switch($validate_types[$i])
			{
				case 'empty':
					$validate_string .= $name . ';empty;' . $label;
					$events['onchange'].="checkempty(this);";
					//$ret = ' onchange="checkempty(this);"';
					break;
				case 'alertonly':
					$validate_string .= $name . ';alertonly;' . $label;
					$ret = '';
					break;
				case 'number':
					$validate_string .= $name . ';number;' . $label;
					$events['onchange'].="checknumber(this);";
					//$ret = ' onchange="checknumber(this);"';
					break;
				case 'email':
					$validate_string .= $name . ';email;' . $label;
					$events['onchange'].="checkemail(this);";
					//$ret = ' onchange="checkemail(this);"';
					break;
				case 'samevalue':
					$validate_string .= $name . '|' . $validate['name2'] . ';same;' . $label . '|' . $validate['label2'];
					$events['onchange'].="checksamevalue(this,this.form.elements['".$validate['name2']."'],'$label','".$validate['label2']."');";
					//$ret = ' onchange="checksame(this);"';
					break;
				case 'length':
					$validate_string .= $name . ';length,'.$validate['length'].';' . $label;
					$events['onchange'].="checklength(this".((isset($validate['length']))?",".$validate['length']:"").");";
					//$ret = ' onchange="checklength(this,'.$validate['length'].');"';
					break;
				case 'date':
					$validate_string .= $name . ';date;' . $label;
					$events['onchange'].="checkdate(this);";
					//$ret = ' onchange="checkdate(this);"';
					break;
				case 'daterange':
					$validate_string .= $name . '|' . $validate['name2'] . ';daterange;' . $label . '|' . $validate['label2'];
					break;
				default:
					$validate_string .= $name . ';empty;' . $label;
					break;
			}
		}
		if($this->validate_fields != "") $validate_string = ';' . $validate_string;
		$this->validate_fields .= $validate_string;
		foreach($events as $name => $value){
			if(!empty($value)) $ret.= ' '. $name .'="'. $value . '" ';
		}
		return $ret;
	}
	function setValidate()
	{
		if($this->validate_fields != "")
		{
			$this->setValidateVariable();
			$this->validate = ' onsubmit="return validateForm(this,'.$this->validate_var.')" ';
		}
	}
	function setInstructions($type='info',$msg='Message not provided')
	{
		$this->help = '<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="images/forms/'. $type .'.gif" /></td>
			<td>
				<p>'. $msg .'</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
';
	}
	
	function setForm($name='main', $action=NULL, $method=NULL, $multpart=false)
	{
		$this->setValidate();
		if(is_null($action) && empty($this->action)) $this->setAction($_SERVER['PHP_SELF']);
		if($multipart) $this->setEncode("multipart/form-data");
		if(empty($this->$name)) $this->setName($name);
		if(is_null($method) && empty($this->method)) $this->setMethod('POST');
		$this->formtag['opentable'] = '
<fieldset class="tbar">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
';
		if(!empty($this->title)) $this->formtag['title'] = '<caption id="tog"><a name="todo" onclick="this.blur();">'.$this->title.'</a></caption>
';
		if(!empty($this->help)) $this->formtag['title'] .= $this->getInstructions();
		$this->formtag['closetable'] = '
</table>
</fieldset>
';
		$this->formtag['open'] = '
<link href="templates/'.TEMPLATE_DEFAULT_GROUP.'/forms.css" type="text/css" rel="stylesheet" />
<form '.$this->name.$this->action.$this->encode.$this->validate.$this->method.' >
';
		$this->formtag['close'] = '
</form>
';
		$this->formset = true;
	}
	
	function getInstructions()
	{
		return $this->help;
	}
	
	function getForm()
	{
		if(!$this->formset) $this->setForm();
		$this->formhtml = $this->formtag['open'];
		$this->formhtml .= $this->getHiddenElements();
		$this->formhtml .= $this->getValidationScript();
		$this->formhtml .= $this->formtag['opentable'];
		$this->formhtml .= $this->formtag['title'];
		foreach($this->form as $row)
		{
			$this->formrows .= $row;
		}
		$this->formhtml .= $this->formrows;
		$this->formhtml .= $this->formtag['closetable'];
		$this->formhtml .= $this->formtag['close'];
		return $this->formhtml;
	}
	
	function getFormResult($mode='html')
	{
		if($mode=='html')
		{
			return $this->posthtml;
		}
		else
		{
			return $this->posttxt;
		}
	}
	
	function getFormResultMsg($mode='html')
	{
		if($mode=='html')
		{
			return $this->posthtml_msg;
		}
		else
		{
			return $this->posttxt_msg;
		}
	}
	
	function getHiddenElements()
	{
		foreach($this->hidden as $field)
		{
			$hidden .= $field;
		}
		return $hidden;
	}
	function getValidationScript()
	{
		$ret = '';
		if($this->validate_fields != "")
		{
			$ret = $this->script.'
<script>
<!--
var '.$this->validate_var.' = "'.$this->validate_fields.'";
//-->
</script>';
		}
		return $ret;
	}

/*
+---------------------------------------------------------------------------
|	Sets the private var with rows of label field cells
+---------------------------------------------------------------------------
*/
	function setFormRows($fields)
	{
		foreach($fields as $attr)
		{
			$this->add($this->getFormRow($attr[0], $attr[1], $attr[2], $attr[3], $attr[4]));
		}
	}
	
	function getHidden($name,$value)
	{
		return $this->getFormElement($name,$value,array('type'=>'hidden'));
	}
/*
+---------------------------------------------------------------------------
|	This function returns a form element
+---------------------------------------------------------------------------
*/
	function getFormElement($name,$value,$attr='',$selected='',$extra='', $alt_label='')
	{
		$sz = 50;
		$rows = 5;
		$cols = $sz;
		$label = $name;
		$class_name = '';
		$attributes = '';
		if(is_array($attr))
		{
			foreach($attr as $key => $val)
			{
				switch($key)
				{
					case 'size':
						$sz = $val;
						break;
					case 'rows':
						$rows = $val;
						break;
					case 'cols':
						$cols = $val;
						break;
					case 'options':
						$options = $val;
						break;
					case 'select': // add more to ignore ;)
						break;
					case 'name': // Override $name with this value
						$name = $val;
						$alt_label = $name;
						break;
					case 'type':
						switch($val)
						{
							case 'group':
							case 'info':
							case 'fieldset':
							case 'submit_group':
								break;
							default:
								$attributes .= ' '.$key.'="'.$val.'"';
								break;
						}
						break;
					case 'class':
						$class_name = $val;
						break;
					case 'value':
						$value = $val;
						break;
					case 'validate':
						$validate = $val;
						break;
					default:
						$attributes .= ' '.$key.'="'.$val.'"';
						break;
				}
			}
		}
		
		if($this->reinsert_field && $value=='')
			$value = $_REQUEST[$name];
		
		// Add validation stuff
		if(is_array($validate) && !empty($validate))
		{
			$attributes .= $this->addValidateField($name,$label,$validate);
		}
		
		switch($attr['type'])
		{
			case 'text':
				if($class_name=='') $class_name='sfield';
				$field = '<input class="'.$class_name.'" name="'.$name.'" id="id_'.$name.'" size="'.$sz.'" value="'.$value.'" '.$attributes.' />';
				break;
			case 'hidden':
				$field = '<input name="'.$name.'" value="'.$value.'" '.$attributes.' />';
				break;
			case 'radio':case 'checkbox':
				if($class_name=='') $class_name='bfield';
				$field = '<input class="'.$class_name.'" name="'.$name.'" id="id_'.$name.'" value="'.$value.'" '.$selected.' '.$attributes.' />';
				break;
			case 'textarea':
				if($class_name=='') $class_name='sfield';
				$field = '<textarea class="'.$class_name.'" name="'.$name.'" id="id_'.$name.'" rows="'.$rows.'" cols="'.$cols.'" '.$attributes.'>'.$value.'</textarea>';
				break;
			case 'select':
				if($class_name=='') $class_name='sfield';
				$field = '<select class="'.$class_name.'" name="'.$name.'" id="id_'.$name.'" '.$attributes.'>';
				if(is_array($options))
				{
					while(list($key, $val) = each($options))
					{
						$options_attributes = '';
						if(is_array($val))
						{
							while(list($okey, $oval) = each($val))
							{
								if(is_array($oval))
								{
									while(list($akey, $aval) = each($oval))
									{
										if($akey=='type')
										{
											if($aval=='span')
											{
												$flagtag[] = array('Other','',array('type'=>$aval,'input'=>array($key=>'',array('type'=>'text'))));
											}
											else
											{
												$flagtag[] = $aval;
											}
										}
										else
										{
											$options_attributes .= ' '.$akey.'="'.$aval.'" ';
										}
									}
								}
								else
								{
									$options_attributes .= ' '.$okey.'="'.$oval.'" ';
								}
							}
							$val = EZW_FORM_PREFIX.$key;
						}
						$field .= '<option value="'.$key.'" '.$options_attributes.'>'.$val.'</option>';
					}
				}
				$field .= '</select>';
				break;
			case 'row':
				$field = '';
				break;
			case 'info':
				$field = '';
				break;
			case 'fieldset':
			case 'group':
				if(is_array($extra) && !empty($extra))
				{
					$col_count = ($extra['columns'])?(int)$extra['columns']:1;
					$col_cntr = 0;
					if(is_array($extra['items']))
					{
						$field = '<table><tr>' . "\n";
						for($i=0;$i<count($extra['items']);$i++)
						{
							$col_cntr++;
							$itm_attrs = $extra['items'][$i][2];
							$name = str_replace(' ','_',$extra['items'][$i][0]);
							$itm = $this->getFormElement($name, $extra['items'][$i][1], $itm_attrs, $extra['items'][$i][3], $extra['items'][$i][4]);
							$lbl = '<label for="id_'.$name.'" class="normal">' . $extra['items'][$i][0] . '</label>';
							$col_1 = '<td>' . $itm . '</td>';
							$col_2 = '<td>' . $lbl . '</td>' . "\n";
							if($col_count == $col_cntr)
							{
								$col_cntr = 0;
								//if((''.$itm_attrs['type'])=="text")
								//echo($itm_attrs['type']);
								if($itm_attrs['type']=="text"){
									$field .= $col_2.$col_1;
								}else{
									$field .= $col_1.$col_2;
								}
								$field .= '</tr>' . "\n" . '<tr>' . "\n";
							}
						}
						$field .= '</tr></table>' . "\n";
					}
				}
				else
				{
					$field = $extra;
				}
				break;
			case 'submit':
				$field = '<input class="button" name="'.$name.'" id="id_'.$name.'" value="'.$value.'" '.$attributes.'>';
				break;
			case 'submit_group':
				$field = '<div style="padding:10px;"><input class="button" name="'.$name.'" id="id_'.$name.'" value="'.$value.'" type="submit" '.$attributes.'>';
				$field .= '&nbsp;<input class="button" name="reset" value="Reset" type="reset"></div>';
				break;
			default:
				$field = $value;
				break;
		}
		
		
		
		return $field;
	}
	
	function getFormRow($label, $value='', $attributes=array('type'=>'row'), $selected='', $extra='')
	{
		$name = str_replace(' ','_',$label);
		if($attributes['type']=='hidden')
		{
			$this->addHidden($this->getHidden($label,$value));
		}
		else
		{
			$field = $this->getFormElement($name, $value, $attributes, $selected, $extra, &$name);
			$tag_attributes = '';
			
			switch($attributes['type'])
			{
				case 'row':
					$ret = '<tr valign="top" '.$tag_attributes.'><td colspan="2" class="header">'.$label.'</td></tr>';
					break;
				case 'info':
					$ret = '<tr valign="top" '.$tag_attributes.'><td colspan="2" class="header">'.$label.'</td></tr>';
					$ret .= '<tr valign="top" '.$tag_attributes.'><td colspan="2">'.$value.'</td></tr>';
					break;
				case 'fieldset':
					$ret = '<tr valign="top" '.$tag_attributes.'><td colspan="2" class="header">'.$label.'</td></tr>';
					if($value!='') $ret .= '<tr valign="top" '.$tag_attributes.'><td colspan="2">'.$value.'</td></tr>';
					$ret .= '<tr valign="top" '.$tag_attributes.'><td colspan="2">'.$field.'</td></tr>';
					break;
				default:
					$label=((strpos($label,EZW_FORM_PREFIX)===0)||($label==''))?'':$label.':';
					$ret = '<tr valign="top" '.$tag_attributes.'><td class="label"><label for="id_'.$name.'">'.$label.'</label></td><td>'.$field.'</td></tr>';
					break;
			}
		}
		
		return $ret;
	}
/*
+---------------------------------------------------------------------------
|	Return row of posted elements
+---------------------------------------------------------------------------
*/
	function parseFormRow($label='label', $value='value', $format='html')
	{
		if($format=='html')
		{
			return '<tr valign="top"><td align="right"><b>'.str_replace('_',' ',$label).':</b></td><td>'.preg_replace('/\r\n/','<br>',$value).'</td></tr>';
		}
		else
		{
			return "$label:  $value".$br;
		}
	}
/*
+---------------------------------------------------------------------------
|	Sets the private var with posted field value pairs
+---------------------------------------------------------------------------
*/
	function parseFormRows($POST)
	{
		foreach($POST as $key => $value)
		{
			$bypass = strstr($key,EZW_FORM_PREFIX);
			if($bypass && $bypass==0)
			{
				$this->posthtml_msg .= $this->parseFormRow($key, $value);
				$this->posttxt_msg .= $this->parseFormRow($key, $value, 'txt');
				if($key==EZW_FORM_RETURN_URL) $this->return_url = $value;
			}
			else
			{
				$this->posthtml .= $this->parseFormRow($key, $value);
				$this->posttxt .= $this->parseFormRow($key, $value, 'txt');
			}
		}
		$this->posthtml = '<table>'.$this->posthtml.'</table><p>&nbsp;</p>';
		$this->posthtml_msg = '<table border=1>'.$this->posthtml_msg.'</table>';
	}
}
// class Form end //
?>