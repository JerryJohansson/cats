<?php
/*
+--------------------------------------------------------------------------
|   Users Database Wrapper
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to access user information. Part of the ezwebmaker CMS.
+---------------------------------------------------------------------------
*/

class User
{
	var $id = 0;			// Current Users ID
	
	function User($id=0)
	{
		if($id==0)
		{
			if(isset($_SESSION['user_details']) && is_array($_SESSION['user_details']))
			{
				$this->id = $_SESSION['user_details']['user_id'];
			}
		}
		else
		{
			$this->id = $id;
		}
	}

	function forgotPass($email)
	{
		require_once(DIR_WS_CLASSES."password.php");
		require_once(DIR_WS_CLASSES."mail.php");
		
		ezw_db_connect();
		
		$data = $this->getFieldsByEmail($email);
		if(is_array($data))
		{
			$password = new Password;
			$pass = $password->getPassword();
			$sql_data = array("password"	=> md5($pass));
			ezw_db_perform(TABLE_USER, $sql_data, "update", "email='".$email."'");
			
			$msg .= "Your new login details for the " . SITE_NAME . " website are as follows.\n";
			$msg .= "Email Address: ".$email."\n";
			$msg .= "Password: ".$pass."\n\n";
			
			if($data['firstname']!="")
			{
				$name = $data['firstname'];
				if($data['lastname']!="")
				{
					$name .= ' '.$data['lastname'];
				}
			}
			elseif($data['user_nick']!="")
			{
				$name = $data['user_nick'];
			}
			else
			{
				$name = '';
			}
			
			$mail = new Mail;
			$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
			$mail->addRecipient($name, $email);
			$mail->setSubject(SITE_NAME . " - Reset Password");
			$mail->setTextMessage($msg);
			if(SITE_IS_LIVE == 'true')
				$ret = $mail->send();
			else
				$mail->showEmail();

			$ret = true;
		}
		else
		{
			$ret = false;
		}
		return $ret;
	}

	// Returns a mysql result resource with every row
	function getAll()
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT * FROM " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d where u.user_id = d.user_id ") or die(ezw_db_error());
		return $result;
	}
	
	// Returns a mysql result resource with every row
	function getLimited($where)
	{
		ezw_db_connect();
		$result = ezw_db_query("select * from " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d where u.user_id = d.user_id and $where") or die(ezw_db_error());
		return $result;
	}
	
	// Returns all active users
	function getCurrent()
	{
		ezw_db_connect();
		$result = ezw_db_query($this->getCurrentSQL()) or die(ezw_db_error());
		return $result;
	}
	function getCurrentSQL()
	{
		return "select * from " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE u.user_id=d.user_id and (u.status & ".(MASK_HIDDEN+MASK_DELETED+MASK_ARCHIVED).") = 0";
	}
	
	// Returns all non active users
	function getPending()
	{
		ezw_db_connect();
		$result = ezw_db_query($this->getPendingSQL()) or die(ezw_db_error());
		return $result;
	}
	function getPendingSQL()
	{
		return "select * from " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE u.user_id=d.user_id and (u.status & ".(MASK_HIDDEN).") > 0 and (u.status & ".(MASK_DELETED+MASK_ARCHIVED).") = 0";
	}
	
	// Returns all archived users
	function getArchived()
	{
		ezw_db_connect();
		$result = ezw_db_query($this->getPendingSQL()) or die(ezw_db_error());
		return $result;
	}
	function getArchivedSQL()
	{
		return "select * from " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE u.user_id=d.user_id and (u.status & ".(MASK_DELETED+MASK_ARCHIVED).") > 0";
	}
	
	// Returns users most recent visited pages
	function getUserHistoryLog($user_id=0)
	{
		ezw_db_connect();
		$result = ezw_db_query($this->getUserHistoryLogSQL($user_id)) or die(ezw_db_error());
		return $result;
	}
	function getUserHistoryLogSQL($id=0,$limit=10)
	{//select n.id, p.title, l.date_time from page p, node n, node_ref r, user_history_log l where n.id=r.node_id and p.id=r.page_id and l.node_id=n.id and l.user_id=1 order by l.date_time desc limit 10
		return "select distinct n.id, p.title, p.type from " . TABLE_PAGE . " p, " . TABLE_NODE . " n, " . TABLE_NODE_REF . " r, " . TABLE_USER_HISTORY . " l where n.id=r.node_id and p.id=r.page_id and l.node_id=n.id and l.user_id=".$id." limit ".$limit." ";
	}
	
	// Returns users most recent visited pages
	function getPendingApprovalPages($user_id=0,$limit=10)
	{
		if($user_id==0) $user_id=$this->getId();
		if($user_id==0) return false;
		ezw_db_connect();
		$result = ezw_db_query($this->getPendingApprovalPagesSQL($user_id,$limit)) or die(ezw_db_error());
		return $result;
	}
	function getPendingApprovalPagesSQL($user_id=0,$limit=10)
	{//select n.id, p.title, l.date_time from page p, node n, node_ref r, user_history_log l where n.id=r.node_id and p.id=r.page_id and l.node_id=n.id and l.user_id=1 order by l.date_time desc limit 10
		
		return "select distinct n.id, p.title, p.type, concat(u.lastname,', ',u.firstname) as editor, n.editor_id, p.date_modified  from " . TABLE_PAGE . " p, " . TABLE_NODE . " n, " . TABLE_NODE_REF . " r, " . TABLE_USER_DETAILS . " u where n.id=r.node_id and p.id=r.page_id and u.user_id=n.publisher_id and ((p.status & ".(MASK_HIDDEN+MASK_DELETED+MASK_ARCHIVE+MASK_PUBLISHED).") = 0) and u.user_id=$user_id limit $limit ";
	}
	
	// Returns an associative array containing all values of user, user_details by $email
	function getFieldsByEmail($email)
	{
		ezw_db_connect();
		//$result = ezw_db_query("SELECT * FROM `user` u, `user_details` d WHERE `u`.`user_id`=`d`.`user_id` and `u`.`user_id` = '$id'") or die(ezw_db_error());
		$result = ezw_db_query("SELECT * FROM " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE u.email='$email' AND u.user_id = d.user_id ") or die(ezw_db_error());
		if( !($value = ezw_db_fetch_array($result)) )
		{
			$value = "Database Error in Users::getFields";
		}
		else
		{
			foreach($value as $akey => $avalue)
			{
				$value[$akey] = stripslashes($value[$akey]);
			}
		}
		return $value;
	}
	// Returns an associative array containing all values of user, user_details $id
	function getFields($id)
	{
		if(empty($id)) $id = $this->id;
		ezw_db_connect();
		$result = ezw_db_query("SELECT * FROM " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE `u`.`user_id`=`d`.`user_id` and `u`.`user_id` = '$id'") or die(ezw_db_error());
		if( !($value = ezw_db_fetch_array($result)) )
		{
			$value = "Database Error in Users::getFields";
		}
		else
		{
			foreach($value as $akey => $avalue)
			{
				$value[$akey] = stripslashes($value[$akey]);
			}
		}
		return $value;
	}
	
	// Returns an associative array containing all values of page $id
	function getSubscribed()
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT u.user_id FROM " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE `u`.`user_id`=`d`.`user_id` and d.newsletter='1'") or die(ezw_db_error());
		$value = array();
		while( $data = ezw_db_fetch_array($result) )
		{
			array_push($value, $data['user_id']);
		}
		return $value;
	}
	
	function getSubscribedCount()
	{
		//print_r(ezw_db_num_rows(ezw_db_query("SELECT u.user_id FROM " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE `u`.`user_id`=`d`.`user_id` and d.newsletter='1'")));
		return count($this->getSubscribed());
		//return ezw_db_getfieldvalue("SELECT count(u.user_id) as user_count FROM " . TABLE_USER . " u, " . TABLE_USER_DETAILS . " d WHERE `u`.`user_id`=`d`.`user_id` and d.newsletter='1'","user_count") or die(ezw_db_error());
	}
	
	function getUserAddress($address_id=0){
		return ezw_db_fetch_array(ezw_db_query("select * from " . TABLE_USER_ADDRESS . " where address_id = '$address_id'"));
	}
	function getAddress($address_id){
		ezw_db_connect();
		$result = ezw_db_query($this->getAddressSQL()) or die(ezw_db_error());
		return $result;
	}
	function getAddressSQL($user_id=0)
	{
		if($user_id==0) $user_id = $this->getId();
		return "select * from " . TABLE_USER_ADDRESS . " where user_id = '$user_id'";
	}
	function addAddress($POST){
		$this->setAddress($POST);
	}
	function setAddress($POST){
/*
`address_id` INTEGER (11) NOT NULL  AUTO_INCREMENT , 
`user_id` INTEGER (11) NOT NULL  DEFAULT 0, 
`name` varchar (50) DEFAULT 'Default', 
`company` varchar (32), 
`firstname` varchar (32) NOT NULL , 
`lastname` varchar (32) NOT NULL , 
`street_address` varchar (64) NOT NULL , 
`suburb` varchar (32), 
`postcode` varchar (10) NOT NULL , 
`city` varchar (32) NOT NULL , 
`state` varchar (32), 
`country_id` INTEGER (11) NOT NULL  DEFAULT 0, 
`zone_id` INTEGER (11) NOT NULL  DEFAULT 0,
*/
		ezw_db_connect();
		$dbfields=explode("|","user_id||name||firstname||lastname||street_address||suburb||postcode||city||state||country_id|1|zone_id|1");
		$pgfields=explode("|","user_id|value|name|value|firstname|value|lastname|value|address|value|suburb|value|postcode|value|city|value|state|value|country_id|value|zone_id|value");
		$sql_data = ezw_db_prepare_array($dbfields,$pgfields,$POST);
		
		if(!isset($POST['address_id']) && ezw_db_num_rows(ezw_db_query("select address_id from " . TABLE_USER_ADDRESS . " where user_id='".$POST['user_id']."' and address_id='".$POST['address_id']."'")) == 0)
		{
			ezw_db_perform(TABLE_USER_ADDRESS, $sql_data);
		}
		else
		{
			ezw_db_perform(TABLE_USER_ADDRESS, $sql_data, "update", "address_id='".$POST['address_id']."'");
		}
	}
	
	function setFields($POST)
	{
		require_once(DIR_WS_CLASSES."password.php");
		$refresh_user_details=false;
		ezw_db_connect();
		if(empty($POST['id'])){
			$POST['id'] = 0;
			// Check if user exists with the same email address
			if(ezw_db_num_rows(ezw_db_query("SELECT user_id FROM " . TABLE_USER . " WHERE email='".$POST['email']."'"))!=0)
			{
				ezw_redirect("post.php?action=email_exists&email=".$POST['email']);
			}
		}
		
		foreach($POST as $key => $val)
		{
			if(!is_array($val)) $POST[$key] = addslashes($val);
		}
		
		if( $POST['newsletter'] != 1 && $POST['newsletter'] != "1" )
			$POST['newsletter'] = 0;
		if( $POST['notify'] != 1 && $POST['notify'] != "1" )
			$POST['notify'] = 0;
		
		$dbfields=explode("|","email||groups|0|roles|0|user_account_type|0|status|0");
		$pgfields=explode("|","email|value|groups|mask|roles|mask|user_account_type|value|status|value");
		$sql_data = ezw_db_prepare_array($dbfields,$pgfields,$POST);
		
		if($POST['password'] != "")
		{
			$sql_data = array_merge($sql_data, array("password"	=> md5($POST['password'])));
		}
		else//if($POST['id']==0)
		{
			$password = new Password;
			$POST['password'] = $password->getPassword();
			$sql_data = array_merge($sql_data, array("password"	=> md5($POST['password'])));
		}
		
		if(ezw_db_num_rows(ezw_db_query("select user_id from " . TABLE_USER . " where user_id='".$POST['id']."'")) == 0)
		{
			ezw_db_perform("user", $sql_data);
			$POST['id'] = ezw_db_insert_id();
		}
		else
		{
			ezw_db_perform(TABLE_USER, $sql_data, "update", "user_id='".$POST['id']."'");
			if(is_array($_SESSION['user_details']) && $POST['id']==$_SESSION['user_details']['user_id']) $refresh_user_details=true;
		}
		
		$dbfields=explode("|","user_nick||firstname||lastname||gender||dob||mobile||telephone||fax||newsletter|1|notification|0");
		$pgfields=explode("|","name|value|firstname|value|lastname|value|gender|value|dob|date|mobile|value|telephone|value|fax|value|newsletter|value|notify|value");
		$sql_data = ezw_db_prepare_array($dbfields,$pgfields,$POST);
		
		if(ezw_db_num_rows(ezw_db_query("select user_id from " . TABLE_USER_DETAILS . " where user_id='".$POST['id']."'")) == 0)
		{
			$sql_data = array_merge($sql_data, array("user_id"	=> $POST['id']));
			ezw_db_perform("user_details", $sql_data);
		}
		else
		{
			ezw_db_perform(TABLE_USER_DETAILS, $sql_data, "update", "user_id='".$POST['id']."'");
			if(is_array($_SESSION['user_details']) && $POST['id']==$_SESSION['user_details']['user_id']) $refresh_user_details=true;
		}
		
		$this->setAddress($POST);
		if($refresh_user_details) ezw_get_user_details($POST['id']);
		return $POST['id'];
	}
	
	function setField($id, $field, $value)
	{
		ezw_db_connect();
		$value = addslashes($value);
		$sql_data = array(
			$field	=> $value
		);
		ezw_db_perform(TABLE_USER, $sql_data, "update", "user_id='".$id."'");
	}
	
	function add($POST)
	{
		require_once(DIR_WS_CLASSES."template.php");
		require_once(DIR_WS_CLASSES."mail.php");
		
		$POST['status'] = 2;
		
		$id = $this->setFields($POST);
		$data = $this->getFields($id);
		$email = $data['email'];
		$name = $data['firstname'].' '.$data['lastname'];
		if($name==' ')
			$name = '';
		else
			$name = ' '.$name;
		// Send Email with profile link and finalisation details ;)
		$usertpl = new Template("templates/user_request_membership.htm");
		$values = array(
			"name"					=> $name,
			"website_name"	=> SITE_NAME,
			"activate_link"	=> DIR_WS_PUBLIC."post.php?decode=".base64_encode("activate_user|".$id),
			"website_owner"	=> SITE_OWNER,
			"remove_link"		=> DIR_WS_PUBLIC."post.php?decode=".base64_encode("remove_user|".$id)
		);
		$usertpl->parse($values);
		
		$mail = new Mail;
		$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
		$mail->addRecipient($name, $email);
		$mail->setSubject(SITE_NAME . " - Request Member Registration");
		$mail->setTextMessage($usertpl->template);
		if(SITE_IS_LIVE == 'true')
			return $mail->send();
		else
			echo($mail->showEmail());
	
	}
	
	function activate($id)
	{
		require_once(DIR_WS_CLASSES."template.php");
		require_once(DIR_WS_CLASSES."mail.php");
		require_once(DIR_WS_CLASSES."password.php");
		
		$data = $this->getFields($id);
		$email = $data['email'];
		$name = $data['firstname'].' '.$data['lastname'];
		if($name==' ')
			$name = '';
		else
			$name = ' '.$name;
		
		$passgen = new Password;
		$pass = $passgen->getPassword();
		$this->setField($id, "status", "0");
		$this->setField($id, "password", md5($pass));
		
		// Send Email confirming membership
		$usertpl = new Template("templates/user_confirm_membership.htm");
		$values = array(
			"name"					=> $name,
			"website_name"	=> SITE_NAME,
			"profile_link"	=> DIR_WS_PUBLIC."profile.php",
			"login"					=> $email,
			"password"			=> $pass,
			"website_owner"	=> SITE_OWNER,
			"remove_link"		=> DIR_WS_PUBLIC."post.php?decode=".base64_encode("remove_user|".$id)
		);
		$usertpl->parse($values);
		
		$mail = new Mail;
		$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
		$mail->addRecipient($name, $email);
		$mail->setSubject(SITE_NAME . " - Membership Registration Confirmation");
		$mail->setTextMessage($usertpl->template);
		if(SITE_IS_LIVE == 'true')
			return $mail->send();
		else
			echo($mail->showEmail());
	
	}
	
	function exists($field='id',$value=0)
	{
		return (ezw_db_getfieldvalue("select ".$field." from ".TABLE_USER." where ".$field."='".$value."'",$field)==$value);
	}
	// Delete page $id
	function delete($id)
	{
		ezw_db_connect();
		ezw_db_query("DELETE FROM " . TABLE_USER . " WHERE `user_id`='$id'") or die(ezw_db_error());
		ezw_db_query("DELETE FROM " . TABLE_USER_DETAILS . " WHERE `user_id`='$id'") or die(ezw_db_error());
		ezw_db_query("DELETE FROM " . TABLE_USER_ADDRESS . " WHERE `user_id`='$id'") or die(ezw_db_error());
	}
	function deleteFlag($id)
	{
		ezw_db_connect();
		ezw_db_query("update " . TABLE_USER . " set status = ".MASK_DELETED." WHERE `user_id`='$id'") or die(ezw_db_error());
	}
	
	function getId(){
		return $this->id;
	}
	function setId($val){
		$this->id = $val;
	}
	
	// HTML Output methods
	/*
	+----------------------------------------
	| Get user address select list
	+----------------------------------------
	*/
	function get_user_address_list($user_id=0)
	{
		// get groups allowed
		ezw_db_connect();
		if($user_id==0) $user_id = $this->getId(); //$_SESSION['user_details']['user_id'];
		$result = ezw_db_query("select * from " . TABLE_USER_ADDRESS . " where user_id = '$user_id'") or die(ezw_db_error());
		$user_address_id = ezw_db_getfieldvalue("select user_address_id from " . TABLE_USER_DETAILS . " where user_id = '$user_id'","user_address_id");
		$ret = '<select class="sfield" name="address_id" id="id_address_id" style="width:200px;" onchange="if(this.selectedIndex>0){location.href=\'address.php?action=edit&address_id=\'+this.options[this.selectedIndex].value;}else{location.href=\'address.php?action=add\'};"><option value=0></option>';
		while ($row = ezw_db_fetch_array($result))
		{
			$selected = ($row['address_id'] == $_REQUEST['address_id'])?"selected":"";
			$ret .= '<option value="'.$row['address_id'].'" '.$selected.'>'.$row['name'].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get user history href list
	+----------------------------------------
	*/
	function get_user_history_list($user_id=0)
	{
		// get groups allowed
		ezw_db_connect();
		if($user_id==0) $user_id = $this->getId();
		$result = ezw_db_query($this->getUserHistoryLogSQL($user_id,10)) or die(ezw_db_error());
		$ret = '<div id="sectionLinks">';
		while ($row = ezw_db_fetch_array($result))
		{
			$ret .= '<a href="editor.php?action=edit&id='.$row['id'].'&type='.$row['type'].'">'.$row['title'].'</a>';
		}
		$ret .= '</div>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get user history href list
	+----------------------------------------
	*/
	function get_user_new_item_list($user_id=0)
	{
		// get groups allowed
		ezw_db_connect();
		if($user_id==0) $user_id = $this->getId();
		$result = ezw_db_query("select id, name as title, icon from node_type where (status & ".MASK_HIDDEN.")=0") or die(ezw_db_error());
		$ret = '<div id="sectionLinks">';
		while ($row = ezw_db_fetch_array($result))
		{
			$ret .= '<a style="padding-left:40px;background:url('.DIR_WS_PUBLIC.'js/tree/icons/'.$row['icon'].') left no-repeat;" href="editor.php?action=add&type='.$row['title'].'">'.ucwords($row['title']).'</a>';
		}
		$ret .= '</div>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get user admin todo list - documents waiting for approval by this user
	+----------------------------------------
	*/
	function get_user_admin_todo_list($user_id=0)
	{
		// get groups allowed
		ezw_db_connect();
		if($user_id==0) $user_id = $this->getId();
		$result = $this->getPendingApprovalPages($user_id,10);
		$ret = '<div id="sectionLinks">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">Pages Pending Approval by me</a></caption>
		<tr class="heading">
			<th width="1%">ID</th><th width="70%">Page Title</th><th width="29%">Editor Name</th>
		</tr>';
		while ($row = ezw_db_fetch_array($result))
		{
			$ret .= '
		<tr>
			<td>'.$row['id'].'</td>
			<td><a style="padding-left:40px;background:url('.DIR_WS_PUBLIC.'js/tree/icons/'.$row['icon'].') left no-repeat;" href="editor.php?action=edit&id='.$row['id'].'&type='.$row['type'].'">'.$row['title'].'</a></td>
			<td><a href="'.$row['email'].'">'.$row['editor'].'</a></td>
		</tr>';
		}
		$ret .= '</table></div>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get user account select list
	+----------------------------------------
	*/
	function get_user_account_type_list($user_account_type_id=0,$default_pair='')
	{
		// get
		ezw_db_connect();
		$result = ezw_db_query("select user_account_type_id as value, user_account_type_name as text from " . TABLE_USER_ACCOUNT_TYPE . " ") or die(ezw_db_error());
		$selected_value = $user_account_type_id?$user_account_type_id:(int)$_SESSION['user_details']['user_account_type'];
		$ret = '<select class="sfield" name="user_account_type" id="id_user_account_type" style="width:200px;">';
		if(is_array($default_pair)) {
			$values = array_values($default_pair);
			$text = array_keys($default_pair);
			$ret .= '<option value="'.$values[0].'">'.$text[0].'</option>';
		}
		while ($row = ezw_db_fetch_array($result))
		{
			$selected = ($row['value'] == $selected_value)?"selected":"";
			$ret .= '<option value="'.$row['value'].'" '.$selected.'>'.$row['text'].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get groups select list
	+----------------------------------------
	*/
	function get_user_groups_list($groups=0)
	{
		// get groups allowed
		ezw_db_connect();
		$user_id = $this->getId(); //$_SESSION['user_details']['user_id'];
		$user_groups = $_SESSION['user_details']['groups'];
		$result = ezw_db_query("select user_group_mask as mask, user_group_name as name from " . TABLE_USER_GROUP . " where ((user_group_mask & ".$user_groups.")>0 and (status & ".MASK_HIDDEN.")>0 or (status = 0)) order by user_group_mask desc") or die(ezw_db_error());
		$ret = '<select class="sfield" name="groups[]" id="id_user_group" multiple size="4" style="width:200px;" cumask='.$user_groups.' gmask='.$groups.'>';
		while ($row = ezw_db_fetch_array($result))
		{
			$selected = ((int)$row['mask'] & (int)$groups)?"selected":"";
			$ret .= '<option value="'.$row['mask'].'" '.$selected.'>'.$row['name'].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get roles select list
	+----------------------------------------
	*/
	function get_user_roles_list($roles=0)
	{
		// get groups allowed
		ezw_db_connect();
		$user_id = $this->getId(); //$_SESSION['user_details']['user_id'];
		$user_roles = $_SESSION['user_details']['roles'];
		$result = ezw_db_query("select user_role_mask as mask, user_role_name as name from " . TABLE_USER_ROLE . " where ((user_role_mask & ".$user_roles.")>0 and (status & ".MASK_HIDDEN.")>0) or (status = 0)") or die(ezw_db_error());
		$ret = '<select class="sfield" name="roles[]" id="id_user_role" multiple size="4" style="width:200px;" cumask='.$user_roles.' gmask='.$roles.'>';
		while ($row = ezw_db_fetch_array($result))
		{
			$selected = (((int)$row['mask'] & (int)$roles)>0)?"selected":"";
			$ret .= '<option value="'.$row['mask'].'" '.$selected.'>'.$row['name'].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get roles select list
	+----------------------------------------
	*/
	function get_user_status_list($status=0)
	{
		// get groups allowed
		$ret = '<select class="sfield" name="status" id="id_status" style="width:200px;" us="'.$_SESSION['user_details']['status'].'">';
		//ezw_db_connect();
		$user_id = $this->getId(); //$_SESSION['user_details']['user_id'];
		$user_status = $_SESSION['user_details']['status'];
		$ret .= '<option value="0" '.(($user_status==0)?"selected":"").'>Active</option>';
		$ret .= '<option value="2" '.(($user_status==2)?"selected":"").'>Inactive</option>';
		if($user_status & 4)
			$ret .= '<option value="4" '.(($user_status==4)?"selected":"").'>System</option>';
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get members menu
	+----------------------------------------
	*/
	function getMenu()
	{
		return '<div id="sectionLinks">
		<a href="'.DIR_WS_PUBLIC.'">Hocti.com</a>
		<a href="profile.php">My Profile</a>
		<!-- <a href="address.php">My Addresses</a>
		<a href="orders.php">My Orders</a> -->
		</div>';
	}
}
?>
