<?php
	/**
	 * Extends text-only email class to allow multipart html & plain-text email
	 *
	 * @author Andrew Blair
	*/
	class MailPlus extends Mail {
		var $htmlMessage;
			
		/**
		 * Sets the HTML code part of the email.
		 * You can use a template if you want just like you can
		 * for <code>setTextMessage</code>
		 *
		 * @param str htmlMsg HTML source code
		 * @see setTextMessage
		 */
		function setHTMLMessage($htmlMsg){
			/*
				Store the html contents, but first insert '3D' in front of the first double quote in each pair. (3D is the url-encoded equivalent of a quote). I don't know why this is needed, but for Outlook on a PC you need to do this.
				i.e. <html bgcolor="#FFFFFF"> becomes <html bgcolor=3D"#FFFFFF">
			*/
			$this->htmlMessage = preg_replace("'(\")(.*\")'imsU","3D\"$2",$htmlMsg);
		}
		
		/**
		 * Envelopes the multipart contents of the email. Syntax here is crucial, including line breaks.
		 * 
		 * @access private
		 * @return str Combined multiparts of the message body
		 */
		function _getCombinedParts(){
			$br = $this->br;
			$message = "--MuLtIpArT_BoUnDaRy$br";
			$message .= "Content-Type: text/plain;charset=\"iso-8859-1\"$br";
			$message .= "Content-Transfer-Encoding: quoted-printable$br$br";
			$message .= $this->textMessage;
			$message .= "$br$br";
			$message .= "--MuLtIpArT_BoUnDaRy$br";
			$message .= "Content-Type: text/html;charset=\"iso-8859-1\"$br";
			$message .= "Content-Transfer-Encoding: quoted-printable$br$br";
			$message .= $this->htmlMessage;
			$message .= "$br$br";
			$message .= "--MuLtIpArT_BoUnDaRy--$br$br";
			return $message;
		}
		
		/**
		 * Adds multipart message header, defines multipart boundary. This method overrides the parent class'.
		 *
		 * @access private
		 */
		function _makeContentHeaders(){
			$this->headers[] = "Content-Type: Multipart/Alternative;boundary=MuLtIpArT_BoUnDaRy" . $this->br;			
		}
		
		/**
		 * Sends the message.
		 *
		 * @see sendMessage API for MailMan
		 */
		function send(){
			//Safeguards to make sure everything needed to send is present
			if (count($this->from) < 1 || count($this->to) < 1 || !isset($this->subject)){
				die("You must provide at least valid sender,receipient and subject");
			}
			if (!isset($this->textMessage) && !isset($this->htmlMessage)){
				die("You must set both TEXT and HTML message parts");
			}
			
			$this->_makeHeaders();
			
			//combine multiparts
			$message = $this->_getCombinedParts();
			
			//Creates To recipients string. CC and BCC recipient strings are handled as additional headers			
			$recipients = $this->_createRecipientString($this->to);
			//exit(implode($this->br,$this->headers));
			//send message
			//mail($recipients,$this->subject,$message,$this->headers);
			mail($recipients,$this->subject,$message,implode($this->br,$this->headers));
			
			//send kickback message if any
			if (isset($this->kickback)){
				echo $this->kickback;
			}
		}
		
		/**
		 * @see sendMessage API for MailMan
		 */
		function showEmail(){
			$email = "Subject: " . $this->subject . $this->br;
			$email .= "To: " . $this->_createRecipientString($this->to) . $this->br;
			$this->_makeHeaders();
			$email .= $this->headers;
			$email .= $this->_getCombinedParts();
			return $email;
		}
		//==========================================
	}
?>