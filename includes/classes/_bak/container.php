<?php
/*
+--------------------------------------------------------------------------
|   Container Database Wrapper
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to access the page information. Part of the ezwebmaker CMS.
+---------------------------------------------------------------------------
*/

class Container
{
	var $error;
	var $id;
	var $pid;
	var $page_id;
	var $page_pid;
	var $title;
	var $node_title;
	var $content;
	var $url;
	var $url_target;
	var $date;
	var $edate;
	
	var $groups;
	var $roles;
	var $owner_id;
	var $editor_id;
	var $publisher_id;
	
	var $notify_published;
	var $email_owner;
	var $email_site_owner;
	var $email_editor;
	var $email_publisher;
	
	var $keywords;
	var $description;
	var $pagetype;
	var $status;
	var $date_published;
	
	var $node_status;
	var $properties;
	var $tbl;
	var $tblref;
	var $tblcontent;
	
	function Container($id=0,$type='page',$tblcontent='page',$tblref='')
	{
		
		$this->tbl='node';
		$this->tblref=($tblref=='')?'node_ref':$tblref;
		$this->tblcontent=$tblcontent;
		if($id != 0)
		{
			$this->setId($id);
			$this->load();
		}
		else
		{
			$this->properties = array();
			$this->setError("");
			$this->setId("");
			$this->setPid("");
			$this->setTitle("");
			$this->setContent("");
			$this->setUrl("");
			$this->setUrlTarget("");
			$this->setDate(ezw_db_getdate());
			$this->setKeywords("");
			$this->setDescription("");
			
			$this->setPublisherId("");
			$this->setOwnerId("");
			$this->setEditorId("");
			
			$this->setNodeStatus(0);
			$this->setNotifyPublish();
			$this->setEmailOwner();
			$this->setEmailSiteOwner();
			$this->setEmailPublisher();
			$this->setEmailEditor();
			
			$this->setStatus(0);
		}
	}
	/*
	+---------------------------------------------------------------------------
	|  Set Properties Function for all child classes
	+---------------------------------------------------------------------------
	*/
	function setProperties($POST='',$prop_names='',$set_funcs=''){
		if(!is_array($prop_names)) $prop_names=$this->getPropertyNames();
		if(!is_array($set_funcs)) $set_funcs=$this->getPropertySetFunctions();
		if(is_array($POST)) $_POST = array_merge($_POST, $POST);
		$properties=array();
		if(!isset($_POST['properties']))
			$_POST = array_merge($_POST, array('properties' => array()));
		$len=count($prop_names);
		for($i=0;$i<$len;$i++) {
			$name=$prop_names[$i];
			//ezw_dbg(">>$name=".$_POST[$name]."::".$POST[$name]);
			if(isset($set_funcs[$i]) && $set_funcs[$i]!="")
				//$properties = array_merge($properties, array($name => $this->$set_funcs[$i]($_POST[$name])));
				$_POST['properties'] = array_merge($_POST['properties'], array($name => $this->$set_funcs[$i]($_POST[$name])));
			else
				$_POST['properties'] = array_merge($_POST['properties'], array($name => $_POST[$name]));
			//ezw_dbg(">>properties[$name]=".$_POST['properties'][$name]);
		}
		
		
		//if(is_array($_POST['properties']))
		//	$_POST['properties'] = array_merge($_POST['properties'], $properties);
		
		$this->properties = $_POST['properties'];//array_merge($this->properties, $properties);
		
		
		/*
		ezw_dbg("<br><b>setProperties::this->properties=</b>");
		ezw_dbg($_POST['properties']);
		ezw_dbg("<br><b>setProperties::prop_names=</b>");
		ezw_dbg($prop_names);
		ezw_dbg("<br><b>setProperties::set_funcs=</b>");
		ezw_dbg($set_funcs);
		*/
		
		//return $POST;
		$pgid=$this->getPageId();
		if($pgid){
			ezw_db_connect();
			$sql_data = array(
					"object"				=> serialize($this->properties)
			);
			ezw_db_perform('page', $sql_data, 'update', "id='$pgid'");
		}
		
	}
	function getProperties(){
		return $this->properties;
	}
	function getPropertyNames(){
		return explode("|","file|theme|template|thumb");
	}
	function getPropertyDisplayFunctions($admin = true){
		if($admin)
			return array('','','','');
		else
			return array('','','','');
	}
	function getPropertySetFunctions(){
		return array('','','','');
	}
	/*
	+---------------------------------------------------------------------------
	|  Load an object from the database 
	|		id of node
	|		edit flag to get unpublished document
	+---------------------------------------------------------------------------
	*/
	function load($id=0, $edit=false)
	{
		if($id == 0)
			$id = $this->getId();
		ezw_db_connect();
		
		// get the editing page and not the published document
		if(IS_ADMIN==true){
			$sql="select n.id, n.pid, n.name as node_title, n.node_start_date as start_date, n.node_end_date as end_date, n.groups, n.roles, n.owner_id, n.publisher_id, n.editor_id, n.status, p.status as status_page, p.date_published, p.id as page_id, p.pid as page_pid, p.title, p.content, p.date, p.keywords, p.description, p.object, p.type FROM ".TABLE_PAGE." p, ".TABLE_NODE." n, ".TABLE_NODE_REF." ref ";
			$sql.=" WHERE (n.id=ref.node_id) and (p.pid=ref.node_id or p.id=ref.page_id) and ((n.status & ".(MASK_HIDDEN+MASK_DELETED+MASK_ARCHIVE).") = 0) and (n.id='$id')  order by p.id desc limit 1";
		}else{// get published document
			$sql="select n.id, n.pid, n.name as node_title, n.node_start_date as start_date, n.node_end_date as end_date, n.groups, n.roles, n.owner_id, n.publisher_id, n.editor_id, n.status, p.status as status_page, p.date_published, p.id as page_id, p.pid as page_pid, p.title, p.content, p.date, p.keywords, p.description, p.object, p.type FROM ".TABLE_PAGE." p, ".TABLE_NODE." n, ".TABLE_NODE_REF." ref ";
			$sql.=" WHERE (n.id=ref.node_id) and (p.id=ref.page_id) and ((n.status & ".(MASK_HIDDEN+MASK_DELETED+MASK_ARCHIVE).") = 0) and (n.id='$id')";
		}
		$result = ezw_db_query($sql) or die("<b>DB Error in Container::load()</b><br>".ezw_db_error());
		
		if(ezw_db_num_rows($result) > 0 && $id > 0)
		{
			$data = ezw_db_fetch_array($result);
			if($data['groups']!='')
			{
				$groups = $data['groups'];
				if($groups > 0)
				{
					if($_REQUEST['id']==$id) authenticate($groups);
				}
			}
			// set data
			$this->setId($data['id']);
			$this->setPid($data['pid']);
			$this->setPageId($data['page_id']);
			$this->setPagePid($data['page_pid']);
			$this->setTitle($data['title']);
			$this->setNodeTitle($data['node_title']);
			$this->setContent($data['content']);
			$this->setDate($data['start_date']);
			$this->setStartDate($data['date']);
			$this->setEndDate($data['end_date']);
			$this->setKeywords($data['keywords']);
			$this->setDescription($data['description']);
			$this->setGroupSecurity($data['groups']);
			$this->setRoleSecurity($data['roles']);
			$this->setOwnerId($data['owner_id']);
			$this->setEditorId($data['editor_id']);
			$this->setPublisherId($data['publisher_id']);
			$this->setDatePublished($data['date_published']);
			
			$properties = unserialize($data['object']);
			//print_r($properties);
			$this->properties = $properties;
			$_POST = array_merge($_POST, array('properties' => $properties));
			
			// set these up as properties of every node
			$this->setNotifyPublish();
			$this->setEmailOwner();
			$this->setEmailSiteOwner();
			$this->setEmailPublisher();
			$this->setEmailEditor();
			
			$this->removeSlashes();
			$this->setStatus($data['status_page']);
			$this->setNodeStatus($data['status']);
			$ret = true;
		}
		else
		{
			$this->setError("<b>No matching row in DB Container::load()</b><br> id = ".$id);
			$ret = false;
		}
		return $ret;
	}
	/*
	+---------------------------------------------------------------------------
	|		Updates the objects properties into the db
	+---------------------------------------------------------------------------
	*/
	function update_properties($properties){
		if(is_array($properties) || is_object($properties)) $object = serialize($properties);
		else $object = $properties;
		ezw_db_connect();
		$sql_data = array(
				"object"				=> $object
		);
		ezw_db_perform(TABLE_PAGE, $sql_data, 'update', "id='".$this->getPageId()."'");
	}	
	/*
	+---------------------------------------------------------------------------
	|   Takes the current object state and updates the database
	+---------------------------------------------------------------------------
	*/
	function updatePageRef(){
		
	}
	
	function updateNodeRefById($id,$pid=0){
		return $this->updateNodeRef($pid,$id);
	}
	function updateNodeRef($pid,$nid=0){
		ezw_db_connect();
		// insert reference into ref table
		$sql_data = array(
				"page_id"					=> $pid
		);
		if($nid==0) $nid = $this->getId();
		
		$ret = ezw_db_perform(TABLE_NODE_REF, $sql_data, "update", "node_id=$nid");
		ezw_db_close();
		return $nid;
	}
	function insertNodeRef($nid,$pid){
		ezw_db_connect();
		// insert reference into ref table
		$sql_data = array(
				"node_id"					=> $nid,
				"page_id"					=> $pid
		);
		ezw_db_perform('node_ref', $sql_data);
		ezw_db_close();
		return $nid;
	}
	
	function update_page()
	{
		ezw_db_connect();
		$sql_data = array(
				"title"					=> ezw_db_prepare_input($this->getTitle()),
				"type"					=> ezw_db_prepare_input($this->getPagetype()),
				"keywords"			=> ezw_db_prepare_input($this->getKeywords()),
				"description"		=> ezw_db_prepare_input($this->getDescription()),
				"content"				=> ezw_db_escape_string($this->getContent()),
				"date"					=> ezw_db_putdate($this->getDate()),
				"date_modified"	=> 'now()',
				"editor_id" 		=> $_SESSION['user_details']['user_id']
		);
		return ezw_db_perform(TABLE_PAGE, $sql_data, 'update', "id='".$this->getPageId()."'");
	}
	
	
	function getMask($in){
		return $this->addMask($this->getStatus(),$in);
	}
	function addMask($int_mask,$in){
		return ((int)($int_mask+0) & (int)$in)==0?((int)$int_mask+$in):((int)$int_mask+0);
	}
	function removeMask($int_mask,$out){
		return ((int)($int_mask+0) & (int)$out)==0?((int)$int_mask-$out):(int)$int_mask+0;
	}
	
	
	
	function update($published_flag=0)
	{
		// update info node and ref tables
		
		ezw_db_connect();
		$sql_data = array(
				"name"						=> ezw_db_prepare_input($this->getNodeTitle()),
				"url"							=> $this->getUrl(),
				"target"					=> $this->getUrlTarget(),
				"node_start_date"	=> ezw_db_putdate($this->getDate()),
				"node_end_date"		=> ezw_db_putdate($this->getEndDate()),
				"groups"					=> $this->getGroupSecurity(),
				"roles"						=> $this->getRoleSecurity(),
				"publisher_id"		=> $this->getPublisherId(),
				"editor_id"				=> $_SESSION['user_details']['user_id'],
				"status"					=> $this->getNodeStatus()
		);
		print_r($sql_data);
		$ret = ezw_db_perform(TABLE_NODE, $sql_data, 'update', "id='".$this->getId()."'");
		ezw_db_close();
		
		// no point in going on if the previous query fails :(
		if($ret==false) return false;
		
		// update and publish the page
		$ret = $this->update_page();
		if((bool)$published_flag==true){
			$ret = $this->publish($published_flag);
		}
		
		return $ret;
	}
	

	
	/*
	+---------------------------------------------------------------------------
	|		Takes the current object state and inserts a new page and node
	|		returning the new nodes id
	+---------------------------------------------------------------------------
	*/
	function insert_page($node_id=0)
	{
		ezw_db_connect();
		// insert new page into page table
		$sql_data = array(
				"title"				=> ezw_db_prepare_input($this->getTitle()),
				"type"				=> ezw_db_prepare_input($this->getPagetype()),
				"keywords"		=> ezw_db_prepare_input($this->getKeywords()),
				"description"	=> ezw_db_prepare_input($this->getDescription()),
				"content"			=> ezw_db_escape_string($this->getContent()),
				"pid"					=> $node_id,
				"date"				=> ezw_db_putdate($this->getDate()),
				"editor_id" 	=> $_SESSION['user_details']['user_id']
		);
		$result = ezw_db_perform(TABLE_PAGE, $sql_data);
		$insert_page_id = ezw_db_insert_id();
		ezw_db_close();
		$this->setPageId($insert_page_id);
		return $insert_page_id;
	}
	
	
	
	function insert()
	{
		ezw_db_connect();
		
		// insert node details into nav tables
		$sql_data = array(
				"pid"							=> ezw_db_prepare_input($this->getPid()),
				"name"						=> ezw_db_prepare_input($this->getTitle()),
				"url"							=> $this->getUrl(),
				"target"					=> $this->getUrlTarget(),
				"node_type"				=> ezw_db_getfieldvalue("select id from node_type where name ='".$this->getPagetype()."'", "id"),
				"node_start_date"	=> ezw_db_putdate($this->getDate()),
				"node_end_date"		=> ezw_db_putdate($this->getEndDate()),
				"groups"					=> $this->getGroupSecurity(),
				"roles"						=> $this->getRoleSecurity(),
				"owner_id"				=> $_SESSION['user_details']['user_id'],
				"publisher_id"		=> $this->getPublisherId(),
				"editor_id"				=> $_SESSION['user_details']['user_id'],
				"sort_order"			=> ezw_db_getfieldvalue("select (max(sort_order)+1) as sort_order from node where pid ='".$this->getPid()."'", "sort_order"),
				"status"					=> $this->getStatus()
		);
		ezw_db_perform('node', $sql_data);
		$insert_node_id = ezw_db_insert_id();
		$this->setId($insert_node_id);
		ezw_db_close();
		
		// insert new page into page table
		$insert_page_id = $this->insert_page($insert_node_id); // insert page with node_id as pid
		
		// insert reference into ref table and return new node id
		return $this->insertNodeRef($insert_node_id, $insert_page_id);;
	}
	
	function postAdd()
	{
		$this->setPid($_POST['pid']);
		$this->setPagetype($_POST['type']);
		$this->setTitle($_POST['title']);
		$this->setNodeTitle($_POST['title']);
		$this->setContent($_POST['content']);
		$this->setUrl($_POST['url']);
		$this->setUrlTarget($_POST['url_target']);
		$this->setDate($_POST['date']);
		$this->setStartDate($_POST['date']);
		$this->setEndDate($_POST['edate']);
		$this->setKeywords($_POST['keywords']);
		$this->setDescription($_POST['description']);
		$this->setGroupSecurity($_POST['groups']);
		$this->setRoleSecurity($_POST['roles']);
		$this->setStatus($_POST['status']);
		
		$this->setOwnerId($_POST['owner_id']);
		$this->setEditorId($_POST['editor_id']);
		$this->setPublisherId($_POST['publisher_id']);
		
		if($this->hasChildren()){
			$_POST = array_merge($_POST, array("refresh_id"=>$_POST['pid']));
		}else{
			$_POST = array_merge($_POST, array("refresh_id"=> $this->getParentId($_POST['pid'])));
		}
		
		
		
		// Go ahead and create the record and then update the class with the new id
		//$_POST = array_merge($_POST, array('id' => $this->insert()));
		$id = $this->insert();
		$this->setProperties();
		
		// if we are publishing the document imediately and we have publishing rights ;)
		if( ((($_POST['status']+0) & MASK_PUBLISHED)>0 ) && ($_SESSION['user_details']['roles'] & EZW_PUBLISH_MASK)>0 )
			$this->publish();
		
		$ret = $id;//$this->getId();
		
		//exit(($ret!=false)?"<hr><h1>All Done without error</h1>":"<hr><h1>Errors mayhave happened</h1>");
		return $ret;
	}
	
	
	function postEdit($update=true)
	{
		$ret = true;
		$this->setId($_POST['id']);
		$this->load();
		$this->setTitle($_POST['title']);
		$this->setContent($_POST['content']);
		$this->setUrl($_POST['url']);
		$this->setUrlTarget($_POST['url_target']);
		$this->setDate($_POST['date']);
		$this->setStartDate($_POST['date']);
		$this->setEndDate($_POST['edate']);
		$this->setKeywords($_POST['keywords']);
		$this->setDescription($_POST['description']);
		$this->setGroupSecurity($_POST['groups']);
		$this->setRoleSecurity($_POST['roles']);
		$this->setStatus($_POST['status']);
		$this->setPublisherId($_POST['publisher_id']);
		$this->setPagetype();
		
		
		//$publish=($this->getStatus()==MASK_PUBLISHED);
		
		ezw_dbg('<h2>'.empty($this->date_published).'</h2>');
		$publish = false;
		switch($this->getStatus()){
			case MASK_PUBLISHED:
				if(!$this->isPublished()){
					$publish = true;
					$this->setNodeStatus(MASK_PUBLISHED);
					$this->setNodeTitle($this->getTitle());
				}else{
					$this->setNodeTitle($this->getNodeTitle());
				}
				break;
			case 132: // re-publish item
				$publish = true;
				$this->setNodeStatus(MASK_PUBLISHED,true);
				$this->setNodeTitle($this->getTitle());
				//echo($this->getNodeStatus());
				break;
			case 0:
				$this->setNodeStatus(0,true);
				$this->setNodeTitle($this->getTitle());
				break;
			default:
				//if($this->isPublished()){
					
					// everythings all good
					
				//}
				break;
		}
		
		/*if(($this->getStatus()==MASK_PUBLISHED)) {
			ezw_db("getStatus()==MASK_PUBLISHED".$this->getStatus().' & '.MASK_PUBLISHED);
			$this->setNodeTitle($_POST['title']);
		}else if($this->getStatus()==0){
			ezw_db("status = 0<br>");
			if(!$this->isPublished()){
				ezw_db("not isPublished<br>");
				$this->setNodeTitle($_POST['title']);
			}
		}else{
			$this->setNodeTitle($this->getNodeTitle());
		}*/
		$this->setProperties();
		if($update) $ret = $this->update($publish);
		
		return $this->getPid();
		//exit(($ret!=false)?"<hr><h1>All Done without error</h1>":"<hr><h1>Errors mayhave happened</h1>");
	}
	/*
	+---------------------------------------------------------------------------
	|   Duplicates a loaded page object and saves it under the $where node id
	+---------------------------------------------------------------------------
	*/
	function duplicate($POST,$where)
	{
		$this->setPid($where);
		$insert_id = $this->insert($POST);
		return $insert_id;
	}
	
	function publish($insert=false){
		// published is equal to:
		// node.status & MASK_PUBLISHED [only set if $_POST['status']==0]
		// page.status = MASK_PUBLISHED [$_POST['status']]
		// page.date_published = datetime published
		// 
		$this->setStatus($this->getMask(MASK_PUBLISHED));
		ezw_db_connect();
		$sql_data = array(
			"publisher_id"	=> $_SESSION['user_details']['user_id'],
			"status"				=> MASK_PUBLISHED,
			"date_modified"	=> 'now()',
			"date_published" => 'now()'
		);
		$pid = $this->getPageId();
		$ret = ezw_db_perform(TABLE_PAGE, $sql_data, 'update', "id='$pid'");
		$field = "date_published";
		$this->setDatePublished(ezw_db_getfieldvalue("select $field from ".TABLE_PAGE." where id='$pid' ",$field));
		// update node ref to point to published page
		$this->updateNodeRef($this->getPageId());
		
		// create a puplicate of page before publishing for editing
		if($insert!=false) $page_id = $this->insert_page($this->getId());
		
		// notify the editor and owner or this action and return success/failure
		return $this->notifyPublished($ret);
		
	}
	
	function notifyPublishedEmail(){
		require_once(DIR_WS_CLASSES."mail.php");
		require_once(DIR_WS_CLASSES."mail_plus.php");
		require_once(DIR_WS_CLASSES."users.php");
		
		$mail = new Mail;
		$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
		$name = "";
		$user=new User;
		
		// get publisher details
		$pub = $user->getFields($this->getPublisherId());
		$ed = $user->getFields($this->getEditorId());
		$own = $user->getFields($this->getOwnerId());
		
		// get common email array
		$published_email_array = array(
				"title"						=> $this->getTitle(),
				"publisher_name"	=> $pub['firstname'].' '.$pub['lastname'],
				"published_date"	=> ezw_db_putdate($this->getDatePublished(),false,'l, F j Y \a\t h:i a'),
				"published_url"		=> DIR_WS_PUBLIC.$this->getPageType().".php?id=".$this->getId()
		);

		if($this->email_owner && $own!=false){
			$ret = $mail->emsgSend("published","owner",$own,$published_email_array+array("name"=>$own['firstname'].' '.$own['lastname']));
		}
		if($this->email_site_owner && defined('SITE_OWNER') && defined('SITE_OWNER_EMAIL')){
			$ret = $mail->emsgSend("published","site_owner",array('firstname'=>SITE_OWNER,'lastname'=>'','email'=>SITE_OWNER_EMAIL),$published_email_array+array("name"=> SITE_OWNER));
		}
		if($this->email_editor && $ed!=false){
			$ret = $mail->emsgSend("published","editor",$ed,$published_email_array+array("name"=>$ed['firstname'].' '.$ed['lastname']));
		}
		// leave publisher til last because we want to keep the user details
		if($this->email_publisher && $pub!=false) {
			if($ret!=false)
				$published_message = " and ".$ed['firstname'].' '.$ed['lastname'].' has been notified';
			else
				$published_message = ", but there was an error sending notification to one of ".$own['email'].",".SITE_OWNER_EMAIL." recipients.";
			$ret = $mail->emsgSend("published","publisher",$email,$published_email_array+array("published_message"	=> $published_message, "name"=>$email['firstname'].' '.$email['lastname']));
		}
		
		return $ret;

	}
	
	function notifyPublished($ret){
		if($this->notify_published){
			if($ret!=false)
				$ret = $this->notifyPublishedEmail();
		}
		return $ret;
	}
	
	function getNodeStatus(){
		return $this->node_status;
	}
	function setNodeStatus($val='',$override=false){
		if($override!=false) $this->node_status = ($val!='')?$val:0;
		else if($this->getStatus()==0) $this->node_status = 0;
	}

    
	/*
	+---------------------------------------------------------------------------
	|   Deletes a node from the database
	+---------------------------------------------------------------------------
	*/
	function delete($id=0)
	{
		if($id == 0)
			$id = $this->getId();
		ezw_db_connect();
		// get page id
		$page_id = $this->getPageId();
		ezw_db_query("DELETE FROM ".TABLE_NODE_REF." WHERE `node_id`='$id'") or die(ezw_db_error());
		ezw_db_query("DELETE FROM ".TABLE_PAGE." WHERE `id`='$page_id'") or die(ezw_db_error());
		ezw_db_query("DELETE FROM ".TABLE_NODE." WHERE `id`='$id'") or die(ezw_db_error());
		ezw_db_close();
		$this = NULL;
		$ret = true;
		return $ret;
	}
	
	function removeSlashes()
	{
		$this->setTitle(stripcslashes($this->getTitle()));
		$this->setContent(stripslashes(stripcslashes($this->getContent())));
		//$this->setFile(stripslashes($this->getFile()));
		//$this->setThumb(stripslashes($this->getThumb()));
	}

/*
+---------------------------------------------------------------------------
|   Database Queries
+---------------------------------------------------------------------------
*/
	// Returns an integer representing the depth from level 0
	function getLevel($id=0)
	{
		if($id = 0)
			$id = $this->getId();
		ezw_db_connect();
		$level = 0;
		$currlevel = $level;
		while($id != 0)
		{
			$result = ezw_db_query("SELECT n.pid FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and `n.id`='$id'") or die(ezw_db_error());
			$data = ezw_db_fetch_array($result);
			$id = $data['pid'];
			$currlevel = $level;
			$level++;
		}
		return $currlevel;
	}
	function searchSerializedObject($search)
	{
		ezw_db_connect();
		// We need to create a match string as it would appear in a serialized object
		$match = '%s:'.strlen($search).':"'.$search.'"%'; // ie s:4:"blah"
		$result = ezw_db_query("SELECT * FROM ".TABLE_PAGE." WHERE `object` LIKE '".$match."'") or die(ezw_db_error());
		$number = ezw_db_num_rows($result);
		ezw_db_close();
		return $number;
	}
	function regexpPageObject($regexp)
	{
		ezw_db_connect();
		// We need to create a match string as it would appear in a serialized object
		$result = ezw_db_query('SELECT * FROM page WHERE object REGEXP "'.$regexp.'"') or die(ezw_db_error());
		$number = ezw_db_num_rows($result);
		ezw_db_close();
		return (int)$number;
	}
	// Returns number of rows using $file
	function checkFile($file)
	{
		return $this->regexpPageObject('\"file\";s:[0-9]+:\"([[:alnum:]]\|'.$file.'|'.$file.')');
	}
	
	// Returns number of rows using $thumb
	function checkThumb($thumb)
	{
		return $this->searchSerializedObject($thumb);
	}
	
	
    
	// Returns number of rows with $image in the content
	function checkImage($image)
	{
		ezw_db_connect();
		// We need to create a match string
		$match = '%'.$image.'%';
		$result = ezw_db_query("SELECT * FROM ".TABLE_PAGE." WHERE `content` LIKE '".$match."'") or die(ezw_db_error());
		$number = ezw_db_num_rows($result);
		ezw_db_close();
		return $number;
	}
	
	function getFileLink($mode='',$title='Download Files',$title_view='View Files')
	{
		$files = '';
		$files_view = '';
		$theme = $this->getTheme();
		//echo($this->getFile());
		if($this->getFile()!='')
		{
			$filenames = explode("|",$this->getFile());
			//print_r($filenames);
			foreach($filenames as $value)
			{
				//echo(DIR_FS_FILES.$value);
				if(file_exists(DIR_FS_FILES.$value))
				{
					$display_name=preg_replace("/_/i"," ",$value);
					if($mode=='admin')
					{
						//$files .= '<input type="checkbox" name="delfile[]" value="'.$value.'" />&nbsp;<a href="'.DIR_WS_FILES.$value.'" target="_blank">'.$value.'</a><br />';
						$files .= '<a href="'.DIR_WS_FILES.$value.'" target="_blank">'.$value.'</a><br />';
					}
					else
					{
						$files .= '<a href="download.php?dlf='.$value.'">'.$display_name.'</a>';
						if(preg_match("/htm|jpg|gif|png|doc|xls|csv|txt|ppp/i", substr($value,strpos($value,'.'))))
							$files_view .= '<a href="'.DIR_WS_FILES.$value.'">'.$display_name.'</a>';
					}
				}
			}
			if($files != '')
			{
				if($mode!='admin')
				{
					$files = '<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<th height="30" align="left"><span id="features_heading">'.$title.'</span></th>
		</tr>
		<tr>
			<td>
				<div id="features">'.$files.'</div>
			</td>
		</tr>
		';
		if($files_view!=""){
					$files .= '
		<tr>
			<td>
				<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<th height="30" align="left"><span id="features_heading">'.$title_view.'</span></th>
		</tr>
		<tr>
			<td>
				<div id="features">'.$files_view.'</div>
			</td>
		</tr>
		';
		}
					$files .= '
		<tr>
			<td height="30">&nbsp;</td>
		</tr>
		</table>';
		//			<table id="related_files"><tr valign="top"><td>:</td><td><div id="sectionLinks"></div></td><td>'.$title_view.':</td><td><div id="sectionLinks">'.$files_view.'</div></td></tr></table>';
				}
			}
		}
		return $files;
	}
	

	
	function isParentProtected()
	{
		return false;
	}
/*
+---------------------------------------------------------------------------
|   Editor Extra Field Display
+---------------------------------------------------------------------------

	function getExtraAdd()
	{
		return '';
	}
    
	function getExtraEdit()
	{
		return '';
	}
*/

/*
+---------------------------------------------------------------------------
|   Editor Extra Field Display
+---------------------------------------------------------------------------
*/
	function getExtraAdd($template='extras',$prop_names='',$prop_funcs='')
	{
		$ret='';
		
		if(is_array($prop_names) && is_array($prop_funcs)){
			$tpl = new Template("templates/$template.htm");
			$values=array();
			$len=count($prop_names);
			// empty the property list
			for($i=0;$i<$len;$i++) {
				$this->properties[$prop_names[$i]] = "" ;
			}
			// fill template values with display properties
			for($i=0;$i<$len;$i++) {
				if($prop_funcs[$i]!=""){
					$values=array_merge($values,array($prop_names[$i] => $this->$prop_funcs[$i]($this->properties[$prop_names[$i]])));
				}else
					$values=array_merge($values,array($prop_names[$i] => "" ));
			}
			
			$tpl->parse($values);
			$ret .= $tpl->template;
		}
		return $ret;
	}

	function getExtraEdit($template='extras',$prop_names='',$prop_funcs='',$show_always=true)
	{
		$ret='';
		//ezw_dbg($prop_names,true);
		//ezw_dbg($prop_funcs,true);
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$show = $show_always;
			//ezw_dbg($show,true);
			if($show_always == false) $show = ($this->hasChildren() == false);
			if($show_always == true) $show = true;
			//ezw_dbg($show,true);
			if($show)
			{
				$tpl = new Template("templates/$template.htm");
				$values=array();
				//ezw_dbg($this->properties,true);
				if(is_array($this->properties)){
					$len=count($prop_names);
					for($i=0;$i<$len;$i++) {
						//ezw_dbg($prop_funcs[$i],true);
						if($prop_funcs[$i]!="")
							$values=array_merge($values,array($prop_names[$i] => $this->$prop_funcs[$i]($this->properties[$prop_names[$i]])));
						else
							$values=array_merge($values,array($prop_names[$i] => $this->properties[$prop_names[$i]]));
					}
				}
				$tpl->parse($values);
				$ret .= $tpl->template;
			}
		}
		return $ret;
	}

	function getAddItem($template='additem',$prop_names='',$prop_funcs='',$show_always=true)
	{
		$ret='';
		//print_r($_POST['properties']);
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$show = $show_always;
			if($show_always == false) $show = ($this->hasChildren() == false);
			if($show_always == true) $show = true;
			//echo($show_always);
			if($show)
			{
				$tpl = new Template("templates/$template.htm");
				$values=array();
				if(is_array($this->properties)){
					$len=count($prop_names);
					for($i=0;$i<$len;$i++) {
						if($prop_funcs[$i]!="")
							$values=array_merge($values,array($prop_names[$i] => $this->$prop_funcs[$i]($this->properties[$prop_names[$i]],true)));
						else
							$values=array_merge($values,array($prop_names[$i] => $this->properties[$prop_names[$i]]));
					}
				}
				$tpl->parse($values);
				$ret .= $tpl->template;
			}
		}
		return $ret;
	}

/*
+---------------------------------------------------------------------------
|   Legacy Functionality
|   We will want to reimplement these at some stage
+---------------------------------------------------------------------------
*/

	function getAll()
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id") or die(ezw_db_error());
		$idArray = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
	
		ezw_db_close();
		return $idArray;
	}

	
	// Returns a mysql result resource of the children of a given parent id
	function getChildren($order='title ASC')
	{
		$pid = $this->getId();
		ezw_db_connect();
		$result = ezw_db_query("SELECT n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and `n.pid`='$pid' ORDER BY $order") or die(ezw_db_error());
		$idArray = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
	
		ezw_db_close();
		return $idArray;
	}


	// MUST REMOVE THIS FUNCTION!!!!
	// Returns a mysql result resource of the children of a given parent id
	function getChildrenData($pid, $order='title ASC')
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT p.* FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and `n.pid`='$pid' ORDER BY $order") or die(ezw_db_error());
		$whole_result = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$whole_result[] = $row;
		}
	
		ezw_db_close();
		return $whole_result;
	}
    
	function getChildrenObject($pid, $order='title ASC')
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT p.object FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and `n.pid`='$pid' ORDER BY $order") or die(ezw_db_error());
		$whole_result = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$whole_result[] = unserialize($row['object']);
		}
	
		ezw_db_close();
		return $whole_result;
	}

	// Does this id have any children - returns true or false
	function hasChildren($id=0)
	{
		if($id == 0)
			$id = $this->getId();
		ezw_db_connect();
		//$result = ezw_db_query("SELECT * FROM ".TABLE_PAGE." WHERE `pid`='$id'") or die(ezw_db_error());
		$result = ezw_db_query("SELECT * FROM ".TABLE_NODE." WHERE `pid`='$id'") or die(ezw_db_error());
		if(ezw_db_num_rows($result) > 0)
			$ret = true;
		else
			$ret = false;
		ezw_db_close();
		return $ret;
	}

	// Returns the number of children for a given parent id
	function numChildren($order='sort_order ASC')
	{
		$pid = $this->getId();
		ezw_db_connect();
		$result = ezw_db_query("SELECT * FROM ".TABLE_NODE." WHERE `pid`='$pid' ORDER BY $order") or die("Database Error in Container::numChildren | ".ezw_db_error());
		$num = ezw_db_num_rows($result);
		ezw_db_close();
		return $num;
	}

	// Returns a mysql result resource of the children of a given parent id, limited to $offset,$limit
	function getChildrenLimited($offset, $limit, $order='n.id ASC')
	{
		$pid = $this->getId();
		ezw_db_connect();
		$result = ezw_db_query($this->getChildrenLimitedSQL($pid, '', $order)." LIMIT $offset,$limit") or die("Database Error in Container::getChildrenLimited | ".ezw_db_error());
		$idArray = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
		ezw_db_close();
		return $idArray;
	}
	function getChildrenLimitedSQL($pid, $filter='', $order='n.id ASC')
	{
		if($filter!='') $filter=' and ('.$filter.')';
		return "select * from ".TABLE_NODE." n, ".TABLE_NODE_REF." r where n.id=r.node_id and n.pid='$pid' $filter order by $order ";
	}


	// Returns the number of results of $type from search $string
	function numSearch($string, $type, $order='p.title ASC')
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and MATCH(p.title, p.content) AGAINST ('".$string."') >= 0.1 AND p.type LIKE '%$type%' ORDER BY $order") or die("Database Error in Container::numSearch | ".ezw_db_error());
		$num = ezw_db_num_rows($result);
		ezw_db_close();
		return $num;
	}

	// Returns a mysql result resource of a search for $string, limited to $offset,$limit
	function getSearchLimited($string, $offset, $limit, $type, $order='title ASC')
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and MATCH(p.title, p.content) AGAINST ('".$string."') >= 0.1 AND p.type LIKE '%$type%' ORDER BY $order LIMIT $offset,$limit") or die(ezw_db_error());
		$idArray = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
		ezw_db_close();
		return $idArray;
	}

	// Returns a mysql result resource from $type, limited to $offset,$limit an ordered by $order
	function getLimited($offset, $limit, $type, $order, $filter = '')
	{
		ezw_db_connect();
		$result = ezw_db_query($this->getLimitedSQL($type, $order, $filter)." LIMIT $offset,$limit ") or die(ezw_db_error());
		$idArray = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
		ezw_db_close();
		return $idArray;
	}
	function getLimitedSQL($type, $order, $filter='')
	{
		if(!empty($filter)) $filter = " and ".$filter;
		return "select n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r where n.id=r.node_id and p.id=r.page_id and p.type='$type' and n.pid!=0 $filter order by $order ";
	}

  // Return the id of the first page matching $title
	function getIdByTitle($title)
	{
		ezw_db_connect();
		$result = ezw_db_query("SELECT n.id, p.title FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r WHERE n.id=r.node_id and p.id=r.page_id and `p.title`='$title'") or die(ezw_db_error());
		if($value = ezw_db_result($result, 0))
		{
			$value = stripslashes($value);
		}
		else
		{
			$value = "Database Error in Container::getIdByTitle";
		}
		return $value;
	}
	
	// Return path to selected template file for this $id
	function getTemplateFile()
	{
		ezw_db_connect();
		$tpl_id = ($this->getTemplate()) ? $this->getTemplate() : DEFAULT_TEMPLATE_ID;
		$sql="SELECT file_path FROM template WHERE id=$tpl_id";
		$result = ezw_db_query($sql) or die(ezw_db_error());
		if($value = ezw_db_result($result, 0))
		{
			if(file_exists($value))
				$value = stripslashes($value);
			else
				$value = "templates/main.htm";
		}
		else
		{
			$value = "templates/main.htm";
		}
		return $value;
	}

/*
+---------------------------------------------------------------------------
|   Default Container functions
+---------------------------------------------------------------------------
*/
	
	function getBreadcrumbs($id=0){
		if($id==0) $id=$this->getId();
		$pid = $id;
		$aPid[] = $id;
		while( $pid > 0 )
		{
			$pid = $this->getParentId($id);
			if($pid > 0) {
				$aPid[] = $pid;
				$id = $pid;
			}
		}
		return $aPid;
	}
	
	function buildCrumbs($id=0)
	{
		$pagetypes = array(
			"index" => "Home",
			"page" => "Home",
			"news" => "Home",
			"product" => "Home",
			"schedule" => "Home",
			"course" => "Home",
			"testimonial" => "Home"
		);
		$limit_str_length = (defined('BREADCRUMBS_ITEM_LENGTH'))?BREADCRUMBS_ITEM_LENGTH:15;
		
		$node_id = ($id==0)?$this->getId():$id;
		if($node_id==HOME_BASE_ID) return 'Home';
		$pid = $node_id;
		ezw_db_connect();
		$aPid = $this->getBreadcrumbs($pid);
		$aPid[]=HOME_BASE_ID;
		//print_r($aPid);
		//exit;
		foreach($aPid as $nid)
		{
			$query = "select n.name as title, t.name, n.url, n.target from node n, node_type t where n.node_type = t.id and n.id = '$nid'";
			$result = ezw_db_query($query) or die(ezw_db_error());
			$home_link = '<a href="index.php">Home</a>';
			if($row = ezw_db_fetch_array($result))
			{
				//echo($row['name'].'<br>');
				$page_type = $row['name'];
				$node_title = $row['title'];
				$url=$row['url'];
				$target=$row['target'];
			
				if(!empty($url)) {
					$crumb = '<a href="'.$url.'" ';
					if(!empty($target)) $crumb.=' target="'.$target.'" ';
					$crumb.='>'.$node_title.'</a> -> ' . $crumbs;
				}else{
					switch($nid){
						case $node_id:
							$crumbs = $node_title;
							break;
						case (int)HOME_BASE_ID:
							$home_link = '<a href="index.php" title="'.$node_title.'">'.$pagetypes['index'].'</a> -> ';
							break;
						default:
							$crumbs = '<a href="'.$page_type.'.php?id='.$nid.'" title="'.$node_title.'">'.substr($node_title,0,$limit_str_length).((strlen($node_title)>$limit_str_length)?"...":"").'</a> -> ' . $crumbs;
							break;
					}
				}
			}
		}
		return $home_link.$crumbs;
	}
	
	function buildTopNav($mode=''){
		$ret = "";
		$theme = TEMPLATE_DEFAULT_GROUP;//$this->getTheme();
		if(isset($_SESSION['user_details']) && is_array($_SESSION['user_details']))
		{
			if($mode=='admin')
			{
				$ret .= '<a href="index.php?logout=1" target="_top" class="headerUtilities"><img src="templates/'.$theme.'/images/layout/logoff.gif" alt="Logout" title="Logout" border="0" /></a>&nbsp;&nbsp;';
				$ret .= '<a href="profile.php" target="frmMainFrame" class="headerUtilities"><img src="templates/'.$theme.'/images/layout/account.gif" alt="My Details" title="My Membership Details" border="0" /></a>&nbsp;&nbsp;';
			}
			else
			{
				$ret .= '<a href="index.php?logout=1" target="_top" class="headerUtilities" title="Logout">Logout</a>&nbsp;&nbsp;';
				$ret .= '<a href="profile.php" target="_top" class="headerUtilities" title="My Membership Details">My&nbsp;Details</a>';
			}
		}
		else
		{
			$ret .= '<a href="login.php" target="_top" class="headerUtilities" title="Login">Login</a>&nbsp;&nbsp;';
		}
		if($mode!='admin' && isset($_COOKIE['basket']) && !empty($_COOKIE['basket']))
		{
			$ret .= '&nbsp;&nbsp;<a href="cart.php" class="headerUtilities">Shopping Basket</a>&nbsp;&nbsp;<a href="checkout.php" class="headerUtilities">Checkout</a>';
		}
		return $ret;
	}
	
	function getSignupForm()
	{
		if(is_array($_SESSION['user_details']))
			return '';
		else
			return '
<script>
<!--
function isEmail (string) {
  var pat = 
    /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
  return pat.test(string);
}
function validateemail (e) {
	ret = true;
  if (!isEmail(e.value)) {
    alert("Please enter a valid email address!");
		e.focus();e.select();
		ret = false;
  }
	return ret;
}
//-->
</script>
<form name="maillistform" action="post.php?action=addemail" method="post" id="id_maillistform" onsubmit="return validateemail(this.email);">
<input name="newsletter" type="hidden" value="1" />
'.ezw_get_box_start().'Receive Newsletter'.ezw_get_box_body().'To receive our newsletter please enter your email address and click the "Signup" button
	<input name="email" type="text" class="sfield" size="20" style="width: 175px;" value="Enter Email..." val="Enter Email..." onfocus="focusValue(this);" onblur="blurValue(this);" /><br>
	<input type="submit" value="Signup" style="width:175px;" />'.ezw_get_box_end().'
</form>';
	}
	
	function getBanner($size='468x60',$count=1, $display_type = '')
	{
		ezw_db_connect();
		$ret = '';
		$i=0;

		if ($banners = ezw_banner_exists('all_id', $size))
		{
			$banner_array = array();
			while($row = ezw_db_fetch_array($banners)) 
			{
				$banner_array[] = $row['banners_id'];
			}
			$banner_count = (count($banner_array) >= $count) ? $count : count($banner_array) ;
			if($banner_count > 0)
			{
				$array_id = array_rand($banner_array,$banner_count);
				if(!is_array($array_id)) $array_id = array($array_id);
				for($i=0;$i<count($array_id);$i++)
				{
					//$ret .= ezw_display_banner('static', $banner_array[$array_id[$i]],$display_type);
					switch($display_type){
						case 'jsarray':
							$ret .= ",".ezw_display_banner('static', $banner_array[$array_id[$i]],$display_type);
							break;
						default:
							$ret .= ezw_display_banner('static', $banner_array[$array_id[$i]],$display_type);
							break;
					}
				}
				switch($display_type){
					case 'jsarray':
						$ret .= ",".ezw_display_banner('static', $banner_array[0],'jsarray');
						break;
					default:
						$ret = ezw_display_banner('static', $banner_array[0]);
						break;
				}
				if($display_type=='jsarray'){
					$ret = "var oImg=[['".DIR_WS_PUBLIC."']$ret];";
				}
			}
			else
			{
				switch($display_type){
					case 'jsarray':
						$ret = ezw_display_banner('static', $banner_array[0],'jsarray');
						$ret = "var oImg=[['".DIR_WS_PUBLIC."'],".$ret."];";
						break;
					default:
						$ret = ezw_display_banner('static', $banner_array[0]);
						break;
				}
			}
		}
		
		return $ret;
	}
	
	
	function getParentId($id=0){
		if($id==0) $id=$this->getId();
		if($id==0) return 0;
		$ret = $this->getParentNode($id,'pid');
		return $ret['pid'];
	}
	function getParentTheme($id=0){
		$pid=$this->getParentId($id);
		if($pid==0) return 0;
		$con = new Container($pid);
		$ret=$con->getTheme();
		unset($con);
		if(empty($ret)) $ret = TEMPLATE_DEFAULT_GROUP;
		return $ret;
	}
	function getParentNode($id=0, $fields = '*'){
		if($id==0) $id=$this->getId();
		//$pid=$this->getParentId($id);
		if($id==0) return false;
		ezw_db_connect();
		$ret = ezw_db_query("select $fields from ".TABLE_NODE." where id='$id'");
		return ezw_db_fetch_array($ret);
	}
	function getNodeDetails($id=0, $fields = '*'){
		if($id==0) $id=$this->getId();
		if($id==0) return false;
		ezw_db_connect();
		$ret = ezw_db_query("select $fields from ".TABLE_NODE." where id='$id'");
		return ezw_db_fetch_array($ret);
	}
/*
+---------------------------------------------------------------------------
|   Accessors / Mutators
+---------------------------------------------------------------------------
*/
	
	function setError($val)
	{
		$this->error = $val;
	}
	
	function getError()
	{
		return $this->error;
	}
	
	function setId($val)
	{
		$this->id = $val;
	}
	
	function getId()
	{
		return $this->id;
	}
	
	function setPid($val)
	{
		$this->pid = $val;
	}
	
	function getPid()
	{
		return $this->pid;
	}
	
	
	function setPageId($val)
	{
		$this->page_id = $val;
	}
	
	function getPageId()
	{
		return $this->page_id;
	}
	function getPageIdFromDB()
	{
		$result = ezw_db_query("select page_id from ".TABLE_NODE_REF." where `node_id`='".$this->getId()."'") or die(ezw_db_error());
		$data = ezw_db_fetch_array($result);
		return $data['page_id'];
	}
	
	
	function setPagePid($val)
	{
		$this->page_pid = $val;
	}
	
	function getPagePid()
	{
		return $this->page_pid;
	}
	
	function setOwnerId($val)
	{
		$this->owner_id = $val;
	}
	
	function getOwnerId()
	{
		return $this->owner_id;
	}
	
	function setEditorId($val)
	{
		$this->editor_id = $val;
	}
	
	function getEditorId()
	{
		return $this->editor_id;
	}
	
	function setPublisherId($val)
	{
		$this->publisher_id = $val;
	}
	
	function getPublisherId()
	{
		return $this->publisher_id;
	}

	// set and get all email notification fields
	function setNotifyPublish($val='')
	{
		$this->notify_published = ($val=='')?EMAIL_NOTIFY_ON_PUBLISH:$val;
	}
	
	function getNotifyPublish()
	{
		return $this->notify_published;
	}
	
	function setEmailOwner($val='')
	{
		$this->email_owner = ($val=='')?EMAIL_NOTIFY_OWNER_ON:$val;
	}
	
	function getEmailOwner()
	{
		return $this->email_owner;
	}
	
	function setEmailSiteOwner($val='')
	{
		$this->email_site_owner = ($val=='')?EMAIL_NOTIFY_SITE_OWNER_ON:$val;
	}
	
	function getEmailSiteOwner()
	{
		return $this->email_site_owner;
	}
	
	function setEmailPublisher($val='')
	{
		$this->email_publisher = ($val=='')?EMAIL_NOTIFY_PUBLISHER_ON:$val;
	}
	
	function getEmailPublisher()
	{
		return $this->email_publisher;
	}
	
	function setEmailEditor($val='')
	{
		$this->email_editor = ($val=='')?EMAIL_NOTIFY_EDITOR_ON:$val;
	}
	
	function getEmailEditor()
	{
		return $this->email_editor;
	}
	
	
// Display fields
	function setNodeTitle($val)
	{
		$this->node_title = ($val!='')?$val:'Please enter a title';
	}
	
	function getNodeTitle()
	{
		return $this->node_title;
	}


	function setTitle($val)
	{
		$this->title = ($val!='')?$val:'Please enter a title';
	}
	
	function getTitle()
	{
		return $this->title;
	}
	
	function setContent($val)
	{
		$this->content = $val;
	}
	
	function getContent()
	{
		return $this->content;
	}
	
/*
+---------------------------------------------------------------------------
|   Url Properties - Accessors / Mutators
+---------------------------------------------------------------------------
*/
	function getUrl(){
		return $this->url;
	}
	
	function setUrl($val){
		$this->url=$val;
	}
	
	function getUrlTarget(){
		return $this->url_target;
	}
	
	function setUrlTarget($val){
		$this->url_target=$val;
	}	
	
	function setDate($val)
	{
		$this->date = $val;
	}
	
	function getDate()
	{
		return $this->date;
	}
	
	
	function setStartDate($val)
	{
		$this->sdate = $val;
	}
	
	function getStartDate()
	{
		return $this->sdate;
	}
	function setEndDate($val)
	{
		$this->edate = $val;
	}
	function getEndDate()
	{
		return $this->edate;
	}
	
	function setDatePublished($val)
	{
		$this->date_published = $val;
	}
	function getDatePublished()
	{
		return $this->date_published;
	}
	
	function isPublished(){
		$field="p.date_published";
		$id=$this->getId();
		$sql="select $field from ".TABLE_PAGE." p , ".TABLE_NODE_REF." r where p.id=r.page_id and r.node_id='$id'";
		$ret = ezw_db_getdate(ezw_db_getfieldvalue($sql,$field));
		echo("DATE_PUBLISHED=".$ret.':'.$sql);
		return !empty($ret);
	}
	
	function setGroupSecurity($val)
	{
		$val = (is_array($val)) ? getMaskFromList( implode(",", $val) ) : $val ;
		$this->groups = $val;//is_array($val)?implode(",",$val):"";
	}
	
	function getGroupSecurity()
	{
		return $this->groups;
	}
	
	function setRoleSecurity($val)
	{
		$val = (is_array($val)) ? getMaskFromList( implode(",", $val) ) : $val ;
		$this->roles = $val;//is_array($val)?implode(",",$val):"";
		
	}
	
	function getRoleSecurity()
	{
		return $this->roles;
	}
	
	
	function setKeywords($val)
	{
		$this->keywords = ($val!='')?$val:DEFAULT_KEYWORDS;
	}
	
	function getKeywords()
	{
		return $this->keywords;
	}
	
	function setDescription($val)
	{
		$this->description = ($val!='')?$val:DEFAULT_DESCRIPTION;
	}
	
	function getDescription()
	{
		return $this->description;
	}
	
	function setPagetype($val=NULL)
	{
		if(isset($val) && !empty($val))
		{
			$this->pagetype = $val;
		}
		else
		{
			$this->pagetype = get_class($this);
		}
	}
	
	function getPagetype()
	{
		return $this->pagetype;
	}
	
	function setStatus($val)
	{
		$this->status = $val;
	}
	
	function getStatus()
	{
		return $this->status;
	}
	
/*
+---------------------------------------------------------------------------
|   Properties helpers - Accessors / Mutators
+---------------------------------------------------------------------------
*/
	function setFile($val)
	{
		$this->properties['file']=$val;
	}
	
	function getFile()
	{
		return $this->properties['file'];
	}
	
	function setTemplate($val)
	{
		$this->properties['template']=$val;
	}
	
	function getTemplate()
	{
		return $this->properties['template'];
	}
	
	function setThumb($val)
	{
		$this->properties['thumb']=$val;
	}
	
	function getThumb()
	{
		return $this->properties['thumb'];
	}
	
	function setTheme($val='')
	{
		$this->properties['theme'] = ($val!='')?$val:$this->getParentTheme();
	}
	
	function getTheme()
	{
		$val=$this->properties['theme'];
		if(empty($val)) {
			$this->setTheme();
			$val=$this->properties['theme'];
		}
		return $val;
	}
}
?>
