<?php
/***
* This class is designed to create somewhat random passwords
* to set up initial user accounts where
* you email out the initial login information. 
* 
* Created By: Kevin Gilbertson (kevin@gilbertsonconsulting.com)
* Last Updated: 4/22/02
* Modified By: Flinn Mueller (flinn AT activeintra DOT net)
* Last Updated: 5/01/02
* Modified By: Kevin Gilbertson (kevin@gilbertsonconsulting.com)
* Last Updated: 5/15/02
* Modified By: Kevin Gilbertson (kevin@gilbertsonconsulting.com)
* Last Updated: 11/14/02
***/
class Password
{
	var $passwdchars;
	var $passwd;
	var $length;
	var $minlength;
	var $maxlength;

	function Password($min=6, $max=8, $special=NULL, $chararray=NULL)
	{
		if($chararray == NULL) 	{
			$passwdstr = "abcdefghijklmnopqrstuvwxyz";
			$passwdstr .= strtoupper($passwdstr);
			$passwdstr .= "12345678901234567890"; // twice to up the likelyhood
			// add special chars to start
			if ($special) {
				$passwdstr .= "!@#$%";
			}
		} else { 
			$passwdstr = $chararray; 
		}

		for($i=0; $i<=strlen($passwdstr) - 1; $i++) {
			$this->passwdchars[$i]=$passwdstr[$i];
		}
		
		// randomize the chars
		srand ((float)microtime()*1000000);
		shuffle($this->passwdchars);

		$this->minlength = $min;
		$this->maxlength = $max;
	}
	// private methods
	function setLength($val=0)	
	{
		$this->length = ($val==0) ? rand($this->minlength, $this->maxlength) : ($val+0);
	}

	function setMin($val)
	{
		$this->minlength = $val;
	}

	function setMax($val)
	{
		$this->maxlength = $val;
	}
	
	function setPassword($val)
	{
		$this->passwd = $val;
	}

	function getPassword()
	{
		$this->passwd = NULL; 
		$this->setLength();

		for($i=0; $i<$this->length; $i++)
		{
			$charnum = rand(0, count($this->passwdchars) - 1);
			$this->passwd .= $this->passwdchars[$charnum];
		}

		return $this->passwd; 
	}

	// to show in browser
	function getHTMLPassword()
	{
		return (htmlentities($this->getPassword()));
	}

	// Allows password to be shown as an image
	// Also semi-tempest resistant, with random text position,
	// and nifty gray color which should difuse tempest emissions
	// Created By: Flinn Mueller (flinn AT activeintra DOT net)
	function getImgPassword()
	{
		$RandPassword = $this->getPassword();
		$this->getImage();
	}
	
	function getImage()
	{
		// create the image
		$png = imagecreate(500,200);
		$canvas = imagecolorallocate($png,200,140,220);
		$bg = imagecolorallocate($png,192,192,192);
		$tx = imagecolorallocate($png,128,128,128);
		imagefilledrectangle($png,0,0,500,200,$canvas);
		imagefilledrectangle($png,5,5,495,195,$bg);
		
		imageellipse($png, 200, 150, 300, 200, $canvas);
		
		srand ((float)microtime()*1000000);
		//imagestring($png,3,rand(0,90),rand(0,50),$this->passwd,$tx);
		imagettftext($png, 40, rand(-10,10), rand(10,200),rand(50,100), $tx, "smash1.ttf", $this->passwd);

		// send the image
		header("content-type: image/png");
		imagepng($png);
		imagedestroy ($png);
	}
}
?>
