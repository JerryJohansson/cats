<?php
//require_once ("../conf.php");
//require_once (CATS_ADODB_PATH.'adodb.inc.php');
class User {
	var $db;
	var $fields=array();
	function User()
	{
	
	//$db = ADONewConnection(DB_DRIVER); # eg 'mysql' or 'postgres'
//$db->Connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
//$rs = $db->Execute('select * from tblPCR_Details where pcr_id = 1');
		$this->db = ADONewConnection(DB_DRIVER);
	}
	
	function login($uid='',$pwd='',$action='Logout')
	{
		//echo($uid.':'.$pwd.':'.$action);
		if($action=='Logout')
		{
			unset($_SESSION['user_details']);
		}
		elseif($action=='Login')
		{
			//$sql = "select * from users where uid = '$uid' and pwd = '$pwd'";
			//$result = $this->db->query($sql);
			$s = "{'message':";
			if(is_array($this->get_user_details($uid,$pwd))){
				$s .= "'Good Morning ".$_SESSION['user_details']['first_name'].", You have successfully logged into the system.\\nPlease wait while you are transferred to your dashboard.'";
				$s .= ",'success':true";
				$s .= ",'redirect':'index.php?top=true'";
				$s .= ",'callback_function':'top.CATS_WINDOW=_open(\"/cats/index.php\",\"CATS_WINDOW\",\"_op\",\"show_user_dashboard\");'";
				$_SESSION['USER_GROUP'] = 1+2+4+8+16+32+46+128+256+512+1024+2048+4096;
			}else{
				$s .= "'Your login and/or password are in correct.\\n\\nPlease check it and try again.'";
				$s .= ",'success':false";
			}
			$s .= "}";
		}
		echo($s);
		return $s;
	}
	
	function get_user_details($uid='',$pwd='')
	{
		//echo($uid.':'.$pwd);
		if($uid=='vern' && $pwd=='test'){
			$_SESSION['user_details'] = array(
				"uid" => "X002",
				"user_name" => "webdog",
				"first_name" => "Vernon",
				"last_name" => "Laskey",
				"email" => "webdog@buzmedia.com.au",
				"lastlogdate"	=> date('l dS of F Y',strtotime('01-Jan-2005')),
				"lastlogtime"=> date('h:i:s A',strtotime("now")),
				"HISTORY_MIN_LENGTH" => 5
			);
		}else{
			$_SESSION['user_details'] = "";
		}
		return $_SESSION['user_details'];
	}
	function get_user_login_form()
	{
		$s='
<div id="login">
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
<tr valign="middle">
	<td>
		<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="box">
		<tr>
			<td align="center" valign="middle">
				<form method="POST" onSubmit="return _login(this);">
		
				<div class="maincontentheader">
					 <h4>Welcome to CATS</h4>
					 <p>Please enter a valid login and password.</p>
					 <p><!-- message --></p>
				</div>
				<table>
				<tr valign="top">
					<td><img src="icons/login.gif" alt=""/></td>
					<td>
						<div class="block">
						<label for="id1">Login</label>
						<input class="halfbox" type="text" size="10" name="uid" id="id1" value="" />
						</div>
						
						<div class="block">
						<label for="id2">Password</label>
						<input class="halfbox" type="password" size="10" name="pwd" id="id2" value="" />
						</div>
						
						<div class="buttonblock">
						<input type="submit" name="login" value="Login" />
						</div>
					</td>
				</tr>
				</table>
				</form>	
			</td>
		</tr>
		</table>
	</td>
</tr>
<!--<tr>
	<td class="copyright" height="40">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</td>
</tr>-->
</table>
</div>
		';
		$s=preg_replace("/\r\n/","",$s);
		$s="{'message':'".$s."','success':true}";
		return $s;
	}
	function set_fields($field_name='',$page_type='menu')
	{
		$where_parent = " and f1.pid = ".(($page_type=='menu') ? " 0 " : " f2.id and f2.field_name = '".$field_name."' ");
		$where_table = (($page_type=='menu') ? " , form_fields f2 " : "");
		$sql = "
			select f1.field_name,f1.field_title,f1.field_type,f1.field_length,f1.field_value,f1.field_columns,f1.field_mandatory,f1.field_options 
			from form_fields f1 ".$where_tables."  
			where	f1.page_type = '".$page_type."' and 
						(f1.user_group_mask & ".$_SESSION['user_details']['user_group_mask'].")>0 ".$where_parent;
		$result = $this->db->query($sql);
		if($result->execute()){
			$num = $result->fields();
			while ($row = $result->fetch ()) {
				$aTypes = $result->getTypes();
				for($i=0;$i<$num+0;$i++){
					$name = $result->field($i);
					$value = $row[$name];
					$s .= $value;
					$s .= (($i==$num)?'':'|');
				}
			}
			$result->free();
		}
		if($s!='') $s = substr($s,0,strlen($s)-1) . '';
		if($s=='') $s = 'No records available';
		$s = "{'name':'".$field_name."','type':1,'int':".(($num)?$num:0).",'".$page_type."':'".$this->sanitize($s,1)."'}";
		$this->fields[] = $s;
	}
	
	function get_user_dashboard_form()
	{
	// *
		
		
		//"select f1.field_name,f1.field_title,f1.field_type,f1.field_length,f1.field_value,f1.field_columns,f1.field_mandatory,f1.field_options from form_fields f1 ".$where_tables."  where f1.page_type = '".$page."' and (f1.user_group_mask & ".$_SESSION['user_details']['user_group_mask'].")>0 ".$where_parent);
		
		
		
		//*/
		$this->set_fields();
		$this->set_fields('cats','item');
		$this->set_fields('admin','item');
		$this->set_fields('pcr','item');
		
		for($i=0;$i<count($this->fields);$i++){
			//$s.='<div>'.$this->fields[$i].'</div>';
			$s.=''.$this->fields[$i].',';
		}
		$s=substr($s,0,strlen($s)-1);
		
		$s2='
<div class="bar" style="background-color: transparent; position:absolute; float:right;">
<a
	title="My History"
	href="javascript:_history();" 
	style="background-image: url(icons/btn_history_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>
<a
	title="My Favorites"
	href="javascript:_favorites();" 
	style="background-image: url(icons/btn_favorites_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>
<a
	title="My Employee Details"
	href="javascript:_account();" 
	style="background-image: url(icons/btn_my_employee_details_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>	
</div>
<div id="top_logo" style="display:none">
<a href="javascript:_iframe_redirect(\\\'dashboard.php\\\')" style="background-image:url(images/cats.jpg);width:205px;height:15px"></a>
<a href="javascript:showMenu(\\\'main\\\', \\\'CATS - Main Menu\\\')" style="background-image:url(icons/btn_cats_bg.gif);">CATS</a>
<a href="javascript:showMenu(\\\'admin\', \\\'CATS - Administration\\\')" style="background-image:url(icons/btn_admin_bg.gif);">ADMIN</a>
<a href="javascript:showMenu(\\\'pcr_admin\\\', \\\'PCR - Administration\\\')" style="background-image:url(icons/btn_pcr_bg.gif);">PCR</a>
<a name="title" id="page_title"></a>
</div>

<div id="toolbar" style="display:none">
<fieldset class="bar" id="tab_buttons">
<div id="tab_toolbar" style="float:left;">
 
</div>
<a
	title="spacer"
	href="javascript:{};" 
	style="background-image: url(icons/spacer.gif);float:right;width:0px; height:20px;padding: 0px"></a>
</fieldset>
</div>
<div id="menu_cont" style="display:none">
	<table align="center" width="70%" class="title"><tr><td id="menu_title"></td><td align="right"><img src="images/btn_close.gif" onmousedown="this.src=\'images/btn_close_down.gif\'" onmouseout="this.src=\'images/btn_close.gif\'" onclick="doMenu();" /></td></tr></table>
	<table align="center" width="70%" class="title"><tr><td><div id="menu"></div></td></tr></table>
	
</div>
<iframe src="" id="menu_frame" name="menu_frame"></iframe>
<div id="menu_main" style="display:none">
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	<a href="javascript:doMenu(\\\'actions.php\\\');" style="background-image: url(icons/icon_48_tools.gif)">Actions</a>
	<a href="javascript:doMenu(\\\'government_inspections.php\\\');" style="background-image: url(icons/icon_48_users.gif)">Government Inspections</a>
	<a href="javascript:doMenu(\\\'major_hazards_register.php\\\');" style="background-image: url(icons/btn_access_bg.gif)">Major Hazards Register</a>
	<a href="javascript:doMenu(\\\'other_records.php\\\');" style="background-image: url(icons/btn_file-manager_bg.gif)">Other Records</a>
	<a href="javascript:doMenu(\\\'site_specific_obligations.php\\\');" style="background-image: url(icons/icon_48_globe.gif)">Site Specific Obligations</a>
	</td>
	<td id="splashLinks" width="50%">
	<a href="javascript:doMenu(\\\'audits_and_inspections.php\\\');" style="background-image: url(icons/btn_audit_bg.gif)">Audits & Inspections</a>
	<a href="javascript:doMenu(\\\'incidents.php\\\');" style="background-image: url(icons/icon_48_catalog.gif)">Incidents</a>
	<a href="javascript:doMenu(\\\'meeting_minutes.php\\\');" style="background-image: url(icons/btn_tickets_bg.gif)">Meeting Minutes</a>
	<a href="javascript:doMenu(\\\'schedules.php\\\');" style="background-image: url(icons/schedule.gif)">Schedules</a>
	<a href="javascript:doMenu(\\\'workload_review.php\\\');" style="background-image: url(icons/icon_48_reports.gif)">Workload Review</a>
	</td>
</tr>
</table>
</div>
<div id="menu_admin" style="display:none">
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	<a href="javascript:doMenu(\\\'organisation_structure.php\\\');" style="background-image: url(icons/btn_domain-user_bg.gif)">Organisation Structure</a>
	<a href="javascript:doMenu(\\\'employees.php\\\');" style="background-image: url(icons/btn_mail-groups_bg.gif)">Employees</a>
	<a href="javascript:doMenu(\\\'references.php\\\');" style="background-image: url(icons/btn_session-setup_bg.gif)">Reference Table</a>
	<a href="javascript:doMenu(\\\'work_hours.php\\\');" style="background-image: url(icons/btn_work-hours_bg.gif)">Work Hours</a>
	<a href="javascript:doMenu(\\\'lost_days.php\\\');" style="background-image: url(icons/btn_lost-days_bg.gif)">Lost Days</a>
	</td>
	<td id="splashLinks" width="50%">
	<a href="javascript:doMenu(\\\'allocate_response.php\\\');" style="background-image: url(icons/btn_re-allocate_bg.gif)">Re-Allocate Responsibility</a>
	<a href="javascript:doMenu(\\\'incident_checkboxes.php\\\');" style="background-image: url(icons/btn_ticket-checked_bg.gif)">Incident Form Checkboxes</a>
	<a href="javascript:doMenu(\\\'risk_definitions.php\\\');" style="background-image: url(icons/btn_permissions_bg.gif)">Risk Definitions</a>
	<a href="javascript:doMenu(\\\'motd.php\\\');" style="background-image: url(icons/btn_tickets_bg.gif)">Message of the day</a>
	<a href="javascript:doMenu(\\\'email_configuration.php\\\');" style="background-image: url(icons/btn_tts-mail-gate_bg.gif)">Email Configuration</a>
	</td>
</tr>
</table>
</div>
<div id="menu_pcr_admin" style="display:none">
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	<a href="javascript:doMenu(\\\'pcr_search.php\\\');" style="background-image: url(icons/pcr.gif)">PCR Search</a>
	<a href="javascript:doMenu(\\\'pcr_maintain_reviewers.php\\\');" style="background-image: url(icons/btn_web-users_bg.gif)">Maintain Reviewers</a>
	<!--<a href="javascript:doMenu(\\\'pcr_maintain_actions.php\\\');" style="background-image: url(icons/icon_48_tools.gif)">Maintain Actions</a>
	<a href="javascript:doMenu(\\\'pcr_maintain_evaluation.php\\\');" style="background-image: url(icons/icon_48_reports.gif)">Maintain Evaluation</a>-->
	</td>
	<td id="splashLinks" width="50%">
	<a href="javascript:doMenu(\\\'pcr_maintain_actions.php\\\');" style="background-image: url(icons/icon_48_tools.gif)">Maintain Actions</a>
	<a href="javascript:doMenu(\\\'pcr_maintain_email.php\\\');" style="background-image: url(icons/btn_mail-resp-files_bg.gif)">Email Messages</a>
	<!--<a href="javascript:doMenu(\\\'pcr_equipment_list.php\\\');" style="background-image: url(icons/icon_48_tools.gif)">Maintain Equipment</a>
	<a href="javascript:doMenu(\\\'pcr_checkboxes.php\\\');" style="background-image: url(icons/icon_48_downloads.gif)">Checkboxes</a>-->
	</td>
</tr>
</table>
</div>
<div id="menu_history" style="display:none;">
<div id="menu_history_list" class="bar" style="height:200px;overflow: auto;"></div>
</div>
<div id="menu_search" style="display:none"></div>
<iframe src="dashboard.php?show_welcome_message=yes" id="main_frame" name="main_frame" style="display:none"></iframe>
						';
		$s=preg_replace("/\r\n/","",$s);
		$s="{'message':'Welcome to you dashboard','data':[".$s."],'success':true}";
		return $s;
	}
	
	function sanitize($s,$dir)
	{
		if($dir==0){ // Going into db
			return preg_replace("/'/","''",$s);
		}else{ // Coming out of db
			return preg_replace("/'/","\\'",$s);
		}
	}
}
//$users=new User;
?>