<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Category class
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

class Category
{
    var $config;            // File that contains database settings

    function Category()
    {
    }

    // Returns the page content
    function getField($id, $field='title')
    {
				ezw_db_connect();
        $result = ezw_db_query("SELECT `$field` FROM `page` WHERE `id`='$id'") or die(mysql_error());
        if($data = ezw_db_fetch_array($result))
        {
            $value = stripslashes($data[$field]);
        }
        else
        {
            $value = "Database Error in Category::getField";
        }
        ezw_db_close();
        return $value;
    }
    
    // Returns an integer representing the depth from level 0
    function getLevel($id)
    {
				ezw_db_connect();
        $level = 0;
        $currlevel = $level;
        while($id != 0)
        {
            $result = ezw_db_query("SELECT * FROM `page` WHERE `id`='$id'") or die(mysql_error());
            $data = ezw_db_fetch_array($result);
            $id = $data['pid'];
            $currlevel = $level;
            $level++;
        }
				ezw_db_close();
        return $currlevel;
    }

    function lookup($category)
    {
        ezw_db_connect();
        $result = ezw_db_query("SELECT `id` FROM `page` WHERE `title`='$category'") or die(mysql_error());
        if($data = ezw_db_fetch_array($result))
        {
            $value = stripslashes($data['id']);
        }
        else
        {
            $value = "Database Error in Category::lookup";
        }
        ezw_db_close();
        return $value;
    }

    function lookupByName($name)
    {
        ezw_db_connect();
        $result = ezw_db_query("SELECT `id` FROM `page` WHERE `title`='$name'") or die(mysql_error());
        if($data = ezw_db_fetch_array($result))
        {
            $value = stripslashes($data['id']);
        }
        else
        {
            $value = -1;
        }
        ezw_db_close($dbPtr);
        return $value;
    }

    function getPage($id)
    {
        ezw_db_connect();
        $result = ezw_db_query("SELECT `page` FROM `page` WHERE `id`='$id'") or die(mysql_error());
        if($data = ezw_db_fetch_array($result))
        {
            $value = stripslashes($data['page']);
        }
        else
        {
            $value = "Database Error in Category::getPage";
        }
        ezw_db_close();
        return $value;
    }

    function getFields($id)
    {
        ezw_db_connect();
        $result = ezw_db_query("SELECT * FROM `page` WHERE `id`='$id'") or die(mysql_error());
        if( !($value = mysql_fetch_array($result)) )
        {
            $value = "Database Error in Category::getFields";
        }
        ezw_db_close();
        return $value;
    }

    function getAll()
    {
        ezw_db_connect();
        $result = ezw_db_query("SELECT * FROM `page`") or die(mysql_error());
        ezw_db_close($dbPtr);
        return $result;
    }

    function getChildren($pid)
    {
        ezw_db_connect();
        $result = ezw_db_query("SELECT * FROM `page` WHERE `pid`='$pid'") or die(mysql_error());
        ezw_db_close();
        return $result;
    }
}
?>
