<?php
	/**
	 * Class Makes it easier to send a text email.
	 *
	 * Note: If mail is not being sent, and you are not getting any
	 * error messages then email may not be set up properly in PHP.
	 * You can check your server's php.ini file or ask
	 * your server administar for help.
	 *	
	 * @author Andrew Blair
	 * @version 1.3	07-23-03
	 */
	class Mail {
		/**
		 * @var str headers Holds the headers
		 * @access private
		 */
		var $headers; 		//string
		var $from;			//name:value paired array, 
		var $to; 			//array of name:email paired array
		var $cc; 			//array of name:email paired array
		var $bcc; 			//array of name:email paired array
		var $subject; 		//string
		var $textMessage; 	//string
		var $kickback; 		//optional string
		var $br = "\n";		//line break		
		
		/**
		 * Sets the sender part of the email. There should only be one sender.
		 *
		 * @param str name The sender's name. i.e. <code>"Andrew Blair"</code>
		 * @param str email The sender's email address. Do not enclose in <code><></code> tags.
		 */
		function setFrom($name,$email){
			$this->from[] = array("name"=>$name,"email"=>$email);
		}
		
		/**
		 * Adds a main (to) recipient. You can run this method as many times as you
		 * like to add more recipients. Don't try to use this method once
		 * for multiple recipients though.
		 *
		 * @param str name The recipient's name. i.e. <code>"Andrew Blair"</code>
		 * @param str email The recipient's email address. Do not enclose in <code><></code> tags.
		 */
		function addRecipient($name,$email){
			$add=array("name"=>$name,"email"=>$email);
			$a=is_array($this->to)?$this->to:array();
			if(!array_search($add,$a)){
				$this->to[] = $add;
			}
		}
		
		/**
		 * Empties out recipient list. Good for looping through a list of contacts
		 * to send an email that is otherwise the same. In other words, if you are
		 * looping through, you can continue to use the same MailMan object.
		 */
		function clearRecipients(){
			$this->to = NULL;
			$this->cc = NULL;
			$this->bcc = NULL;
		}
		
		/**
		 * Adds a carbon copy (cc) recipient. You can run this method as many times as you
		 * like to add more cc recipients. Don't try to use this method once
		 * for multiple recipients though.
		 *
		 * @param str name The cc recipient's name. i.e. <code>"Andrew Blair"</code>
		 * @param str email The cc recipient's email address. Do not enclose in <code><></code> tags.
		 */
		function addCCRecipient($name,$email){
			$add=array("name"=>$name,"email"=>$email);
			$a=is_array($this->cc)?$this->cc:array();
			if(!array_search($add,$a)){
				$this->cc[] = $add;
			}
		}
		
		/**
		 * Adds a blind carbon copy (bcc) recipient. You can run this method as many times as you
		 * like to add more cc recipients. Don't try to use this method once
		 * for multiple recipients though.
		 *
		 * @param str name The bcc recipient's name. i.e. <code>"Andrew Blair"</code>
		 * @param str email The bcc recipient's email address. Do not enclose in <code><></code> tags.
		 */
		function addBCCRecipient($name,$email){
			$add=array("name"=>$name,"email"=>$email);
			$a=is_array($this->bcc)?$this->bcc:array();
			if(!array_search($add,$a)){
				$this->bcc[] = $add;
			}
		}
		
		/**
		 * Sets the subject for the email.
		 *
		 * @param str subject
		 */
		function setSubject($subject){
			$this->subject = $subject;
		}
		
		/**
		 * Sets the return value for when the <code>sendMessage</code> method is done.
		 * Not so useful for PHP scripts alone, but if using in conjunction with Flash,
		 * there are cases where having a return value like 'sent=1' can come in handy.
		 *
		 * @param str message the text to return
		 * @see sendMessage
		 */
		function setKickback($message){
			$this->kickback = $message; //for use with things like Flash. i.e. "sent=1"
		}
		
		/**
		 * Adds the text for the body of the message.
		 * You can use a template if you want by first retrieving the contents using:<br>
		   <code>$txtMsg = file_get_contents("myTextTemplate.txt");<br>
		   $txtMsg = str_replace("{favourite food}","Thai",$txtMsg);<br>
		   $myMailManObj->setTextMessage($txtMsg);
		   </code>
		 *
		 * @param str txtMsg The text of the message
		 */
		function setTextMessage($txtMsg){ 
			$this->textMessage = $txtMsg;
		}
		
		/**
		 * Combines General and Content headers. Separated for inheritance reasons.
		 *
		 * @access private
		 */
		function _makeHeaders(){
			$this->_makeGeneralHeaders();
			$this->_makeContentHeaders();

			//convert headers to string for source email
			$this->_headers = implode($this->br,$this->_headers) . $this->br;
		}
		
		/**
		 * Adds the standard headers common to both text and HTML emails.
		 *
		 * @access private
		 */
		function _makeGeneralHeaders(){
			$userAgent = "EZW Mail";
		
			//Additional headers in an array
			$this->_headers = array();
			$this->_headers[] = "Return-Path: " . $this->_createRecipientString($this->from);
			$this->_headers[] = "Reply-To: " . $this->_createRecipientString($this->from);
			$this->_headers[] = "User-Agent: $userAgent";
										  //Date: Tue, 24 Jun 2003 12:06:48 -0400
			$this->_headers[] = "Date: " . date("r");			
			$this->_headers[] = "From: " . $this->_createRecipientString($this->from);
			
			/* 
				Note: The (to) field is not an additional header,
				it is a primary argument of the mail() function, used later.
			*/
						
			//If cc and bcc recipients exist, add them to the headers
			if (isset($this->cc)){
				$this->_headers[] = "CC: " . $this->_createRecipientString($this->cc);
			}
			if (isset($this->bcc)){
				$this->_headers[] = "BCC: " . $this->_createRecipientString($this->bcc);
			}
			
			$this->_headers[] = "X-Sender: " . $this->_createRecipientString($this->from);
			$this->_headers[] = "X-Mailer: $userAgent";
			$this->_headers[] = "MIME-Version: 1.0";
		}
		
		/**
		 * Add the headers that are specific to a plain text email
		 *
		 * @access private
		 */
		function _makeContentHeaders(){
			$this->_headers[] = "Content-type: text/plain; charset=\"US-ASCII\"";
			$this->_headers[] = "Content-transfer-encoding: 7bit" . $this->br;			
		}
		
		/**
		 * Sends the message.
		 *
		 * @return var If a value has been set with <code>setKickback</code>,
		 * whatever the kickback message was will be returned.
		 * Otherwise, <code>true</code> will be returned.
		 * @see setKickback
		 */
		function send(){
			if (count($this->from) < 1 || count($this->to) < 1 || !isset($this->subject)){
				die("You must provide at least valid sender,receipient and subject");
			}
			if(SITE_IS_LIVE != 'true')
				ezw_dbg($this->showEmail());
			
			$this->_makeHeaders();
						
			$recipients = $this->_createRecipientString($this->to);
			
			mail($recipients,$this->subject,$this->textMessage,$this->_headers);
			
			if (isset($this->kickback)){
				return $this->kickback;
			}else{
				return true;
			}
		}
		
		/**
		 * Combines all recipients so they're on one line in proper format.
		 *
		 * @access private
		 * @param 2D_array recipients Containing array(str name, str email)
		 * @return str
		 */
		function _createRecipientString($recipients){
			foreach($recipients as $r){
				$x[] = $r["name"] . " <" . $r["email"] . ">";
			}
			return implode(",",$x);
		}
		
		/**
		 * DEBUG FUNCTION that shows email source as plain text.
		 * You should either set the content header to text/plain when using this
		 * or simply view source, because some parts may not be visible since
		 * the browser can think that the email <code><></code> tags are unknown HTML tags.
		 * Note: Mail servers will add their own headers on top of yours and potentially
		 * overwrite some of them.
		 *
		 * @return str
		 */		
		function showEmail(){
			$email = "Subject: " . $this->subject . $this->br;
			$email .= "To: " . $this->_createRecipientString($this->to) . $this->br;
			$this->_makeHeaders();
			$email .= $this->_headers;
			$email .= $this->textMessage;
			return $email;
		}
		
		function emsgSend($type,$to,$email,$values){
			require_once(DIR_WS_CLASSES."template.php");
			
			ezw_db_connect();
			$result = ezw_db_query("select * from " . TABLE_EMAIL_MESSAGES . " where emsg_type = '$type' and emsg_to = '$to' ") or die(ezw_db_error());
			if( !($msg = ezw_db_fetch_array($result)) )
			{
				$err_msg = "Database Error in Mail::emsgSend";
			}
			else
			{
				foreach($msg as $akey => $v)
				{
					$msg[$akey] = stripslashes($msg[$akey]);
				}
			}
			$tpl = new Template($msg['emsg_message'],DIR_WS_PUBLIC,TEMPLATE_DEFAULT_GROUP,true);
			$tpl->parse($values);
			$msg['emsg_message'] = $tpl->get();
			$this->clearRecipients();
			$this->setFrom(SITE_NAME, ADMIN_EMAIL);
			$this->addRecipient($email['firstname'].' '.$email['lastname'], $email['email']);
			$this->setSubject($msg['emsg_subject']);
			$this->setTextMessage($msg['emsg_message']);
			
			// send the message
			return $this->send();
			
		}
	}
?>