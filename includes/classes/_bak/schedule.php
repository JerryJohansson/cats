<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Schedule Definition
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

class Schedule extends Container
{
	var $property_names;
	function Schedule($id=0)
	{
		parent::Container($id,'schedule');
		$this->setPagetype("schedule");
		//ezw_dbg('<hr />');
		//ezw_dbg($this->properties);
		
	}
	
/*
+---------------------------------------------------------------------------
|	Add/Edit Administration functions
+---------------------------------------------------------------------------
*/
	function setProperties($POST=''){
		$properties=$this->getAllPropertyNames();
		$funcs=$this->getAllPropertySetFunctions();
		parent::setProperties($POST, $properties, $funcs);
	}
	
	function getAllPropertyNames(){
		return array_merge($this->getPropertyNames(),parent::getPropertyNames());
	}
	function getPropertyNames(){
		return array('cal_id','fid','cal_type','cal_start_date','cal_end_date','cal_location','cal_address','cal_city','cal_state','cal_post_code','cal_url','cal_comment');
	}
	
	function getAllPropertySetFunctions(){
		return array_merge($this->getPropertySetFunctions(),parent::getPropertySetFunctions());
	}
	function getPropertyDisplayFunctions($admin = true){
		if($admin)//getCalendarStartDateTimeField
			return array('','getId','getCalendarTypesCBO','','getCalendarStartEndDateTimeField','','','','','','getCalendarUrlCBO','');
		else
			return array('','getId','getCalendarDisplayType','','','getDisplayLocation','getDisplayAddress','getDisplayCity','getDisplayState','getDisplayPostCode','getDisplayUrl','getDisplayComment');
	}
	function getPropertySetFunctions(){
		return array('','','','','','','','','','','');
	}
	
	
	
	function postAdd()
	{
		$_POST=array_merge($_POST, array('id' => parent::postAdd()));
		
		$dbfields=explode("|","fid|".$_POST['id']."|cal_type||cal_start_date||cal_end_date||cal_location||cal_address||cal_city||cal_state||cal_post_code||cal_url||cal_comment");
		$pgfields=explode("|","id|value|cal_type|value|cal_start_date|value|cal_end_date|value|cal_location|value|cal_address|value|cal_city|value|cal_state|value|cal_post_code|value|cal_url|value|cal_comment|value");
		$sql_data = ezw_db_prepare_array($dbfields,$pgfields,$_POST);
		ezw_db_perform(TABLE_EVENTS, $sql_data);
		$_POST=array_merge($_POST, array('cal_id' => ezw_db_insert_id()));
		$this->setProperties();
		return $_POST['id'];//$insert_id;
	}
	//ezw_dbg($this->getId());
	function postEdit()
	{
		$this->setProperties();
		//$properties=serialize($this->properties);
		//ezw_dbg("schedule::postEdit::properties=");
		//ezw_dbg(parent::getProperties());
		//ezw_dbg("schedule::POST=");
		//ezw_dbg($POST);
		$dbfields=explode("|","fid|".$_POST['id']."|cal_type||cal_start_date||cal_end_date||cal_location||cal_address||cal_city||cal_state||cal_post_code||cal_url||cal_comment");
		$pgfields=explode("|","id|value|cal_type|value|cal_start_date|datetime|cal_end_date|datetime|cal_location|value|cal_address|value|cal_city|value|cal_state|value|cal_post_code|value|cal_url|value|cal_comment|value");
		$sql_data = ezw_db_prepare_array($dbfields,$pgfields,$_POST);
		ezw_db_perform(TABLE_EVENTS, $sql_data, "update", "id='".$_POST['cal_id']."'");

		parent::postEdit();
		//$this->update();
		//exit;
	}
	
/*
+---------------------------------------------------------------------------
|   Editor Extra Field Display
+---------------------------------------------------------------------------
*/
	function getExtraAdd($template='schedule_extras')
	{
		return parent::getExtraAdd($template,$this->getPropertyNames(),$this->getPropertyDisplayFunctions(true));
	}

	function getExtraEdit($template='schedule_extras'){
		return parent::getExtraEdit($template,$this->getPropertyNames(),$this->getPropertyDisplayFunctions(true));
	}

	function getAddItem($template='additem_schedule')
	{
		return parent::getAddItem($template,$this->getPropertyNames(),$this->getPropertyDisplayFunctions(false));
	}
/*--------------------------------------------------------------------------*/
	
/*
+---------------------------------------------------------------------------
|   Display Functions
+---------------------------------------------------------------------------
*/
	function getDisplayRow($heading,$val,$tag='td',$table_row=1){
		$s='<'.$tag.' class="course_features_hd"><b>'.$heading.':</b></'.$tag.'><'.$tag.' class="course_features">'.$val.'</'.$tag.'>';
		if($table_row==1) $s='<tr align="top">'.$s.'</tr>';
		if($table_row==2) $s='<tr align="top"><td>'.$s.'</td></tr>';
		return $s;
	}
	function getDisplayLocation($val){
		return $this->getDisplayRow("Location",$val);
	}
	function getDisplayAddress($val){
		return $this->getDisplayRow("Address",$val);
	}
	function getDisplayCity($val){
		return $this->getDisplayRow("City",$val);
	}
	function getDisplayState($val){
		return $this->getDisplayRow("State",$val);
	}
	function getDisplayPostCode($val){
		return $this->getDisplayRow("Post Code",$val);
	}
	function getDisplayUrl($val){
		$val=$this->getCalendarUrlHyperlinks($val,false);
		return $this->getDisplayRow("Related Document",$val,'div',0);
	}
	function getDisplayComment($val){
		return $this->getDisplayRow("Description",$val);
	}
	
	
	function getCalendarDisplayType($type=''){
		if(empty($type)) $type = $this->properties['cal_type'];
		//ezw_dbg($this->properties);
		$s="";
		$sreturn='
			<div id="id_cal_type_'.$type.'">';
		ezw_db_connect();
		$result = ezw_db_query("select * from calendar_types where id='$type' and status=0") or die(ezw_db_error());
		while ($row = ezw_db_fetch_array($result))
		{
			$s.='
				<h1><img src="'.DIR_WS_MODULES.'schedule/images/thumbs/'.$row['thumb'].'" title="'.$row['value'].'"  align="absmiddle" />'.$this->getTitle().'</h1>';
		}
		ezw_db_close();
		if($s!=''){
			$sreturn.=$s.'
			</div>';
		}else	$sreturn='';
		return $sreturn;
	}
	function getCalendarTypesCBO($type=1){
		$s="";
		$sreturn='
			<select class="sfield" name="cal_type" id="id_cal_type">';
		ezw_db_connect();
		$result = ezw_db_query("select * from calendar_types where status=0") or die(ezw_db_error());
		while ($row = ezw_db_fetch_array($result))
		{
			$s.='
				<option value="'.$row['id'].'" icon="'.$row['icon'].'" '.(($row['id']==$type)?"selected":"").'>'.$row['name'].'</option>';
		}
		ezw_db_close();
		if($s!=''){
			$sreturn.=$s.'
			</select>';
		}else	$sreturn='';
		return $sreturn;
	}
	
	function getDelimUrl(){
		return '|';
	}
	
	function getCalendarStartEndDateTimeField($value){
//ezw_get_input_start_end_datetimes($start_date, $start_date_value, $end_date, $end_date_value, $alt_start_time = '', $alt_end_time = '', $start_label = 'Start Date', $end_label = 'End Date', $show_start_label = false){
		$props = $_POST['properties'];
		return ezw_get_input_start_end_datetimes('cal_start_date', $props['cal_start_date'], 'cal_end_date', $value,  "08:30", "18:45", 'Begins on', 'Ends on', true);
	}
	
	function getCalendarStartDateTimeField($value){
		return ezw_get_calendar_date_time_field('cal_start_date', $value, "08:30");
	}
	
	function getCalendarEndDateTimeField($value){
		return ezw_get_calendar_date_time_field('cal_end_date', $value, "16:30");
	}
	
	function getCalendarUrlHyperlinks($url_string, $admin = false){
		$related_field = "cal_url";
		$s='
			<div id="add_'.$related_field.'" title="'.$url_string.'">';
		if(!empty($url_string)){
			ezw_db_connect();
			$urls=explode($this->getDelimUrl(), $url_string);
			$urls_array=array();
			$len=count($urls);
			
			for($i=0;$i<$len;$i++){
				if(!in_array($urls[$i], $urls_array)) $urls_array[]=$urls[$i];
			}
			$ids_in_string=implode(",",$urls_array);
			$result = ezw_db_query("select n.id, n.name, n.url, t.name as type from node n, node_type t where n.node_type=t.id and n.id in($ids_in_string)") or die(ezw_db_error());
			while ($row = ezw_db_fetch_array($result))
			{
				$url=(empty($row['url']))?$row['type'].'.php?id='.$row['id']:$row['url'];
				$value=$row['name'];
				if($admin){
					//<!--input type="checkbox" name="hyperlink[]" value="'.$row['id'].'" /-->&nbsp;
					$s .= '<a href="'.DIR_WS_PUBLIC.$url.'" target="url_preview">'.$value.'</a><br />';
				}else{
					$s .= '<a href="'.DIR_WS_PUBLIC.$url.'">'.$value.'</a><br />';
				}
			}
			ezw_db_close();
		}
		$s .= '
			</div>';
		if($admin){
			$s .= '
			<input class="sfield" type="hidden" name="'.$related_field.'" id="id_'.$related_field.'" value="'.$url_string.'"><br />
			<input type="button" value="Add/Remove Hyperlink" title="Click here if you wish to add/remove hyperlinks to/from the list" onclick="return addHyperlinks(this.form.'.$related_field.', \'add_'.$related_field.'\')" />';
		}
		return $s;
	}
	function getCalendarUrlCBO($url_string=""){
		return $this->getCalendarUrlHyperlinks($url_string, true);
	}
	
	function buildThumb()
	{
		if(defined('SCHEDULE_THUMB_GEN') && SCHEDULE_THUMB_GEN == true)
		{
			// Do a quick check to see if a thumbnail is set and exists
			if($this->getThumb() != '' && file_exists(DIR_FS_THUMBS.$this->getThumb()))
				$thumb = '<img src="'.DIR_WS_THUMBS.$this->getThumb().'" border="0">';
			else
				$thumb = '';
		}
		else
		{
			$thumb = '';
		}
		return $thumb;
	}

	function getEventsList($page,$count=10){
		return $this->getPage($page,$count,'events_list');
	}
	
	// Returns a mysql result resource from $type, limited to $offset,$limit an ordered by $order
	function getLimited($offset, $limit, $type, $order, $filter = '')
	{
		ezw_db_connect();
		$result = ezw_db_query($this->getLimitedSQL($type, $order, $filter)." LIMIT $offset,$limit ") or die(ezw_db_error());
		$idArray = array();
		while ($row = ezw_db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
		ezw_db_close();
		return $idArray;
	}
	function getLimitedSQL($type, $order, $filter='')
	{
		if(!empty($filter)) $filter = " and ".$filter;
		return "select n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r, ".TABLE_EVENTS." c where c.fid=n.id and n.id=r.node_id and p.id=r.page_id and p.type='$type' and n.pid!=0 $filter order by $order ";
	}
	
	function getPage($pagenum,$count_per_page=0,$template='schedule')
	{
		require_once(DIR_WS_CLASSES."template.php");
		$article = new Schedule;
		$tpl = new Template("templates/$template.htm");
		$tpl->parse_loop("schedulesummary");
		
		// const to vars
		if($count_per_page==0)
			$count_per_page = (defined('SCHEDULE_COUNT_PER_PAGE')) ? (int)SCHEDULE_COUNT_PER_PAGE : (int)DEFAULT_COUNT_PER_PAGE ;
		$summary_length = (defined('SCHEDULE_SUMMARY_LENGTH')) ? (int)SCHEDULE_SUMMARY_LENGTH : (int)DEFAULT_SUMMARY_LENGTH ;

		$rows = '';
		$offset = $pagenum * $count_per_page;
		$articles = $this->getLimited($offset, $count_per_page, "schedule", "p.date DESC","c.cal_start_date < now()");
		foreach($articles as $articleId)
		{
			$article->load($articleId);
			$url=DIR_WS_PUBLIC.get_class($this).'.php?id='.$article->getId();
			$summary = substr( stripslashes( strip_tags( $article->getContent() ) ), 0, $summary_length );
			$summary .= '... <a href="'.$url.'" class="read_more">read more...</a>';
			$values = array(
				"type"			=> $this->getCalendarDisplayType(),
				"title"     => $article->getTitle(),
				"link"			=> $url,
				"date"      => date("m.d.y", $article->getDate()),//date('jS F Y', $article->getDate()),
				"summary"   => $summary
			);

			$rows .= $tpl->get_loop('schedulesummary', $values);
		}

		$values = array( "loop" => $rows );
		$tpl->parse($values);
		return $tpl->template;
	}

	function getArticle($template='schedule_article')
	{
		require_once(DIR_WS_CLASSES."template.php");
		$tpl = new Template("templates/$template.htm");
		$values=array();
		if(is_array($this->properties)){
			$array_update = $this->getPropertyNames(false);
			$array_funcs = $this->getPropertyDisplayFunctions(false);
			$len=count($array_update);
			for($i=0;$i<$len;$i++) {
				if($array_funcs[$i]!="")
					$values=array_merge($values,array($array_update[$i] => $this->$array_funcs[$i]($this->properties[$array_update[$i]])));
				else
					$values=array_merge($values,array($array_update[$i] => $this->properties[$array_update[$i]]));
			}
		}
		$values = array_merge($values,array(
			"title"     => $this->getTitle(),
			"content"   => $this->getContent(),
			"file"      => $this->getFileLink()/*,
			"calendar"	=> $this->getAddItem()*/
		));
		$tpl->parse($values);
		$ret .= $tpl->template;
		return $ret;
	}
	
	function get($pagenum)
	{
		if($this->hasChildren() == true)
		{
			if(defined('SCHEDULE_RESULT_TYPE') && SCHEDULE_RESULT_TYPE == "thumbs")
				$ret = $this->getCategoryThumbs($pagenum);
			else
				$ret = $this->getPage($pagenum); //$this->getCategoryList($pagenum);
		}
		else
		{
			$ret = $this->getPage($pagenum);
		}

		return $ret;
	}

}
?>