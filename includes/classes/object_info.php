<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Object Info class
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
  class objectInfo {

// class constructor
    function objectInfo($object_array) {
      reset($object_array);
      while (list($key, $value) = each($object_array)) {
        $this->$key = ezw_db_prepare_input($value);
      }
    }
  }
?>
