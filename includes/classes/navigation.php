<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Menu Generation
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
class Navigation
{
	var $error;
	var $db;
	var $type;
	var $menu_js;
	var $menu_html;
	var $mode;
	var $module = 'form';
	var $tbl;
	var $tbl_types;
	var $tblcontent;
	var $sql_func;
	
	function Navigation($type='', $mode='', $sql_func='getSQL', $types_table='form_types', $content_table='form_help')
	{
		global $db;
		$this->db=$db;
	//echo('type='.$type.';=mode'.$mode.';sql_func='.$sql_func.';<br/>');
		$this->type = ($type=='')?DEFAULT_MENU_SYSTEM:$type;
		$this->mode = $mode;
		$this->sql_func = $sql_func;
		$this->tbl = 'form_fields';
		$this->tbl_types = $types_table;
		$this->tblcontent = $content_table;
	}
	
	function getFormSQL($id)
	{
		return $this->getSQL($id,"field_id");
	}
	function getSQL($id=0, $identifier_name = 'field_pid', $order='f.sort_order ASC', $fields = 'f.*,t.name,t.description,t.display_function as display_func, h.help_tip, h.help_user_guide')
	{
		$table = $this->tbl;
		$filter = '';
//		if($this->mode=="admin")
//		{
			//$filter = " and ((f.role_mask+0) = 0) or ( (bitand(f.role_mask, " . ($_SESSION['user_details']['roles']+0) . ")+0) > 0 ) ";
//		}
//		else
//		{
			$filter = " and (bitand(f.status, ".MASK_PUBLISHED.")+0) > 0 ";
			$filter .= "  and (( ((f.group_mask+0) = 0) ";

			if(is_array($_SESSION['user_details']))
			{
				$filter .= " or (bitand(f.group_mask , ".($_SESSION['user_details']['groups']+0)."+0) > 0) " ;
			}
			$filter .= ") )";
//		}
		//$filter=" and f.field_id != 4 ";

		$table_types = $this->tbl_types;
		$ret = "SELECT $fields FROM $table f, $table_types t, help_fields h WHERE f.field_type_id=t.id and f.$identifier_name=$id and f.field_id = h.field_id $filter  ORDER BY $order";
		//echo "<!--".$this->mode."-->";
		//echo "<!--".$sql."-->";
		
		return $ret;
		/*ADMIN SQL
SELECT 
	f.*,
	t.name,
	t.description,
	t.display_function as display_func, 
	h.help_tip, 
	h.help_user_guide 
FROM 
	form_fields f, 
	form_types t, 
	help_fields h 
WHERE 
	f.field_type_id=t.id 
	and f.field_pid=0 
	and f.field_id(+) = h.field_id  
	and (bitand(f.status, 128)+0) > 0  
	and (( ((f.group_mask+0) = 0)  
	or (bitand(f.group_mask , 9977086+0) > 0) ) )  
ORDER BY 
	f.sort_order ASC

		SELECT 
			f.*,
			t.name,
			t.description,
			t.display_function as display_func 
		FROM 
			form_fields f, 
			form_types t 
		WHERE 
			f.field_type_id=t.id and 
			f.field_pid=0  and 
			(
				(f.role_mask+0) = 0
			) or (
				(bitand(f.role_mask, 254)+0) > 0 
			)   
		ORDER BY 
			f.sort_order ASC
		
		select cast(bitand(role_mask,254) as varchar2(20))||'-'||role_mask as bits from form_fields;
		update form_fields set role_mask = 0, group_mask = 0
		
		*/
	}
	
	function getItems($id=0){
		global $db;
		$sql=$this->getSQL($id);
		//echo($sql);
		if($rs = $db->Execute($sql))
			return $rs->GetAll();
		else
			return false;
	}
	
	function drawItems($id=0,$title='Administration'){
		global $db, $IFRAMES;
		$sql=$this->getSQL($id);
		$img_path = WS_ICONS_PATH;
		$ret="";
		$top="";
		$col1="";
		$col2="";
		$i=0;
		if($id>0){
// Changes for Work Item #12887
//			if($id==3 && strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false){
//				$title = "CATS - Change Management Request";
//			}else{
				$title = db_get_field_value("select field_label from form_fields where field_id = $id");
//			}
		}
	
//		print_r($menu_items);
		$js_func = ($id==0)?"showMenu":"top.show_screen";
		$menu_func="top.show_screen";
		if($rs = $db->Execute($sql)){
			if($id!=0){
				$ret .= '<div>
<div class="dialog_title_bg">
	<div class="dialog_title">
		<img src="'.WS_ICONS_PATH.'btn_close_bg.gif" onclick="top.hideMenus();" align="right" />
		<span id="dialog_title">'.$title.'</span>
	</div>
</div>
			';
			}
			
			while($opts = $rs->FetchRow()){
				//print_r($opts);
				//echo($opts['FIELD_NAME']);
				if($id==0){
					//$top .= "		<a href=\"javascript:$js_func('{$opts['FIELD_NAME']}','{$opts['FIELD_LABEL']}',{$opts['FIELD_ID']});\" style=\"background-image:url(".$img_path."btn_".strtolower($opts['FIELD_NAME'])."_bg.gif);\" title=\"{$opts['FIELD_LABEL']}\">{$opts['FIELD_NAME']}</a>\n";
// Changes for Work Item #12887
//					if($opts['FIELD_ID']==3 && strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false){
//						$title = "CMR";
//						$label = "CATS - Change Management Request";
//					}else{
						$title = $opts['FIELD_NAME'];
						$label = $opts['FIELD_LABEL'];
//					}
					if($opts['FIELD_GROUP_TYPE']=='menu'){
					$top .= "		<a href=\"javascript:$js_func({$opts['FIELD_ID']});\" style=\"background-image:url(".$img_path."btn_".strtolower($opts['FIELD_NAME'])."_bg.gif);\" title=\"{$label}\">{$title}</a>\n";
					if($i ==0) {
						$top .= "		<a href=\"javascript:top.show_screen('early_advice','search')\" style=\"background-image:url(".$img_path."btn_advice_bg.gif);\" title=\"CATS - Early Advice Form\" >ADVICE</a>\n";				
				
								}			
					}else{
						$top .= "		<a href=\"javascript:top.show_screen({$title},{$opts['FIELD_TYPE']})\" style=\"background-image:url(".$img_path."btn_".strtolower($title)."_bg.gif);\" title=\"{$label}\">{$title}</a>\n";				
						echo $top;
					}
				}else{
					$params="";//($opts['FIELD_DISPLAY_PARAMS']!="")?",".$opts['FIELD_DISPLAY_PARAMS']:"";
					if(($i % 2)==0)
						$col1 .= "<a href=\"javascript:$js_func('{$opts['FIELD_NAME']}','search'$params);\" style=\"background-image:url(".$img_path."btn_{$opts['FIELD_NAME']}_bg.gif);\">{$opts['FIELD_LABEL']}</a>\n";
					else
						$col2 .= "<a href=\"javascript:$js_func('{$opts['FIELD_NAME']}','search'$params);\" style=\"background-image:url(".$img_path."btn_{$opts['FIELD_NAME']}_bg.gif);\">{$opts['FIELD_LABEL']}</a>\n";
				}
				$i++;
			}
			
			if($id==0){
				$top .= "		<a href=\"javascript:window_withfocus('http://boprd/','boprd')\" style=\"background-image:url(".$img_path."btn_reports_bg.gif);\" title=\"Reports - BusinessObjects\" >REPORTS</a>\n";
// Line added MTG 05Dec2007				
			//	$top .= "		<a href=\"javascript:top.show_screen('early_advice','search')\" style=\"background-image:url(".$img_path."btn_advice_bg.gif);\" title=\"CATS - Early Advice Form\" >ADVICE</a>\n";				
				$ret .= $top;
			}else{
				$ret .= '
<table width="100%"  border="0" cellspacing="5" cellpadding="5" align="center" class="splash_menu">
<tr>
	<td id="splashLinks" width="50%">
	' . $col1 . '
	</td>
	<td id="splashLinks" width="50%">
	' . $col2 . '
	</td>
</tr>
</table>
';
			}
			
			if($id!=0){
				$ret .= '</div>';
			}
			
			return $ret;
		}else
			return false;
	}
	
	/*
	+---------------------------------------------------------------------------
	|   MB Menus - DOM Browsers - uses <ul> | <ol> structs to generate menus
	+---------------------------------------------------------------------------
	*/
	function getTreeMenu($id=0, $prefix='', $type = '')
	{
		global $db;
		$index = 1;
		$s='';
		$sql=$this->getSQL($id);
		//print_r($db);
		//$db->debug=true;
		$rs = $db->Execute($sql);// or die(db_error());
		//echo("what the <pre>$sql</pre>");
		$menu_build_type = $this->type;
		//print_r($rs);
		while ($row = $rs->FetchRow())
		{
			//print_r($row);
			
			//if($this->mode=='user_admin') $query="?".$row['query_string'];
			//else 
			$query = WS_PATH . "index.php?m={$this->module}&id=". $row['FIELD_ID'];
			if($this->mode=='admin')
			{
				$func = "go('". $query ."&p=edit&a=edit')";
			}
			else
			{
				$func = "go('". $row['FIELD_TYPE'] .".php". $query ."&mt=tree')";
			}
			switch($menu_build_type){
				case 'tree_all':
					$subMenu = $this->getTreeMenu($row['FIELD_ID'], $prefix .''. $index, $row['FIELD_TYPE']);
					if(strlen($subMenu)>0) $subMenu = ",menu:[\n". $subMenu ."\n]";
					break;
				default:
					$subMenu = '';
					$subCnt = db_query($this->{$this->sql_func}($row['FIELD_ID']));
					if($subCnt) $subMenu = ",menu:[". $subCnt->RecordCount() ."]";
					//$subMenu .= ",sql:\"" . ($this->{$this->sql_func}($row['FIELD_ID'])) . "\"";
					break;
			}
			$s .= '{id:'. $row['FIELD_ID'] .',pid:"'.$row['FIELD_PID'].'",type:"'.$row['NAME'].'",icon:'.($row['FIELD_TYPE_ID']-1).',desc:"'. stripslashes($row['FIELD_LABEL']) .'",func:"'. $func .'"'. $subMenu .'},'."\n";
			//echo($s);
			$index++;
			//$rs->MoveNext();
		}
		if(strlen($s)>0) $s = substr($s, 0, strrpos($s,","));
		return $s;
	}

	/*
	+---------------------------------------------------------------------------
	|   General methods
	+---------------------------------------------------------------------------
	*/
	function getParentID($id){
		$table = $this->tbl;
		$sql =	"select field_pid from $table WHERE	field_id = " . $id . " ";
		$result = db_query($sql);
		$parent = db_fetch_array($result);
		return $parent['field_pid'];
	}
	function getSortOrder($id){
		$table = $this->tbl;
		$sql =	"select sort_order from $table WHERE	field_id = " . $id . " ";
		return db_get_field_value($sql, 'sort_order');
	}
	
	/*
	+-----------------------------------------------------------------------------
	|   Cut/Copy/Paste/Delete operations
	+-----------------------------------------------------------------------------
	*/
	function paste($mode, $list, $where, $dbg=false)
	{
		global $db;
		
		$ok = true;
		$ret = false;
		$table = $this->tbl;
		$alist = explode(",", $list);
		$max_order = db_get_field_value("select max(sort_order)+1 as ord from $table where field_pid=$where","ord");
		if($mode=="cut")
		{
			$ok = db_query("update $table set field_pid=$where where field_id in ($list)");
			$ret = (bool)$ok;
		}
		else
		{
			$values = array();
			foreach($alist as $id){
				$values[]=array($id);
			}
			// copy nodes to new location (PID)
			$db->debug=$dbg;
			$old_bindInputArray=$db->_bindInputArray;
			$db->_bindInputArray=false;
			$ok = $db->Execute("insert into $table values(select * from $table where field_id=?)", $values);
			$db->_bindInputArray=$old_bindInputArray;
			$ret = (bool)$ok;
		}
		// correct sort_order for new nodes
		//  Always paste nodes at the end of the collection
		foreach($alist as $id)
		{
			$ok = db_query("update $table set sort_order=" . ($max_order++) . " where field_id=$id");
		}
		return $ret;
	}
	
	function delete($id=0)
	{
		// TODO: add recursive delete if using MySQL versions < 4.0
		if($id == 0) return false;
		$table = $this->tbl;
		$mask_deleted = defined('MASK_DELETED')?MASK_DELETED:4096;
		
		$ok = db_query("update $table set status = $mask_deleted WHERE field_id=$id") or die(db_error());
		//$ok = db_query("DELETE FROM $table WHERE field_id=$id") or die(db_error());
		return (bool)$ok;
	}
	
	/*
	+-----------------------------------------------------------------------------
	|   Reorder node.
	+-----------------------------------------------------------------------------
	*/
	function reorder( $id, $direction )
	{
		$table = $this->tbl;
		$PendingDeleteFlag = " bitand(status, " . MASK_DELETED . ") = 0 ";
		$case = array(
			"movetop" => 1,
			"movebot"	=> 2,
			"moveup"	=> 3,
			"movedown"	=> 4
		);
		$index = 0;
		$pid = $this->getParentID($id);
		switch($case[$direction]){
			case $case["movetop"]:
				
				$sql =	"select * from $table WHERE	field_id = $pid ";
				$result = db_query($sql);
				
				$sql =	"UPDATE	$table SET	sort_order = 0 WHERE	field_id = $id ";
				$result = db_query($sql);
				
				$sql = "select * from $table where field_pid = $pid order by sort_order ";
				//echo $sql;
				$result = db_query($sql) or die(db_error());
				while ($row = db_fetch_array($result))
				{
					$index++;
					$sql =	"UPDATE	$table SET	sort_order = $index WHERE	field_id = " . $row['FIELD_ID'] . " ";
					db_query($sql);
					echo $index;
				}
				
				break;
			case $case["movebot"]:
			
				$sql =	"select * from $table WHERE	field_id = $pid ";
				$result = db_query($sql);
				
				$sql =	"UPDATE	$table SET	sort_order = 2000000 WHERE	field_id = $id ";
				$result = db_query($sql);
				
				$sql = "select * from $table where field_pid = $pid order by sort_order ";
				echo $sql;
				$result = db_query($sql) or die(db_error());
				while ($row = db_fetch_array($result))
				{
					$index++;
					$sql =	"UPDATE	$table SET	sort_order = $index WHERE	field_id = " . $row['FIELD_ID'] . " ";
					db_query($sql);
					echo $index;
				}
				break;
			case $case["moveup"]:
			
				$sortNew = $this->getSortOrder($id)-1;
				$sql = "select * from $table where pid = $pid and sort_order = $sortNew  ";

				$result = db_query($sql) or die("<b>DB Error in Navigation::Move(up)</b><br>".db_error());
				if(db_num_rows($result) > 0 && $id > 0)
				{
					$data = db_fetch_array($result);
							
					//'   Increment the sort_order for the previous node.
					//'
					$sql =	"UPDATE  $table SET sort_order = sort_order + 1 WHERE   field_id = " . $data['FIELD_ID'] . " 	AND $PendingDeleteFlag ";
					$result = db_query($sql);
								
					//'   Decrement the sort_order for the selected node.
					//'
					$sql =	"UPDATE  $table SET  sort_order = sort_order - 1 WHERE  field_id = $id AND $PendingDeleteFlag ";
					$result = db_query($sql);
				}
				break;
			case $case["movedown"]:
				
				$sortNew = $this->getSortOrder($id)+1;
				$sql = "select * from $table where field_pid = $pid and sort_order = $sortNew ";
				
				$result = db_query($sql) or die("<b>DB Error in Navigation::Move(down)</b><br>".db_error());
				if(db_num_rows($result) > 0 && $id > 0)
				{
					$data = db_fetch_array($result);
					
					//'   Decrement the sort_order for the next node.
					//'
					$sql =	"UPDATE  $table SET sort_order = sort_order - 1 WHERE field_id = " . $data['FIELD_ID'] . " AND $PendingDeleteFlag ";
					$result = db_query($sql);
			
					//'   Increment the sort_order for the selected node.
					//'
					$sql =	"UPDATE  $table SET sort_order = sort_order + 1 WHERE field_id = $id AND $PendingDeleteFlag ";
					$result = db_query($sql);
				
				}else{
					 //To-Do And then ?
				}
			default:
				// nothing happnin here ;)
		}
	}
	
	/*
	+----------------------------------------------------------------------------
	|   Determine if node has parent or not. Note: this routine does not return
	|   an error code.
	+----------------------------------------------------------------------------
	*/
	function nodeHasParent($id)
	{
		$table = $this->tbl;
		$sql =	"SELECT	field_id FROM	$table WHERE field_id = '" .$id ."' AND (bitand(CAST(status AS BIGINT), CAST(" . MASK_DELETED . " AS BIGINT) )+0) = 0 ";
		$result = db_query($sql);
		return (db_num_rows($result) > 0);	
	}

	/*
	+---------------------------------------------------------------------------
	|   Print a basic sitemap to the page
	+---------------------------------------------------------------------------
	*/
	function getSiteMap($id=0, $level='top', $display = 'site', $type = '') // site, home, plain
	{
		$result = db_query($this->{$this->sql_func}($id,$type)) or die(db_error());
		while ($row = db_fetch_array($result))
		{
			if(!isset($s)) $s =	($level=='top')?'<div id="sectionLinks">':'<div id="m'. $id .'" class="menu" onmouseover="menuMouseover(event)">';
			
			$SubMenu = $this->getSiteMap($row['FIELD_ID'], 'child', $display, $row['FIELD_TYPE']);
			$HasChildren = (strlen($SubMenu)>0);
			
			if($this->mode=='user_admin') $query="?".$row['query_string'];
			else $query = "?id=". $row['FIELD_ID'];
			
			// Get url for menu item
			$target="";
			if(!empty($row['url'])){
				$url = $row['url'];
				if(!empty($row['target'])) $target=' target="'.$row['target'].'" ';
			}else $url=$row['FIELD_TYPE'].".php" . $query . '&mt=bj" ';
			
			$s .= '<a href="' . $url . $target;
			
			if($level=='child') $s .= ' class="menuItem" ';
			if($HasChildren) $s .= ($level=='top')?'onmouseover="buttonMouseover(event, \'m'. $row['FIELD_ID'] .'\');" ':'onmouseover="menuItemMouseover(event, \'m'. $row['FIELD_ID'] .'\');" ><span class="menuItemText" ';
			$s .= '>'. stripslashes($row['title']);
			if($HasChildren) $s .= ($level=='top')?'':'</span><span class="menuItemArrow">&#9654;</span>';
			$s .= '</a>';
			
			$SubMenus .= $SubMenu;
			
		}
		if(strlen($s)>0) $s .= '</div>'.$SubMenus;
		return $s;
	}
	
	/*
	+---------------------------------------------------------------------------
	|   Brain Jar Menu system
	|			This menu system is for DOM browsers. Older browsers see ugly menus ;)
	+---------------------------------------------------------------------------
	*/
	function getBJ_Menu($id=0, $level='top', $type = '')
	{
		$result = db_query($this->{$this->sql_func}($id)) or die(db_error());
		while ($row = db_fetch_array($result))
		{
			if(!isset($s)) $s =	($level=='top')?'<div id="sectionLinks">':'<div id="m'. $id .'" class="menu" onmouseover="menuMouseover(event)">';
			
			$SubMenu = $this->getBJ_Menu($row['FIELD_ID'], 'child', $row['FIELD_TYPE']);
			$HasChildren = (strlen($SubMenu)>0);
			
			if($this->mode=='user_admin') $query="?".$row['query_string'];
			else $query = "?id=". $row['FIELD_ID'];
			
			// Get url for menu item
			$target="";
			if(!empty($row['url'])){
				$url = $row['url'];
				if(!empty($row['target'])) $target=' target="'.$row['target'].'" ';
			}else $url=$row['FIELD_TYPE'].".php" . $query . '&mt=bj" ';
			
			$s .= '<a href="' . $url . $target;
			
			if($level=='child') $s .= ' class="menuItem" ';
			if($HasChildren) $s .= ($level=='top')?'onmouseover="buttonMouseover(event, \'m'. $row['FIELD_ID'] .'\');" ':'onmouseover="menuItemMouseover(event, \'m'. $row['FIELD_ID'] .'\');" ><span class="menuItemText" ';
			$s .= '>'. stripslashes($row['title']);
			if($HasChildren) $s .= ($level=='top')?'':'</span><span class="menuItemArrow">&#9654;</span>';
			$s .= '</a>';
			
			$SubMenus .= $SubMenu;
			
		}
		if(strlen($s)>0) $s .= '</div>'.$SubMenus;
		return $s;
	}
	

	
	/*
	+---------------------------------------------------------------------------
	|   Setters
	+---------------------------------------------------------------------------
	*/
	function setMenus($id=1,$selected_id=0)
	{
		global $db;
		
		switch($this->type){
		
		case 'bj':
			$this->menu_html = $this->getBJ_Menu($id);
			$this->menu_js = '<link rel="stylesheet" href ="'.WS_PATH.'js/bj/menu.css" type="text/css">
<script type="text/javascript" src ="'.WS_PATH.'js/bj/menu.js"></script>';
			break;
		case 'tree':case 'tree_all':
			$this->menu_html = '<div id="MENU_1_CONTAINER_0" style="width:180px;"></div>';
			$this->menu_js = '
<script type="text/javascript" src ="'.WS_PATH.'js/tree/rc_menu.js"></script>
<script type="text/javascript" src ="'.WS_PATH.'js/tree/treemenu.js"></script>
<script type="text/javascript" src ="'.WS_PATH.'js/tree/contextmenu.js"></script>
<script type="text/javascript">
function _createTreeMenu(){
	aMenu = ['. $this->getTreeMenu($id,$this->mode) .'];
	oMenuImages = {
		path:"'.WS_PATH.'js/tree/icons/",
		icons:[';
			$sql = "select icon, icon_plus, icon_minus from " . $this->tbl_types . " ";
			$rs = $db->Execute($sql) or die(db_error());
			$i=0;
			while ($row = $rs->FetchRow()){
				$this->menu_js .= '["'.$row['ICON'].'","'.$row['ICON_PLUS'].'","'.$row['ICON_MINUS'].'"],';
			}
			$this->menu_js = preg_replace("/,$/","",$this->menu_js);
			$this->menu_js .= ']
	}
	oMenu = new MenuSystem(1,"aMenu",oMenuImages,"MENU_1_CONTAINER_0",200,0);
	oMenu.config = {
		contextMenuOn:true,
		contextMenuFunction:"return rcMenu(this);"
	};
	oMenu.build();
	vcoMenuSystem = oMenu;
	ContextMenu = new ContextMenu("MENU_1_CONTAINER_0");
}
</script>';
//echo ($this->menu_js);
			break;
		case 'refreshtree':
			$this->menu_js = '<script type="text/javascript">
onload=function(){
	aMenu = ['. $this->getTreeMenu($id,$this->mode) .'];
	//var s="";
	//for(i=0;i<aMenu.length;i++)for(e in aMenu[i])if(e!="icon")s+=e+":"+aMenu[i][e]+"\n";
	//alert(s);
	if(aMenu.length>0)parent.oMenu.refresh('.$id.',aMenu);
	else parent.oMenu.refresh('.$id.');
	if("'.$selected_id.'"!=""&&parseInt("'.$selected_id.'")>0) parent.initTree('.$selected_id.');
}
</script>';
			break;
		
		
		/*
		SHITE GOES HERE
		*/
		
		case 'hm':
			if($id==0) $id = HOME_BASE_ID;
			$this->menu_js = '<script type="text/javascript" src ="'.DIR_WS_ROOT.'hm_menu.php?id='. $id .'"></script>';
			$tpl=new Template("templates/hm_menu.htm");
			$this->menu_html = $tpl->get();
			break;
		case 'mb':
			$this->menu_html = '<div id="topnav">'. $this->getMB_Menu($id,'','menu') .'</div><script type="text/javascript">mbSet("menu", "mb");</script>';
			$this->menu_js = '<link rel="stylesheet" href ="'.DIR_WS_ROOT.'js/mb/'.$theme.'menu.css" type="text/css">
<script type="text/javascript" src ="'.DIR_WS_ROOT.'js/mb/'.$theme.'menu.js"></script>';
			break;
		default:
			$this->menu_html = $this->getMenu($id);
		}
	}
	
	function setTable($val)
	{
		$this->tbl = $val;
	}


/*
+---------------------------------------------------------------------------
|   Getters
+---------------------------------------------------------------------------
*/
	function getMenuHTML(){
		return $this->menu_html;
	}
	function getMenuJS(){
		return $this->menu_js;
	}
}
?>