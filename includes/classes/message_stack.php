<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Configuration box menu
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------

  Example usage:

  $messageStack = new messageStack();
  $messageStack->add('Error: Error 1', 'error');
  $messageStack->add('Error: Error 2', 'warning');
  if ($messageStack->size > 0) echo $messageStack->output();
*/

  class messageStack {
    var $size = 0;

   public function __construct() {
      global $messageToStack;

      $this->errors = array();

      if (cats_session_is_registered('messageToStack')) {
        for ($i = 0, $n = count($messageToStack); $i < $n; $i++) {
          $this->add($messageToStack[$i]['text'], $messageToStack[$i]['type']);
        }
        cats_session_unregister('messageToStack');
      }
    }

    function add($message, $type = 'error') {
			if ($type == 'error') {
        $err = array('params' => 'class="messageStackError"', 'text' => $message);
      } elseif ($type == 'warning') {
        $err = array('params' => 'class="messageStackWarning"', 'text' => $message);
      } elseif ($type == 'success') {
        $err = array('params' => 'class="messageStackSuccess"', 'text' => $message);
			} elseif ($type == 'info') {
        $err = array('params' => 'class="messageStackInfo"', 'text' => $message);
      } else {
        $err = array('params' => 'class="messageStackError"', 'text' => $message);
      }
			
			$this->errors[] = array_merge($err, array('type'=>$type));

      $this->size++;
    }

    function reset() {
      $this->errors = array();
      $this->size = 0;
    }

		function output() {
			$ret = '';
			$err = '';
			$size=$this->size;
      if ($size > 0) {
				foreach($this->errors as $key => $val){
					if($val['type']=='error') $err .= 'Error: ' . $val['text'] . "\n";
					$ret .= '<div '.$val['params'].'><span>'.$val['text'].'</span></div>';
				}
				if($err!=''){
					$msg = "Errors From CATS: {$err} : This email was sent by the CATS error monitor.";
					error_log($msg);
				}
			}
			$this->reset();
      return $ret;
		}
		function output_js() {
			$ret = '';
			$size=$this->size;
      if ($size > 0) {
				foreach($this->errors as $key => $val) $ret .= '"'.$key.'":"'.$val.'",';
			}
			if($ret!='') $ret = "var messageStack = {".$ret."};";
			$this->reset();
      return $ret;
		}
  }
?>
