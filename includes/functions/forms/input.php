<?php
/*
+--------------------------------------------------------------------------
|   input.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for <input> and <textarea> elements such as text fields or file upload fields etc
+---------------------------------------------------------------------------
*/
$baseDir = dirname(__FILE__);
$host_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : getenv('HTTP_HOST');
$server_name = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : getenv('SERVER_NAME');
if (!defined('CATS_ROOT_PATH')) define('CATS_ROOT_PATH', $baseDir);
if (!defined('CATS_INCLUDE_PATH')) define('CATS_INCLUDE_PATH',CATS_ROOT_PATH.'includes/');
$config = parse_ini_file(CATS_INCLUDE_PATH."conf/$host_name/cats_admin.ini");

foreach ($config as $key => $value) {
	if(!defined($key))
		define($key, $value);
}


  function html_imagefile_input_field($name, $value = '', $parameters = '', $type = 'file', $reinsert_value = true) {
    $field = '<input class="sfield" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '"';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;

  }
  
  
  
/*
+---------------------------------------------------------------------------
|	List of photos connected to incident - SRA Jerry
+---------------------------------------------------------------------------
*/
	function html_list_incident_images($name, $value, $parameters = ''){
	
		$s=""; 
		
		// $id = "";//add dev
		// if($id==0){
		// 	if(isset($_GET['id'])) $id = $_GET['id'];
		// }


		$id = isset($_GET['id'])?$_GET['id']:"";
		
		if($id==null || $id=="")
			return '<span id="'.$name.'" '.$parameters.'>Please upload photo(s) after the incident have been saved. </span>';
		
		$s .= '<span id="'.$name.'" '.$parameters.'><table width="80%" class="class="imagetable">';
		$sql = 'select PHOTO_NUMBER, SHOW_IN_REPORT, IMAGE from TBLINCIDENT_PHOTOS where INVESTIGATION_NUMBER = ' . $id;
	
			
		$photo_number = 0;
		$show_in_report = "";
		$image = null;
	
		$result = db_query($sql) or die(db_error());
		$i = 0;
		
		$s .= '<tr>';
		$s .= '<td>';
		$s .= '	<div id="divPhotoButtonSubmit">';
		$s .= '		<button type="button" id="BtnUploadWindowOpen" class="btn btn-warning btn-xs" style="color:white; width:180px; " data-toggle="modal" data-target="#myModalUpload">Upload...</button>';
		$s .= '	</div>';

		$s .= '</td>';
		$s .= '<td></td>';
		$s .= '</tr>';
		
		$s .= '<tr>';
		$s .= '<th width="80%">Photo</td>';
		$s .= '<th width="10%">Show in report</td>';
		$s .= '<th width="10%">Delete</td>';
		$s .= '</tr>';
		while ($row = db_fetch_array($result))//change dev row parameter with ''
		{ 
	        $i = $i + 1;
			$s .= '<tr>';
			$s .= '		<td><img src="' . CATS_DOT_NET_APP_URL . 'ImageHandler.ashx?id=' . $row['PHOTO_NUMBER'] .'"/ width="400px"></td>';
			if($row['SHOW_IN_REPORT'] == 'Y')
				$s .= '		<td><input class="bfield" onclick="javascript:showInReport('. $row['PHOTO_NUMBER'] .', this);" type="checkbox" name="SHOW_IN_REPORT[' . $row['PHOTO_NUMBER'] . ']"  checked  id="SHOW_IN_REPORT[' . $row['PHOTO_NUMBER'] . ']"></td>';
			else
				$s .= '		<td><input class="bfield" onclick="javascript:showInReport('. $row['PHOTO_NUMBER'] .', this);" type="checkbox" name="SHOW_IN_REPORT[' . $row['PHOTO_NUMBER'] . ']"   id="SHOW_IN_REPORT[' . $row['PHOTO_NUMBER'] . ']"></td>';
			$s .= '		<td><button type="button" id="BtnDeletePhoto_'. $row['PHOTO_NUMBER'] . '" class="btn btn-warning btn-xs" style="color:white; " onClick="deletePhoto('. $row['PHOTO_NUMBER'] .')">Delete</button></td>';
			$s .= '</tr>';
			$s .= '<tr>';
			$s .= '  <td>&nbsp;</td>';
			$s .= '</tr>';
		}
		$s .= '</table></span>';
		$s .= '<script type="text/javascript"> var numberOfPhotos = ' . $i .'; if(numberOfPhotos > 3) {	$("#BtnUploadWindowOpen").attr("disabled","true"); } </script>';
		
		return $s;
	}
  
/*
+---------------------------------------------------------------------------
|	List of photos connected to incident - SRA Jerry
+---------------------------------------------------------------------------
*/
	function html_list_incident_images_view($name, $value, $parameters = ''){
	
		$s="";
		// if($id==0){ comment dev
		// 	if(isset($_GET['id'])) $id = $_GET['id'];
		// }

		$id = isset($_GET['id'])?$_GET['id']:"";
		if($id==null || $id=="")
			return '<span id="'.$name.'" '.$parameters.'>Please upload photo(s) after the incident have been saved.</span>';
		
		$s .= '<span id="'.$name.'" '.$parameters.'><table width="80%" class="class="imagetable">';
		$sql = 'select PHOTO_NUMBER, SHOW_IN_REPORT, IMAGE from TBLINCIDENT_PHOTOS where INVESTIGATION_NUMBER = ' . $id;
	
			
		$photo_number = 0;
		$show_in_report = "";
		$image = null;
	
		$result = db_query($sql) or die(db_error());
		$i = 0;
		
		$s .= '<tr>';
		$s .= '<th width="80%">Photo</td>';
		$s .= '</tr>';
		while ($row = db_fetch_array($result))
		{
			$s .= '<tr>';
			$s .= '		<td><img src="'. CATS_DOT_NET_APP_URL . 'ImageHandler.ashx?id=' . $row['PHOTO_NUMBER'] .'"/ width="400px"></td>';
			$s .= '</tr>';
			$s .= '<tr>';
			$s .= '  <td>&nbsp;</td>';
			$s .= '</tr>';
		}
		$s .= '</table></span>';
		
		return $s;
	}  
  
  /*
  
  
  
  
+---------------------------------------------------------------------------
|	List of photos connected to early advice - SRA Craig
+---------------------------------------------------------------------------
*/
  function html_list_early_advice_images($name, $value, $parameters = ''){ 
	
		$s="";
		// if($id==0){ comment dev
		// 	if(isset($_GET['id'])) $id = $_GET['id'];
		// }

		$id = isset($_GET['id'])?$_GET['id']:"";
		if($id==null || $id=="")
			return '<span id="'.$name.'" '.$parameters.'>Please upload photo(s) after the early advice/incident have been saved.</span>';
		
		
		$s .= '<span id="'.$name.'" '.$parameters.'><table width="80%" class="class="imagetable">';
		//$sql = 'select PHOTO_NUMBER, SHOW_IN_REPORT, IMAGE from TBLINCIDENT_PHOTOS where INVESTIGATION_NUMBER = ' . $id;
	
		$photo_number = 0;
		$show_in_report = "";
		$image = null;
	
		//$result = db_query($sql) or die(db_error());
		$i = 0;
		
		$s .= '<tr>';
		$s .= '<td>';
		$s .= '	<div id="divPhotoButtonSubmit">'; 
		$s .= '		<button type="button" id="BtnUploadWindowOpen"   class="btn btn-warning btn-xs" style="color:white; width:180px; " data-toggle="modal" data-target="#myModalUpload">Upload...</button>';
		$s .= '	</div>';

		$s .= '</td>';
		$s .= '<td></td>';
		$s .= '</tr>';
		
		$s .= '<tr>';
		$s .= '<th width="80%">Photo</td>';
		$s .= '<th width="10%">Show in report</td>';
		$s .= '<th width="10%">Delete</td>';
		$s .= '</tr>';
		/* 
		while ($row = db_fetch_array($result))
		{
			$s .= '<tr>';
			$s .= '		<td><img width="800px" src="http://prtdev003:81//ImageHandler.ashx?id=' . $row[PHOTO_NUMBER] .'"/></td>';
			if($row[SHOW_IN_REPORT] == 'Y')
				$s .= '		<td><input class="bfield" type="checkbox" name="SHOW_IN_REPORT[' . $row[PHOTO_NUMBER] . ']"  checked  id="SHOW_IN_REPORT["' . $row[PHOTO_NUMBER] . ']"></td>';
			else
				$s .= '		<td><input class="bfield" type="checkbox" name="SHOW_IN_REPORT[' . $row[PHOTO_NUMBER] . ']"   id="SHOW_IN_REPORT["' . $row[PHOTO_NUMBER] . ']"></td>';
			$s .= '		<td><button type="button" id="BtnDeletePhoto_'. $row[PHOTO_NUMBER] . '" class="btn btn-warning btn-xs" style="color:white; " onClick="deletePhoto('. $row[PHOTO_NUMBER] .')">Delete</button></td>';
			$s .= '</tr>';
		}
		*/
		$s .= '</table></span>';
		
		return $s;
	}
  
  
/*
+---------------------------------------------------------------------------
|	Output a form input field
+---------------------------------------------------------------------------
*/
	function html_form_hidden_display_value($name, $value, $parameters = ''){
		if(empty($value)){
			$display_value = (isset($_POST[$name]))?$_POST[$name]:'';
		}else{
			$display_value = $value;
		}
		return '<span>'.$display_value.'</span>'.html_draw_hidden_field($name, $value, $parameters);
	}

	function html_form_show_hidden($name, $value, $parameters = ''){
		return html_draw_input_field($name."_DISABLED", $value, $parameters . ' disabled=true ').html_draw_hidden_field($name, $value);
	}

	function html_form_hidden($name, $value, $parameters = ''){
		return html_draw_input_field($name, $value, $parameters . ' disabled=true ');
	}

  function html_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" id="' . main_output_string($name) . '" name="' . main_output_string($name) . '"';

    if (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    } elseif (isset($_POST[$name])) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

  
    function html_draw_start_div($name, $value = '') {
		$s = '<div id="'.$name.'" >' . $value;
		return $s;
	}
    function html_draw_end_div($name, $value = '') {
		$s = '</div';
		return $s;
	}

	
  function html_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '"';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;

  }

  function html_draw_upper_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '" OnBlur="javascript:{this.value = this.value.toUpperCase(); }"' ;

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;

  }
  
/*
+---------------------------------------------------------------------------
|	Output a form input field which is disabled
+---------------------------------------------------------------------------
*/	

	function html_draw_disabled_field($name, $value = '', $parameters = '') {
		return html_draw_input_field($name, $value, ' disabled="true" ');
	}
	
	function html_draw_sml_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield_sml" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '"';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }
	
	function html_draw_lge_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield_lge" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '"';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

	function html_draw_revised_score_field($name, $value = '', $parameters = '') {
		global $RECORD;
		$v = $RECORD['REVISED_SCORE'];
		if(($RECORD["CONFIRM_LEVEL"] != "Yes")) {
			return html_draw_input_field($name, $v);
		}else{
			return html_display_value($name, $v);
		}
	}
	
	function html_pcr_assessment_score_display_value($name, $value, $parameters = '')
	{
		$v = 'N/A';
		if(!empty($value)){
			$sql = "SELECT reftable_comment AS TEXT FROM tblRefTable where reftable_type = 'PCR Assessment Level' and reftable_id = $value";
			$v = db_get_one($sql);
		}
		return html_display_value($name, $v, $parameters);
	}

/*
+---------------------------------------------------------------------------
|	Output a form password field
+---------------------------------------------------------------------------
*/

  function html_draw_password_field($name, $value = '', $parameters = 'maxlength="40"') {
    return html_draw_input_field($name, $value, $parameters, 'password', false);
  }

/*
+---------------------------------------------------------------------------
|	Output a form file field
+---------------------------------------------------------------------------
*/

  function html_draw_file_field($name, $required = false) {
    return html_draw_input_field($name, '', '', 'file', $required);
  }

/*
+---------------------------------------------------------------------------
|	Output a form number input field
+---------------------------------------------------------------------------
*/

  function html_draw_number_field($name, $value = '', $parameters = '', $xtra_parameters = '') {
		$len='';
		if($xtra_parameters!='') $len = ", $xtra_parameters";
    return html_draw_input_field($name, $value, $parameters . 'onchange="check_number(this'.$len.');"');
  }
	
/*
+---------------------------------------------------------------------------
|	Output a form IN (number) input field for use with search forms where you 
|	need to validate an IN clause with numbers only
|	e.g. 
|		a valid IN value:
|			1,2,456,865,345
|		an invalid value:
|			1,234,[,in what,blah
+---------------------------------------------------------------------------
*/

  function html_draw_number_in_clause_field($name, $value = '', $parameters = '') {
    return html_draw_input_field($name, $value, $parameters . 'onchange="check_number_in_clause(this);"');
  }

/*
+---------------------------------------------------------------------------
|	Output a form IN (string) input field for use with search forms where you 
|	need to validate an IN clause with string values
|	e.g. 
|		a valid IN value:
|			what,the,blah,1,2,456,865,345 -- NOTE numbers will be quoted
+---------------------------------------------------------------------------
*/

  function html_draw_string_in_clause_field($name, $value = '', $parameters = '') {
    return html_draw_input_field($name, $value, $parameters . 'onchange="check_string_in_clause(this);"');
  }

	/* File Location Control
	|----------------------------------------------------------------------
	| GOVERNMENT INSPECTION FORM
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_get_file_location_field($name, $value){
		return '
		<input class="ltfield" value="'.$value.'" type="text" name="'.$name.'" id="'.$name.'" 
			onclick="load_document(this);" onchange="GetUNC(this,value);"><input type="button" name="btn'.$name.'" value="Browse..." 
			onclick="change_file_location(this.previousSibling,this.nextSibling);"><input type="file" name="browse'.$name.'" style="display: none">
		';
	}

	function html_get_folder_location_field($name, $value){
		return '
		<input class="ltfield" value="'.$value.'" type="text" name="'.$name.'" id="'.$name.'" 
			onclick="open_folder(this);"><input type="button" name="btn'.$name.'" value="Change Folder..." 
			onclick="change_folder_location(this.previousSibling,this.nextSibling);"><input type="file" name="browse'.$name.'" style="display: none">
		';
	}
	
	function html_get_file_location_value($name, $value){
		return '
		<span class="ltfield" id="'.$name.'" onclick="load_document(this);">'.$value.'</span>
		';
	}

	function html_get_folder_location_value($name, $value){
		return '
		<span class="ltfield" id="'.$name.'" onclick="open_folder(this);">'.$value.'</span>
		';
	}
	
/*
+---------------------------------------------------------------------------
|	Output a span to display value (display only)
+---------------------------------------------------------------------------
*/	
	function html_display_value($name, $value, $parameters = ''){
		return '<span id="'.$name.'" '.$parameters.'>'.$value.'</span>';
	}
	
	function html_display_value_icam($name, $value, $parameters = ''){
		return '<span id="'.$name.'" '.$parameters.'>'.$value.'</span>' .' <input type="button" class="button" name="cats::IncidentReport" value="Incident Report" onClick="goToIncidentReport();" >';
	}
	
	function html_display_yesno_value($name, $value, $parameters = ''){
		if($value==0){
			$value="No";
		}else{
			$value="Yes";
		}	
		return '<span id="'.$name.'" '.$parameters.'>'.$value.'</span>';
	}
	
	function html_get_site_access_value($value){
		$ret = "No Access";
		if($value){
			$ret = "";
			$sql = "select site_id as ID, site_description as TEXT from tblsite where site_id in ($value) ";
			//echo($sql);
			$result = db_query($sql) or die(db_error());
			$i = 0;
			while ($row = db_fetch_array($result))
			{
				if(($i++) > 0) $ret .= ", ";
				$ret .= $row['TEXT'];
			}
		}
		//---------------
		return ($ret);
	}

	function html_display_site_access_value($name, $value, $parameters = ''){
		return html_display_value($name, html_get_site_access_value($value));
	}
	
	function html_get_user_groups_access($value){
		$ret = "No Access";
		$i = 0;
		if(((int)$value) > 0){
			$ret = "";
			$sql = "select user_group_mask as mask, user_group_name as name from user_group where ((bitand(user_group_mask, ".$value.")>0 or (user_group_mask=0)) and (bitand(status, ".MASK_HIDDEN.")>0 or (status = 0))) order by user_group_mask desc";
			//echo($sql);
			$result = db_query($sql) or die(db_error());
			while ($row = db_fetch_array($result))
			{
				if($row['MASK'] != 0) {
					if(($i++) > 0) $ret .= ", ";
					$ret .= $row['NAME'];
				}
			}
		}
		return $ret;
	}

	function html_display_user_groups_value($name, $value, $parameters = ''){
		return html_display_value($name, html_get_user_groups_access($value));
	}
	
	function html_draw_incident_category_display($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'IncidentCategory' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			if($arr['CHECKED']==1){
				if($s!="") $s .= ", ";
				$s .= $arr['CHECKBOX_DESC'];
			}
		}
		return $s;
	}

	function html_draw_incident_report_type_display($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'IncidentReportType' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Report_Type' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Report_Type' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			if($arr['CHECKED']==1){
				if($s!="") $s .= ", ";
				$s .= $arr['CHECKBOX_DESC'];
			}
		}
		return $s;
	}
	
	function html_draw_hazard_category_display($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.HazardId) checked FROM tblIncidentForm_Checkboxes B, tblHazard_Checkboxvalues I WHERE I.HazardId(+) = $id AND I.CheckboxType(+) = 'Category' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			if($arr['CHECKED']==1){
				if($s!="") $s .= ", ";
				$s .= $arr['CHECKBOX_DESC'];
			}
		}
		return $s;
	}


	function html_get_reftable_shift_value($value){
		$ret = "No Shift";
		if($value){
			$ret = "";
			$sql = "SELECT reftable_id AS ID, reftable_description AS TEXT FROM tblreftable WHERE reftable_type = 'Shift' AND reftable_id = ($value) ";
			//echo($sql);
			$result = db_query($sql) or die(db_error());
			$i = 0;
			while ($row = db_fetch_array($result))
			{
				if(($i++) > 0) $ret .= ", ";
				$ret .= $row['TEXT'];
			}
		}
		//---------------
		return ($ret);
	}

	function html_display_reftable_shift_value($name, $value, $parameters = ''){
		return html_display_value($name, html_get_reftable_shift_value($value));
	}	
	
/*
+---------------------------------------------------------------------------
|	Output a form textarea field
+---------------------------------------------------------------------------
*/

  function html_draw_textarea_field($name, $text = '', $parameters = '', $wrap=true, $width=100, $height=4, $reinsert_value = true) {
    $field = '<textarea class="sfield" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '" ';
		if($wrap) $field.=' wrap="' . main_output_string($wrap) . '" ';
		if($width) $field.=' cols="' . main_output_string($width) . '" ';
		if($height) $field.=' rows="' . main_output_string($height) . '"';

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= stripslashes($_POST[$name]);
    } elseif (main_not_null($text)) {
      $field .= $text;
    }

    $field .= '</textarea>';

    return $field;
  }

/*
+---------------------------------------------------------------------------
|	Output for PCRT Meeting Date to PCR Meeting Record Link
+---------------------------------------------------------------------------
*/	
	function html_draw_pcr_meeting_link($name, $value, $parameters, $id=0) {
		global $db;
		$s="";
		// Get the id
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		// Build the SQL Statement
		$sql = "SELECT m.meeting_id FROM tblpcr_meetings m, tblpcr_meetings_pcr_details d WHERE m.meeting_id=d.meeting_id AND d.pcr=".$id." AND m.meeting_date=TO_DATE('".$value."','DD-MON-YY')";
		// Get the Matching Meeting
		$meeting_id = db_get_one($sql);
		if ($meeting_id == 0)
			$s .= html_display_value($name, $value, $parameters);
		else {
			$s .= '<a href="'."javascript:top.view('pcr_meetings',".$meeting_id.')">';
			$s .= html_display_value($name, $value, $parameters);
			$s .= '</a>';
		}
		
		return $s;
	}

/*
+---------------------------------------------------------------------------
|	Output for major hazards risks
+---------------------------------------------------------------------------
*/	
	function html_display_initial_risk_table($name, $parameters, $id=0) {
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		
		$s = '<span id="'.$name.'" '.$parameters.'><table width="100%">';
		
		$sql = "SELECT * FROM tblhazardregister WHERE hazard_id=".$id;
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			// First do the headers
			$s .= '<tr>';
			$s .= '<th width="20%">Category</td>';
			$s .= '<th width="20%">Likelihood</td>';
			$s .= '<th width="20%">Consequence</td>';
			$s .= '<th width="20%">Risk Level</td>';
			$s .= '</tr>';
			// Now Community / Reputation
			$s .= '<tr>';
			$s .= '<td>Community/Reputation</td>';
			$s .= '<td>'.$arr['INIRISK_0_L'].'</td>';
			$s .= '<td>'.$arr['INIRISK_0_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("INIRISK_0_R",$arr['INIRISK_0_R'],"READONLY").html_draw_sml_input_field("INIRISK_0_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Safety
			$s .= '<tr>';
			$s .= '<td>Safety</td>';
			$s .= '<td>'.$arr['INIRISK_1_L'].'</td>';
			$s .= '<td>'.$arr['INIRISK_1_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("INIRISK_1_R",$arr['INIRISK_1_R'],"READONLY").html_draw_sml_input_field("INIRISK_1_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Environmental
			$s .= '<tr>';
			$s .= '<td>Environmental</td>';
			$s .= '<td>'.$arr['INIRISK_2_L'].'</td>';
			$s .= '<td>'.$arr['INIRISK_2_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("INIRISK_2_R",$arr['INIRISK_2_R'],"READONLY").html_draw_sml_input_field("INIRISK_2_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Economic
			$s .= '<tr>';
			$s .= '<td>Economic</td>';
			$s .= '<td>'.$arr['INIRISK_3_L'].'</td>';
			$s .= '<td>'.$arr['INIRISK_3_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("INIRISK_3_R",$arr['INIRISK_3_R'],"READONLY").html_draw_sml_input_field("INIRISK_3_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Quality
			$s .= '<tr>';
			$s .= '<td>Quality</td>';
			$s .= '<td>'.$arr['INIRISK_4_L'].'</td>';
			$s .= '<td>'.$arr['INIRISK_4_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("INIRISK_4_R",$arr['INIRISK_4_R'],"READONLY").html_draw_sml_input_field("INIRISK_4_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Overall
			$s .= '<tr>';
			$s .= '<td>Overall</td>';
			$s .= '<td></td>';
			$s .= '<td></td>';
			$s .= '<td>'.html_draw_input_field("INIRISK_OR",$arr[INIRISK_OR],"READONLY").html_draw_sml_input_field("INIRISK_OR_light","","READONLY").'</td>';
			$s .= '</tr>';	
		}
		$s .= '</table></span></td>';
		return $s;
	}
	
		function html_display_current_risk_table($name, $parameters, $id=0) {
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		
		$s = '<span id="'.$name.'" '.$parameters.'><table width="100%">';
		
		$sql = "SELECT * FROM tblhazardregister WHERE hazard_id=".$id;
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			// First do the headers
			$s .= '<tr>';
			$s .= '<th width="20%">Category</td>';
			$s .= '<th width="20%">Likelihood</td>';
			$s .= '<th width="20%">Consequence</td>';
			$s .= '<th width="20%">Risk Level</td>';
			$s .= '</tr>';
			// Now Community / Reputation
			$s .= '<tr>';
			$s .= '<td>Community/Reputation</td>';
			$s .= '<td>'.$arr['RESRISK_0_L'].'</td>';
			$s .= '<td>'.$arr['RESRISK_0_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("RESRISK_0_R",$arr['RESRISK_0_R'],"READONLY").html_draw_sml_input_field("RESRISK_0_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Safety
			$s .= '<tr>';
			$s .= '<td>Safety</td>';
			$s .= '<td>'.$arr['RESRISK_1_L'].'</td>';
			$s .= '<td>'.$arr['RESRISK_1_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("RESRISK_1_R",$arr['RESRISK_1_R'],"READONLY").html_draw_sml_input_field("RESRISK_1_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Environmental
			$s .= '<tr>';
			$s .= '<td>Environmental</td>';
			$s .= '<td>'.$arr['RESRISK_2_L'].'</td>';
			$s .= '<td>'.$arr['RESRISK_2_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("RESRISK_2_R",$arr['RESRISK_2_R'],"READONLY").html_draw_sml_input_field("RESRISK_2_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Economic
			$s .= '<tr>';
			$s .= '<td>Economic</td>';
			$s .= '<td>'.$arr['RESRISK_3_L'].'</td>';
			$s .= '<td>'.$arr['RESRISK_3_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("RESRISK_3_R",$arr['RESRISK_3_R'],"READONLY").html_draw_sml_input_field("RESRISK_3_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Quality
			$s .= '<tr>';
			$s .= '<td>Quality</td>';
			$s .= '<td>'.$arr['RESRISK_4_L'].'</td>';
			$s .= '<td>'.$arr['RESRISK_4_C'].'</td>';
			$s .= '<td>'.html_draw_input_field("RESRISK_4_R",$arr['RESRISK_4_R'],"READONLY").html_draw_sml_input_field("RESRISK_4_R_light","","READONLY").'</td>';
			$s .= '</tr>';
			// Now Overall
			$s .= '<tr>';
			$s .= '<td>Overall</td>';
			$s .= '<td></td>';
			$s .= '<td></td>';
			$s .= '<td>'.html_draw_input_field("RESRISK_OR",$arr[RESRISK_OR],"READONLY").html_draw_sml_input_field("RESRISK_OR_light","","READONLY").'</td>';
			$s .= '</tr>';
		}
		$s .= '</table></span></td>';
		return $s;
	}
	
	/*
	+---------------------------------------------------------------------------
	|	Input for major hazards risks
	+---------------------------------------------------------------------------
	*/	
	function html_draw_initial_risk_table($name, $parameters, $id=0) {
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		
		$s = '<span id="'.$name.'" '.$parameters.'><table width="100%">';

		$harr = array(
			"INIRISK_0_L"=>"", "INIRISK_0_C"=>"", "INIRISK_0_R"=>"",
			"INIRISK_1_L"=>"", "INIRISK_1_C"=>"", "INIRISK_1_R"=>"",
			"INIRISK_2_L"=>"", "INIRISK_2_C"=>"", "INIRISK_2_R"=>"",
			"INIRISK_3_L"=>"", "INIRISK_3_C"=>"", "INIRISK_3_R"=>"",
			"INIRISK_4_L"=>"", "INIRISK_4_C"=>"", "INIRISK_4_R"=>"", "INIRISK_OR" => "",//add dev
		);
		
		// Copy Data from Database into Array
		$sql = "SELECT * FROM tblhazardregister WHERE hazard_id=".$id;
		if ($rs = db_query($sql)) {
			while($arr = $rs->FetchRow()){
				foreach($arr as $hkey => $hvalue) {
					$harr[$hkey] = $hvalue;
				}
			}
		}

		// First do the headers
		$s .= '<tr>';
		$s .= '<th width="20%">Category</td>';
		$s .= '<th width="20%">Likelihood</td>';
		$s .= '<th width="20%">Consequence</td>';
		$s .= '<th width="20%">Risk Level</td>';
		$s .= '</tr>';
		// Now Community / Reputation
		$s .= '<tr>';
		$s .= '<td>Community/Reputation</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("INIRISK_0_L_dd",$harr['INIRISK_0_L'],"onchange=Change_Risk_New('INIRISK_0_L','INIRISK_0_C','INIRISK_0_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("INIRISK_0_C_dd",$harr['INIRISK_0_C'],"onchange=Change_Risk_New('INIRISK_0_L','INIRISK_0_C','INIRISK_0_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("INIRISK_0_R_display",$harr['INIRISK_0_R'],"READONLY").html_draw_sml_input_field("INIRISK_0_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Safety
		$s .= '<tr>';
		$s .= '<td>Safety</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("INIRISK_1_L_dd",$harr['INIRISK_1_L'],"onchange=Change_Risk_New('INIRISK_1_L','INIRISK_1_C','INIRISK_1_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("INIRISK_1_C_dd",$harr['INIRISK_1_C'],"onchange=Change_Risk_New('INIRISK_1_L','INIRISK_1_C','INIRISK_1_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("INIRISK_1_R_display",$harr['INIRISK_1_R'],"READONLY").html_draw_sml_input_field("INIRISK_1_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Environmental
		$s .= '<tr>';
		$s .= '<td>Environmental</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("INIRISK_2_L_dd",$harr['INIRISK_2_L'],"onchange=Change_Risk_New('INIRISK_2_L','INIRISK_2_C','INIRISK_2_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("INIRISK_2_C_dd",$harr['INIRISK_2_C'],"onchange=Change_Risk_New('INIRISK_2_L','INIRISK_2_C','INIRISK_2_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("INIRISK_2_R_display",$harr['INIRISK_2_R'],"READONLY").html_draw_sml_input_field("INIRISK_2_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Economic
		$s .= '<tr>';
		$s .= '<td>Economic</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("INIRISK_3_L_dd",$harr['INIRISK_3_L'],"onchange=Change_Risk_New('INIRISK_3_L','INIRISK_3_C','INIRISK_3_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("INIRISK_3_C_dd",$harr['INIRISK_3_C'],"onchange=Change_Risk_New('INIRISK_3_L','INIRISK_3_C','INIRISK_3_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("INIRISK_3_R_display",$harr['INIRISK_3_R'],"READONLY").html_draw_sml_input_field("INIRISK_3_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Quality
		$s .= '<tr>';
		$s .= '<td>Quality</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("INIRISK_4_L_dd",$harr['INIRISK_4_L'],"onchange=Change_Risk_New('INIRISK_4_L','INIRISK_4_C','INIRISK_4_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("INIRISK_4_C_dd",$harr['INIRISK_4_C'],"onchange=Change_Risk_New('INIRISK_4_L','INIRISK_4_C','INIRISK_4_R','INIRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("INIRISK_4_R_display",$harr['INIRISK_4_R'],"READONLY").html_draw_sml_input_field("INIRISK_4_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Overall
		$s .= '<tr>';
		$s .= '<td>Overall</td>';
		$s .= '<td></td>';
		$s .= '<td></td>';
		$s .= '<td>'.html_draw_input_field("INIRISK_OR_display",$harr['INIRISK_OR'],"READONLY").html_draw_sml_input_field("INIRISK_OR_light","","READONLY").'</td>';
		$s .= '</tr>';		
		$s .= '</table></span></td>';
		return $s;
	}
	
	/*
	+---------------------------------------------------------------------------
	|	Input for major hazards risks
	+---------------------------------------------------------------------------
	*/	
	function html_draw_current_risk_table($name, $parameters, $id=0) {
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		
		$s = '<span id="'.$name.'" '.$parameters.'><table width="100%">';

		$harr = array(
			"RESRISK_0_L"=>"", "RESRISK_0_C"=>"", "RESRISK_0_R"=>"",
			"RESRISK_1_L"=>"", "RESRISK_1_C"=>"", "RESRISK_1_R"=>"",
			"RESRISK_2_L"=>"", "RESRISK_2_C"=>"", "RESRISK_2_R"=>"",
			"RESRISK_3_L"=>"", "RESRISK_3_C"=>"", "RESRISK_3_R"=>"",
			"RESRISK_4_L"=>"", "RESRISK_4_C"=>"", "RESRISK_4_R"=>"", "RESRISK_OR"=>""//add dev
		);
		
		// Copy Data from Database into Array
		$sql = "SELECT * FROM tblhazardregister WHERE hazard_id=".$id;
		if ($rs = db_query($sql)) {
			while($arr = $rs->FetchRow()){
				foreach($arr as $hkey => $hvalue) {
					$harr[$hkey] = $hvalue;
				}
			}
		}

		// First do the headers
		$s .= '<tr>';
		$s .= '<th width="20%">Category</td>';
		$s .= '<th width="20%">Likelihood</td>';
		$s .= '<th width="20%">Consequence</td>';
		$s .= '<th width="20%">Risk Level</td>';
		$s .= '</tr>';
		// Now Community / Reputation
		$s .= '<tr>';
		$s .= '<td>Community/Reputation</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("RESRISK_0_L_dd",$harr['RESRISK_0_L'],"onchange=Change_Risk_New('RESRISK_0_L','RESRISK_0_C','RESRISK_0_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("RESRISK_0_C_dd",$harr['RESRISK_0_C'],"onchange=Change_Risk_New('RESRISK_0_L','RESRISK_0_C','RESRISK_0_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("RESRISK_0_R_display",$harr['RESRISK_0_R'],"READONLY").html_draw_sml_input_field("RESRISK_0_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Safety
		$s .= '<tr>';
		$s .= '<td>Safety</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("RESRISK_1_L_dd",$harr['RESRISK_1_L'],"onchange=Change_Risk_New('RESRISK_1_L','RESRISK_1_C','RESRISK_1_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("RESRISK_1_C_dd",$harr['RESRISK_1_C'],"onchange=Change_Risk_New('RESRISK_1_L','RESRISK_1_C','RESRISK_1_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("RESRISK_1_R_display",$harr['RESRISK_1_R'],"READONLY").html_draw_sml_input_field("RESRISK_1_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Environmental
		$s .= '<tr>';
		$s .= '<td>Environmental</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("RESRISK_2_L_dd",$harr['RESRISK_2_L'],"onchange=Change_Risk_New('RESRISK_2_L','RESRISK_2_C','RESRISK_2_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("RESRISK_2_C_dd",$harr['RESRISK_2_C'],"onchange=Change_Risk_New('RESRISK_2_L','RESRISK_2_C','RESRISK_2_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("RESRISK_2_R_display",$harr['RESRISK_2_R'],"READONLY").html_draw_sml_input_field("RESRISK_2_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Economic
		$s .= '<tr>';
		$s .= '<td>Economic</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("RESRISK_3_L_dd",$harr['RESRISK_3_L'],"onchange=Change_Risk_New('RESRISK_3_L','RESRISK_3_C','RESRISK_3_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("RESRISK_3_C_dd",$harr['RESRISK_3_C'],"onchange=Change_Risk_New('RESRISK_3_L','RESRISK_3_C','RESRISK_3_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("RESRISK_3_R_display",$harr['RESRISK_3_R'],"READONLY").html_draw_sml_input_field("RESRISK_3_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Quality
		$s .= '<tr>';
		$s .= '<td>Quality</td>';
		$s .= '<td>'.html_form_draw_hazard_likelihood_dd("RESRISK_4_L_dd",$harr['RESRISK_4_L'],"onchange=Change_Risk_New('RESRISK_4_L','RESRISK_4_C','RESRISK_4_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_form_draw_hazard_consequence_dd("RESRISK_4_C_dd",$harr['RESRISK_4_C'],"onchange=Change_Risk_New('RESRISK_4_L','RESRISK_4_C','RESRISK_4_R','RESRISK_OR')").'</td>';
		$s .= '<td>'.html_draw_input_field("RESRISK_4_R_display",$harr['RESRISK_4_R'],"READONLY").html_draw_sml_input_field("RESRISK_4_R_light","","READONLY").'</td>';
		$s .= '</tr>';
		// Now Overall
		$s .= '<tr>';
		$s .= '<td>Overall</td>';
		$s .= '<td></td>';
		$s .= '<td></td>';
		$s .= '<td>'.html_draw_input_field("RESRISK_OR_display",$harr['RESRISK_OR'],"READONLY").html_draw_sml_input_field("RESRISK_OR_light","","READONLY").'</td>';
		$s .= '</tr>';	
		$s .= '</table></span></td>';
		return $s;
	}

	/*
	+---------------------------------------------------------------------------
	|	Modal Popup for Risk - Consequence Actual
	+---------------------------------------------------------------------------
	*/	
	function html_draw_risk_consequence_actual_modal($name, $parameters, $id=0) {
		$s="";


		$s .= '<button type="button" id="BtnConsequenceActualOpen" class="btn btn-warning btn-xs" style="color:white; width:180px; position: relative; top: -24px; left: 230px" data-toggle="modal" data-target="#myModalConsequenceActual">Set Actual Consequence</button>';

//		$s .='<table style="width: 800px"><tr><td style="width: 800px">';

		
        $s .= '<div class="modal fade" id="myModalConsequenceActual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="position: relative; top: 120px; left: 350px">';
        $s .= '  <div class="modal-dialog" style="width: 1100px">';
        $s .= '    <div class="modal-content">';
        $s .= '      <div class="modal-header">';
        $s .= '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $s .= '        <h4 class="modal-title" id="myModalLabelActual">Actual Consequence, select from the appropriate category</h4>';
        $s .= '      </div>';
        $s .= '      <div class="modal-body" style="background-color: #dedef8;">';



        $s .= '        <table class="imagetable" style="width:600px">';
        $s .= '        <tr>';
	    $s .= '            <th>Select Level</th><th>Safety</th><th>Environment</th><th>Community /<br/> Reputation</th><th>Quality</th><th>Economic</th>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btn5ConsequenceActual" type="button" class="btn btn-xs" style="width:100px; background-color:red" onclick="javascript:setConsequenceActual(\'5 - Catastrophic\')" >5 - Catastropic</button></td>';
		$s .= '			<td>Potential fatality from injury or occ illness</td>';
		$s .= '			<td>Catastrophic impact / Off-site release impoacting on external partis</td>';
		$s .= '			<td>Catastrophic impact on reputation / National Media / Ongoing State Medie / Court Action</td>';
		$s .= '			<td>Loss of major customer or market</td>';
		$s .= '			<td>>$5 mill</td>';
        $s .= '        </tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn4ConsequenceActual" type="button" class="btn btn-xs" style="width:100px; background-color:red" onclick="javascript:setConsequenceActual(\'4 - Major\')" >4 - Major</button></td>';
		$s .= '			<td>Occ illness or Injury resulting in permanent disability</td>';
		$s .= '			<td>Major impact / Off-site release</td>';
		$s .= '			<td>Major impact on reputation / Regional or State Media / Significant stakeholder interest / Fine or Notice issued</td>';
		$s .= '			<td>Quality issue affecting multiple customers</td>';
		$s .= '			<td>$500K - $5 mill</td>';
		$s .= '		</tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn3ConsequenceActual" type="button" class="btn btn-xs" style="width:100px; background-color:orange; color:black" onclick="javascript:setConsequenceActual(\'3 - Moderate\')" >3 - Moderate</button></td>';
		$s .= '			<td>Occ illnes or injury resulting in LTI, RWI or MTI</td>';
		$s .= '			<td>Moderate impact / On-site release not immediately contained</td>';
		$s .= '			<td>Moderate impact on reputation / Local Media / Repeat External complaint / Complaint irate</td>';
		$s .= '			<td>Quality excursion affecting multiple lots</td>';
		$s .= '			<td>$50K - $500K</td>';
		$s .= '		</tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn2ConsequenceActual" type="button" class="btn btn-xs" style="width:100px; background-color:yellow; color:black" onclick="javascript:setConsequenceActual(\'2 - Minor\')" >2 - Minor</button></td>';
		$s .= '			<td>Occ illnes or injury resulting in First Aid Treatment</td>';
		$s .= '			<td>Minor impact / On-site release immediatly contained</td>';
		$s .= '			<td>Minor impact on reputation / Local Issue / External complaint over minor issue</td>';
		$s .= '			<td>Quality excursion affecting multiple lots</td>';
		$s .= '			<td>$5K - $50K</td>';
		$s .= '		</tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn1ConsequenceActual" type="button" class="btn btn-xs" style="width:100px; background-color:green" onclick="javascript:setConsequenceActual(\'1 - Insignificant\')" >1 - Insignificant</button></td>';
		$s .= '			<td>No Injury or occupational illness</td>';
		$s .= '			<td>No or negligible environmental impact</td>';
		$s .= '			<td>Unsubstantiated External Inquiry / No stakeholder interest</td>';
		$s .= '			<td>Quality isue within specification</td>';
		$s .= '			<td><5K</td>';
		$s .= '		</tr>';
        $s .= '        </table>';

        $s .= '      </div>';
        $s .= '      <div class="modal-footer">';
        $s .= '        <button type="button" id="BthConsequenceCloseActual" class="btn btn-default" data-dismiss="modal">Close</button>';
        $s .= '      </div>';
        $s .= '    </div>';
        $s .= '  </div>';
		$s .='<img src="style/cats/images/riskmatrix.png" id="riskmatriximg" style="position: relative; top: -545px; left: 900px" />';	
        $s .= '</div>';

//		$s .='</td><td>';
//		$s .= '';
//		$s .='</td><td>';
//		$s .='</tr></table>';
		
		return $s;
	}


	/*
	+---------------------------------------------------------------------------
	|	Modal Popup for Risk - Consequence Potential
	+---------------------------------------------------------------------------
	*/	
	function html_draw_risk_consequence_potential_modal($name, $parameters, $id=0) {
		$s="";

		$s = '<button type="button" id="BtnConsequencePotentialOpen" class="btn btn-warning btn-xs" style="color:white; width:180px; position: relative; top: -23px; left: 230px" data-toggle="modal" data-target="#myModalConsequencePotential">Set Potential Consequence</button>';
		
        $s .= '<div class="modal fade" id="myModalConsequencePotential" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="position: relative; top: 0px; left: -40px">';
        $s .= '  <div class="modal-dialog" style="width: 1100px">';
        $s .= '    <div class="modal-content">';
        $s .= '      <div class="modal-header">';
        $s .= '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $s .= '        <h4 class="modal-title" id="myModalLabelPotential">Potential Consequence, select from the appropriate category</h4>';
        $s .= '      </div>';
        $s .= '      <div class="modal-body" style="background-color: #dedef8;">';



        $s .= '        <table class="imagetable" style="width:600px">';
        $s .= '        <tr>';
	    $s .= '            <th>Select Level</th><th>Safety</th><th>Environment</th><th>Community /<br/> Reputation</th><th>Quality</th><th>Economic</th>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btn5ConsequencePotential" type="button" class="btn btn-xs" style="width:100px; background-color:red" onclick="javascript:setConsequencePotential(\'5 - Catastrophic\')" >5 - Catastropic</button></td>';
		$s .= '			<td>Potential fatality from injury or occ illness</td>';
		$s .= '			<td>Catastrophic impact / Off-site release impoacting on external partis</td>';
		$s .= '			<td>Catastrophic impact on reputation / National Media / Ongoing State Medie / Court Action</td>';
		$s .= '			<td>Loss of major customer or market</td>';
		$s .= '			<td>>$5 mill</td>';
        $s .= '        </tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn4ConsequencePotential" type="button" class="btn btn-xs" style="width:100px; background-color:red" onclick="javascript:setConsequencePotential(\'4 - Major\')" >4 - Major</button></td>';
		$s .= '			<td>Occ illness or Injury resulting in permanent disability</td>';
		$s .= '			<td>Major impact / Off-site release</td>';
		$s .= '			<td>Major impact on reputation / Regional or State Media / Significant stakeholder interest / Fine or Notice issued</td>';
		$s .= '			<td>Quality issue affecting multiple customers</td>';
		$s .= '			<td>$500K - $5 mill</td>';
		$s .= '		</tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn3ConsequencePotential" type="button" class="btn btn-xs" style="width:100px; background-color:orange; color:black" onclick="javascript:setConsequencePotential(\'3 - Moderate\')" >3 - Moderate</button></td>';
		$s .= '			<td>Occ illnes or injury resulting in LTI, RWI or MTI</td>';
		$s .= '			<td>Moderate impact / On-site release not immediately contained</td>';
		$s .= '			<td>Moderate impact on reputation / Local Media / Repeat External complaint / Complaint irate</td>';
		$s .= '			<td>Quality excursion affecting multiple lots</td>';
		$s .= '			<td>$50K - $500K</td>';
		$s .= '		</tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn2ConsequencePotential" type="button" class="btn btn-xs" style="width:100px; background-color:yellow; color:black" onclick="javascript:setConsequencePotential(\'2 - Minor\')" >2 - Minor</button></td>';
		$s .= '			<td>Occ illnes or injury resulting in First Aid Treatment</td>';
		$s .= '			<td>Minor impact / On-site release immediatly contained</td>';
		$s .= '			<td>Minor impact on reputation / Local Issue / External complaint over minor issue</td>';
		$s .= '			<td>Quality excursion affecting multiple lots</td>';
		$s .= '			<td>$5K - $50K</td>';
		$s .= '		</tr>';
		$s .= '		<tr>';
		$s .= '			<td><button id="btn1ConsequencePotential" type="button" class="btn btn-xs" style="width:100px; background-color:green" onclick="javascript:setConsequencePotential(\'1 - Insignificant\')" >1 - Insignificant</button></td>';
		$s .= '			<td>No Injury or occupational illness</td>';
		$s .= '			<td>No or negligible environmental impact</td>';
		$s .= '			<td>Unsubstantiated External Inquiry / No stakeholder interest</td>';
		$s .= '			<td>Quality isue within specification</td>';
		$s .= '			<td><5K</td>';
		$s .= '		</tr>';
        $s .= '        </table>';

        $s .= '      </div>';
        $s .= '      <div class="modal-footer">';
        $s .= '        <button type="button" id="BthConsequenceClosePotential" class="btn btn-default" data-dismiss="modal">Close</button>';
        $s .= '      </div>';
        $s .= '    </div>';
        $s .= '  </div>';
		$s .='<img src="/style/cats/images/riskmatrix.png" id="riskmatriximg" style="position: relative; top: -545px; left: 900px" />';	
        $s .= '</div>';

		return $s;
	}

	
	
	/*
	+---------------------------------------------------------------------------
	|	Modal Popup for Risk - Likelihood Actual
	+---------------------------------------------------------------------------
	*/	
	function html_draw_risk_likelihood_actual_modal($name, $parameters, $id=0) {
		$s="";

		$s = '<button type="button" id="BtnLikelihoodActualOpen" class="btn btn-warning btn-xs" style="color: white; width: 180px; position: relative; top: -23px; left: 230px" data-toggle="modal" data-target="#myModalLikelihoodActual">Set Actual Likelihood</button>';
		
		$s .= '<div class="modal fade" id="myModalLikelihoodActual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="position: relative; top: 0px; left: -40px">';
        $s .= '<div class="modal-dialog" style="width: 1000px">';
        $s .= '    <div class="modal-content">';
        $s .= '      <div class="modal-header">';
        $s .= '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $s .= '        <h4 class="modal-title" id="myModalLabelActual">Determine the likelihood of the specific Actual consequence</h4>';
        $s .= '      </div>';
        $s .= '      <div class="modal-body" style="background-color: #dedef8;">';
        $s .= '        <!-- Table goes in the document BODY -->';
        $s .= '        <table class="imagetable" style="width:550px">';
        $s .= '        <tr>';
	    $s .= '            <th>Select Likelihood</th><th>Description</th>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnA_Actual" type="button" class="btn btn-xs" style="width:150px; background-color:red" onclick="javascript:setLikelihoodActual(\'A - Almost Certain\')" >A - Almost Certain</button></td>';
		$s .= '			<td>Consequence is expected to occur in most circumstances. <br/> Greater than 1 in 10 or Expected to occur in the next month</td>';
        $s .= '        </tr>';
	
        $s .= '        <tr>';
		$s .= '			<td><button id="btnB_Actual" type="button" class="btn btn-xs" style="width:150px; background-color:red" onclick="javascript:setLikelihoodActual(\'B - Likely\')" >B - Likely</button></td>';
		$s .= '			<td>Consequence will probably occur in some circumstances.<br/>  <1 in 10 but > 1 in 100 OR Could occur in the next year</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnC_Actual" type="button" class="btn btn-xs" style="width:150px; background-color:orange; color:black" onclick="javascript:setLikelihoodActual(\'C - Moderate\')" >C - Moderate</button></td>';
		$s .= '			<td>The consequence might occur at some time.<br/>  <1 in 100 but > 1,000 OR Could occur in the next 5 years.</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnD_Actual" type="button" class="btn  btn-xs" style="width:150px; background-color:yellow; color:black" onclick="javascript:setLikelihoodActual(\'D - Unlikely\')" >D - Unlikely</button></td>';
		$s .= '			<td>The consequence could occur at some time, but not expected. <br/>  <1 in 1,000 but > 1 in 100,000 OR Could occur in next 20 years.</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnE_Actual" type="button" class="btn  btn-xs" style="width:150px; background-color:green; " onclick="javascript:setLikelihoodActual(\'E - Rare\')" >E - Rare</button></td>';
		$s .= '			<td>The consequence may occur in exceptional circumstances.<br/>  <1 in 1000,000 OR Could occur in the next 50 years.</td>';
        $s .= '        </tr>';
	
	
        $s .= '        </table>';

        $s .= '      </div>';
        $s .= '      <div class="modal-footer">';
        $s .= '        <button type="button" id="BthLikelihoodActualClose" class="btn btn-default" data-dismiss="modal">Close</button>';
        $s .= '      </div>';
        $s .= '    </div>';
        $s .= '  </div>';
		$s .='   <img src="/style/cats/images/riskmatrix.png" id="riskmatriximg" style="position: relative; width:350px; top: -400px; left: 900px" />';	
        $s .= '</div>';
	
		return $s;
	}


	/*
	+---------------------------------------------------------------------------
	|	Modal Popup for Risk - Likelihood Potential
	+---------------------------------------------------------------------------
	*/	
	function html_draw_risk_likelihood_potential_modal($name, $parameters, $id=0) {
		$s="";

		$s = '<button type="button" id="BtnLikelihoodPotentialOpen" class="btn btn-warning btn-xs" style="color: white; width: 180px; position: relative; top: -23px; left: 230px" data-toggle="modal" data-target="#myModalLikelihoodPotential">Set Potential Likelihood</button>';
		
		$s .= '<div class="modal fade" id="myModalLikelihoodPotential" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="position: relative; top: 0px; left: -40px">';
        $s .= '<div class="modal-dialog" style="width: 1050px">';
        $s .= '    <div class="modal-content">';
        $s .= '      <div class="modal-header">';
        $s .= '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $s .= '        <h4 class="modal-title" id="myModalLabelPotential">Determine the likelihood of the specific Potential consequence</h4>';
        $s .= '      </div>';
        $s .= '      <div class="modal-body" style="background-color: #dedef8;">';
        $s .= '        <!-- Table goes in the document BODY -->';
        $s .= '        <table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Select Likelihood</th><th>Description</th>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnA_Potential" type="button" class="btn btn-xs" style="width:150px; background-color:red" onclick="javascript:setLikelihoodPotential(\'A - Almost Certain\')" >A - Almost Certain</button></td>';
		$s .= '			<td>Consequence is expected to occur in most circumstances. <br/> Greater than 1 in 10 or Expected to occur in the next month</td>';
        $s .= '        </tr>';
	
        $s .= '        <tr>';
		$s .= '			<td><button id="btnB_Potential" type="button" class="btn btn-xs" style="width:150px; background-color:red" onclick="javascript:setLikelihoodPotential(\'B - Likely\')" >B - Likely</button></td>';
		$s .= '			<td>Consequence will probably occur in some circumstances.<br/>  <1 in 10 but > 1 in 100 OR Could occur in the next year</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnC_Potential" type="button" class="btn btn-xs" style="width:150px; background-color:orange; color:black" onclick="javascript:setLikelihoodPotential(\'C - Moderate\')" >C - Moderate</button></td>';
		$s .= '			<td>The consequence might occur at some time.<br/>  <1 in 100 but > 1,000 OR Could occur in the next 5 years.</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnD_Potential" type="button" class="btn btn-xs" style="width:150px; background-color:yellow; color:black" onclick="javascript:setLikelihoodPotential(\'D - Unlikely\')" >D - Unlikely</button></td>';
		$s .= '			<td>The consequence could occur at some time, but not expected. <br/>  <1 in 1,000 but > 1 in 100,000 OR Could occur in next 20 years.</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '			<td><button id="btnE_Potential" type="button" class="btn btn-xs" style="width:150px; background-color:green; " onclick="javascript:setLikelihoodPotential(\'E - Rare\')" >E - Rare</button></td>';
		$s .= '			<td>The consequence may occur in exceptional circumstances.<br/>  <1 in 1000,000 OR Could occur in the next 50 years.</td>';
        $s .= '        </tr>';
	
	
        $s .= '        </table>';

        $s .= '      </div>';
        $s .= '      <div class="modal-footer">';
        $s .= '        <button type="button" id="BthLikelihoodPotentialClose" class="btn btn-default" data-dismiss="modal">Close</button>';
        $s .= '      </div>';
        $s .= '    </div>';
        $s .= '  </div>';
		$s .='   <img src="/style/cats/images/riskmatrix.png" id="riskmatriximg" style="position: relative; width: 350px; top: -400px; left: 900px" />';	
        $s .= '</div>';
	
		return $s;
	}

	
	/*
	+---------------------------------------------------------------------------
	|	Add actions
	+---------------------------------------------------------------------------
	*/	
	function html_draw_actions($name, $parameters, $id=0) {
		$s="";

		$s = '<div id="SequenceOfEventsGroupDIV">';
		$s .= '	<input type="button" id="seqOfEventsBtn" class="btn btn-info btn-xs active" value="Show/Hide Events" onClick="$(\'#SequenceOfEventsGroupSubDIV\').toggle(); " >';
		$s .= '	<div id="SequenceOfEventsGroupSubDIV" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Date</th><th>Time</th><th>Event Description</th>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW1" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW1" >';
        $s .= '        		</td>';
        $s .= '        		<td style="width:800px">';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW1" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW2" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW2" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW2" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW3" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW3" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW3" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW4" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW4" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW4" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW5" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW5" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW5" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW6" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW6" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW6" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW7" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW7" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW7" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW8" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW8" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW8" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW9" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW9" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW9" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW10" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW10" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW10" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW11" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW11" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW11" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DATEACTIONROW12" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_TIMEACTIONROW12" >';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_ACTIONROW12" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
		
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}	
	
	
	
	//---------------- creates the table for Timeline events -----------------------------------------
	function html_draw_timeline_events($name, $parameters, $id=0) { 
		$s="";

		$s = '<div id="TimelineBoxesGroup">';
		$s .= '	<input type="button" id="TimelineBtn" class="btn btn-info btn-xs active" value="Add Timeline" onClick="$(\'#TimelineDiv1\').show(); $(\'#trTL1\').show(); $(\'#TimelineBtn\').hide(); $(\'#TimelineShowHideBtn\').show();" >';
		$s .= '	<input type="button" id="TimelineShowHideBtn" style="display:none" class="btn btn-info btn-xs active" value="Show/Hide Timeline" onClick="$(\'#TimelineDiv1\').toggle();" >';
		$s .= '	<div id="TimelineDiv1" style="display:none">';
		$s .= '		<input type="button" id="AddTimelineBtn" class="btn btn-info btn-xs active" value="Add Another Event" onClick="addNewEvent();" >';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <td colspan="4">If your Event Timeline is likely to exceed 30 lines please do it outside of CATS and attach as a document below. <br/>';
		$s .= '            	                Please avoid special characters eg ~ # ^.';
		$s .= '            </td>';
        $s .= '        </tr>';  		
		$s .= '        <tr>';
	    $s .= '             <th>Date</th><th>Time</th><th>Description</th>';
        $s .= '        </tr>';

		
		$i=1;
		while($i <= 30){
			$s .= '        <tr id="trTL'.$i.'" style="display:none">';
			$s .= '        		<td style="display:none">';
			$s .= '					<input type="textbox" style=\'width="60px"\' id="_TLROW'.$i.'" value="'.$i.'" disabled>';
			$s .= '        		</td>';
			$s .= '        		<td>';
			$s .= '					<input type="textbox" style=\'width="200px"\' id="_TLDATE'.$i.'" maxlenght="100">';
			$s .= '        		</td>';
			$s .= '        		<td>';
			$s .= '					<input type="textbox" style=\'width="100px"\' id="_TLTIME'.$i.'" maxlenght="100">';
			$s .= '        		</td>';
			$s .= '        		<td style="width:1200px">';
			$s .= '					<textarea rows="2" cols="600" id="_TLDESCRIPTION'.$i.'" maxlenght="2000"></textarea>';
			$s .= '        		</td>';
			$s .= '        </tr>';	
			$i = $i + 1;
		}
		
      
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
	}
		
	//---------------- creates the table for Absent or Failed defences-----------------------------------------
	function html_draw_codes_df($name, $parameters, $id=0) { 
		$s="";

		$s = '<div id="DFCodeBoxesGroup">';
		$s .= '	<input type="button" id="DFCodeBtn" class="btn btn-info btn-xs active" value="Show/Hide Codes" onClick="$(\'#DFCodeBoxDiv1\').toggle(); " >';
		$s .= '	<div id="DFCodeBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Code</th><th>Based on the above Incident Facts, IDENTIFY the Absent/Failed defences that contributed to the incident and Give Reasons.</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF1">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW1"  maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td style="width:1200px">';
		$s .= '					<textarea class="sfield" rows="2" cols="600" id="_DF_CODE_FACTOR_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF2">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW2" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea class="sfield" rows="2" cols="600" id="_DF_CODE_FACTOR_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF3">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW3" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea class="sfield" rows="2" cols="600" id="_DF_CODE_FACTOR_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF4">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW4" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea class="sfield" rows="2" cols="600" id="_DF_CODE_FACTOR_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF5">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW5" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea class="sfield" rows="2" cols="600" id="_DF_CODE_FACTOR_ROW5" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF6">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW6" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_DF_CODE_FACTOR_ROW6" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF7">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW7" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_DF_CODE_FACTOR_ROW7" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trDF8">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_DF_CODEROW8" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_DF_CODE_FACTOR_ROW8" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}	


	//---------------- creates the table for Individual/Team Actions-----------------------------------------
	function html_draw_codes_it($name, $parameters, $id=0) {
		$s="";

		$s = '<div id="ITCodeBoxesGroup">';
		$s .= '	<input type="button" id="ITCodeBtn" class="btn btn-info btn-xs active" value="Show/Hide Codes" onClick="$(\'#ITCodeBoxDiv1\').toggle(); " >';
		$s .= '	<div id="ITCodeBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Code</th><th>Based on the above Incident Facts, IDENTIFY the Individual / team actions that contributed to the Incident and Give Reasons.</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trIT1">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_IT_CODEROW1" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td style="width:1200px">';
		$s .= '					<textarea rows="2" cols="600" id="_IT_CODE_FACTOR_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trIT2">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_IT_CODEROW2" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_IT_CODE_FACTOR_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trIT3">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_IT_CODEROW3" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_IT_CODE_FACTOR_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trIT4">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_IT_CODEROW4" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_IT_CODE_FACTOR_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}	

	//---------------- creates the table for Workplace-----------------------------------------
	function html_draw_codes_wf($name, $parameters, $id=0) {
		$s="";

		$s = '<div id="WFCodeBoxesGroup">';
		$s .= '	<input type="button" id="WFCodeBtn" class="btn btn-info btn-xs active" value="Show/Hide Codes" onClick="$(\'#WFCodeBoxDiv1\').toggle(); " >';
		$s .= '	<div id="WFCodeBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Code</th><th>Based on the above Incident Facts, IDENTIFY the Task/ Environmental conditions that contributed and Give Reasons</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trWF1">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_WF_CODEROW1" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td style="width:1200px">';
		$s .= '					<textarea rows="2" cols="600" id="_WF_CODE_FACTOR_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trWF2">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_WF_CODEROW2" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_WF_CODE_FACTOR_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trWF3">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_WF_CODEROW3" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_WF_CODE_FACTOR_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trWF4">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_WF_CODEROW4" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_WF_CODE_FACTOR_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}	


	//---------------- creates the table for Human Factor-----------------------------------------
	function html_draw_codes_hf($name, $parameters, $id=0) {
		$s="";

		$s = '<div id="HFCodeBoxesGroup">';
		$s .= '	<input type="button" id="HFCodeBoxBtn" class="btn btn-info btn-xs active" value="Show/Hide Codes" onClick="$(\'#HFCodeBoxDiv1\').toggle(); " >';
		$s .= '	<div id="HFCodeBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Code</th><th>Based on the above Incident Facts, IDENTIFY the Task/ Environmental conditions that contributed and Give Reasons</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trHF1">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_HF_CODEROW1" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td style="width:1200px">';
		$s .= '					<textarea rows="2" cols="600" id="_HF_CODE_FACTOR_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trHF2">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_HF_CODEROW2" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_HF_CODE_FACTOR_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trHF3">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_HF_CODEROW3" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_HF_CODE_FACTOR_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trHF4">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_HF_CODEROW4" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_HF_CODE_FACTOR_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}	

	//---------------- creates the table for Organisational Factor-----------------------------------------
	function html_draw_codes_of($name, $parameters, $id=0) {
		$s="";

		$s = '<div id="OFCodeBoxesGroup">';
		$s .= '	<input type="button" id="OFCodeBoxBtn" class="btn btn-info btn-xs active" value="Show/Hide Codes" onClick="$(\'#OFCodeBoxDiv1\').toggle(); " >';
		$s .= '	<div id="OFCodeBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Code</th><th>Based on the above Incident Facts, IDENTIFY the Organisational factors that contributed to the Incident – Give Reasons</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trOF1">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_OF_CODEROW1" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td style="width:1200px">';
		$s .= '					<textarea rows="2" cols="600" id="_OF_CODE_FACTOR_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trOF2">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_OF_CODEROW2" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_OF_CODE_FACTOR_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trOF3">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_OF_CODEROW3" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_OF_CODE_FACTOR_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trOF4">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_OF_CODEROW4" maxlenght="20">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" id="_OF_CODE_FACTOR_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}	
	
	//----------------------------- the function below draws the table for Key Learnings----------------------------------------
	function html_draw_key_learnings($name, $parameters, $id=0) {
		$s="";

		$s = '<div id="KeyLearningBoxesGroup">';
		$s .= '	<input type="button" id="keyLearningBtn" class="btn btn-info btn-xs active" value="Show/Hide Key Learnings" onClick="$(\'#KeyLearningBoxDiv1\').toggle(); " >';
		$s .= '	<div id="KeyLearningBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Title</th><th>Description</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trKL1">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_KEY_LEARING_TITLE_ROW1"  maxlenght="200">';
        $s .= '        		</td>';
        $s .= '        		<td style="width:1200px">';
		$s .= '					<textarea rows="2" cols="600" maxlenght="2000" id="_KEY_LEARING_DESC_ROW1" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trKL2">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_KEY_LEARING_TITLE_ROW2"  maxlenght="200">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" maxlenght="2000" id="_KEY_LEARING_DESC_ROW2" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trKL3">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_KEY_LEARING_TITLE_ROW3"  maxlenght="200">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" maxlenght="2000" id="_KEY_LEARING_DESC_ROW3" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trKL4">';
        $s .= '        		<td>';
		$s .= '					<input type="textbox" id="_KEY_LEARING_TITLE_ROW4" maxlenght="200">';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="600" maxlenght="2000" id="_KEY_LEARING_DESC_ROW4" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '		</table>';
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;         
		
	}       
	//--------------------------------------function table with text boxes ------------------//
	function html_draw_appendixes($name, $parameters, $id=0) { //-------------------------//---------------------//----------------- Craig Wood test---------
		$s="";

		$s = '<div id="AppendicesBoxesGroup">';
		$s .= '	<input type="button" id="appendixBtn" class="btn btn-info btn-xs active" value="Show/Hide Appendixes" onClick="$(\'#AppendicesBoxDiv1\').toggle(); " >';
		$s .= '	<div id="AppendicesBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th>Document Link / url</th><th>Description </th><th>Date</th>';
        $s .= '        </tr>';
        $s .= '        <tr id="trAPX1">';
		$s .= '        		<td style="width:300px">';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_URL_ROW1" maxlenght="500"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td style="width:700px">';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DESC_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '           	<td style="width:200px">';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DATE_ROW1" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trAPX2">';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_URL_ROW2" maxlenght="500"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DESC_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DATE_ROW2" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trAPX3">';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_URL_ROW3" maxlenght="500"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DESC_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DATE_ROW3" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr id="trAPX4">';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_URL_ROW4" maxlenght="500"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DESC_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_APPENDIXES_DATE_ROW4" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
    
        $s .= '		</table>';  
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}
	
	//--------------------------------------function table with text boxes ------------------//
	function html_draw_action_plan($name, $parameters, $id=0) { //-------------------------//---------------------//----------------- Craig Wood test---------
		$s="";

		$s = '<div id="ActionPlanBoxesGroup">';
		$s .= '	<input type="button" id="actionPlanBtn" class="btn btn-info btn-xs active" value="Show/Hide Action Plan" onClick="$(\'#ActionPlanBoxDiv1\').toggle(); " >';
		$s .= '	<div id="ActionPlanBoxDiv1" style="display:none">';
        $s .= '		<table class="imagetable">';
        $s .= '        <tr>';
	    $s .= '            <th> Ref# </th><th>Recommended Action</th> <th>Start Date</th><th>Date Complete or Estimated Completion.</th><th>By Whom</th>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td style="width:100px">';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW1" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td style="width:570px">';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW1" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '           	<td style="width:150px">';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW1" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		 $s .= '           	<td style="width:150px">';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW1" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		 $s .= '           	<td style="width:200px">';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW1"maxlenght="200" ></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW2" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW2" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW2"maxlenght="50" ></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW2" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW2" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW3" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW3" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW3" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW3" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW3" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW4" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW4" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW4" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW4" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW4" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW5" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW5" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW5" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW5" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW5" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW6" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW6" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW6" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW6" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW6" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW7" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW7" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW7" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW7" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW7" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW8" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW8" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW8" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW8" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW8" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW9" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW9" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW9" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW9" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW9" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';
        $s .= '        <tr>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_REF_ROW10" maxlenght="50"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_RECOMMENDED_ROW10" maxlenght="2000"></textarea>';
        $s .= '        		</td>';
        $s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_STARTDATE_ROW10" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_ENDDATE_ROW10" maxlenght="50"></textarea>';
        $s .= '        		</td>';
		$s .= '        		<td>';
		$s .= '					<textarea rows="2" cols="800" id="_ACTION_PLAN_BYWHOM_ROW10" maxlenght="200"></textarea>';
        $s .= '        		</td>';
        $s .= '        </tr>';

	
	
	
        $s .= '		</table>';  
		
		$s .= '	</div>';
		$s .= '</div>';
		return $s;
		
	}
	/*
	+---------------------------------------------------------------------------
	|	Blank Road
	+---------------------------------------------------------------------------
	*/	
	function html_draw_line_break($name, $parameters, $id=0) {
		$s="<br/>";
		return $s;
	}	
?>