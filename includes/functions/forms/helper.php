<?php
/*
+--------------------------------------------------------------------------
|   helper.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for custom elements such as employee lookup field or site lookup dropdown
+---------------------------------------------------------------------------
*/
/*
+---------------------------------------------------------------------------
|	Output a helper function to get employee details
+---------------------------------------------------------------------------
*/
    function html_draw_employee_helper_with_approval_rejection($name, $value, $parameters = '', $xtra_parameters = ''){
		$s = html_get_employee_helper($name, $value, $parameters, $xtra_parameters, true, true);
		$s .= '&nbsp;<button id="btnApprove_' . $name . '" type="button" class="btn btn-success btn-xs" style="width:100px;" onclick="javascript:submitApprove(\''.$name.'\');" >Approve</button>&nbsp;';
		$s .= '&nbsp;<button id="btnReject_' . $name . '" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:red" onclick="javascript:submitReject(\''.$name.'\');" >Reject</button>';
		$s .= '<br\>';
		$s .= '&nbsp;<button id="btnClear_' . $name . '" type="button" class="btn btn-warning btn-xs" style="width:100px;" onclick="javascript:clearWFLfields(\''.$name.'\');" >Clear</button>';
		//$s .= '&nbsp;<button id="btnSaveSend_' . $name . '" type="button" class="btn btn-primary btn-xs" style="width:100px;" onclick="javascript:saveAndSend();" >Save and Send</button>&nbsp;';
		return $s;
	}
    function html_draw_employee_helper_with_approval_rejection_icam($name, $value, $parameters = '', $xtra_parameters = ''){
		$s = html_get_employee_helper($name, $value, $parameters, $xtra_parameters, true, true);
		$s .= '&nbsp;<button id="btnApproveIcam_' . $name . '" type="button" class="btn btn-success btn-xs" style="width:100px; " onclick="javascript:submitApprove(\''.$name.'\');" >Approve</button>&nbsp;';
		$s .= '&nbsp;<button id="btnRejectIcam_' . $name . '" type="button" class="btn btn-info btn-xs" style="width:100px; background-color:red" onclick="javascript:submitReject(\''.$name.'\');" >Reject</button>';
		$s .= '<br\>';
		$s .= '&nbsp;<button id="btnClearIcam_' . $name . '" type="button" class="btn btn-warning btn-xs" style="width:100px;" onclick="javascript:clearWFLfields(\''.$name.'\');" >Clear</button>';
		//$s .= '&nbsp;<button id="btnSaveSendIcam_' . $name . '" type="button" class="btn btn-primary btn-xs" style="width:100px;" onclick="javascript:saveAndSend();" >Save and Send</button>&nbsp;';
		return $s;
	}

	
	
	function html_draw_employee_helper_linked($name, $value, $parameters = '', $linked = ''){
		$attribs = "'SITE_ID','SITE_ID_text','DEPARTMENT_ID','DEPARTMENT_ID_text','SECTION_ID','SECTION_ID_text','POSITION_ID','POSITION_ID_text'";
		return html_draw_employee_helper($name, $value, $parameters, $attribs);
	}
	function html_draw_employee_helper_linked_action_confirm($name, $value, $parameters = '', $linked = ''){
		$attribs = "'SITE_ID','SITE_ID_text','DEPARTMENT_ID','DEPARTMENT_ID_text','SECTION_ID','SECTION_ID_text','POSITION_ID','POSITION_ID_text'";
		return html_draw_employee_helper_action_confirm($name, $value, $parameters, $attribs);
	}
	
	function html_draw_employee_helper($name, $value, $parameters = '', $xtra_parameters = ''){
		return html_get_employee_helper($name, $value, $parameters, $xtra_parameters, true, true);
	}
	function html_draw_employee_helper_action_confirm($name, $value, $parameters = '', $xtra_parameters = ''){
		return html_get_employee_helper_action_confirm($name, $value, $parameters, $xtra_parameters, true, true);
	}
	
	function html_draw_employee_helper_search($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true, $mandatory = false){
		return html_get_employee_helper($name, $value, $parameters, $xtra_parameters, $reinsert_value, $mandatory);
	}
	function html_draw_employee_all_helper($name, $value, $parameters = '', $xtra_parameters = ''){
		return html_get_employee_helper($name, $value, $parameters, $xtra_parameters, true, true);
	}
	
/*
+------------------------------------------------------------------------
|  PCR Employee helpers
+------------------------------------------------------------------------
*/
	function html_draw_pcr_employee_helper_linked($name, $value, $parameters = '', $linked = ''){
		$attribs = "'SITE_ID','SITE_ID_text','DEPARTMENT_ID','DEPARTMENT_ID_text','SECTION_ID','SECTION_ID_text','POSITION_ID','POSITION_ID_text'";
		return html_get_pcr_employee_helper($name, $value, $parameters, $attribs, true, true);
	}
	
/*
+------------------------------------------------------------------------
|  Get Employee NAme and display it in span
	Filtered by the site_id parameter or if site_id = 0 then we use the current logged on users site_id
+------------------------------------------------------------------------
*/	
	function html_draw_employee_display($name, $value, $site_id = 0, $xtra_parameters = ''){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
			$s = $val['TEXT'];
		}else{
			$val = array('TEXT'=>'','ID'=>'');
			$s = "*Not Available*";
		}
		return html_display_value($name,$s);
	}
/*
+------------------------------------------------------------------------
|  Get Employee By Site ID
	Filtered by the site_id parameter or if site_id = 0 then we use the current logged on users site_id
+------------------------------------------------------------------------
*/
	function html_get_site_employee_helper($name, $value, $site_id = 0, $xtra_parameters = ''){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$site_id = ($site_id==0)?$_SESSION['user_details']['site_id']:$site_id;
		$onclick_params = '_helper_show_site_employee(\'get_site_employee_list\',\'users\','.$site_id.',this.previousSibling, this';
		if($xtra_parameters!='') $onclick_params .= ' ,'.$xtra_parameters;
		$onclick_params .= ');';
		
		return html_get_mandatory_helper($name, $val, $onclick_params);
	}
	
/*
+------------------------------------------------------------------------
|  Draw a helper with no extra buttons etc...
	- use this helper for mandatory items
+------------------------------------------------------------------------
*/
	function html_get_mandatory_helper($name, $value, $xtra_parameters = '', $show_clear_button = false){
		if(is_array($value)){
			$val = $value;
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$display_value = $val['TEXT'];
		$real_value = $val['ID'];
		if(preg_match("/(.*)(\[\])$/",$name,$amatch))
			$dname = $amatch[1].'_text'.$amatch[2];
		else
			$dname = $name.'_text';
		
		// format the onclick event handler if needed
		if($xtra_parameters!='') $xtra_parameters = preg_replace("/($\))/","$1;",$xtra_parameters);
		$s = '<input type="hidden" 
		name="'.$name.'" id="'.$name.'" value="'.$real_value.'" /><input type="text" 
		name="'.$dname.'" id="'.$dname.'" value="'.$display_value.'" 
		class="hdfield" onclick="'.$xtra_parameters.'this.blur();"
	 />';
	 	if($show_clear_button) $s .= '<input type="text" class="dfield_clear" title="Clear" onclick="this.previousSibling.value=this.previousSibling.previousSibling.value=\'\';this.blur();"/>';
		return $s;
	}
	
/*
+------------------------------------------------------------------------
|  Incidents employee helpers
+------------------------------------------------------------------------
*/
	function html_draw_injured_employee_helper($name, $value, $parameters = '', $xtra_parameters = ''){
		return html_get_employee_helper($name, $value, $parameters, $xtra_parameters, true, true);
	}
	
	function html_draw_injured_employee_helper_search($name, $value, $parameters = '', $xtra_parameters = ''){
		return html_get_employee_helper($name, $value, $parameters, $xtra_parameters, true, false);
	}
/*
+------------------------------------------------------------------------
|  HTML Table Rows for use in the risk definitions form
+------------------------------------------------------------------------
*/
	function html_risk_definition_helper($type = '1', $category = '0', $parameters = ''){
		global $db;
		//$db->debug=true;
		$categories = array('Consequence', 'Likelihood');
		$sql = "SELECT * FROM View_Risk_Definitions WHERE RiskDefType = $type AND RiskRangeCL = $category ";
		$rs = db_query($sql);
		
		$category_name = $categories[$category];
		$rows = '
		<tr><th colspan="4" align="left">Risk Category: <font color="#00AF00">'.$category_name.'</font></th></tr>
		<tr>
			<th align="left">Definition Title</th>
			<th align="left" colspan="3">Definition Description</th>
		</tr>';
		$i = 0;
		while ($row = $rs->FetchRow()) {
			$i++;
			$rows .= '
		<tr>
			<td class="label"><label>'.$row["RISKRANGEDESC"].'</label></td> 
			<td colspan="3">
				<textarea class="sfield" name="'.$category_name.'_txt['.$row["RISKRANGEORDER"].']" id="'.$category_name.'_txt['.$row["RISKRANGEORDER"].']" cols="70" rows="3" maxlength="250">'.$row["RISKDEFDESC"].'</textarea> 
				<input type="hidden" name="'.$category_name.'['.$row["RISKRANGEORDER"].']" id="'.$category_name.'['.$row["RISKRANGEORDER"].']" value="'.$row["RISKDEFDESC"].'">
				<input type="hidden" name="'.$category_name.'ID['.$row["RISKRANGEORDER"].']" id="'.$category_name.'ID['.$row["RISKRANGEORDER"].']" value="'.$row["RISKDEFID"].'">
			</td>
		</tr>';
		}
		$rows .= '<input type="hidden" name="'.$category_name.'_cnt" value="'.$i.'">';
		return $rows;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a form input field which calls a remote script when the onchange event is triggered
+---------------------------------------------------------------------------
*/	
	
	function html_draw_site_linked($name, $value='', $parameters = '', $department='', $section=''){
		$ret = html_form_draw_site($name, $value, $parameters);
		return $ret;
	}
	
	function html_form_draw_site($name, $value, $parameters = ''){
		if($value>0){
			$description = db_get_field_value("select site_description from tblsite where site_id = $value","site_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, ' style="width:100%;border:none;" disabled=true ').html_draw_hidden_field($name, $value, $parameters);
		return $ret;
	}
	
	function html_form_draw_department($name, $dept_id, $parameters=''){
		if($dept_id>0){
			$sql = "select department_description from tbldepartment where department_id = $dept_id";// and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"department_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' style="width:100%;border:none;" disabled=true ').html_draw_hidden_field($name, $dept_id, $parameters);
		return $ret;
	}
	function html_form_draw_section($name, $section_id, $parameters=''){
		if($section_id>0){// && $dept_id>0 && $site_id>0){
			$sql = "select section_description from tblsection where section_id=$section_id";//department_id = $dept_id and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"section_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' style="width:100%;border:none;" disabled=true ').html_draw_hidden_field($name, $section_id, $parameters);
		return $ret;
	}
	
	function html_form_draw_position($name, $position_id, $parameters=''){//var_dump("html_form_draw_position");
		//if($position_id>0){// && $dept_id>0 && $site_id>0){
			$sql = "select position_description from tblposition where position_id=$position_id";//department_id = $dept_id and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"position_description");
		//}else{
			//$description = "N/A";
		//}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' style="width:100%;border:none;" disabled=true ').html_draw_hidden_field($name, $position_id, $parameters);
		return $ret;
	}
	
	
	function html_get_motd_wysiwyg_editor($name, $value, $parameters=''){
		return html_get_wysiwyg_editor($name, $value, $parameters, 'CATS');
	}
	
	function html_get_form_wysiwyg_editor($name, $value, $parameters=''){
		return html_get_wysiwyg_editor($name, $value, $parameters, 'CATS');
	}
	// build a html editor control
	function html_get_wysiwyg_editor($name, $value, $parameters='', $toolbar_options = 'CATS'){
		
		require_once(CATS_ROOT_PATH."js/ezedit/fckeditor.php") ;
		
		$ed							= new FCKeditor($name) ;
		$ed->BasePath		= WS_PATH . 'js/ezedit/' ;
		$ed->ToolbarSet	= $toolbar_options ;
		$ed->SID				= strip_tags(session_id()) ;
		$ed->Value			= html_entity_decode($value) ;
		$ed->parameters	= $parameters;
		
		return $ed->CreateHtml();

	}
	
	

?>