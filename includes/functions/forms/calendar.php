<?php
/*
+--------------------------------------------------------------------------
|   calendar.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for calendar/date/datetime elements such as a calendar popup chooser
+---------------------------------------------------------------------------
*/

/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with start and end date ranges
+---------------------------------------------------------------------------
*/
	function html_get_input_start_end_dates($date, $date_value, $end_date, $end_date_value, $show_start_lebel = false){
		$s="";
		if($show_start_label) $s .= '<label for="'.$date.'" style="width:auto;display:inline;">Start Date:&nbsp;</label>';
		//$s .= '<input class="dfield" name="'.$date.'" id="'.$date.'" type="text" value="'.$date_value.'" onclick="this.blur();_show_calendar(this);"><label for="'.$end_date.'" style="width:auto;display:inline;"> --> End Date:&nbsp;</label><input class="dfield" name="'.$end_date.'" id="'.$end_date.'" type="text" value="'.$end_date_value.'" " onclick="this.blur();_show_calendar(this);">';
		$s .= html_get_calendar_date_field($date, $date_value,'Y-m-j','yyyy-mm-dd','j-M-Y','dd-mmm-yyyy') . '<label for="'.$end_date.'" style="width:auto;display:inline;">';
		$s .= ' --> End Date:&nbsp;</label>' . html_get_calendar_date_field($end_date, $end_date_value,'Y-m-j','yyyy-mm-dd','j-M-Y','dd-mmm-yyyy', '', ' onchange="check_raw_date_range(this.form.elements[\''.$date.'\'],this.form.elements[\''.$end_date.'\']);" ');
		$s .= '
		<script type="text/javascript">
		EDITOR_VALIDATE += ((EDITOR_VALIDATE!="")?";":"") + "'.$date.'|'.$end_date.';rawdaterange;Start Date|End Date";
		</script>
		';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with start and end date+time ranges
+---------------------------------------------------------------------------
*/
	function html_get_input_start_end_datetimes($start_date, $start_date_value, $end_date, $end_date_value, $alt_start_time = '', $alt_end_time = '', $start_label = 'Start Date', $end_label = 'End Date', $show_start_label = false){
		$s="";
		$t_raw = 'H:i';
		$t_in = 'HH:MM';
		$f_r = 'Y-m-j';
		$f_raw = $f_r.' '.$t_raw;
		$f_i = 'yyyy-mm-dd';
		$f_in = $f_i.' '.$t_in;
		$d_r = 'j-M-Y';
		$d_raw = $d_r.' '.$t_raw;
		$d_i = 'dd-mmm-yyyy';
		$d_in = $d_i.' '.$t_in;
		//$s .= ';'.$start_date_value.':'.$end_date_value;
		$cal_start_time = db_getdate($start_date_value,$timestamp,$t_raw);
		if($cal_start_time=="00:00") $cal_start_time=(empty($alt_start_time))?"09:00":$alt_start_time;
		$start_date_value = db_getdate($start_date_value,$timestamp,$f_r).' '.$cal_start_time;
		$start_date_value = db_getdate($start_date_value,$timestamp,$f_raw);
		
		$cal_end_time = db_getdate($end_date_value,$timestamp,$t_raw);
		if($cal_end_time=="00:00") $cal_end_time=(empty($alt_end_time))?"16:30":$alt_end_time;
		//$s .= ';'.$cal_time.':join the date parts='.db_getdate($end_date_value,$timestamp,$f_r).' '.$cal_end_time;
		$end_date_value = db_getdate($end_date_value,$timestamp,$f_r).' '.$cal_end_time;
		$end_date_value = db_getdate($end_date_value,$timestamp,$f_raw);
		
		$control_start = html_get_calendar_date_field($start_date, $start_date_value, $f_raw,$f_in,$d_raw,$d_in);
		$control_end = html_get_calendar_date_field($end_date, $end_date_value,$f_raw,$f_in,$d_raw,$d_in, '', ' onchange="check_raw_date_range(this.form.elements[\''.$start_date.'\'],this.form.elements[\''.$end_date.'\']);" ');
		
		//$s .= ';start='.$start_date_value.':end='.$end_date_value;
		$s_start_label='';
		if($show_start_label){
			$s .= '<table><tr><td><label for="'.$start_date.'" style="width:auto;display:inline;">'.$start_label.':&nbsp;</label></td>';
			$s .= '</td><td></td><td>';
			$s .= '<label for="'.$end_date.'" style="width:auto;display:inline;">'.$end_label.':&nbsp;</label>';
			$s .= '</td></tr><tr><td>';
			$s .= $control_start.'</td><td>&nbsp;<b>to -></b>&nbsp;</td><td>'.$control_end.'</td></tr></table>';
		}else{
			$s .= $control_start;
			$s .= '<label for="'.$end_date.'" style="width:auto;display:inline;">';
			$s .= ' -> '.$end_label.':&nbsp;</label>';
			$s .= $control_end;
		}
		$s .= '
		<script type="text/javascript">
		EDITOR_VALIDATE += ((EDITOR_VALIDATE!="")?";":"") + "'.$start_date.'|'.$end_date.';rawdaterange;'.$start_label.'|'.$end_label.'";
		</script>
		';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use date
+---------------------------------------------------------------------------
*/
	/* Calendar Control
	|----------------------------------------------------------------------
	| GOVERNMENT INSPECTION FORM
	| - Search
	|   - From
	|   - To
	|
	| AUDITS AND INSPECTIONS
	| - Search
	|   - From
	|   - To
	| - Add
	|   - Inspection Date
	|
	| OTHER RECORDS
	| - Search
	|   - From
	|   - To
	|----------------------------------------------------------------------
	*/
	function html_get_calendar_date_field($name = 'cal_date', $value = '', $parameters = '', $mandatory = false, $format_in = 'Y-m-j', $format = 'yyyy-mm-dd', $display_in = 'j-M-Y', $display = 'dd-mmm-yyyy', $alt_time_value = '', $attributes = '', $timestamp = false, $reinsert_value=true){
		//$value = '2005-05-23 08:30';
		//$format = 'dd-mmm-yyyy' old format
		if(empty($value)){
			$cal_date = '';//date($format_in);
			$cal_display_date = '';//date($display_in);
		}else{
			$cal_date = db_getdate($value,$timestamp,$format_in);
			$cal_display_date = db_getdate($value,$timestamp,$display_in);
		}
		$class_name = (preg_match("/H|M/",$format))?"dtfield":"dfield";
		
		if(preg_match("/(.*)(\[.*\])$/",$name,$amatch))
			$dname = $amatch[1].'_d'.$amatch[2];
		else
			$dname = $name.'_d';
		//$dname=$name.'_d';
		
		//echo("value={$value}<br>cal_date={$cal_date}<br>cal_display_date={$cal_display_date}<br>");
		
		$s = '
		<input class="'.$class_name.'" name="'.$dname.'" id="'.$dname.'" type="text" ';
		
		if ( (isset($_POST[$dname])) && ($reinsert_value == true) ) {
      $s .= ' value="' . main_output_string(stripslashes($_POST[$dname])) . '"';
    } elseif (main_not_null($cal_display_date)) {
      $s .= ' value="' . main_output_string($cal_display_date) . '"';
    } else {
		  $s .= ' value="' . main_output_string($cal_display_date) . '"';
		}
		//echo $cal_date.'<br>'.$value.'<br>';
		//$s .=' onkeypress="return _deny_keypress(this,0,event);" onfocus="_show_calendar(this.form.elements[\''.$name.'\'],\''.$format.'\',\''.$display.'\');" '.$attributes.'>';
		$s .=' onkeypress="return _deny_keypress(this,0,event);" onfocus="_show_calendar(this.nextSibling,\''.$format.'\',\''.$display.'\');" '.$attributes.'>';
		$s .= html_draw_hidden_field($name, $cal_date, $parameters);//		$s .= '<input name="'.$name.'" id="'.$name.'" type="hidden" value="'.$cal_date.'" />';
		if($mandatory==false) $s .= '<input type="text" class="dfield_clear" style="width:25px;" title="Clear Date" onclick="this.form.elements[\''.$name.'\'].value=\'\';this.form.elements[\''.$dname.'\'].value=\'\';this.blur();"/>';
		
		//if($name=="SCHEDULED_DATE"){
		//this.form.elements['REMINDER_DATE'].value = "01/01/2007";
		//this.form.elements['SCHEDULED_DATE'].value='';this.form.elements['SCHEDULED_DATE_d'].value='';this.blur();
		//}
		return $s;
	}
	function html_get_mandatory_calendar_date_field($name, $value, $parameters = '', $mandatory = true){
		return html_get_calendar_date_field($name, $value, $parameters, $mandatory);
	}

	function html_get_calendar_date_field_ver2($name = 'cal_date', $value = '', $parameters = '', $mandatory = false, $format_in = 'Y-m-j', $format = 'yyyy-mm-dd', $display_in = 'j-M-Y', $display = 'dd-mmm-yyyy', $alt_time_value = '', $attributes = '', $timestamp = false, $reinsert_value=true){
		//$value = '2005-05-23 08:30';
		//$format = 'dd-mmm-yyyy' old format
		if(empty($value)){
			$cal_date = '';//date($format_in);
			$cal_display_date = '';//date($display_in);
		}else{
			$cal_date = db_getdate($value,$timestamp,$format_in);
			$cal_display_date = db_getdate($value,$timestamp,$display_in);
		}
		
		if(preg_match("/(.*)(\[.*\])$/",$name,$amatch))
			$dname = $amatch[1].'_d'.$amatch[2];
		else
			$dname = $name.'_d';
		
		
		$s = '
		<input name="'.$dname.'" id="'.$dname.'" type="text" ';
		
		if ( (isset($_POST[$dname])) && ($reinsert_value == true) ) {
		  $s .= ' value="' . main_output_string(stripslashes($_POST[$dname])) . '"';
		} elseif (main_not_null($cal_display_date)) {
		  $s .= ' value="' . main_output_string($cal_display_date) . '"';
		} else {
		  $s .= ' value="' . main_output_string($cal_display_date) . '"';
		}
		$s .= '>';
		
		$s = '<input type="text" value="'. main_output_string($cal_display_date) . '" id="'.$name.'_d" >';
		$s .= '<script>';
		$s .= '$(function(){';
		$s .= '			$("#'.$name.'_d").fdatetimepicker({';
		$s .= '			format: "dd-M-yyyy hh:ii"';
		$s .= '		}); ';
		$s .= '	}); ';
		$s .= '</script>';
	
		$s .= html_draw_hidden_field($name, $cal_date, $parameters);
		
		return $s;
	}

	function html_get_calendar_date_field_notime_ver2($name = 'cal_date', $value = '', $parameters = '', $mandatory = false, $format_in = 'Y-m-j', $format = 'yyyy-mm-dd', $display_in = 'j-M-Y', $display = 'dd-mmm-yyyy', $alt_time_value = '', $attributes = '', $timestamp = false, $reinsert_value=true){
		//$value = '2005-05-23 08:30';
		//$format = 'dd-mmm-yyyy' old format
		if(empty($value)){
			$cal_date = '';//date($format_in);
			$cal_display_date = '';//date($display_in);
		}else{
			$cal_date = db_getdate($value,$timestamp,$format_in);
			$cal_display_date = db_getdate($value,$timestamp,$display_in);
		}
		
		if(preg_match("/(.*)(\[.*\])$/",$name,$amatch))
			$dname = $amatch[1].'_d'.$amatch[2];
		else
			$dname = $name.'_d';
		
		
		$s = '
		<input name="'.$dname.'" id="'.$dname.'" type="text" ';
		
		if ( (isset($_POST[$dname])) && ($reinsert_value == true) ) {
		  $s .= ' value="' . main_output_string(stripslashes($_POST[$dname])) . '"';
		} elseif (main_not_null($cal_display_date)) {
		  $s .= ' value="' . main_output_string($cal_display_date) . '"';
		} else {
		  $s .= ' value="' . main_output_string($cal_display_date) . '"';
		}
		$s .= '>';
		
		$s = '<input type="text" value="'. main_output_string($cal_display_date) . '" id="'.$name.'_d" >';
		$s .= '<script>';
		$s .= '$(function(){';
		$s .= '			$("#'.$name.'_d").fdatetimepicker({';
		$s .= '			format: "dd-M-yyyy"';
		$s .= '		}); ';
		$s .= '	}); ';
		$s .= '</script>';
	
		$s .= html_draw_hidden_field($name, $cal_date, $parameters);
		
		return $s;
	}
	
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use date
+---------------------------------------------------------------------------
*/
	/* Calendar Control
	|----------------------------------------------------------------------
	| GOVERNMENT INSPECTION FORM
	| - Search
	|   - From
	|   - To
	|
	| AUDITS AND INSPECTIONS
	| - Search
	|   - From
	|   - To
	| - Add
	|   - Inspection Date
	|
	| OTHER RECORDS
	| - Search
	|   - From
	|   - To
	|----------------------------------------------------------------------
	*/
	function html_get_action_administrator_calendar_date_field($name = 'cal_date', $value = '', $parameters = '', $mandatory = false, $format_in = 'Y-m-j', $format = 'yyyy-mm-dd', $display_in = 'j-M-Y', $display = 'dd-mmm-yyyy', $alt_time_value = '', $attributes = '', $timestamp = false, $reinsert_value=true){
		//$value = '2005-05-23 08:30';
		//$format = 'dd-mmm-yyyy' old format
		if(empty($value)){
			$cal_date = '';
			$cal_display_date = '';
		}else{
			$cal_date = db_getdate($value,$timestamp,$format_in);
			$cal_display_date = db_getdate($value,$timestamp,$display_in);
		}
		$class_name = (preg_match("/H|M/",$format))?"dtfield":"dfield";
		$dname=$name.'_d';
		$dvalue = '';
		$s = '
		<input class="'.$class_name.'" name="'.$dname.'" id="'.$dname.'" type="text" ';
		
		if ( (isset($_POST[$dname])) && ($reinsert_value == true) ) {
      $dvalue = main_output_string(stripslashes($_POST[$dname]));
    } elseif (main_not_null($cal_display_date)) {
      $dvalue = main_output_string($cal_display_date);
    } else {
		  $dvalue = main_output_string($cal_display_date);
		}
		//echo $cal_date.'<br>'.$value.'<br>';
		$s .= ' value="' . $dvalue . '" olddate="' . $dvalue . '"';
		$s .=' onkeypress="return _deny_keypress(this,0,event);" onfocus="if(return_check_admin()) _show_calendar(this.form.elements[\''.$name.'\'],\''.$format.'\',\''.$display.'\');" onblur="return return_remind_admin(this);" />';
		$s .= html_draw_hidden_field($name, $cal_date, $parameters);//		$s .= '<input name="'.$name.'" id="'.$name.'" type="hidden" value="'.$cal_date.'" />';
		if($mandatory==false) $s .= '<input type="text" class="dfield_clear" style="width:25px;" title="Clear Date" onclick="this.form.elements[\''.$name.'\'].value=\'\';this.form.elements[\''.$dname.'\'].value=\'\';this.blur();"/>';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with time
+---------------------------------------------------------------------------
*/
	function html_get_calendar_time_field($name = 'cal_time', $value = '', $attributes = '', $timestamp = false, $alt_value = ''){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		$cal_time = db_getdate($value,$timestamp,'H:i');
		if($cal_time=="00:00") $cal_time=(empty($alt_value))?"09:00":$alt_value;
		$s = '
		<input title="'.$value.'" class="tfield" style="width:58px" name="'.$name.'" type="text" value="'.$cal_time.'" '.$attributes.'><span class="format">24hr(HH:MM)</span>';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with datetime
+---------------------------------------------------------------------------
*/
	function html_get_calendar_date_time_field($name = 'cal_date', $value = '', $parameters = '', $mandatory = true){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		
		$cal_raw_date = db_getdate($value,false,'Y-m-d H:i');
		
		$s = html_get_calendar_date_field($name, $cal_raw_date, $parameters, $mandatory, 'Y-m-d H:i', 'yyyy-m-d HH:MM',  'j-M-Y H:i', 'dd-mmm-yyyy HH:MM');
		return $s;
	}

	function html_get_calendar_date_time_field_ver2($name = 'cal_date', $value = '', $parameters = '', $mandatory = true){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		
		$cal_raw_date = db_getdate($value,false,'Y-m-d H:i');
		
		$s = html_get_calendar_date_field_ver2($name, $cal_raw_date, $parameters, $mandatory, 'Y-m-d H:i', 'yyyy-m-d HH:MM',  'j-M-Y H:i', 'dd-mmm-yyyy HH:MM');
		return $s;
	}

	function html_get_calendar_date($name = 'cal_date', $value = '', $parameters = '', $mandatory = true){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d');
		
		$cal_raw_date = db_getdate($value,false,'Y-m-d');
		
		$s = html_get_calendar_date_field_notime_ver2($name, $cal_raw_date, $parameters, $mandatory, 'Y-m-d', 'yyyy-m-d',  'j-M-Y', 'dd-mmm-yyyy');
		return $s;
	}

	
?>