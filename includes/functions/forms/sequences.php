<?php
/*
+--------------------------------------------------------------------------
|   sequences.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for calendar/date/datetime elements such as a calendar popup chooser
+---------------------------------------------------------------------------
*/

/*
+---------------------------------------------------------------------------
|	Reviewers Sequences
|	PCR - Reviewers
|		Generic Reviewers Sequences for use in PCR section
|
|
+---------------------------------------------------------------------------
*/
	function html_get_pcr_initial_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_initialreview_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_get_pcr_reviewers_sequence($sql, 'Initial', $site_id);
	}
	function html_display_pcr_initial_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_initialreview_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_display_pcr_reviewers_sequence($sql, 'Initial', $site_id);
	}
	
	function html_get_pcr_area_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_areareview_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_get_pcr_reviewers_sequence($sql, 'Area', $site_id);
	}
	function html_display_pcr_area_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_areareview_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_display_pcr_reviewers_sequence($sql, 'Area', $site_id);
	}
	
	function html_get_pcr_extension_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		// $show = ($RECORD['REVIEW_TYPE']=='Area');comment dev
		$show = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='Area');
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_extension_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_get_pcr_reviewers_sequence($sql, 'Area', $site_id,$show);
	}
	function html_display_pcr_extension_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		// $show = ($RECORD['REVIEW_TYPE']=='Area');comment dev
		$show = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='Area');
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_extension_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_display_pcr_reviewers_sequence($sql, 'Area', $site_id,$show);
	}
	
	function html_get_pcr_signoff_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$type = ($RECORD['CRITICALITY']=='NC')?"Area":"PCRT";
		// $show = ($RECORD['REVIEW_TYPE']=='Area');comment dev
		$show = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='Area');
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_signoff_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_get_pcr_reviewers_sequence($sql, $type, $site_id, $show);
	}
	function html_display_pcr_signoff_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$type = ($RECORD['CRITICALITY']=='NC')?"Area":"PCRT";
		$show = ($RECORD['REVIEW_TYPE']=='Area');
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_signoff_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_display_pcr_reviewers_sequence($sql, $type, $site_id, $show);
	}
	
	function html_get_pcr_pcrt_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$type = "PCRT";
		// $show = ($RECORD['REVIEW_TYPE']=='PCRT Walkthrough');comment dev
		$show = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='PCRT Walkthrough');
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_pcrtreview_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_get_pcr_reviewers_sequence($sql, $type, $site_id, $show);
	}
	function html_display_pcr_pcrt_reviewers_sequence(){
		global $id,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$type = "PCRT";
		// $show = ($RECORD['REVIEW_TYPE']=='PCRT Walkthrough');comment dev
		$show = (isset($RECORD['REVIEW_TYPE'])&&$RECORD['REVIEW_TYPE']=='PCRT Walkthrough');
		$sql = "SELECT detail_id,reviewer_id,TO_CHAR(date_reviewed,'YYYY-MM-DD') as date_reviewed,status,comments,document_loc,required FROM tblpcr_pcrtreview_details WHERE pcr_id = $id AND reviewer_detail_id = ";
		return html_display_pcr_reviewers_sequence($sql, $type, $site_id, $show);
	}
	
	function html_get_pcr_reviewers_sequence($sql, $type, $site_id, $show = true){
		global $id,$db,$RECORD;
		$area_id = $RECORD['AREA_ID'];
		$display_table = ($show)?'block':'none';
		//$db->debug=true;
		//PCR_Id|none,none,NULL|Detail_Id|none,none,NULL|Reviewer_Id|none,none,NULL|Date_Reviewed|',none,NULL|Status|',none,''|Comments|',none,''|Document_Loc|',none,''|Required|none,1,0

		// First Get the list of Reviewers		
		$rs = db_query("SELECT * FROM view_pcr_reviewer_subset WHERE review_type = '$type' AND pcr_id = $id ORDER BY reviewer_type ASC");
		
		//if($rs->EOF){
		//	$rs = db_query("SELECT * FROM VIEW_PCR_REVIEWER_DETAIL WHERE Review_Type = '$type' AND Site_Id = $site_id ORDER BY Reviewer_Type ASC");
		//}
		
		$s = '';
		$arr=array();
		$arri = 0;
		while($row = $rs->FetchRow()){
			$fcnt=$rs->FieldCount();
			
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						//$params['maxlength']=$fld->max_length;
						// remove unwanted characters so it will display correctly
						// in the input field control
						//print($row[$fld->name]."<br>");
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
			// now try and get the specific table details
			$rs2 = db_query($sql.$arr['REVIEWER_DETAIL_ID']);
			if($rs2->EOF){
				// No Record Found - extend the array
				$arr['DETAIL_ID'] = 0;
				$arr['REVIEWER_ID'] = 0;
				$arr['DATE_REVIEWED'] = "";
				$arr['STATUS'] = "";
				$arr['COMMENTS'] = "";
				$arr['DOCUMENT_LOC'] = "";
				$arr['REQUIRED'] = "";
			} else {
				// Record Found - extend the array
				$row2 = $rs2->FetchRow();
				$fcnt2=$rs2->FieldCount();
				for($i=0;$i<$fcnt2;$i++){
					// get field object so we know what we are dealing with
					$fld=$rs2->FetchField($i);
					// get the field type
					$type = $rs2->MetaType($fld->type);
					switch($type){
						case 'D':case 'T': // format the date value
							$arr[$fld->name] = $row2[$fld->name];
							break;
						case 'N':case 'I': // don't really need this one as this is the same as the default case
							$arr[$fld->name] = $row2[$fld->name];
							break;
						case 'C':case 'X': // format text value
							//$arr[$fld->name] = addslashes(($row2[$fld->name]));//htmlentities($row[$fld->name]);
							$arr[$fld->name] = $row2[$fld->name];//htmlentities($row[$fld->name]);
							break;
						default: // default value is simply the value of the field
							$arr[$fld->name] = $row2[$fld->name];
							break;
					}
				}
			}
			
			
			$detail_id = $arr['REVIEWER_DETAIL_ID'];
			//<table id="table_reviewer_'.$detail_id.'" cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1;display:'.$display_table.';">
			
			$s .=	'
			<table width="100%" class="admin" style="background-color:#fff; border-bottom:1px dotted #ccc;">
			<tr>
				<th class="row_reviewer" colspan="4"><span >'.$arr['REVIEWER_TYPE'].'<input type="hidden" name="REVIEWER_DETAIL_ID[]" value="'.$detail_id.'" /></span></th>
			</tr>
			<tr>
				<td class="label"><label>Required</label></td>
				<td>';
				//find out if we need to check the required checkbox
				$required = $arr['REQUIRED'];
				if (empty($required)){
					if($arr['REQUIREMENT'] == "Optional"){
						$required = 0;
					}else{
						$required = 1;
					}
				}
				
				//used to determine if we show the details or not
				$display = ($required == 1)?'':'none';
				
				//used to determine if we give the user write access to the details
				$reviewer = $arr['REVIEWER_ID'];
				if (empty($reviewer)){
					$reviewer = $arr['DEF_REVIEWER_ID'];
				}
				if (empty($reviewer)){
					$reviewer = db_get_one("SELECT Reviewer_Id FROM tblPCR_Reviewer_Name WHERE Detail_Id = $detail_id AND Area_Id = $area_id");
				}
				//the logged on user is either the saved reviewer or the default reviewer for this reviewer type
				$disabled = (($reviewer == $_SESSION['user_details']['user_id'])||(cats_user_is_admin_group()))?'':'disabled="true"';
				$s .= html_draw_yes_no_number_radioset('REQUIRED_'.$arri, $required, $disabled.' onclick="show_approvers(this,\'row_reviewer_'.$detail_id.'\',\''.$arr['REQUIREMENT'].'\')" ');
				$s .= html_draw_hidden_field('REQUIREMENT[]',$arr['REQUIREMENT']);
				$s .= '</td>
				<td class="label"><label>Reviewer Name</label></td>
				<td>';
				//$s .= html_draw_hidden_field('REQUIREMENT[]',$arr['REQUIREMENT']);
				$s .= html_draw_employee_helper('REVIEWER_ID[]',$reviewer, $disabled);
				$s .= '</td>
			</tr>';
			$s .= '
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td class="label"><label>Date Reviewed</label></td>
				<td>'.html_get_calendar_date_field('DATE_REVIEWED[]', $arr['DATE_REVIEWED'], $disabled).'</td>
				<td class="label"><label>Status</label></td>
				<td>'.html_draw_pcr_reviewer_status_radioset('STATUS_'.$arri,$arr['STATUS'], $disabled).'</td>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<th class="row_header" colspan="4"><span >Comments</span></th>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td colspan="4">'.html_draw_textarea_field('COMMENTS_TEXT[]',$arr['COMMENTS'],$disabled.' maxlength="255"').'</td>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<th colspan="4" class="row_header" ><span >Location of Saved Checklist</span></th>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td colspan="4">'.html_get_file_location_field('DOCUMENT_LOC[]',$arr['DOCUMENT_LOC'], $disabled).'</td>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td colspan="4">'.html_draw_hidden_field('DETAIL_ID[]',$arr['DETAIL_ID']).'</td>
			</tr>
			</table>
			';
			$arri += 1;
		} // END:: Loop
		if(empty($s)){
			// Changes for Work Item #12887
			// $pcr = (strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false)?"CMR":"PCR";
			$pcr = "PCR";
			$s .= "<h4>There are no reviewers. You must first create reviewers by going to the Maintain Reviewers screen which can be located within the main $pcr menu.</h4>";
		}
		return $s;
	}
	function html_display_pcr_reviewers_sequence($sql, $type, $site_id, $show = true){
		global $id,$db,$RECORD;
		$area_id = $RECORD['AREA_ID'];
		$display_table = ($show)?'block':'none';
		//$db->debug=true;
		//PCR_Id|none,none,NULL|Detail_Id|none,none,NULL|Reviewer_Id|none,none,NULL|Date_Reviewed|',none,NULL|Status|',none,''|Comments|',none,''|Document_Loc|',none,''|Required|none,1,0

		// First Get the list of Reviewers		
		$rs = db_query("SELECT * FROM view_pcr_reviewer_subset WHERE review_type = '$type' AND pcr_id = $id ORDER BY reviewer_type ASC");
		
		//if($rs->EOF){
		//	$rs = db_query("SELECT * FROM VIEW_PCR_REVIEWER_DETAIL WHERE Review_Type = '$type' AND Site_Id = $site_id ORDER BY Reviewer_Type ASC");
		//}
		
		$s = '';
		$arr=array();
		$arri = 0;
		while($row = $rs->FetchRow()){
			$fcnt=$rs->FieldCount();
			
			for($i=0;$i<$fcnt;$i++){
				// get field object so we know what we are dealing with
				$fld=$rs->FetchField($i);
				// get the field type
				$type = $rs->MetaType($fld->type);
				switch($type){
					case 'D':case 'T': // format the date value
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'N':case 'I': // don't really need this one as this is the same as the default case
						$arr[$fld->name] = $row[$fld->name];
						break;
					case 'C':case 'X': // format text value
						//$params['maxlength']=$fld->max_length;
						// remove unwanted characters so it will display correctly
						// in the input field control
						//print($row[$fld->name]."<br>");
						$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
						break;
					default: // default value is simply the value of the field
						$arr[$fld->name] = $row[$fld->name];
						break;
				}
			}
			// now try and get the specific table details
			$rs2 = db_query($sql.$arr['REVIEWER_DETAIL_ID']);
			if($rs2->EOF){
				// No Record Found - extend the array
				$arr['DETAIL_ID'] = 0;
				$arr['REVIEWER_ID'] = 0;
				$arr['DATE_REVIEWED'] = "";
				$arr['STATUS'] = "";
				$arr['COMMENTS'] = "";
				$arr['DOCUMENT_LOC'] = "";
				$arr['REQUIRED'] = "";
			} else {
				// Record Found - extend the array
				$row2 = $rs2->FetchRow();
				$fcnt2=$rs2->FieldCount();
				for($i=0;$i<$fcnt2;$i++){
					// get field object so we know what we are dealing with
					$fld=$rs2->FetchField($i);
					// get the field type
					$type = $rs2->MetaType($fld->type);
					switch($type){
						case 'D':case 'T': // format the date value
							$arr[$fld->name] = $row2[$fld->name];
							break;
						case 'N':case 'I': // don't really need this one as this is the same as the default case
							$arr[$fld->name] = $row2[$fld->name];
							break;
						case 'C':case 'X': // format text value
							//$arr[$fld->name] = addslashes(($row2[$fld->name]));//htmlentities($row[$fld->name]);
							$arr[$fld->name] = $row2[$fld->name];//htmlentities($row[$fld->name]);
							break;
						default: // default value is simply the value of the field
							$arr[$fld->name] = $row2[$fld->name];
							break;
					}
				}
			}
			
			
			$detail_id = $arr['REVIEWER_DETAIL_ID'];
			//<table id="table_reviewer_'.$detail_id.'" cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1;display:'.$display_table.';">
			
			$s .=	'
			<table width="100%" class="admin" style="background-color:#fff; border-bottom:1px dotted #ccc;">
			<tr>
				<th class="row_reviewer" colspan="4"><span >'.$arr['REVIEWER_TYPE'].'<input type="hidden" name="REVIEWER_DETAIL_ID[]" value="'.$detail_id.'" /></span></th>
			</tr>
			<tr>
				<td class="label"><label>Required</label></td>
				<td>';
				//find out if we need to check the required checkbox
				$required = $arr['REQUIRED'];
				if (empty($required)){
					if($arr['REQUIREMENT'] == "Optional"){
						$required = 0;
					}else{
						$required = 1;
					}
				}
				
				//used to determine if we show the details or not
				$display = ($required == 1)?'':'none';
				
				//used to determine if we give the user write access to the details
				$reviewer = $arr['REVIEWER_ID'];
				if (empty($reviewer)){
					$reviewer = $arr['DEF_REVIEWER_ID'];
				}
				if (empty($reviewer)){
					$reviewer = db_get_one("SELECT Reviewer_Id FROM tblPCR_Reviewer_Name WHERE Detail_Id = $detail_id AND Area_Id = $area_id");
				}
				//the logged on user is either the saved reviewer or the default reviewer for this reviewer type
				$s .= html_display_value('REQUIREMENT[]',$arr['REQUIREMENT']);
				$s .= '</td>
				<td class="label"><label>Reviewer Name</label></td>
				<td>';
				//$s .= html_draw_hidden_field('REQUIREMENT[]',$arr['REQUIREMENT']);
				$s .= html_draw_employee_display('REVIEWER_ID[]',$reviewer);
				$s .= '</td>
			</tr>';
			$s .= '
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td class="label"><label>Date Reviewed</label></td>
				<td>'.html_display_value('DATE_REVIEWED[]', $arr['DATE_REVIEWED']).'</td>
				<td class="label"><label>Status</label></td>
				<td>'.html_display_value('STATUS_'.$arri,$arr['STATUS']).'</td>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<th class="row_header" colspan="4"><span >Comments</span></th>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td colspan="4">'.html_display_value('COMMENTS_TEXT[]',$arr['COMMENTS']).'</td>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<th colspan="4" class="row_header" ><span >Location of Saved Checklist</span></th>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td colspan="4">'.html_get_file_location_value('DOCUMENT_LOC[]',$arr['DOCUMENT_LOC']).'</td>
			</tr>
			<tr id="row_reviewer_'.$detail_id.'" style="display:'.$display.'">
				<td colspan="4">'.html_draw_hidden_field('DETAIL_ID[]',$arr['DETAIL_ID']).'</td>
			</tr>
			</table>
			';
			$arri += 1;
		} // END:: Loop
		if(empty($s)){
			// Changes for Work Item #12887
			// $pcr = (strpos("|CO|CH|",$_SESSION['user_details']['site_code'])!==false)?"CMR":"PCR";
			$pcr = "PCR";
			$s .= "<h4>There are no reviewers. You must first create reviewers by going to the Maintain Reviewers screen which can be located within the main $pcr menu.</h4>";
		}
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Actions Sequences
|	PCR - Actions
|		Generic Actions Sequences for use in PCR section
|
|
+---------------------------------------------------------------------------
*/
	function html_get_pcr_actions_stage1_sequence(){
		return html_get_pcr_actions_sequence('stage1',1);
	}
	function html_display_pcr_actions_stage1_sequence(){
		return html_display_pcr_actions_sequence('stage1',1);
	}
	
	function html_get_pcr_actions_stage2_sequence(){
		return html_get_pcr_actions_sequence('stage2',2);
	}
	function html_display_pcr_actions_stage2_sequence(){
		return html_display_pcr_actions_sequence('stage2',2);
	}
	
	function html_get_pcr_actions_stage3_sequence(){
		return html_get_pcr_actions_sequence('stage3',3);
	}
	function html_display_pcr_actions_stage3_sequence(){
		return html_display_pcr_actions_sequence('stage3',3);
	}
	
	function html_draw_pcr_action_title_helper($name, $value, $stage_id = 1, $mandatory = false){
		global $RECORD;
		$site_id = $RECORD['SITE_ID'];
		if(empty($site_id)) $site_id = 3;
		$onclick_params = '_helper_show_pcr_actions(\'get_pcr_actions_list\',\'pcr\', '.$site_id.', '.$stage_id.', this.previousSibling';
		$onclick_params .= ');';
		$s = html_draw_hidden_field('STAGE_ID[]',$stage_id); // make sure we have the stage id for posting back
		// $s .= html_get_value_helper($name, $value, $parameters, $onclick_params, $mandatory , false, '300'); comment dev
		$s .= html_get_value_helper($name, $value, isset($parameters)?$parameters:'', $onclick_params, $mandatory , false, '300');
		return $s;
	}
	
	function html_get_value_helper($name, $value, $parameters = '', $xtra_parameters = '', $mandatory = false, $show_clear_button = false, $width = '200'){
		// inforce mandatory
		$mand_attribs = ' style="width:90%" onfocus="this.className=\'active\';this.select();" onblur="this.className=\'\';" ';
		if($mandatory){
			$mand_attribs = ' style="width:'.$width.'px;cursor:pointer" onfocus="this.nextSibling.onclick();this.blur();" ';
		}
		// format the onclick event handler if needed
		if($xtra_parameters!='') $xtra_parameters = preg_replace("/($\))/","$1;",$xtra_parameters);
		$s = '<input type="text" 
		name="'.$name.'" id="'.$name.'" value="'.$value.'" 
			'.$mand_attribs.' />';
	 	$s .= '<input type="text" class="hfield" onclick="'.$xtra_parameters.'this.blur();" />';
		return $s;
	}
	
	
	function html_draw_employee_by_site_helper($name, $value, $site_id=0){
		return html_get_site_employee_helper($name, $value, $site_id);
	}
	
	
	function html_draw_pcr_action_confirmed_checkbox($name, $value, $action_id=''){
		if(empty($value)) $value='N';
		$checked = ($value=='Y');
		if($action_id!='' && $checked){
			$s = html_image(WS_ICONS_PATH."success_sml.gif","Action has been confirmed");
		}else{
			$s = html_draw_hidden_field($name."[$action_id]", $value);
			$s .= html_draw_checkbox_field($name."__chk[$action_id]", '', $checked, ' title="Check to Confirm Action is complete" onclick="this.previousSibling.value=((this.checked)?\'Y\':\'N\');" ');
		}
		return $s;
	}
	function html_display_pcr_action_confirmed_checkbox($name, $value, $action_id=''){
		if(empty($value)) $value='N';
		$checked = ($value=='Y');
		if($action_id!='' && $checked){
			$s = html_image(WS_ICONS_PATH."success_sml.gif","Action has been confirmed");
		}
		return $s;
	}
	
	
	function html_draw_raise_now_checkbox_field($name, $value, $parameters){
		if(empty($value)) $value='N';
		$checked = ($value=='Y');
		if(preg_match("/(.*)(\[\])$/",$name,$amatch))
			$dname = $amatch[1].'__chk'.$amatch[2];
		else
			$dname = $name.'__chk';
		$s = html_draw_hidden_field($name, $value);
		$s .= html_draw_checkbox_field($dname, '', $checked, ' onclick="this.previousSibling.value=((this.checked)?\'Y\':\'N\');" ');
		return $s;
	}
	
	
	function html_draw_pcr_action_date_field($name, $value, $stage_id = 1){
		if($stage_id==1){
			$s = html_get_mandatory_calendar_date_field($name, $value);
			$s .= html_draw_hidden_field('DAYS_AFTER_PREV_STAGE[]',''); // add days to keep arrays in sync
			$s .= html_draw_hidden_field('RAISE_NOW[]',''); // add raise now to keep arrays in sync
		}else{
			$s = html_draw_number_field($name, $value);
			$s .= html_draw_hidden_field('DUE_DATE[]',''); // add date to keep arrays in sync
		}
		return $s;
	}

	function html_draw_pcr_action_date_field_ver2($name, $value, $stage_id = 1){
		if($stage_id==1){
			//$s = html_get_calendar_date_field_ver2($name, $value);
			$s = html_get_calendar_date_field_notime_ver2($name, $value);
			$s .= html_draw_hidden_field('DAYS_AFTER_PREV_STAGE[]',''); // add days to keep arrays in sync
			$s .= html_draw_hidden_field('RAISE_NOW[]',''); // add raise now to keep arrays in sync
		}else{
			$s = html_draw_number_field($name, $value);
			$s .= html_draw_hidden_field('DUE_DATE[]',''); // add date to keep arrays in sync
		}
		return $s;
	}	
	
	function html_get_pcr_actions_sequence($name, $stage, $table_parameters = 'cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"'){
		global $db,$RECORD, $id;
		$site_id = $RECORD['SITE_ID'];
		$func_return="";
		
		$props_in = array();
		switch($stage){
			case 1:
				$props_in = array(
					// hidden values
					'ACTION_REGISTER_ID'		=> array('label'=>'Action ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'MANAGED_BY'						=> array('label'=>'Manager', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'ALLOCATED_TO'					=> array('label'=>'Allocated', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'DETAIL_ID'							=> array('label'=>'ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'DAYS_AFTER_PREV_STAGE'	=> array('label'=>'How many days after a Stage is completed', 'value'=>NULL, 'func'=>'html_display_value','hide'=>false),
					// show values
					'ACTION_DESCRIPTION'		=> array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_draw_pcr_action_title_helper', 'params'=>$stage),
					'MANAGED_BY_ID'					=> array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_by_site_helper', 'params'=>$site_id),
					'ALLOCATED_TO_ID'				=> array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_by_site_helper', 'params'=>$site_id),
					'STATUS'								=> array('label'=>'Status', 'value'=>NULL, 'func'=>'html_form_show_hidden'),
					'DUE_DATE'							=> array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_draw_pcr_action_date_field','params'=>$stage),
					'ACTION_CONFIRMED'			=> array('label'=>'Action Confirmed', 'value'=>NULL, 'func'=>'html_draw_pcr_action_confirmed_checkbox','hide'=>true)
				);
				break;
			case 2:
				$props_in = array(
					// hidden values
					'ACTION_REGISTER_ID'		=> array('label'=>'Action ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'MANAGED_BY'						=> array('label'=>'Manager', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'ALLOCATED_TO'					=> array('label'=>'Allocated', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'DETAIL_ID'							=> array('label'=>'ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'DUE_DATE'							=> array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					// show values
					'ACTION_DESCRIPTION'		=> array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_draw_pcr_action_title_helper', 'params'=>$stage),
					'MANAGED_BY_ID'					=> array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_by_site_helper', 'params'=>$site_id),
					'ALLOCATED_TO_ID'				=> array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_by_site_helper', 'params'=>$site_id),
					'STATUS'								=> array('label'=>'Status', 'value'=>NULL, 'func'=>'html_form_show_hidden'),
					'RAISE_NOW'							=> array('label'=>'Raise Now', 'value'=>NULL, 'func'=>'html_draw_raise_now_checkbox_field'), 
					'DAYS_AFTER_PREV_STAGE'	=> array('label'=>'How many days after Stage 1 completed', 'value'=>NULL, 'func'=>'html_draw_pcr_action_date_field_ver2','params'=>$stage),
					'ACTION_CONFIRMED'			=> array('label'=>'Action Confirmed', 'value'=>NULL, 'func'=>'html_draw_pcr_action_confirmed_checkbox','hide'=>true)
				);
				break;
			case 3:
				$props_in = array(
					// hidden values
					'ACTION_REGISTER_ID'		=> array('label'=>'Action ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'MANAGED_BY'						=> array('label'=>'Manager', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'ALLOCATED_TO'					=> array('label'=>'Allocated', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'DETAIL_ID'							=> array('label'=>'ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'DUE_DATE'							=> array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					// show values
					'ACTION_DESCRIPTION'		=> array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_draw_pcr_action_title_helper', 'params'=>$stage),
					'MANAGED_BY_ID'					=> array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_by_site_helper', 'params'=>$site_id),
					'ALLOCATED_TO_ID'				=> array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_by_site_helper', 'params'=>$site_id),
					'STATUS'								=> array('label'=>'Status', 'value'=>NULL, 'func'=>'html_form_show_hidden'),
					'RAISE_NOW'							=> array('label'=>'Raise Now', 'value'=>NULL, 'func'=>'html_draw_raise_now_checkbox_field'),
					'DAYS_AFTER_PREV_STAGE'	=> array('label'=>'How many days after Stage 2 completed', 'value'=>NULL, 'func'=>'html_draw_pcr_action_date_field_ver2','params'=>$stage),
					'ACTION_CONFIRMED'			=> array('label'=>'Action Confirmed', 'value'=>NULL, 'func'=>'html_draw_pcr_action_confirmed_checkbox','hide'=>true)
				);
				
				break;
		}
		$fields = implode(",",array_keys($props_in));
		
		//replace ",DUE_DATE," with ",TO_CHAR(DUE_DATE,'YYYY-MM-DD') as DUE_DATE,"
		$fields = str_replace(",DUE_DATE,", ",TO_CHAR(DUE_DATE,'YYYY-MM-DD') as DUE_DATE,",implode(",",array_keys($props_in)));
		
		if($id>0){
			$sql_in = "select {$fields}, 'delete:javascript:remove_sequence_row(this, ::)' from view_pcr_actions_details where pcr_id = $id and stage_id = $stage";
		}else{
			$sql_in = "select {$fields}, 'delete:javascript:remove_sequence_row(this, ::)' from view_pcr_actions_details where rownum = 1";
		}
		
		//$add_cell_content = ""; // used for extra hidden items where we don't want an extra column
		//$s_add_cell_content = ""; // used for extra hidden items where we don't want an extra column within the add control
		//echo($sql_in);
		if( is_array($props_in) ){

			// create record set
			$rs = db_query($sql_in);

			// begin the return string with a container div and table
			$s = "<table id=\"{$name}_table\" $table_parameters><thead><tr>";//';print_r($arr);
			$ncols = $rs->FieldCount();
			for ($i=0; $i < $ncols; $i++) {	
		
				$field = $rs->FetchField($i);
				// ignore item
				//if($props_in[$field->name]['hide']==true) continue;
				//if(is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])) continue;
				// if(is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])){ comment dev
				if(isset($props_in[$field->name])&&is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])){
					if($props_in[$field->name]['hide']==true) continue;
					else {
						// carry on
						//$s_add_cell_content .= html_draw_hidden_field($field->name.'[]','');
						continue;
					}
				}
				$field_match = preg_replace("/^'(EDIT|DELETE):(.*)/", "$1", $field->name);
				//echo("field_match = $field_match <br>fname = {$field->name}<br>");
				switch($field_match){
					case 'EDIT':
						//$fname = $props_in[$field->name]['label'];
						$col_attribs = ' width="5" ';
						//echo $field->name."<br>";
						$edit_button=explode(":",strtolower($field->name));
						$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
						$fname = '';//$edit_button[0];
						$edit_url = $edit_button[2];
						$edit_id = $edit_button[3];
						if(count($edit_button)>3)
							$edit_close = $edit_button[4];
						else
							$edit_close = '';
						//print_r($edit_button);
						$fname='';
						break;
					case 'DEL': case 'DELETE':
						//$fname = $props_in[$field->name]['label'];
						//print_r($edit_button);
						$col_attribs = ' width="5" ';
						$delete_button=explode(":",strtolower($field->name));
						$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
						$delete_url = $delete_button[2];
						$delete_id = $delete_button[3];
						if(count($delete_button)>3)
							$delete_close = $delete_button[4];
						else
							$delete_close = '';
					
						$fname='';
						break;
					default:
						$fname = $props_in[$field->name]['label'];
						if($field->name == 'ACTION_DESCRIPTION') $col_attribs = ' width="50%" ';
						else $col_attribs = '';
						break;
				} // end::switch fname
				if (strlen($fname)==0) $fname = '&nbsp;';
				
				$thattribs=($col_attribs!='')?$col_attribs:'';
				
				
				$s .= "<th$thattribs>$fname</th>";
			}
			$s .= "</tr></thead>";
			
			$row_attribs='';//' onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';
			$numoffset = isset($rs->fields[0]) ||isset($rs->fields[1]) || isset($rs->fields[2]);
			$i_hilite = 0;
			
			$s .= "<tbody>";
			$srow = "";
			$s_add_row = "";
			$add_row = "";
			$add_row_only = false;
			
			// if we have no records then create sql to get one row so we can build the add item row
			// for dynamic creation via javascript
			if($rs->EOF){
				// strip the where clause from sql_in
				// and create new query to get the first row
				$sql_tmp = preg_replace("/( where )(.*)/i", "$1 rownum = 1 ", $sql_in);
				//echo ($sql_tmp);
				$rs = db_query($sql_tmp);
				$add_row_only = true;
			}
			
			while (!$rs->EOF) {
				
				$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
				$i_hilite++;
				$irows = ($i_hilite-1);
				
				$srow = "<TR valign=top $row_hilite $row_attribs>";
				
				$render_cells = true;
				$table_cells = '';
				
				for ($i=0; $i < $ncols; $i++) {
					if ($i===0) $v=($numoffset) ? $rs->fields[0] : reset($rs->fields);
					else $v = ($numoffset) ? $rs->fields[$i] : next($rs->fields);
					$field = $rs->FetchField($i);
					
					if($field->name == 'ACTION_REGISTER_ID' && !empty($v)){
						//$func = $props_in[$field->name]['func'];
						//$func($field->name.'[]', $v, $parameters)
						$table_cells = '';
						$action_id=$v;//$rs->Fields('ACTION_REGISTER_ID');
						$title=$rs->Fields('ACTION_DESCRIPTION');
						$managed=$rs->Fields('MANAGED_BY');
						$allocated=$rs->Fields('ALLOCATED_TO');
						$status=$rs->Fields('STATUS');
						$date_cells='';
						if($stage>1){
							$date_cells.='<td>'.html_display_value('',$rs->Fields('RAISE_NOW')).'</td>';
							$date_cells.='<td>'.html_display_value('',round($rs->Fields('DAYS_AFTER_PREV_STAGE'),0).":".cats_date_short($rs->Fields('DUE_DATE'))).'</td>';
						}else{
							$date_cells.='<td>'.html_display_value('',cats_date_short($rs->Fields('DUE_DATE'))).'</td>';
						}
						$action_confirmed=html_draw_pcr_action_confirmed_checkbox('ACTION_CONFIRMED',$rs->Fields('ACTION_CONFIRMED'),$action_id);
						
						$table_cells .= "<td><a href=\"javascript:top.edit('actions',$v);\">{$title}</a></td>";
						$table_cells .= "<td>$managed</td>";
						$table_cells .= "<td>$allocated</td>";
						$table_cells .= "<td>$status</td>";
						$table_cells .= $date_cells;
						$table_cells .= "<td>$action_confirmed</td>";
						//$table_cells .= "<td></td>"; // cell for delete/edit button
						$render_cells = false;
					}
					
					
					
					// ignore item
					//echo ($props_in[$field->name].":".$field_match);
					// if(is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])){comment dev
					if(isset($props_in[$field->name])&&is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])){
						if($props_in[$field->name]['hide']==true) continue;
						else {
							// carry on
							//if($render_cells) $add_cell_content .= html_draw_hidden_field($field->name.'[]','');
							continue;
						}
					}
					
					// check for action button
					$field_match = preg_replace("/^'(EDIT|DELETE):(.*)/", "$1", $field->name);
					//echo("field_match = $field_match <br>fname = {$field->name}<br>");
					switch($field_match){
						case 'EDIT':
							$col_attribs = ' width="5" ';
							$edit_button=explode(":",$v);
							$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
							$edit_url = $edit_button[2];
							$edit_id = $edit_button[3];
							if(count($edit_button)>3)
								$edit_close = $edit_button[4];
							else
								$edit_close = '';
							//if($render_cells) $table_cells .= "	<TD width=5><a href='" . $edit_protocol . $edit_url . $rs->Fields($edit_id) . $edit_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[0]) . "_bg.gif' border='0' /></a></TD>\n";
							if($render_cells) $cell_content = "<a href='" . $edit_protocol . $edit_url . $rs->Fields($edit_id) . $edit_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[0]) . "_bg.gif' border='0' /></a>";
							break;
						case 'DEL': case 'DELETE':
							$col_attribs = ' width="5" ';
							$delete_button=explode(":",$v);
							$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
							$delete_url = $delete_button[2];
							$delete_id = $delete_button[3];
							if(count($delete_button)>3)
								$delete_close = $delete_button[4];
							else
								$delete_close = '';
							if($delete_protocol!=''){
								//if($render_cells) $table_cells .= "<TD width=5><div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div></TD>";
								if($render_cells) $cell_content = "<div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div>";
								if($add_row=='') $s_add_row .= "<TD width=5><div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div></TD>";
							}else{
								//if($render_cells) $table_cells .= "<TD width=5><input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" /></TD>";
								if($render_cells) $cell_content = "<input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" />";
							}
							break;
						default:
							$v = trim($v);
							if (array_key_exists('func', $props_in[$field->name])) 
								$func = $props_in[$field->name]['func'];
							else
								$func = "html_draw_input_field";
							$parameters = '';
							if (array_key_exists('params', $props_in[$field->name])) 
								$parameters = $props_in[$field->name]['params'];
							// if the style attribute isn't set then set the control to 100% width
							//if(strpos($params," style=")===false) $parameters .= ' style="width:100%;" ';
							//if($render_cells) $table_cells .= "<TD>". $func($field->name.'[]', $v, $parameters) ."</TD>";
							if($render_cells) $cell_content = $func($field->name.'[]', $v, $parameters);
							if($add_row=='') $s_add_row .= "<TD>". $func($field->name."[]", '', $parameters) ."</TD>";
							break;
					} // end::switch fname
					if($render_cells) $table_cells .= "<TD>". $cell_content . "</TD>";
				} // for
				
				$srow .= $table_cells . "</TR>";
				if($add_row=='') $add_row .= $s_add_row . "</TR>";
				$rows = 0 ;//add dev
				if(!$add_row_only) $s .= $srow;
				$rows += 1;
				//$add_cell_content = '';
		
				$rs->MoveNext();
			
			} // while
			//$add_row = $s_add_row;
			$s .= "</tbody></table>";
			$s .= "<table id=\"{$name}_add\" style='display:none;'>";
			$s .= $add_row;
			$s .= "</table>";
			$s .= '<div id="'.$name.'_controller" style="background-image:url('.WS_STYLE_PATH.'images/toolbar_bg.jpg)"><input type="button" name="cats::btn_'.$name.'_add" value="Add '.$name.'" onclick="add_sequence_row(this, \''.$name.'\');" /></div>';
			//$s .= '</td></tr></tfoot></table>';
			
		} // props_in is array
		
		return $s;
	}
	function html_display_pcr_actions_sequence($name, $stage, $table_parameters = 'cellspacing="0" cellpadding="5" border="0" class="admin" width="100%" style="background: #fff;border-color:#e1e1e1"'){
		global $db,$RECORD, $id;
		$site_id = $RECORD['SITE_ID'];
		$func_return="";
		
		$props_in = array();
		switch($stage){
			case 1:
				$props_in = array(
					// hidden values
					'ACTION_REGISTER_ID'		=> array('label'=>'Action ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'MANAGED_BY'						=> array('label'=>'Manager', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'ALLOCATED_TO'					=> array('label'=>'Allocated', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'DETAIL_ID'							=> array('label'=>'ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'DAYS_AFTER_PREV_STAGE'	=> array('label'=>'How many days after a Stage is completed', 'value'=>NULL, 'func'=>'html_display_value','hide'=>false),
					// show values
					'ACTION_DESCRIPTION'		=> array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>$stage),
					'MANAGED_BY_ID'					=> array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'params'=>$site_id),
					'ALLOCATED_TO_ID'				=> array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'params'=>$site_id),
					'STATUS'								=> array('label'=>'Status', 'value'=>NULL, 'func'=>'html_form_show_hidden'),
					'DUE_DATE'							=> array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value','params'=>$stage),
					'ACTION_CONFIRMED'			=> array('label'=>'Action Confirmed', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true)
				);
				break;
			case 2:
				$props_in = array(
					// hidden values
					'ACTION_REGISTER_ID'		=> array('label'=>'Action ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'MANAGED_BY'						=> array('label'=>'Manager', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'ALLOCATED_TO'					=> array('label'=>'Allocated', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'DETAIL_ID'							=> array('label'=>'ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'DUE_DATE'							=> array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					// show values
					'ACTION_DESCRIPTION'		=> array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>$stage),
					'MANAGED_BY_ID'					=> array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'params'=>$site_id),
					'ALLOCATED_TO_ID'				=> array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'params'=>$site_id),
					'STATUS'								=> array('label'=>'Status', 'value'=>NULL, 'func'=>'html_form_show_hidden'),
					'RAISE_NOW'							=> array('label'=>'Raise Now', 'value'=>NULL, 'func'=>'html_display_value'), 
					'DAYS_AFTER_PREV_STAGE'	=> array('label'=>'How many days after Stage 1 completed', 'value'=>NULL, 'func'=>'html_display_value','params'=>$stage),
					'ACTION_CONFIRMED'			=> array('label'=>'Action Confirmed', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true)
				);
				break;
			case 3:
				$props_in = array(
					// hidden values
					'ACTION_REGISTER_ID'		=> array('label'=>'Action ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'MANAGED_BY'						=> array('label'=>'Manager', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'ALLOCATED_TO'					=> array('label'=>'Allocated', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					'DETAIL_ID'							=> array('label'=>'ID', 'value'=>NULL, 'func'=>'html_draw_hidden_field','hide'=>true),
					'DUE_DATE'							=> array('label'=>'Due Date', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true),
					// show values
					'ACTION_DESCRIPTION'		=> array('label'=>'Action Title', 'value'=>NULL, 'func'=>'html_display_value', 'params'=>$stage),
					'MANAGED_BY_ID'					=> array('label'=>'Managed By', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'params'=>$site_id),
					'ALLOCATED_TO_ID'				=> array('label'=>'Allocated To', 'value'=>NULL, 'func'=>'html_draw_employee_display', 'params'=>$site_id),
					'STATUS'								=> array('label'=>'Status', 'value'=>NULL, 'func'=>'html_form_show_hidden'),
					'RAISE_NOW'							=> array('label'=>'Raise Now', 'value'=>NULL, 'func'=>'html_display_value'),
					'DAYS_AFTER_PREV_STAGE'	=> array('label'=>'How many days after Stage 2 completed', 'value'=>NULL, 'func'=>'html_display_value','params'=>$stage),
					'ACTION_CONFIRMED'			=> array('label'=>'Action Confirmed', 'value'=>NULL, 'func'=>'html_display_value','hide'=>true)
				);
				
				break;
		}
		$fields = implode(",",array_keys($props_in));
		
		//replace ",DUE_DATE," with ",TO_CHAR(DUE_DATE,'YYYY-MM-DD') as DUE_DATE,"
		$fields = str_replace(",DUE_DATE,", ",TO_CHAR(DUE_DATE,'YYYY-MM-DD') as DUE_DATE,",implode(",",array_keys($props_in)));
		
		if($id>0){
			$sql_in = "select {$fields} from view_pcr_actions_details where pcr_id = $id and stage_id = $stage";
		}else{
			$sql_in = "select {$fields} from view_pcr_actions_details where rownum = 1";
		}
		
		//$add_cell_content = ""; // used for extra hidden items where we don't want an extra column
		//$s_add_cell_content = ""; // used for extra hidden items where we don't want an extra column within the add control
		//echo($sql_in);
		if( is_array($props_in) ){

			// create record set
			$rs = db_query($sql_in);

			// begin the return string with a container div and table
			$s = "<table id=\"{$name}_table\" $table_parameters><thead><tr>";//';print_r($arr);
			$ncols = $rs->FieldCount();
			for ($i=0; $i < $ncols; $i++) {	
		
				$field = $rs->FetchField($i);
				// ignore item
				//if($props_in[$field->name]['hide']==true) continue;
				//if(is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])) continue;
				if(is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])){
					if($props_in[$field->name]['hide']==true) continue;
					else {
						// carry on
						//$s_add_cell_content .= html_draw_hidden_field($field->name.'[]','');
						continue;
					}
				}
				$field_match = preg_replace("/^'(EDIT|DELETE):(.*)/", "$1", $field->name);
				//echo("field_match = $field_match <br>fname = {$field->name}<br>");
				switch($field_match){
					case 'EDIT':
						//$fname = $props_in[$field->name]['label'];
						$col_attribs = ' width="5" ';
						//echo $field->name."<br>";
						$edit_button=explode(":",strtolower($field->name));
						$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
						$fname = '';//$edit_button[0];
						$edit_url = $edit_button[2];
						$edit_id = $edit_button[3];
						if(count($edit_button)>3)
							$edit_close = $edit_button[4];
						else
							$edit_close = '';
						//print_r($edit_button);
						$fname='';
						break;
					case 'DEL': case 'DELETE':
						//$fname = $props_in[$field->name]['label'];
						//print_r($edit_button);
						$col_attribs = ' width="5" ';
						$delete_button=explode(":",strtolower($field->name));
						$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
						$delete_url = $delete_button[2];
						$delete_id = $delete_button[3];
						if(count($delete_button)>3)
							$delete_close = $delete_button[4];
						else
							$delete_close = '';
					
						$fname='';
						break;
					default:
						$fname = $props_in[$field->name]['label'];
						if($field->name == 'ACTION_DESCRIPTION') $col_attribs = ' width="50%" ';
						else $col_attribs = '';
						break;
				} // end::switch fname
				if (strlen($fname)==0) $fname = '&nbsp;';
				
				$thattribs=($col_attribs!='')?$col_attribs:'';
				
				
				$s .= "<th$thattribs>$fname</th>";
			}
			$s .= "</tr></thead>";
			
			$row_attribs='';//' onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);" ';
			$numoffset = isset($rs->fields[0]) ||isset($rs->fields[1]) || isset($rs->fields[2]);
			$i_hilite = 0;
			
			$s .= "<tbody>";
			$srow = "";
			$s_add_row = "";
			$add_row = "";
			$add_row_only = false;
			
			// if we have no records then create sql to get one row so we can build the add item row
			// for dynamic creation via javascript
			if($rs->EOF){
				// strip the where clause from sql_in
				// and create new query to get the first row
				$sql_tmp = preg_replace("/( where )(.*)/i", "$1 rownum = 1 ", $sql_in);
				//echo ($sql_tmp);
				$rs = db_query($sql_tmp);
				$add_row_only = true;
			}
			
			while (!$rs->EOF) {
				
				$row_hilite = ($i_hilite % 2)?" bgcolor='#eeeeee' ":" bgcolor='#ffffff' ";
				$i_hilite++;
				$irows = ($i_hilite-1);
				
				$srow = "<TR valign=top $row_hilite $row_attribs>";
				
				$render_cells = true;
				$table_cells = '';
				
				for ($i=0; $i < $ncols; $i++) {
					if ($i===0) $v=($numoffset) ? $rs->fields[0] : reset($rs->fields);
					else $v = ($numoffset) ? $rs->fields[$i] : next($rs->fields);
					$field = $rs->FetchField($i);
					
					if($field->name == 'ACTION_REGISTER_ID' && !empty($v)){
						//$func = $props_in[$field->name]['func'];
						//$func($field->name.'[]', $v, $parameters)
						$table_cells = '';
						$action_id=$v;//$rs->Fields('ACTION_REGISTER_ID');
						$title=$rs->Fields('ACTION_DESCRIPTION');
						$managed=$rs->Fields('MANAGED_BY');
						$allocated=$rs->Fields('ALLOCATED_TO');
						$status=$rs->Fields('STATUS');
						$date_cells='';
						if($stage>1){
							$date_cells.='<td>'.html_display_value('',$rs->Fields('RAISE_NOW')).'</td>';
							$date_cells.='<td>'.html_display_value('',round($rs->Fields('DAYS_AFTER_PREV_STAGE'),0).":".cats_date_short($rs->Fields('DUE_DATE'))).'</td>';
						}else{
							$date_cells.='<td>'.html_display_value('',cats_date_short($rs->Fields('DUE_DATE'))).'</td>';
						}
						$action_confirmed=html_display_pcr_action_confirmed_checkbox('ACTION_CONFIRMED',$rs->Fields('ACTION_CONFIRMED'),$action_id);
						
						$table_cells .= "<td><a href=\"javascript:top.edit('actions',$v);\">{$title}</a></td>";
						$table_cells .= "<td>$managed</td>";
						$table_cells .= "<td>$allocated</td>";
						$table_cells .= "<td>$status</td>";
						$table_cells .= $date_cells;
						$table_cells .= "<td>$action_confirmed</td>";
						//$table_cells .= "<td></td>"; // cell for delete/edit button
						$render_cells = false;
					}
					
					
					
					// ignore item
					//echo ($props_in[$field->name].":".$field_match);
					if(is_array($props_in[$field->name]) && array_key_exists('hide', $props_in[$field->name])){
						if($props_in[$field->name]['hide']==true) continue;
						else {
							// carry on
							//if($render_cells) $add_cell_content .= html_draw_hidden_field($field->name.'[]','');
							continue;
						}
					}
					
					// check for action button
					$field_match = preg_replace("/^'(EDIT|DELETE):(.*)/", "$1", $field->name);
					//echo("field_match = $field_match <br>fname = {$field->name}<br>");
					switch($field_match){
						case 'EDIT':
							$col_attribs = ' width="5" ';
							$edit_button=explode(":",$v);
							$edit_protocol=($edit_button[1])?$edit_button[1].":":"";
							$edit_url = $edit_button[2];
							$edit_id = $edit_button[3];
							if(count($edit_button)>3)
								$edit_close = $edit_button[4];
							else
								$edit_close = '';
							//if($render_cells) $table_cells .= "	<TD width=5><a href='" . $edit_protocol . $edit_url . $rs->Fields($edit_id) . $edit_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[0]) . "_bg.gif' border='0' /></a></TD>\n";
							if($render_cells) $cell_content = "<a href='" . $edit_protocol . $edit_url . $rs->Fields($edit_id) . $edit_close . "'><img src='" . WS_ICONS_PATH . "btn_" . stripslashes($edit_button[0]) . "_bg.gif' border='0' /></a>";
							break;
						case 'DEL': case 'DELETE':
							$col_attribs = ' width="5" ';
							$delete_button=explode(":",$v);
							$delete_protocol=($delete_button[1])?$delete_button[1].":":"";
							$delete_url = $delete_button[2];
							$delete_id = $delete_button[3];
							if(count($delete_button)>3)
								$delete_close = $delete_button[4];
							else
								$delete_close = '';
							if($delete_protocol!=''){
								//if($render_cells) $table_cells .= "<TD width=5><div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div></TD>";
								if($render_cells) $cell_content = "<div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div>";
								if($add_row=='') $s_add_row .= "<TD width=5><div id=\"b_{$name}\" class=\"delete\" onclick='remove_sequence_row(this)'></div></TD>";
							}else{
								//if($render_cells) $table_cells .= "<TD width=5><input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" /></TD>";
								if($render_cells) $cell_content = "<input type=\"checkbox\" class=\"bfield\" name=\"".CATS_FORM_ACTION_CHECKBOX."\" value=\"".$rs->Fields($delete_id)."\" />";
							}
							break;
						default:
							$v = trim($v);
							if (array_key_exists('func', $props_in[$field->name])) 
								$func = $props_in[$field->name]['func'];
							else
								$func = "html_display_value";
							$parameters = '';
							if (array_key_exists('params', $props_in[$field->name])) 
								$parameters = $props_in[$field->name]['params'];
							// if the style attribute isn't set then set the control to 100% width
							//if(strpos($params," style=")===false) $parameters .= ' style="width:100%;" ';
							//if($render_cells) $table_cells .= "<TD>". $func($field->name.'[]', $v, $parameters) ."</TD>";
							if($render_cells) $cell_content = $func($field->name.'[]', $v, $parameters);
							if($add_row=='') $s_add_row .= "<TD>". $func($field->name."[]", '', $parameters) ."</TD>";
							break;
					} // end::switch fname
					if($render_cells) $table_cells .= "<TD>". $cell_content . "</TD>";
				} // for
				
				$srow .= $table_cells . "</TR>";
				if($add_row=='') $add_row .= $s_add_row . "</TR>";
				if(!$add_row_only) $s .= $srow;
				$rows += 1;
				//$add_cell_content = '';
		
				$rs->MoveNext();
			
			} // while
			//$add_row = $s_add_row;
			$s .= "</tbody></table>";
			$s .= "<table id=\"{$name}_add\" style='display:none;'>";
			$s .= $add_row;
			$s .= "</table>";
			//$s .= '</td></tr></tfoot></table>';
			
		} // props_in is array
		
		return $s;
	}


/*
+---------------------------------------------------------------------------
|	Default Area Reviewers Sequences
|	PCR - Reviewers
|		Area Reviewers Sequence for PCR Reviewers
|
+---------------------------------------------------------------------------
*/

	function html_display_area_reviewers_sequence(){
		global $id,$db,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$detail_id = $RECORD['DETAIL_ID'];
		$display_table = 'block';
		
		//$db->debug=true;

		// First Get the list of Area Reviewers for PCR Reviewer		
		$rs = db_query("SELECT * FROM tblpcr_area WHERE site_id=".$site_id." ORDER BY area_id ASC");

		// Start the Table
		$s .=	'<table width="100%" class="admin" style="background-color:#fff; border-bottom:1px dotted #ccc;">';

		// Populate the Table		
		$arr=array();
		
		while($row = $rs->FetchRow()){
			$fcnt=$rs->FieldCount();
			
			// Populate the temporary array
			for($i=0;$i<$fcnt;$i++){
				$fld=$rs->FetchField($i);
				$arr[$fld->name] = $row[$fld->name];
			}
			
			// now try and get the specific table details
			$arr['EMP_NAME'] = db_get_one("SELECT emp_name FROM view_pcr_defaultnames WHERE detail_id=".$detail_id." AND area_id=".$arr['AREA_ID']);		
			
			$s .= '
			<tr>
				<td class="label"><label>'.$arr['AREA_DESCRIPTION'].'</label></td>
				<td>';
				$s .= html_display_value('EMP_NAME[]',$arr['EMP_NAME']);
				$s .= '</td>
				<td>';
				$s .= html_draw_hidden_field('AREA_ID[]',$arr['AREA_ID']);
				$s .= '</td>
			</tr>';
		}
		
		// If no Area's for Site, then use default one.
		if (sizeof($arr) == 0) {
			$arr['SITE_ID'] = $site_id;
			$arr['AREA_ID'] = 1;
			$arr['AREA_DESCRIPTION'] = "Default Area";

			// now try and get the specific table details
			$arr['EMP_NAME'] = db_get_one("SELECT emp_name FROM view_pcr_defaultnames WHERE detail_id=".$detail_id." AND area_id=".$arr['AREA_ID']);		
			
			$s .= '
			<tr>
				<td class="label"><label>'.$arr['AREA_DESCRIPTION'].'</label></td>
				<td>';
				$s .= html_display_value('EMP_NAME[]',$arr['EMP_NAME']);
				$s .= '</td>
				<td>';
				$s .= html_draw_hidden_field('AREA_ID[]',$arr['AREA_ID']);
				$s .= '</td>
			</tr>';
		}		
		
		// End the Table
		$s .= '</table>';
		
		return $s;
	}

	function html_draw_area_reviewers_sequence(){
		global $id,$db,$RECORD;
		$site_id = $RECORD['SITE_ID'];
		$detail_id = $RECORD['DETAIL_ID'];
		$display_table = 'block';
		//$db->debug=true;
		
		if ($site_id == "")
			return "";

		// First Get the list of Area Reviewers for PCR Reviewer		
		$rs = db_query("SELECT * FROM tblpcr_area WHERE site_id=".$site_id." ORDER BY area_id ASC");

		// Start the Table
		$s = "";//add dev
		$s .=	'<table width="100%" class="admin" style="background-color:#fff; border-bottom:1px dotted #ccc;">';

		// Populate the Table		
		$arr=array();
		$arri = 0;
		while($row = $rs->FetchRow()){
			$fcnt=$rs->FieldCount();
			
			// Populate the temporary array
			for($i=0;$i<$fcnt;$i++){
					$fld=$rs->FetchField($i);
					$arr[$fld->name] = $row[$fld->name];
			}
			
			// now try and get the specific table details
			$arr['REVIEWER_ID'] = db_get_one("SELECT reviewer_id FROM view_pcr_defaultnames WHERE detail_id=".$detail_id." AND area_id=".$arr['AREA_ID']);		
			
			$s .= '
			<tr>
				<td class="label"><label>'.$arr['AREA_DESCRIPTION'].'</label></td>
				<td>';
				$s .= html_draw_employee_helper_linked('REVIEWER_ID[]',$arr['REVIEWER_ID'],'"SITE_ID"');
				$s .= '</td>
				<td colspan="2">';
				$s .= html_draw_hidden_field('AREA_ID[]',$arr['AREA_ID']);
				$s .= '</td>
			</tr>';
		}
		
		// If no Area's for Site, then use default one.
		if (sizeof($arr) == 0) {
			$arr['SITE_ID'] = $site_id;
			$arr['AREA_ID'] = 1;
			$arr['AREA_DESCRIPTION'] = "Default Area";
		
			// now try and get the specific table details
			$arr['REVIEWER_ID'] = db_get_one("SELECT reviewer_id FROM view_pcr_defaultnames WHERE detail_id=".$detail_id." AND area_id=".$arr['AREA_ID']);		
			
			$s .= '
			<tr>
				<td class="label"><label>'.$arr['AREA_DESCRIPTION'].'</label></td>
				<td>';
				//$s .= html_draw_employee_helper_linked('REVIEWER_ID[]',$arr['REVIEWER_ID'],'"SITE_ID"');
				$s .= html_draw_employee_helper('REVIEWER_ID[]',$arr['REVIEWER_ID']);
				$s .= '</td>
				<td colspan="2">';
				$s .= html_draw_hidden_field('AREA_ID[]',$arr['AREA_ID']);
				$s .= '</td>
			</tr>';		
		}
		
		// End the Table
		$s .= '</table>';
		
		return $s;
	}
	
?>