<?php
/*
+--------------------------------------------------------------------------
|   calendar.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for <input> elements of the section type such as radio buttons of checkboxes
+---------------------------------------------------------------------------
*/

/*
|----------------------------------------------------------------------
| Actions Category - drop down list
|----------------------------------------------------------------------
*/

	function html_form_draw_action_reference_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT reftable_description AS ID ".
		       "FROM tblRefTable where reftable_type = 'Action Reference'";
				
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
/*
+---------------------------------------------------------------------------
|	Output a form checkbox field
+---------------------------------------------------------------------------
*/
  function html_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return html_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }
/*
+---------------------------------------------------------------------------
|	Output a form checkbox field with a default value of 1
+---------------------------------------------------------------------------
*/
	function html_form_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
		if(!main_not_null($value)) $value='1';
    return html_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

/*
+---------------------------------------------------------------------------
|	Output a form radio field
+---------------------------------------------------------------------------
*/
  function html_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return html_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }
	
/*
+---------------------------------------------------------------------------
|	Output employee type radioset with employee as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_employee_type_radioset($name, $value = 'Employee', $parameters = ''){
		if(empty($value)) $value = 'Employee';
		$i=0;
		$s = html_draw_radio_field($name, 'Employee', 'Employee'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Employee');($i++);
		$s .= html_draw_radio_field($name, 'Contractor', 'Contractor'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Contractor');($i++);
		return $s;
	}
	

/*
|----------------------------------------------------------------------
| PCR - PCRT Meetings - status radio set
|----------------------------------------------------------------------
*/
	function html_draw_pcr_meetings_status_radioset($name, $value='', $parameters = '') 
	{
		if(empty($value)) $value = 'Planned';
		$i=0;
		$s = html_draw_radio_field($name, 'Planned', 'Planned'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Planned');($i++);
		$s .= html_draw_radio_field($name, 'Held', 'Held'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Held');($i++);
		$s .= html_draw_radio_field($name, 'Cancelled', 'Cancelled'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Cancelled');($i++);
		return $s;
	}

/*  PCR - PCR Type - radio set
|----------------------------------------------------------------------
| PCR
| - New
|----------------------------------------------------------------------
*/
	function html_draw_pcr_type_radioset($name, $value='', $parameters = '') 
	{
		if(empty($value)) $value = 'Permanent';
		$i=0;
		$s = html_draw_radio_field($name, 'Permanent', 'Permanent'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Permanent');($i++);
		$s .= html_draw_radio_field($name, 'Trial', 'Trial'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Trial');($i++);
		$s .= html_draw_radio_field($name, 'Emergency', 'Emergency'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Emergency');($i++);
		return $s;
	}
	
	function html_draw_authority_status_radioset($name, $value='', $parameters = '') 
	{
		global $p;
		$i=0;
		$s = '';
		if($p!='pcr_sign_off'){
			if(empty($value)) $value = 'Approve';
			$s .= html_draw_radio_field($name, 'Approve', 'Approve'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Approve');($i++);
			$s .= html_draw_radio_field($name, 'Reject', 'Reject'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Reject');($i++);
		}else{
			$s .= html_draw_radio_field($name, 'Close', 'Close'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Close');($i++);
			$s .= html_draw_radio_field($name, '', ''==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Leave Open');($i++);
		}
		return $s;
	}
	
	function html_draw_pcr_review_type_radioset($name, $value='', $parameters = '') 
	{
		global $p;
		$i=0;
		$s = '';
		$parameters .= ' onclick="toggle_reviewer_team();" ';
		switch($p){
			case 'pcr_pcrt_review':
				//if(empty($value)) $value = 'PCRT Meeting';
				$s .= html_draw_radio_field($name, 'PCRT Meeting', 'PCRT Meeting'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'PCRT Meeting');($i++);
				$s .= html_draw_radio_field($name, 'PCRT Walkthrough', 'PCRT Walkthrough'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'PCRT Walkthrough');($i++);
				break;
			case 'pcr_extension':
			case 'pcr_sign_off':
				//if(empty($value)) $value = 'PCRT';
				$s .= html_draw_radio_field($name, 'PCRT', 'PCRT'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'PCRT');($i++);
				$s .= html_draw_radio_field($name, 'Area', 'Area'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Area');($i++);
				break;
		}
		return $s;
	}
	
	function html_draw_pcr_sign_off_radioset($name, $value='', $parameters = '') 
	{
		if(empty($value)) $value = 'Approved';
		$i=0;
		$s = html_draw_radio_field($name, 'Approved', 'Approved'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Approve');($i++);
		$s .= html_draw_radio_field($name, 'Cancelled', 'Cancelled'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Cancel');($i++);
		return $s;
	}
	
	function html_draw_approve_reject_radioset($name, $value='', $parameters = '') 
	{
		if(empty($value)) $value = 'Approve';
		$i=0;
		$s = html_draw_radio_field($name, 'Approve', 'Approve'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Approve');($i++);
		$s .= html_draw_radio_field($name, 'Reject', 'Reject'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Reject');($i++);
		return $s;
	}
	
	function html_draw_pcr_action_taken_radioset($name, $value = '', $parameters = ''){
		if(empty($value)) $value = 'Proceed with extension';//Proceed with change,Further information required,Change rejected
		$i=0;
		$s = html_draw_radio_field($name, 'Proceed with change', 'Proceed with change'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Proceed with change');($i++);
		$s .= html_draw_radio_field($name, 'Further information required', 'Further information required'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Further information required');($i++);
		$s .= html_draw_radio_field($name, 'Change rejected', 'Change rejected'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Change rejected');($i++);
		return $s;
	}
	
	function html_draw_pcr_extension_action_taken_radioset($name, $value = '', $parameters = ''){
		if(empty($value)) $value = 'Proceed with extension';//Proceed with extension,Further information required,Change rejected
		$i=0;
		$s = html_draw_radio_field($name, 'Proceed with extension', 'Proceed with extension'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Proceed with extension');($i++);
		$s .= html_draw_radio_field($name, 'Further information required', 'Further information required'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Further information required');($i++);
		$s .= html_draw_radio_field($name, 'Change rejected', 'Change rejected'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Change rejected');($i++);
		return $s;
	}
	
	function html_draw_pcr_reviewer_status_radioset($name, $value = '', $parameters = ''){
		if(empty($value)) $value = 'Proceed';
		$i=0;
		$s = html_draw_radio_field($name, 'Proceed', 'Proceed'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Proceed');($i++);
		$s .= html_draw_radio_field($name, 'Further Info Required', 'Further Info Required'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Further Info Required');($i++);
		$s .= html_draw_radio_field($name, 'Reject', 'Reject'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Reject');($i++);
		return $s;
	}
	
	function html_draw_pcr_due_by_radioset($name, $value = ''){
		//3 months,6 months,12 months
		$i=0;
		$s = html_draw_radio_field($name, '3 months', '3 months'==$value, ' id="'.$name.'['.($i).']" onclick="date_add_month(this,\'DUE_DATE\',3)" ') . html_draw_label($name.'['.($i).']', '3 months');($i++);
		$s .= html_draw_radio_field($name, '6 months', '6 months'==$value, ' id="'.$name.'['.($i).']" onclick="date_add_month(this,\'DUE_DATE\',6)" ') . html_draw_label($name.'['.($i).']', '6 months');($i++);
		$s .= html_draw_radio_field($name, '12 months', '12 months'==$value, ' id="'.$name.'['.($i).']" onclick="date_add_month(this,\'DUE_DATE\',12)" ') . html_draw_label($name.'['.($i).']', '12 months');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output pcr reviewer requirements radioset with Recommended as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_pcr_reviewer_requirement_radioset($name, $value = '', $parameters = ''){
		if(empty($value)) $value = 'Recommended';//Mandatory Recommended Optional
		$i=0;
		$s = html_draw_radio_field($name, 'Mandatory', 'Mandatory'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Mandatory');($i++);
		$s .= html_draw_radio_field($name, 'Recommended', 'Recommended'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Recommended');($i++);
		$s .= html_draw_radio_field($name, 'Optional', 'Optional'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Optional');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a yes/no radioset with no as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_yes_no_radioset($name, $value = 'No', $parameters = ''){
		if(empty($value)) $value = 'No';
		$i=0;
		$s = html_draw_radio_field($name, 'Yes', 'Yes'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Yes');($i++);
		$s .= html_draw_radio_field($name, 'No', 'No'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'No');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a On/Off radioset with no as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_on_off_radioset($name, $value = 'On', $parameters = ''){
		if(empty($value)) $value = 'On';
		$i=0;
		$s = html_draw_radio_field($name, 'On', 'On'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'On');($i++);
		$s .= html_draw_radio_field($name, 'Off', 'Off'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Off');($i++);
		return $s;
	}	
/*
+---------------------------------------------------------------------------
|	Output a yes(1)/no(0) radioset with 0 as the default value
+---------------------------------------------------------------------------
*/		
	function html_draw_yes_no_number_radioset($name, $value = '0', $parameters = ''){
		if(empty($value)) $value = '0';
		$i=0;
		$s = html_draw_radio_field($name, '1', '1'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Yes');($i++);
		$s .= html_draw_radio_field($name, '0', '0'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'No');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a yes/no radioset (values Y/N)
+---------------------------------------------------------------------------
*/	
	function html_draw_y_n_radioset($name, $value, $parameters = ''){
		if(empty($value)) $value = 'N';
		$i=0;
		$s = html_draw_radio_field($name, 'Y', 'Y'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'Yes');($i++);
		$s .= html_draw_radio_field($name, 'N', 'N'==$value, ' id="'.$name.'['.($i).']" '.$parameters) . html_draw_label($name.'['.($i).']', 'No');($i++);
		return $s;
	}
		
/*
+---------------------------------------------------------------------------
|	Output a yes(1)/no(0) radioset with 0 as the default value
+---------------------------------------------------------------------------
*/		
	function html_draw_gender_radioset($name, $value = 'Male'){
		if(empty($value)) $value = 'Male';
		$i=0;
		$s = html_draw_radio_field($name, 'Male', 'Male'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Male');($i++);
		$s .= html_draw_radio_field($name, 'Female', 'Female'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Female');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a yes(1)/no(0) radioset with 0 as the default value
+---------------------------------------------------------------------------
*/
	function html_draw_estimated_cost_radioset($name, $value = ''){
		$i=0;
		$s = html_draw_radio_field($name, 'less than $100', 'less than $100'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'less than $100');($i++);
		$s .= html_draw_radio_field($name, '$100 to $500', '$100 to $500'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', '$100 to $500');($i++);
		$s .= html_draw_radio_field($name, '$500 to $5000', '$500 to $5000'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', '$500 to $5000');($i++);
		$s .= html_draw_radio_field($name, '$5000 to $50,000', '$5000 to $50,000'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', '$5000 to $50,000');($i++);
		$s .= html_draw_radio_field($name, '$50,000 to $500,000', '$50,000 to $500,000'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', '$50,000 to $500,000');($i++);
		$s .= html_draw_radio_field($name, 'more than $500,000', 'more than $500,000'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'more than $500,000');($i++);
		return $s;
	}
	
	function html_get_incident_severity_radioset($name, $value = ''){
		$i=0;
		$s = html_draw_radio_field($name, 'Minor', 'Minor'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Minor');($i++);
		//$s .= html_draw_radio_field($name, 'Externally Reportable', 'Externally Reportable'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Externally Reportable');($i++);
		$s .= html_draw_radio_field($name, 'Significant', 'Significant'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Significant');($i++);
		return $s;
	}
	
	function html_get_incident_substantiated_radioset($name, $value = ''){
		$i=0;
		$s = html_draw_radio_field($name, 'Substantiated', 'Substantiated'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Substantiated');($i++);
		$s .= html_draw_radio_field($name, 'Unsubstantiated', 'Unsubstantiated'==$value, ' id="'.$name.'['.($i).']" ') . html_draw_label($name.'['.($i).']', 'Unsubstantiated');($i++);
		return $s;
	}
	
	function html_get_incident_status_radioset($name, $value='Open') {
		return html_get_status_radioset($name,$value,'TBLINCIDENT_DETAILS');
	}
	
	function html_draw_employee_status_radioset($name, $value='Active') {
		return html_get_status_radioset($name,$value,'TBLEMPLOYEE_DETAILS');
	}
	
	function html_get_status_radioset($name,$value='',$type='',$parameters=''){
		$field = '';
		$sql="select status_title as ID from status_values where status_type='$type'";
		$values = html_db_options($sql, 'ID', 'ID', true);
		for ($i=0, $n=sizeof($values); $i<$n; $i++) {
			$val=main_output_string($values[$i]['id']);
			$txt=main_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;'));
      $field .= html_draw_radio_field($name, $val, $val==$value, ' id="'.$name.'['.($i).']" ');
			$field .= html_draw_label($name.'['.($i).']', $txt);
    }
		return $field;
	}

/*
+---------------------------------------------------------------------------
|	Output pcr reason for change checkbox set with no default value
+---------------------------------------------------------------------------
*/	
	function html_draw_pcr_reason_for_change_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(A.PCRID) checked FROM tblIncidentForm_Checkboxes B, tblPCR_CheckboxValues A WHERE A.PCRID(+) = $id AND A.CheckboxType(+) = 'Reason_For_Request' AND B.Checkbox_Id = A.CheckboxId(+) AND B.Checkbox_Type = 'Reason_For_Request' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Reason_For_Request' ORDER BY Checkbox_Id ASC";
		}
		
		return html_draw_checkbox_values($name, $value, $sql);
		/*
		$i=0;
		if($rs = db_query($sql)){
			$s .= '<table width="100%"><tr>';
			while($arr = $rs->FetchRow()){
				$n = $name.'[]';
				$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
				$s .= '<td>';
				$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="'.$n_id.'"') . html_draw_label($n_id, $arr['CHECKBOX_DESC']);
				$s .= '</td>';
				if(((++$i) % 4)==0) $s .= "</tr><tr>";
			}
			$s .= '</tr></table>';
		}else{
			$s .= "N/A";
		}
		return $s;
		*/
	}

	function html_draw_checkbox_values($name, $value = '', $sql){
		$s="";
		$i=0;
		if($rs = db_query($sql)){
			$s .= '<table width="100%" style="border-bottom:1px dotted #ccc"><tr>';
			while($arr = $rs->FetchRow()){
				$n = $name.'[]';
				$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
				$s .= '<td width="25%">';
				$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="'.$n_id.'"') . html_draw_label($n_id, $arr['CHECKBOX_DESC']);
				$s .= '</td>';
				if(((++$i) % 4)==0) $s .= "</tr><tr>";
			}
			$s .= '</tr></table>';
		}else{
			$s .= "N/A";
		}
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a form checkbox set for the action categories
+---------------------------------------------------------------------------
*/	
	function html_draw_action_category_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(A.ActionId) checked FROM tblIncidentForm_Checkboxes B, tblaction_checkboxvalues A WHERE A.ActionId(+) = $id AND A.CheckboxType(+) = 'ActionCategory' AND B.Checkbox_Id = A.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
		/*
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			$n = $name.'[]';
			$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
			$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="'.$n_id.'"') . html_draw_label($n_id, $arr['CHECKBOX_DESC']);
		}
		return $s;
		*/
	}
	
/*
+---------------------------------------------------------------------------
|	Output a form checkbox set for the incidents form
+---------------------------------------------------------------------------
*/	


function html_draw_incident_life_checkboxset($name, $value = '', $id=0){
	$s="";
	if($id==0){
		if(isset($_GET['id'])) $id = $_GET['id'];
	}
	if($id>0){
		$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'LIFE' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'LIFE' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
	}else{
		$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Life' ORDER BY Checkbox_Id ASC";
	}
	return html_draw_checkbox_values($name, $value, $sql);
}

	function html_draw_incident_category_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'IncidentCategory' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}
	
	
	function html_draw_incident_report_type_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'IncidentReportType' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Report_Type' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Report_Type' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}

/*
+---------------------------------------------------------------------------
|	Output a form checkbox set for the major hazard form
+---------------------------------------------------------------------------
*/	

	function html_draw_hazard_category_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.HazardId) checked FROM tblIncidentForm_Checkboxes B, tblHazard_Checkboxvalues I WHERE I.HazardId(+) = $id AND I.CheckboxType(+) = 'Category' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}
	
/*
+---------------------------------------------------------------------------
|	Output a radioset for the status values on the actions form
+---------------------------------------------------------------------------
*/
	function html_get_action_status_radio_fields($name, $value='Open') {
		return html_get_action_status_radioset($name,$value,'TBLACTION_DETAILS');
	}

/*
+---------------------------------------------------------------------------
|	Output a radioset containing status values for a particular form
| the status_values table contains all the data for the values etc
| the status_type will usually equal the table name of the main table 
| being used on the form
+---------------------------------------------------------------------------
*/
	function html_get_action_status_radioset($name,$value='',$type='',$parameters=''){
		global $db;
		$sql="select * from status_values where status_type='$type'";
		$rs=$db->Execute($sql);
		$v=$rs->GetAll();
		$len=count($v);
		$s='';
		if(empty($value)){
			$value='Open';
		}
		foreach($v as $i=>$row){
			$lbl=$row['STATUS_TITLE'];
			$s.=html_draw_radio_field($name, $lbl, $lbl==$value, ' id="'.$name.'['.$i.']"  onclick="_close_action();" ') . html_draw_label($name.'['.$i.']', $lbl);
		}
		return $s;
	}
/*
+---------------------------------------------------------------------------
|	Output a form set for the investigation page injury details - Craig Wood SRA
+---------------------------------------------------------------------------
*/	

	function html_draw_investigation_TREATMENT_OUTCOME_checkboxset($name, $value = '', $id=0){ //////----------------------------------Craig Wood SRA------------CHECK THIS CODE-----------
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'TREATMENT_OUTCOME' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'TREATMENT_OUTCOME' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'TREATMENT_OUTCOME' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

/*
+---------------------------------------------------------------------------
|	Output a form checkbox set for the investigation - 
+---------------------------------------------------------------------------
*/	

	function html_draw_investigation_ABS_OR_FAILED_DF1_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF1' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF1' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF1' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}	
	
	function html_draw_investigation_ABS_OR_FAILED_DF2_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF2' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF2' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF2' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}	

	function html_draw_investigation_ABS_OR_FAILED_DF3_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF3' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF3' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF3' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_ABS_OR_FAILED_DF4_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF4' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF4' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF4' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
		
	function html_draw_investigation_ABS_OR_FAILED_DF5_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF5' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF5' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF5' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}
	
	function html_draw_investigation_ABS_OR_FAILED_DF6_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF6' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF6' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF6' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}	
	
	
	function html_draw_investigation_ABS_OR_FAILED_DF7_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF7' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF7' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF7' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}	

	
	function html_draw_investigation_ABS_OR_FAILED_DF8_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF8' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF8' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF8' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}	

	function html_draw_investigation_ABS_OR_FAILED_DF9_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF9' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF9' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF9' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_ABS_OR_FAILED_DF10_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF10' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF10' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF10' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_ABS_OR_FAILED_DF11_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF11' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF11' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF11' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_ABS_OR_FAILED_DF12_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF12' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF12' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF12' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_ABS_OR_FAILED_DF13_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF13' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF13' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF13' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_ABS_OR_FAILED_DF14_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF14' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF14' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF14' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			
	
	function html_draw_investigation_ABS_OR_FAILED_DF15_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF15' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF15' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF15' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			


	function html_draw_investigation_ABS_OR_FAILED_DF16_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF16' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF16' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF16' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			

	function html_draw_investigation_ABS_OR_FAILED_DF17_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF17' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF17' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF17' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			

	function html_draw_investigation_ABS_OR_FAILED_DF18_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF18' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF18' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF18' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			
	
	function html_draw_investigation_ABS_OR_FAILED_DF19_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF19' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF19' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF19' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			
	
	function html_draw_investigation_ABS_OR_FAILED_DF20_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF20' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF20' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF20' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			
	
	function html_draw_investigation_ABS_OR_FAILED_DF21_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ABS_OR_FAILED_DF21' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ABS_OR_FAILED_DF21' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ABS_OR_FAILED_DF21' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}			
	
	
	
	
	
	
	
	
	
	
	function html_draw_investigation_IT1_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT1' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT1' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT1' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_IT2_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT2' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT2' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT2' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT3_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT3' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT3' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT3' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT4_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT4' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT4' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT4' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT5_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT5' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT5' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT5' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT6_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT6' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT6' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT6' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT7_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT7' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT7' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT7' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT8_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT8' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT8' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT8' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_IT9_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT9' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT9' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT9' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_IT10_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT10' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT10' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT10' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT11_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT11' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT11' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT11' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_IT12_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT12' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT12' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT12' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT13_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT13' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT13' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT13' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_IT14_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'INDIV_TEAM_IT14' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'INDIV_TEAM_IT14' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'INDIV_TEAM_IT14' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	
	

	function html_draw_investigation_WF1_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF1' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF1' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF1' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF2_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF2' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF2' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF2' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF3_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF3' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF3' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF3' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	

	function html_draw_investigation_WF4_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF4' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF4' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF4' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF5_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF5' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF5' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF5' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF6_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF6' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF6' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF6' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF7_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF7' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF7' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF7' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF8_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF8' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF8' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF8' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF9_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF9' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF9' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF9' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF10_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF10' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF10' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF10' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF11_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF11' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF11' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF11' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF12_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF12' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF12' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF12' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF13_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF13' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF13' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF13' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF14_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF14' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF14' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF14' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF15_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF15' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF15' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF15' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_WF16_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF16' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF16' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF16' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF17_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF17' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF17' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF17' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF18_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF18' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF18' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF18' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF19_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF19' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF19' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF19' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF20_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF20' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF20' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF20' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF21_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF21' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF21' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF21' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF22_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF22' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF22' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF22' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF23_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF23' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF23' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF23' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_WF24_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'WORKFACTOR_WF24' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'WORKFACTOR_WF24' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'WORKFACTOR_WF24' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	
	
	
	function html_draw_investigation_HF1_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF1' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF1' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF1' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF2_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF2' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF2' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF2' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF3_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF3' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF3' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF3' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF4_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF4' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF4' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF4' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF5_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF5' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF5' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF5' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		


	function html_draw_investigation_HF6_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF6' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF6' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF6' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	

	function html_draw_investigation_HF7_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF7' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF7' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF7' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		


	function html_draw_investigation_HF8_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF8' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF8' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF8' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		


	function html_draw_investigation_HF9_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF9' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF9' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF9' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF10_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF10' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF10' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF10' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	
	function html_draw_investigation_HF11_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF11' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF11' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF11' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF12_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF12' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF12' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF12' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF13_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF13' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF13' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF13' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		


	function html_draw_investigation_HF14_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF14' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF14' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF14' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF15_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF15' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF15' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF15' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF16_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF16' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF16' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF16' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF17_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF17' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF17' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF17' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF18_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF18' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF18' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF18' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF19_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF19' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF19' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF19' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF20_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF20' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF20' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF20' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF21_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF21' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF21' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF21' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF22_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF22' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF22' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF22' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF23_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF23' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF23' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF23' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF24_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF24' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF24' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF24' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_HF25_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF25' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF25' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF25' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_HF26_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'HUMANFACTOR_HF26' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'HUMANFACTOR_HF26' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'HUMANFACTOR_HF26' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	
	
	
	
	
	function html_draw_investigation_OrgFactor_HW_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_HW' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_HW' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_HW' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_OrgFactor_TR_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_TR' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_TR' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_TR' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
		
	function html_draw_investigation_OrgFactor_OR_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_OR' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_OR' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_OR' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_CO_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_CO' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_CO' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_CO' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		


	
	function html_draw_investigation_OrgFactor_IG_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_IG' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_IG' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_IG' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_PR_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_PR' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_PR' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_PR' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_MM_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_MM' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_MM' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_MM' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	function html_draw_investigation_OrgFactor_DE_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_DE' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_DE' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_DE' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_RM_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_RM' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_RM' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_RM' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_MC_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_MC' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_MC' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_MC' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_CM_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_CM' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_CM' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_CM' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_OC_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_OC' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_OC' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_OC' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_RI_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_RI' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_RI' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_RI' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_OL_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_OL' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_OL' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_OL' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_VM_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_VM' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_VM' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_VM' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		

	function html_draw_investigation_OrgFactor_MS_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.INVESTIGATIONID) checked FROM tblIncidentForm_Checkboxes B, TBLINVESTIG_CHECKBOXVALUES I WHERE I.INVESTIGATIONID(+) = $id AND I.CheckboxType(+) = 'ORGFACTOR_MS' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'ORGFACTOR_MS' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'ORGFACTOR_MS' ORDER BY Checkbox_Id ASC";
		}
		return html_draw_checkbox_values($name, $value, $sql);
	}		
	
	
?>