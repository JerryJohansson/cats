<?php
/*
+--------------------------------------------------------------------------
|   calendar.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for <input> elements of the section type such as radio buttons of checkboxes
+---------------------------------------------------------------------------
*/

/*
+---------------------------------------------------------------------------
|	Output a form checkbox field
+---------------------------------------------------------------------------
*/
  function html_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return html_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }
/*
+---------------------------------------------------------------------------
|	Output a form checkbox field with a default value of 1
+---------------------------------------------------------------------------
*/
	function html_form_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
		if(!main_not_null($value)) $value='1';
    return html_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

/*
+---------------------------------------------------------------------------
|	Output a form radio field
+---------------------------------------------------------------------------
*/
  function html_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return html_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }
	
/*
+---------------------------------------------------------------------------
|	Output a yes/no radioset with no as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_employee_type_radioset($name, $value = 'Employee', $parameters = ''){
		if(empty($value)) $value = 'Employee';
		$i=0;
		$s = html_draw_radio_field($name, 'Employee', 'Employee'==$value, ' id="id_'.$name.'['.($i).']" '.$parameters) . html_draw_label("id_".$name.'['.($i).']', 'Employee');($i++);
		$s .= html_draw_radio_field($name, 'Contractor', 'Contractor'==$value, ' id="id_'.$name.'['.($i).']" '.$parameters) . html_draw_label("id_".$name.'['.($i).']', 'Contractor');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a yes/no radioset with no as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_yes_no_radioset($name, $value = 'No'){
		if(empty($value)) $value = 'No';
		$i=0;
		$s = html_draw_radio_field($name, 'Yes', 'Yes'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Yes');($i++);
		$s .= html_draw_radio_field($name, 'No', 'No'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'No');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a On/Off radioset with no as the default value
+---------------------------------------------------------------------------
*/	
	function html_draw_on_off_radioset($name, $value = 'On'){
		if(empty($value)) $value = 'On';
		$i=0;
		$s = html_draw_radio_field($name, 'On', 'On'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'On');($i++);
		$s .= html_draw_radio_field($name, 'Off', 'Off'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Off');($i++);
		return $s;
	}	
/*
+---------------------------------------------------------------------------
|	Output a yes(1)/no(0) radioset with 0 as the default value
+---------------------------------------------------------------------------
*/		
	function html_draw_yes_no_number_radioset($name, $value = '0'){
		if(empty($value)) $value = '0';
		$i=0;
		$s = html_draw_radio_field($name, '1', '1'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Yes');($i++);
		$s .= html_draw_radio_field($name, '0', '0'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'No');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a yes(1)/no(0) radioset with 0 as the default value
+---------------------------------------------------------------------------
*/		
	function html_draw_gender_radioset($name, $value = 'Male'){
		if(empty($value)) $value = 'Male';
		$i=0;
		$s = html_draw_radio_field($name, 'Male', 'Male'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Male');($i++);
		$s .= html_draw_radio_field($name, 'Female', 'Female'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Female');($i++);
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a yes(1)/no(0) radioset with 0 as the default value
+---------------------------------------------------------------------------
*/
	function html_draw_estimated_cost_radioset($name, $value = ''){
		$i=0;
		$s = html_draw_radio_field($name, 'less than $100', 'less than $100'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'less than $100');($i++);
		$s .= html_draw_radio_field($name, '$100 to $500', '$100 to $500'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', '$100 to $500');($i++);
		$s .= html_draw_radio_field($name, '$500 to $5000', '$500 to $5000'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', '$500 to $5000');($i++);
		$s .= html_draw_radio_field($name, '$5000 to $50,000', '$5000 to $50,000'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', '$5000 to $50,000');($i++);
		$s .= html_draw_radio_field($name, '$50,000 to $500,000', '$50,000 to $500,000'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', '$50,000 to $500,000');($i++);
		$s .= html_draw_radio_field($name, 'more than $500,000', 'more than $500,000'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'more than $500,000');($i++);
		return $s;
	}
	
	function html_get_incident_severity_radioset($name, $value = ''){
		$i=0;
		$s = html_draw_radio_field($name, 'Minor', 'Minor'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Minor');($i++);
		$s .= html_draw_radio_field($name, 'Externally Reportable', 'Externally Reportable'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Externally Reportable');($i++);
		$s .= html_draw_radio_field($name, 'Significant', 'Significant'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Significant');($i++);
		return $s;
	}
	
	function html_get_incident_substantiated_radioset($name, $value = ''){
		$i=0;
		$s = html_draw_radio_field($name, 'Substantiated', 'Substantiated'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Substantiated');($i++);
		$s .= html_draw_radio_field($name, 'Unsubstantiated', 'Unsubstantiated'==$value, ' id="id_'.$name.'['.($i).']" ') . html_draw_label('id_'.$name.'['.($i).']', 'Unsubstantiated');($i++);
		return $s;
	}
	
	function html_get_incident_status_radioset($name, $value='Open') {
		return html_get_status_radioset($name,$value,'TBLINCIDENT_DETAILS');
	}
	
	function html_draw_employee_status_radioset($name, $value='Active') {
		return html_get_status_radioset($name,$value,'TBLEMPLOYEE_DETAILS');
	}
	
	function html_get_status_radioset($name,$value='',$type='',$parameters=''){
		$field = '';
		$sql="select status_title as ID from status_values where status_type='$type'";
		$values = html_db_options($sql, 'ID', 'ID', true);
		for ($i=0, $n=sizeof($values); $i<$n; $i++) {
			$val=main_output_string($values[$i]['id']);
			$txt=main_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;'));
      $field .= html_draw_radio_field($name, $val, $val==$value, ' id="id_'.$name.'['.($i).']" ');
			$field .= html_draw_label('id_'.$name.'['.($i).']', $txt);
    }
		return $field;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a form checkbox set for the action categories
+---------------------------------------------------------------------------
*/	
	function html_draw_action_category_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(A.ActionId) checked FROM tblIncidentForm_Checkboxes B, tblaction_checkboxvalues A WHERE A.ActionId(+) = $id AND A.CheckboxType(+) = 'ActionCategory' AND B.Checkbox_Id = A.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			$n = $name.'[]';
			$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
			$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="id_'.$n_id.'"') . html_draw_label('id_'.$n_id, $arr['CHECKBOX_DESC']);
		}
		return $s;
	}
	
	function html_draw_incident_category_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'IncidentCategory' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			$n = $name.'[]';
			$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
			$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="id_'.$n_id.'"') . html_draw_label('id_'.$n_id, $arr['CHECKBOX_DESC']);
		}
		return $s;
	}
	function html_draw_incident_report_type_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(I.IncidentId) checked FROM tblIncidentForm_Checkboxes B, tblIncident_Checkboxvalues I WHERE I.IncidentId(+) = $id AND I.CheckboxType(+) = 'IncidentReportType' AND B.Checkbox_Id = I.CheckboxId(+) AND B.Checkbox_Type = 'Report_Type' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Report_Type' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			$n = $name.'[]';
			$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
			$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="id_'.$n_id.'"') . html_draw_label('id_'.$n_id, $arr['CHECKBOX_DESC']);
		}
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Output a radioset for the status values on the actions form
+---------------------------------------------------------------------------
*/
	function html_get_action_status_radio_fields($name, $value='Open') {
		return html_get_action_status_radioset($name,$value,'TBLACTION_DETAILS');
	}

/*
+---------------------------------------------------------------------------
|	Output a radioset containing status values for a particular form
| the status_values table contains all the data for the values etc
| the status_type will usually equal the table name of the main table 
| being used on the form
+---------------------------------------------------------------------------
*/
	function html_get_action_status_radioset($name,$value='',$type='',$parameters=''){
		global $db;
		$sql="select * from status_values where status_type='$type'";
		$rs=$db->Execute($sql);
		$v=$rs->GetAll();
		$len=count($v);
		$s='';
		if(empty($value)){
			$value='Open';
		}
		foreach($v as $i=>$row){
			$lbl=$row['STATUS_TITLE'];
			$s.=html_draw_radio_field($name, $lbl, $lbl==$value, ' id="id_'.$name.'['.$i.']"  onclick="_close_action();" ') . html_draw_label('id_'.$name.'['.$i.']', $lbl);
		}
		return $s;
	}
?>