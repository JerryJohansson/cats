<?php
/*
+--------------------------------------------------------------------------
|   HTML Functions and Output helpers
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html components
+---------------------------------------------------------------------------
*/
require_once("forms/input.php");
require_once("forms/select.php");
require_once("forms/checkbox_radio.php");
require_once("forms/helper.php");
require_once("forms/calendar.php");
// added davids functions
require_once("sql/actions.php");

	/*
	+---------------------------------------------------------------------------
	|	Helper function to filter users access to site filtered elements
	+---------------------------------------------------------------------------
	*/
	function get_user_site_access_filter($type = '', $clause_type='AND'){
		$site_id = $_SESSION['user_details']['site_id'];
		$site_access = $_SESSION['user_details']['site_access'];
		$multi_site_access = false;
		switch($type){
			case 'single':
				$multi_site_access = false;
				break;
			case 'multi':
				$multi_site_access = (bool)!empty($site_access);
				break;
			default:
				$multi_site_access = (bool)!empty($site_access);
				break;
		}
		if($multi_site_access){
			// coment dev
			// if(strpos($multi_sites,"SITE_ID")===false)
			// 	$where = " SITE_ID IN(" . $site_access . ") ";
			// else
			// 	$where = " " . $site_access . " ";

			if(isset($multi_sites)){
				if(strpos($multi_sites,"SITE_ID")===false)
				$where = " SITE_ID IN(" . $site_access . ") ";
			else
				$where = " " . $site_access . " ";
			}
			else
			{
				$where = " SITE_ID IN(" . $site_access . ") ";
			}

		}else{
			$where = " SITE_ID = " . $site_id . " ";
		}
		return " $clause_type $where";
	}
	
	/*
	+---------------------------------------------------------------------------
	|	Helper function returns an employee helper element
	+---------------------------------------------------------------------------
	*/
	function html_get_employee_helper($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true, $mandatory = false){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$dname = $name.'_text';
		//$onclick_params = '_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$dname.'\'';
		$onclick_params = '_helper_show_employee(\'get_employee_list\',\'users\',this.previousSibling.previousSibling, this.previousSibling';
		if($xtra_parameters!='') $onclick_params .= ' ,'.$xtra_parameters;
		$onclick_params .= ');';
		
		return html_get_helper($name, $val, $parameters, $onclick_params, $reinsert_value, $mandatory);
	}
	
	function html_get_employee_helper_action_confirm($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true, $mandatory = false){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$dname = $name.'_text';
		//$onclick_params = '_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$dname.'\'';
		$onclick_params = '_helper_show_employee(\'get_employee_list_action_confirm\',\'users\',this.previousSibling.previousSibling, this.previousSibling';
		if($xtra_parameters!='') $onclick_params .= ' ,'.$xtra_parameters;
		$onclick_params .= '); ';
		
		return html_get_helper($name, $val, $parameters, $onclick_params, $reinsert_value, $mandatory);
	}
	
	function html_get_pcr_employee_helper($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true, $mandatory = false){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$dname = $name.'_text';
		//$onclick_params = '_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$dname.'\'';
		$onclick_params = '_helper_show_pcr_employee(\'get_employee_list\',\'users\',this.previousSibling.previousSibling, this.previousSibling';
		if($xtra_parameters!='') $onclick_params .= ' ,'.$xtra_parameters;
		$onclick_params .= ');';

		return html_get_helper($name, $val, $parameters, $onclick_params, $reinsert_value, $mandatory, true);
	}
	
	function html_get_obligation_responsibilities_helper($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true, $mandatory = false){
		if($value){
			//$sql = "SELECT p.position_id as ID, p.position_description as TEXT FROM tblObligationPositions o, tblposition p WHERE o.Obligation_ID = $value";
			$sql = "SELECT Position_Id AS ID, Position_Description AS TEXT FROM tblPosition WHERE Position_Id = $value";
			//$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		
		$dname = preg_replace("/(\[\])$/","_text$1",$name);//.'_text';
		$onclick_params = '_helper_show_obligations(\'get_obligation_responsibilities_list\',\'obligations\',this.previousSibling.previousSibling, this.previousSibling';
		if($xtra_parameters!='') $onclick_params .= ' ,'.$xtra_parameters;
		$onclick_params .= ');';
		
		return html_get_helper($name, $val, $parameters, $onclick_params, $reinsert_value, true, true, '300');
	}
	
	/*
	+---------------------------------------------------------------------------
	|	Main Helper (helper)function returns a helper object
	+---------------------------------------------------------------------------
	*/
	function html_get_helper($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true, $mandatory = false, $show_clear_button = false, $width = '200'){
		if(is_array($value)){
			$val = $value;
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$display_value = $val['TEXT'];
		$real_value = $val['ID'];
		//$dname = $name.'_text';
		if(preg_match("/(.*)(\[\])$/",$name,$amatch))
			$dname = $amatch[1].'_text'.$amatch[2];
		else
			$dname = $name.'_text';
		
		// re-insert posted value for id element
		if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $real_value = main_output_string(stripslashes($_POST[$name]));
    } elseif (main_not_null($real_value)) {
      $real_value = main_output_string($real_value);
    }
		// re-insert posted value for text element
		if ( (isset($_POST[$dname])) && ($reinsert_value == true) ) {
      $display_value = main_output_string(stripslashes($_POST[$dname]));
    } elseif (main_not_null($display_value)) {
      $display_value = main_output_string($display_value);
    }
		// inforce mandatory
		$mand_attribs = ' style="width:'.$width.'px;" onfocus="this.className=\'active\';this.select();" onblur="this.className=\'\';" ';
		if($mandatory){
			$mand_attribs = ' style="width:'.$width.'px;cursor:pointer" onfocus="this.nextSibling.onclick();this.blur();" ';
		}
		// format the onclick event handler if needed
		if($xtra_parameters!='') $xtra_parameters = preg_replace("/($\))/","$1;",$xtra_parameters);
		$s = '<input type="hidden" 
		name="'.$name.'" id="'.$name.'" value="'.$real_value.'" '.$parameters.' /><input type="text" 
		name="'.$dname.'" id="'.$dname.'" value="'.$display_value.'" 
			'.$mand_attribs.'
	 />';
	 	if($show_clear_button==false) $show_clear_button = ($mandatory==false);
	 	$s .= '<input type="text" id="'.$name.'_btn" class="hfield" onclick="'.$xtra_parameters.'this.blur();" />';
		if($show_clear_button) $s .= '<input type="text" class="dfield_clear" title="Clear" onclick="this.form.elements[\''.$name.'\'].value=\'\';this.form.elements[\''.$dname.'\'].value=\'\';this.blur();"/>';
		return $s;
	}
/*
+---------------------------------------------------------------------------
|	Functions to display form items etc
+---------------------------------------------------------------------------
*/
	/*
	+---------------------------------------------------------------------------
	|	Output a form
	+---------------------------------------------------------------------------
	*/
  function html_draw_form($name, $action, $method = 'post', $parameters = '') {
    $form = '<form name="' . main_output_string($name) . '" action="' . main_output_string($action) . '" method="' . main_output_string($method) . '"';

    if (main_not_null($parameters)) $form .= ' ' . $parameters;

    $form .= '>';

    return $form;
  }
/*
+---------------------------------------------------------------------------
|	Output a form label
+---------------------------------------------------------------------------
*/
  function html_draw_label($name, $value = '', $parameters = '') {
    return '<label class="inline_lbl" for="' . main_output_string($name) . '" ' . $parameters . '>' . main_output_string($value) . '</label>';
  }
	
	function html_form_label($name, $value = '', $parameters = '') {
    return '<label for="' . main_output_string($name) . '" ' . $parameters . '>' . main_output_string($value) . '</label>';
  }

/*
+---------------------------------------------------------------------------
|	Output a selection field - alias function for html_draw_checkbox_field() and html_draw_radio_field()
+---------------------------------------------------------------------------
*/
  function html_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {
    $selection = '<input class="bfield" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '" ';

    if (main_not_null($value)) $selection .= ' value="' . main_output_string($value) . '"';

    if ( ($checked == true) || ( isset($_POST[$name]) && is_string($_POST[$name]) && ( ($_POST[$name] == 'on') || (isset($value) && (stripslashes($_POST[$name]) == $value)) ) ) ) {
      $selection .= ' CHECKED';
    }

    if (main_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= '>';

    return $selection;
  }
	
/*
+---------------------------------------------------------------------------
|	Output a form pull down menu
+---------------------------------------------------------------------------
*/
	
  function html_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false, $empty_option = false) {
    $field = '<select class="sfield" name="' . main_output_string($name) . '" id="' . main_output_string($name) . '"';

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($_POST[$name])) $default = stripslashes($_POST[$name]);
		$empty_text = "";
		if(gettype($empty_option)=='string') $empty_text = $empty_option;
		if($empty_option==true) $field .= '<option>'.$empty_text.'</option>';
		
    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . main_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' SELECTED';
      }

      $field .= '>' . main_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }
	
/*
+------------------------------------------------------------------------
|
|  Function: html_db_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|  Returns: Array in the following format:
|			array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|			(string)||(array)sql = sql query string or array or ADODB_Recordset
|			(string)id = value attribute identifier
|			(string)text = text attribute identifier
|			(bool)ucwords = whether or not to show text as ucwords and replace underscores with space i.e. sheep_are_good in bed -> Sheep Are Good In Bed			
|
+------------------------------------------------------------------------
*/
	function html_db_options($sql, $id='ID', $text='TEXT', $ucwords=true){
		global $db;
		
		if(empty($sql)) return false;
		if(is_array($sql)){
			$arr = $sql;
		}else{ 
			$rs = $db->Execute($sql);
			$arr=$rs->GetAll();
			
		}
		// build array for options function
		$out=array();
		//print_r($arr);
		foreach($arr as $k => $v){
			$key=(is_array($v))?$v[$id]:$v;
			$val=(is_array($v))?$v[$text]:$v;
			$out[]=array("id"=>$key,"text"=>($ucwords?ucwords($val):$val));
		}
		//print_r($out);
		return $out;
	}
	
	
	function html_get_months_options($arr){
		$i = 1;
		$m = array(1=>'January','February','March','April','May','June','July','August','September','October','November','December');
		for($i=1;$i<(count($m)+1);$i++) $arr[] = array("id"=>"$i", "text"=>"{$m[$i]}");
		return $arr;
	}
	
	function html_get_days_of_week_options($arr, $weekend = true){
		$i = 0;
		$days = array('Every Day','Monday','Tuesday','Wednesday','Thursday','Friday');
		if($weekend) $days = array_merge($days, array('Saturday','Sunday'));
		for($i=0;$i<count($days);$i++) $arr[] = array("id"=>"$i", "text"=>"{$days[$i]}");
		return $arr;
	}

/*
+---------------------------------------------------------------------------
|	Return an array to be used as options of a combo
+---------------------------------------------------------------------------
*/	
	function html_get_pull_down_options($sql, $field_value=0, $field_text=1) {		
		// global ADOBD object
		global $db;

		$i=0;
		$ret=array();
		$result = db_query($sql);
		//echo($sql."<br>");
		//print_r($result);
		while($row = db_fetch_array($result))
		{
			$ret[$i]['id'] = $row[$field_value];
			$ret[$i]['text'] = $row[$field_text];
			$i++;
		}
		return $ret;
	}


/*
+------------------------------------------------------------------------
|
|  Function: html_image
|  Purposes: Returns an img tag
|  Returns: html image tag
|  Params: 
|    (string)src = image source
|    (string)alt = alt text
|    (string)width = width of image
|    (string)height = height of image
|    (string)parameters = extra atrributes
|
+------------------------------------------------------------------------
*/
	function html_image($src, $alt = '', $width = '', $height = '', $parameters = '') {
    
// alt is added to the img tag even if it is null to prevent browsers from outputting
// the image filename as default
    $image = '<img src="' . main_output_string($src) . '" border="0" alt="' . main_output_string($alt) . '"';

    if (main_not_null($alt)) {
      $image .= ' title=" ' . main_output_string($alt) . ' "';
    }
/*
    if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true') && (empty($width) || empty($height)) ) {
      if ($image_size = @getimagesize($src)) {
        if (empty($width) && ezw_not_null($height)) {
          $ratio = $height / $image_size[1];
          $width = $image_size[0] * $ratio;
        } elseif (ezw_not_null($width) && empty($height)) {
          $ratio = $width / $image_size[0];
          $height = $image_size[1] * $ratio;
        } elseif (empty($width) && empty($height)) {
          $width = $image_size[0];
          $height = $image_size[1];
        }
      } elseif (IMAGE_REQUIRED == 'false') {
        return false;
      }
    }
*/
    if (main_not_null($width) && main_not_null($height)) {
      $image .= ' width="' . main_output_string($width) . '" height="' . main_output_string($height) . '"';
    }

    if (main_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= '>';

    return $image;
  }
	
/*
+------------------------------------------------------------------------
|
|  Function: html_form_functions_chooser
|  Purposes: Combines function files dd with functions dd
|  Returns: (string)HTML dropdown group that remotes together to filter functions list
|  Params: 
|    (string)name = Name of main dropdown box(functions dd)
|    (variant)value = Value of dropdown box/s ... can be array("selected file value","selected function value")
|    (string)parameters = Element attributes
|
+------------------------------------------------------------------------
*/
	function html_form_functions_chooser($file_value='html_functions.php', $name='funcs', $value='html_draw_input_field', $parameters = ''){
		$files_dd = html_form_function_files_helper($name.'_files', $value, $parameters . 'onchange="remote.get_options_add(\'get_functions_array\',\'admin\',this.options[this.selectedIndex].value,this,this.form.'.$name.');"');
		$ar=explode("_",$value);
		if(is_array($ar))
			$funcs_dd = html_form_functions_helper($ar[0].'_',$name,$value);
		else
			$funcs_dd = html_form_functions_helper(str_replace("functions.php","",$file_value),$name,$value);
		//$func_params = html_draw_input_field($name,$params='');
		return $files_dd.$funcs_dd;
	}
	
	function html_form_function_files_helper($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_function_files_options(), $value, $parameters, false, true);
	}
	
	function html_form_functions_helper($filter, $name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_functions_options($filter), $value, $parameters, false, true);
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_function_files_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|            using defined functions php funciton
|  Returns: Array in the following format:
|    array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|    (string)value = N/A
|
+------------------------------------------------------------------------
*/
	
	function html_form_function_files_options(){
		$out = array();
		$d = dir(CATS_FUNCTIONS_PATH.'forms/');
		while (false !== ($file = $d->read())) {
			 if(strpos(basename($file),'.')) $out[]=array("id" => $file, "text" => str_replace(array("_",".php"),array(" ",""),basename($file)));
		}
		$d->close();
		return $out;
	}
	
	function html_function_files_options($preg_match='functions.php'){
		$arr = get_included_files();
		$out=array();
		
		$match="/($preg_match)/i";
		//echo($match);
		foreach ($arr as $file) if(preg_match($match,$file)) $out[]=array("id" => basename($file), "text" => str_replace(array("_",".php"),array(" ",""),basename($file)));
		return $out;
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_functions_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|            using defined functions php funciton
|  Returns: Array in the following format:
|    array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|    (string)value = N/A
|
+------------------------------------------------------------------------
*/
	function html_form_functions_options($filename='html'){
		$file_name = CATS_FUNCTIONS_PATH.'forms/'.$filename;
		$out=array();
		if(file_exists($file_name)){
			$lines = file($file_name);
			foreach ($lines as $line_num => $line) {
				if(preg_match("/\sfunction (html_)(.*_)+(.*)\(/",$line,$matches)){
					$val = trim($matches[1].$matches[2].$matches[3]);
					$out[] = array("id" => $val, "text" => $val);
				}
			}
		}else{
			$out[]=array("id" => '', "text" => '');
		}
		return $out;
	}
	
	function html_form_all_functions_options($preg_match='html',$ftype='user'){
		$vars_arr = get_defined_functions();
		//print_r($vars_arr);
		$arr = $vars_arr[$ftype];
		$out=array();
		$match="/^($preg_match)/i";
		foreach ($arr as $val) if(preg_match($match,$val)) $out[]=array("id" => $val, "text" => $val);
		return $out;
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_columns_helper
|  Purposes: Dropdown box of columns filtered by $table_value
|  Returns: HTML Select element:
|  Params: 
|    (string)table_value = name of table to filter(defaults to tblaction_details)
|    (string*)name = name of columns select element
|    (string*)value = value of selected column
|    (string)parameters = attributes to add to the element
|
+------------------------------------------------------------------------
*/
	function html_form_columns_helper($table_value='', $name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_columns_options($table_value), $value, $parameters, false, true);
	}
	function html_form_columns_helper_list($table_values, $name, $value, $parameters = ' size=10 ', $default_title = true){
		return html_draw_pull_down_menu($name, $table_values, $value, $parameters, false, $default_title);
	}
	function html_form_columns_checkbox_list($value='tblaction_details', $name = 'chks', $value1 = '', $parameters = ''){
		if(empty($value1)) $value = 'tblaction_details';// default table
		$sql = "select * from $value where rownum = 1"; // select a row from the table
		$s = '';
		$arr = db_get_columns($value); // return columns array
		$out=array();
		$s .= '<table width="100%" style="border-bottom:1px dotted #ccc"><tr>';
		foreach($arr as $key => $val){ // create options array
			$out[]=array("id"=>$val['CNAME'],"text"=>$val['CNAME']);
			
			//while($arr = $rs->FetchRow()){
				$n = $name.'[]';
				$n_id=$name . '['.$val['CNAME'].']';
				$s .= '<td width="25%">';
				$s .= html_draw_checkbox_field($n, $val['CNAME'], 0, ' id="'.$n_id.'"') . html_draw_label($n_id, ucwords(preg_replace("/_/",' ',$val['CNAME'])));
				$s .= '</td>';
				//if(((++$i) % 4)==0) 
					$s .= "</tr><tr>";
			//}
			
		}
		$s .= '</tr></table>';
		// re-create options array
		return $s;
	}
	function html_form_columns_options($value='tblaction_details'){
		if(empty($value)) $value = 'tblaction_details';// default table
		$sql = "select * from $value where rownum = 1"; // select a row from the table
		$arr = db_get_columns($value); // return columns array
		$out=array();
		foreach($arr as $key => $val){ // create options array
			$out[]=array("id"=>$val['CNAME'],"text"=>$val['CNAME']);
		}
		// re-create options array
		return html_db_options($out, $id='id', $text='text', false);
	}
	
	function html_group_type_options($value){
		$sql = "select distinct FIELD_GROUP_TYPE as ID from form_fields";
		return html_db_options($sql, 'ID', 'ID',true);
	}
	
	function html_form_types_options($value){
		$sql = "select id, name as text from form_types";
		return html_db_options($sql, $id='ID', $text='TEXT', true);
	}
	
	function html_form_tables_helper($cols_name = '', $name, $value, $parameters = ''){
		$cols_name=(empty($cols_name))?$name.'_TABLES':$cols_name;
		return html_draw_pull_down_menu($name, html_form_tables_options($value), $value, $parameters . ' onchange="remote.get_options_add(\'get_columns_array\',\'admin\',this.options[this.selectedIndex].value,this,this.form.'.$cols_name.');" ', false, true);
	}
	function html_form_tables_options($value){
		return html_db_options(db_get_cat_details('TABLE', 'TBL'), $id='id', $text='text', true);
	}
	
	
	function html_display_actions_created($name='',$value='',$parameters='', $id=0, $origin=''){
		$total = Get_Created_Actions($id, $origin);
		return html_display_value($name, $total);
	}
	
	function html_display_actions_closed($name='',$value='',$parameters='', $id=0, $origin=''){
		$total = Get_Closed_Actions($id, $origin);
		return html_display_value($name, $total);
	}
	
	$html_config_loader_index=0;
	function html_config_loader($name='',$value='',$parameters=''){
		global $html_config_loader_index;
		if($value=='') return html_draw_input_field($name, $value, ' style="width:100%; background-image:url('.WS_STYLE_PATH.'loading.gif);" ');
		$s = '<img src="'.WS_STYLE_PATH.'loading.gif" id="CONFIGURATION_VALUE['.$html_config_loader_index.']" value="'.$value.'" onload="load_config(this,'.($html_config_loader_index++).')" height="13" />';
		return $s;
	}
	// show a dd of filters or templates currently available for the current page
	function html_filters_templates_dd(){
		return '<select name="cats::templates" id="cats::templates" onchange="_m.saveTemplate(this);"><option></option><option value="Action::New...">New...</option></select>';
	}

?>