<?PHP

/*
|-----------------------------------------------
| REFERENCE TABLE
|-----------------------------------------------
*/


    /*
    |-------------------------------------------------------------------
    | Create_ReferenceTable_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblRefTable
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_ReferenceTable_SQL_InsertString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
        $sql = "INSERT INTO TBLREFTABLE ".
               "(REFTABLE_ID, REFTABLE_TYPE, REFTABLE_DESCRIPTION, REFTABLE_COMMENT, SITE_ID) ".
	           "VALUES".
	           "( [SEQUENCE].NEXTVAL, '[REFTABLE_TYPE]', '[REFTABLE_DESCRIPTION]', '[REFTABLE_COMMENT]', [SITE_ID] )";

        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE             = 'TBLREFTABLE_SEQ';  // Sequence used to get next table identifier value
        $REFTABLE_TYPE        = $Post_Values['REFTABLE_TYPE']; // Required
        $SITE_ID              = $Post_Values['SITE_ID']; // Required
        $REFTABLE_DESCRIPTION = $Post_Values['REFTABLE_DESCRIPTION']; // Required
        $REFTABLE_COMMENT     = $Post_Values['REFTABLE_COMMENT'];

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[REFTABLE_TYPE]', $REFTABLE_TYPE, $sql);
        $sql = str_replace('[REFTABLE_DESCRIPTION]', $REFTABLE_DESCRIPTION, $sql);
        $sql = str_replace('[REFTABLE_COMMENT]', $REFTABLE_COMMENT, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);


        return $sql;
    }






    /*
    |-------------------------------------------------------------------
    | Create_ReferenceTable_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblRefTable
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_ReferenceTable_SQL_UpdateString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
        $sql = "UPDATE tblRefTable ".
               "SET RefTable_Type = '[REFTABLE_TYPE]', ".
		       "Site_ID = [SITE_ID], ".
               "RefTable_Description = '[REFTABLE_DESCRIPTION]', ". 
               "RefTable_Comment = '[REFTABLE_COMMENT]' ".
               "WHERE RefTable_ID = [REFTABLE_ID]";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $REFTABLE_ID          = $Get_Values['id']; // Required
	      $REFTABLE_TYPE        = $Post_Values['REFTABLE_TYPE']; // Required
        $SITE_ID              = $Post_Values['SITE_ID']; // Required
        $REFTABLE_DESCRIPTION = $Post_Values['REFTABLE_DESCRIPTION']; // Required
        $REFTABLE_COMMENT     = $Post_Values['REFTABLE_COMMENT'];

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[REFTABLE_ID]', $REFTABLE_ID, $sql);	
        $sql = str_replace('[REFTABLE_TYPE]', $REFTABLE_TYPE, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);	
        $sql = str_replace('[REFTABLE_DESCRIPTION]', $REFTABLE_DESCRIPTION, $sql);
        $sql = str_replace('[REFTABLE_COMMENT]', $REFTABLE_COMMENT, $sql);

        return $sql;
    }



    /*
    |-------------------------------------------------------------------
    | Delete_ReferenceTable_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $id:- Entry to delete
    |-------------------------------------------------------------------
    */

    function Delete_ReferenceTable_SQL($id)
	  {
		    $sql = "DELETE FROM tblRefTable ".
		           "WHERE REFTABLE_ID = [REFTABLE_ID]";

        $sql = str_replace('[REFTABLE_ID]', $id, $sql);

		    return $sql;
	}

?>