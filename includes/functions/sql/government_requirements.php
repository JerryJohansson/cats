<?PHP

/*
|-----------------------------------------------
| GOVERNMENT DETAILS
|-----------------------------------------------
*/



    /*
    |-------------------------------------------------------------------
    | Create_GovernmentDetails_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblGovernment_Details
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */
	
    function Create_GovernmentDetails_SQL_InsertString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;
	
        $sql = "INSERT INTO TBLGOVERNMENT_DETAILS ".
		       "(GOVERNMENT_ID, REQUIREMENT_TYPE, DATE_COMPLETED, SITE_ID, ".
			   "SHIFT_ID, CONDUCTED_BY_ID, TIWEST_CONTACT_ID, COMMENTS, ".
			   "GOVT_DEPARTMENT, LOCATION, LOCATION_OF_DOCUMENTS) ".
			   "VALUES ".
			   "([SEQUENCE].NextVal, [REQUIREMENT_TYPE], [DATE_COMPLETED], ".
               "[SITE_ID], [SHIFT_ID], '[CONDUCTED_BY_ID]', [TIWEST_CONTACT_ID], '[COMMENTS]', ". 
			   "'[GOVT_DEPARTMENT]', '[LOCATION]', '[LOCATION_OF_DOCUMENTS]')";
	
	
        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE               = 'TBLGOV_DETAILS_SEQ';  // Sequence used to get next table identifier value
	    $REQUIREMENT_TYPE       = $Post_Values['REQUIREMENT_TYPE']; // Required
	    $DATE_COMPLETED         = $db->DBDate(trim($Post_Values['DATE_COMPLETED'])); // Required
	    $SITE_ID                = $Post_Values['SITE_ID']; // Required
		$SHIFT_ID               = $Post_Values['SHIFT_ID'];
		$CONDUCTED_BY_ID        = $Post_Values['CONDUCTED_BY_ID']; // Required
		$TIWEST_CONTACT_ID      = $Post_Values['TIWEST_CONTACT_ID']; // Required
		$COMMENTS               = $Post_Values['COMMENTS'];
		$GOVT_DEPARTMENT        = $Post_Values['GOVT_DEPARTMENT'];
		$LOCATION               = $Post_Values['LOCATION'];
		$LOCATION_OF_DOCUMENTS  = $Post_Values['LOCATION_OF_DOCUMENTS'];

	
        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------	
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[REQUIREMENT_TYPE]', $REQUIREMENT_TYPE, $sql);
        $sql = str_replace('[DATE_COMPLETED]', $DATE_COMPLETED, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);		
        $sql = str_replace('[SHIFT_ID]', $SHIFT_ID, $sql);		
        $sql = str_replace('[CONDUCTED_BY_ID]', $CONDUCTED_BY_ID, $sql);
        $sql = str_replace('[TIWEST_CONTACT_ID]', $TIWEST_CONTACT_ID, $sql);		
        $sql = str_replace('[COMMENTS]', $COMMENTS, $sql);		
        $sql = str_replace('[GOVT_DEPARTMENT]', $GOVT_DEPARTMENT, $sql);		
        $sql = str_replace('[LOCATION]', $LOCATION, $sql);		
        $sql = str_replace('[LOCATION_OF_DOCUMENTS]', $LOCATION_OF_DOCUMENTS, $sql);		

        return $sql;

	}



    /*
    |-------------------------------------------------------------------
    | Create_GovernmentDetails_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to update the values against
    |              tblGovernment_Details
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */
	
    function Create_GovernmentDetails_SQL_UpdateString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;	
	
	    $sql = "UPDATE TBLGOVERNMENT_DETAILS ". 
		       "SET DATE_COMPLETED = [DATE_COMPLETED], ". 
			   "SITE_ID = [SITE_ID], ".
			   "SHIFT_ID = [SHIFT_ID], ".
			   "CONDUCTED_BY_ID = '[CONDUCTED_BY_ID]', ".
			   "TIWEST_CONTACT_ID = '[TIWEST_CONTACT_ID]', ".
			   "COMMENTS = '[COMMENTS]', ".
			   "GOVT_DEPARTMENT = '[GOVT_DEPARTMENT]', ".
			   "LOCATION = '[LOCATION]', ".
			   "LOCATION_OF_DOCUMENTS = '[LOCATION_OF_DOCUMENTS]' ".
			   "WHERE GOVERNMENT_ID = [GOVERNMENT_ID]";
	
	
        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $GOVERNMENT_ID          = $Get_Values['id']; // Required
	    //$REQUIREMENT_TYPE       = $Post_Values['REQUIREMENT_TYPE']; // Required
	    $DATE_COMPLETED         = $db->DBDate(trim($Post_Values['DATE_COMPLETED'])); // Required
	    $SITE_ID                = $Post_Values['SITE_ID']; // Required
		$SHIFT_ID               = $Post_Values['SHIFT_ID'];
		$CONDUCTED_BY_ID        = $Post_Values['CONDUCTED_BY_ID']; // Required
		$TIWEST_CONTACT_ID      = $Post_Values['TIWEST_CONTACT_ID']; // Required
		$COMMENTS               = $Post_Values['COMMENTS'];
		$GOVT_DEPARTMENT        = $Post_Values['GOVT_DEPARTMENT'];
		$LOCATION               = $Post_Values['LOCATION'];
		$LOCATION_OF_DOCUMENTS  = $Post_Values['LOCATION_OF_DOCUMENTS'];

	
        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------	
        $sql = str_replace('[GOVERNMENT_ID]', $GOVERNMENT_ID, $sql);
        //$sql = str_replace('[REQUIREMENT_TYPE]', $REQUIREMENT_TYPE, $sql);
        $sql = str_replace('[DATE_COMPLETED]', $DATE_COMPLETED, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);		
        $sql = str_replace('[SHIFT_ID]', $SHIFT_ID, $sql);		
        $sql = str_replace('[CONDUCTED_BY_ID]', $CONDUCTED_BY_ID, $sql);
        $sql = str_replace('[TIWEST_CONTACT_ID]', $TIWEST_CONTACT_ID, $sql);		
        $sql = str_replace('[COMMENTS]', $COMMENTS, $sql);		
        $sql = str_replace('[GOVT_DEPARTMENT]', $GOVT_DEPARTMENT, $sql);		
        $sql = str_replace('[LOCATION]', $LOCATION, $sql);		
        $sql = str_replace('[LOCATION_OF_DOCUMENTS]', $LOCATION_OF_DOCUMENTS, $sql);		
	
	    return $sql;
	
	  }



    /*
    |-------------------------------------------------------------------
    | Delete_GovernmentDetails_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $id:- Entry to delete
    |-------------------------------------------------------------------
    */

    function Delete_GovernmentDetails_SQL($id)
	  {
	      $sql = "DELETE FROM TBLGOVERNMENT_DETAILS ".
		           "WHERE TBLGOVERNMENT_DETAILS = [TBLGOVERNMENT_DETAILS]";

          $sql = str_replace('[TBLGOVERNMENT_DETAILS]', $id, $sql);

		  return $sql;
	  }

?>