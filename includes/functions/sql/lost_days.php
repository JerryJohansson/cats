<?PHP


    /*
    |-------------------------------------------------------------------
    | Create_LostDays_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblLost_Days
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_LostDays_SQL_InsertString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
        $sql = "INSERT INTO tblLost_Days ".
               "(Lost_Days_ID, Incident_Number, Lost_Days, Restricted_Days, Years, Months, Year_Month) ".
               "VALUES ".
               "([SEQUENCE].NextVal, [INCIDENT_NUMBER], [LOST_DAYS], [RESTRICTED_DAYS], [YEARS], [MONTHS], [YEAR_MONTH])";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE         = 'TBLLOST_DAYS_SEQ';  // Sequence used to get next table identifier value
        $INCIDENT_NUMBER  = $Post_Values['INCIDENT_NUMBER']; // Required
        $LOST_DAYS        = $Post_Values['LOST_DAYS']; // Required
        $RESTRICTED_DAYS  = $Post_Values['RESTRICTED_DAYS'];
        $YEARS            = $Post_Values['YEARS'];
        $MONTHS           = $Post_Values['MONTHS'];
        $YEAR_MONTH       = 'NULL'; // NOT USED

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[INCIDENT_NUMBER]', $INCIDENT_NUMBER, $sql);
        $sql = str_replace('[LOST_DAYS]', $LOST_DAYS, $sql);
        $sql = str_replace('[RESTRICTED_DAYS]', $RESTRICTED_DAYS, $sql);
        $sql = str_replace('[YEARS]', $YEARS, $sql);
        $sql = str_replace('[MONTHS]', $MONTHS, $sql);
        $sql = str_replace('[YEAR_MONTH]', $YEAR_MONTH, $sql);			


        return $sql;
    }






    /*
    |-------------------------------------------------------------------
    | Create_LostDays_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblLost_Days
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_LostDays_SQL_UpdateString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
        $sql = "UPDATE tblLost_Days ".
               "SET Lost_Days = [LOST_DAYS], ". 
               "Restricted_Days = [RESTRICTED_DAYS], ". 
               "Years = [YEARS], ". 
               "Months = [MONTHS], ". 
               "Year_Month = [YEAR_MONTH] ".
               "WHERE Lost_Days_ID = [LOST_DAYS_ID]";

        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $LOST_DAYS_ID     = $Get_Values['id'];  // Sequence used to get next table identifier value
        $INCIDENT_NUMBER  = $Post_Values['INCIDENT_NUMBER']; // Required
        $LOST_DAYS        = $Post_Values['LOST_DAYS']; // Required
        $RESTRICTED_DAYS  = $Post_Values['RESTRICTED_DAYS'];
        $YEARS            = $Post_Values['YEARS'];
        $MONTHS           = $Post_Values['MONTHS'];
        $YEAR_MONTH       = 'NULL'; // NOT USED

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[LOST_DAYS_ID]', $LOST_DAYS_ID, $sql);
        $sql = str_replace('[INCIDENT_NUMBER]', $INCIDENT_NUMBER, $sql);
        $sql = str_replace('[LOST_DAYS]', $LOST_DAYS, $sql);
        $sql = str_replace('[RESTRICTED_DAYS]', $RESTRICTED_DAYS, $sql);
        $sql = str_replace('[YEARS]', $YEARS, $sql);
        $sql = str_replace('[MONTHS]', $MONTHS, $sql);
        $sql = str_replace('[YEAR_MONTH]', $YEAR_MONTH, $sql);	

        return $sql;
    }



    /*
    |-------------------------------------------------------------------
    | Delete_LostDays_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $id:- Entry to delete
    |-------------------------------------------------------------------
    */

    function Delete_LostDays_SQL($id)
	{
	    $sql = "DELETE FROM tblLost_Days ".
		       "WHERE LOST_DAYS_ID = [LOST_DAYS_ID]";

        $sql = str_replace('[LOST_DAYS_ID]', $id, $sql);

		return $sql;
	}


?>