<?PHP


/*
|-----------------------------------------------
| ACTIONS
|-----------------------------------------------
*/



    /*
    |-------------------------------------------------------------------
    | Get_Created_Actions
    |-------------------------------------------------------------------
    | Description: Retrieve the Message Of The Day
    |
    | Parameters: $Report_ID:- The Calling Table Primary Key Identifier
		|             $Origin_Table:- The table type calling ie Originating_Government | Originating_Other
		|             
		| Returns: Integer of the total action created
    |-------------------------------------------------------------------
    */

    function Get_Created_Actions($Report_ID, $Origin_Table)
    {
			$sql = "select ACTIONS_CREATED($Report_ID, '$Origin_Table') from dual";
			$Total = db_get_one($sql);
			return $Total;
    }






    /*
    |-------------------------------------------------------------------
    | Get_Closed_Actions
    |-------------------------------------------------------------------
    | Description: Retrieve the Message Of The Day
    |
    | Parameters: $Report_ID:- The Calling Table Primary Key Identifier
		|             $Origin_Table:- The table type calling ie Originating_Government | Originating_Other
		|         
		| Returns: Integer of the total actions closed    
    |-------------------------------------------------------------------
    */

    function Get_Closed_Actions($Report_ID, $Origin_Table)
    {
			$sql = "select ACTIONS_CLOSED($Report_ID, '$Origin_Table') from dual";
			$Total = db_get_one($sql);
			return $Total;
    }



?>