<?PHP

/*
|-----------------------------------------------
| MESSAGE OF THE DAY
|-----------------------------------------------
*/



    /*
    |-------------------------------------------------------------------
    | GET_MessageOfTheDay_SQL_SelectString
    |-------------------------------------------------------------------
    | Description: Retrieve the Message Of The Day
    |
    | Parameters: $ID
    |-------------------------------------------------------------------
    */


    function GET_MessageOfTheDay_SQL_SelectString($id)
    {
                                
        $sql = "SELECT ".
               "MOTDID, MOTD_TYPE, MOTD_URL, MOTD_SHORT, MOTD_DESC, MOTD_STARTDATE, MOTD_ENDDATE, ".
			   "MOTD_FREQUENCY, MOTD_REPEATPERIOD, MOTD_DELETE, MOTD_DATEOPTION_1, MOTD_DATEOPTION_2, ". 
			   "MOTD_DATEOPTION_3, MOTD_SITE, TIMESTAMP ".
			   "FROM tblMOTD ".
			   "WHERE MOTDID = ".$id. " ";

	    $rs = db_query($sql);
        
		while($row = $rs->FetchRow())
        {
            $MOTD = array ( "MOTDID" => $row["MOTDID"], 
                            "MOTD_TYPE" => $row["MOTD_TYPE"], 
                            "MOTD_URL" => $row["MOTD_URL"], 
                            "MOTD_SHORT" => $row["MOTD_SHORT"], 
                            "MOTD_DESC" => $row["MOTD_DESC"], 
                            "MOTD_STARTDATE" => $row["MOTD_STARTDATE"], 
                            "MOTD_ENDDATE" => $row["MOTD_ENDDATE"], 
                            "MOTD_FREQUENCY" => $row["MOTD_FREQUENCY"], 
                            "MOTD_REPEATPERIOD" => $row["MOTD_REPEATPERIOD"], 
                            "MOTD_DELETE" => $row["MOTD_DELETE"], 
                            "MOTD_DATEOPTION_1" => $row["MOTD_DATEOPTION_1"], 
                            "MOTD_DATEOPTION_2" => $row["MOTD_DATEOPTION_2"], 
                            "MOTD_DATEOPTION_3" => $row["MOTD_DATEOPTION_3"], 
                            "MOTD_SITE" => $row["MOTD_SITE"], 
                            "TIMESTAMP" => $row["TIMESTAMP"]);
		}

		return $MOTD;
    }





    /*
    |-------------------------------------------------------------------
    | Create_MessageOfTheDay_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblMOTD
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_MessageOfTheDay_SQL_InsertString($Post_Values, $Get_Values)
    {
	
	    $Post_Values = ReplaceArrayQuotes($Post_Values);

	    global $db;		
	
        $sql = "INSERT INTO tblMOTD ".
               "(MOTDID, MOTD_TYPE, MOTD_URL, MOTD_SHORT, MOTD_DESC, MOTD_STARTDATE, MOTD_ENDDATE, ".
			   "MOTD_FREQUENCY, MOTD_REPEATPERIOD, MOTD_DELETE, MOTD_DATEOPTION_1, MOTD_DATEOPTION_2, ". 
			   " MOTD_DATEOPTION_3, MOTD_SITE, TIMESTAMP) ".
               " VALUES ".
               "([SEQUENCE].NextVal, [MOTD_TYPE], '[MOTD_URL]', '[MOTD_SHORT]', '[MOTD_DESC]', [MOTD_STARTDATE], [MOTD_ENDDATE], ".
			   "[MOTD_FREQUENCY], [MOTD_REPEATPERIOD], [MOTD_DELETE], [MOTD_DATEOPTION_1],[MOTD_DATEOPTION_2], ". 
			   "[MOTD_DATEOPTION_3], '[MOTD_SITE]', [TIMESTAMP])";

        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE                  = 'TBLMOTD_SEQ';  // Sequence used to get next table identifier value
        $MOTD_TYPE                 = $Post_Values['MOTD_TYPE'];
        $MOTD_URL                  = $Post_Values['MOTD_URL'];
        $MOTD_SHORT                = $Post_Values['MOTD_SHORT'];
        $MOTD_DESC                 = $Post_Values['MOTD_DESC'];
        $MOTD_STARTDATE            = $db->DBDate(trim($Post_Values['MOTD_STARTDATE_d']));
        $MOTD_ENDDATE              = $db->DBDate(trim($Post_Values['MOTD_ENDDATE_d']));
        $MOTD_FREQUENCY            = $Post_Values['MOTD_Frequency'];
        $MOTD_REPEATPERIOD         = $Post_Values['MOTD_RepeatPeriod'];
        $MOTD_DELETE               = $Post_Values['MOTD_Delete'];
        $MOTD_DATEOPTION_1         = $Post_Values['MOTD_dateoption_1'];
        $MOTD_DATEOPTION_2         = $Post_Values['MOTD_dateoption_2'];
        $MOTD_DATEOPTION_3         = $Post_Values['MOTD_dateoption_3'];
        $MOTD_SITE                 = $Post_Values['MOTD_Site'];
        $TIMESTAMP                 = 'NULL'; //$Post_Values['TIMESTAMP'];											

        
		// Check if values exist
		if (strlen($MOTD_DATEOPTION_1) <= 0)
        {
            $MOTD_DATEOPTION_1 = 'NULL';
            $MOTD_DATEOPTION_2 = 'NULL';
            $MOTD_DATEOPTION_3 = 'NULL';
		}
		
		if (strlen($MOTD_DELETE) <= 0)
		{
            $MOTD_DELETE = 'NULL';
		}		


        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[MOTD_TYPE]', $MOTD_TYPE, $sql);
        $sql = str_replace('[MOTD_URL]', $MOTD_URL, $sql);
        $sql = str_replace('[MOTD_SHORT]', $MOTD_SHORT, $sql);
        $sql = str_replace('[MOTD_DESC]', $MOTD_DESC, $sql);
        $sql = str_replace('[MOTD_STARTDATE]', $MOTD_STARTDATE, $sql);
        $sql = str_replace('[MOTD_ENDDATE]', $MOTD_ENDDATE, $sql);			
        $sql = str_replace('[MOTD_FREQUENCY]', $MOTD_FREQUENCY, $sql);	
        $sql = str_replace('[MOTD_REPEATPERIOD]', $MOTD_REPEATPERIOD, $sql);
        $sql = str_replace('[MOTD_DELETE]', $MOTD_DELETE, $sql);
        $sql = str_replace('[MOTD_DATEOPTION_1]', $MOTD_DATEOPTION_1, $sql);
        $sql = str_replace('[MOTD_DATEOPTION_2]', $MOTD_DATEOPTION_2, $sql);
        $sql = str_replace('[MOTD_DATEOPTION_3]', $MOTD_DATEOPTION_3, $sql);						
        $sql = str_replace('[MOTD_SITE]', $MOTD_SITE, $sql);					
        $sql = str_replace('[TIMESTAMP]', $TIMESTAMP, $sql);

        return $sql;
    }











    /*
    |-------------------------------------------------------------------
    | Create_MessageOfTheDay_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to update the values in
    |              tblMOTD
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_MessageOfTheDay_SQL_UpdateString($Post_Values, $Get_Values)
    {

	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;		
	
        $sql = "UPDATE tblMOTD ".
               "SET MOTD_TYPE = '[MOTD_TYPE]', ".  
               "MOTD_URL = '[MOTD_URL]', ". 
               "MOTD_SHORT = '[MOTD_SHORT]', ". 
               "MOTD_DESC = '[MOTD_DESC]', ". 
               "MOTD_STARTDATE = [MOTD_STARTDATE], ". 
               "MOTD_ENDDATE = [MOTD_ENDDATE], ". 
               "MOTD_FREQUENCY = [MOTD_FREQUENCY], ". 
               "MOTD_REPEATPERIOD = [MOTD_REPEATPERIOD],  ".
               "MOTD_DELETE = [MOTD_DELETE], ". 
               "MOTD_DATEOPTION_1 = [MOTD_DATEOPTION_1], ". 
               "MOTD_DATEOPTION_2 = [MOTD_DATEOPTION_2], ". 
               "MOTD_DATEOPTION_3 = [MOTD_DATEOPTION_3], ".
               "MOTD_SITE = '[MOTD_SITE]', ". 
               "TIMESTAMP = [TIMESTAMP] ". 
               "WHERE MOTDID = [MOTDID]";
        

        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $MOTDID                    = $Get_Values['id'];  // Sequence used to get next table identifier value
        $MOTD_TYPE                 = $Post_Values['MOTD_TYPE'];
        $MOTD_URL                  = $Post_Values['MOTD_URL'];
        $MOTD_SHORT                = $Post_Values['MOTD_SHORT'];
        $MOTD_DESC                 = $Post_Values['MOTD_DESC'];
        $MOTD_STARTDATE            = $db->DBDate(trim($Post_Values['MOTD_STARTDATE_d']));
        $MOTD_ENDDATE              = $db->DBDate(trim($Post_Values['MOTD_ENDDATE_d']));
        $MOTD_FREQUENCY            = $Post_Values['MOTD_Frequency'];
        $MOTD_REPEATPERIOD         = $Post_Values['MOTD_RepeatPeriod'];
        $MOTD_DELETE               = $Post_Values['MOTD_Delete'];
        $MOTD_DATEOPTION_1         = $Post_Values['MOTD_dateoption1_1'];
        $MOTD_DATEOPTION_2         = $Post_Values['MOTD_dateoption_2'];
        $MOTD_DATEOPTION_3         = $Post_Values['MOTD_dateoption_3'];
        $MOTD_SITE                 = $Post_Values['MOTD_Site'];
        $TIMESTAMP                 = 'NULL'; //$Post_Values['TIMESTAMP'];											


		// Check if values exist
		if (strlen($MOTD_DATEOPTION_1) <= 0)
        {
            $MOTD_DATEOPTION_1 = 'NULL';
            $MOTD_DATEOPTION_2 = 'NULL';
            $MOTD_DATEOPTION_3 = 'NULL';
		}

		if (strlen($MOTD_DELETE) <= 0)
		{
            $MOTD_DELETE = 'NULL';
		}

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[MOTDID]', $MOTDID, $sql);
        $sql = str_replace('[MOTD_TYPE]', $MOTD_TYPE, $sql);
        $sql = str_replace('[MOTD_URL]', $MOTD_URL, $sql);
        $sql = str_replace('[MOTD_SHORT]', $MOTD_SHORT, $sql);
        $sql = str_replace('[MOTD_DESC]', $MOTD_DESC, $sql);
        $sql = str_replace('[MOTD_STARTDATE]', $MOTD_STARTDATE, $sql);
        $sql = str_replace('[MOTD_ENDDATE]', $MOTD_ENDDATE, $sql);			
        $sql = str_replace('[MOTD_FREQUENCY]', $MOTD_FREQUENCY, $sql);	
        $sql = str_replace('[MOTD_REPEATPERIOD]', $MOTD_REPEATPERIOD, $sql);
        $sql = str_replace('[MOTD_DELETE]', $MOTD_DELETE, $sql);
        $sql = str_replace('[MOTD_DATEOPTION_1]', $MOTD_DATEOPTION_1, $sql);
        $sql = str_replace('[MOTD_DATEOPTION_2]', $MOTD_DATEOPTION_2, $sql);
        $sql = str_replace('[MOTD_DATEOPTION_3]', $MOTD_DATEOPTION_3, $sql);						
        $sql = str_replace('[MOTD_SITE]', $MOTD_SITE, $sql);					
        $sql = str_replace('[TIMESTAMP]', $TIMESTAMP, $sql);

        return $sql;
    }








?>