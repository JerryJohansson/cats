<?PHP

/*
|-----------------------------------------------
| SITE SPECIFIC OBLIGATIONS
|-----------------------------------------------
*/


    /*
    |-------------------------------------------------------------------
    | Create_OtherRecords_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblObligations
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_SiteObligations_SQL_InsertString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;		
	
        $sql = "INSERT INTO tblObligations ".
               "(OBLIGATION_ID, SUBJECT, ISSUE, KEY_REQUIREMENT, LEGISLATION1, MANAGED_BY_POSITION_ID, ALLOCATED_TO_POSITION_ID, VERIFICATION, SITE_ID) ".
               "VALUES ".
               "([SEQUENCE].NextVal, [SUBJECT], [ISSUE], '[KEY_REQUIREMENT]', '[LEGISLATION1]', [MANAGED_BY_POSITION_ID], [ALLOCATED_TO_POSITION_ID], '[VERIFICATION]', [SITE_ID])";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE                 = 'TBLOBLIGATIONS_SEQ';  // Sequence used to get next table identifier value
        $SUBJECT                   = $Post_Values['SUBJECT'];
        $ISSUE                     = $Post_Values['ISSUE'];
        $KEY_REQUIREMENT           = $Post_Values['KEY_REQUIREMENT'];
        $LEGISLATION1              = $Post_Values['LEGISLATION1'];
        $MANAGED_BY_POSITION_ID    = 'NULL'; $Post_Values['MANAGED_BY_POSITION_ID'];
        $ALLOCATED_TO_POSITION_ID  = 'NULL';
        $VERIFICATION              = $Post_Values['VERIFICATION'];
        $SITE_ID                   = $Post_Values['SITE_ID'];


        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[SUBJECT]', $SUBJECT, $sql);
        $sql = str_replace('[ISSUE]', $ISSUE, $sql);
        $sql = str_replace('[KEY_REQUIREMENT]', $KEY_REQUIREMENT, $sql);
        $sql = str_replace('[LEGISLATION1]', $LEGISLATION1, $sql);
        $sql = str_replace('[MANAGED_BY_POSITION_ID]', $MANAGED_BY_POSITION_ID, $sql);
        $sql = str_replace('[ALLOCATED_TO_POSITION_ID]', $ALLOCATED_TO_POSITION_ID, $sql);			
        $sql = str_replace('[VERIFICATION]', $VERIFICATION, $sql);	
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);


        return $sql;
    }




    /*
    |-------------------------------------------------------------------
    | Create_SiteObligations_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblObligations
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_SiteObligations_SQL_UpdateString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;		
	
        $sql = "UPDATE TBLOBLIGATIONS ".
               "SET SUBJECT = '[SUBJECT]', ".
               "ISSUE = '[ISSUE]', ".
               "KEY_REQUIREMENT = '[KEY_REQUIREMENT]', ".
               "LEGISLATION1 = '[LEGISLATION1]', ".
               "VERIFICATION = '[VERIFICATION]', ".
               "MANAGED_BY_POSITION_ID = [MANAGED_BY_POSITION_ID], ".
               "ALLOCATED_TO_POSITION_ID = [ALLOCATED_TO_POSITION_ID], ".			   			   
               "SITE_ID = [SITE_ID] ".
               "WHERE OBLIGATION_ID = [OBLIGATION_ID]";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $OBLIGATION_ID             = $Get_Values['id'];
        $SUBJECT                   = $Post_Values['SUBJECT'];
        $ISSUE                     = $Post_Values['ISSUE'];
        $KEY_REQUIREMENT           = $Post_Values['KEY_REQUIREMENT'];
        $LEGISLATION1              = $Post_Values['LEGISLATION1'];
        $MANAGED_BY_POSITION_ID    = 'NULL'; //$Post_Values['MANAGED_BY_POSITION_ID'];
        $ALLOCATED_TO_POSITION_ID  = 'NULL';
        $VERIFICATION              = $Post_Values['VERIFICATION'];
        $SITE_ID                   = $Post_Values['SITE_ID'];


        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[OBLIGATION_ID]', $OBLIGATION_ID, $sql);
        $sql = str_replace('[SUBJECT]', $SUBJECT, $sql);
        $sql = str_replace('[ISSUE]', $ISSUE, $sql);
        $sql = str_replace('[KEY_REQUIREMENT]', $KEY_REQUIREMENT, $sql);
        $sql = str_replace('[LEGISLATION1]', $LEGISLATION1, $sql);
        $sql = str_replace('[MANAGED_BY_POSITION_ID]', $MANAGED_BY_POSITION_ID, $sql);
        $sql = str_replace('[ALLOCATED_TO_POSITION_ID]', $ALLOCATED_TO_POSITION_ID, $sql);			
        $sql = str_replace('[VERIFICATION]', $VERIFICATION, $sql);	
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);


        return $sql;
    }




    /*
    |-------------------------------------------------------------------
    | Insert_ObligationPosition_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $obligation_id:- Obligation ID
	|             $position_idL - Position ID
    |-------------------------------------------------------------------
    */
    function Insert_ObligationPosition_SQL($obligation_id, $position_id)
	{
	    $sql = "INSERT INTO tblObligationPositions ".
		       "(OBLIGATION_ID, POSITION_ID) ".
			   "VALUES ".
			   "([OBLIGATION_ID], [POSITION_ID])";
		
        $sql = str_replace('[OBLIGATION_ID]', $obligation_id, $sql);
        $sql = str_replace('[POSITION_ID]', $position_id, $sql);
		
		return $sql;
	}


    /*
    |-------------------------------------------------------------------
    | Delete_ObligationPositions_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $obligation_id:- Obligation ID
    |-------------------------------------------------------------------
    */
    function Delete_ObligationPositions_SQL($obligation_id)
	{
	    $sql = "DELETE FROM tblObligationPositions ".
		       "WHERE Obligation_ID = [OBLIGATION_ID]";

        $sql = str_replace('[OBLIGATION_ID]', $obligation_id, $sql);

		return $sql;
	}




    /*
    |-------------------------------------------------------------------
    | Select_ObligationPositions_FROM_ID_SQL
    |-------------------------------------------------------------------
    | Description: Retrieve the obligation positions
    |
    | Parameters: $obligation_id:- Obligation ID
    |-------------------------------------------------------------------
    */
    function Select_ObligationPositions_FROM_ID_SQL($obligation_id)
	{
	    $sql = "SELECT * FROM tblObligationPositions ".
		       "WHERE Obligation_ID = [OBLIGATION_ID]";
			   
        $sql = str_replace('[OBLIGATION_ID]', $obligation_id, $sql);

		return $sql;
	}


?>