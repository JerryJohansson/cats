<?PHP

/*
|-----------------------------------------------
| AUDITS AND INSPECTIONS
|-----------------------------------------------
*/


    /*
    |-------------------------------------------------------------------
    | Create_AuditInspection_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblAudits
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */
	
    function Create_AuditInspection_SQL_InsertString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;		
	
	    $sql = "INSERT INTO tblAudits ".
		       "(Report_ID, Register_Origin, Record_Type, Date_Inspection, ". 
			   "Site_ID, Department_ID, Audit_Notes, Location_Docs) ".
			   "VALUES ".
			   "([SEQUENCE].NextVal, '[REGISTER_ORIGIN]', '[RECORD_TYPE]', [DATE_INSPECTION], ". 
			   "[SITE_ID], [DEPARTMENT_ID], '[AUDIT_NOTES]', '[LOCATION_DOCS]')";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE             = 'TBLAUDITS_SEQ';  // Sequence used to get next table identifier value
        $REGISTER_ORIGIN      = 'Audit'; // Required
        $RECORD_TYPE          = $Post_Values['RECORD_TYPE']; // Required
        $DATE_INSPECTION      = $db->DBDate(trim($Post_Values['DATE_INSPECTION'])); // Required
        $SITE_ID              = $Post_Values['SITE_ID'];  // Required
		$DEPARTMENT_ID        = $Post_Values['DEPARTMENT_ID']; // Required
		$AUDIT_NOTES          = $Post_Values['AUDIT_NOTES'];
		$LOCATION_DOCS        = $Post_Values['LOCATION_DOCS'];

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[REGISTER_ORIGIN]', $REGISTER_ORIGIN, $sql);
        $sql = str_replace('[RECORD_TYPE]', $RECORD_TYPE, $sql);
        $sql = str_replace('[DATE_INSPECTION]', $DATE_INSPECTION, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);
        $sql = str_replace('[DEPARTMENT_ID]', $DEPARTMENT_ID, $sql);
        $sql = str_replace('[AUDIT_NOTES]', $AUDIT_NOTES, $sql);
        $sql = str_replace('[LOCATION_DOCS]', $LOCATION_DOCS, $sql);				
			   
	    return $sql;
	}
	

    /*
    |-------------------------------------------------------------------
    | Create_AuditInspection_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblAudits
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_AuditInspection_SQL_UpdateString($Post_Values, $Get_Values)
    {
	    global $db;			
	
        $sql = "UPDATE tblAudits ".
		       "SET Record_Type = [RECORD_TYPE], ".
			   "Date_Inspection = [DATE_INSPECTION], ".
			   "Site_ID = [SITE_ID], ".
			   "Department_ID = [DEPARTMENT_ID], ".
			   "Audit_Notes = '[AUDIT_NOTES]', ".
			   "Location_Docs = '[LOCATION_DOCS]' ".
			   "WHERE Report_ID = [REPORT_ID]";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $REPORT_ID            = $Get_Values['id']; // Sequence used to get next table identifier value
        //$REGISTER_ORIGIN      = 'Audit'; //$Post_Values['REGISTER_ORIGIN']; // Required
        $RECORD_TYPE          = $Post_Values['RECORD_TYPE']; // Required
        $DATE_INSPECTION      = $db->DBDate(trim($Post_Values['DATE_INSPECTION'])); // Required
        $SITE_ID              = $Post_Values['SITE_ID'];
		$DEPARTMENT_ID        = $Post_Values['DEPARTMENT_ID'];
		$AUDIT_NOTES          = $Post_Values['AUDIT_NOTES'];
		$LOCATION_DOCS        = $Post_Values['LOCATION_DOCS'];

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[REPORT_ID]', $REPORT_ID, $sql);
        //$sql = str_replace('[REGISTER_ORIGIN]', $REGISTER_ORIGIN, $sql);
        $sql = str_replace('[RECORD_TYPE]', $RECORD_TYPE, $sql);
        $sql = str_replace('[DATE_INSPECTION]', $DATE_INSPECTION, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);
        $sql = str_replace('[DEPARTMENT_ID]', $DEPARTMENT_ID, $sql);
        $sql = str_replace('[AUDIT_NOTES]', $AUDIT_NOTES, $sql);
        $sql = str_replace('[LOCATION_DOCS]', $LOCATION_DOCS, $sql);

        return $sql;
    }



    /*
    |-------------------------------------------------------------------
    | Delete_AuditInspection_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $id:- Entry to delete
    |-------------------------------------------------------------------
    */

    function Delete_AuditInspection_SQL($id)
	{
	    $sql = "DELETE FROM tblAudits ".
		       "WHERE tblAudits = [tblAudits]";

        $sql = str_replace('[tblAudits]', $id, $sql);

		return $sql;
	}

?>