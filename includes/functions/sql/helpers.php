<?PHP



/*
|-----------------------------------------------
| HELPERS
|-----------------------------------------------
*/


    /*
    |-------------------------------------------------------------------
    | GetNextMaxIdentifierID
    |-------------------------------------------------------------------
    | Description: Gets the next primary identifier id
    |
    | Parameters: $Table: The table to query
    |             $Identifier: Primary Key of Table
    |-------------------------------------------------------------------
    */
    function GetNextMaxIdentifierID($Table, $Identifier)
	{
	      $nextValue = 1;
	      $sql = "SELECT MAX(".$Identifier.") AS MAX FROM ".$Table."";
		
		  $rs = db_query($sql);
		
          while($row = $rs->FetchRow())
          {
              $nextValue = $row["MAX"] + 1;
		  }

		  return $nextValue;
	}



    /*
    |-------------------------------------------------------------------
    | GetNextMaxIdentifierID
    |-------------------------------------------------------------------
    | Description: Gets the next primary identifier id
    |
    | Parameters: $Table: The table to query
    |             $Identifier: Primary Key of Table
    |-------------------------------------------------------------------
    */
    function GetMaxIdentifierID($Table, $Identifier)
	{
	    $nextValue = 1;
	    $sql = "SELECT MAX(".$Identifier.") AS MAX FROM ".$Table."";
		
	    $rs = db_query($sql);
		
        while($row = $rs->FetchRow())
        {
          $nextValue = $row["MAX"];
        }
		
		  return $nextValue;
    }


?>