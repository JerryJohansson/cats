<?PHP


/*
|-----------------------------------------------
| ACTIONS
|-----------------------------------------------
*/



    /*
    |-------------------------------------------------------------------
    | Get_Created_Actions
    |-------------------------------------------------------------------
    | Description: Retrieve the Message Of The Day
    |
    | Parameters: $Report_ID:- The Calling Table Primary Key Identifier
	|             $Origin_Table:- The table type calling ie Originating_Government | Originating_Other
	|             
	| Returns: Integer of the total action created
    |-------------------------------------------------------------------
    */

    function Get_Created_Actions($Report_ID, $Origin_Table)
    {

        $sql = "SELECT Count(Action_Id) AS TOTAL ".  
               "FROM tblAction_Details ".
               "WHERE Report_Id = [Report_ID] ".
               "AND Origin_Table = '[Origin_Table]' ";

        $sql = str_replace('[Report_ID]', $Report_ID, $sql);
        $sql = str_replace('[Origin_Table]', $Origin_Table, $sql);

	    $rs = db_query($sql);
        
		while($row = $rs->FetchRow())
        {
            $Total = $row["TOTAL"];
		}

		return $Total;
    }






    /*
    |-------------------------------------------------------------------
    | Get_Closed_Actions
    |-------------------------------------------------------------------
    | Description: Retrieve the Message Of The Day
    |
    | Parameters: $Report_ID:- The Calling Table Primary Key Identifier
	|             $Origin_Table:- The table type calling ie Originating_Government | Originating_Other
	|         
	| Returns: Integer of the total actions closed    
    |-------------------------------------------------------------------
    */

    function Get_Closed_Actions($Report_ID, $Origin_Table)
    {

        $sql = "SELECT Count(Action_Id) AS TOTAL ".  
		       "FROM tblAction_Details ". 
			   "WHERE Report_Id = [Report_ID] ".
			   "AND Status != 'Open' ".  
			   "AND Origin_Table = '[Origin_Table]' ";

        $sql = str_replace('[Report_ID]', $Report_ID, $sql);
        $sql = str_replace('[Origin_Table]', $Origin_Table, $sql);

	    $rs = db_query($sql);
        
		while($row = $rs->FetchRow())
        {
            $Total = $row["TOTAL"];
		}

		return $Total;
    }



?>