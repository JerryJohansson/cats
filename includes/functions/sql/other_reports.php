<?PHP

/*
|-----------------------------------------------
| OTHER RECORDS
|-----------------------------------------------
*/


    /*
    |-------------------------------------------------------------------
    | Create_OtherRecords_SQL_InsertString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblOther_Reports
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_OtherRecords_SQL_InsertString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	
	    global $db;		
	
        $sql = "INSERT INTO tblOther_Reports ".
               "(Report_ID, Register_Origin, Date_Completed, Site_ID, Department_ID, Report_Notes, Comments, Record_Type, Reported_By, Location_Docs) ".
               "VALUES ".
               "([SEQUENCE].NextVal, '[REGISTER_ORIGIN]', [DATE_COMPLETED], [SITE_ID], [DEPARTMENT_ID], '[REPORT_NOTES]', '[COMMENTS]', '[RECORD_TYPE]', '[REPORTED_BY]', '[LOCATION_DOCS]')";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $SEQUENCE         = 'TBLOTHER_REPORTS_SEQ';  // Sequence used to get next table identifier value
        $REGISTER_ORIGIN  = 'Audit'; //$Post_Values['REGISTER_ORIGIN']; // Required
        $DATE_COMPLETED   = $db->DBDate(trim($Post_Values['DATE_COMPLETED'])); // Required
        $SITE_ID          = $Post_Values['SITE_ID'];
        $DEPARTMENT_ID    = $Post_Values['DEPARTMENT_ID'];
        $REPORT_NOTES     = $Post_Values['REPORT_NOTES'];
        $COMMENTS         = $Post_Values['COMMENTS'];
        $RECORD_TYPE      = $Post_Values['RECORD_TYPE'];
        $REPORTED_BY        = $Post_Values['REPORTED_BY'];
        $LOCATION_DOCS    = $Post_Values['LOCATION_DOCS'];

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[SEQUENCE]', $SEQUENCE, $sql);
        $sql = str_replace('[REGISTER_ORIGIN]', $REGISTER_ORIGIN, $sql);
        $sql = str_replace('[DATE_COMPLETED]', $DATE_COMPLETED, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);
        $sql = str_replace('[DEPARTMENT_ID]', $DEPARTMENT_ID, $sql);
        $sql = str_replace('[REPORT_NOTES]', $REPORT_NOTES, $sql);
        $sql = str_replace('[COMMENTS]', $COMMENTS, $sql);			
        $sql = str_replace('[RECORD_TYPE]', $RECORD_TYPE, $sql);	
        $sql = str_replace('[REPORTED_BY]', $REPORTED_BY, $sql);	
        $sql = str_replace('[LOCATION_DOCS]', $LOCATION_DOCS, $sql);					


        return $sql;
    }




    /*
    |-------------------------------------------------------------------
    | Create_OtherRecords_SQL_UpdateString
    |-------------------------------------------------------------------
    | Description: Create the sql statement to insert the values into
    |              tblOtherRecords
    |
    | Parameters: $Post_Values: Form post values
    |             $Get_Values: Form get values
    |-------------------------------------------------------------------
    */

    function Create_OtherRecords_SQL_UpdateString($Post_Values, $Get_Values)
    {
	    $Post_Values = ReplaceArrayQuotes($Post_Values);	
	
	    global $db;	
	
        $sql = "UPDATE tblOther_Reports ".
               "SET Date_Completed = [DATE_COMPLETED], ".
               "Site_ID = [SITE_ID], ".
               "Department_ID = [DEPARTMENT_ID], ".
               "Report_Notes = '[REPORT_NOTES]', ".
               "Comments = '[COMMENTS]', ".
               "Record_Type = '[RECORD_TYPE]', ".
               "Reported_By = '[REPORTED_BY]', ".
               "Location_Docs = '[LOCATION_DOCS]' ".
			   "WHERE Report_ID = [REPORT_ID]";


        // ------------------------------
        // Retrieve the post values
        // ------------------------------
        $REPORT_ID        = $Get_Values['id'];  // Sequence used to get next table identifier value
        //$REGISTER_ORIGIN  = 'Audit'; //$Post_Values['REGISTER_ORIGIN']; // Required
        $DATE_COMPLETED   = $db->DBDate(trim($Post_Values['DATE_COMPLETED'])); // Required
        $SITE_ID          = $Post_Values['SITE_ID'];
        $DEPARTMENT_ID    = $Post_Values['DEPARTMENT_ID'];
        $REPORT_NOTES     = $Post_Values['REPORT_NOTES'];
        $COMMENTS         = $Post_Values['COMMENTS'];
        $RECORD_TYPE      = $Post_Values['RECORD_TYPE'];
        $REPORTED_BY        = $Post_Values['REPORTED_BY'];
        $LOCATION_DOCS    = $Post_Values['LOCATION_DOCS'];

        // ------------------------------
        // Replace the identifiers with the form values
        // ------------------------------
        $sql = str_replace('[REPORT_ID]', $REPORT_ID, $sql);
        $sql = str_replace('[REGISTER_ORIGIN]', $REGISTER_ORIGIN, $sql);
        $sql = str_replace('[DATE_COMPLETED]', $DATE_COMPLETED, $sql);
        $sql = str_replace('[SITE_ID]', $SITE_ID, $sql);
        $sql = str_replace('[DEPARTMENT_ID]', $DEPARTMENT_ID, $sql);
        $sql = str_replace('[REPORT_NOTES]', $REPORT_NOTES, $sql);
        $sql = str_replace('[COMMENTS]', $COMMENTS, $sql);			
        $sql = str_replace('[RECORD_TYPE]', $RECORD_TYPE, $sql);	
        $sql = str_replace('[REPORTED_BY]', $REPORTED_BY, $sql);	
        $sql = str_replace('[LOCATION_DOCS]', $LOCATION_DOCS, $sql);	

        return $sql;
    }




    /*
    |-------------------------------------------------------------------
    | Delete_OtherRecords_SQL
    |-------------------------------------------------------------------
    | Description: Delete a record from the table
    |
    | Parameters: $id:- Entry to delete
    |-------------------------------------------------------------------
    */

    function Delete_OtherRecords_SQL($id)
	{
	    $sql = "DELETE FROM tblOther_Reports ".
		       "WHERE Report_ID = [OBLIGATION_ID]";

        $sql = str_replace('[OBLIGATION_ID]', $id, $sql);

		return $sql;
	}

?>