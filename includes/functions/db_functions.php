<?php
//require_once(CATS_CLASSES_PATH . "query.class.php");
require_once(CATS_ADODB_PATH . "adodb.inc.php");
/*
+--------------------------------------------------------------------
|
|		Default Database functions go here
|
+--------------------------------------------------------------------
*/
//$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
$db = NewADOConnection(DB_DRIVER);

function db_connect( $host=DB_HOSTNAME, $dbname=DB_DATABASE, $user=DB_USERNAME, $passwd=DB_PASSWORD, $persist=false ) {
	global $db, $ADODB_FETCH_MODE;

	if ($persist) {
		$db->PConnect($host, $user, $passwd, $dbname)
			or die( 'FATAL ERROR: Connection to database server failed' );
	} else {
		
		$db->Connect($host, $user, $passwd, $dbname)
			or die( 'FATAL ERROR: Connection to database server failed' );
	}

	$ADODB_FETCH_MODE=ADODB_FETCH_ASSOC;
}

function db_error() {
	global $db;
	if (! is_object($db))
	  dprint(__FILE__,__LINE__, 0, "Database object does not exist");
	return $db->ErrorMsg();
}

function db_errno() {
  global $db;
	if (! is_object($db))
	  dprint(__FILE__,__LINE__, 0, "Database object does not exist");
	return $db->ErrorNo();
}

function db_insert_id() {
  global $db;
	if (! is_object($db))
	  dprint(__FILE__,__LINE__, 0, "Database object does not exist");
	return $db->Insert_ID();
}

function db_exec( $sql ) {
  global $db;

	if (! is_object($db))
	  dprint(__FILE__,__LINE__, 0, "Database object does not exist");
	$qid = $db->Execute( $sql );
	dprint(__FILE__, __LINE__, 10, $sql);
	if ($msg = db_error())
	{
		global $AppUI;
		dprint(__FILE__, __LINE__, 0, "Error executing: <pre>$sql</pre>");
		// Useless statement, but it is being executed only on error,
		// and it stops infinite loop.
		$db->Execute( $sql );
		if (!db_error())
			echo '<script language="JavaScript"> location.reload(); </script>';
	}
  if ( ! $qid && preg_match('/^\<select\>/i', $sql) )
	  dprint(__FILE__, __LINE__, 0, $sql);
	return $qid;
}

function db_free_result($cur ) {
	if (! is_object($cur))
	  dprint(__FILE__, __LINE__, 0, "Invalid object passed to db_free_result");
	$cur->Close();
}

function db_num_rows( $qid ) {
	if (! is_object($qid))
	  dprint(__FILE__, __LINE__, 0, "Invalid object passed to db_num_rows");
	return $qid->RecordCount();
}

function db_get_key_value_array($sql){
	$out = array();
	if($arr = db_get_all($sql)){
		foreach($arr as $key => $a){
			if(is_array($a)){
				$out[$a['AKEY']]=$a['AVALUE'];
			}else{
				$out[$key]=$a;
			}
		}
	}//print_r($out);
	return $out;
}

function db_get_all($sql) {
	if($rs = db_query($sql)){
		return $rs->GetAll();
	}else{
		return false;
	}
}

function db_get_array($sql_or_rs) {
	// if its not a string assume we have recordset
	$rs = (is_string($sql_or_rs))? db_query($sql_or_rs) : $sql_or_rs;
	return db_fetch_array($rs);
}

function db_fetch_row( &$qid ) {
	if (! is_object($qid))
	  dprint(__FILE__, __LINE__, 0, "Invalid object passed to db_fetch_row");
	return $qid->FetchRow();
}

function db_fetch_assoc( &$qid ) {
	if (! is_object($qid))
	  dprint(__FILE__, __LINE__, 0, "Invalid object passed to db_fetch_assoc");
  return $qid->FetchRow();
}

function db_fetch_array( &$qid  ) {
	$result = "";
	if (! is_object($qid))
	  dprint(__FILE__, __LINE__, 0, "Invalid object passed to db_fetch_array");
	else
  	  $result = $qid->FetchRow();
	// Ensure there are numerics in the result.
	if ($result && ! isset($result[0])) {
	  $ak = array_keys($result);
	  foreach ($ak as $k => $v) {
	    $result[$k] = $result[$v];
	  }
	}
	return $result;
}

function db_fetch_object( $qid  ) {
	if (! is_object($qid))
	  dprint(__FILE__, __LINE__, 0, "Invalid object passed to db_fetch_object");
	return $qid->FetchNextObject(false);
}

function db_escape( $str ) {
  global $db;
	return substr($db->qstr( $str ), 1, -1);
}

function db_version() {
  return "ADODB";
}

function db_unix2dateTime( $time ) {
	global $db;
	return $db->DBDate($time);
}

function db_dateTime2unix( $time ) {
	global $db;
	return $db->UnixDate($time);
}

function db_query($sql){
//echo($sql);
	global $db;
	return $db->Execute($sql);
}

function db_exec_query($sql){
	global $db;
	return $db->_exec_query_string($sql);//Execute($sql);
}

function db_get_one($sql,$field=0){
	global $db;
	//$result = db_query($sql);
	return $db->GetOne($sql);
}
function db_get_field_value($sql, $field=0){
	global $db;
	$result = db_query($sql);
	if(is_array($field))
	{
		$ret = array();
		$arr = db_fetch_array($result);
		for($i=0;$i<count($field);$i++)
		{
			$ret[strtoupper($field[$i])] = $arr[strtoupper($field[$i])];
		}
		return $ret;
	}
	else
	{
		$arr = db_fetch_array($result);
		return $arr[strtoupper($field)];
	}
}

function db_insert($sql, $arr=false, $table_seq='', $identity=0){
	global $db;
	$rs=$db->Execute($sql,$arr);
	if($rs){
		if($table_seq!='')
			return $db->Insert_ID($table_seq,$identity);
		else
			return true;
	}
	return false;
}

// Creates an sql statement which is binded to an array of PKey values
// - only good for deleting records based on PKey and where you
// are deleting records where PKey = PKeyValue e.g. delete from table_name where id = 555 or something like that
// This is used to delete multiple records and is a hell of a lot faster than inline sql because
// it gets executed at the database level...kinda like executing a stored proc.
function db_bulk_delete($table, $field_id, $values, $dbg=false){
	global $db;
	$db->debug=$dbg;
	$old_bindInputArray=$db->_bindInputArray;
	$db->_bindInputArray=false;
	$db->bulkBind=true;
	$ok = $db->Execute("delete from $table where $field_id = ?", $values);
	$db->_bindInputArray=$old_bindInputArray;
	return (bool)$ok;
}
// Creates an sql statement which is binded to an array of values e.g. array(val1,val2,val3...)
// This is used to delete multiple records and is a hell of a lot faster than inline sql because
// it gets executed at the database level...kinda like executing a stored proc.
//un comment dev
function db_bulk_insert_dev($table, $fields, $values, $dbg=false){
	
	global $db;
	$db->debug=$dbg;
	$mappedFields = [];
		
	$flds = explode(",",$fields);
	for ($i = 0; $i < count($flds); $i++) {
		$mappedFields[] = ':'. strtolower($flds[$i]);
		
	}

	// $flds=explode(",",$fields);
	$vals=str_repeat("?,",count($flds));
	$vals=preg_replace("/,$/","",$vals);
	$old_bindInputArray=$db->_bindInputArray;
	// $db->_bindInputArray=false;
	$mappedValues = $values;

	// $ok = $db->Execute("insert into $table ($fields) values (". join(',', $mappedFields) . ")", $mappedValues, true);remove oci8 call change itas below
	$ok = $db->Execute_DEV("insert into $table ($fields) values (". join(',', $mappedFields) . ")", $mappedValues, true);
	
	$db->_bindInputArray=$old_bindInputArray;
	return (bool)$ok;
}

// function db_bulk_insert_mod($table, $fields, $values, $dbg=false){
// 	global $db;
// 	$db->debug=$dbg;
// 	$mappedFields = [];
// 	$mappedValues = [];
// 	$extractedValues = array_shift($values);
// 	$flds = explode(",",$fields);
// 	for ($i = 0; $i < count($flds); $i++) {
// 		$mappedFields[] = ':'. strtolower($flds[$i]);
// 		$mappedValues[strtolower($flds[$i])] =  $extractedValues[$i];
// 	}
// 	// var_dump($table);die();
// 	// $flds=explode(",",$fields);
// 	$vals=str_repeat("?,",count($flds));
// 	$vals=preg_replace("/,$/","",$vals);
// 	$old_bindInputArray=$db->_bindInputArray;
// 	// $db->_bindInputArray=false;
// 	$ok = $db->Execute("insert into $table ($fields) values (". join(',', $mappedFields) . ")", $mappedValues);
// 	$db->_bindInputArray=$old_bindInputArray;
// 	return (bool)$ok;
// }

function db_bulk_insert($table, $fields, $values, $dbg=false){
	global $db;
	$db->debug=$dbg;
	$flds=explode(",",$fields);
	$vals=str_repeat("?,",count($flds));
	$vals=preg_replace("/,$/","",$vals);
	$old_bindInputArray=$db->_bindInputArray;
	$db->_bindInputArray=false;
	$db->bulkBind=true;
		
	$ok = $db->Execute("insert into $table ($fields) values ($vals)", $values);
	
	$db->_bindInputArray=$old_bindInputArray;
	return (bool)$ok;
}

// Same as above but the update version ...
// NOTE: not tested yet...haven't had the time - cross my fingers and hope for the best ;)
function db_bulk_update($table, $fields, $values, $dbg=false){
	global $db;
	$db->debug=$dbg;
	$flds=explode(",",$fields);
	$vals=str_repeat("?,",count($flds));
	$vals=preg_replace("/,$/","",$vals);
	$old_bindInputArray=$db->_bindInputArray;
	$db->_bindInputArray=false;

	$id = 0; $i = 0;
	$stmt = $db->PrepareSP( "update {$table} set val=:i where id=:id");
	$db->Parameter($stmt,$id,'id');
	$db->Parameter($stmt,$i, 'i');
	for ($cnt=0; $cnt < 1000; $cnt++) {
		$id = $cnt;
		$i = $cnt * $cnt; //# works with oci8!
		$db->Execute($stmt);
	}
	$db->_bindInputArray=$old_bindInputArray;
	return (bool)$ok;
}

function db_render_pager($sql, $column_labels=false, $column_attributes=false, $title = '', $sort_enabled=false, $order_by_id='', $order_direction = '', $row_count=CATS_DEFAULT_ROW_COUNT){
	// NOTE: do not use order by clause in sql when sort_enabled is true
	//  instead - there are two params to pass into Pager - order_by_id = 'FIELD_ID' and order_direction = 'ASC'
	global $db,$m; 
	require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
	//&$db, $sql, $id = 'cats', $showPageLinks = true, $columnAttributes = false, $sort_enabled = true, $order_by_id = '', $order_direction = ''



	$pager = new ADODB_Pager($db,$sql,$m,true,$column_attributes,$sort_enabled,$order_by_id,$order_direction);

	if(!empty($title)){
		$pager->caption_title=$title;
	}
	if(is_array($column_labels)){
		$pager->gridHeader=$column_labels;
	}
	$pager->Render($row_count);
}

function db_render_pager_links($sql, $column_labels=false, $column_attributes=false, $links=false, $title = '', $sort_enabled=false, $order_by_id='', $order_direction = '', $row_count=CATS_DEFAULT_ROW_COUNT){
	// NOTE: do not use order by clause in sql when sort_enabled is true
	//  instead - there are two params to pass into Pager - order_by_id = 'FIELD_ID' and order_direction = 'ASC'
	global $db,$m;
	require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
	//&$db, $sql, $id = 'cats', $showPageLinks = true, $columnAttributes = false, $sort_enabled = true, $order_by_id = '', $order_direction = ''
	$pager = new ADODB_Pager($db,$sql,$m,true,$column_attributes,$sort_enabled,$order_by_id,$order_direction);
	$pager->linkArray = $links;
	if(!empty($title)){
		$pager->caption_title=$title;
	}
	if(is_array($column_labels)){
		$pager->gridHeader=$column_labels;
	}
	$pager->Render($row_count);
}

function db_close() {
	global $db;

	$db->_close();
}
/*
+--------------------------------------------------------------------
|		END common db functions
+--------------------------------------------------------------------
*/

/*
+------------------------------------------------------------------------
|  BEGIN:: DB Append to where clause
|		This function is used by search result pages
+------------------------------------------------------------------------
*/
function db_get_where_clause($column_name, &$sql_where, $operator=' = ', $delim=''){
	global $db;
	if(isset($_POST[$column_name]) && !empty($_POST[$column_name])){
		$sql_where .= ($sql_where == "")?" WHERE ":" AND ";
		$column_value = $_POST[$column_name];
		$operator_type = trim(strtoupper($operator));
		switch($operator_type){
			case 'LIKE':
				$sql_where .=  " lower($column_name)  $operator lower('%$column_value%') ";
				break;
			case 'IN':
				// create the in clause using an array or comma delimited string
				if(!is_array($column_value)) {
					// using comma delimited string
					// remove single quotes(if any) and make an array using the value
					$column_value = explode( "," , preg_replace("/'/","",$column_value) );
				}
				// create a string with delimiters omitting the outer delimiters as this is added when we
				// finish the in clause off...
				$column_value = implode("$delim,$delim",$column_value);
				$sql_where .=  $column_name . $operator . " (" . $delim . $column_value . $delim . ") ";
				break;
			case 'DT':
				// match a date...we must use the DBDate method for compatibility with other databases
				$delim='';
				$sql_where .=  $column_name . " = " . $delim . $db->DBTimeStamp(trim($column_value)) . $delim;
				break;
			case 'DATE':
				// match a date...we must use the DBDate method for compatibility with other databases
				$delim='';
				$sql_where .=  $column_name . " = " . $delim . $db->DBDate(trim($column_value)) . $delim;
				break;
			case 'DT_FROM':
				// posted field name must match column name with "_FROM" appended to the end
				// e.g. :
				// db column name=DATE_COLUMN
				// form field name=DATE_COLUMN_FROM
				$delim='';
				$sql_where .=  substr($column_name,0,-5) . " >= " . $delim . $db->DBTimeStamp(trim($column_value)) . $delim;
				break;
			case 'DATE_FROM':
				// posted field name must match column name with "_FROM" appended to the end
				// e.g. :
				// db column name=DATE_COLUMN
				// form field name=DATE_COLUMN_FROM
				$delim='';
				$sql_where .=  substr($column_name,0,-5) . " >= " . $delim . $db->DBDate(trim($column_value)) . $delim;
				break;
			case 'DT_TO':
				// posted field name must match column name with "_TO" appended to the end
				// e.g. :
				// db column name=DATE_COLUMN
				// form field name=DATE_COLUMN_TO
				$delim='';
				$sql_where .=  substr($column_name,0,-3) . " <= " . $delim . $db->DBTimeStamp(trim($column_value)) . $delim;
				break;
			case 'DATE_TO':
				// posted field name must match column name with "_TO" appended to the end
				// e.g. :
				// db column name=DATE_COLUMN
				// form field name=DATE_COLUMN_TO
				$delim='';
				$sql_where .=  substr($column_name,0,-3) . " <= " . $delim . $db->DBDate(trim($column_value)) . $delim;
				break;
			case 'MASK':
				// match a bit mask using bitand() function
				$delim='';
				$mask_value = (is_array($column_value)) ? main_list2mask( implode(",", $column_value) ) : (int)$column_value ;
				$sql_where .=  " (bitand($column_name, $mask_value)>0) ";
				break;
			case 'NUM_FROM':
				// posted field name must match column name with "_FROM" appended to the end
				// e.g. :
				// db column name=DATE_COLUMN
				// form field name=DATE_COLUMN_FROM
				$delim='';
				$sql_where .=  substr($column_name,0,-5) . " >= " . $delim . $column_value . $delim;
				break;
			case 'NUM_TO':
				// posted field name must match column name with "_TO" appended to the end
				// e.g. :
				// db column name=DATE_COLUMN
				// form field name=DATE_COLUMN_TO
				$delim='';
				$sql_where .=  substr($column_name,0,-3) . " <= " . $delim . $column_value . $delim;
				break;
			default:
				$sql_where .=  $column_name . $operator . $delim . $column_value . $delim;
				break;
		}
	}
	//return $sql_where;
}
/*
+------------------------------------------------------------------------
|  END:: db_get_where_clause()
+------------------------------------------------------------------------
*/


/*
+------------------------------------------------------------------------
|  BEGIN:: DB User Helper Functions
+------------------------------------------------------------------------
*/
function db_get_user_sites_where_string($column_names = ''){
	$where = '';
	if(is_array($_SESSION['user_details'])){
		$multi_sites = $_SESSION['user_details']['site_access'];
		$users_site = $_SESSION['user_details']['site_id'];
		if($column_names!=''){
			if($multi_sites!=''){
				$asites = explode(",",$column_names);
				$x = count($asites);
				$cnt = 0;
				for($i=0; $i<$x; $i++){
					if($cnt == 0){$where = "(";}//start the where string
					if($cnt >0){$where .= " OR ";}
					$where .= "(" . $asites[$i] . " IN ($multi_sites))";
					$cnt++;
				}
				if($cnt >0){$where .= ")";}//close the where string
			}else{
				$where = $users_site . " IN ($column_names)";
			}
		}else{
			if($multi_sites!=''){
				$where = " site_id in($multi_sites) ";
			}else{
				$where = " site_id = $users_site ";
			}
		}
	}
	return $where;
}

/*
+------------------------------------------------------------------------
|  END:: DB User Helper Functions
+------------------------------------------------------------------------
*/



/*
+--------------------------------------------------------------------
|
|		Default DB Helper functions go here
|
+--------------------------------------------------------------------
*/
function db_query_error_log($query) {
	global $db;

	if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
		error_log('QUERY ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
	}

	$result = db_query($query) or db_error();

	if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
		 $result_error = db_error();
		 error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
	}

	return $result;
}

function db_perform($table, $data, $action = 'insert', $parameters = '') {
	reset($data);
	if ($action == 'insert') {
		$query = 'insert into ' . $table . ' (';
		while (list($columns, ) = each($data)) {
			$query .= $columns . ', ';
		}
		$query = substr($query, 0, -2) . ') values (';
		reset($data);
		while (list(, $value) = each($data)) {
			switch ((string)$value) {
				case 'now()':
					$query .= 'now(), ';
					break;
				case 'null':
					$query .= 'null, ';
					break;
				default:
					$query .= '\'' . db_input($value) . '\', ';
					break;
			}
		}
		$query = substr($query, 0, -2) . ')';
	} elseif ($action == 'update') {
		$query = 'update ' . $table . ' set ';
		while (list($columns, $value) = each($data)) {
			switch ((string)$value) {
				case 'now()':
					$query .= $columns . ' = now(), ';
					break;
				case 'null':
					$query .= $columns .= ' = null, ';
					break;
				default:
					$query .= $columns . ' = \'' . db_input($value) . '\', ';
					break;
			}

		}
		$query = substr($query, 0, -2) . ' where ' . $parameters;
	}
	return db_query($query);
}

function db_output($string) {
	return htmlspecialchars($string);
}

function db_input($string) {
	$string = addslashes($string);
	return $string;
}

function db_escape_field_value($name,$value){
	return db_escape_string($value);
}

function db_escape_string($string) {
	if(empty($string)) return "";
	return stripslashes($string);
}

function db_sanitize($string){
	if(empty($string)) return "";
	global $db;
	return $db->qstr($string,get_magic_quotes_gpc());
}

function db_prepare_input($string) {
	if (is_string($string)) {
		return trim(main_sanitize_string(stripslashes($string)));
	} elseif (is_array($string)) {
		reset($string);
		while (list($key, $value) = each($string)) {
			$string[$key] = db_prepare_input($value);
		}
		return $string;
	} else {
		return $string;
	}
}

function db_prepare_array($fields,$names,$values) {
	$sql_data = array();
	if(is_array($fields))
	{
		for($i=0;$i<count($fields);$i+=2)
		{
			$val = (isset($values[$names[$i]]))?$values[$names[$i]]:$fields[$i+1];
			switch((string)$names[$i+1])
			{
				case 'date':
					$val = ($val!='') ? date_raw($val) : CATS_DB_DATE ;
					break;
				case 'datetime':
					$val = ($val!='') ? db_putdate($val) : CATS_DB_DATE ;
					break;
				case 'mask':
					// Requires security functions to be loaded
					$val = (is_array($val)) ? main_list2mask( implode(",", $val) ) : $val ;
					break;
				default:
					$val = $val;
					break;
			}

			$sql_data[$fields[$i]] = db_prepare_input($val);
		}
	}
	else
	{
		$sql_data[] = db_prepare_input($val);
	}
	dprint(__FILE__, __LINE__, 4, "sql_data(".serialize($sql_data).")");
	return $sql_data;
}

function db_getdate($sdate='',$ts=false,$format='j-M-Y')
{
	if($sdate=='')
	{
		$sdate = time();
		$ts = true;
	}
	//echo("sdate=$sdate<br>");
	if(!$ts) $sdate = strtotime($sdate);
	return date($format,$sdate);
}

function db_sql_date($col,$ftype='short'){
	return "TO_CHAR($col,'".(($ftype=='short')?CATS_DATE_FORMAT_SHORT:CATS_DATE_FORMAT_LONG)."')";
}
/*
+---------------------------------------------------------------------------
|	Output a raw date string in the selected locale date format
|	$raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
|	NOTE: Includes a workaround for dates before 01/01/1970 that fail on windows servers
+---------------------------------------------------------------------------
*/
	function cats_date_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || empty($raw_date) ) return false;

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
			//echo("done with date: $year, $month, $day, $hour, $minute, $second <br>".date(CATS_APP_DATE_FORMAT_SHORT, mktime($hour, $minute, $second, $month, $day, $year)).'<br>');
      return date(CATS_APP_DATE_FORMAT_SHORT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
			//echo("done with date raw_date = $raw_date: $year, $month, $day, $hour, $minute, $second <br>");
      return ereg_replace('2037' . '$', $year, date(CATS_APP_DATE_FORMAT_SHORT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
  }


function db_put_date($d)
{
	if (empty($d) && $d !== 0) return CATS_DB_DATE;
	if (is_string($d)) $d = db_check_date($d);
	//echo("d=$d<br>");
	return "TO_DATE('$d','".CATS_DATE_FORMAT_SHORT."')";
}

function db_put_datetime($d)
{
	if (empty($d) && $d !== 0) return CATS_DB_DATE;
	//if (is_string($d)) $d = db_check_date($d);
	return "TO_DATE('$d','".CATS_DATE_FORMAT_LONG."')";
}

function db_putdate($sdate='',$ts=false,$format='Y-m-d H:i:s')
{
	if(!$ts) $sdate = strtotime($sdate);
	return date($format,$sdate);
}

function db_put_ts($date)
{
	$ret = strtotime($date);
	if($ret==-1) return $date;
	else return $ret;
}



function db_get_tables() {
	return db_get_cat_details();
}

function db_get_views() {
	return db_get_cat_details('VIEW');
}

function db_get_columns($table_name){
	global $db;
	if(empty($table_name)) return false;
	$table = strtoupper($table_name);
	$sql = "select cname,coltype,width, SCALE, PRECISION, NULLS, DEFAULTVAL from col where tname='$table' order by colno";
	//$db->debug=true;
	$rs = $db->Execute($sql);
	$ret = $rs->GetAll();
	return $ret;
}

function db_get_cat_details($type = 'TABLE', $prefix='', $dim2 = false) {
	global $db;
	$ret = array();
	$rs = $db->Execute("select table_name from cat where table_type = '$type' and table_name like '$prefix%'");
	while(!$rs->EOF){
		$tblname=$rs->Fields("TABLE_NAME");
		if($prefix!=''){
			if(preg_match("/^".$prefix."/",$tblname)) $tbl=($dim2)?array('id'=>$tblname,'text'=>$tblname):$tblname;
		}else $tbl=($dim2)?array('id'=>$tblname,'text'=>$tblname):$tblname;
		$ret[]=$tbl;
		$rs->MoveNext();
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|	Returns the parent nodes pagetype
+---------------------------------------------------------------------------
*/
//db_draw_pull_down_menu('type', db_get_pull_down_options("select name, insert(`name`,1,1,substring(upper(name),1,1)) from ".TABLE_form_types." where status > 0 "), $type, (($action=="edit")?"disabled":""));
function db_get_parent_pagetype($pid) {
	$arr=db_get_allowed_pagetype($pid);
	return $arr[0][0];
}

function db_get_pagetype($type){
	return db_getfieldvalue("select name from form_types where id=$type");
}


function db_get_allowed_pagetype($pid) {

	// global ADOBD object
	global $db;

	$ret=array();
	$sql="select nt.name, nt.allowed_child_types as types from form_fields n, form_types nt where n.field_type_id=nt.id and n.field_id=$pid";
	//echo($sql);
	$rs = $db->Execute($sql);
	while($row = $rs->FetchRow())
	{
	//print_r($row);
		$ret[]=array($row['NAME'], $row['TYPES']);
	}
	return $ret;
}
function db_get_allowed_pagetype_matrix() {
	$ret=array();
	$result = db_query("select nt.name, nt.allowed_child_types as types from form_types nt where nt.status > 0 ");
	while($row = db_fetch_array($result))
	{
		$ret[]=array($row['NAME'], $row['TYPES']);
	}
	return $ret;
}

/*
+------------------------------------------------------------------------
|
|  Function: db_insert_history_log
|  Purposes: Inserts a record into the user_history_log table
|  Returns: Boolean true for success or false if something went wrong
|  Params:
|    (string)log_type = login, log, error, ...
|    (string)id = employee_number
|
+------------------------------------------------------------------------
*/
function db_insert_history_log($log_type='log',$uid){
/* DESC USER_HISTORY_LOG
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID                                        NOT NULL NUMBER
 LOG_TYPE                                           VARCHAR2(50)
 SESSION_ID                                         VARCHAR2(50)
 EMPLOYEE_NUMBER                                    NUMBER
 FORM_ID                                            NUMBER
 FORM_NAME                                          VARCHAR2(128)
 IP                                                 VARCHAR2(15)
 REQUEST                                            VARCHAR2(255)
 DATE_TIME                                          DATE
 MSG                                                VARCHAR2(255)
*/
	$sess = session_id();
	$ip = main_get_ip_address();

	// take a copy of the $_GET array
	$reqarr = array_diff($_GET,array());
	// Hide the Password
	$reqarr['pwd'] = '**********';
	// Serialize the Copy
	$req = serialize($reqarr);
	if(count($_POST) > 1){
		$post = serialize($_POST);
		if(strlen($post)>1000){
			if(count($_GET) > 1){
				$req = serialize($_GET);
			}
		}else{
			$req = $post;
		}
	}

	$sql = "insert into user_history_log (log_type, session_id, employee_number, ip, request)";
	$sql .= " values ('$log_type','$sess',$uid,'$ip','$req')";

	return db_insert($sql);
}

/*
+------------------------------------------------------------------------
|
|  Function: db_insert_error_log
|  Purposes: Inserts a record into the user_history_log table
|  Returns: Boolean true for success or false if something went wrong
|  Params:
|    (array or false)params = assoc. array where the keys match the tables column names
|    (string)file = the filename where the error occured... the __FILE__ constant should do it ;)
|    (int)line = the approx. line number where error happened... __LINE__ constant will do
|    (string)type = type of error. Please keep these lower case. Possibilities are ("error","warning","info","mail","select","insert","update","debug","permission","system","user")
|    (string)msg = the message you want to store
|
+------------------------------------------------------------------------
*/
function db_insert_error_log($params=false, $file='N/A',$line=0,$type='error',$msg='N/A'){
/* DESC ERROR_LOG
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 ID                                        NOT NULL NUMBER
 FILE_NAME                                          VARCHAR2(500)
 LINE_NUMBER                                        NUMBER(10)
 ERROR_TYPE                                         VARCHAR2(50)
 ERROR_MESSAGE                                      VARCHAR2(500)
 SESSION_ID                                         VARCHAR2(50)
 EMPLOYEE_NUMBER                                    NUMBER
 IP                                                 VARCHAR2(15)
 POST_INFO                                          VARCHAR2(2000)
 DATE_TIME                                          DATE
*/
	global $db;
	$sql = '';// wonder what this could be ????
	// serialize the request object and truncate to 2000 characters if needed
	$req = serialize($_REQUEST);
	if(strlen($req)>2000) $req = substr($req, 0, 2000);
	$sess = session_id();
	$ip = main_get_ip_address();
	$uid = (int)$_SESSION['user_details']['user_id'];
	if(is_array($params)){
		// set default data and merge with $params
		// the values in the $params array will be overwrite any of the default values if the corresponding keys exist
		$sql_data = array_merge(array(
			'FILE_NAME'=>$file,'LINE_NUMBER'=>$line,'ERROR_TYPE'=>$type,'ERROR_MESSAGE'=>$msg,
			'SESSION_ID'=>$sess,
			'EMPLOYEE_NUMBER'=>$uid,
			'IP'=>$ip,
			'POST_INFO'=>$req
		),$params);
		// create the sql for insert
		$rs=$db->Execute("select * from error_log where id=1");
		$sql=$db->GetInsertSQL($rs);
	}else{
		$req = db_sanitize($req);
		$sql = "insert into error_log (file_name,line_number,error_type,error_message,session_id,employee_number,ip,post_info) ";
		$sql .= " values('$file',$line,'$type','$msg',";
		$sql .= "'$sess',$uid,'$ip','$req') ";
	}
	$db->Execute($sql);
}

// DEBUG FUNCTIONS
function db_dump_results($rs) {
	print "<pre>";
	print_r($rs->GetRows());
	print "</pre>";
}

// since this page is loaded we assume database functionality is need so lets
// autoconnect for convenients sake...but only if all connection constants are defined
if(defined('DB_HOSTNAME') && defined('DB_DATABASE') && defined('DB_USERNAME') && defined('DB_PASSWORD'))
	db_connect();
?>