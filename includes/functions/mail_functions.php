<?php

/*
+------------------------------------------------------------------------
|
|  Function: email_pcr_send_notification
|  Purposes: Sends email to PCR Originator of an PCR
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)pcr_id = action id
|
+------------------------------------------------------------------------
*/
function email_pcr_send_notification($action,$pcr_id){
	//*********************** Form variables used in email creation **********************
	// get the form variables to insert into the email message
	$sql = "SELECT * FROM tblpcr_details WHERE pcr_id=".$pcr_id;
	$rs = db_query($sql);
	// build the array of fields
	$arr=array();
	$row = $rs->FetchRow();
	$fcnt=$rs->FieldCount();
	for($i=0;$i<$fcnt;$i++){
		// get field object so we know what we are dealing with
		$fld=$rs->FetchField($i);
		// get the field type
		$type = $rs->MetaType($fld->type);
		switch($type){
			case 'D':case 'T': // format the date value
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'N':case 'I': // don't really need this one as this is the same as the default case
				$arr[$fld->name] = $row[$fld->name];
				break;
			case 'C':case 'X': // format text value
				$arr[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
				break;
			default: // default value is simply the value of the field
				$arr[$fld->name] = $row[$fld->name];
				break;
		}
	}
	
	// Get the email Configuration
	$sql = "SELECT * FROM tblEmailOptions";
	$email_config = db_get_array($sql);
	
	//$_SESSION['messageStack']->add("Tony testing, Action = ".$action,"success");
	
	// Set return code to successful, will set to false later if it actually tries to do something and fails 
	$ret = true;	
		
	//if ($arr['SITE_ID'] == 5) {
		switch($action){
			case "add": // Add record
				//$ret = mail_notify_originator_add($arr['ORIGINATOR_ID'],$arr['SITE_ID'],$arr['PCR_ID'],$arr['PCR_DISPLAYID'],$arr['TITLE'],$email_config);
				$ret = true;
				break;
				
			case "edit": // Update record

				// retrieve sign off details
				$sql = "SELECT * FROM tblpcr_signoff WHERE pcr_id=".$pcr_id;
				$rs = db_query($sql);
				// build the array of fields
				$soff=array();
				$row = $rs->FetchRow();
				$fcnt=$rs->FieldCount();
				for($i=0;$i<$fcnt;$i++){
					// get field object so we know what we are dealing with
					$fld=$rs->FetchField($i);
					// get the field type
					$type = $rs->MetaType($fld->type);
					switch($type){
						case 'D':case 'T': // format the date value
							$soff[$fld->name] = $row[$fld->name];
							break;
						case 'N':case 'I': // don't really need this one as this is the same as the default case
							$soff[$fld->name] = $row[$fld->name];
							break;
						case 'C':case 'X': // format text value
							$soff[$fld->name] = addslashes(($row[$fld->name]));//htmlentities($row[$fld->name]);
							break;
						default: // default value is simply the value of the field
							$soff[$fld->name] = $row[$fld->name];
							break;
					}
				}
	
				// If approved or cancelled then send e-mail to originator
				if ($soff['SIGN_OFF'] == 'Approved' or $soff['SIGN_OFF'] == 'Cancelled') {
					$ret = mail_notify_originator_update($arr['ORIGINATOR_ID'],$arr['SITE_ID'],$arr['PCR_ID'],$arr['PCR_DISPLAYID'],$arr['TITLE'],$soff['SIGN_OFF'],$email_config);
				}
				break;
		}
	//} else {
	//	$ret = true;
	//}
	
	return $ret;
}

/*
+------------------------------------------------------------------------
|
|  Function: mail_notify_originator_add
|  Purposes: Sends an email to originator of a pcr
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)id = employee_number
|    (int)site_id = site_id
|    (int)pcr_id = pcr id
|    (string)pcr_displayid = pcr display id
|    (string)pcr_title = action title
|    (array)email_config = email configuration details
|
+------------------------------------------------------------------------
*/
function mail_notify_originator_add($id,$site_id,$pcr_id,$pcr_displayid,$pcr_title,$email_config){
	//************************************ Originator Email **************************************
	$ret = true;
	//*********** create the recordset *************
	global $db;
	$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
	$rs_emp = db_get_array($sql);
	$emp_name = $rs_emp["EMP_NAME"];
	$to = $rs_emp["EMAIL_ADDRESS"];
	
	//get the email from address for this persons site
	$sql = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " . $site_id;
	$from = db_get_one($sql);
	
	$subject = "PCR Has Been Registered";
	$message = "This is an automatic message from CATS notifying you that the PCR below has been registered. Click on the link to see full details of the PCR.\n\n";
	$message .= "PCR No.:      " . $pcr_displayid . "\n";
	$message .= "PCR Title:    " . $pcr_title . "\n";
	$message .= "Date Entered: " . date("d-m-Y") . "\n";
	$message .= "Link:         " . WS_PATH . "index.php?m=pcr_search&p=view&id=" . $pcr_id. "\n";
	//$message .= "Link:         " . WS_PATH . "\n\n";
		
//	if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
//		$to = $email_config["EMAIL_TESTADDRESS"];
//		$emp_name = $email_config["EMAIL_TESTNAME"];
//		$from = $email_config["EMAIL_FROMADDRESS"];
//	}
	//echo("<br>To=".$to);
	$IsEmail_1 = strpos($to,"@");
	$IsEmail_2 = strpos($from,"@");
			
	if($IsEmail_1 > 0 && $IsEmail_2 > 0){
		if($ret = _mail_send($to,$subject,$message,false,4,'','',$from,false))
			$_SESSION['messageStack']->add("Notification has been sent to the PCR Originator ($to).","success");
		else
			dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $to , From: $from");
	}else{
		$ret=false;
		dprint(__FILE__,__LINE__,0,"Error:" . __FUNCTION__ . " - One or both of the Email addresses is incomplete<br>To:".$to."<br>From:".$from);
	}
	return (bool)$ret;
}

/*
+------------------------------------------------------------------------
|
|  Function: mail_notify_originator_add
|  Purposes: Sends an email to originator of a pcr
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)id = employee_number
|    (int)site_id = site_id
|    (int)pcr_id = pcr id
|    (string)pcr_displayid = pcr display id
|    (string)pcr_title = action title
|    (array)email_config = email configuration details
|
+------------------------------------------------------------------------
*/
function mail_notify_originator_update($id,$site_id,$pcr_id,$pcr_displayid,$pcr_title,$sign_off,$email_config){
	//************************************ Originator Email **************************************
	$ret = true;
	//*********** create the recordset *************
	global $db;
	
	// Get Sign off Details
	$sql = "SELECT * FROM tblpcr_signoff WHERE pcr_id = " . $pcr_id;
	$rs_signoff = db_get_array($sql);
	$signoff_date = $rs_signoff["SIGNED_OFF_DATE"];
	$originator_notified = $rs_signoff["ORIGINATOR_NOTIFIED"];
	
	// Has is been signed off already ?
	if(($signoff_date == "") or ($originator_notified != "")){
		return (bool)$ret;
	}
		
	// Get Employee Details
	$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
	$rs_emp = db_get_array($sql);
	$emp_name = $rs_emp["EMP_NAME"];
	$to = $rs_emp["EMAIL_ADDRESS"];
	
	//get the email from address for this persons site
	$sql = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " . $site_id;
	$from = db_get_one($sql);
	
	// Get the Email Details
	//$sql = "SELECT * FROM tblpcr_email WHERE site_id = " . $site_id . " AND screen_id = 8 AND person_description = 'Originator'";
	//$rs_eml = db_get_array($sql);
		
	//$subject = $rs_eml["SUBJECT"];
	//$message = $rs_eml["MESSAGE"];
	$subject = "A PCR Has Been Closed and Signed Off - (" . $sign_off . ")" ;
	$message = "This is an automatic message from CATS notifying you that the below PCR has been closed and signed off. Click on the link to see full details of the PCR.";
	$message .= "\n\n";
	$message .= "PCR No.:      " . $pcr_displayid . "\n";
	$message .= "PCR Title:    " . $pcr_title . "\n";
	$message .= "Sign Off:     " . $sign_off . "\n";
	$message .= "Link:         " . WS_PATH . "index.php?m=pcr_search&p=view&id=" . $pcr_id. "\n";
	//$message .= "Link:         " . WS_PATH . "\n\n";
		
//	if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
//		$to = $email_config["EMAIL_TESTADDRESS"];
//		$emp_name = $email_config["EMAIL_TESTNAME"];
//		$from = $email_config["EMAIL_FROMADDRESS"];
//	}
	//echo("<br>To=".$to);
	$IsEmail_1 = strpos($to,"@");
	$IsEmail_2 = strpos($from,"@");
			
	if($IsEmail_1 > 0 && $IsEmail_2 > 0){
		if($ret = _mail_send($to,$subject,$message,false,4,'','',$from,false))
			$_SESSION['messageStack']->add("Notification has been sent to the PCR Originator ($to).","success");
		else
			dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $to , From: $from");
	}else{
		$ret=false;
		dprint(__FILE__,__LINE__,0,"Error:" . __FUNCTION__ . " - One or both of the Email addresses is incomplete<br>To:".$to."<br>From:".$from);
	}
	
	// Update Notification Flag
	if($ret == true){
		$sql = "UPDATE tblpcr_signoff SET originator_notified = SYSDATE WHERE pcr_id = " . $pcr_id;
		$db->Execute($sql);
	}
	
	return (bool)$ret;
}

/*
+------------------------------------------------------------------------
|
|  Function: email_action_send_notification
|  Purposes: Sends emails to managed by person and allocated persons of an action
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)action_id = action id
|    (array)POST = Posted Data
|
+------------------------------------------------------------------------
*/
function email_action_send_notification($action_id, $POST){
	//return true;
	//*********************** Form variables used in email creation **********************
	// get the form variables to insert into the email message
	$due_date = $POST["SCHEDULED_DATE"];
	$action_title = $POST["ACTION_TITLE"];
	$action_desc = $POST["ACTION_DESCRIPTION"];
	$closing_comment = $POST["RECORD_COMMENTS"];
	$managed_by_id = $POST["MANAGED_BY_ID"];
	$managed_by_id_old = $POST["MANAGED_BY_ID_OLD"];
	$allocated_to_id = $POST["ALLOCATED_TO_ID"];
	$allocated_to_id_old = $POST["ALLOCATED_TO_ID_OLD"];
	$notify_allocated_email = $POST["NOTIFY_ALLOCATED_EMAIL"];
	$allocated_send_email = ($notify_allocated_email==1)?'Yes':'No';
	$notify_managed_email = $POST["NOTIFY_MANAGED_EMAIL"];
	$send_email = ($notify_managed_email==1)?'Yes':'No';
	$status = $POST["STATUS"];
	$status_old = $POST["STATUS_OLD"];
	$register_origin = $POST['REGISTER_ORIGIN'];
	$origin_table = $POST['ORIGIN_TABLE'];
	$report_id = $POST['REPORT_ID'];

	$sql = "SELECT * FROM tblEmailOptions";
	$email_config = db_get_array($sql);

	$ret = mail_notify_managed_by($managed_by_id,$managed_by_id_old,$send_email,$status,$action_id,$action_title,$action_desc,$due_date,$email_config);
	$ret = mail_notify_allocated_to($allocated_to_id,$allocated_to_id_old,$allocated_send_email,$status,$action_id,$action_title,$action_desc,$due_date,$email_config);
	$ret = mail_notify_managed_by_closed($managed_by_id,$status,$status_old,$action_id,$action_title,$action_desc,$due_date,$closing_comment,$email_config);

	// If PCR Action
	if($register_origin == 'PCR Action'){
		$ret = mail_notify_admin_to_closed($allocated_to_id,$origin_table,$report_id,$status,$status_old,$action_id,$action_title,$action_desc,$due_date,$closing_comment,$email_config);
	}
	
	return $ret;
}



function email_wfl_approval_rejection_investigation($POST, $id)
{
	//$from = "jerry.johansson@sra.com.au";
	
  $incident_number = $_POST["INCIDENT_NUMBER"];
  $investigation_number = $_POST["INVESTIGATION_NUMBER"];
  
	  $approval_status = "";
	  $wfl_stage = "";
	  $sign_off_by = "";
	  $sign_off_date_time = "";
	  $forward_to = "";
	  $message = "";		


	if($_POST['DO_EMAIL_FLAG_1'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_1"], "1", $_POST["SIGNOFF_PERSON_ID_1"], $_POST["SIGNOFF_DATE_1"], $_POST["FORWARD_PERSON_ID_1"], $_POST["MESSAGE_1"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_2'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_2"], "2", $_POST["SIGNOFF_PERSON_ID_2"], $_POST["SIGNOFF_DATE_2"], $_POST["FORWARD_PERSON_ID_2"], $_POST["MESSAGE_2"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_3'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_3"], "3", $_POST["SIGNOFF_PERSON_ID_3"], $_POST["SIGNOFF_DATE_3"], $_POST["FORWARD_PERSON_ID_3"], $_POST["MESSAGE_3"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_INC_4'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_INC_4"], "7", $_POST["SIGNOFF_PERSON_ID_INC_4"], $_POST["SIGNOFF_DATE_INC_4"], $_POST["FORWARD_PERSON_ID_INC_4"], $_POST["MESSAGE_INC_4"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_INC_5'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_INC_5"], "8", $_POST["SIGNOFF_PERSON_ID_INC_5"], $_POST["SIGNOFF_DATE_INC_5"], $_POST["FORWARD_PERSON_ID_INC_5"], $_POST["MESSAGE_INC_5"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_INC_6'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_INC_6"], "9", $_POST["SIGNOFF_PERSON_ID_INC_6"], $_POST["SIGNOFF_DATE_INC_6"], $_POST["FORWARD_PERSON_ID_INC_6"], $_POST["MESSAGE_INC_6"], $id);	
	}
	
	
	
	if($_POST['DO_EMAIL_FLAG_4'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_4"], "4", $_POST["SIGNOFF_PERSON_ID_4"], $_POST["SIGNOFF_DATE_4"], $_POST["FORWARD_PERSON_ID_4"], $_POST["MESSAGE_4"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_5'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_5"], "5", $_POST["SIGNOFF_PERSON_ID_5"], $_POST["SIGNOFF_DATE_5"], $_POST["FORWARD_PERSON_ID_5"], $_POST["MESSAGE_5"], $id);	
	}
	if($_POST['DO_EMAIL_FLAG_6'] == 'True'){
		email_wfl_approval_rejection_investigation_one($POST, $_POST["STATUS_6"], "6", $_POST["SIGNOFF_PERSON_ID_6"], $_POST["SIGNOFF_DATE_6"], $_POST["FORWARD_PERSON_ID_6"], $_POST["MESSAGE_6"], $id);	
	}
}


/*
+------------------------------------------------------------------------
|
|  Function: email_wfl_approval_rejection_investigation
|  Purposes: Sends emails to next person in the workflow, approved or jrejected 
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (array)POST = Posted Data
|
+------------------------------------------------------------------------
*/
//function email_wfl_approval_rejection_investigation($POST){
function email_wfl_approval_rejection_investigation_one($POST, $approval_status, $wfl_stage, $sign_off_by, $sign_off_date_time, $forward_to, $message, $id){
	//*********************** Form variables used in email creation **********************
	// get the form variables to insert into the email message
	$from = "cats.admin@tronox.com.au";
	
  $incident_number = $_POST["INCIDENT_NUMBER"];
  $investigation_number = $_POST["INVESTIGATION_NUMBER"];
  echo 'INCIDENT_NUMBER: ' . $incident_number;
  if ($incident_number == "")
	$incident_number = $id;
  
  echo 'sign_off_by: ' . $sign_off_by;
  echo 'forward_to: ' . $forward_to;
  
  //$approval_status = $_POST["APPROVAL_STATUS"];
  //$wfl_stage = $_POST["WFL_STAGE"];
 // $sign_off_by = $_POST["SIGN_OFF_BY"];
 // $sign_off_date_time = $_POST["SIGN_OFF_DATE_TIME"];
 // $forward_to = $_POST["FORWARD_TO"];
 // $message = $_POST["MESSAGE"];

	$sql = "SELECT * FROM tblEmailOptions";
	$email_config = db_get_array($sql);

	$incdisplyid = "";
	$emailToAlert = "";
	$firstNameToAlert = "";
	$lastNameToAlert = "";
	$firstNameFrom = "";
	$lastNameFrom = "";
	$incidentTitle ="";
	$incidentDecription = "";
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $forward_to);
	while($tab = $rs->FetchRow()){

		$emailToAlert = $tab['EMAIL_ADDRESS'];	
		$firstNameToAlert = $tab['FIRST_NAME'];
		$lastNameToAlert = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $sign_off_by);
	while($tab = $rs->FetchRow()){

		$firstNameFrom = $tab['FIRST_NAME'];
		$lastNameFrom = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from VIEW_INVESTIGATION_PRINTVIEW where INCIDENT_NUMBER=" . $incident_number);
	while($tab = $rs->FetchRow()){
		$incdisplyid = $tab['INCDISPLYID'];
		$incidentTitle = $tab['INCIDENT_TITLE'];
		$incidentDecription = $tab['INCIDENT_DESCRIPTION'];
	}
	
	$subject = "Allocated Approval to complete: CATS Investigation workflow - " . $approval_status;
	if($wfl_stage < 4)
	{
		$subject = "Allocated Approval to complete: CATS Incident workflow - " . $approval_status;
		if($approval_status = "APPROVED")
		{		
			$emailMessage = "An incident has been set with you as the next approver:";
		}
		else //Rejected
		{
			$emailMessage = "An incident assignment for approval has been REJECTED:";
		}
	}
	if($wfl_stage > 6)
	{
		$subject = "Allocated Approval to complete: CATS Incident workflow - " . $approval_status;
		if($approval_status = "APPROVED")
		{		
			$emailMessage = "An incident has been set with you as the next approver:";
		}
		else //Rejected
		{
			$emailMessage = "An incident assignment for approval has been REJECTED:";
		}
		$wfl_stage = $wfl_stage - 3;
		
	}
	else
	{
		$subject = "Allocated Approval to complete: CATS workflow - " . $approval_status;
		if($approval_status = "APPROVED")
		{		
			$emailMessage = "An incident has been set with you as the next approver:";
		}
		else //Rejected
		{
			$emailMessage = "An incident assignment for approval has been REJECTED:";
		}
		
		$wfl_stage = $wfl_stage - 3;
	}
	

		//$emailMessage =  "Dear " . $firstNameToAlert . " " .  $lastNameToAlert . ". " . $firstNameFrom . " " . $lastNameFrom . " has " . $approval_status . " step " . $wfl_stage . " of the investigation of incident: " . $incdisplyid . "\n\n"; 
		//$emailMessage =  "Dear " . $firstNameToAlert . " " .  $lastNameToAlert . ". " . $firstNameFrom . " " . $lastNameFrom . " has " . $approval_status . " step " . ($wfl_stage - 3) . " of the ICAM investigation of incident: " . $incdisplyid . "\n\n"; 
		//$emailMessage = $emailMessage . "Message from " . $firstNameFrom . " " . $lastNameFrom . ": \n" . $message . "\n\n";
		//$emailMessage = $emailMessage . "Please enter the CATS system and complete the investigation report. " . $_SERVER["HTTP_REFERER"];
	
	
		$emailMessage .= "\n";
		$emailMessage .= "Incident Number:        " . $incdisplyid;
		$emailMessage .= "\n";
		$emailMessage .= "Approval Workflow step: " . $wfl_stage;
		$emailMessage .= "\n";
		$emailMessage .= "Assigned User:          " . $firstNameToAlert . " " .  $lastNameToAlert;
		$emailMessage .= "\n";
		$emailMessage .= "Assigned from User:     " . $firstNameFrom . " " . $lastNameFrom;
		$emailMessage .= "\n";
		$emailMessage .= "Incident Title:         " . $incidentTitle;
		$emailMessage .= "\n";
		$emailMessage .= "Incident Description:   " . $incidentDecription;
		$emailMessage .= "\n";
		$emailMessage .= "\n";
		$emailMessage .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
		$emailMessage .= "\n";
		$emailMessage .= "http://" . $_SERVER['SERVER_NAME'] . "/index.php?top=true&rdir=index.php?m=incidents&p=edit&link=incidents&id=" . $incident_number; 

	
	
	
//	$emailToAlert = "jerry.johansson@sra.com.au";
	
	if($ret = _mail_send($emailToAlert,$subject,$emailMessage,false,4,'','',$from,false))
		$_SESSION['messageStack']->add("Notification has been sent to the managed by person ($emailToAlert).","success");
	else
		dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $emailToAlert , From: $from");	
		
		
	return $ret;
	
	
}


/*
+------------------------------------------------------------------------
|
|  Function: email_wfl_approval_rejection_investigation
|  Purposes: Sends emails to next person in the workflow, approved or jrejected 
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (array)POST = Posted Data
|
+------------------------------------------------------------------------
*/
function email_action_rejection($POST, $id){
	//*********************** Form variables used in email creation **********************
	// get the form variables to insert into the email message
	//$from = "jerry.johansson@sra.com.au";
	$from = "cats.admin@tronox.com.au";
	
	 // echo 'APPROVAL_STATUS: ' . $_POST["APPROVAL_STATUS"]  . '<br/>';
  
 // echo 'REJECTION_MESSAGE: ' . $_POST["REJECTION_MESSAGE"]  . '<br/>';
//  echo 'REJECTED_BY_USER_ID: ' . $_POST["REJECTED_BY_USER_ID"]  . '<br/>';
 // echo 'ALLOCATED_SET_BY_ID: ' . $_POST["ALLOCATED_SET_BY_ID"]  . '<br/>';	
	



	$sql = "SELECT * FROM tblEmailOptions";
	$email_config = db_get_array($sql);

	$incdisplyid = "";
	$emailToAlert = "";
	$firstNameToAlert = "";
	$lastNameToAlert = "";
	$firstNameFrom = "";
	$lastNameFrom = "";
	$actionTitle = "";
	$actionDesc = "";
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" .  $_POST["ALLOCATED_SET_BY_ID"]);
	while($tab = $rs->FetchRow()){

		$emailToAlert = $tab['EMAIL_ADDRESS'];	
		$firstNameToAlert = $tab['FIRST_NAME'];
		$lastNameToAlert = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $_POST["REJECTED_BY_USER_ID"]);
	while($tab = $rs->FetchRow()){

		$firstNameFrom = $tab['FIRST_NAME'];
		$lastNameFrom = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLACTION_DETAILS where ACTION_ID=" . $id);
	while($tab = $rs->FetchRow()){

		$actionTitle = $tab['ACTION_TITLE'];
		$actionDesc = $tab['ACTION_DESCRIPTION'];
	}
	
	$subject = "Allocated Action to complete - Rejected";
	//$emailMessage =  "Dear " . $firstNameToAlert . " " .  $lastNameToAlert . ". " . $firstNameFrom . " " . $lastNameFrom . " has REJECTED the action that you assigned action. \n\n"; 
	
	//$emailMessage = $emailMessage . "Message from " . $firstNameFrom . " " . $lastNameFrom . ": \n" . $_POST["REJECTION_MESSAGE"] . "\n\n";
	//$emailMessage = $emailMessage . "Please enter the CATS system and change the action at: " . $_POST["REF_URL"];
	

	
		$emailMessage .= "An action you assigned have been rejected: ";
		$emailMessage .= "\n";
		$emailMessage .= "Action Id:              " . $id;
		$emailMessage .= "\n";
		$emailMessage .= "Assigned User:          " . $firstNameToAlert . " " .  $lastNameToAlert;
		$emailMessage .= "\n";
		$emailMessage .= "Assigned from User:     " . $firstNameFrom . " " . $lastNameFrom;
		$emailMessage .= "\n";
		$emailMessage .= "Action Title:           " . $actionTitle;
		$emailMessage .= "\n";
		$emailMessage .= "Action Description:     " . $actionDesc;
		$emailMessage .= "\n";
		$emailMessage .= "\n";
		$emailMessage .= "\n";
		$emailMessage .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
		$emailMessage .= "\n";
		$emailMessage .= "http://" . $_SERVER['SERVER_NAME'] . "/index.php?top=true&rdir=index.php?m=actions&p=edit&link=actions&id=" . $id; 
  
  
//	$emailToAlert = "jerry.johansson@sra.com.au";
	
	if($ret = _mail_send($emailToAlert,$subject,$emailMessage,false,4,'','',$from,false))
		$_SESSION['messageStack']->add("Notification has been sent to the managed by person ($emailToAlert).","success");
	else
		dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $emailToAlert , From: $from");	
		
		
	return $ret;
	
	
}




/*
+------------------------------------------------------------------------
|
|  Function: mail_notify_managed_by
|  Purposes: Sends an email to managed by person of an atcion
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)id = employee_number
|    (int)id_old = old employee_number
|    (string)send_email = Yes or No
|    (string)status = action status
|    (int)action_id = action id
|    (string)action_title = action title
|    (string)action_desc = action description
|    (string)due_date = scheduled date
|    (array)email_config = email configuration details
|
+------------------------------------------------------------------------
*/
function mail_notify_managed_by($id,$id_old,$send_email,$status,$action_id,$action_title,$action_desc,$due_date,$email_config){
	//************************************ Managed By Email **************************************
	/* 
	if there is a selected managed by and the managed by has changed and the managed by person
	is not the logged in user then create and send an email to the action manager 
	*/
	$ret = true;
	if($id != $id_old && $send_email == "Yes" && $status == "Open") { //create and send the email
		//*********** create the recordset *************
		global $db;
		$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
		$rs_emp = db_get_array($sql);
		$emp_name = $rs_emp["EMP_NAME"];
		$to = $rs_emp["EMAIL_ADDRESS"];
		$emp_site_id = $rs_emp["SITE_ID"];
		/*print_r($rs_emp);
exit;*/
		//*********** create and send the managed by email ****************
//		if($id != $_SESSION['user_details']['user_id']){
			//get the email from address for this persons site
			$sql = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " . $emp_site_id;
			$from = db_get_one($sql);
			
			$subject = "Action to Manage: " . $action_title;
			$message = "You have been assigned to manage the following action:\n\n";
			$message .= "Action Number:      " . $action_id . "\n";
			$message .= "Action Title:       " . $action_title . "\n";
			$message .= "Action Description: " . $action_desc . "\n";
			$message .= "Action Date Due:    " . $due_date . "\n\n";
			
			$message .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
			$message .= WS_PATH . 'index.php?top=true&link=actions&m=actions&p=edit&id='.$action_id;
			
			//if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
			//	$to = $email_config["EMAIL_TESTADDRESS"];
			//	$emp_name = $email_config["EMAIL_TESTNAME"];
			//	$from = $email_config["EMAIL_FROMADDRESS"];
			//}
			//echo("<br>To=".$to);
			$IsEmail_1 = strpos($to,"@");
			$IsEmail_2 = strpos($from,"@");
			
			if($IsEmail_1 > 0 && $IsEmail_2 > 0){
				if($ret = _mail_send($to,$subject,$message,false,4,'','',$from,false))
					$_SESSION['messageStack']->add("Notification has been sent to the managed by person ($to).","success");
				else
					dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $to , From: $from");
			}else{
				$ret=false;
				dprint(__FILE__,__LINE__,0,"Error:" . __FUNCTION__ . " - One or both of the Email addresses is incomplete<br>To:".$to."<br>From:".$from);
			}
//		}
	}
	return (bool)$ret;
}
/*
+------------------------------------------------------------------------
|
|  Function: mail_notify_allocated_to
|  Purposes: Sends an email to allocated to person of an atcion
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)id = employee_number
|    (int)id_old = old employee_number
|    (string)send_email = Yes or No
|    (string)status = action status
|    (int)action_id = action id
|    (string)action_title = action title
|    (string)action_desc = action description
|    (string)due_date = scheduled date
|    (array)email_config = email configuration details
|
+------------------------------------------------------------------------
*/
function mail_notify_allocated_to($id,$id_old,$send_email,$status,$action_id,$action_title,$action_desc,$due_date,$email_config){
	//************************************ Allocated To Email **************************************
	/*
	if there is a selected Allocated to and the Allocated to has changed and the Allocated to person is not the logged 
	in user and the NOTIFY_ALLOCATED_EMAIL checkbox IS checked then create and send an email to the allocated person
	*/
	$ret = true;
	if($id != $id_old && $send_email == "Yes" && $status == "Open") { //create and send the email
		//*********** create the recordset *************
		global $db;
		$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
		$rs_emp = db_get_array($sql);
		$emp_name = $rs_emp["EMP_NAME"];
		$to = $rs_emp["EMAIL_ADDRESS"];
		$emp_site_id = $rs_emp["SITE_ID"];

		//*********** create and send the allocated to email ****************
//		if($id != $_SESSION['user_details']['user_id']){
			//get the email from address for this persons site
			$sql = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " . $emp_site_id;
			$from = db_get_one($sql);
			
			$subject = "Allocated Action to complete: " . $action_title;
			$message = "You have been assigned to complete the following action:\n\n";
			$message .= "Action Number:      " . $action_id . "\n";
			$message .= "Action Title:       " . $action_title . "\n";
			$message .= "Action Description: " . $action_desc . "\n";
			$message .= "Action Date Due:    " . $due_date . "\n\n";
			
			$message .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
			$message .= WS_PATH . 'index.php?top=true&link=actions&m=actions&p=edit&id='.$action_id;
			
			//if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
			//	$to = $email_config["EMAIL_TESTADDRESS"];
			//	$emp_name = $email_config["EMAIL_TESTNAME"];
			//	$from = $email_config["EMAIL_FROMADDRESS"];
			//}
			
			$IsEmail_1 = strpos($to,"@");
			$IsEmail_2 = strpos($from,"@");
			
			if($IsEmail_1 > 0 && $IsEmail_2 > 0){
				if($ret = _mail_send($to,$subject,$message,false,4,'','',$from,false))
					$_SESSION['messageStack']->add("Notification has been sent to the allocated person ($to).","success");
				else
					dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $to , From: $from");
			}else{
				$ret = false;
				dprint(__FILE__,__LINE__,0,"Error:" . __FUNCTION__ . " - One or both of the Email addresses is incomplete<br>To:".$to."<br>From:".$from);
			}
//		}
	}
	return (bool)$ret;
}

/*
+------------------------------------------------------------------------
|
|  Function: mail_notify_managed_by_closed
|  Purposes: Sends an email to managed by person of an atcion
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)id = employee_number
|    (string)status = action status
|    (string)stats_old = original status value
|    (int)action_id = action id
|    (string)action_title = action title
|    (string)action_desc = action description
|    (string)due_date = scheduled date
|    (string)closing_comments = closing comment
|    (array)email_config = email configuration details
|
+------------------------------------------------------------------------
*/
function mail_notify_managed_by_closed($id,$status,$status_old,$action_id,$action_title,$action_desc,$due_date,$closing_comments,$email_config){
	//************************************ Action Closed Email ************************************
	/*
	if the action has just been closed then we send an email to the action manager to notify them.
	*/
	$ret = true;
	if($id>0 &&  $status_old == "Open" && $status != "Open") { //create and send the email
		//*********** create the recordset *************
		global $db;
		$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
		$rs_emp = db_get_array($sql);
		$emp_name = $rs_emp["EMP_NAME"];
		$to = $rs_emp["EMAIL_ADDRESS"];
		$emp_site_id = $rs_emp["SITE_ID"];
		
		//*********** create and send the managed by email ****************
		if($id != $_SESSION['user_details']['user_id']){
			//get the email from address for this persons site
			$sql = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " . $emp_site_id;
			$from = db_get_one($sql);
			
			$subject = "Action Closed: " . $action_title;
			$message = "The following Action which you are the Manager of has now been closed:\n\n";
			$message .= "Action Number:      " . $action_id . "\n";
			$message .= "Action Title:       " . $action_title . "\n";
			$message .= "Action Description: " . $action_desc . "\n";
			$message .= "Action Date Due:    " . $due_date . "\n";
			$message .= "Closing Comments:   " . $closing_comments . "\n\n";
			
			$message .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
			$message .= WS_PATH . 'index.php?top=true&link=actions&m=actions&p=edit&id='.$action_id;
			
			//if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
			//	$to = $email_config["EMAIL_TESTADDRESS"];
			//	$emp_name = $email_config["EMAIL_TESTNAME"];
			//	$from = $email_config["EMAIL_FROMADDRESS"];
			//}
			
			$IsEmail_1 = strpos($to,"@");
			$IsEmail_2 = strpos($from,"@");
			
			if($IsEmail_1 > 0 && $IsEmail_2 > 0){
				if($ret = _mail_send($to,$subject,$message,false,4,'','',$from,false))
					$_SESSION['messageStack']->add("An email has been sent to the managed by person ($to) notifying them that the action has been closed.","success");
				else
					dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $to , From: $from");
			}else{
				$ret = false;
				dprint(__FILE__,__LINE__,0,"Error:" . __FUNCTION__ . " - One or both of the Email addresses is incomplete<br>To:".$to."<br>From:".$from);
			}
		}
	}
	return (bool)$ret;
}

/*
+------------------------------------------------------------------------
|
|  Function: mail_notify_admin_to_closed
|  Purposes: Sends an email to administrator of an atcion
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)id = employee_number
|    (string)status = action status
|    (string)stats_old = original status value
|    (int)action_id = action id
|    (string)action_title = action title
|    (string)action_desc = action description
|    (string)due_date = scheduled date
|    (string)closing_comments = closing comment
|    (array)email_config = email configuration details
|
+------------------------------------------------------------------------
*/
function mail_notify_admin_to_closed($id,$origin_table,$report_id,$status,$status_old,$action_id,$action_title,$action_desc,$due_date,$closing_comments,$email_config){

	//************************************ Action Closed Email ************************************
	/*
	if the action has just been closed then we send an email to the administrator to notify them.
	*/
	$ret = true;
	if($id>0 &&  $status_old == "Open" && $status != "Open") { //create and send the email
		//*********** create the recordset *************
		global $db;
		$sql = "SELECT *  FROM VIEW_EMPLOYEE_DETAILS WHERE Employee_Number = " . $id;
		$rs_emp = db_get_array($sql);
		$emp_name = $rs_emp["EMP_NAME"];
		//$to = $rs_emp["EMAIL_ADDRESS"];
		$emp_site_id = $rs_emp["SITE_ID"];

		//*********** retrieve PCR No and Title *************
		$sql = "SELECT DISPLAY_ID, DETAILS FROM ".$origin_table." WHERE Report_Id = ".$report_id;
		$rs_pcr = db_get_array($sql);
		
		//*****fetch the PCR Administrator's email address - MTG 11Mar2008
		$sql = "SELECT PCR_ADMIN_EMAIL FROM tblSite WHERE Site_Id = " . $emp_site_id;
		$to = db_get_one($sql);
		
		$pcr_display_id = $rs_pcr["DISPLAY_ID"];
		//$pcr_id = $rs_pcr["PCR_ID"];
		$pcr_title = $rs_pcr["DETAILS"];
				
		//*********** create and send the PCR Administrator email ****************
		if($id != $_SESSION['user_details']['user_id']){
			//get the email from address for this PCR's site - MTG 11Mar2008
			$sql = "SELECT Email_From_Address FROM tblSite WHERE Site_Id = " . $emp_site_id;
			$from = db_get_one($sql);
			
			$subject = "Action Closed: " . $action_title;
			$message = "The following PCR Action, of which you are Administrator, has now been closed:\n\n";
			$message .= "Action Number:      " . $action_id . "\n";
			$message .= "Action Title:       " . $action_title . "\n";
			$message .= "Action Description: " . $action_desc . "\n";
			$message .= "Action Date Due:    " . $due_date . "\n";
			$message .= "Closing Comments:   " . $closing_comments . "\n\n";
			$message .= "PCR No:             " . $pcr_display_id . "\n";
			$message .= "PCR Title:          " . $pcr_title . "\n\n";
			
			$message .= "Click the link below to go to the PCR\n";
			$message .= WS_PATH . 'index.php?m=pcr_search&p=view&id='.$report_id;
			
			//if($email_config["EMAIL_STATUS"] == 1){//we are sending the email to the test address
			//	$to = $email_config["EMAIL_TESTADDRESS"];
			//	$emp_name = $email_config["EMAIL_TESTNAME"];
			//	$from = $email_config["EMAIL_FROMADDRESS"];
			//}
			
			$IsEmail_1 = strpos($to,"@");
			$IsEmail_2 = strpos($from,"@");
			
			if($IsEmail_1 > 0 && $IsEmail_2 > 0){
				if($ret = _mail_send($to,$subject,$message,false,4,'','',$from,false))
					$_SESSION['messageStack']->add("An email has been sent to the PCR Administrator ($to) notifying them that the action has been closed.","success");
				else
					dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $to , From: $from");
			}else{
				$ret = false;
				dprint(__FILE__,__LINE__,0,"Error:" . __FUNCTION__ . " - One or both of the Email addresses is incomplete<br>To:".$to."<br>From:".$from);
			}
		}
	}
	return (bool)$ret;
}


/*
+------------------------------------------------------------------------
|
|  Function: IncidentSeveritySignificantEmailSend
|  Purposes: Sends an email when the incident have been set to a significant severity type
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (string)to = email address
|    (string)subject = email subject
|    (string)message = email message
|    (array)attachments = array of attachments to include: array(array('file'=>'file_location','type'=>'image/gif'))
|    (int)priority = priority for mail - 1-5 5 being the lowest
|    (string)cc = cc email
|    (string)bcc = Bcc email
|    (string)from = from address defaults to CATS_ADMIN_EMAIL
|    (bool)show = show the message after it has been sent
|
+------------------------------------------------------------------------
*/
function IncidentSeveritySignificantEmailSend($POST, $id, $to,$subject,$message,$attachments=false,$priority=1,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false){

		$subject = "CATS: Significant Incident Alert";
		
		$message = "An incident has been set with a SIGNIFICANT severity rating:";
		$message .= "\n";
		$message .= "Incident Number:      " . $_POST['INCDISPLYID'];
		$message .= "\n";
		$message .= "Incident Title:       " . $POST[INCIDENT_TITLE];
		$message .= "\n";
		$message .= "Incident Description: " . $POST[INCIDENT_DESCRIPTION];
		$message .= "\n";
		$message .= "\n";
		$message .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
		$message .= "\n";
		$message .= "http://" . $_SERVER['SERVER_NAME'] . "/index.php?top=true&rdir=index.php?m=incidents&p=edit&link=incidents&id=" . $id; 
		
		
		$attachments = false;
		$cc="";
		$bcc="";
		$priority = 1;
		$show = false;
		$from = "";	
	
	//$to = "jerry.johansson@sra.com.au";
	
	_mail_send($to,$subject,$message,$attachments=false,$priority=1,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false);
}

/*
+------------------------------------------------------------------------
|
|  Function: EarlyAdviceEmailSend
|  Purposes: Sends an email for the Notify Tronox Subervisor name has been set/changed.
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (int)primaryEmpId = EmployeeId who is primary involved in incident
|	 (int)empIdTo = EmployeeId to find email address
|    (string)to = email address
|    (string)subject = email subject
|    (string)message = email message
|    (array)attachments = array of attachments to include: array(array('file'=>'file_location','type'=>'image/gif'))
|    (int)priority = priority for mail - 1-5 5 being the lowest
|    (string)cc = cc email
|    (string)bcc = Bcc email
|    (string)from = from address defaults to CATS_ADMIN_EMAIL
|    (bool)show = show the message after it has been sent
|
+------------------------------------------------------------------------
*/
function EarlyAdviceEmailSend($id, $primaryEmpId, $empIdTo, $to,$subject,$message,$attachments=false,$priority=1,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false)
{

	if(!$empIdTo > 0 || !$primaryEmpId > 0)
	{
		echo 'User to email are not set, will return and not email anything.';
		return;
	}	

	$emailToAlert = "";
	$firstNameToAlert = "";
	$lastNameToAlert = "";
	$firstNamePrimary = "";
	$lastNamePrimary = "";
	$displayId = "";
	$incidentTitle = "";
	$incidentDescription = "";
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $empIdTo);
	while($tab = $rs->FetchRow()){

		$emailToAlert = $tab['EMAIL_ADDRESS'];	
		$firstNameToAlert = $tab['FIRST_NAME'];
		$lastNameToAlert = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLEMPLOYEE_DETAILS where EMPLOYEE_NUMBER=" . $primaryEmpId);
	while($tab = $rs->FetchRow())
	{
		$firstNamePrimary = $tab['FIRST_NAME'];
		$lastNamePrimary = $tab['LAST_NAME'];
	}
	$rs = db_query("select * from TBLINCIDENT_DETAILS where INCIDENT_NUMBER=" . $id);
	while($tab = $rs->FetchRow())
	{
		$displayId = $tab['INCDISPLYID'];
		$incidentTitle = $tab['INCIDENT_TITLE'];
		$incidentDescription = $tab['INCIDENT_DESCRIPTION'];
	}
	$subject = "CATS Early Advice alert";
	//$emailMessage =  "Dear " . $firstNameToAlert . " " . $lastNameToAlert . ", " . $firstNamePrimary . " " . $lastNamePrimary . " has been registered as a primary person involved in an incident. You are set as his supervisor." . "\n\n"; 
	//$emailMessage = $emailMessage . "Please enter the CATS system and complete the incident report with Id " .$displayId . ". " . $_SERVER["HTTP_REFERER"];
	
	
		$emailMessage .= "An incident has been registered, where you have been set to be notified:  ";
		$emailMessage .= "\n";
		$emailMessage .= "Incident Number:        " . $displayId;
		$emailMessage .= "\n";
		$emailMessage .= "Employee:               " . $firstNamePrimary . " " . $lastNamePrimary;
		$emailMessage .= "\n";
		$emailMessage .= "Assigned as supervisor: " . $firstNameToAlert . " " . $lastNameToAlert;
		$emailMessage .= "\n";
		$emailMessage .= "Incident Title:         " . $incidentTitle;
		$emailMessage .= "\n";
		$emailMessage .= "Incident Description:   " . $incidentDescription;
		$emailMessage .= "\n";
		$emailMessage .= "\n";
		$emailMessage .= "PLEASE LOGIN TO CATS BEFORE CLICKING THE LINK BELOW\n";
		$emailMessage .= "\n";
		$emailMessage .= "http://" . $_SERVER['SERVER_NAME'] . "/index.php?top=true&rdir=index.php?m=incidents&p=edit&link=incidents&id=" . $id; 


//	$emailToAlert = "jerry.johansson@sra.com.au";
	
	if($ret = _mail_send($emailToAlert,$subject,$emailMessage,false,4,'','',$from,false))
		$_SESSION['messageStack']->add("Notification has been sent to the managed by person ($emailToAlert).","success");
	else
		dprint(__FILE__,__LINE__,0,"Error sending mail in:" . __FUNCTION__ . " - To: $emailToAlert , From: $from");	
		
		
	return $ret;

}

/*
+------------------------------------------------------------------------
|
|  Function: _mail_send
|  Purposes: Sends an email
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (string)to = email address
|    (string)subject = email subject
|    (string)message = email message
|    (array)attachments = array of attachments to include: array(array('file'=>'file_location','type'=>'image/gif'))
|    (int)priority = priority for mail - 1-5 5 being the lowest
|    (string)cc = cc email
|    (string)bcc = Bcc email
|    (string)from = from address defaults to CATS_ADMIN_EMAIL
|    (bool)show = show the message after it has been sent
|
+------------------------------------------------------------------------
*/
	function _mail_send($to,$subject,$message,$attachments=false,$priority=4,$cc='',$bcc='',$from=CATS_ADMIN_EMAIL,$show=false){
		require_once(CATS_CLASSES_PATH . 'mail.class.php');
 
		$m= new Mail; // create the mail
		$m->From( $from );
		$m->To( $to );
		$m->Subject( $subject );
		$m->Body( $message );	// set the body
		if($cc!='') $m->Cc( $cc );
		if($bcc!='') $m->Bcc( $bcc );
		$m->Priority( $priority ) ;	// set the priority (default Low(4))
		if(is_array($attachments)){
			foreach($attachments as $k=>$v)
				$m->Attach( $v['file'], $v['type'] ) ;	// attach a file of type ?
		}
		if(defined('CATS_TESTING_EMAIL') && CATS_TESTING_EMAIL=='yes'){
			$ret = true;
			echo "The mail below has been sent:<br><pre>", $m->Get(), "</pre>";
		}else{
			$ret = $m->Send();	// send the mail
			if($show){
				echo "the mail below has been sent:<br><pre>", $m->Get(), "</pre>";
			}
		}
		return (bool)$ret;
	}


/*
+------------------------------------------------------------------------
|
|  Function: mail_send
|  Purposes: Sends an email
|  Returns: Boolean - true if mail was sent or false otherwise
|  Params: 
|    (string)to = email address
|    (string)subject = email subject
|    (string)message = email message
|    (array)attachments = array of attachments to include: array(array('file'=>'file_location','type'=>'image/gif'))
|    (string)cc = cc email
|    (string)bcc = Bcc email
|    (int)priority = priority for mail - 1-5 5 being the lowest
|    (bool)show = show the message after it has been sent
|
+------------------------------------------------------------------------
*/
	function mail_send($to,$subject,$message,$attachments=false,$cc='',$bcc='',$priority=4,$show=false){
		require_once(CATS_CLASSES_PATH . 'mail.class.php');
 
		$m= new Mail; // create the mail
		$m->From( $from );
		$m->To( $to );
		$m->Subject( $subject );
		$m->Body( $message );	// set the body
		if($cc!='') $m->Cc( $cc );
		if($bcc!='') $m->Bcc( $bcc );
		$m->Priority( $priority ) ;	// set the priority (default Low(4))
		if(is_array($attachments)){
			foreach($attachments as $k=>$v)
				$m->Attach( $v['file'], $v['type'] ) ;	// attach a file of type ?
		}
		$ret = $m->Send();	// send the mail
		if($show){
			echo "the mail below has been sent:<br><pre>", $m->Get(), "</pre>";
		}
		return (bool)$ret;
	}

	


function email_to_jerry($POST, $subject, $emailMessage){
	//return true;
	//*********************** Form variables used in email creation **********************
	// get the form variables to insert into the email message
	$from = "jerry.johansson@sra.com.au";
	
	
	_mail_send($from,$subject,$emailMessage,false,4,'','',$from,false);
		
	
}
	

	
?>