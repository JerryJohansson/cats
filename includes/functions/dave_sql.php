<?php


require_once(CATS_FUNCTIONS_PATH. 'sql/actions.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/government_requirements.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/audit_and_inspections.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/reference_table.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/site_specific.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/lost_days.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/other_reports.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/motd.php');
require_once(CATS_FUNCTIONS_PATH. 'sql/helpers.php');





/*
|-------------------------------------------
| Name: ReplaceArrayQuotes
|-------------------------------------------
| Parameters: $myArray:- A Web Post Array
|
| Returns: $myArray:- Formatted for insert / update insert into
|                     the database
|-------------------------------------------
*/
function ReplaceArrayQuotes($myArray)
{
    $tempArray = $myArray; 
    foreach ($tempArray as $key => $value) 
    {
        $myArray[$key] = str_replace("'", "''", $value);
    }  

   return $myArray;
}






?>