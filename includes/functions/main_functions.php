<?php

/*
+---------------------------------------------------------------------------
|	Stop from parsing any further PHP code
+---------------------------------------------------------------------------
*/
function main_exit() {
	cats_session_close();
	exit();
}

/*
+---------------------------------------------------------------------------
|	Check if value is null
+---------------------------------------------------------------------------
*/
function main_not_null($value) {
	if (is_array($value)) {
		if (sizeof($value) > 0) {
			return true;
		} else {
			return false;
		}
	} else {
		if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
			return true;
		} else {
			return false;
		}
	}
}
/*
+---------------------------------------------------------------------------
|	Convert string to integer - use this as call back function
+---------------------------------------------------------------------------
*/
function main_string_to_int($string) {
	return (int)$string;
}

/*
+---------------------------------------------------------------------------
|	Return  padded strings with zero's
+---------------------------------------------------------------------------
*/
function main_pad_zero_right($string,$length)
{
	return str_pad($string,$length,'0',STR_PAD_RIGHT);
}

function main_pad_zero_left($string,$length)
{
	return str_pad($string,$length,'0',STR_PAD_LEFT);
}

function main_currency_format($number)
{
	return number_format($number,2,'.',' ');
}
function main_display_currency_format($number)
{
	$ret = ''.number_format($number,2,'.',' ');
	return preg_replace('/\s/', CURRENCY_SEPERATOR, $ret);
}

/*
+---------------------------------------------------------------------------
|	Redirect to another page or site
+---------------------------------------------------------------------------
*/
function main_redirect($url) {
	if (defined('ENABLE_SSL') && (ENABLE_SSL == true) && (getenv('HTTPS') == 'on') ) { // We are loading an SSL page
		if (substr($url, 0, strlen(HTTP_SERVER)) == HTTP_SERVER) { // NONSSL url
			$url = HTTPS_SERVER . substr($url, strlen(HTTP_SERVER)); // Change it to SSL
		}
	}

	header('Location: ' . $url);

	main_exit();
}


/*
+---------------------------------------------------------------------------
|	Parse the data used in the html tags to ensure the tags will not break
+---------------------------------------------------------------------------
*/
function main_parse_input_field_data($data, $parse) {
	return strtr(trim($data), $parse);
}

function main_output_string($string, $translate = false, $protected = false) {
	if ($protected == true) {
		return htmlspecialchars($string);
	} else {
		if ($string=='0') return $string;
		if ($translate == false) {
			return main_parse_input_field_data($string, array('"' => '&quot;'));
		} else {
			return main_parse_input_field_data($string, $translate);
		}
	}
}

function main_output_string_protected($string) {
	return main_output_string($string, false, true);
}

function main_sanitize_string($string) {
	$string = ereg_replace(' +', ' ', trim($string));

	return preg_replace("/[<>]/", '_', $string);
}	


function main_get_ip_address() {
	if (isset($_SERVER)) {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
	} else {
		if (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		} elseif (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		} else {
			$ip = getenv('REMOTE_ADDR');
		}
	}

	return $ip;
}

/*
+------------------------------------------------------------------------------
|  Returns a mask from a list, eg Pass in '1,2,4' it returns 7
+------------------------------------------------------------------------------
*/
function main_arr2mask($arr_list)
{
	$ret = 0;
	foreach( $arr_list as $mask )
	{
		if(($ret & $mask)==0)
			$ret = $ret + $mask;
	}
	return $ret;
}

function main_list2mask($list='0')
{
	return main_arr2mask(explode(",",$list));
}

/*
+------------------------------------------------------------------------------
|  Returns a list from the mask, eg Pass in 7 it returns 1,2,4
+------------------------------------------------------------------------------
*/
function main_mask2list($mask=0)
{
	$value = 0;
	$count = 0;
	while( $value < $mask )
	{
		// Increase value by factor of two
		$value = pow(2, $count++);
		if (($value & $mask) > 0)
		{
			if($ret == "")
			{
				$ret = $value;
			}
			else
			{
				$ret = $ret.", ".$value;
			}
		}
	}
	return $ret;
}
	

function format_backtrace($bt, $file, $line, $msg)
{
  echo "<pre>\n";
  echo "ERROR: $file($line): $msg\n";
  echo "Backtrace:\n";
  foreach ($bt as $level => $frame) {
    echo "$level $frame[file]:$frame[line] $frame[function](";
    $in = false;
    foreach ($frame['args'] as $arg) {
      if ($in)
				echo ",";
      else
				$in = true;
      echo var_export($arg, true);
    }
    echo ")\n";
  }
}

function dprint($file, $line, $level, $msg, $force_dbg=false)
{
	global $db;
	$debug=($force_dbg)?$force_dbg:$db->debug;
	$err_type=array("error","warn","info","mail","select","insert","update","debug","permission","system","user");
	$_SESSION['messageStack']->add("$file($line): $msg",$err_type[$level]);
	if ($debug) {
		echo "$file($line): $msg <br />";
	  if(defined('CATS_LOG_ERRORS') && (bool)(CATS_LOG_ERRORS)==true) error_log("$file($line): $msg");
    if (version_compare(phpversion(), "4.3.0") >= 0)
      format_backtrace(debug_backtrace(), $file, $line, $msg);
	}
	$log=(isset($_SESSION['keep_log']))?(bool)$_SESSION['keep_log']:(defined('CATS_DB_LOG')?(bool)(CATS_DB_LOG):false);
	if($log){
		db_insert_error_log(array('FILE_NAME'=>$file,'LINE_NUMBER'=>$line,'ERROR_TYPE'=>$err_type[$level],'ERROR_MESSAGE'=>$msg));
	}
}

?>