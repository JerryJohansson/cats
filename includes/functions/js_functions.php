<?php
/*
+------------------------------------------------------------------------
|
|  Function: js_get_options
|  Purposes: Generates a string to be evaluated by javascript
|  Returns: JSArray in the following format:
|    [[100, "Sheep"],...]
|  Params: 
|    (array)arr = array of items - array(array("ID"=>"ID_VALUE","TEXT"=>"TEXT_VALUE"),...)
|    (string)id = identifier column name
|    (string)text = identifier column name
|
+------------------------------------------------------------------------
*/
function js_get_options($arr, $id='ID', $text='TEXT',$extra=true){
//print_r($arr);
	$s="[";
	if($extra) $s .= "['',''],";
	if(is_array($arr)){
		foreach($arr as $key => $opt){
			$value=db_input($opt[$id]);
			$text_value=db_input($opt[$text]);
			$s .= "['$value','$text_value'],";
		}
	}
	$s = preg_replace("/,$/i","",$s);
	$s .= "]";
	return $s;
}

function js_get_checkboxes($arr, $id='ID', $text='TEXT', $desc='TITLE', $checked='CHECKED'){
// id = value, text = label, desc = friendly description/title tag
//print_r($arr);
	$s="[";
	foreach($arr as $key => $opt){
		$value=db_input($opt[$id]);
		$text_value=db_input($opt[$text]);
		$desc_value=db_input($opt[$desc]);
		$checked_value=($opt[$checked]+0);
		$s .= "['$value','$text_value','$desc_value',$checked_value],";
	}
	$s = preg_replace("/,$/i","",$s);
	$s .= "]";
	return $s;
}
?>