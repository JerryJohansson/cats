<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Site Configuration
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

function cats_session_start() {
	return session_start();
}

function cats_session_register($variable) {
	global $session_started;

	if ($session_started == true) {
		return session_register($variable);
	} else {
		return false;
	}
}

function cats_session_is_registered($variable) {
	if (isset($_SESSION[$variable]))
    return true;
    else
    return false;
	
	//return session_is_registered($variable);
}

function cats_session_unregister($variable) {
	unset($_SESSION[$variable]);
	return true; 
	// return session_unregister($variable);
}

function cats_session_id($sessid = '') {
	if (!empty($sessid)) {
		return session_id($sessid);
	} else {
		return session_id();
	}
}

function cats_session_name($name = '') {
	if (!empty($name)) {
		return session_name($name);
	} else {
		return session_name();
	}
}

function cats_session_close() {
	if (PHP_VERSION >= '4.0.4') {
		return session_write_close();
	} elseif (function_exists('session_close')) {
		return session_close();
	}
}

function cats_session_destroy() {
	return session_destroy();
}

function cats_session_save_path($path = '') {
	if (!empty($path)) {
		return session_save_path($path);
	} else {
		return session_save_path();
	}
}

function cats_session_recreate() {
	if (PHP_VERSION >= 4.1) {
		$session_backup = $_SESSION;

		unset($_COOKIE[cats_session_name()]);

		cats_session_destroy();

		cats_session_start();

		$_SESSION = $session_backup;
		unset($session_backup);
	}
}
function cats_user_is_incident_privileged(){
	return cats_user_group_mask_match(CATS_INCIDENT_PRIVILEGED);
}

function cats_user_is_incident_analysis(){
	return cats_user_group_mask_match(CATS_INCIDENT_ANALYSIS);
}

function cats_user_is_editor_privileged(){
	return cats_user_group_mask_match(CATS_EDITOR_PRIVILEGED);
}

function cats_user_is_editor($privileged=false){
	$mask = ($privileged==true) ? CATS_EDITOR_PRIVILEGED : CATS_EDITOR;
	return cats_user_group_mask_match($mask);
}

function cats_user_is_reader($privileged=false){
	$mask = ($privileged==true) ? CATS_READER_PRIVILEGED : CATS_READER;
	return cats_user_group_mask_match($mask);
}

function cats_user_is_pcr_administrator(){
	return cats_user_group_mask_match(CATS_PCR_ADMINISTRATOR);
}

function cats_user_is_administrator(){
	return cats_user_group_mask_match(CATS_ADMINISTRATOR);
}
function cats_user_is_admin_group(){
	return cats_user_group_mask_match(512+CATS_ADMINISTRATOR);
}
function cats_user_is_super_administrator(){
	return cats_user_group_mask_match(CATS_SUPER_ADMINISTRATOR);
}

function cats_user_is_action_administrator(){
	return cats_user_group_mask_match(CATS_ACTION_ADMINISTRATOR);
}

function cats_user_is_action_privileged(){
	return cats_user_group_mask_match(CATS_ACTION_PRIVILEGED);
}

function cats_user_is_admin_assistant(){
	return cats_user_group_mask_match(CATS_ADMIN_ASSISTANT);
}

function cats_user_is_pcr_team_member(){
	return cats_user_group_mask_match(CATS_PCR_TEAM_MEMBER);
}



function cats_user_is_root_administrator(){
	return cats_user_group_mask_match(CATS_ROOT_ADMINISTRATOR);
}

function cats_user_group_mask_match($mask){
	$match=($_SESSION['user_details']['groups']+0);
	$in=($mask+0);
	return (($match & $in)>0);
}

function cats_check_user_site_access($site_id){
	$access = false;
	if(!is_array($site_id))	$site_id = explode(",",$site_id);
	$user_site_id = $_SESSION['user_details']['site_id'];
	$user_site_access = $_SESSION['user_details']['site_access'];
	//print_r($site_id);
	//echo($user_site_access . ':' . $user_site_id);
	
	if(!empty($user_site_access)){
		$a_sites = explode(",", $user_site_access);
		// Add the User's site to the end of the array
		array_push($a_sites, $user_site_id);
	} else {
		// The User's site is the array
		$a_sites = explode(",", $user_site_id);
	}
	
	foreach($a_sites as $site){
		$n = 0+$site;
		foreach($site_id as $id){
			if($n == $id){
				$access = true;
				break;
			}
		}
		if($access) break;
	}
	
	return $access;
}

function cats_set_fields_array_value($name, $value, $insert_global=true){
	global $FIELDS;
	if($insert_global){
		if(isset($_POST[$name]) && !empty($_POST[$name]))
			$value = $_POST[$name];
		elseif(isset($_GET[$name]) && !empty($_GET[$name]))
			$value = $_GET[$name];
	}
	if(isset($FIELDS[$name])) $FIELDS[$name]['value'] = $value;
}

function cats_actions_closed($origin='ORIGINATING_ACTION'){
	global $id, $db;
	$sql = "select count(*) from tblaction_details where report_id=$id and upper(ORIGIN_TABLE)=upper('$origin') and upper(status) like 'OPEN%'";
	//echo db_get_one($sql) . $sql;
	return (bool)(db_get_one($sql)==0);
}

function cats_get_checkboxes_values_array($key=false){
	$ret = array(
		'LIFE'=>'LIFE',
		'Reported_To'=>'ReportedTo',
		'Systemic_Cause'=>'SystemicCause',
		'Behaviour_Cause'=>'BehaviourCause',
		'Injury_Classification'=>'InjuryClassification',
		'Injury_Location'=>'InjuryLocation',
		'Injury_Mechanism'=>'InjuryMechanism',
		'Injury_Type'=>'InjuryType',
		'Injury_Agency'=>'InjuryAgency',
		'Environment_Classification'=>'EnviroClassification',
		'Environment_ImpactTo'=>'EnviroImpactTo',
		'Environment_ImpactBy'=>'EnviroImpactBy',
		'Community_Classification'=>'CommunityClassification',
		'Contact_Source'=>'ContactSource',
		'Quality_Classification'=>'QualityClassification',
		'Economic'=>'Economic',
		'Report_Type'=>'IncidentReportType',
		'Incident_Category'=>'IncidentCategory',
		'Hazard_Category'=>'Category',
		'TREATMENT_OUTCOME'=>'TREATMENT_OUTCOME',
		'DF_1'=>'ABS_OR_FAILED_DF1',
		'DF_2'=>'ABS_OR_FAILED_DF2',
		'DF_3'=>'ABS_OR_FAILED_DF3',
		'DF_4'=>'ABS_OR_FAILED_DF4',
		'DF_5'=>'ABS_OR_FAILED_DF5',
		'DF_6'=>'ABS_OR_FAILED_DF6',
		'DF_7'=>'ABS_OR_FAILED_DF7',
		'DF_8'=>'ABS_OR_FAILED_DF8',
		'DF_9'=>'ABS_OR_FAILED_DF9',
		'DF_10'=>'ABS_OR_FAILED_DF10',
		'DF_11'=>'ABS_OR_FAILED_DF11',
		'DF_12'=>'ABS_OR_FAILED_DF12',
		'DF_13'=>'ABS_OR_FAILED_DF13',
		'DF_14'=>'ABS_OR_FAILED_DF14',
		'DF_15'=>'ABS_OR_FAILED_DF15',
		'DF_16'=>'ABS_OR_FAILED_DF16',
		'DF_17'=>'ABS_OR_FAILED_DF17',
		'DF_18'=>'ABS_OR_FAILED_DF18',
		'DF_19'=>'ABS_OR_FAILED_DF19',
		'DF_20'=>'ABS_OR_FAILED_DF20',
		'DF_21'=>'ABS_OR_FAILED_DF21',
		'IT_1'=>'INDIV_TEAM_IT1',
		'IT_2'=>'INDIV_TEAM_IT2',
		'IT_3'=>'INDIV_TEAM_IT3',
		'IT_4'=>'INDIV_TEAM_IT4',
		'IT_5'=>'INDIV_TEAM_IT5',
		'IT_6'=>'INDIV_TEAM_IT6',
		'IT_7'=>'INDIV_TEAM_IT7',
		'IT_8'=>'INDIV_TEAM_IT8',
		'IT_9'=>'INDIV_TEAM_IT9',
		'IT_10'=>'INDIV_TEAM_IT10',
		'IT_11'=>'INDIV_TEAM_IT11',
		'IT_12'=>'INDIV_TEAM_IT12',
		'IT_13'=>'INDIV_TEAM_IT13',
		'IT_14'=>'INDIV_TEAM_IT14',
		'WF_1'=>'WORKFACTOR_WF1',
		'WF_2'=>'WORKFACTOR_WF2',
		'WF_3'=>'WORKFACTOR_WF3',
		'WF_4'=>'WORKFACTOR_WF4',
		'WF_5'=>'WORKFACTOR_WF5',
		'WF_6'=>'WORKFACTOR_WF6',
		'WF_7'=>'WORKFACTOR_WF7',
		'WF_8'=>'WORKFACTOR_WF8',
		'WF_9'=>'WORKFACTOR_WF9',
		'WF_10'=>'WORKFACTOR_WF10',
		'WF_11'=>'WORKFACTOR_WF11',
		'WF_12'=>'WORKFACTOR_WF12',
		'WF_13'=>'WORKFACTOR_WF13',
		'WF_14'=>'WORKFACTOR_WF14',
		'WF_15'=>'WORKFACTOR_WF15',
		'WF_16'=>'WORKFACTOR_WF16',
		'WF_17'=>'WORKFACTOR_WF17',
		'WF_18'=>'WORKFACTOR_WF18',
		'WF_19'=>'WORKFACTOR_WF19',
		'WF_20'=>'WORKFACTOR_WF20',
		'WF_21'=>'WORKFACTOR_WF21',
		'WF_22'=>'WORKFACTOR_WF22',
		'WF_23'=>'WORKFACTOR_WF23',
		'WF_24'=>'WORKFACTOR_WF24',
		
		'HF_1'=>'HUMANFACTOR_HF1',
		'HF_2'=>'HUMANFACTOR_HF2',
		'HF_3'=>'HUMANFACTOR_HF3',
		'HF_4'=>'HUMANFACTOR_HF4',
		'HF_5'=>'HUMANFACTOR_HF5',
		'HF_6'=>'HUMANFACTOR_HF6',
		'HF_7'=>'HUMANFACTOR_HF7',
		'HF_8'=>'HUMANFACTOR_HF8',
		'HF_9'=>'HUMANFACTOR_HF9',
		'HF_10'=>'HUMANFACTOR_HF10',
		'HF_11'=>'HUMANFACTOR_HF11',
		'HF_12'=>'HUMANFACTOR_HF12',
		'HF_13'=>'HUMANFACTOR_HF13',
		'HF_14'=>'HUMANFACTOR_HF14',

		'HF_15'=>'HUMANFACTOR_HF15',
		'HF_16'=>'HUMANFACTOR_HF16',
		'HF_17'=>'HUMANFACTOR_HF17',
		'HF_18'=>'HUMANFACTOR_HF18',
		'HF_19'=>'HUMANFACTOR_HF19',
		'HF_20'=>'HUMANFACTOR_HF20',
		'HF_21'=>'HUMANFACTOR_HF21',
		'HF_22'=>'HUMANFACTOR_HF22',
		'HF_23'=>'HUMANFACTOR_HF23',
		'HF_24'=>'HUMANFACTOR_HF24',
		'HF_25'=>'HUMANFACTOR_HF25',
		'HF_26'=>'HUMANFACTOR_HF26',

		
		'HW'=>'ORGFACTOR_HW',
		'MM'=>'ORGFACTOR_MM',
		'TR'=>'ORGFACTOR_TR',
		'DE'=>'ORGFACTOR_DE',
		'OR'=>'ORGFACTOR_OR',
		'RM'=>'ORGFACTOR_RM',
		'CO'=>'ORGFACTOR_CO',
		'MC'=>'ORGFACTOR_MC',
		'IG'=>'ORGFACTOR_IG',
		'CM'=>'ORGFACTOR_CM',
		'PR'=>'ORGFACTOR_PR',
		'OC'=>'ORGFACTOR_OC',
		'RI'=>'ORGFACTOR_RI',
		'OL'=>'ORGFACTOR_OL',
		'VM'=>'ORGFACTOR_VM',
		'MS'=>'ORGFACTOR_MS'
		
	);
	if($key){
		return $ret[$key];
	}else{
		return $ret;
	}
}

function cats_form_pager($sql, $column_labels=false, $column_attributes=false, $title = '', $sort_id='', $order_by_id='', $order_direction = '', $links=false, $ignore=false, $row_count=CATS_DEFAULT_ROW_COUNT){
	// NOTE: do not use order by clause in sql when sort_enabled is true
	//  instead - there are two params to pass into Pager - order_by_id = 'FIELD_ID' and order_direction = 'ASC'
	global $db;
	require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
	//&$db, $sql, $id = 'cats', $showPageLinks = true, $columnAttributes = false, $sort_enabled = true, $order_by_id = '', $order_direction = ''
	
	$pager = new ADODB_Pager($db,$sql,$sort_id,true,$column_attributes,($sort_id!=''),$order_by_id,$order_direction);
	$pager->use_form_pager = true;
	$pager->linkArray = $links;
	$pager->ignoreColumns = $ignore;
	if(!empty($title)){
		$pager->caption_title=$title;
	}
	if(is_array($column_labels)){
		$pager->gridHeader=$column_labels;
	}
	$pager->Render($row_count);
}

// PCR Stuff
function is_pcr_actions_complete($id){
	if(empty($id)) return false;//select count(*) as count from tblPCR_Actions where action_confirmed ='N' and pcr_id = $id
	$sql = "select count(*) from tblPCR_Actions where (action_confirmed ='N' or action_confirmed is null) and pcr_id = $id";
	return (db_get_one($sql)==0);
}
function is_direct_to_actions($id){
	if(is_int($id)) return false;
	$sql = "select Go_To_Actions from tblPCR_Evaluation where pcr_id = $id";
	return (db_get_one($sql)=="Yes");
}
function is_direct_to_evaluation($id){
	if(is_int($id)) return false;
	$sql = "select Go_To_Evaluation from tblPCR_Details where pcr_id = $id";
	return (db_get_one($sql)=="Yes");
}
?>