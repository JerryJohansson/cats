<?php
/*
+--------------------------------------------------------------------------
|   select.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for <select> elements such as dropdowns or list/multilist elements
+---------------------------------------------------------------------------
*/








    /*
	    START: PCR STUFF
	*/


	/*  PCR Impact Level - drop down list
	|----------------------------------------------------------------------
	| PCR
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_pcr_impact_level_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT RefTable_Description AS TEXT, RefTable_Id AS ID FROM tblRefTable 
		        WHERE RefTable_Type = 'PCR Criticality Level' 
				ORDER BY RefTable_Description ASC";
				
				
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}	





	/*  PCR Status Types - drop down list
	|----------------------------------------------------------------------
	| PCR
	| - Search
	|----------------------------------------------------------------------
	*/
	function html_form_draw_pcr_status_types_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT RefTable_Description AS TEXT FROM tblreftable 
		        WHERE reftable_type = 'Status Types'";
				
				
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}





	/*  PCR Type Of Change - drop down list
	|----------------------------------------------------------------------
	| PCR
	| - Search
	|----------------------------------------------------------------------
	*/
	function html_form_draw_pcr_type_of_change_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT RefTable_Description AS TEXT, RefTable_Id AS ID 
		        FROM tblRefTable 
				WHERE RefTable_Type = 'PCR Type of Change' 
				ORDER BY RefTable_Description ASC";
				
				
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}





	/*  PCR TCP - radio set
	|----------------------------------------------------------------------
	| PCR
	| - Search
	|----------------------------------------------------------------------
	*/
	function html_draw_pcr_tcp_radioset($name, $value='') 
	{
	    $html = '<input type="radio" name="TCP" id="id_TCP_yes" class="inputTextBox"  value="Yes"> Yes 
                 <input type="radio" name="TCP" id="id_TCP_no" class="inputTextBox"  value="No"> No';
	
	
		return $html;//html_get_status_radioset($name,$value,'TBLEMPLOYEE_DETAILS');
	}



	/*  PCR - PCR Type - radio set
	|----------------------------------------------------------------------
	| PCR
	| - New
	|----------------------------------------------------------------------
	*/
	function html_draw_pcr_type_radioset($name, $value='') 
	{
	    $html = '<input type="radio" name="PCR_Type" value="Permanent" checked>
                Permanent
                <input type="radio" name="PCR_Type" value="Trial">
                Trial
                <input type="radio" name="PCR_Type" value="Emergency">
                Emergency';
	
		return $html;
	}





	/*  PCR - PCR Reason For Change - check box 
	|----------------------------------------------------------------------
	| PCR
	| - New
	|----------------------------------------------------------------------
	*/
	function html_draw_pcr_reason_for_change_checkbox($name, $value='') 
	{
	    $html = '<input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  New Product/Service 
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  Increased Production 
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  Capacity Cost Reduction <br>
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  Technical Capability 
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  Economic Protection 
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  Health Safety Environment <br>
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  New Product/Service 
		         <input name="chkChange" type="checkbox" id="chkChange" value="checkbox">
                  Other Mandatory ';

		return $html;
	}



	/*  PCR - PCR Site Area - drop down list
	|----------------------------------------------------------------------
	| PCR
	| - New
	|----------------------------------------------------------------------
	*/
	function html_draw_pcr_site_area_dd($name, $value='') 
	{
		$sql = 'SELECT Area_Id AS ID, Area_Description AS TEXT  
		        FROM tblPCR_Area 
			    ORDER BY Area_Description ASC';
				
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}








    /*
	    END: PCR STUFF
	*/
















	function html_group_type_dd($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_group_type_options($value), $value, $parameters, false, true);
	}



	/*  Active Employees - drop down list
	|----------------------------------------------------------------------
	| OTHER RECORDS
	| - Add
	|----------------------------------------------------------------------
	*/
	function html_form_draw_active_employee_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Emp_Name AS TEXT  FROM VIEW_EMPLOYEE_DETAILS WHERE User_Status = 'Active' ORDER BY Emp_Name ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}	


	/* MESSAGE OF THE DAY Type Drop Down List
	|----------------------------------------------------------------------
	|----------------------------------------------------------------------
	*/		
	
	function html_motd_type_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"0", "text" => "Message Of The Day"),
			array("id"=>"1", "text" => "Link Of Interest")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}

	function html_motd_frequency_dd($name, $value, $parameters = ''){
		$arr = array(array("id"=>"0", "text"=>""));
		$i=1;
		for($i=1;$i<13;$i++) $arr[] = array("id"=>"$i","text"=>"$i");
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}
	
	function html_motd_repeat_period_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"0", "text" => ""),
			array("id"=>"1", "text" => "Day/s"),
			array("id"=>"2", "text" => "Week/s"),
			array("id"=>"3", "text" => "Month/s"),
			array("id"=>"4", "text" => "Year/s")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}
	
	function html_draw_motd_date_option2_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"0", "text" => "Every Week"),
			array("id"=>"1", "text" => "On the First"),
			array("id"=>"2", "text" => "On the Second"),
			array("id"=>"3", "text" => "On the Third"),
			array("id"=>"4", "text" => "On the Fourth")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}
	
	function html_draw_motd_date_option1_dd($name, $value, $parameters = ''){
		$arr = html_get_days_of_week_options(array());
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}
	
	function html_draw_motd_date_option3_dd($name, $value, $parameters = ''){
		$arr = array(array("id"=>"0", "text"=>"Every Month"));
		$arr = html_get_months_options($arr);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}

	
	/* Inspection Type - drop down list
	|----------------------------------------------------------------------
	| AUDITS AND INSPECTIONS FORM
	| - Search
	|
	| GOVERNMENT INSPECTION FORM
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_audit_type_dd($name, $value, $parameters = '')
	{
		$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT RefTable_Description as TEXT, RefTable_ID AS ID  FROM tblRefTable  WHERE RefTable_Type = 'Audit or Inspection' $where ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}



	/* Inspection Type - drop down list
	|----------------------------------------------------------------------
	| GOVERNMENT INSPECTION FORM
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_government_type_dd($name, $value, $parameters = '')
	{
		$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT RefTable_Description as TEXT, RefTable_ID AS ID  FROM tblRefTable  WHERE RefTable_Type = 'Requirement Type' $where ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}



	/* MONTHS Drop Down List
	|----------------------------------------------------------------------
	| LOST DAY FORM
	| - Search
	| - Add
	| - Edit
	|
	|----------------------------------------------------------------------
	*/	
	
	function html_months_dd($name, $value, $parameters = ''){
		$arr = html_get_months_options(array());
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}	
	
	function html_months_search_dd($name, $value, $parameters = ''){
		$arr = array(array("id"=>"", "text"=>""));
		$arr = html_get_months_options($arr);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}
	
	/* YEARS Drop Down List
	|----------------------------------------------------------------------
	| LOST DAY FORM
	| - Search
	| - Add
	| - Edit
	|
	|----------------------------------------------------------------------
	*/		
	
	function html_years_dd($name, $value, $parameters = '')
	{
		$arr = array();
		for ($i = 1995; $i <= 2020; $i++)
			 $arr[] = array("id"=>"$i", "text" => "$i");
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false);
	}
	








	
/*REMOVE
	function html_form_draw_inspection_type_dd($name, $value, $parameters = ''){
		$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT RefTable_Description as TEXT  FROM tblRefTable  WHERE RefTable_Type = 'Requirement Type' $where ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
*/
	function html_form_draw_group_name_dd($name, $value, $parameters = ''){
		$sql = "SELECT Group_Description as TEXT FROM tblAccess_Groups_Defn ORDER BY Group_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_employee_position_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Position_Id as ID, Position_Description as TEXT FROM tblPosition ORDER BY Position_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_company_dd($name, $value, $parameters = ''){
		$sql = "SELECT CompanyId as ID, CompanyName as TEXT FROM tblCompanies ORDER BY CompanyName ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	function html_form_draw_company_filter_dd($name, $value, $parameters = ''){
		$s = html_draw_sml_input_field("val_".$name,'',' onblur="get_companies(this.form.'.$name.',this.value);" ');
		$s .= html_form_draw_company_dd($name, $value, $parameters);
		return $s;
	}

	function html_form_draw_government_locations_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Location as TEXT  FROM tblGovernment_Details  ORDER BY UPPER(Location) ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_employee_type_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"Contrator", "text" => "Contractor"),
			array("id"=>"Employee", "text" => "Employee")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	function html_form_draw_on_off_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"On", "text" => "On"),
			array("id"=>"Off", "text" => "Off")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	/* Estimated Cost - Drop Down List
	+----------------------------------------------------------------------
	| INCIDENTS FORM
	| - Search(privileged)
	+----------------------------------------------------------------------
	*/
	function html_form_draw_estimated_costs_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"less than $100", "text" => "less than $100"),
			array("id"=>"$100 to $500", "text" => "$100 to $500"),
			array("id"=>"$500 to $5000", "text" => "$500 to $5000"),
			array("id"=>"$5000 to $50,000", "text" => "$5000 to $50,000"),
			array("id"=>"more than $500,000", "text" => "more than $500,000")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	/* SITES Drop Down List
	+----------------------------------------------------------------------
	| GOVERNMENT INSPECTION FORM
	| - Search
	|
	| AUDITS AND INSPECTIONS
	| - Search
	| - Add
	|
	| OTHER RECORDS
	| - Search
	+----------------------------------------------------------------------
	*/
	function html_form_draw_site_dd($name, $value, $parameters = '', $filtered=false){
		$where = ($filtered) ? get_user_site_access_filter('', 'WHERE') : "" ;
		$sql = "select site_id as ID, site_description as TEXT from tblsite $where ";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/*  Sites filtered by users site_id or multi_site - drop down list
	+----------------------------------------------------------------------
	|
	+----------------------------------------------------------------------
	*/
	function html_form_draw_filtered_site_dd($name, $value, $parameters = ''){
		return html_form_draw_site_dd($name, $value, $parameters, true);
	}
	function html_form_draw_filtered_site_dd_linked($name, $value, $parameters = '', $department='', $section=''){
		return html_form_draw_site_dd_linked($name, $value, $parameters, $department, $section, true);
	}
	// uses remote call to get data for linked dropdown box
	function html_form_draw_site_dd_linked($name, $value, $parameters = '', $department='', $section='', $filtered = false){
		$parameters .= ' onchange="get_site_department(this';
		if($department!=''){
			$parameters .= ",this.form.$department";
		}
		if($section!=''){
			$parameters .= ",this.form.$section";
		}
		$parameters .= ');" ';
		return html_form_draw_site_dd($name, $value, $parameters, $filtered);
	}
	
	/*  Departments - drop down list
	+----------------------------------------------------------------------
	| AUDITS AND INSPECTIONS FORM
	| - Search
	| - Add
	|
	| OTHER RECORDS
	| - Search
	+----------------------------------------------------------------------
	*/
	function html_form_draw_department_dd($name, $value, $parameters = '', $filtered=false){
		$where = ($filtered) ? get_user_site_access_filter('', 'WHERE') : "" ;
		$sql = "select department_id as ID, department_description as TEXT from tbldepartment $where ";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	/*  Departments filtered by users site_id or multi_site - drop down list
	+----------------------------------------------------------------------
	|
	+----------------------------------------------------------------------
	*/
	function html_form_draw_filtered_department_dd($name, $value, $parameters = ''){
		return html_form_draw_department_dd($name, $value, $parameters, true);
	}
	// uses remote call to get data for linked dropdown box
	function html_form_draw_department_dd_linked($name, $value, $parameters = '', $section=''){
		$parameters .= ' onchange="get_department_section(this';
		if($section!=''){
			$parameters .= ",this.form.$section";
		}
		$parameters .= ');" ';
		
		return html_form_draw_department_dd($name, $value, $parameters);
	}
	
	/*  Sections - drop down list
	|----------------------------------------------------------------------
	| AUDITS AND INSPECTIONS FORM
	| - Search
	| - Add
	|
	| OTHER RECORDS
	| - Search
	|----------------------------------------------------------------------
	*/
	function html_form_draw_section_dd($name, $value, $parameters = ''){
		$sql = "select section_id as ID, section_description as TEXT from tblsection";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	// not used
	function html_form_draw_mimms_employees_dd($name, $value, $parameters = ''){
		$sql = "SELECT EMPID as ID, SURNAME || ', ' || GIVEN_NAME || ' (' || EMPID || ')' as TEXT FROM View_MIMS_Data ORDER BY SURNAME ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_draw_configuration_keys_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"CATS_DEFAULT_ROW_COUNT", "text" => "Number of search results to display"),
			array("id"=>"CATS_REUSE_LOGIN", "text" => "Enter my login into login screen when I logout"),
			array("id"=>"DEFAULT_STYLE", "text" => "My Default Skin")
		);
		if(cats_user_is_root_administrator()){
			$arr = array_merge($arr,
				array(
					array("id"=>"CATS_MAINTENANCE", "text" => "View during maintenance window"),
					array("id"=>"CATS_HISTORY_LENGTH", "text" => "Maximum Allowed History Items")
				)
			);
		}
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	function html_action_type_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT Action_Type as ID  FROM tblAction_Details  ORDER BY Action_Type ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_action_category_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT Checkbox_Desc as TEXT, Checkbox_Id as ID  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Desc ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_action_register_origin_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT Register_Origin as ID  FROM tblAction_Details  ORDER BY Register_Origin ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_action_origin_table_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT origin_table as ID, Register_Origin  FROM tblAction_Details  ORDER BY Register_Origin ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_form_fields_status_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"0", "text" => "Inactive"),
			array("id"=>"128", "text" => "Active"),
			array("id"=>"2", "text" => "Hidden")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	function html_get_email_status_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"2", "text" => "On"),
			array("id"=>"0", "text" => "Off"),
			array("id"=>"1", "text" => "Test")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	function html_get_employee_status_dd($name, $value='Active') {
		return html_get_status_dd($name,$value,'TBLEMPLOYEE_DETAILS');
	}
	
	function html_get_action_status_dd($name, $value='Open') {
		return html_get_status_dd($name,$value,'TBLACTION_DETAILS');
	}
	
	function html_get_incident_status_dd($name, $value='Open') {
		return html_get_status_dd($name,$value,'TBLINCIDENT_DETAILS');
	}
	
	function html_get_status_dd($name,$value='',$type='',$parameters=''){
		$sql="select status_title as ID from status_values where status_type='$type'";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_form_types_dd($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_types_options($value), $value, $parameters, false, true);
	}
	
	/* Risk Perspectives - drop down list
	|----------------------------------------------------------------------
	| Risk Definitions
	| - Edit
	|----------------------------------------------------------------------
	*/
	function html_form_draw_risk_perspectives_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter();
		$sql = "SELECT RISKTYPEID as ID,RISKTYPEDESC as TEXT FROM tblRiskTypes ORDER BY RISKTYPEDESC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Checkbox Desc - drop down list
	|----------------------------------------------------------------------
	| Incident Checkboxes
	| - Search/edit/new
	|----------------------------------------------------------------------
	*/
	function html_form_draw_checkbox_description_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT Checkbox_Desc TEXT FROM tblIncidentForm_Checkboxes WHERE Checkbox_Type != 'Incident_Category' AND Checkbox_Type != 'Report_Type' ORDER BY Checkbox_Desc ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	/* Checkbox Types - drop down list
	|----------------------------------------------------------------------
	| Incident Checkboxes
	| - Search/edit/new
	|----------------------------------------------------------------------
	*/
	function html_form_draw_checkbox_types_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT Checkbox_Type as ID, Dropdown_Display as TEXT  FROM tblIncidentForm_Checkboxes WHERE Checkbox_Type != 'Incident_Category' AND Checkbox_Type != 'Report_Type' ORDER BY Dropdown_Display ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	/* Incident Categories - drop down list
	|----------------------------------------------------------------------
	| Incidents
	| - Search/edit/new
	|----------------------------------------------------------------------
	*/
	function html_form_draw_incident_categories_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Checkbox_Id as ID, Checkbox_Desc as TEXT  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Desc ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_incident_report_types_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Checkbox_Id as ID, Checkbox_Desc as TEXT  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Report_Type' ORDER BY Checkbox_Desc ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	
	
	function html_form_draw_reftable_meeting_minutes_dd($name, $value, $parameters = ''){
		$where = get_user_site_access_filter();
		//$sql = "SELECT RefTable_Id AS ID, RefTable_Description AS TEXT  FROM tblRefTable WHERE RefTable_Type = 'Meeting Minutes' $where ORDER BY RefTable_Description";
		$sql = "SELECT RefTable_Description AS TEXT  FROM tblRefTable WHERE RefTable_Type = 'Meeting Minutes' $where ORDER BY RefTable_Description";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Incident Risk Category and Levels - drop down lists
	|----------------------------------------------------------------------
	| INCIDENTS
	| - Search
	| - Edit
	| - New
	|----------------------------------------------------------------------
	*/
	function html_form_draw_risk_level_dd($name, $value, $parameters = ''){
		$sql = "select RefTable_Id as ID, RefTable_Description as TEXT from tblRefTable WHERE RefTable_Type = 'Risk Level'";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_risk_category_dd($name, $value, $parameters = ''){
		$sql = "select RefTable_Id as ID, RefTable_Description as TEXT from tblRefTable WHERE RefTable_Type = 'Risk Category' ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Major Hazard Process - drop down list
	|----------------------------------------------------------------------
	| MAJOR HAZARDS TABLE
	| - Search
	| - Edit
	| - New
	|----------------------------------------------------------------------
	*/

	function html_form_draw_major_hazard_process_dd($name, $value, $parameters = ''){
		$sql = "SELECT RefTable_Id as ID, RefTable_Description as TEXT  FROM tblRefTable  WHERE RefTable_Type = 'Major Hazard Process' ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Major Hazard Types - drop down list
	|----------------------------------------------------------------------
	| MAJOR HAZARDS TABLE
	| - Search
	| - Edit
	| - New
	|----------------------------------------------------------------------
	*/
	function html_form_draw_major_hazard_type_dd($name, $value, $parameters = ''){
		$sql = "SELECT RefTable_Id as ID, RefTable_Description as TEXT  FROM tblRefTable  WHERE RefTable_Type = 'Major Hazard Type' ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Major Hazard Categories - drop down list
	|----------------------------------------------------------------------
	| MAJOR HAZARDS TABLE
	| - Search
	| - Edit
	| - New
	|----------------------------------------------------------------------
	*/
	function html_form_draw_major_hazard_category_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Checkbox_Id as ID, Checkbox_Desc as TEXT  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Desc ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Hazard Status - drop down list
	|----------------------------------------------------------------------
	| MAJOR HAZARDS TABLE
	| - Search
	| - Edit
	| - New
	|----------------------------------------------------------------------
	*/
	function html_form_daw_hazard_status_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"0", "text" => "Current Hazards"),
			array("id"=>"1", "text" => "Hazards No Longer Major Risk")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	// DAVID: Shift Function
	function html_form_draw_reftable_shift_dd($name, $value, $parameters = ''){
		$where = get_user_site_access_filter('single');
		$sql = "SELECT RefTable_Id AS ID, RefTable_Description AS TEXT  FROM tblRefTable WHERE RefTable_Type = 'Shift' $where ORDER BY RefTable_Description";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	// DAVID: Subject Function
	function html_form_draw_reftable_subject_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT DISTINCT RefTable_Id AS ID, RefTable_Description AS TEXT FROM tblRefTable  WHERE RefTable_Type = 'Obligation Subject' ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}	
	
	// DAVID: Issue Function
	function html_form_draw_reftable_issue_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT DISTINCT RefTable_Id AS ID, RefTable_Description AS TEXT FROM tblRefTable  WHERE RefTable_Type = 'Obligation Issue' ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}		
	
	// DAVID: Read Emp Positions
	function html_form_draw_employee_positions_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT DISTINCT Position_Id AS ID, Position_Description AS TEXT FROM tblPosition ORDER BY Position_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}			
	

	/* Reference Table Types - drop down list
	|----------------------------------------------------------------------
	| REFERENCE TABLE
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_reference_table_types_dd($name, $value, $parameters = '')
	{
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT DISTINCT RefTable_Type AS TEXT FROM tblRefTable ORDER BY RefTable_Type ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}		
    

	/* Audit or Inspection Type - drop down list
	|----------------------------------------------------------------------
	| AUDIT AND INSPECTIONS
	| - Add
	|----------------------------------------------------------------------
	*/

	function html_form_draw_reference_table_audit_inspection_types_dd($name, $value, $parameters = '')
	{
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT RefTable_ID AS ID, RefTable_Description AS TEXT FROM tblRefTable WHERE RefTable_Type = 'Audit or Inspection' ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}		

	/* Reference Table Description - drop down list
	|----------------------------------------------------------------------
	| REFERENCE TABLE
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_reference_table_descriptions_dd($name, $value, $parameters = '')
	{
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT DISTINCT RefTable_Description AS TEXT FROM tblRefTable ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}	


	/* Reported By - drop down list
	|----------------------------------------------------------------------
	| OTHER RECORDS
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_reported_by_dd($name, $value, $parameters = ''){
		//$where = get_user_site_access_filter('single');
		$sql = "SELECT DISTINCT Emp_Name AS TEXT FROM VIEW_EMPLOYEE_DETAILS ORDER BY Emp_Name ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}		
    


	/* Inspection Type - drop down list
	|----------------------------------------------------------------------
	| AUDITS AND INSPECTIONS FORM
	| - Search
	|
	| GOVERNMENT INSPECTION FORM
	| - Search
	|----------------------------------------------------------------------
	*/

	function html_form_draw_inspection_type_dd($name, $value, $parameters = '')
	{
		$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT RefTable_Description as TEXT  FROM tblRefTable  WHERE RefTable_Type = 'Requirement Type' $where ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	
	/* Other Reports - drop down list
	|----------------------------------------------------------------------
	| Other Records
	| - Search
	|----------------------------------------------------------------------
	*/	
	
	function html_form_draw_reftable_other_reports_dd($name, $value, $parameters = '')
	{
		$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT RefTable_Description as TEXT  FROM tblRefTable  WHERE RefTable_Type = 'Other Reports' $where ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	
	/* Incident Numbers - drop down list
	|----------------------------------------------------------------------
	| LOST DAYS
	| - Search
	|----------------------------------------------------------------------
	*/	

	function html_form_draw_incident_numbers_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT Incident_Number AS ID, IncDisplyId AS TEXT FROM tblIncident_Details WHERE Injured_Person_Id > 0 ORDER BY IncDisplyId ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', false), $value, $parameters, false, false);
	}
		
	
	/* Injured Persons - drop down list
	|----------------------------------------------------------------------
	| LOST DAYS
	| - Search
	|----------------------------------------------------------------------
	*/	

	function html_form_draw_injured_persons_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT DISTINCT Employee_Number AS ID, Emp_Name AS TEXT  FROM View_Injured_People ORDER BY Emp_Name ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	/* Government Departments - drop down list
	|----------------------------------------------------------------------
	| GOVERNMENT INSPECTION FORM
	| - Search
	|----------------------------------------------------------------------
	*/
	
	function html_form_draw_government_departments_dd($name, $value, $parameters = '')
	{
		$sql = "SELECT DISTINCT Govt_Department as TEXT FROM tblGovernment_Details  ORDER BY UPPER(Govt_Department) ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	+----------------------------------------
	| Get roles select list
	+----------------------------------------
	*/
	function html_draw_sites_list($name,$value='')
	{
		// explode the value
		$values = explode(",",$value);
		
		$sql = "select site_id as ID, site_description as TEXT from tblsite ";
		//echo($sql);
		$result = db_query($sql) or die(db_error());
		
		$ret = '<select class="sfield" name="'.$name.'[]" id="'.$name.'" multiple size="4" style="width:200px;">';
		while ($row = db_fetch_array($result))
		{
			$selected = (in_array($row['ID'],$values))?"selected":"";
			$ret .= '<option value="'.$row['ID'].'" '.$selected.'>'.$row['TEXT'].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	
	
	/*
	+----------------------------------------
	| Get groups select list
	+----------------------------------------
	*/
	function html_user_groups_list($name,$groups='',$user_id=0)
	{
		if(!is_array($_SESSION['user_details'])) return false;
		// get groups allowed
		$value_id='MASK';
		$text_id='NAME';
		//print_r($_SESSION['user_details']);
		$user_groups = $_SESSION['user_details']['groups'] ;
		if(empty($user_groups)) $user_groups = 0;
		$sql = "select user_group_mask as mask, user_group_name as name from user_group where ((bitand(user_group_mask, ".$user_groups.")>0 or (user_group_mask=0)) and (bitand(status, ".MASK_HIDDEN.")>0 or (status = 0))) order by user_group_mask desc";
		//echo($sql);
		$result = db_query($sql) or die(db_error());
		$ret = '<select class="sfield" name="'.$name.'[]" id="'.$name.'" multiple size="4" style="width:200px;" cumask='.$user_groups.' gmask='.$groups.'>';
		while ($row = db_fetch_array($result))
		{
			$selected = (((int)$row[$value_id] & (int)$groups)>0)?"selected":"";
			if($groups!='' && ((int)$groups==0) && ((int)$row[$value_id]==0)) $selected = "selected";
			$ret .= '<option value="'.$row[$value_id].'" '.$selected.'>'.$row[$text_id].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get roles select list
	+----------------------------------------
	*/
	function html_user_roles_list($name,$roles=0,$user_id=0)
	{
		if(!is_array($_SESSION['user_details'])) return false;
		// get groups allowed
		$value_id='MASK';
		$text_id='NAME';
//print_r($_SESSION['user_details']);

		$user_roles = $_SESSION['user_details']['roles'];
		if(empty($user_roles)) $user_roles = 0;
		$sql = "select user_role_mask as mask, user_role_name as name from user_role where ((bitand(user_role_mask, ".$user_roles.")>0 or (user_role_mask=0)) and (bitand(status, ".MASK_HIDDEN.")>0 or (status = 0))) ";
		//echo($sql);
		$result = db_query($sql) or die(db_error());
		$ret = '<select class="sfield" name="'.$name.'[]" id="'.$name.'" multiple size="4" style="width:200px;" cumask='.$user_roles.' gmask='.$roles.'>';
		while ($row = db_fetch_array($result))
		{
			$selected = ((int)$row[$value_id] & (int)$user_roles)?"selected":"";
			if(((int)$user_roles==0) && ((int)$row[$value_id]==0)) $selected = "selected";
			$ret .= '<option value="'.$row[$value_id].'" '.$selected.'>'.$row[$text_id].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get roles select dropdown
	+----------------------------------------
	*/
	function html_user_status_dd($status=0)
	{
		// get groups allowed
		$ret = '<select class="sfield" name="status" id="status" style="width:200px;" us="'.$_SESSION['user_details']['status'].'">';
		$user_id = $this->getId(); //$_SESSION['user_details']['user_id'];
		$user_status = $_SESSION['user_details']['status'];
		$ret .= '<option value="0" '.(($user_status==0)?"selected":"").'>Active</option>';
		$ret .= '<option value="2" '.(($user_status==2)?"selected":"").'>Inactive</option>';
		if($user_status & 4)
			$ret .= '<option value="4" '.(($user_status==4)?"selected":"").'>System</option>';
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	
	function html_file_links_list($files_in='',$title='Download Files',$title_view='View Files')
	{
		$files = '';
		$files_view = '';
		
		if($files_in!='')
		{
			if(is_array($files_in)) $filenames = $files_in;
			else $filenames = explode("|",$files_in);
			
			foreach($filenames as $value)
			{
				if(file_exists(CATS_FILES_PATH.$value))
				{
					$display_name=preg_replace("/_/i"," ",$value);
					if($mode=='admin')
					{
						//$files .= '<input type="checkbox" name="delfile[]" value="'.$value.'" />&nbsp;<a href="'.WS_FILES_PATH.$value.'" target="_blank">'.$value.'</a><br />';
						$files .= '<a href="'.WS_FILES_PATH.$value.'" target="_blank">'.$value.'</a><br />';
					}
					else
					{
						$files .= '<a href="download.php?dlf='.$value.'">'.$display_name.'</a>';
						if(preg_match("/htm|jpg|gif|png|doc|xls|csv|txt|ppp/i", substr($value,strpos($value,'.'))))
							$files_view .= '<a href="'.WS_FILES_PATH.$value.'">'.$display_name.'</a>';
					}
				}
			}
			if($files != '')
			{
				if($mode!='admin')
				{
					$files = '<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<th height="30" align="left"><span id="features_heading">'.$title.'</span></th>
		</tr>
		<tr>
			<td>
				<div id="features">'.$files.'</div>
			</td>
		</tr>
		';
		if($files_view!=""){
					$files .= '
		<tr>
			<td>
				<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<th height="30" align="left"><span id="features_heading">'.$title_view.'</span></th>
		</tr>
		<tr>
			<td>
				<div id="features">'.$files_view.'</div>
			</td>
		</tr>
		';
		}
					$files .= '
		<tr>
			<td height="30">&nbsp;</td>
		</tr>
		</table>';
		//			<table id="related_files"><tr valign="top"><td>:</td><td><div id="sectionLinks"></div></td><td>'.$title_view.':</td><td><div id="sectionLinks">'.$files_view.'</div></td></tr></table>';
				}
			}
		}
		return $files;
	}

?>