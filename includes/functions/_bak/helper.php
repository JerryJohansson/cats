<?php
/*
+--------------------------------------------------------------------------
|   helper.php
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html for custom elements such as employee lookup field or site lookup dropdown
+---------------------------------------------------------------------------
*/
/*
+---------------------------------------------------------------------------
|	Output a helper function to get employee details remotely via HTTPRequest Object
+---------------------------------------------------------------------------
*/
	function html_draw_employee_helper_linked($name, $value, $parameters = '', $linked = ''){
		$attribs = ' onclick="remote.get_options_add(\'get_site_details_array\',\'user\',this';
		if($linked!=''){
			$attribs .= ",this.form.$linked";
		}
		$attribs .= ');" ';
		$attribs = "'SITE_ID','SITE_ID_text','DEPARTMENT_ID','DEPARTMENT_ID_text','SECTION_ID','SECTION_ID_text'";
		return html_draw_employee_helper($name, $value, $parameters, $attribs);
	}
	
	function html_draw_employee_helper($name, $value, $parameters = '', $xtra_parameters = ''){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$display_value = $val['TEXT'];
		$real_value = $val['ID'];
		if($xtra_parameters!=''){
			$xtra_parameters = ','.$xtra_parameters;
		}
		return '
	<input type="hidden" name="'.$name.'" id="id_'.$name.'" value="'.$real_value.'" />
	<input type="text" style="width:200px;" name="'.$name.'_text" id="id_'.$name.'_text" value="'.$display_value.'" '.$parameters.'
			onfocus="this.className=\'active\';this.select();" 
			onblur="this.className=\'\';" 
	 />
	<input type="image" src="'.WS_ICONS_PATH.'btn_get_employee_array_bg.gif" onclick="_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$name.'_text\''.$xtra_parameters.');return false;" style="cursor:pointer;" align="absmiddle" id="I_'.$name.'" />
';
	}
	
	function html_draw_employee_helper_search($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'0');
		}
		$display_value = $val['TEXT'];
		$real_value = $val['ID'];
		// re-insert posted value for id element
		if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $real_value = main_output_string(stripslashes($_POST[$name]));
    } elseif (main_not_null($real_value)) {
      $real_value = main_output_string($real_value);
    }
		// re-insert posted value for text element
		if ( (isset($_POST[$name.'_text'])) && ($reinsert_value == true) ) {
      $display_value = main_output_string(stripslashes($_POST[$name.'_text']));
    } elseif (main_not_null($display_value)) {
      $display_value = main_output_string($display_value);
    }
		return '
	<input type="hidden" name="'.$name.'" id="id_'.$name.'" value="'.$real_value.'" />
	<input type="text" style="width:200px;" name="'.$name.'_text" id="id_'.$name.'_text" value="'.$display_value.'" '.$xtra_parameters.'
			onfocus="this.className=\'active\';this.select();" 
			onblur="this.className=\'\';" 
	 />
	<input type="image" src="'.WS_ICONS_PATH.'btn_get_employee_array_bg.gif" onclick="_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$name.'_text\');return false;" style="cursor:pointer;" align="absmiddle" id="I_'.$name.'" />
';
	}
	
/*
+------------------------------------------------------------------------
|  HTML Table Rows for use in the risk definitions form
+------------------------------------------------------------------------
*/
	function html_risk_definition_helper($type = '1', $category = '0', $parameters = ''){
		global $db;
		//$db->debug=true;
		$categories = array('Consequence', 'Likelihood');
		$sql = "SELECT * FROM View_Risk_Definitions WHERE RiskDefType = $type AND RiskRangeCL = $category ";
		$rs = db_query($sql);
		
		$category_name = $categories[$category];
		$rows = '
		<tr><th colspan="4" align="left">Risk Category: <font color="#00AF00">'.$category_name.'</font></th></tr>
		<tr>
			<th align="left">Definition Title</th>
			<th align="left" colspan="3">Definition Description</th>
		</tr>';
		$i = 0;
		while ($row = $rs->FetchRow()) {
			$i++;
			$rows .= '
		<tr>
			<td class="label"><label>'.$row["RISKRANGEDESC"].'</label></td> 
			<td colspan="3">
				<textarea class="sfield" name="'.$category_name.'_txt['.$row["RISKRANGEORDER"].']" cols="70" rows="3" maxlength="250">'.$row["RISKDEFDESC"].'</textarea> 
				<input type="hidden" name="'.$category_name.'['.$row["RISKRANGEORDER"].']" value="'.$row["RISKDEFDESC"].'">
				<input type="hidden" name="'.$category_name.'ID['.$row["RISKRANGEORDER"].']" value="'.$row["RISKDEFID"].'">
			</td>
		</tr>';
		}
		$rows .= '<input type="hidden" name="'.$category_name.'_cnt" value="'.$i.'">';
		return $rows;
	}
	

/*
+------------------------------------------------------------------------
|
|  Function: html_form_functions_chooser
|  Purposes: Combines function files dd with functions dd
|  Returns: (string)HTML dropdown group that remotes together to filter functions list
|  Params: 
|    (string)name = Name of main dropdown box(functions dd)
|    (variant)value = Value of dropdown box/s ... can be array("selected file value","selected function value")
|    (string)parameters = Element attributes
|
+------------------------------------------------------------------------
*/
	function html_form_functions_chooser($file_value='html_functions.php', $name='funcs', $value='html_draw_input_field', $parameters = ''){
		$files_dd = html_form_function_files_helper($name.'_files', $value, $parameters . 'onchange="remote.get_options_add(\'get_functions_array\',\'admin\',this,this.form.'.$name.');"');
		$ar=explode("_",$value);
		if(is_array($ar))
			$funcs_dd = html_form_functions_helper($ar[0].'_',$name,$value);
		else
			$funcs_dd = html_form_functions_helper(str_replace("functions.php","",$file_value),$name,$value);
		//$func_params = html_draw_input_field($name,$params='');
		return $files_dd.$funcs_dd;
	}
	
	function html_form_function_files_helper($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_function_files_options(), $value, $parameters, false, true);
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_function_files_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|            using defined functions php funciton
|  Returns: Array in the following format:
|    array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|    (string)value = N/A
|
+------------------------------------------------------------------------
*/
	function html_form_function_files_options($preg_match='functions.php'){
		$arr = get_included_files();
		$out=array();
		
		$match="/($preg_match)/i";
		//echo($match);
		foreach ($arr as $file) if(preg_match($match,$file)) $out[]=array("id" => basename($file), "text" => str_replace(array("_",".php"),array(" ",""),basename($file)));
		return $out;
	}
	
	function html_form_functions_helper($filter, $name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_functions_options($filter), $value, $parameters, false, true);
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_functions_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|            using defined functions php funciton
|  Returns: Array in the following format:
|    array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|    (string)value = N/A
|
+------------------------------------------------------------------------
*/
	function html_form_functions_options($preg_match='html',$ftype='user'){
		$vars_arr = get_defined_functions();
		//print_r($vars_arr);
		$arr = $vars_arr[$ftype];
		$out=array();
		$match="/^($preg_match)/i";
		foreach ($arr as $val) if(preg_match($match,$val)) $out[]=array("id" => $val, "text" => $val);
		return $out;
	}

/*
+------------------------------------------------------------------------
|
|  Function: html_form_columns_helper
|  Purposes: Dropdown box of columns filtered by $table_value
|  Returns: HTML Select element:
|  Params: 
|    (string)table_value = name of table to filter(defaults to tblaction_details)
|    (string*)name = name of columns select element
|    (string*)value = value of selected column
|    (string)parameters = attributes to add to the element
|
+------------------------------------------------------------------------
*/
	function html_form_columns_helper($table_value='', $name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_columns_options($table_value), $value, $parameters, false, true);
	}
	function html_form_columns_options($value='tblaction_details'){
		if(empty($value)) $value = 'tblaction_details';// default table
		$sql = "select * from $value where rownum = 1"; // select a row from the table
		$arr = db_get_columns($value); // return columns array
		$out=array();
		foreach($arr as $key => $val){ // create options array
			$out[]=array("id"=>$val['CNAME'],"text"=>$val['CNAME']);
		}
		// re-create options array
		return html_db_options($out, $id='id', $text='text', false);
	}
	
	function html_group_type_options($value){
		$sql = "select distinct FIELD_GROUP_TYPE as ID from form_fields";
		return html_db_options($sql, 'ID', 'ID',true);
	}
	
	function html_form_types_options($value){
		$sql = "select id, name as text from form_types";
		return html_db_options($sql, $id='ID', $text='TEXT', true);
	}
	
	function html_form_tables_helper($cols_name = '', $name, $value, $parameters = ''){
		$cols_name=(empty($cols_name))?$name.'_TABLES':$cols_name;
		return html_draw_pull_down_menu($name, html_form_tables_options($value), $value, $parameters . ' onchange="remote.get_options_add(\'get_columns_array\',\'admin\',this,this.form.'.$cols_name.');" ', false, true);
	}
	function html_form_tables_options($value){
		return html_db_options(db_get_cat_details('TABLE', 'TBL'), $id='id', $text='text', true);
	}

/*
+---------------------------------------------------------------------------
|	Output a form input field which calls a remote script when the onchange event is triggered
+---------------------------------------------------------------------------
*/	
	
	function html_draw_site_linked($name, $value='', $parameters = '', $department='', $section=''){
		$attribs = ' onchange="remote.get_options_add(\'get_site_details_array\',\'user\',this';
		if($department!=''){
			$attribs .= ",this.form.$department";
		}
		if($section!=''){
			$attribs .= ",this.form.$section";
		}
		$attribs .= ');" '.$parameters;
		
		$ret = html_form_draw_site($name, $value, $attribs);
		return $ret;
	}
	
	function html_form_draw_site($name, $value, $parameters = ''){
		if($value>0){
			$description = db_get_field_value("select site_description from tblsite where site_id = $value","site_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, ' disabled=true ').html_draw_hidden_field($name, $value, $parameters);
		return $ret;
	}
	
	function html_form_draw_department($name, $dept_id, $parameters=''){
		if($dept_id>0){
			$sql = "select department_description from tbldepartment where department_id = $dept_id";// and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"department_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' disabled=true ').html_draw_hidden_field($name, $value);
		return $ret;
	}
	function html_form_draw_section($name, $section_id, $parameters=''){
		if($section_id>0){// && $dept_id>0 && $site_id>0){
			$sql = "select section_description from tblsection where section_id=$section_id";//department_id = $dept_id and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"section_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' disabled=true ').html_draw_hidden_field($name, $value);
		return $ret;
	}
?>