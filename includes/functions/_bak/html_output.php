<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS General HTML output functions
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

/*
+---------------------------------------------------------------------------
|	The HTML href link wrapper function
+---------------------------------------------------------------------------
*/
  function ezw_href_link($page = '', $parameters = '', $connection = 'NONSSL', $add_session_id = true, $search_engine_safe = true) {
    global $request_type, $session_started, $SID;
		
    if (!ezw_not_null($page)) {
      die('<br><br><font color="#ff0000"><b>Error!</b></font><br><br><b>Unable to determine the page link!<br><br>');
    }

    if ($connection == 'NONSSL') {
      $link = (IS_ADMIN) ? DIR_WS_ADMIN : DIR_WS_PUBLIC ;
    } elseif ($connection == 'SSL') {
      if (ENABLE_SSL == true) {
        $link = (IS_ADMIN) ? HTTPS_WS_ADMIN : HTTPS_WS_PUBLIC;
      } else {
        $link = (IS_ADMIN) ? DIR_WS_ADMIN : DIR_WS_PUBLIC ;
      }
    } else {
      die('<br><br><font color="#ff0000"><b>Error!</b></font><br><br><b>Unable to determine connection method on a link!<br><br>Known methods: NONSSL SSL</b><br><br>');
    }

    if (ezw_not_null($parameters)) {
      $link .= $page . '?' . ezw_output_string($parameters);
      $separator = '&';
    } else {
      $link .= $page;
      $separator = '?';
    }

    while ( (substr($link, -1) == '&') || (substr($link, -1) == '?') ) $link = substr($link, 0, -1);

// Add the session ID when moving from different HTTP and HTTPS servers, or when SID is defined
    if ( ($add_session_id == true) && ($session_started == true) && (SESSION_FORCE_COOKIE_USE == 'False') ) {
      if (ezw_not_null($SID)) {
        $_sid = $SID;
      } elseif ( ( ($request_type == 'NONSSL') && ($connection == 'SSL') && (ENABLE_SSL == true) ) || ( ($request_type == 'SSL') && ($connection == 'NONSSL') ) ) {
        if (HTTP_COOKIE_DOMAIN != HTTPS_COOKIE_DOMAIN) {
          $_sid = ezw_session_name() . '=' . ezw_session_id();
        }
      }
    }

    if ( (SEARCH_ENGINE_FRIENDLY_URLS == 'true') && ($search_engine_safe == true) ) {
      while (strstr($link, '&&')) $link = str_replace('&&', '&', $link);

      $link = str_replace('?', '/', $link);
      $link = str_replace('&', '/', $link);
      $link = str_replace('=', '/', $link);

      $separator = '?';
    }

    if (isset($_sid)) {
      $link .= $separator . $_sid;
    }

    return $link;
  }


/*
+---------------------------------------------------------------------------
|	The HTML image wrapper function
+---------------------------------------------------------------------------
*/
	function ezw_get_box_start() {
		return '<table class="box" width="100%"><tr><td class="hdr">';
	}
	
	function ezw_get_box_body() {
		return '</td></tr><tr><td class="body">';
	}
	
	function ezw_get_box_end() {
		return '</td></tr></table>';
	}

/*
+---------------------------------------------------------------------------
|	The HTML image wrapper function
+---------------------------------------------------------------------------
*/
  function ezw_image($src, $alt = '', $width = '', $height = '', $parameters = '') {
    if ( (empty($src) || ($src == DIR_WS_IMAGES)) && (IMAGE_REQUIRED == 'false') ) {
      return false;
    }

// alt is added to the img tag even if it is null to prevent browsers from outputting
// the image filename as default
    $image = '<img src="' . ezw_output_string($src) . '" border="0" alt="' . ezw_output_string($alt) . '"';

    if (ezw_not_null($alt)) {
      $image .= ' title=" ' . ezw_output_string($alt) . ' "';
    }

    if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true') && (empty($width) || empty($height)) ) {
      if ($image_size = @getimagesize($src)) {
        if (empty($width) && ezw_not_null($height)) {
          $ratio = $height / $image_size[1];
          $width = $image_size[0] * $ratio;
        } elseif (ezw_not_null($width) && empty($height)) {
          $ratio = $width / $image_size[0];
          $height = $image_size[1] * $ratio;
        } elseif (empty($width) && empty($height)) {
          $width = $image_size[0];
          $height = $image_size[1];
        }
      } elseif (IMAGE_REQUIRED == 'false') {
        return false;
      }
    }

    if (ezw_not_null($width) && ezw_not_null($height)) {
      $image .= ' width="' . ezw_output_string($width) . '" height="' . ezw_output_string($height) . '"';
    }

    if (ezw_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= '>';

    return $image;
  }


/*
+---------------------------------------------------------------------------
|	The HTML form submit button wrapper function
|	Outputs a button in the selected language
+---------------------------------------------------------------------------
*/  function ezw_image_submit($image, $alt = '', $parameters = '') {
    global $language;

    $image_submit = '<input type="image" src="' . ezw_output_string(DIR_WS_IMAGES . $image) . '" border="0" alt="' . ezw_output_string($alt) . '"';

    if (ezw_not_null($alt)) $image_submit .= ' title=" ' . ezw_output_string($alt) . ' "';

    if (ezw_not_null($parameters)) $image_submit .= ' ' . $parameters;

    $image_submit .= '>';

    return $image_submit;
  }

/*
+---------------------------------------------------------------------------
|	Output a function button in the selected language
+---------------------------------------------------------------------------
*/
  function ezw_image_button($image, $alt = '', $parameters = '') {
    global $language;

    return ezw_image(DIR_WS_IMAGES . $image, $alt, '', '', $parameters);
  }

////
// Output a separator either through whitespace, or with an image
  function ezw_draw_separator($image = 'pixel_black.gif', $width = '100%', $height = '1') {
    return ezw_image(DIR_WS_IMAGES . $image, '', $width, $height);
  }

/*
+---------------------------------------------------------------------------
|	Output a form
+---------------------------------------------------------------------------
*/
  function ezw_draw_form($name, $action, $method = 'post', $parameters = '') {
    $form = '<form name="' . ezw_output_string($name) . '" action="' . ezw_output_string($action) . '" method="' . ezw_output_string($method) . '"';

    if (ezw_not_null($parameters)) $form .= ' ' . $parameters;

    $form .= '>';

    return $form;
  }
/*
+---------------------------------------------------------------------------
|	Output a form filefield
+---------------------------------------------------------------------------
*/
  function ezw_draw_file_field($name, $required = false) {
    return ezw_draw_input_field($name, '', '', 'file', $required);
  }
/*
+---------------------------------------------------------------------------
|	Output a form input field
+---------------------------------------------------------------------------
*/
  function ezw_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield" type="' . ezw_output_string($type) . '" name="' . ezw_output_string($name) . '"';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . ezw_output_string(stripslashes($GLOBALS[$name])) . '"';
    } elseif (ezw_not_null($value)) {
      $field .= ' value="' . ezw_output_string($value) . '"';
    }

    if (ezw_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

/*
+---------------------------------------------------------------------------
|	Output a form password field
+---------------------------------------------------------------------------
*/
  function ezw_draw_password_field($name, $value = '', $parameters = 'maxlength="40"') {
    return ezw_draw_input_field($name, $value, $parameters, 'password', false);
  }

/*
+---------------------------------------------------------------------------
|	Output a selection field - alias function for ezw_draw_checkbox_field() and ezw_draw_radio_field()
+---------------------------------------------------------------------------
*/
  function ezw_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {
    $selection = '<input class="bfield" type="' . ezw_output_string($type) . '" name="' . ezw_output_string($name) . '"';

    if (ezw_not_null($value)) $selection .= ' value="' . ezw_output_string($value) . '"';

    if ( ($checked == true) || ( isset($GLOBALS[$name]) && is_string($GLOBALS[$name]) && ( ($GLOBALS[$name] == 'on') || (isset($value) && (stripslashes($GLOBALS[$name]) == $value)) ) ) ) {
      $selection .= ' CHECKED';
    }

    if (ezw_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= '>';

    return $selection;
  }

/*
+---------------------------------------------------------------------------
|	Output a form checkbox field
+---------------------------------------------------------------------------
*/
  function ezw_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return ezw_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

/*
+---------------------------------------------------------------------------
|	Output a form radio field
+---------------------------------------------------------------------------
*/
  function ezw_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return ezw_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }

/*
+---------------------------------------------------------------------------
|	Output a form textarea field
+---------------------------------------------------------------------------
*/
  function ezw_draw_textarea_field($name, $wrap, $width, $height, $text = '', $parameters = '', $reinsert_value = true) {
    $field = '<textarea class="sfield" name="' . ezw_output_string($name) . '" wrap="' . ezw_output_string($wrap) . '" cols="' . ezw_output_string($width) . '" rows="' . ezw_output_string($height) . '"';

    if (ezw_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($GLOBALS[$name])) && ($reinsert_value == true) ) {
      $field .= stripslashes($GLOBALS[$name]);
    } elseif (ezw_not_null($text)) {
      $field .= $text;
    }

    $field .= '</textarea>';

    return $field;
  }

/*
+---------------------------------------------------------------------------
|	Output a form hidden field
+---------------------------------------------------------------------------
*/
  function ezw_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . ezw_output_string($name) . '"';

    if (ezw_not_null($value)) {
      $field .= ' value="' . ezw_output_string($value) . '"';
    } elseif (isset($GLOBALS[$name])) {
      $field .= ' value="' . ezw_output_string(stripslashes($GLOBALS[$name])) . '"';
    }

    if (ezw_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

/*
+---------------------------------------------------------------------------
|	Hide form elements
+---------------------------------------------------------------------------
*/
  function ezw_hide_session_id() {
    global $session_started, $SID;

    if (($session_started == true) && ezw_not_null($SID)) {
      return ezw_draw_hidden_field(ezw_session_name(), ezw_session_id());
    }
  }

/*
+---------------------------------------------------------------------------
|	Output a form pull down menu
+---------------------------------------------------------------------------
*/
  function ezw_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false, $empty_option = false) {
    $field = '<select class="sfield" name="' . ezw_output_string($name) . '"';

    if (ezw_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($GLOBALS[$name])) $default = stripslashes($GLOBALS[$name]);
		if($empty_option==true) $field .= '<option></option>';
    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . ezw_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' SELECTED';
      }

      $field .= '>' . ezw_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }
	
	function ezw_get_pull_down_options($sql, $field_value=0, $field_text=1) {
		ezw_db_connect();
		$i=0;
		$ret=array();
		$result = ezw_db_query($sql);
		while($row = ezw_db_fetch_array($result, MYSQL_BOTH))
		{
			$ret[$i]['id'] = $row[$field_value];
			$ret[$i]['text'] = $row[$field_text];
			$i++;
		}
		return $ret;
	}
	
	function ezw_get_input_start_end_dates($date, $date_value, $end_date, $end_date_value, $show_start_lebel = false){
		$s="";
		if($show_start_label) $s .= '<label for="id_'.$date.'" style="width:auto;display:inline;">Start Date:&nbsp;</label>';
		//$s .= '<input class="dfield" name="'.$date.'" id="id_'.$date.'" type="text" value="'.$date_value.'" onclick="this.blur();_show_calendar(this);"><label for="id_'.$end_date.'" style="width:auto;display:inline;"> --> End Date:&nbsp;</label><input class="dfield" name="'.$end_date.'" id="id_'.$end_date.'" type="text" value="'.$end_date_value.'" " onclick="this.blur();_show_calendar(this);">';
		$s .= ezw_get_calendar_date_field($date, $date_value,'Y-m-j','yyyy-mm-dd','j-M-Y','dd-mmm-yyyy') . '<label for="id_'.$end_date.'" style="width:auto;display:inline;">';
		$s .= ' --> End Date:&nbsp;</label>' . ezw_get_calendar_date_field($end_date, $end_date_value,'Y-m-j','yyyy-mm-dd','j-M-Y','dd-mmm-yyyy', '', ' onchange="check_raw_date_range(this.form.elements[\''.$date.'\'],this.form.elements[\''.$end_date.'\']);" ');
		$s .= '
		<script type="text/javascript">
		EDITOR_VALIDATE += ((EDITOR_VALIDATE!="")?";":"") + "'.$date.'|'.$end_date.';rawdaterange;Start Date|End Date";
		</script>
		';
		return $s;
	}
	
	function ezw_get_input_start_end_datetimes($start_date, $start_date_value, $end_date, $end_date_value, $alt_start_time = '', $alt_end_time = '', $start_label = 'Start Date', $end_label = 'End Date', $show_start_label = false){
		$s="";
		$t_raw = 'H:i';
		$t_in = 'HH:MM';
		$f_r = 'Y-m-j';
		$f_raw = $f_r.' '.$t_raw;
		$f_i = 'yyyy-mm-dd';
		$f_in = $f_i.' '.$t_in;
		$d_r = 'j-M-Y';
		$d_raw = $d_r.' '.$t_raw;
		$d_i = 'dd-mmm-yyyy';
		$d_in = $d_i.' '.$t_in;
		//$s .= ';'.$start_date_value.':'.$end_date_value;
		$cal_start_time = ezw_db_getdate($start_date_value,$timestamp,$t_raw);
		if($cal_start_time=="00:00") $cal_start_time=(empty($alt_start_time))?"09:00":$alt_start_time;
		$start_date_value = ezw_db_getdate($start_date_value,$timestamp,$f_r).' '.$cal_start_time;
		$start_date_value = ezw_db_getdate($start_date_value,$timestamp,$f_raw);
		
		$cal_end_time = ezw_db_getdate($end_date_value,$timestamp,$t_raw);
		if($cal_end_time=="00:00") $cal_end_time=(empty($alt_end_time))?"16:30":$alt_end_time;
		//$s .= ';'.$cal_time.':join the date parts='.ezw_db_getdate($end_date_value,$timestamp,$f_r).' '.$cal_end_time;
		$end_date_value = ezw_db_getdate($end_date_value,$timestamp,$f_r).' '.$cal_end_time;
		$end_date_value = ezw_db_getdate($end_date_value,$timestamp,$f_raw);
		
		$control_start = ezw_get_calendar_date_field($start_date, $start_date_value, $f_raw,$f_in,$d_raw,$d_in);
		$control_end = ezw_get_calendar_date_field($end_date, $end_date_value,$f_raw,$f_in,$d_raw,$d_in, '', ' onchange="check_raw_date_range(this.form.elements[\''.$start_date.'\'],this.form.elements[\''.$end_date.'\']);" ');
		
		//$s .= ';start='.$start_date_value.':end='.$end_date_value;
		$s_start_label='';
		if($show_start_label){
			$s .= '<table><tr><td><label for="id_'.$start_date.'" style="width:auto;display:inline;">'.$start_label.':&nbsp;</label></td>';
			$s .= '</td><td></td><td>';
			$s .= '<label for="id_'.$end_date.'" style="width:auto;display:inline;">'.$end_label.':&nbsp;</label>';
			$s .= '</td></tr><tr><td>';
			$s .= $control_start.'</td><td>&nbsp;<b>to -></b>&nbsp;</td><td>'.$control_end.'</td></tr></table>';
		}else{
			$s .= $control_start;
			$s .= '<label for="id_'.$end_date.'" style="width:auto;display:inline;">';
			$s .= ' -> '.$end_label.':&nbsp;</label>';
			$s .= $control_end;
		}
		$s .= '
		<script type="text/javascript">
		EDITOR_VALIDATE += ((EDITOR_VALIDATE!="")?";":"") + "'.$start_date.'|'.$end_date.';rawdaterange;'.$start_label.'|'.$end_label.'";
		</script>
		';
		return $s;
	}

	function ezw_get_calendar_date_field($name = 'cal_date', $value = '', $format_in = 'j-M-Y', $format = 'dd-mm-yyyy', $display_in = 'j-M-Y', $display = 'dd-mm-yyyy', $alt_time_value = '', $attributes = '', $timestamp = false){
		//$value = '2005-05-23 08:30';
		if(empty($value)){
			$cal_date = date($format_in);
			$cal_display_date = date($display_in);
		}else{
			$cal_date = ezw_db_getdate($value,$timestamp,$format_in);
			$cal_display_date = ezw_db_getdate($value,$timestamp,$display_in);
		}
		$class_name = (preg_match("/H|M/",$format))?"dtfield":"dfield";
		$s = '
		<input class="'.$class_name.'" name="'.$name.'_d" id="id_'.$name.'_d" type="text" value="'.$cal_display_date.'" onkeypress="return _deny_keypress(this,0,event);" onfocus="_show_calendar(this.form.elements[\''.$name.'\'],\''.$display.'\',\''.$format.'\');" '.$attributes.'>
		<input name="'.$name.'" id="id_'.$name.'" type="hidden" value="'.$cal_date.'" />
		';
		//$s .= $cal_date.':'.':'.$format_in.$cal_display_date.':'.$display_in;
		return $s;
	}
	
	function ezw_get_calendar_time_field($name = 'cal_time', $value = '', $attributes = '', $timestamp = false, $alt_value = ''){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		$cal_time = ezw_db_getdate($value,$timestamp,'H:i');
		if($cal_time=="00:00") $cal_time=(empty($alt_value))?"09:00":$alt_value;
		$s = '
		<input title="'.$value.'" class="tfield" style="width:58px" name="'.$name.'" type="text" value="'.$cal_time.'" '.$attributes.'><span class="format">24hr(HH:MM)</span>';
		return $s;
	}
	
	function ezw_get_calendar_date_time_field($name = 'cal_date', $value = '', $alt_value = ''){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		
		$cal_raw_date = ezw_db_getdate($value,false,'Y-m-d H:i');
		
		$s = ezw_get_calendar_date_field($name, $cal_raw_date, 'Y-m-d H:i', 'yyyy-m-d HH:MM', 'j-M-Y H:i', 'dd-mmm-yyyy HH:MM');/* . '
		<label style="text-align:left;display:inline;" for="id_'.$name.'_t"> at </label>' . ezw_get_calendar_time_field($name . '_t', $value,'onchange="checktime(this);get_raw_date(this, \''.$name.'_d\',\''.$name.'_t\',\''.$name.'\');"',false,$alt_value) . '
		<input name="'.$name.'" type="hidden" value="'.$cal_raw_date.'">
		';*/
		return $s;
	}
/*
+---------------------------------------------------------------------------
|	Returns the parent nodes pagetype
+---------------------------------------------------------------------------
*/
//ezw_draw_pull_down_menu('type', ezw_get_pull_down_options("select name, insert(`name`,1,1,substring(upper(name),1,1)) from ".TABLE_NODE_TYPE." where status > 0 "), $type, (($action=="edit")?"disabled":""));
  function ezw_get_parent_pagetype($pid) {
    $arr=ezw_get_allowed_pagetype($pid);
		return $arr[0][0];
  }
	function ezw_get_pagetype($type){
		return ezw_db_getfieldvalue("select nt.name from node_type nt where nt.id=$type");
	}
	function ezw_get_allowed_pagetype($pid) {
		$ret=array();
    $result = ezw_db_query("select nt.name, nt.allowed_child_types as types from node n, node_type nt where n.node_type=nt.id and n.id=$pid");
		while($row = ezw_db_fetch_array($result, MYSQL_BOTH))
		{
			$ret[]=array($row['name'], $row['types']);
		}
		return $ret;
  }
	function ezw_get_allowed_pagetype_matrix() {
		$ret=array();
    $result = ezw_db_query("select nt.name, nt.allowed_child_types as types from node_type nt where nt.status > 0 ");
		while($row = ezw_db_fetch_array($result, MYSQL_BOTH))
		{
			$ret[]=array($row['name'], $row['types']);
		}
		return $ret;
  }
/*
+---------------------------------------------------------------------------
|	Creates a pull-down list of countries
+---------------------------------------------------------------------------
*/
  function ezw_get_country_list($name, $selected = '', $parameters = '') {
    $countries_array = array(array('id' => '', 'text' => PULL_DOWN_DEFAULT));
    $countries = ezw_get_countries();

    for ($i=0, $n=sizeof($countries); $i<$n; $i++) {
      $countries_array[] = array('id' => $countries[$i]['countries_id'], 'text' => $countries[$i]['countries_name']);
    }

    return ezw_draw_pull_down_menu($name, $countries_array, $selected, $parameters);
  }
?>
