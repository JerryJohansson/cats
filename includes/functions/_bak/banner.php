<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Banner Functions
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

/*
+---------------------------------------------------------------------------
|	Sets the status of a banner
+---------------------------------------------------------------------------
*/
  function ezw_set_banner_status($banners_id, $status) {
    if ($status == '1') {
      return ezw_db_query("update " . TABLE_BANNERS . " set status = '1', date_status_change = now(), date_scheduled = NULL where banners_id = '" . (int)$banners_id . "'");
    } elseif ($status == '0') {
      return ezw_db_query("update " . TABLE_BANNERS . " set status = '0', date_status_change = now() where banners_id = '" . (int)$banners_id . "'");
    } else {
      return -1;
    }
  }

/*
+---------------------------------------------------------------------------
|	Auto activate banners
+---------------------------------------------------------------------------
*/
  function ezw_activate_banners() {
    $banners_query = ezw_db_query("select banners_id, date_scheduled from " . TABLE_BANNERS . " where date_scheduled != ''");
    if (ezw_db_num_rows($banners_query)) {
      while ($banners = ezw_db_fetch_array($banners_query)) {
        if (date('Y-m-d H:i:s') >= $banners['date_scheduled']) {
          ezw_set_banner_status($banners['banners_id'], '1');
        }
      }
    }
  }

/*
+---------------------------------------------------------------------------
|	Auto expire banners
+---------------------------------------------------------------------------
*/
  function ezw_expire_banners() {
    $banners_query = ezw_db_query("select b.banners_id, b.expires_date, b.expires_impressions, sum(bh.banners_shown) as banners_shown from " . TABLE_BANNERS . " b, " . TABLE_BANNERS_HISTORY . " bh where b.status = '1' and b.banners_id = bh.banners_id group by b.banners_id");
    if (ezw_db_num_rows($banners_query)) {
      while ($banners = ezw_db_fetch_array($banners_query)) {
        if (ezw_not_null($banners['expires_date'])) {
          if (date('Y-m-d H:i:s') >= $banners['expires_date']) {
            ezw_set_banner_status($banners['banners_id'], '0');
          }
        } elseif (ezw_not_null($banners['expires_impressions'])) {
          if ( ($banners['expires_impressions'] > 0) && ($banners['banners_shown'] >= $banners['expires_impressions']) ) {
            ezw_set_banner_status($banners['banners_id'], '0');
          }
        }
      }
    }
  }

/*
+---------------------------------------------------------------------------
|	Display a banner from the specified group or banner id ($identifier)
+---------------------------------------------------------------------------
*/
  function ezw_display_banner($action, $identifier, $display_type = '') { // image or list
    if ($action == 'dynamic') {
      $banners_query = ezw_db_query("select count(*) as count from " . TABLE_BANNERS . " where status = '1' and banners_group = '" . $identifier . "'");
      $banners = ezw_db_fetch_array($banners_query);
      if ($banners['count'] > 0) {
        $banner = ezw_random_select("select banners_id, banners_title, banners_image, banners_html_text, banners_url from " . TABLE_BANNERS . " where status = '1' and banners_group = '" . $identifier . "'");
      } else {
        //ezw_log_error('<b>EZW ERROR! (ezw_display_banner(' . $action . ', ' . $identifier . ') -> No banners with group \'' . $identifier . '\' found!</b>');
				return '';
      }
    } elseif ($action == 'static') {
      if (is_array($identifier)) {
        $banner = $identifier;
      } else {
        $banner_query = ezw_db_query("select banners_id, banners_title, banners_image, banners_html_text, banners_url from " . TABLE_BANNERS . " where status = '1' and banners_id = '" . (int)$identifier . "'");
        if (ezw_db_num_rows($banner_query)) {
          $banner = ezw_db_fetch_array($banner_query);
        } else {
          //ezw_log_error('<b>EZW ERROR! (ezw_display_banner(' . $action . ', ' . $identifier . ') -> Banner with ID \'' . $identifier . '\' not found, or status inactive</b>');
					return '';
        }
      }
    } else {
			//ezw_log_error('<b>EZW ERROR! (ezw_display_banner(' . $action . ', ' . $identifier . ') -> Unknown $action parameter value - it must be either \'dynamic\' or \'static\'</b>');
      return '';
    }

    if (ezw_not_null($banner['banners_html_text'])) {
      $banner_string = $banner['banners_html_text'];
    } else {
			switch($display_type){
				case 'jsarray': //['image1.jpg','http://worklink.murdoch.wa.edu.au/',1],
					$target=(strstr($banner['banners_url'],'http://') || strstr($banner['banners_url'],'https://'))?'1':'0';
					$banner_string = "['" .DIR_WS_BANNERS . $banner['banners_image']."','". ezw_href_link(FILENAME_REDIRECT, 'action=banner&goto=' . $banner['banners_id']) . "'," . $target ."]";
					break;
				default:
					if(strstr($banner['banners_url'],'http://') || strstr($banner['banners_url'],'https://'))
					{
						$target=' target="_blank"';
					}
					$banner_string = '<a href="' . ezw_href_link(FILENAME_REDIRECT, 'action=banner&goto=' . $banner['banners_id']) . '"' . $target . '>' . ezw_image(DIR_WS_BANNERS . $banner['banners_image'], $banner['banners_title']) . '</a>';
					break;
			}
    }

    ezw_update_banner_display_count($banner['banners_id']);

    return $banner_string;
  }

/*
+---------------------------------------------------------------------------
|	Check to see if a banner exists
+---------------------------------------------------------------------------
*/
  function ezw_banner_exists($action, $identifier) {
    if ($action == 'dynamic') {
      return ezw_random_select("select banners_id, banners_title, banners_image, banners_html_text from " . TABLE_BANNERS . " where status = '1' and banners_group = '" . $identifier . "'");
    } elseif ($action == 'static') {
      $banner_query = ezw_db_query("select banners_id, banners_title, banners_image, banners_html_text from " . TABLE_BANNERS . " where status = '1' and banners_id = '" . (int)$identifier . "'");
      return ezw_db_fetch_array($banner_query);
		} elseif ($action == 'all_id') {
			return ezw_db_query("select banners_id from " . TABLE_BANNERS . " where status = '1' and banners_group = '" . $identifier . "'");
    } else {
      return false;
    }
  }

/*
+---------------------------------------------------------------------------
|	Update the banner display statistics
+---------------------------------------------------------------------------
*/
  function ezw_update_banner_display_count($banner_id) {
    $banner_check_query = ezw_db_query("select count(*) as count from " . TABLE_BANNERS_HISTORY . " where banners_id = '" . (int)$banner_id . "' and date_format(banners_history_date, '%Y%m%d') = date_format(now(), '%Y%m%d')");
    $banner_check = ezw_db_fetch_array($banner_check_query);

    if ($banner_check['count'] > 0) {
      ezw_db_query("update " . TABLE_BANNERS_HISTORY . " set banners_shown = banners_shown + 1 where banners_id = '" . (int)$banner_id . "' and date_format(banners_history_date, '%Y%m%d') = date_format(now(), '%Y%m%d')");
    } else {
      ezw_db_query("insert into " . TABLE_BANNERS_HISTORY . " (banners_id, banners_shown, banners_history_date) values ('" . (int)$banner_id . "', 1, now())");
    }
  }

/*
+---------------------------------------------------------------------------
|	Update the banner click statistics
+---------------------------------------------------------------------------
*/
  function ezw_update_banner_click_count($banner_id) {
    ezw_db_query("update " . TABLE_BANNERS_HISTORY . " set banners_clicked = banners_clicked + 1 where banners_id = '" . (int)$banner_id . "' and date_format(banners_history_date, '%Y%m%d') = date_format(now(), '%Y%m%d')");
  }
	
	function ezw_banner_image_extension() {
		if (function_exists('imagetypes')) {
			if (imagetypes() & IMG_PNG) {
				return 'png';
			} elseif (imagetypes() & IMG_JPG) {
				return 'jpg';
			} elseif (imagetypes() & IMG_GIF) {
				return 'gif';
			}
		} elseif (function_exists('imagecreatefrompng') && function_exists('imagepng')) {
			return 'png';
		} elseif (function_exists('imagecreatefromjpeg') && function_exists('imagejpeg')) {
			return 'jpg';
		} elseif (function_exists('imagecreatefromgif') && function_exists('imagegif')) {
			return 'gif';
		}
		return false;
	}
?>