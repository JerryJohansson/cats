<?php
/*
+--------------------------------------------------------------------------
|   Module Security
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|
+---------------------------------------------------------------------------
|    Purpose:
|			-Provides user Authentication routines.
|			-Creates a user object (array of users details)
|
+---------------------------------------------------------------------------
*/
//ob_start();
//ob_start("ob_gzhandler");
//$oldSessionName=session_name(SESSION_NAME);
//session_start();
//putenv('TZ=Australia/Perth');
//include("user_details.php");
// Authenticate the user

function authenticate($groups_in='',$redir_in='',$return_to_callee=false)
{
//print_r($_SESSION);
//echo(session_id());
//echo($groups_in.":".$_POST['login'].":".$_POST['uid'].":".md5($_POST['pwd']));
	if (isset($_POST['login']) && $_POST['login']=="Login") unset($_SESSION['user_details']);
	//if (isset($_REQUEST['login']) && $_REQUEST['login']=="Login") unset($_SESSION['user_details']);
	if (!is_array($_SESSION['user_details'])) // if there is no user object(array)
	{
	
		session_destroy();
		//if (isset($_POST['login']) && $_POST['login']=="Login")
		if (isset($_REQUEST['login']) && $_REQUEST['login']=="Login")
		{
			ezw_db_connect();
			//$query = sprintf("SELECT * FROM user WHERE email='%s' AND password='%s'",ezw_db_prepare_input($_POST['uid']), md5($_POST['pwd']));
			$query = sprintf("SELECT * FROM user WHERE email='%s' AND password='%s'",ezw_db_prepare_input($_REQUEST['uid']), md5($_REQUEST['pwd']));
			$result = ezw_db_query($query) or die(ezw_db_error());
			if (!($user_data = ezw_db_fetch_array($result)))
			{
				// Display login template
				displayLogin("Incorrect username or password<br>\n");
			}
			else
			{
				/*$lastlogdate = $user_data['logdate'];
				$result = ezw_db_query(sprintf("update user set logdate = '%s', lognum = lognum+1 where user_id='%s'",strftime("%Y-%m-%d %H:%M:%S"),$user_data['user_id']));
				$query = sprintf("SELECT u.user_id, u.groups, u.roles, u.user_account_type, u.email, u.logdate, u.lognum, u.status, d.user_nick, d.firstname, d.lastname FROM `user` as u, user_details as d WHERE u.user_id = d.user_id and u.user_id='%s'", $user_data['user_id']);
				$result = ezw_db_query($query) or die(ezw_db_error());
				if (($user_data = ezw_db_fetch_array($result))!=false)
				{
					session_register("user_details");
					$user_data['lastlogdate'] = $lastlogdate;
					$_SESSION['user_details'] = $user_data;
				}*/
				//if(function_exists('ezw_get_user_details'))
				//echo('ezw_get_user_details');
					ezw_get_user_details($user_data['user_id'],$user_data['logdate']);
				//echo('<br>ezw_get_user_details done');
			}
			if($return_to_callee) return true;
			if(isset($_POST['RedirectURI']))
			{
				header("Location: ".$_POST['RedirectURI']);
			}
			else
			{
				header("Location: index.php");
			}
		}
		else
		{
			if($return_to_callee) return false;
			// Display login template
			if($_SERVER['QUERY_STRING']!='') $_SERVER['QUERY_STRING']='?'.$_SERVER['QUERY_STRING'];
			displayLogin('',$_SERVER['REQUEST_URI'].$_SERVER['QUERY_STRING']);
		}
	}
	elseif($groups_in && $groups_in!='admin')
	{
	
		$user_groups = (int)($_SESSION['user_details']['groups']+0);//explode(",",$_SESSION['user_details']['groups']);
		$groups_result = (((int)$user_groups & (int)$groups_in)>0);//array_intersect($user_groups,$groups_in);
		//print_r($user_groups.'<br>'.$groups_result.'<br>'.$groups_in);
		if(!$groups_result){
			if($return_to_callee) return false;
			displayLogin('<h3>Access Denied</h3><p>You do not have sufficient permissions to view this page. Please use a login with higher permissions.</p>',$_SERVER['REQUEST_URI']);
		}
	}
	else
	{
	
		if (defined('EZW_ADMIN_MASK') && (EZW_ADMIN_MASK & (int)($_SESSION['user_details']['groups']+0))==0 ){
			displayLogin('<h3>Access Denied</h3><p>You do not have sufficient permissions to view this page. Please use a login with higher permissions.</p>',$_SERVER['REQUEST_URI']);
		}
		
		if (isset($_GET['logout']) || isset($_POST['logout']))
		{
			// User has had enough...
			// Log them out and destroy their session
			session_unset();
			session_destroy();
			if($return_to_callee) return true;
			displayLogin("Logged out successfully");
		}
		elseif(isset($_POST['RedirectURI'])) 
		{
			if($return_to_callee) return true;
			header("Location: ".$_POST['RedirectURI']);
			//exit;
		}
	}
}

// Display login template and exit
function displayLogin($msg='',$redir_in='')
{
	require_once(DIR_WS_CLASSES."template.php");
	$tpl = new Template(DIR_WS_TEMPLATES."login.htm");
	if(isset($_POST['RedirectURI']) && $_POST['RedirectURI']!='')
		$redir = $_POST['RedirectURI'];
	elseif($redir_in!='')
		$redir = $redir_in;
	else
		$redir = "index.php";
	$values = array( 
		"MESSAGE"						=> $msg,
		"redirect"					=> $redir
	);
	$tpl->parse($values);
	$tpl->output();
	exit;
}

function getMaskFromList($mask_list='0')
{
	$arr_mask_list = explode(",",$mask_list);
	$ret = 0;
	foreach( $arr_mask_list as $value )
	{
		if(($ret & $value)==0)
			$ret = $ret + $value;
	}
	return $ret;
}

function getListFromMask($mask=0)
{
	/*
	+------------------------------------------------------------------------------
	'  Returns a list from the mask, eg Pass in 7 it returns 1,2,4
	+------------------------------------------------------------------------------
	*/
	$value = 0;
	$count = 0;
	while( $value < $mask )
	{
		// Increase value by factor of two
		$value = pow(2, $count++);
		if (($value & $mask) > 0)
		{
			if($ret == "")
			{
				$ret = $value;
			}
			else
			{
				$ret = $ret.", ".$value;
			}
		}
	}
	return $ret;
}

function download($file)
{
	$dir='templates/';
	if (isset($file))
	{
		 $file=$dir.$file;
		 header('Content-type: application/force-download');
		 header('Content-Transfer-Encoding: Binary');
		 header('Content-length: '.filesize($file));
		 header('Content-disposition: attachment; filename="'.basename($file).'"');
		 readfile($file);
	}
	else
	{
		 echo "No file selected";
	}
}
function ezw_get_user_details($user_id=0,$lastlogdate='')
{
	$result = ezw_db_query(sprintf("update user set logdate = '%s', lognum = lognum+1 where user_id='%s'",strftime("%Y-%m-%d %H:%M:%S"),$user_id));
	$query = sprintf("SELECT u.user_id, u.groups, u.roles, u.user_account_type, u.email, u.logdate, u.lognum, u.status, d.user_nick, d.firstname, d.lastname FROM `user` as u, user_details as d WHERE u.user_id = d.user_id and u.user_id='%s'", $user_id);
	$result = ezw_db_query($query) or die(ezw_db_error());
	if (($user_data = ezw_db_fetch_array($result))!=false)
	{
		session_register("user_details");
		$user_data['lastlogdate'] = $lastlogdate;
		$_SESSION['user_details'] = $user_data;
	}
}
function ezw_history_log()
{
	ezw_db_connect();
	$result = ezw_db_query(sprintf("insert into user_history_log (date_time,user_id,session_id,request,ip,node_id) values('%s','%s','%s','%s','%s','%s')",strftime("%Y-%m-%d %H:%M:%S"),$_SESSION['user_details']['user_id'],session_id(),addslashes(serialize($_REQUEST)),ezw_get_ip_address(),$_REQUEST['id'] ));
	return $result;
}
if(IS_ADMIN) $history_logged = ezw_history_log();
?>
