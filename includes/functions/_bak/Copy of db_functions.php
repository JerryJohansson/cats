<?php
require_once(CATS_CLASSES_PATH . "query.class.php");

/*
+--------------------------------------------------------------------
|
|		Default Database functions go here
|
+--------------------------------------------------------------------
*/
//$db = NewADOConnection(DB_DRIVER);

function db_connect() {
	global $db;
	
	$db = ADONewConnection(DB_DRIVER); # eg 'mysql' or 'postgres'
	// turn on debuging if desired by editing the conf proterty CATS_DEBUG or settng the session 'debug' variable to true
	$db->debug = (isset($_SESSION['debug']))?(bool)$_SESSION['debug']:(defined('CATS_DEBUG')?(bool)(CATS_DEBUG):false);
	
	//$db->fetchMode = (isset($_SESSION['ADODB_FETCH_MODE']))?(int)$_SESSION['ADODB_FETCH_MODE']:(defined('ADODB_FETCH_MODE')?(int)(ADODB_FETCH_MODE):ADODB_FETCH_ASSOC);
	$db->Connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	
	return $db;
}

function db_query($sql){
	global $db;
	return $db->Execute($sql);
}

function db_fetch_array($rs){
	return $rs->GetArray();
}

function db_get_field_value($sql, $field=0){
	global $db;
	$result = db_query($sql);
	if(is_array($field))
	{
		$ret = array();
		$arr = db_fetch_array($result);
		for($i=0;$i<count($field);$i++)
		{
			$ret[$field[$i]] = $arr[$field[$i]];
		}
		return $ret;
	}
	else
	{
		$arr = db_fetch_array($result);
		return $arr[$field];
	}
}


function db_bulk_insert($table, $fields, $values, $dbg=false){
	global $db;
	$db->debug=$dbg;
	$flds=explode(",",$fields);
	$vals=str_repeat("?,",count($flds));
	$vals=preg_replace("/,$/","",$vals);
	$old_bindInputArray=$db->_bindInputArray;
	$db->_bindInputArray=false;
	$ok = $db->Execute("insert into $table ($fields) values ($vals)",$values);
	$db->_bindInputArray=$old_bindInputArray;
	return (bool)$ok;
}

function db_render_pager($sql,$column_labels=false,$row_count=CATS_DEFAULT_ROW_COUNT){
	global $db;
	require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');
	$pager = new ADODB_Pager($db,$sql);
	if(is_array($column_labels)){
		$pager->gridHeader=$column_labels;
	}
	$pager->Render($row_count);
}

function db_close() {
	global $db;
	
	$db->_close();
}
/*
+--------------------------------------------------------------------
|		END common db functions
+--------------------------------------------------------------------
*/


/*
+--------------------------------------------------------------------
|
|		Default DB Heler functions go here
|
+--------------------------------------------------------------------
*/

function db_error($query='', $errno='', $error='') { 
	
	
	dprint(__FILE__,__LINE__,0,'<font color="#000000"><b>' . $errno . ' - ' . $error . '<br><br>' . $query . '<br><br><small><hr /><font color="#ff0000">[EZW STOP]</font></small><br><br></b></font>');
	
	
}

function db_query_error_log($query) {
	global $db;

	if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
		error_log('QUERY ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
	}

	$result = mysql_query($query, $$link) or db_error($query, mysql_errno(), mysql_error());

	if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
		 $result_error = mysql_error();
		 error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
	}

	return $result;
}

function db_perform($table, $data, $action = 'insert', $parameters = '') {
	reset($data);
	if ($action == 'insert') {
		$query = 'insert into ' . $table . ' (';
		while (list($columns, ) = each($data)) {
			$query .= $columns . ', ';
		}
		$query = substr($query, 0, -2) . ') values (';
		reset($data);
		while (list(, $value) = each($data)) {
			switch ((string)$value) {
				case 'now()':
					$query .= 'now(), ';
					break;
				case 'null':
					$query .= 'null, ';
					break;
				default:
					$query .= '\'' . db_input($value) . '\', ';
					break;
			}
		}
		$query = substr($query, 0, -2) . ')';
	} elseif ($action == 'update') {
		$query = 'update ' . $table . ' set ';
		while (list($columns, $value) = each($data)) {
			switch ((string)$value) {
				case 'now()':
					$query .= $columns . ' = now(), ';
					break;
				case 'null':
					$query .= $columns .= ' = null, ';
					break;
				default:
					$query .= $columns . ' = \'' . db_input($value) . '\', ';
					break;
			}
		}
		$query = substr($query, 0, -2) . ' where ' . $parameters;
	}
//dbg("<br>QUERY=$query<br>");
	return db_query($query);
}

function db_fetch_array1($db_query,$return_type='') {
	global $db;
	$return_type = ($return_type=='')?ADODB_FETCH_ASSOC:$return_type;
	return $db->FetchArray($db_query, $return_type);
}

function db_num_rows($rs) {
	return $rs->RecordCount();}

function db_insert_id() {
	global $db;
	return $db->GetLastInsertedId();
}

function db_output($string) {
	return htmlspecialchars($string);
}

function db_input($string) {
	return addslashes($string);
}

function db_escape_string($string) {
	return stripslashes($string);
}

function db_prepare_input($string) {
	if (is_string($string)) {
		return trim(sanitize_string(stripslashes($string)));
	} elseif (is_array($string)) {
		reset($string);
		while (list($key, $value) = each($string)) {
			$string[$key] = db_prepare_input($value);
		}
		return $string;
	} else {
		return $string;
	}
}

function db_prepare_array($fields,$names,$values) {
	$sql_data = array();
	if(is_array($fields))
	{
		for($i=0;$i<count($fields);$i+=2)
		{
			$val = (isset($values[$names[$i]]))?$values[$names[$i]]:$fields[$i+1];
			switch((string)$names[$i+1])
			{
				case 'date':
					$val = ($val!='') ? date_raw($val) : 'now()' ;
					break;
				case 'datetime':
					$val = ($val!='') ? db_putdate($val) : 'now()' ;
					break;
				case 'mask':
					// Requires security functions to be loaded
					$val = (is_array($val)) ? getMaskFromList( implode(",", $val) ) : $val ;
					break;
				default:
					$val = $val;
					break;
			}
			
			$sql_data[$fields[$i]] = db_prepare_input($val);
		}
	}
	else
	{
		$sql_data[] = db_prepare_input($val);
	}
	dprint(__FILE__, __LINE__, 4, "sql_data(".serialize($sql_data).")");
	return $sql_data;
}

function db_getdate($sdate='',$ts=false,$format='j-M-Y')
{
	if($sdate=='')
	{
		$sdate = time();
		$ts = true;
	}
	if(!$ts) $sdate = strtotime($sdate);
	return date($format,$sdate);
}

function db_putdate($sdate='',$ts=false,$format='Y-m-d H:i:s')
{
	if(!$ts) $sdate = strtotime($sdate);
	return date($format,$sdate);
}

function db_put_ts($date)
{
	$ret = strtotime($date);
	if($ret==-1) return $date;
	else return $ret;
}



function db_get_tables() {
	return db_get_cat_details();
}

function db_get_views() {
	return db_get_cat_details('VIEW');
}

function db_get_columns($sql){
	global $db;
	$rs = $db->Execute($sql);
	$ret = $rs->FetchColumns();
	return $ret;
}

function db_get_cat_details($type = 'TABLE') {
	global $db;
	$ret = array();
	$rs = $db->Execute("select table_name from cat where table_type = '$type'");
	while(!$rs->EOF){
		$ret[]=$rs->Fields("table_name");
		$rs->MoveNext();
	}
	return $ret;
}

/*
+---------------------------------------------------------------------------
|	Returns the parent nodes pagetype
+---------------------------------------------------------------------------
*/
//db_draw_pull_down_menu('type', db_get_pull_down_options("select name, insert(`name`,1,1,substring(upper(name),1,1)) from ".TABLE_form_types." where status > 0 "), $type, (($action=="edit")?"disabled":""));
function db_get_parent_pagetype($pid) {
	$arr=db_get_allowed_pagetype($pid);
	return $arr[0][0];
}
function db_get_pagetype($type){
	return db_getfieldvalue("select name from form_types where id=$type");
}
function db_get_allowed_pagetype($pid) {
	
// global ADOBD object
global $db;

	$ret=array();
	$sql="select nt.name, nt.allowed_child_types as types from form_fields n, form_types nt where n.ftype=nt.id and n.field_id=$pid";
	//echo($sql);
	$rs = $db->Execute($sql);	
	while($row = $rs->FetchRow())
	{
	//print_r($row);
		$ret[]=array($row['NAME'], $row['TYPES']);
	}
	return $ret;
}
function db_get_allowed_pagetype_matrix() {
	$ret=array();
	$result = db_query("select nt.name, nt.allowed_child_types as types from form_types nt where nt.status > 0 ");
	while($row = db_fetch_array($result))
	{
		$ret[]=array($row['NAME'], $row['TYPES']);
	}
	return $ret;
}


// DEBUG FUNCTIONS
function db_dump_results($rs) {
	print "<pre>";
	print_r($rs->GetRows());
	print "</pre>";
}

?>