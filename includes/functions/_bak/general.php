<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - General Functions
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

/*
+---------------------------------------------------------------------------
|	Define date constants
+---------------------------------------------------------------------------
*/
define('DATE_FORMAT_SHORT', '%j-%M-%Y');  // this is used for strftime()
define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
define('DATE_FORMAT', 'j-M-Y'); // this is used for date()
define('DATE_TIME', 'h:i:s A');
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');
define('PHP_DATE_TIME_FORMAT', DATE_FORMAT .' '. DATE_TIME);
define('CURRENCY_SEPERATOR','&nbsp;');
/*
+---------------------------------------------------------------------------
|	Stop from parsing any further PHP code
+---------------------------------------------------------------------------
*/
  function ezw_exit() {
		ezw_session_close();
		exit();
  }

/*
+---------------------------------------------------------------------------
|	Get users ip address
+---------------------------------------------------------------------------
*/
  function ezw_get_ip_address() {
    if (isset($_SERVER)) {
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
      } else {
        $ip = $_SERVER['REMOTE_ADDR'];
      }
    } else {
      if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ip = getenv('HTTP_X_FORWARDED_FOR');
      } elseif (getenv('HTTP_CLIENT_IP')) {
        $ip = getenv('HTTP_CLIENT_IP');
      } else {
        $ip = getenv('REMOTE_ADDR');
      }
    }

    return $ip;
  }
	
	function ezw_set_time_limit($seconds=1440) {
		set_time_limit($seconds);
	}
/*
+---------------------------------------------------------------------------
|	Check if value is null
+---------------------------------------------------------------------------
*/
  function ezw_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if (($value != '') && (strtolower($value) != 'null') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }
/*
+---------------------------------------------------------------------------
|	Convert string to integer - use this as call back function
+---------------------------------------------------------------------------
*/
  function ezw_string_to_int($string) {
    return (int)$string;
  }

/*
+---------------------------------------------------------------------------
|	Return  padded strings with zero's
+---------------------------------------------------------------------------
*/
	function ezw_pad_zero_right($string,$length)
	{
		return str_pad($string,$length,'0',STR_PAD_RIGHT);
	}
	
	function ezw_pad_zero_left($string,$length)
	{
		return str_pad($string,$length,'0',STR_PAD_LEFT);
	}

	function ezw_currency_format($number)
	{
		return number_format($number,2,'.',' ');
	}
	function ezw_display_currency_format($number)
	{
		$ret = ''.number_format($number,2,'.',' ');
		return preg_replace('/\s/', CURRENCY_SEPERATOR, $ret);
	}

/*
+---------------------------------------------------------------------------
|	Redirect to another page or site
+---------------------------------------------------------------------------
*/
  function ezw_redirect($url) {
    if (defined('ENABLE_SSL') && (ENABLE_SSL == true) && (getenv('HTTPS') == 'on') ) { // We are loading an SSL page
      if (substr($url, 0, strlen(HTTP_SERVER)) == HTTP_SERVER) { // NONSSL url
        $url = HTTPS_SERVER . substr($url, strlen(HTTP_SERVER)); // Change it to SSL
      }
    }

    header('Location: ' . $url);

    ezw_exit();
  }

/*
+---------------------------------------------------------------------------
|	Parse the data used in the html tags to ensure the tags will not break
+---------------------------------------------------------------------------
*/
  function ezw_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }

  function ezw_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return ezw_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return ezw_parse_input_field_data($string, $translate);
      }
    }
  }

  function ezw_output_string_protected($string) {
    return ezw_output_string($string, false, true);
  }

  function ezw_sanitize_string($string) {
    $string = ereg_replace(' +', ' ', trim($string));

    return preg_replace("/[<>]/", '_', $string);
  }

/*
+---------------------------------------------------------------------------
|	Return a random row from a database query
+---------------------------------------------------------------------------
*/
  function ezw_random_select($query) {
    $ret = '';
    $random_query = ezw_db_query($query);
    $num_rows = ezw_db_num_rows($random_query);
    if ($num_rows > 0) {
      $random_row = ezw_rand(0, ($num_rows - 1));
      ezw_db_data_seek($random_query, $random_row);
      $ret = ezw_db_fetch_array($random_query);
    }

    return $ret;
  }

/*
+---------------------------------------------------------------------------
|	Returns the clients browser
+---------------------------------------------------------------------------
*/
  function ezw_browser_detect($component) {
    global $HTTP_USER_AGENT;

    return stristr($HTTP_USER_AGENT, $component);
  }

////
// Alias function to ezw_get_countries()
  function ezw_get_country_name($country_id) {
    $country_array = ezw_get_countries($country_id);

    return $country_array['countries_name'];
  }

/*
+---------------------------------------------------------------------------
|	Returns the zone (State/Province) name
| TABLES: zones
+---------------------------------------------------------------------------
*/
  function ezw_get_zone_name($country_id, $zone_id, $default_zone) {
    $zone_query = ezw_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (ezw_db_num_rows($zone_query)) {
      $zone = ezw_db_fetch_array($zone_query);
      return $zone['zone_name'];
    } else {
      return $default_zone;
    }
  }

/*
+---------------------------------------------------------------------------
|	Returns the zone (State/Province) code
| TABLES: zones
+---------------------------------------------------------------------------
*/
  function ezw_get_zone_code($country_id, $zone_id, $default_zone) {
    $zone_query = ezw_db_query("select zone_code from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (ezw_db_num_rows($zone_query)) {
      $zone = ezw_db_fetch_array($zone_query);
      return $zone['zone_code'];
    } else {
      return $default_zone;
    }
  }



/*
+---------------------------------------------------------------------------
|	Left Pad 0 if $number is < 10
+---------------------------------------------------------------------------
*/
  function ezw_row_number_format($number) {
    if ( ($number < 10) && (substr($number, 0, 1) != '0') ) $number = '0' . $number;

    return $number;
  }

/*
+---------------------------------------------------------------------------
|	Send email (text/html) using MIME
| This is the central mail function. The SMTP Server should be configured
| correct in php.ini
| Parameters:
| $to_name           The name of the recipient, e.g. "Jan Wildeboer"
| $to_email_address  The eMail address of the recipient,
|                    e.g. jan.wildeboer@gmx.de
| $email_subject     The subject of the eMail
| $email_text        The text of the eMail, may contain HTML entities
| $from_email_name   The name of the sender, e.g. Shop Administration
| $from_email_adress The eMail address of the sender,
|                    e.g. info@mytepshop.com
+---------------------------------------------------------------------------
*/
  function ezw_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address, $format = 'html') {
    if (SEND_EMAILS != 'true') return false;

    // Instantiate a new mail object
    $message = new email(array('X-Mailer: ezwebmaker Mailer'));

    // Build the text version
    $text = strip_tags($email_text);
    //if (EMAIL_USE_HTML == 'true') {
		if($format == 'html'){
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

/*
+---------------------------------------------------------------------------
|	Return a random value
+---------------------------------------------------------------------------
*/
  function ezw_rand($min = null, $max = null) {
    static $seeded;

    if (!isset($seeded)) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }
	

/*
+---------------------------------------------------------------------------
|	Get the number of times a word/character is present in a string
+---------------------------------------------------------------------------
*/	
  function ezw_word_count($string, $needle) {
    $temp_array = split($needle, $string);

    return sizeof($temp_array);
  }

  function ezw_create_random_value($length, $type = 'mixed') {
    if ( ($type != 'mixed') && ($type != 'chars') && ($type != 'digits')) return false;

    $rand_value = '';
    while (strlen($rand_value) < $length) {
      if ($type == 'digits') {
        $char = ezw_rand(0,9);
      } else {
        $char = chr(ezw_rand(0,255));
      }
      if ($type == 'mixed') {
        if (eregi('^[a-z0-9]$', $char)) $rand_value .= $char;
      } elseif ($type == 'chars') {
        if (eregi('^[a-z]$', $char)) $rand_value .= $char;
      } elseif ($type == 'digits') {
        if (ereg('^[0-9]$', $char)) $rand_value .= $char;
      }
    }

    return $rand_value;
  }
	

/*
+---------------------------------------------------------------------------
|	Wrapper function for round()
|		rounds a number to the nearest whole number
| $number=digit
| $precision=decimal length
| Example:
|		ezw_round(5.4,2); should return 5.00
+---------------------------------------------------------------------------
*/	
  function ezw_round($number, $precision) {
    if (strpos($number, '.') && (strlen(substr($number, strpos($number, '.')+1)) > $precision)) {
      $number = substr($number, 0, strpos($number, '.') + 1 + $precision + 1);

      if (substr($number, -1) >= 5) {
        if ($precision > 1) {
          $number = substr($number, 0, -1) + ('0.' . str_repeat(0, $precision-1) . '1');
        } elseif ($precision == 1) {
          $number = substr($number, 0, -1) + 0.1;
        } else {
          $number = substr($number, 0, -1) + 1;
        }
      } else {
        $number = substr($number, 0, -1);
      }
    }

    return $number;
  }
	
/*
+---------------------------------------------------------------------------
|	Convert an array to string seperated by ($equals) and delimited by ($seperator)
+---------------------------------------------------------------------------
*/	
  function ezw_array_to_string($array, $exclude = '', $equals = '=', $separator = '&') {
    if (!is_array($exclude)) $exclude = array();

    $get_string = '';
    if (sizeof($array) > 0) {
      while (list($key, $value) = each($array)) {
        if ( (!in_array($key, $exclude)) && ($key != 'x') && ($key != 'y') ) {
          $get_string .= $key . $equals . $value . $separator;
        }
      }
      $remove_chars = strlen($separator);
      $get_string = substr($get_string, 0, -$remove_chars);
    }

    return $get_string;
  }

/*
+---------------------------------------------------------------------------
|	Parse and secure the cPath parameter values
+---------------------------------------------------------------------------
*/
  function ezw_parse_delimited_path($cPath, $delim = '_') {
// make sure the category IDs are integers
    $cPath_array = array_map('ezw_string_to_int', explode($delim, $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i=0; $i<$n; $i++) {
      if (!in_array($cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $cPath_array[$i];
      }
    }

    return $tmp_array;
  }

  function ezw_setcookie($name, $value = '', $expire = 0, $path = '/', $domain = '', $secure = 0) {
    setcookie($name, $value, $expire, $path, (ezw_not_null($domain) ? $domain : ''), $secure);
  }

/*
+---------------------------------------------------------------------------
|	nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
+---------------------------------------------------------------------------
*/
  function ezw_convert_linefeeds($from, $to, $string) {
    if ((PHP_VERSION < "4.0.5") && is_array($from)) {
      return ereg_replace('(' . implode('|', $from) . ')', $to, $string);
    } else {
      return str_replace($from, $to, $string);
    }
  }

/*
+---------------------------------------------------------------------------
|	Return date in raw format
|	$sdate should be in format dd-mmm-yyyy
|	raw date is in format YYYYMMDD, or DDMMYYYY
+---------------------------------------------------------------------------
*/
	function ezw_date_raw($sdate, $reverse = false) {
		$adate = explode('-',$sdate);
		$months = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
		if ($reverse) {
			return ezw_pad_zero_left($adate[0],2) . ezw_pad_zero_left((string)array_search($adate[1],$months),2) . $adate[2];
		} else {
			return $adate[2] . ezw_pad_zero_left(array_search($adate[1],$months),2) . ezw_pad_zero_left($adate[0],2);
		}
	}
/*
+---------------------------------------------------------------------------
|	Return datetime in raw format
|	$sdate should be in format dd-mmm-yyyy
|	raw date is in format YYYYMMDDHHMMSS, or DDMMYYYYHHMMSS
+---------------------------------------------------------------------------
*/	
	function ezw_datetime_raw($sdate, $reverse = false) {
		if(is_array($sdate)){
			$stime=$sdate[1];
			$tmp=$sdate[0];
			$sdate=$tmp;
		}else{
			$stime = '00:00';
		}
		
		$adate = explode('-',trim($sdate));
		$atime = explode(':',trim($stime));
		$months = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
		if ($reverse) {
			return ezw_pad_zero_left($adate[0],2) . ezw_pad_zero_left((string)array_search($adate[1],$months),2) . $adate[2];
		} else {
			return $adate[2] . ezw_pad_zero_left(array_search($adate[1],$months),2) . ezw_pad_zero_left($adate[0],2) . ezw_pad_zero_left($atime[0],2) . ezw_pad_zero_left($atime[1],2);
		}
	}
/*
+---------------------------------------------------------------------------
|	Output a raw date string in the selected locale date format
|	$raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
+---------------------------------------------------------------------------
*/  
	function ezw_date_long($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = (int)substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    return strftime(DATE_FORMAT_LONG, mktime($hour,$minute,$second,$month,$day,$year));
  }

/*
+---------------------------------------------------------------------------
|	Output a raw date string in the selected locale date format
|	$raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
|	NOTE: Includes a workaround for dates before 01/01/1970 that fail on windows servers
+---------------------------------------------------------------------------
*/  
	function ezw_date_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || empty($raw_date) ) return false;

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
      return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
      return ereg_replace('2037' . '$', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }
  }

/*
+---------------------------------------------------------------------------
|	Check date
+---------------------------------------------------------------------------
*/
  function ezw_checkdate($date_to_check, $format_string, &$date_array) {
    $separator_idx = -1;

    $separators = array('-', ' ', '/', '.');
    $month_abbr = array('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
    $no_of_days = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

    $format_string = strtolower($format_string);

    if (strlen($date_to_check) != strlen($format_string)) {
      return false;
    }

    $size = sizeof($separators);
    for ($i=0; $i<$size; $i++) {
      $pos_separator = strpos($date_to_check, $separators[$i]);
      if ($pos_separator != false) {
        $date_separator_idx = $i;
        break;
      }
    }

    for ($i=0; $i<$size; $i++) {
      $pos_separator = strpos($format_string, $separators[$i]);
      if ($pos_separator != false) {
        $format_separator_idx = $i;
        break;
      }
    }

    if ($date_separator_idx != $format_separator_idx) {
      return false;
    }

    if ($date_separator_idx != -1) {
      $format_string_array = explode( $separators[$date_separator_idx], $format_string );
      if (sizeof($format_string_array) != 3) {
        return false;
      }

      $date_to_check_array = explode( $separators[$date_separator_idx], $date_to_check );
      if (sizeof($date_to_check_array) != 3) {
        return false;
      }

      $size = sizeof($format_string_array);
      for ($i=0; $i<$size; $i++) {
        if ($format_string_array[$i] == 'mm' || $format_string_array[$i] == 'mmm') $month = $date_to_check_array[$i];
        if ($format_string_array[$i] == 'dd') $day = $date_to_check_array[$i];
        if ( ($format_string_array[$i] == 'yyyy') || ($format_string_array[$i] == 'aaaa') ) $year = $date_to_check_array[$i];
      }
    } else {
      if (strlen($format_string) == 8 || strlen($format_string) == 9) {
        $pos_month = strpos($format_string, 'mmm');
        if ($pos_month != false) {
          $month = substr( $date_to_check, $pos_month, 3 );
          $size = sizeof($month_abbr);
          for ($i=0; $i<$size; $i++) {
            if ($month == $month_abbr[$i]) {
              $month = $i;
              break;
            }
          }
        } else {
          $month = substr($date_to_check, strpos($format_string, 'mm'), 2);
        }
      } else {
        return false;
      }

      $day = substr($date_to_check, strpos($format_string, 'dd'), 2);
      $year = substr($date_to_check, strpos($format_string, 'yyyy'), 4);
    }

    if (strlen($year) != 4) {
      return false;
    }

    if (!settype($year, 'integer') || !settype($month, 'integer') || !settype($day, 'integer')) {
      return false;
    }

    if ($month > 12 || $month < 1) {
      return false;
    }

    if ($day < 1) {
      return false;
    }

    if (ezw_is_leap_year($year)) {
      $no_of_days[1] = 29;
    }

    if ($day > $no_of_days[$month - 1]) {
      return false;
    }

    $date_array = array($year, $month, $day);

    return true;
  }


  function ezw_is_leap_year($year) {
    if ($year % 100 == 0) {
      if ($year % 400 == 0) return true;
    } else {
      if (($year % 4) == 0) return true;
    }

    return false;
  }
	
	
	function ezw_is_ip_within_range($ip)
	{
		$filter_ip = array('127.0.0.1','220.235.121.149','202.72.146.233','218.214.14.4');
		return (!in_array($ip,$filter_ip));
	}
/*
+----------------------------------------------------------------------------
|	$$$  DEGUG  $$$  DEGUG  $$$  DEGUG  $$$  DEGUG  $$$  DEGUG  $$$  DEGUG  $$$
+----------------------------------------------------------------------------
*/	
	function ezw_dbg($s,$override=true){
		//if($override==true||ADMINISTRATION_DGB_ON===true){
			if(is_array($s) || is_object($s)) print_r($s);
			else echo("<b>#>></b>".$s."<br />");
		//}
	}
?>
