<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - mySQL database functions
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
		
	function db_getfieldvalue($sql, $field=0){
		$result = db_query($sql);
		if(is_array($field))
		{
			$ret = array();
			$rec = db_fetch_array($result);
			for($i=0;$i<count($field);$i++)
			{
				$ret[$field[$i]] = $rec[$field[$i]];
			}
			return $ret;
		}
		else
		{
			$data = db_fetch_array($result,MYSQL_BOTH);
			return $data[$field];
		}
	}
	
	/*
	+--------------------------------------------------------------------
	|
	|		Default Database functions go here
	|
	+--------------------------------------------------------------------
	*/

  function db_error($query='', $errno='', $error='') { 
    die('<font color="#000000"><b>' . $errno . ' - ' . $error . '<br><br>' . $query . '<br><br><small><hr /><font color="#ff0000">[EZW STOP]</font></small><br><br></b></font>');
  }

  function db_query($query) {
    global $db;

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
      error_log('QUERY ' . $query . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
    }

    $result = mysql_query($query, $$link) or db_error($query, mysql_errno(), mysql_error());

    if (defined('STORE_DB_TRANSACTIONS') && (STORE_DB_TRANSACTIONS == 'true')) {
       $result_error = mysql_error();
       error_log('RESULT ' . $result . ' ' . $result_error . "\n", 3, STORE_PAGE_PARSE_TIME_LOG);
    }

    return $result;
  }

  function db_perform($table, $data, $action = 'insert', $parameters = '', $link = 'db_link') {
    reset($data);
    if ($action == 'insert') {
      $query = 'insert into ' . $table . ' (';
      while (list($columns, ) = each($data)) {
        $query .= $columns . ', ';
      }
      $query = substr($query, 0, -2) . ') values (';
      reset($data);
      while (list(, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= 'now(), ';
            break;
          case 'null':
            $query .= 'null, ';
            break;
          default:
            $query .= '\'' . db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ')';
    } elseif ($action == 'update') {
      $query = 'update ' . $table . ' set ';
      while (list($columns, $value) = each($data)) {
        switch ((string)$value) {
          case 'now()':
            $query .= $columns . ' = now(), ';
            break;
          case 'null':
            $query .= $columns .= ' = null, ';
            break;
          default:
            $query .= $columns . ' = \'' . db_input($value) . '\', ';
            break;
        }
      }
      $query = substr($query, 0, -2) . ' where ' . $parameters;
    }
//dbg("<br>QUERY=$query<br>");
    return db_query($query, $link);
  }

  function db_fetch_array($db_query,$return_type='') {
		$return_type = ($return_type=='')?MYSQL_ASSOC:$return_type;
    return $db->FetchArray($db_query, $return_type);
  }

  function db_num_rows($db_query) {
    return mysql_num_rows($db_query);
  }

  function db_insert_id() {
    return 0;
  }

  function db_output($string) {
    return htmlspecialchars($string);
  }

  function db_input($string) {
    return addslashes($string);
  }

	function db_escape_string($string) {
		return mysql_escape_string($string);
	}

  function db_prepare_input($string) {
    if (is_string($string)) {
      return trim(sanitize_string(stripslashes($string)));
    } elseif (is_array($string)) {
      reset($string);
      while (list($key, $value) = each($string)) {
        $string[$key] = db_prepare_input($value);
      }
      return $string;
    } else {
      return $string;
    }
  }
	
	function db_prepare_array($fields,$names,$values) {
		$sql_data = array();
		if(is_array($fields))
		{
			for($i=0;$i<count($fields);$i+=2)
			{
	//dbg($values[$i]."=".$POST[$values[$i]]."<br>");
				$val = (isset($values[$names[$i]]))?$values[$names[$i]]:$fields[$i+1];
				switch((string)$names[$i+1])
				{
					case 'date':
						$val = ($val!='') ? date_raw($val) : 'now()' ;
						break;
					case 'datetime':
						$val = ($val!='') ? db_putdate($val) : 'now()' ;
						break;
					case 'mask':
						// Requires security functions to be loaded
						$val = (is_array($val)) ? getMaskFromList( implode(",", $val) ) : $val ;
						break;
					default:
						$val = $val;
						break;
				}
				
				$sql_data[$fields[$i]] = db_prepare_input($val);
			}
		}
		else
		{
			$sql_data[] = db_prepare_input($val);
		}
	dbg("<br />SQL_DATA=");
	dbg($sql_data);
	dbg("<br />");
	//dbg($_POST);
		return $sql_data;
	}
	
	function db_getdate($sdate='',$ts=false,$format='j-M-Y')
	{
		if($sdate=='')
		{
			$sdate = time();
			$ts = true;
		}
		if(!$ts) $sdate = strtotime($sdate);
		return date($format,$sdate);
	}
	
	function db_putdate($sdate='',$ts=false,$format='Y-m-d H:i:s')
	{
		if(!$ts) $sdate = strtotime($sdate);
		return date($format,$sdate);
	}
	
	function db_put_ts($date)
	{
		$ret = strtotime($date);
		if($ret==-1) return $date;
		else return $ret;
	}
?>
