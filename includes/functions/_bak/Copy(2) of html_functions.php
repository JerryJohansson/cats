<?php
/*
+--------------------------------------------------------------------------
|   HTML Functions and Output helpers
|   ========================================
|   by Vernon Laskey
|   (c) 2005 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to build html components
+---------------------------------------------------------------------------
*/

	/*
	+----------------------------------------
	| Get groups select list
	+----------------------------------------
	*/
	function html_user_groups_list($groups=0,$user_id=0)
	{
		if(!is_array($_SESSION['user_details'])) return false;
		// get groups allowed
		$value_id='MASK';
		$text_id='NAME';
		//print_r($_SESSION['user_details']);
		$user_groups = $_SESSION['user_details']['groups'];
		if(empty($user_groups)) $user_groups = 0;
		$sql = "select user_group_mask as mask, user_group_name as name from user_group where (bitand(user_group_mask, ".$user_groups.")>0 and bitand(status, ".MASK_HIDDEN.")>0 or (status = 0)) order by user_group_mask desc";
		//echo($sql);
		$result = db_query($sql) or die(db_error());
		$ret = '<select class="sfield" name="groups[]" id="id_user_group" multiple size="4" style="width:200px;" cumask='.$user_groups.' gmask='.$groups.'>';
		while ($row = db_fetch_array($result))
		{
			$selected = ((int)$row[$value_id] & (int)$groups)?"selected":"";
			$ret .= '<option value="'.$row[$value_id].'" '.$selected.'>'.$row[$text_id].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get roles select list
	+----------------------------------------
	*/
	function html_user_roles_list($roles=0,$user_id=0)
	{
		if(!is_array($_SESSION['user_details'])) return false;
		// get groups allowed
		$value_id='MASK';
		$text_id='NAME';
//print_r($_SESSION['user_details']);

		$user_roles = $_SESSION['user_details']['roles'];
		if(empty($user_roles)) $user_roles = 0;
		$sql = "select user_role_mask as mask, user_role_name as name from user_role where (bitand(user_role_mask, ".$user_roles.")>0 and bitand(status, ".MASK_HIDDEN.")>0) or (status = 0)";
		//echo($sql);
		$result = db_query($sql) or die(db_error());
		$ret = '<select class="sfield" name="roles[]" id="id_user_role" multiple size="4" style="width:200px;" cumask='.$user_roles.' gmask='.$roles.'>';
		while ($row = db_fetch_array($result))
		{
			$selected = ((int)$row[$value_id] & (int)$roles)?"selected":"";
			$ret .= '<option value="'.$row[$value_id].'" '.$selected.'>'.$row[$text_id].'</option>';
		}
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	/*
	+----------------------------------------
	| Get roles select dropdown
	+----------------------------------------
	*/
	function html_user_status_dd($status=0)
	{
		// get groups allowed
		$ret = '<select class="sfield" name="status" id="id_status" style="width:200px;" us="'.$_SESSION['user_details']['status'].'">';
		$user_id = $this->getId(); //$_SESSION['user_details']['user_id'];
		$user_status = $_SESSION['user_details']['status'];
		$ret .= '<option value="0" '.(($user_status==0)?"selected":"").'>Active</option>';
		$ret .= '<option value="2" '.(($user_status==2)?"selected":"").'>Inactive</option>';
		if($user_status & 4)
			$ret .= '<option value="4" '.(($user_status==4)?"selected":"").'>System</option>';
		$ret .= '</select>';
		//---------------
		return ($ret);
	}
	
	function html_file_links_list($files_in='',$title='Download Files',$title_view='View Files')
	{
		$files = '';
		$files_view = '';
		
		if($files_in!='')
		{
			if(is_array($files_in)) $filenames = $files_in;
			else $filenames = explode("|",$files_in);
			
			foreach($filenames as $value)
			{
				if(file_exists(CATS_FILES_PATH.$value))
				{
					$display_name=preg_replace("/_/i"," ",$value);
					if($mode=='admin')
					{
						//$files .= '<input type="checkbox" name="delfile[]" value="'.$value.'" />&nbsp;<a href="'.WS_FILES_PATH.$value.'" target="_blank">'.$value.'</a><br />';
						$files .= '<a href="'.WS_FILES_PATH.$value.'" target="_blank">'.$value.'</a><br />';
					}
					else
					{
						$files .= '<a href="download.php?dlf='.$value.'">'.$display_name.'</a>';
						if(preg_match("/htm|jpg|gif|png|doc|xls|csv|txt|ppp/i", substr($value,strpos($value,'.'))))
							$files_view .= '<a href="'.WS_FILES_PATH.$value.'">'.$display_name.'</a>';
					}
				}
			}
			if($files != '')
			{
				if($mode!='admin')
				{
					$files = '<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<th height="30" align="left"><span id="features_heading">'.$title.'</span></th>
		</tr>
		<tr>
			<td>
				<div id="features">'.$files.'</div>
			</td>
		</tr>
		';
		if($files_view!=""){
					$files .= '
		<tr>
			<td>
				<p>&nbsp;</p>
			</td>
		</tr>
		<tr>
			<th height="30" align="left"><span id="features_heading">'.$title_view.'</span></th>
		</tr>
		<tr>
			<td>
				<div id="features">'.$files_view.'</div>
			</td>
		</tr>
		';
		}
					$files .= '
		<tr>
			<td height="30">&nbsp;</td>
		</tr>
		</table>';
		//			<table id="related_files"><tr valign="top"><td>:</td><td><div id="sectionLinks"></div></td><td>'.$title_view.':</td><td><div id="sectionLinks">'.$files_view.'</div></td></tr></table>';
				}
			}
		}
		return $files;
	}

/*
+---------------------------------------------------------------------------
|	Functions to display form items etc
+---------------------------------------------------------------------------
*/
	/*
	+---------------------------------------------------------------------------
	|	Output a form
	+---------------------------------------------------------------------------
	*/
  function html_draw_form($name, $action, $method = 'post', $parameters = '') {
    $form = '<form name="' . main_output_string($name) . '" action="' . main_output_string($action) . '" method="' . main_output_string($method) . '"';

    if (main_not_null($parameters)) $form .= ' ' . $parameters;

    $form .= '>';

    return $form;
  }
/*
+---------------------------------------------------------------------------
|	Output a form label
+---------------------------------------------------------------------------
*/
  function html_draw_label($name, $value = '', $parameters = '') {
    return '<label style="display:inline;text-align:auto;color:auto;font-weight:normal;background:auto;" for="id_' . main_output_string($name) . '" ' . $parameters . '>' . main_output_string($value) . '</label>';
  }
	
	function html_form_label($name, $value = '', $parameters = '') {
    return '<label for="id_' . main_output_string($name) . '" ' . $parameters . '>' . main_output_string($value) . '</label>';
  }
	
	function html_display_value($name, $value, $parameters = ''){
		return '<span id="'.$name.'" '.$parameters.'>'.$value.'</span>';
	}
/*
+---------------------------------------------------------------------------
|	Output a form filefield
+---------------------------------------------------------------------------
*/
  function html_draw_file_field($name, $required = false) {
    return html_draw_input_field($name, '', '', 'file', $required);
  }
/*
+---------------------------------------------------------------------------
|	Output a form input field
+---------------------------------------------------------------------------
*/
  function html_draw_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '"';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }
	function html_draw_disabled_field($name, $value = '', $parameters = '') {
		return html_draw_input_field($name, $value, ' disabled="true" ');
	}
	
	function html_draw_lge_input_field($name, $value = '', $parameters = '', $type = 'text', $reinsert_value = true) {
    $field = '<input class="sfield_lge" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '"';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    } elseif (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }
	

/*
+---------------------------------------------------------------------------
|	Output a form password field
+---------------------------------------------------------------------------
*/
  function html_draw_password_field($name, $value = '', $parameters = 'maxlength="40"') {
    return html_draw_input_field($name, $value, $parameters, 'password', false);
  }

/*
+---------------------------------------------------------------------------
|	Output a selection field - alias function for html_draw_checkbox_field() and html_draw_radio_field()
+---------------------------------------------------------------------------
*/
  function html_draw_selection_field($name, $type, $value = '', $checked = false, $parameters = '') {
    $selection = '<input class="bfield" type="' . main_output_string($type) . '" name="' . main_output_string($name) . '"';

    if (main_not_null($value)) $selection .= ' value="' . main_output_string($value) . '"';

    if ( ($checked == true) || ( isset($_POST[$name]) && is_string($_POST[$name]) && ( ($_POST[$name] == 'on') || (isset($value) && (stripslashes($_POST[$name]) == $value)) ) ) ) {
      $selection .= ' CHECKED';
    }

    if (main_not_null($parameters)) $selection .= ' ' . $parameters;

    $selection .= '>';

    return $selection;
  }

/*
+---------------------------------------------------------------------------
|	Output a form checkbox field
+---------------------------------------------------------------------------
*/
  function html_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
    return html_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }
	
	function html_form_draw_checkbox_field($name, $value = '', $checked = false, $parameters = '') {
		if(!main_not_null($value)) $value='1';
    return html_draw_selection_field($name, 'checkbox', $value, $checked, $parameters);
  }

/*
+---------------------------------------------------------------------------
|	Output a form radio field
+---------------------------------------------------------------------------
*/
  function html_draw_radio_field($name, $value = '', $checked = false, $parameters = '') {
    return html_draw_selection_field($name, 'radio', $value, $checked, $parameters);
  }
	
	function html_draw_yes_no_radioset($name, $value = 'No'){
		if(empty($value)) $value = 'No';
		$s = html_draw_radio_field($name, 'Yes', 'Yes'==$value, ' id="id_'.$name.'[0]" ') . html_draw_label("id_".$name.'[0]', 'Yes');
		$s .= html_draw_radio_field($name, 'No', 'No'==$value, ' id="id_'.$name.'[1]" ') . html_draw_label("id_".$name.'[1]', 'No');
		return $s;
	}
	
	function html_draw_yes_no_number_radioset($name, $value = '0'){
		if(empty($value)) $value = '0';
		$s = html_draw_radio_field($name, '1', '1'==$value, ' id="id_'.$name.'[0]" ') . html_draw_label("id_".$name.'[0]', 'Yes');
		$s .= html_draw_radio_field($name, '0', '0'==$value, ' id="id_'.$name.'[1]" ') . html_draw_label("id_".$name.'[1]', 'No');
		return $s;
	}
	
	function html_draw_action_category_checkboxset($name, $value = '', $id=0){
		$s="";
		if($id==0){
			if(isset($_GET['id'])) $id = $_GET['id'];
		}
		if($id>0){
			$sql = "SELECT B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment, count(A.ActionId) checked FROM tblIncidentForm_Checkboxes B, tblaction_checkboxvalues A WHERE A.ActionId(+) = $id AND A.CheckboxType(+) = 'ActionCategory' AND B.Checkbox_Id = A.CheckboxId(+) AND B.Checkbox_Type = 'Incident_Category' GROUP BY B.Checkbox_Id, B.Checkbox_Desc, B.Checkbox_Comment ORDER BY B.Checkbox_Id";
		}else{
			$sql = "SELECT DISTINCT Checkbox_Desc, Checkbox_Id, Checkbox_Comment, 0 as checked  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Id ASC";
		}
		$rs = db_query($sql);
		while($arr = $rs->FetchRow()){
			$n = $name.'[]';// . '['.$arr['CHECKBOX_ID'].']';
			$n_id=$name . '['.$arr['CHECKBOX_ID'].']';
			$s .= html_draw_checkbox_field($n, $arr['CHECKBOX_ID'], ($arr['CHECKED']==1), ' id="id_'.$n_id.'"') . html_draw_label($n_id, $arr['CHECKBOX_DESC']);
			//<input class="inputTextBox" type="checkbox" <%=((arCategoryResult[1] == 1)?"CHECKED":"")%> name="ActionCategory_2" value="<%=arCategoryValue[1]%>">Safety / Health 
		}
		return $s;
	}
	function html_draw_site_linked($name, $value='', $parameters = '', $department='', $section=''){
		$attribs = ' onchange="remote.get_options_add(\'get_site_details_array\',\'user\',this';
		if($department!=''){
			$attribs .= ",this.form.$department";
		}
		if($section!=''){
			$attribs .= ",this.form.$section";
		}
		$attribs .= ');" '.$parameters;
		
		$ret = html_form_draw_site($name, $value, $attribs);
		return $ret;
	}
	function html_form_draw_site($name, $value, $parameters = ''){
		if($value>0){
			$description = db_get_field_value("select site_description from tblsite where site_id = $value","site_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, ' disabled=true ').html_draw_hidden_field($name, $value, $parameters);
		return $ret;
	}
	
	function html_form_draw_department($name, $dept_id, $parameters=''){
		if($dept_id>0){
			$sql = "select department_description from tbldepartment where department_id = $dept_id";// and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"department_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' disabled=true ').html_draw_hidden_field($name, $value);
		return $ret;
	}
	function html_form_draw_section($name, $section_id, $parameters=''){
		if($section_id>0){// && $dept_id>0 && $site_id>0){
			$sql = "select section_description from tblsection where section_id=$section_id";//department_id = $dept_id and site_id = $site_id";
			//echo($sql);
			$description = db_get_field_value($sql,"section_description");
		}else{
			$description = "N/A";
		}
		$ret = html_draw_input_field($name."_text", $description, $parameters . ' disabled=true ').html_draw_hidden_field($name, $value);
		return $ret;
	}
	
	function html_get_action_status_radio_fields($name, $value='Open') {
		return html_get_status_radioset($name,$value,'TBLACTION_DETAILS');
	}
	function html_get_status_radioset($name,$value='',$type='',$parameters=''){
		global $db;
		$sql="select * from status_values where status_type='$type'";
		$rs=$db->Execute($sql);
		$v=$rs->GetAll();
		//$v=$db_get_array();
		//print_r($v);
		$len=count($v);
		$s='';
		if(empty($value)){
			$value='Open';
		}
		foreach($v as $i=>$row){
			$lbl=$row['STATUS_TITLE'];
			/*if($lbl=='Open'){
				$params=' onclick="_close_action();" '
			}*/
			$s.=html_draw_radio_field($name, $lbl, $lbl==$value, ' id="id_'.$name.'['.$i.']"  onclick="_close_action();" ') . html_draw_label($name.'['.$i.']', $lbl);
		}
		return $s;
	}

/*
+---------------------------------------------------------------------------
|	Output a form textarea field
+---------------------------------------------------------------------------
*/
  function html_draw_textarea_field($name, $text = '', $parameters = '', $wrap=true, $width=100, $height=4, $reinsert_value = true) {
    $field = '<textarea class="sfield" name="' . main_output_string($name) . '" ';
		if($wrap) $field.=' wrap="' . main_output_string($wrap) . '" ';
		if($width) $field.=' cols="' . main_output_string($width) . '" ';
		if($height) $field.=' rows="' . main_output_string($height) . '"';

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $field .= stripslashes($_POST[$name]);
    } elseif (main_not_null($text)) {
      $field .= $text;
    }

    $field .= '</textarea>';

    return $field;
  }

/*
+---------------------------------------------------------------------------
|	Output a form hidden field
+---------------------------------------------------------------------------
*/
	function html_draw_employee_helper_linked($name, $value, $parameters = '', $linked = ''){
		$attribs = ' onclick="remote.get_options_add(\'get_site_details_array\',\'user\',this';
		if($linked!=''){
			$attribs .= ",this.form.$linked";
		}
		$attribs .= ');" ';
		$attribs = "'SITE_ID','SITE_ID_text','DEPARTMENT_ID','DEPARTMENT_ID_text','SECTION_ID','SECTION_ID_text'";
		return html_draw_employee_helper($name, $value, $parameters, $attribs);
	}
	
	function html_draw_employee_helper($name, $value, $parameters = '', $xtra_parameters = ''){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'');
		}
		$display_value = $val['TEXT'];
		$real_value = $val['ID'];
		if($xtra_parameters!=''){
			$xtra_parameters = ','.$xtra_parameters;
		}
		return '
	<input type="hidden" name="'.$name.'" id="id_'.$name.'" value="'.$real_value.'" />
	<input type="text" style="width:200px;" name="'.$name.'_text" id="id_'.$name.'_text" value="'.$display_value.'" '.$parameters.'
			onfocus="this.className=\'active\';this.select();" 
			onblur="this.className=\'\';" 
	 />
	<img type="helper" src="'.WS_ICONS_PATH.'btn_get_employee_array_bg.gif" onclick="_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$name.'_text\''.$xtra_parameters.');" style="cursor:pointer;" align="absmiddle" id="I_'.$name.'" />
';
	}
	
	function html_draw_employee_helper_search($name, $value, $parameters = '', $xtra_parameters = '', $reinsert_value = true){
		if($value){
			$sql = "select last_name||', '||first_name as TEXT, employee_number as ID from tblemployee_details where employee_number = $value ";
			$val = db_get_array($sql);
		}else{
			$val = array('TEXT'=>'','ID'=>'0');
		}
		$display_value = $val['TEXT'];
		$real_value = $val['ID'];
		// re-insert posted value for id element
		if ( (isset($_POST[$name])) && ($reinsert_value == true) ) {
      $real_value = main_output_string(stripslashes($_POST[$name]));
    } elseif (main_not_null($real_value)) {
      $real_value = main_output_string($real_value);
    }
		// re-insert posted value for text element
		if ( (isset($_POST[$name.'_text'])) && ($reinsert_value == true) ) {
      $display_value = main_output_string(stripslashes($_POST[$name.'_text']));
    } elseif (main_not_null($display_value)) {
      $display_value = main_output_string($display_value);
    }
		return '
	<input type="hidden" name="'.$name.'" id="id_'.$name.'" value="'.$real_value.'" />
	<input type="text" style="width:200px;" name="'.$name.'_text" id="id_'.$name.'_text" value="'.$display_value.'" '.$xtra_parameters.'
			onfocus="this.className=\'active\';this.select();" 
			onblur="this.className=\'\';" 
	 />
	<img type="helper" src="'.WS_ICONS_PATH.'btn_get_employee_array_bg.gif" onclick="_helper_show_employee(\'get_employee_list\',\'users\',\''.$name.'\', \''.$name.'_text\');" style="cursor:pointer;" align="absmiddle" id="I_'.$name.'" />
';
	}
	

	function html_form_hidden_display_value($name, $value, $parameters = ''){
		if(empty($value)){
			$display_value = (isset($_POST[$name]))?$_POST[$name]:'';
		}else{
			$display_value = $value;
		}
		return '<span>'.$display_value.'</span>'.html_draw_hidden_field($name, $value);
	}
	function html_form_show_hidden($name, $value, $parameters = ''){
		return html_draw_input_field($name."_DISABLED", $value, $parameters . ' disabled=true ').html_draw_hidden_field($name, $value);
	}
	function html_form_hidden($name, $value, $parameters = ''){
		return html_draw_input_field($name, $value, $parameters . ' disabled=true ');
	}
	
  function html_draw_hidden_field($name, $value = '', $parameters = '') {
    $field = '<input type="hidden" name="' . main_output_string($name) . '"';

    if (main_not_null($value)) {
      $field .= ' value="' . main_output_string($value) . '"';
    } elseif (isset($_POST[$name])) {
      $field .= ' value="' . main_output_string(stripslashes($_POST[$name])) . '"';
    }

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    return $field;
  }

	
/*
+---------------------------------------------------------------------------
|	Output a form pull down menu
+---------------------------------------------------------------------------
*/
	function html_group_type_dd($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_group_type_options($value), $value, $parameters, false, true);
	}

	// DAVID: Shift Function
	function html_form_draw_reftable_shift_dd($name, $value, $parameters = ''){
		$where = get_user_site_access_filter('single');
		$sql = "SELECT RefTable_Id AS ID, RefTable_Description AS TEXT  FROM tblRefTable WHERE RefTable_Type = 'Shift' $where ORDER BY RefTable_Description";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_inspection_type_dd($name, $value, $parameters = ''){
		$where = get_user_site_access_filter();
		$sql = "SELECT DISTINCT RefTable_Description as TEXT  FROM tblRefTable  WHERE RefTable_Type = 'Requirement Type' $where ORDER BY RefTable_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function get_user_site_access_filter($type = ''){
		$site_id = $_SESSION['user_details']['site_id'];
		$site_access = $_SESSION['user_details']['site_access'];
		$multi_site_access = false;
		switch($type){
			case 'single':
				$multi_site_access = false;
				break;
			case 'multi':
				$multi_site_access = (bool)!empty($site_access);
				break;
			default:
				$multi_site_access = (bool)!empty($site_access);
				break;
		}
		if(!empty($type)){
			
		}
		if($multi_site_access){
			if(strpos($multi_sites,"SITE_ID")===false)
				$where = " SITE_ID IN(" . $site_access . ") ";
			else
				$where = " " . $site_access . " ";
		}else{
			$where = " SITE_ID = " . $site_id . " ";
		}
		return " AND $where";
	}

	function html_form_draw_group_name_dd($name, $value, $parameters = ''){
		$sql = "SELECT Group_Description as TEXT FROM tblAccess_Groups_Defn ORDER BY Group_Description ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_company_dd($name, $value, $parameters = ''){
		$sql = "SELECT CompanyId as ID, CompanyName as TEXT FROM tblCompanies ORDER BY CompanyName ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_government_departments_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Govt_Department as TEXT FROM tblGovernment_Details  ORDER BY UPPER(Govt_Department) ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_government_locations_dd($name, $value, $parameters = ''){
		$sql = "SELECT DISTINCT Location as TEXT  FROM tblGovernment_Details  ORDER BY UPPER(Location) ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'TEXT', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_employee_type_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"Contrator", "text" => "Contractor"),
			array("id"=>"Employee", "text" => "Employee")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	function html_form_draw_site_dd($name, $value, $parameters = ''){
		$sql = "select site_id as ID, site_description as TEXT from tblsite";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	function html_form_draw_site_dd_linked($name, $value, $parameters = ''){
		
		return html_form_draw_site_dd($name, $value, $parameters);
	}
	
	function html_form_draw_department_dd($name, $value, $parameters = ''){
		$sql = "select department_id as ID, department_description as TEXT from tbldepartment";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	function html_form_draw_department_dd_linked($name, $value, $parameters = ''){
		
		return html_form_draw_department_dd($name, $value, $parameters);
	}
	
	function html_form_draw_section_dd($name, $value, $parameters = ''){
		$sql = "select section_id as ID, section_description as TEXT from tblsection";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_form_draw_mimms_employees_dd($name, $value, $parameters = ''){
		$sql = "SELECT EMPID as ID, SURNAME || ', ' || GIVEN_NAME || ' (' || EMPID || ')' as TEXT FROM View_MIMS_Data ORDER BY SURNAME ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	
	
	function html_action_type_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT Action_Type as ID  FROM tblAction_Details  ORDER BY Action_Type ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_action_category_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT Checkbox_Desc as TEXT, Checkbox_Id as ID  FROM tblIncidentForm_Checkboxes  WHERE Checkbox_Type = 'Incident_Category' ORDER BY Checkbox_Desc ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'TEXT', true), $value, $parameters, false, true);
	}
	
	function html_action_register_origin_dd($name, $value, $parameters = ''){
		$sql="SELECT DISTINCT Register_Origin as ID  FROM tblAction_Details  ORDER BY Register_Origin ASC";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_get_email_status_dd($name, $value, $parameters = ''){
		$arr = array(
			array("id"=>"2", "text" => "On"),
			array("id"=>"0", "text" => "Off"),
			array("id"=>"1", "text" => "Test")
		);
		return html_draw_pull_down_menu($name, $arr, $value, $parameters, false, true);
	}
	
	function html_get_employee_status_dd($name, $value='Open') {
		return html_get_status_dd($name,$value,'TBLEMPLOYEE_DETAILS');
	}
	
	function html_get_action_status_dd($name, $value='Open') {
		return html_get_status_dd($name,$value,'TBLACTION_DETAILS');
	}
	function html_get_status_dd($name,$value='',$type='',$parameters=''){
		$sql="select status_title as ID from status_values where status_type='$type'";
		return html_draw_pull_down_menu($name, html_db_options($sql, 'ID', 'ID', true), $value, $parameters, false, true);
	}
	
	function html_form_types_dd($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_types_options($value), $value, $parameters, false, true);
	}
	
	function html_group_type_options($value){
		$sql = "select distinct FIELD_GROUP_TYPE as ID from form_fields";
		return html_db_options($sql, 'ID', 'ID',true);
	}
	
	function html_form_types_options($value){
		$sql = "select id, name as text from form_types";
		return html_db_options($sql, $id='ID', $text='TEXT', true);
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_columns_helper
|  Purposes: Dropdown box of columns filtered by $table_value
|  Returns: HTML Select element:
|  Params: 
|    (string)table_value = name of table to filter(defaults to tblaction_details)
|    (string*)name = name of columns select element
|    (string*)value = value of selected column
|    (string)parameters = attributes to add to the element
|
+------------------------------------------------------------------------
*/
	function html_form_columns_helper($table_value='', $name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_columns_options($table_value), $value, $parameters, false, true);
	}
	function html_form_columns_options($value='tblaction_details'){
		if(empty($value)) $value = 'tblaction_details';// default table
		$sql = "select * from $value where rownum = 1"; // select a row from the table
		$arr = db_get_columns($value); // return columns array
		$out=array();
		foreach($arr as $key => $val){ // create options array
			$out[]=array("id"=>$val['CNAME'],"text"=>$val['CNAME']);
		}
		// re-create options array
		return html_db_options($out, $id='id', $text='text', false);
	}
	
	
	function html_form_tables_helper($cols_name = '', $name, $value, $parameters = ''){
		$cols_name=(empty($cols_name))?$name.'_TABLES':$cols_name;
		return html_draw_pull_down_menu($name, html_form_tables_options($value), $value, $parameters . ' onchange="remote.get_options_add(\'get_columns_array\',\'admin\',this,this.form.'.$cols_name.');" ', false, true);
	}
	function html_form_tables_options($value){
		return html_db_options(db_get_cat_details('TABLE', 'TBL'), $id='id', $text='text', true);
	}
	
/*
+------------------------------------------------------------------------
|
|  Function: html_db_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|  Returns: Array in the following format:
|			array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|			(string)||(array)sql = sql query string or array or ADODB_Recordset
|			(string)id = value attribute identifier
|			(string)text = text attribute identifier
|			(bool)ucwords = whether or not to show text as ucwords and replace underscores with space i.e. sheep_are_good in bed -> Sheep Are Good In Bed			
|
+------------------------------------------------------------------------
*/
	function html_db_options($sql, $id='ID', $text='TEXT', $ucwords=true){
		global $db;
		
		if(empty($sql)) return false;
		if(is_array($sql)){
			$arr = $sql;
		}else{
			$rs = $db->Execute($sql);
			$arr=$rs->GetAll();
		}
		// build array for options function
		$out=array();
		//print_r($arr);
		foreach($arr as $k => $v){
			$key=(is_array($v))?$v[$id]:$v;
			$val=(is_array($v))?$v[$text]:$v;
			$out[]=array("id"=>$key,"text"=>($ucwords?ucwords($val):$val));
		}
		//print_r($out);
		return $out;
	}
	
/*
+------------------------------------------------------------------------
|
|  Function: html_form_functions_chooser
|  Purposes: Combines function files dd with functions dd
|  Returns: (string)HTML dropdown group that remotes together to filter functions list
|  Params: 
|    (string)name = Name of main dropdown box(functions dd)
|    (variant)value = Value of dropdown box/s ... can be array("selected file value","selected function value")
|    (string)parameters = Element attributes
|
+------------------------------------------------------------------------
*/
	function html_form_functions_chooser($file_value='html_functions.php', $name='funcs', $value='html_draw_input_field', $parameters = ''){
		$files_dd = html_form_function_files_helper($name.'_files', $value, $parameters . 'onchange="remote.get_options_add(\'get_functions_array\',\'admin\',this,this.form.'.$name.');"');
		$ar=explode("_",$value);
		if(is_array($ar))
			$funcs_dd = html_form_functions_helper($ar[0].'_',$name,$value);
		else
			$funcs_dd = html_form_functions_helper(str_replace("functions.php","",$file_value),$name,$value);
		//$func_params = html_draw_input_field($name,$params='');
		return $files_dd.$funcs_dd;
	}
	
	function html_form_function_files_helper($name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_function_files_options(), $value, $parameters, false, true);
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_function_files_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|            using defined functions php funciton
|  Returns: Array in the following format:
|    array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|    (string)value = N/A
|
+------------------------------------------------------------------------
*/
	function html_form_function_files_options($preg_match='functions.php'){
		$arr = get_included_files();
		$out=array();
		
		$match="/($preg_match)/i";
		//echo($match);
		foreach ($arr as $file) if(preg_match($match,$file)) $out[]=array("id" => basename($file), "text" => str_replace(array("_",".php"),array(" ",""),basename($file)));
		return $out;
	}
	
	function html_form_functions_helper($filter, $name, $value, $parameters = ''){
		return html_draw_pull_down_menu($name, html_form_functions_options($filter), $value, $parameters, false, true);
	}
/*
+------------------------------------------------------------------------
|
|  Function: html_form_functions_options
|  Purposes: Generates an array that is compatible with html_draw_pull_down_menu
|            using defined functions php funciton
|  Returns: Array in the following format:
|    array(array("id"=>100, "text" => "Sheep"))
|  Params: 
|    (string)value = N/A
|
+------------------------------------------------------------------------
*/
	function html_form_functions_options($preg_match='html',$ftype='user'){
		$vars_arr = get_defined_functions();
		//print_r($vars_arr);
		$arr = $vars_arr[$ftype];
		$out=array();
		$match="/^($preg_match)/i";
		foreach ($arr as $val) if(preg_match($match,$val)) $out[]=array("id" => $val, "text" => $val);
		return $out;
	}
	
	
	
  function html_draw_pull_down_menu($name, $values, $default = '', $parameters = '', $required = false, $empty_option = false) {
    $field = '<select class="sfield" name="' . main_output_string($name) . '"';

    if (main_not_null($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && isset($_POST[$name])) $default = stripslashes($_POST[$name]);
		if($empty_option==true) $field .= '<option></option>';
    for ($i=0, $n=sizeof($values); $i<$n; $i++) {
      $field .= '<option value="' . main_output_string($values[$i]['id']) . '"';
      if ($default == $values[$i]['id']) {
        $field .= ' SELECTED';
      }

      $field .= '>' . main_output_string($values[$i]['text'], array('"' => '&quot;', '\'' => '&#039;', '<' => '&lt;', '>' => '&gt;')) . '</option>';
    }
    $field .= '</select>';

    if ($required == true) $field .= TEXT_FIELD_REQUIRED;

    return $field;
  }
/*
+---------------------------------------------------------------------------
|	Return an array to be used as options of a combo
+---------------------------------------------------------------------------
*/	
	function html_get_pull_down_options($sql, $field_value=0, $field_text=1) {		
		// global ADOBD object
		global $db;

		$i=0;
		$ret=array();
		$result = db_query($sql);
		//echo($sql."<br>");
		//print_r($result);
		while($row = db_fetch_array($result))
		{
			$ret[$i]['id'] = $row[$field_value];
			$ret[$i]['text'] = $row[$field_text];
			$i++;
		}
		return $ret;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with start and end date ranges
+---------------------------------------------------------------------------
*/
	function html_get_input_start_end_dates($date, $date_value, $end_date, $end_date_value, $show_start_lebel = false){
		$s="";
		if($show_start_label) $s .= '<label for="id_'.$date.'" style="width:auto;display:inline;">Start Date:&nbsp;</label>';
		//$s .= '<input class="dfield" name="'.$date.'" id="id_'.$date.'" type="text" value="'.$date_value.'" onclick="this.blur();_show_calendar(this);"><label for="id_'.$end_date.'" style="width:auto;display:inline;"> --> End Date:&nbsp;</label><input class="dfield" name="'.$end_date.'" id="id_'.$end_date.'" type="text" value="'.$end_date_value.'" " onclick="this.blur();_show_calendar(this);">';
		$s .= html_get_calendar_date_field($date, $date_value,'Y-m-j','yyyy-mm-dd','j-M-Y','dd-mmm-yyyy') . '<label for="id_'.$end_date.'" style="width:auto;display:inline;">';
		$s .= ' --> End Date:&nbsp;</label>' . html_get_calendar_date_field($end_date, $end_date_value,'Y-m-j','yyyy-mm-dd','j-M-Y','dd-mmm-yyyy', '', ' onchange="check_raw_date_range(this.form.elements[\''.$date.'\'],this.form.elements[\''.$end_date.'\']);" ');
		$s .= '
		<script type="text/javascript">
		EDITOR_VALIDATE += ((EDITOR_VALIDATE!="")?";":"") + "'.$date.'|'.$end_date.';rawdaterange;Start Date|End Date";
		</script>
		';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with start and end date+time ranges
+---------------------------------------------------------------------------
*/
	function html_get_input_start_end_datetimes($start_date, $start_date_value, $end_date, $end_date_value, $alt_start_time = '', $alt_end_time = '', $start_label = 'Start Date', $end_label = 'End Date', $show_start_label = false){
		$s="";
		$t_raw = 'H:i';
		$t_in = 'HH:MM';
		$f_r = 'Y-m-j';
		$f_raw = $f_r.' '.$t_raw;
		$f_i = 'yyyy-mm-dd';
		$f_in = $f_i.' '.$t_in;
		$d_r = 'j-M-Y';
		$d_raw = $d_r.' '.$t_raw;
		$d_i = 'dd-mmm-yyyy';
		$d_in = $d_i.' '.$t_in;
		//$s .= ';'.$start_date_value.':'.$end_date_value;
		$cal_start_time = db_getdate($start_date_value,$timestamp,$t_raw);
		if($cal_start_time=="00:00") $cal_start_time=(empty($alt_start_time))?"09:00":$alt_start_time;
		$start_date_value = db_getdate($start_date_value,$timestamp,$f_r).' '.$cal_start_time;
		$start_date_value = db_getdate($start_date_value,$timestamp,$f_raw);
		
		$cal_end_time = db_getdate($end_date_value,$timestamp,$t_raw);
		if($cal_end_time=="00:00") $cal_end_time=(empty($alt_end_time))?"16:30":$alt_end_time;
		//$s .= ';'.$cal_time.':join the date parts='.db_getdate($end_date_value,$timestamp,$f_r).' '.$cal_end_time;
		$end_date_value = db_getdate($end_date_value,$timestamp,$f_r).' '.$cal_end_time;
		$end_date_value = db_getdate($end_date_value,$timestamp,$f_raw);
		
		$control_start = html_get_calendar_date_field($start_date, $start_date_value, $f_raw,$f_in,$d_raw,$d_in);
		$control_end = html_get_calendar_date_field($end_date, $end_date_value,$f_raw,$f_in,$d_raw,$d_in, '', ' onchange="check_raw_date_range(this.form.elements[\''.$start_date.'\'],this.form.elements[\''.$end_date.'\']);" ');
		
		//$s .= ';start='.$start_date_value.':end='.$end_date_value;
		$s_start_label='';
		if($show_start_label){
			$s .= '<table><tr><td><label for="id_'.$start_date.'" style="width:auto;display:inline;">'.$start_label.':&nbsp;</label></td>';
			$s .= '</td><td></td><td>';
			$s .= '<label for="id_'.$end_date.'" style="width:auto;display:inline;">'.$end_label.':&nbsp;</label>';
			$s .= '</td></tr><tr><td>';
			$s .= $control_start.'</td><td>&nbsp;<b>to -></b>&nbsp;</td><td>'.$control_end.'</td></tr></table>';
		}else{
			$s .= $control_start;
			$s .= '<label for="id_'.$end_date.'" style="width:auto;display:inline;">';
			$s .= ' -> '.$end_label.':&nbsp;</label>';
			$s .= $control_end;
		}
		$s .= '
		<script type="text/javascript">
		EDITOR_VALIDATE += ((EDITOR_VALIDATE!="")?";":"") + "'.$start_date.'|'.$end_date.';rawdaterange;'.$start_label.'|'.$end_label.'";
		</script>
		';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use date
+---------------------------------------------------------------------------
*/
	function html_get_calendar_date_field($name = 'cal_date', $value = '', $format_in = 'j-M-Y', $format = 'yyyy-mm-dd', $display_in = 'j-M-Y', $display = 'dd-mmm-yyyy', $alt_time_value = '', $attributes = '', $timestamp = false, $reinsert_value=true){
		//$value = '2005-05-23 08:30';
		//$format = 'dd-mmm-yyyy' old format
		if(empty($value)){
			$cal_date = '';//date($format_in);
			$cal_display_date = '';//date($display_in);
		}else{
			$cal_date = db_getdate($value,$timestamp,$format_in);
			$cal_display_date = db_getdate($value,$timestamp,$display_in);
		}
		$class_name = (preg_match("/H|M/",$format))?"dtfield":"dfield";
		$dname=$name.'_d';
		$s = '
		<input class="'.$class_name.'" name="'.$dname.'" id="id_'.$dname.'" type="text" ';
		
		if ( (isset($_POST[$dname])) && ($reinsert_value == true) ) {
      $s .= ' value="' . main_output_string(stripslashes($_POST[$dname])) . '"';
    } elseif (main_not_null($value)) {
      $s .= ' value="' . main_output_string($value) . '"';
    } else {
		  $s .= ' value="' . main_output_string($cal_display_date) . '"';
		}
		$s .=' onkeypress="return _deny_keypress(this,0,event);" onfocus="_show_calendar(this.form.elements[\''.$name.'\'],\''.$display.'\',\''.$format.'\');" '.$attributes.'>';
		$s .= html_draw_hidden_field($name, $cal_date);//<input name="'.$name.'" id="id_'.$name.'" type="hidden" value="'.$cal_date.'" />
		$s .= '<input type="text" class="dfield_clear" style="width:25px;" title="Clear Date" onclick="this.form.elements[\''.$name.'\'].value=\'\';this.form.elements[\''.$dname.'\'].value=\'\';this.blur();"/>';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with time
+---------------------------------------------------------------------------
*/
	function html_get_calendar_time_field($name = 'cal_time', $value = '', $attributes = '', $timestamp = false, $alt_value = ''){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		$cal_time = db_getdate($value,$timestamp,'H:i');
		if($cal_time=="00:00") $cal_time=(empty($alt_value))?"09:00":$alt_value;
		$s = '
		<input title="'.$value.'" class="tfield" style="width:58px" name="'.$name.'" type="text" value="'.$cal_time.'" '.$attributes.'><span class="format">24hr(HH:MM)</span>';
		return $s;
	}
	
/*
+---------------------------------------------------------------------------
|	Return a smart form element for use with datetime
+---------------------------------------------------------------------------
*/
	function html_get_calendar_date_time_field($name = 'cal_date', $value = '', $alt_value = ''){
		//$value = '2005-05-23 08:30';
		if(empty($value)) $value = date('Y-m-d H:i');
		
		$cal_raw_date = db_getdate($value,false,'Y-m-d H:i');
		
		$s = html_get_calendar_date_field($name, $cal_raw_date, 'Y-m-d H:i', 'yyyy-m-d HH:MM', 'j-M-Y H:i', 'dd-mmm-yyyy HH:MM');
		return $s;
	}

	function html_get_file_location_field($name, $value){
		return '
		<input class="ltfield" value="'.$value.'" type="text" name="'.$name.'" onClick="browse_file_location(this);">
    <input type="button" name="btn'.$name.'" value="Change" onClick="change_file_location(this.form.'.$name.',this.form.browse'.$name.');">
    <input type="file" name="browse'.$name.'" style="display: none">
		';
	}

/*
+------------------------------------------------------------------------
|
|  Function: html_image
|  Purposes: Returns an img tag
|  Returns: html image tag
|  Params: 
|    (string)src = image source
|    (string)alt = alt text
|    (string)width = width of image
|    (string)height = height of image
|    (string)parameters = extra atrributes
|
+------------------------------------------------------------------------
*/
	function html_image($src, $alt = '', $width = '', $height = '', $parameters = '') {
    
// alt is added to the img tag even if it is null to prevent browsers from outputting
// the image filename as default
    $image = '<img src="' . main_output_string($src) . '" border="0" alt="' . main_output_string($alt) . '"';

    if (main_not_null($alt)) {
      $image .= ' title=" ' . main_output_string($alt) . ' "';
    }
/*
    if ( (CONFIG_CALCULATE_IMAGE_SIZE == 'true') && (empty($width) || empty($height)) ) {
      if ($image_size = @getimagesize($src)) {
        if (empty($width) && ezw_not_null($height)) {
          $ratio = $height / $image_size[1];
          $width = $image_size[0] * $ratio;
        } elseif (ezw_not_null($width) && empty($height)) {
          $ratio = $width / $image_size[0];
          $height = $image_size[1] * $ratio;
        } elseif (empty($width) && empty($height)) {
          $width = $image_size[0];
          $height = $image_size[1];
        }
      } elseif (IMAGE_REQUIRED == 'false') {
        return false;
      }
    }
*/
    if (main_not_null($width) && main_not_null($height)) {
      $image .= ' width="' . main_output_string($width) . '" height="' . main_output_string($height) . '"';
    }

    if (main_not_null($parameters)) $image .= ' ' . $parameters;

    $image .= '>';

    return $image;
  }
?>