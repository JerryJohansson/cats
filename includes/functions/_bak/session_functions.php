<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Site Configuration
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

function ezw_session_start() {
	return session_start();
}

function ezw_session_register($variable) {
	global $session_started;

	if ($session_started == true) {
		return session_register($variable);
	} else {
		return false;
	}
}

function ezw_session_is_registered($variable) {
	return session_is_registered($variable);
}

function ezw_session_unregister($variable) {
	return session_unregister($variable);
}

function ezw_session_id($sessid = '') {
	if (!empty($sessid)) {
		return session_id($sessid);
	} else {
		return session_id();
	}
}

function ezw_session_name($name = '') {
	if (!empty($name)) {
		return session_name($name);
	} else {
		return session_name();
	}
}

function ezw_session_close() {
	if (PHP_VERSION >= '4.0.4') {
		return session_write_close();
	} elseif (function_exists('session_close')) {
		return session_close();
	}
}

function ezw_session_destroy() {
	return session_destroy();
}

function ezw_session_save_path($path = '') {
	if (!empty($path)) {
		return session_save_path($path);
	} else {
		return session_save_path();
	}
}

function ezw_session_recreate() {
	if (PHP_VERSION >= 4.1) {
		$session_backup = $_SESSION;

		unset($_COOKIE[ezw_session_name()]);

		ezw_session_destroy();

		if (STORE_SESSIONS == 'mysql') {
			session_set_save_handler('_sess_open', '_sess_close', '_sess_read', '_sess_write', '_sess_destroy', '_sess_gc');
		}

		ezw_session_start();

		$_SESSION = $session_backup;
		unset($session_backup);
	}
}
?>
