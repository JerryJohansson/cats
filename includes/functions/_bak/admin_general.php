<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - General Functions
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
//- VJL - BEGIN::ADMIN
/*
+---------------------------------------------------------------------------
|	Check login and file access
+---------------------------------------------------------------------------
*/
function ezw_admin_check_login($query_string = '') {
  global $PHP_SELF, $login_groups_id;
  if (!ezw_session_is_registered('login_id')) {
    ezw_redirect(ezw_href_link(FILENAME_LOGIN, 'f=' . basename( $PHP_SELF ) . '&q=' . str_replace('&','/',$query_string) , 'SSL'));
  } else {
    $filename = basename( $PHP_SELF );
    if ($filename != FILENAME_DEFAULT && $filename != FILENAME_FORBIDEN && $filename != FILENAME_LOGOFF && $filename != FILENAME_ADMIN_ACCOUNT && $filename != FILENAME_POPUP_IMAGE && $filename != 'packingslip.php' && $filename != 'invoice.php') {
      $db_file_query = ezw_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where ( " . $_SESSION['user_details']['groups'] . " & admin_groups ) and admin_files_name = '" . $filename . "'");
      if (!ezw_db_num_rows($db_file_query)) {
        ezw_redirect(ezw_href_link(FILENAME_FORBIDEN));
      }
    }
  }  
}
/*
+---------------------------------------------------------------------------
|	Return 'true' or 'false' value to display boxes and files in index.php and column_left.php
+---------------------------------------------------------------------------
*/
function ezw_admin_check_boxes($filename, $boxes='') {
  global $login_groups_id;
  
  $is_boxes = 1;
  if ($boxes == 'sub_boxes') {
    $is_boxes = 0;
  }
  $dbquery = ezw_db_query("select admin_files_id from " . TABLE_ADMIN_FILES . " where ( " . $_SESSION['user_details']['groups'] . " & admin_groups ) and category = '" . $is_boxes . "' and admin_files_name = '" . $filename . "'");
  
  $return_value = false;
  if (ezw_db_num_rows($dbquery)) {
    $return_value = true;
  }
  return $return_value;
}
/*
+---------------------------------------------------------------------------
|	Return files stored in box that can be accessed by user
+---------------------------------------------------------------------------
*/
function ezw_admin_files_boxes($filename, $sub_box_name, $params='') {
  $sub_boxes = '';
  if (ezw_admin_files_access($filename)) {
    $sub_boxes = '<a href="' . ezw_href_link($filename, $params) . '">' . $sub_box_name . '</a>';
  }
  return $sub_boxes;
}
function ezw_admin_files_access($filename) {
  $dbquery = ezw_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where ( " . $_SESSION['user_details']['groups'] . " & admin_groups ) and category = '0' and admin_files_name = '" . $filename . "'");
  return (ezw_db_num_rows($dbquery)>0);
}
/*
+---------------------------------------------------------------------------
|	Get selected file for index.php
+---------------------------------------------------------------------------
*/
function ezw_selected_file($filename) {
  global $login_groups_id;
  $randomize = FILENAME_ADMIN_ACCOUNT;
  
  $dbquery = ezw_db_query("select admin_files_id as boxes_id from " . TABLE_ADMIN_FILES . " where ( " . $_SESSION['user_details']['groups'] . " & admin_groups ) and category = '1' and admin_files_name = '" . $filename . "'");
  if (ezw_db_num_rows($dbquery)) {
    $boxes_id = ezw_db_fetch_array($dbquery);
    $randomize_query = ezw_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where ( " . $_SESSION['user_details']['groups'] . " & admin_groups ) and category = '0' and parent_category = '" . $boxes_id['boxes_id'] . "'");
    if (ezw_db_num_rows($randomize_query)) {
      $file_selected = ezw_db_fetch_array($randomize_query);
      $randomize = $file_selected['admin_files_name'];
    }
  }
  return $randomize;
}
//- VJL - END::ADMIN
/*
+---------------------------------------------------------------------------
|	Alias function for Store configuration values in the Administration Tool
+---------------------------------------------------------------------------
*/
function ezw_cfg_pull_down_country_list($country_id) {
	return ezw_draw_pull_down_menu('configuration_value', ezw_get_countries(), $country_id);
}

function ezw_cfg_pull_down_zone_list($zone_id) {
	return ezw_draw_pull_down_menu('configuration_value', ezw_get_country_zones(STORE_COUNTRY), $zone_id);
}

function ezw_cfg_pull_down_tax_classes($tax_class_id, $key = '') {
	$name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

	$tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
	$tax_class_query = ezw_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
	while ($tax_class = ezw_db_fetch_array($tax_class_query)) {
		$tax_class_array[] = array('id' => $tax_class['tax_class_id'],
															 'text' => $tax_class['tax_class_title']);
	}

	return ezw_draw_pull_down_menu($name, $tax_class_array, $tax_class_id);
}
/*
+---------------------------------------------------------------------------
|	Function to read in text area in admin
+---------------------------------------------------------------------------
*/
function ezw_cfg_textarea($text) {
	return ezw_draw_textarea_field('configuration_value', false, 35, 5, $text);
}

function ezw_cfg_get_zone_name($zone_id) {
	$zone_query = ezw_db_query("select zone_name from " . TABLE_ZONES . " where zone_id = '" . (int)$zone_id . "'");

	if (!ezw_db_num_rows($zone_query)) {
		return $zone_id;
	} else {
		$zone = ezw_db_fetch_array($zone_query);
		return $zone['zone_name'];
	}
}
/*
+---------------------------------------------------------------------------
|	Alias function for Store configuration values in the Administration Tool
+---------------------------------------------------------------------------
*/
function ezw_cfg_select_option($select_array, $key_value, $key = '') {
	$string = '';

	for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
		$name = ((ezw_not_null($key)) ? 'configuration[' . $key . ']' : 'configuration_value');

		$string .= '<br><input type="radio" id="id_' . $name . $i . '" name="' . $name . '" value="' . $select_array[$i] . '"';

		if ($key_value == $select_array[$i]) $string .= ' CHECKED';

		$string .= '><label for="id_' . $name . $i . '" style="display:inline;padding-left:5px;">' . $select_array[$i] . '</label>';
	}

	return $string;
}
/*
+---------------------------------------------------------------------------
|	Alias function for module configuration keys
+---------------------------------------------------------------------------
*/
function ezw_mod_select_option($select_array, $name, $key_value) {
	reset($select_array);
	$i = 0;
	while (list($key, $value) = each($select_array)) {
		if (is_int($key)) $key = $value;
		$string .= '<br><input type="radio" id="id_' . $name . $i . '" name="configuration[' . $name . ']" value="' . $key . '"';
		if ($key_value == $key) $string .= ' CHECKED';
		$string .= '><label for="id_' . $name . $i . '" style="display:inline;padding-left:5px;">' . $value . '</label>';
		$i++;
	}

	return $string;
}
?>
