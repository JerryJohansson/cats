<?php
require(DIR_WS_CLASSES."template.php");
require(DIR_WS_CLASSES."container.php");
require(DIR_WS_CLASSES."schedule.php");
require(DIR_WS_CLASSES."navigation.php");


if($id)
{
    $obj = new Schedule($id);
    $content = $obj->getArticle();
}
elseif($item)
{
    $obj = new Schedule;
    $content = $obj->getPage($item);
}
else
{
    $obj = new Schedule;
    $content = $obj->getPage(0);
}

if(!isset($mt)) $mt = DEFAULT_MENU_SYSTEM;
$menu = new Navigation($mt);
$menu->setMenus();

$tbl_file = $obj->getTemplateFile();
$tpl = new Template($tbl_file);

$values = array( 
	"title"						=> $obj->getTitle(),
	"keywords"				=> $obj->getKeywords(),
	"description"			=> $obj->getDescription(),
	"banner"					=> $obj->getBanner(),
	"banner_left"			=> $obj->getBanner(BANNER_LEFT_TYPE,BANNER_LEFT_COUNT),
	"banner_right"		=> $obj->getBanner(BANNER_RIGHT_TYPE,BANNER_RIGHT_COUNT),
	"top_navigation"	=> $obj->buildTopNav(),
	"main"						=> $content , 
	"menu"						=> $menu->getMenuHTML(), 
	"menujs"					=> $menu->getMenuJS(),
	"crumbs"					=> $obj->buildCrumbs(),
	"signup"					=> $obj->getSignupForm()
);
$tpl->parse($values);
$tpl->output();
?>