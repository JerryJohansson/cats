<link type="text/css" rel="stylesheet" href="style/top.css"/>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script language="JavaScript" src="js/forms.js"></script>
<script language="JavaScript" src="js/classes/tabs/tab.js"></script>


<script>



function _session(){
	this.SID='<?php echo(session_id());?>';
	this.history=new Object();
	this.add_history=function(itm){
		this.history[itm.id]=new Object();
		var o=this.history[itm.id];
		o.innerHTML=itm.innerHTML;
	}
	return this;
}
function _option(name){
	this.name = name;
	this._array = new Array();
	this._html = "";
	this.options = new Array();
	
	this.to_array = function(e){
		var i;
		var o=e.options;
		var x=o.length;
		var a=new Array();
		for(i=0;i<x;i++){
			if(o[i].text!="") a.push([o[i].text , o[i].value]);
		}
		return a;
	}
	this.add = function(name,el,arr){
		//alert(name+":From options["+name+"]");
		var value=0;
		var text=1;
		var i=0;
		var len=arr.length;
		
		this._array=this.to_array(el);
		var atop=this._array;
		
		//alert(this._array+":"+atop.length);
		//alert(atop.length+":"+atop);
		for(i=0;i<len;i++){
			var olen=atop.length;//el.options.length;
			var add = true;
			if(olen==0) add = true;
			JLOOP:for(j=0;j<olen;j++){
				if(String(atop[j][value])==String(arr[i][value])){
					add=false;
					break JLOOP;
				}
			}
			dalert(add);
			if(add){
				this._array.push([arr[i][text],arr[i][value]]);
				var o=new Option(arr[i][text],arr[i][value]);
				this.options.push(o)
				//el.options[el.options.length]=o;//new Option(arr[i][text],arr[i][value]);
			}
		}
		var newa = this._array;
		_asort(newa);
		//_parray(newa);
		//alert(this._array);
		//alert(this._array+"\n\n"+newa+":"+newa.length);
		el.options.length=0;
		var x=this.options.length;
		//alert(x);
		for(i=0;i<x;i++) el.options[el.options.length]=new Option(this.options[i].text,this.options[i].value);
		//alert(arguments[3]);
		if(typeof(arguments[3])!="undefined" && arguments[3]!=null){
			var id=arguments[3];
			//alert("ID From options["+name+"].add="+id);
			if(typeof(id)=="object")
				id=id.value;
			return this.select(el,id);
		}
	}
	this.compare = function(x,y){
		return x-y;
	}
	this.select = function(el,id){
		var ret='';
		var i=0;
		var opts=el.options;
		var len=opts.length;
		for(i=0;i<len;i++){
			if(String(id)==String(opts[i].value)){
				ret=opts[i].value;
				opts[i].selected=true;
				break;
			}
		}
		return ret;
	}
	this.clear = function(el){
		el.options.length = 0;
	}
	this.replace = function(el,arr){
		this.clear(el);
		this.add(el,arr);
	}
}
function _gui(){
	try{
	this.SID='<?php echo(session_id());?>';
	this.options=new Object();
	this.screens=new Object();
	this.tabs=new Object();
	this.session=new _session();
	}catch(e){alert(e)}
	//return this;
}
var gui=new _gui();
top.gui.options.add = function(name){
	if(typeof(top.gui.options[name])!="object")
		top.gui.options[name] = new _option(name);
}
top.gui.options.remove = function(name){
	if(top.gui.options[name]) top.gui.options[name] = null;
}
//alert(gui);
var session=new _session();



var oXML=null;
function cats_get_path(path,module){
	return path+module+".php";
}
function cats_get_remote_path(m){
	return cats_get_path(CATS_REMOTE_PATH,m);
}


function getAvailableHeight(sobj,offset){
	var obj=el=element(sobj);
	var h=h1=0;
	var bh=(!document.all)?window.innerHeight:document.body.offsetHeight;//-40;
	while(((el=el.parentNode)!=null) && el.nodeName != "BODY" ){
		h1+=parseInt(el.offsetTop);
	}
	//alert(""+bh+":"+h1+":"+document.body.clientHeight);
	h=(bh-h1);
	return h-((typeof(offset)=="number")?offset:0);
}
function getAvailableWidth(sobj,offset){
	//var obj=el=element(sobj);
	var w=(!document.all)?window.innerWidth:document.body.offsetWidth+2;
	return w-((typeof(offset)=="number")?offset:0);
}
function create_element(id,tag){
	var el=element(id);
	if(el) return el;
	//alert("carry on and create tag");
	var o=document.createElement(tag);
	o.id=id;
	var d=document.body.appendChild(o);
	//alert(d);
	return d;
	el=element(id);
	return el;
}
var IFRAME_WAIT_COUNTER_VALUE=null;
var IFRAME_FOUND=false;
function return_menu(o){
	if(CATS_MENU!=null){
		var f=create_element(CATS_MENU,"iframe");
		tabs[CATS_MENU]['element']=f;
		tabs[CATS_MENU]['html']=o;
	}else{
		var f=element("menu_frame");
	}
	/*var win=window.open("","debugging");
	win.document.open();
	win.document.write("<h1>Debugging:</h1><pre>"+o+"</pre>");
	win.document.close();
	win.focus();*/
	IFRAME_WAIT_COUNTER_VALUE=0;
	function wait4Iframe(){
		IFRAME_WAIT_COUNTER_VALUE++;
		try{
			f.contentWindow.document.body.innerHTML=o;
			IFRAME_WAIT_COUNTER_VALUE=null;
			IFRAME_FOUND=true;
		}catch(e){if(IFRAME_WAIT_COUNTER_VALUE<1000) wait4Iframe();}
	}
	if(IFRAME_FOUND==false) wait4Iframe();
	else IFRAME_FOUND = false;
	//alert(f.contentWindow.document.body.innerHTML)
	var c=f.contentWindow.document.getElementById(_cont);
	var d=document.body;
	var w=(d.offsetWidth*.7);
	//alert(f.id);
	f.style.backgroundColor = "white";
	f.style.width=w;
	f.style.height=c.offsetHeight;
	f.style.top=70;
	f.style.left=(d.offsetWidth/2)-(w/2);
	f.style.visibility = '';
}
var CATS_MENU=null;
function showMenu(id){
	if(CATS_MENU!=null) hideMenu();
	CATS_MENU=id;
	//alert(id);
	if(tabs[CATS_MENU]&&typeof(tabs[CATS_MENU]['html'])=="string"){
		return_menu(tabs[CATS_MENU].html,"testing");
	}else{
		var f=create_element(CATS_MENU,"iframe");
		//alert(f)
		f.contentWindow.location.href="index.php?m=index&p=menu";
		f.style.position="absolute";
		f.style.zIndex=99999;
		f.style.visibility = 'hidden';
		//alert(f);
		var id = (arguments[2])?arguments[2]:1;
		var url=cats_get_remote_path('menu');
		var s="id="+id+"&m="+CATS_MENU+"&t="+((arguments[1])?arguments[1]:"CATS");
		oXML=_HTTPRequest(url,"post",return_menu,s.replace(/&$/,""));;
		tabs[CATS_MENU]={};
	}
}
function hideMenu(){
	try{
		var f = element(CATS_MENU);
		CATS_MENU=null;
	//	alert(f.style.visibility);
		f.style.visibility = 'hidden';
		f.contentWindow.document.body.innerHTML="";
	}catch(e){}
}
function doMenu(){
	//try{
	hideMenu();
		var name="";
		if(arguments[0]){
			name=arguments[0].replace(/\.php/,"").replace(/\s/gi,"_");
			//alert(typeof(tabs[name]))
			if(typeof(tabs[name])=="object"){// && tabs.toolbar[name]){
				var s ="";
				for(e in tabs[name]){s+=e+"="+tabs[name][e]+'\n'}
				alert(s);
			}else{
				var o=tabs.add(name, true);//[name]=true;
				//element(_iframe).contentWindow.window.get_fields(name);
				//element("page_title").innerHTML = title;
			}
		}
		
		if(arguments[0])
			element(_iframe).contentWindow.window.location.href = "index.php?m=search&a="+name;
		if(arguments[1])
			element(_title).innerHTML = arguments[1];
	//}catch(e){}
}

window.onload = function(){
	doloaded();
	doresize();
}
window.onresize = doresize;

function doloaded(){
	element(_message).style.display = 'none';
	element(_iframe).style.display = 'block';
	element(_top_bar).style.display = 'block';
	element("top_personal").style.display = 'block';
	element(_tab_toolbar_cont).style.display = 'block';
	element("loading_message").style.visibility = 'hidden';
}
function doresize(){
	var iframe = (arguments.length>0)?arguments[0]:_iframe;
	element(iframe).style.height = ""+getAvailableHeight(iframe,70)+"px";
	element(iframe).style.width = ""+getAvailableWidth(iframe,4)+"px";
}
function element(s){return document.getElementById(s);}

function _get_class_path(s){
	return CATS_CLASSES_PATH+s;
}
function _iframe_redirect(){
	try{element(_iframe).contentWindow.window.location.href = arguments[0];}catch(e){};
}
function _history(){
	//element("menu_history").style.display='block';
	showMenu('history', 'My History',true)
}
function _favorites(){
	//element("menu_history").style.display='block';
	showMenu('history', 'My Favorites',true)
}

function _search(){
	showMenu('audits_and_inspections','Search:Audits & Inspections');
}
function _results(){
	doMenu();
}
function _edit(){
	doMenu();
}
function _cancel(){
	doMenu();
}
function _new(){
	_iframe_redirect('edit.php');
	doMenu();
}
</script>
<style>
html, body{
	height:100%;
	width:100%;
	overflow:hidden;
}
</style>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0">
<div id="loading_message" style="display:block;position:absolute;width:100%;height:100%;z-index:9999; background-attachment: fixed; background-repeat:no-repeat; background-position: center; background-image:url(images/cats_bg.gif)">
	<h1>Loading...</h1>
</div>
<div id="top_personal" class="bar" style="display:none;background-color: transparent; position:absolute; float:right; z-index: 10000; top: 0px; right: 0px;">
<a
	title="My History"
	href="javascript:_history();" 
	style="background-image: url(style/icons/btn_history_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>
<a
	title="My Favorites"
	href="javascript:_favorites();" 
	style="background-image: url(style/icons/btn_favorites_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>
<a
	title="My Employee Details"
	href="javascript:_account();" 
	style="background-image: url(style/icons/btn_my_employee_details_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>
<a
	title="Logout"
	href="index.php?m=login&p=logout" 
	style="background-image: url(style/icons/btn_logout_bg.gif);float:right; height:20px; width:24px; padding:0px;"></a>
</div>
<div id="top_logo" style="display:none">
<a href="javascript:_iframe_redirect('index.php')" class="cats_home"></a>
<?php
$NO_CONFIG=true;
require_once(CATS_REMOTE_PATH . 'menu.php');
?>
<a name="title" id="page_title"></a>
</div>

<div id="toolbar" style="display:none">
<fieldset class="bar" id="tab_buttons">
<div id="tab_toolbar" style="float:left;">
 
</div>
<!--<a
	title="spacer"
	href="javascript:{};" 
	style="background-image: url(icons/spacer.gif);float:right;width:0px; height:20px;padding: 0px"></a>-->
</fieldset>
</div>
<!--iframe src="?m=index&p=menu" id="menu_frame" name="menu_frame" style="position:absolute;visibility:hidden;border:1px solid;top:120px;width:400px;z-index:99999;" ></iframe-->
<div id="menu_history" style="display:none;">
	<div id="menu_history_list" class="bar" style="height:200px;overflow: auto;"></div>
</div>
<div id="menu_search" style="display:none"></div>
<iframe src="index.php" id="main_frame" name="main_frame" style="display:none"></iframe>
<iframe id="eze_popup_buffer" src="includes/blank.php" marginwidth="0" marginheight="0" border="0" frameborder="no" scrolling="no" style="visibility:hidden; position: absolute;z-index:10000;"></iframe>
