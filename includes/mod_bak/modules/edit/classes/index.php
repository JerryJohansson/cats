<?php
class Edit{
	/*
	+---------------------------------------------------------------------------
	|  Set Properties Function for all child classes
	+---------------------------------------------------------------------------
	*/
	function setProperties($POST='',$prop_names='',$set_funcs=''){
		if(!is_array($prop_names)) $prop_names=$this->getPropertyNames();
		if(!is_array($set_funcs)) $set_funcs=$this->getPropertySetFunctions();
		if(is_array($POST)) $_POST = array_merge($_POST, $POST);
		$properties=array();
		if(!isset($_POST['properties']))
			$_POST = array_merge($_POST, array('properties' => array()));
		$len=count($prop_names);
		for($i=0;$i<$len;$i++) {
			$name=$prop_names[$i];
			//ezw_dbg(">>$name=".$_POST[$name]."::".$POST[$name]);
			if(isset($set_funcs[$i]) && $set_funcs[$i]!="")
				//$properties = array_merge($properties, array($name => $this->$set_funcs[$i]($_POST[$name])));
				$_POST['properties'] = array_merge($_POST['properties'], array($name => $this->$set_funcs[$i]($_POST[$name])));
			else
				$_POST['properties'] = array_merge($_POST['properties'], array($name => $_POST[$name]));
			//ezw_dbg(">>properties[$name]=".$_POST['properties'][$name]);
		}
		
		
		//if(is_array($_POST['properties']))
		//	$_POST['properties'] = array_merge($_POST['properties'], $properties);
		
		$this->properties = $_POST['properties'];//array_merge($this->properties, $properties);
		
		
		/*
		ezw_dbg("<br><b>setProperties::this->properties=</b>");
		ezw_dbg($_POST['properties']);
		ezw_dbg("<br><b>setProperties::prop_names=</b>");
		ezw_dbg($prop_names);
		ezw_dbg("<br><b>setProperties::set_funcs=</b>");
		ezw_dbg($set_funcs);
		*/
		
		//return $POST;
		$pgid=$this->getPageId();
		if($pgid){
			ezw_db_connect();
			$sql_data = array(
					"object"				=> serialize($_POST['properties'])
			);
			ezw_db_perform('page', $sql_data, 'update', "id='$pgid'");
		}
		
	}
	function getProperties(){
		return $this->properties;
	}
	function getPropertyNames(){
		return explode("|","file|theme|template|thumb");
	}
	function getPropertyDisplayFunctions($admin = true){
		if($admin)
			return array('','','','');
		else
			return array('','','','');
	}
	function getPropertySetFunctions(){
		return array('','','','');
	}
	
}
?>