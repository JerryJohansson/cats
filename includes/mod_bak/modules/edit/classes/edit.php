<?php
/*
+--------------------------------------------------------------------------
|   Container Database Wrapper
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
|   Used to access the page information. Part of the ezwebmaker CMS.
+---------------------------------------------------------------------------
*/

class Edit
{
	var $error;
	var $id;
	var $pid;
	
	var $date_published;
	var $notify_published;
	var $email_owner;
	var $email_site_owner;
	var $email_editor;
	var $email_publisher;
	
	var $properties;
	var $status;
	
	function Edit($id=0,$type='page',$tblcontent='page',$tblref='')
	{
		
		if($id != 0)
		{
			$this->setId($id);
			$this->load();
		}
		else
		{
			$this->setError("");
			$this->setId("");
			$this->setPid("");
			$this->setStatus(0);
		}
	}
	/*
	+---------------------------------------------------------------------------
	|  Set Properties Function for all child classes
	+---------------------------------------------------------------------------
	*/
	function setProperties($properties_in='',$prop_names='',$set_funcs='', $input_type='POST'){
		// eval the input type: $_GET, $_POST, $_SESSION
		eval('$input_values = $_'.addslashes($input_type).';');
		if(!is_array($input_values)) $input_values = $_POST; // fall back to post if input_type is incorrect
		// properties used to fill form fields etc...
		$properties=(isset($this->properties) && is_array($this->properties)) ? $this->properties : array();
		
		// get property dislay helper functions
		if(!is_array($prop_names)) $prop_names=$this->getPropertyNames();
		
		// get property set helper functions
		if(!is_array($set_funcs)) $set_funcs=$this->getPropertySetFunctions();
		
		// merge properties with properties_in
		if(is_array($properties_in)) $properties = array_merge($properties, $properties_in);
		
		// loop through expected property names and set their values using the $_POST
		// TODO: make other globals available i.e. $_GET, $_POST, or $_SESSION
		$len=count($prop_names);
		for($i=0;$i<$len;$i++) {
			$name=$prop_names[$i];
			if(isset($set_funcs[$i]) && $set_funcs[$i]!="") {
				// if there is a function set, use it!
				// if the function is not ended correctly we could get proplems with eval
				//  functions must end with a ( or , so we can properly finish off the function statement
				// example function db value: html_display_dropdown("param1","param2",  
				// example when compiled: html_display_dropdown("param1","param2", "$value");
				//  NOTE: the addition to the end of the function call: , "$value");
				//  the line below constructs the function call and inserts the value automatically as the last parameter
				//
				$func = (preg_match("/(\(|,)$/",$this->$set_funcs[$i])>0)?$this->$set_funcs[$i]:$this->$set_funcs[$i].'("';
				eval('$properties = ' . $this->$set_funcs[$i] . '"' . htmlspecialchars($_POST[$name]) . '");');
				//$properties = array_merge($properties, array($name => $this->$set_funcs[$i]($_POST[$name])));
			} else {
				// no function to use so insert default value
				$properties = array_merge($properties, array($name => $_POST[$name]));
			}
		}
		// set the properties property to new value
		$this->properties = $properties;
	}
	function getProperties(){
		return $this->properties;
	}
	function getPropertyNames($type='edit', $mode='admin'){
		return explode("|","FIELD_ID|FIELD_PID|FIELD_GROUP_TYPE|FIELD_NAME|FIELD_LABEL|FIELD_TYPE|FIELD_LENGTH|FIELD_VALUE|FIELD_ATTRIBUTES|FIELD_TABLE_NAME|FIELD_COLUMN_NAME|FIELD_DISPLAY_FUNC|FIELD_SET_FUNC|COL_NUM|COL_SPAN|ROW_SPAN|COL_COUNT|MANDATORY|OPTIONS|SORT_ORDER|ROLE_MASK|GROUP_MASK|STATUS");
	}
	function getPropertyDisplayFunctions($type='edit', $mode='admin'){
		// return an array of display functions for each property
		switch($type){
			case 'edit':
				if($mode=='admin')
					return explode("|","html_form_hidden(|html_form_hidden(|html_group_type_dd(|||html_form_types_dd(||||html_form_tables_helper('FIELD_COLUMN_NAME',|html_form_columns_helper(|html_form_functions_chooser('db_functions.php',|html_form_functions_chooser('db_functions.php',||||||||||");
				else
					return explode("|","||||||||||||||||||||||");
				break;
			case 'search':
				if($mode=='admin')
					return explode("|","|html_form_hidden(|||||||||||||||||||||");
				else
					return explode("|","||||||||||||||||||||||");
				break;
			default:
				return explode("|","||||||||||||||||||||||");
				break;
		}
	}
	function getPropertySetFunctions($type='edit', $mode='admin'){
		switch($type){
			case 'edit':
				if($mode=='admin')
					return explode("|","||||||||||||||||||||||");
				else
					return explode("|","||||||||||||||||||||||");
				break;
			case 'search':
				if($mode=='admin')
					return explode("|","||||||||||||||||||||||");
				else
					return explode("|","||||||||||||||||||||||");
				break;
			default:
				return explode("|","||||||||||||||||||||||");
				break;
		}
	}
	
/*
+---------------------------------------------------------------------------
|   Editor Fields Display
|		!NOTE FOR EXTRACT:
|			Make sure you use one of the non-overwriting extract_type values such
|			as EXTR_SKIP and be aware that you should extract $_SERVER, $_SESSION,
|			$_COOKIE, $_POST and $_GET in that order. 
|		!Example: extract($properties, EXTR_PREFIX_SAME, "cats_");
+---------------------------------------------------------------------------
*/
	/*
	+---------------------------------------------------------------------------
	|   drawForm
	|		Draws a table given a two dimensional array - uses tohtml library from ADODB
	|			Use this method to return a form using the class properties to populate
	|		Params:	
	|			$arr = Two dimensional array of values
	|		!Example: 
	|			$myarray[]=array($val1,$val2,...);
	|			$this->drawForm($myarray); // print html table
	+---------------------------------------------------------------------------
	*/
	function drawForm($arr){
		$out=array();
		foreach($arr as $key => $value) $out[]=array($key, $value);
		arr2html_form($out,'cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;"');
		//<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
	}
	
	function getFormAdd()
	{
		// set return value to false incase we have no props
		$values=false;
		$prop_names=$this->getPropertyNames("add","admin");
		$prop_funcs=$this->getPropertyDisplayFunctions("add","admin");
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$values=array();
			// empty the property list
			for($i=0;$i<$len;$i++) {
				$this->properties[$prop_names[$i]] = "" ;
			}
			if(is_array($this->properties)){
				$len=count($prop_names);
				for($i=0;$i<$len;$i++) {
					$name=$prop_names[$i];
					if($prop_funcs[$i]!=""){
						$func = $this->$set_funcs[$i];
						$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'("';
						eval('$values = array_merge($values, array($name => ' . $func . '"' . $this->properties[$name] . '");');
					}else{
						$values=array_merge($values,array($name => $this->properties[$name]));
					}
				}
			}
		}
		return $values;
	}

	function getFormEdit()
	{
		global $db;
		$func_return="";
		// set return value to false incase we have no props
		$values=false;
		$prop_names=$this->getPropertyNames("edit","admin");
		$prop_funcs=$this->getPropertyDisplayFunctions("edit","admin");
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$values=array();
			if(is_array($this->properties)){
				$len=count($prop_names);
				foreach($this->properties as $id => $props){
					$i=0;
					
					//echo("<br>id=$id:props=<br>");
					//print_r($props);
					//print_r($prop_names);
					//$plen=count($props);
					//echo("<br>plen=$plen:len=$len<br>");
					for($i=0;$i<$len;$i++) {
						$pname=$prop_names[$i];
						//echo("<b>PNAME</b>$pname=$props[$pname]<br>");
						//if(
						$value=$props[$pname];
						//){
							//echo("<br>i=$i;<br>$pname:<br>value=$value<br>");
							
							if($prop_funcs[$i]!=""){
								$func = $prop_funcs[$i];
								$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'("';
								//echo('$values = array_merge($values, array("'.$pname.'" => ' . $func . '"' . $pname . '","' . $value . '")));');
								eval('$values = array_merge($values, array("'.$pname.'" => ' . $func . '"' . $pname . '","' . $value . '")));');
							}else{
								$values=array_merge($values,array($pname => html_draw_input_field($pname,$value)));
							}
						//}
					}
				}
			}
		}
		return $values;
	}

	function getFormSearch()
	{
		// set return value to false incase we have no props
		$values=false;
		$prop_names=$this->getPropertyNames("search","admin");
		$prop_funcs=$this->getPropertyDisplayFunctions("search","admin");
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$values=array();
			if(is_array($this->properties)){
				$len=count($prop_names);
				for($i=0;$i<$len;$i++) {
					$name=$prop_names[$i];
					if($prop_funcs[$i]!=""){
						$func = $this->$set_funcs[$i];
						$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'("';
						eval('$values = array_merge($values, array($name => ' . $func . '"' . $this->properties[$name] . '");');
					}else{
						$values=array_merge($values,array($name => $this->properties[$name]));
					}
				}
			}
		}
		return $values;
	}

	function getFormReport()
	{
		// set return value to false incase we have no props
		$values=false;
		$prop_names=$this->getPropertyNames("report");
		$prop_funcs=$this->getPropertyDisplayFunctions("report");
		if(is_array($prop_names) && is_array($prop_funcs))
		{
			$values=array();
			if(is_array($this->properties)){
				$len=count($prop_names);
				for($i=0;$i<$len;$i++) {
					$name=$prop_names[$i];
					if($prop_funcs[$i]!=""){
						$func = $this->$set_funcs[$i];
						$func = (preg_match("/(\(|,)$/",$func)>0)?$func:$func.'("';
						eval('$values = array_merge($values, array($name => ' . $func . '"' . $this->properties[$name] . '");');
					}else{
						$values=array_merge($values,array($name => $this->properties[$name]));
					}
				}
			}
		}
		return $values;
	}

	/*
	+---------------------------------------------------------------------------
	|  Load an object from the database 
	|		id of node
	|		edit flag to get unpublished document
	+---------------------------------------------------------------------------
	*/
	function load($id=0, $edit=false)
	{
				
		// global ADOBD object
		global $db;

		if($id == 0)
			$id = $this->getId();
		require_once(CATS_CLASSES_PATH . "navigation.php");
		$nav = new Navigation("edit","admin");
		$sql_form = $nav->getFormSQL($id);
		$sql = $nav->getSQL($id);
		//echo($sql);
		// get the editing page and not the published document
		//echo($sql_form);
		//$rs = db_query($sql_form) or die("<b>DB Error in Container::load()</b><br><b>File:".__FILE__."</b><br/><b>Line:".__LINE__."</b>".db_error());
		$rs = $db->Execute($sql_form);
		//echo("<br><b>load rs</b>=");print_r($rs);
		//if($rs->RecordCount() > 0 && $id > 0)
		
		if($this->properties = $rs->GetArray())
		{
			//print_r($this->properties);
			$data = $this->properties[0];
			//$data = db_fetch_array($rs);
			if($data['GROUP_MASK']!='')
			{
				$groups = $data['GROUP_MASK'];
				if($groups > 0)
				{
					if($_REQUEST['id']==$id) auth_groups($groups);
				}
			}
			// set data
			$this->setId($data['FIELD_ID']);
			$this->setPid($data['FIELD_PID']);
			
/*
			$this->setGroupSecurity($data['GROUPS']);
			$this->setRoleSecurity($data['ROLES']);
			$this->setOwnerId($data['OWNER_ID']);
			$this->setEditorId($data['EDITOR_ID']);
			$this->setPublisherId($data['PUBLISHER_ID']);
			$this->setDatePublished($data['DATE_PUBLISHED']);

// TODO: set these up as properties of each node so they
// can be set at a global level or at the node level

			$this->setNotifyPublish();
			$this->setEmailOwner();
			$this->setEmailSiteOwner();
			$this->setEmailPublisher();
			$this->setEmailEditor();
*/
			$this->setStatus($data['STATUS']);
			$ret = true;
		}
		else
		{
			$this->setError("<b>No matching row in DB Container::load()</b><br> field_id = ".$id);
			$ret = false;
		}
		return $ret;
	}
	
	
	function getMask($in){
		return $this->addMask($this->getStatus(),$in);
	}
	function addMask($int_mask,$in){
		return ((int)($int_mask+0) & (int)$in)==0?((int)$int_mask+$in):((int)$int_mask+0);
	}
	function removeMask($int_mask,$out){
		return ((int)($int_mask+0) & (int)$out)==0?((int)$int_mask-$out):(int)$int_mask+0;
	}
	
	
	function insert()
	{
		
		// global ADOBD object
		global $db;

		// insert node details into nav tables
		$sql_data = $this->setProperties();
		$rs = db_perform($this->tbl, $sql_data);
		return $rs;
	}
	
	function postAdd()
	{		
		// Go ahead and create the record and then update the class with the new id
		$id = $this->insert();
		if($this->hasChildren()){
			$ret = $_POST['pid'];
		}else{
			$ret = $this->getParentId($_POST['pid']);
		}
		return $ret;
	}
	
	
	function postEdit($update=true)
	{
		$ret = true;
		$publish = false;
		$this->setStatus($_POST['status']);
		switch($this->getStatus()){
			case MASK_PUBLISHED:
				if(!$this->isPublished()){
					$publish = true;
					$this->setStatus(MASK_PUBLISHED);
				}
				break;
			case 132: // re-publish item
				$publish = true;
				$this->setStatus(MASK_PUBLISHED,true);
				break;
			case 0:
				$this->setNodeStatus(0,true);
				break;
			default:
				//if($this->isPublished()){
					
					// everythings all good
					
				//}
				break;
		}
		
		$this->setProperties();
		if($update) $ret = $this->update($publish);
		
		return $this->getPid();
	}
	/*
	+---------------------------------------------------------------------------
	|   Duplicates a loaded page object and saves it under the $where node id
	+---------------------------------------------------------------------------
	*/
	function duplicate($POST,$where)
	{
		$this->setPid($where);
		$insert_id = $this->insert($POST);
		return $insert_id;
	}
	
	function publish($insert=false){
		// published is equal to:
		// node.status & MASK_PUBLISHED [only set if $_POST['status']==0]
		// page.status = MASK_PUBLISHED [$_POST['status']]
		// page.date_published = datetime published
		// 
		$this->setStatus($this->getMask(MASK_PUBLISHED));
		db_connect();
		$sql_data = array(
			"publisher_id"	=> $_SESSION['user_details']['user_id'],
			"status"				=> MASK_PUBLISHED,
			"date_modified"	=> 'now()',
			"date_published" => 'now()'
		);
		$pid = $this->getPageId();
		$ret = db_perform(TABLE_PAGE, $sql_data, 'update', "id='$pid'");
		$field = "date_published";
		$this->setDatePublished(db_getfieldvalue("select $field from ".TABLE_PAGE." where id='$pid' ",$field));
		// update node ref to point to published page
		$this->updateNodeRef($this->getPageId());
		
		// create a puplicate of page before publishing for editing
		if($insert!=false) $page_id = $this->insert_page($this->getId());
		
		// notify the editor and owner or this action and return success/failure
		return $this->notifyPublished($ret);
		
	}
	
	function notifyPublishedEmail(){
		require_once(DIR_WS_CLASSES."mail.php");
		require_once(DIR_WS_CLASSES."mail_plus.php");
		require_once(DIR_WS_CLASSES."users.php");
		
		$mail = new Mail;
		$mail->setFrom(SITE_NAME, ADMIN_EMAIL);
		$name = "";
		$user=new User;
		
		// get publisher details
		$pub = $user->getFields($this->getPublisherId());
		$ed = $user->getFields($this->getEditorId());
		$own = $user->getFields($this->getOwnerId());
		
		// get common email array
		$published_email_array = array(
				"title"						=> $this->getTitle(),
				"publisher_name"	=> $pub['firstname'].' '.$pub['lastname'],
				"published_date"	=> db_putdate($this->getDatePublished(),false,'l, F j Y \a\t h:i a'),
				"published_url"		=> DIR_WS_PUBLIC.$this->getPageType().".php?id=".$this->getId()
		);

		if($this->email_owner && $own!=false){
			$ret = $mail->emsgSend("published","owner",$own,$published_email_array+array("name"=>$own['firstname'].' '.$own['lastname']));
		}
		if($this->email_site_owner && defined('SITE_OWNER') && defined('SITE_OWNER_EMAIL')){
			$ret = $mail->emsgSend("published","site_owner",array('firstname'=>SITE_OWNER,'lastname'=>'','email'=>SITE_OWNER_EMAIL),$published_email_array+array("name"=> SITE_OWNER));
		}
		if($this->email_editor && $ed!=false){
			$ret = $mail->emsgSend("published","editor",$ed,$published_email_array+array("name"=>$ed['firstname'].' '.$ed['lastname']));
		}
		// leave publisher til last because we want to keep the user details
		if($this->email_publisher && $pub!=false) {
			if($ret!=false)
				$published_message = " and ".$ed['firstname'].' '.$ed['lastname'].' has been notified';
			else
				$published_message = ", but there was an error sending notification to one of ".$own['email'].",".SITE_OWNER_EMAIL." recipients.";
			$ret = $mail->emsgSend("published","publisher",$email,$published_email_array+array("published_message"	=> $published_message, "name"=>$email['firstname'].' '.$email['lastname']));
		}
		
		return $ret;

	}
	
	function notifyPublished($ret){
		if($this->notify_published){
			if($ret!=false)
				$ret = $this->notifyPublishedEmail();
		}
		return $ret;
	}
	  
	/*
	+---------------------------------------------------------------------------
	|   Deletes a node from the database
	+---------------------------------------------------------------------------
	*/
	function delete($id=0)
	{
		if($id == 0)
			$id = $this->getId();
		db_connect();
		// get page id
		$page_id = $this->getPageId();
		db_query("DELETE FROM ".TABLE_NODE_REF." WHERE `node_id`='$id'") or die(db_error());
		db_query("DELETE FROM ".TABLE_PAGE." WHERE `id`='$page_id'") or die(db_error());
		db_query("DELETE FROM ".TABLE_NODE." WHERE `id`='$id'") or die(db_error());
		db_close();
		$this = NULL;
		$ret = true;
		return $ret;
	}
	
/*
+---------------------------------------------------------------------------
|   Database Queries
+---------------------------------------------------------------------------
*/

	
	function isParentProtected()
	{
		return false;
	}

/*
+---------------------------------------------------------------------------
|   Legacy Functionality
|   We will want to reimplement these at some stage
+---------------------------------------------------------------------------
*/
	
	// Does this id have any children - returns true or false
	function hasChildren($id=0)
	{
		if($id == 0)
			$id = $this->getId();
		db_connect();
		//$result = db_query("SELECT * FROM ".TABLE_PAGE." WHERE `pid`='$id'") or die(db_error());
		$result = db_query("SELECT * FROM ".TABLE_NODE." WHERE `pid`='$id'") or die(db_error());
		if(db_num_rows($result) > 0)
			$ret = true;
		else
			$ret = false;
		db_close();
		return $ret;
	}

	// Returns the number of children for a given parent id
	function numChildren($order='sort_order ASC')
	{
		$pid = $this->getId();
		db_connect();
		$result = db_query("SELECT * FROM ".TABLE_NODE." WHERE `pid`='$pid' ORDER BY $order") or die("Database Error in Container::numChildren | ".db_error());
		$num = db_num_rows($result);
		db_close();
		return $num;
	}

	// Returns a mysql result resource of the children of a given parent id, limited to $offset,$limit
	function getChildrenLimited($offset, $limit, $order='n.id ASC')
	{
		$pid = $this->getId();
		db_connect();
		$result = db_query($this->getChildrenLimitedSQL($pid, '', $order)." LIMIT $offset,$limit") or die("Database Error in Container::getChildrenLimited | ".db_error());
		$idArray = array();
		echo("<br><b>getChildrenLimited results</b>=");print_r($result);
		while ($row = db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
		db_close();
		return $idArray;
	}
	function getChildrenLimitedSQL($pid, $filter='', $order='n.id ASC')
	{
		if($filter!='') $filter=' and ('.$filter.')';
		return "select * from ".TABLE_NODE." n, ".TABLE_NODE_REF." r where n.id=r.node_id and n.pid='$pid' $filter order by $order ";
	}
	
	// Returns a mysql result resource from $type, limited to $offset,$limit an ordered by $order
	function getLimited($offset, $limit, $type, $order, $filter = '')
	{
		db_connect();
		$result = db_query($this->getLimitedSQL($type, $order, $filter)." LIMIT $offset,$limit ") or die(db_error());
		$idArray = array();
		echo("<br><b>getLimited results</b>=");print_r($result);
		while ($row = db_fetch_array($result))
		{
			$idArray[] = $row['id'];
		}
		db_close();
		return $idArray;
	}
	function getLimitedSQL($type, $order, $filter='')
	{
		if(!empty($filter)) $filter = " and ".$filter;
		return "select n.id FROM ".TABLE_NODE." n, ".TABLE_PAGE." p, ".TABLE_NODE_REF." r where n.id=r.node_id and p.id=r.page_id and p.type='$type' and n.pid!=0 $filter order by $order ";
	}
	
/*
+---------------------------------------------------------------------------
|   Accessors / Mutators
+---------------------------------------------------------------------------
*/
	
	function setError($val)
	{
		$this->error = $val;
	}
	
	function getError()
	{
		return $this->error;
	}
	
	function setId($val)
	{
		$this->id = $val;
	}
	
	function getId()
	{
		return $this->id;
	}
	
	function setPid($val)
	{
		$this->pid = $val;
	}
	
	function getPid()
	{
		return $this->pid;
	}
		
	function setOwnerId($val)
	{
		$this->owner_id = $val;
	}
	
	function getOwnerId()
	{
		return $this->owner_id;
	}
	
	function setEditorId($val)
	{
		$this->editor_id = $val;
	}
	
	function getEditorId()
	{
		return $this->editor_id;
	}
	
	function setPublisherId($val)
	{
		$this->publisher_id = $val;
	}
	
	function getPublisherId()
	{
		return $this->publisher_id;
	}

	// set and get all email notification fields
	function setNotifyPublish($val='')
	{
		$this->notify_published = ($val=='')?EMAIL_NOTIFY_ON_PUBLISH:$val;
	}
	
	function getNotifyPublish()
	{
		return $this->notify_published;
	}
	
	function setEmailOwner($val='')
	{
		$this->email_owner = ($val=='')?EMAIL_NOTIFY_OWNER_ON:$val;
	}
	
	function getEmailOwner()
	{
		return $this->email_owner;
	}
	
	function setEmailSiteOwner($val='')
	{
		$this->email_site_owner = ($val=='')?EMAIL_NOTIFY_SITE_OWNER_ON:$val;
	}
	
	function getEmailSiteOwner()
	{
		return $this->email_site_owner;
	}
	
	function setEmailPublisher($val='')
	{
		$this->email_publisher = ($val=='')?EMAIL_NOTIFY_PUBLISHER_ON:$val;
	}
	
	function getEmailPublisher()
	{
		return $this->email_publisher;
	}
	
	function setEmailEditor($val='')
	{
		$this->email_editor = ($val=='')?EMAIL_NOTIFY_EDITOR_ON:$val;
	}
	
	function getEmailEditor()
	{
		return $this->email_editor;
	}
		
	function setDatePublished($val)
	{
		$this->date_published = $val;
	}
	function getDatePublished()
	{
		return $this->date_published;
	}
	
	function isPublished(){
		$field="p.date_published";
		$id=$this->getId();
		$sql="select $field from ".TABLE_PAGE." p , ".TABLE_NODE_REF." r where p.id=r.page_id and r.node_id='$id'";
		$ret = db_getdate(db_getfieldvalue($sql,$field));
		echo("DATE_PUBLISHED=".$ret.':'.$sql);
		return !empty($ret);
	}
	
	function setGroupSecurity($val)
	{
		$val = (is_array($val)) ? getMaskFromList( implode(",", $val) ) : $val ;
		$this->groups = $val;//is_array($val)?implode(",",$val):"";
	}
	
	function getGroupSecurity()
	{
		return $this->groups;
	}
	
	function setRoleSecurity($val)
	{
		$val = (is_array($val)) ? getMaskFromList( implode(",", $val) ) : $val ;
		$this->roles = $val;//is_array($val)?implode(",",$val):"";
		
	}
	
	function getRoleSecurity()
	{
		return $this->roles;
	}
	
	function setStatus($val)
	{
		$this->status = $val;
	}
	
	function getStatus()
	{
		return $this->status;
	}
}
?>
