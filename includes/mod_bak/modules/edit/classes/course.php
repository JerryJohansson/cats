<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS - Course Definition extends Container
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/

class Course extends Container
{

	function Course($id=0)
	{
		parent::Container($id,'course');
		$this->setPagetype("course");
		
		if($id == 0)
		{
			$this->setTarget("");
			$this->setAssessment("");
			$this->setPreRequisites("");
			$this->setObjectives("");
			$this->setRelatedCourses("");
			$this->setPrice("");
			$this->setDuration("");
			$this->setCourseType("");
			$this->setOutcome("");
		}
	}

/*
+---------------------------------------------------------------------------
|   Accessors / Mutators
+---------------------------------------------------------------------------
*/
   
	function setProperties($POST=''){
		$properties=$this->getAllPropertyNames();
		$funcs=$this->getAllPropertySetFunctions();
		parent::setProperties($POST, $properties, $funcs);
	}
	
	function getAllPropertyNames(){
		return array_merge($this->getPropertyNames(),parent::getPropertyNames());
	}
	
	function getPropertyNames(){
		return array('price','target','duration','outcome','course_type','objectives','assessment','related_courses','pre_requisites');
	}
	function getPropertyDisplayFunctions($admin = true){
		if($admin)
			return array('getPrice','getTarget','getDuration','getOutcome','getCourseType','getObjectives','getAssessment','getRelatedCoursesChooser','getPreRequisites');
		else
			return array('getPrice','getTarget','getDuration','getOutcome','getCourseType','getObjectives','getAssessment','getRelatedCourses','getPreRequisites');
	}
	
	function getAllPropertySetFunctions(){
		return array_merge($this->getPropertySetFunctions(),parent::getPropertySetFunctions());
	}
	function getPropertySetFunctions(){
		return array('','','','','','','','','');
	}
	
	function postAdd()
	{
		$this->setProperties();
		//$properties=serialize($this->properties);
		$insert_id = parent::postAdd();
		return $insert_id;
	}
	
	function postEdit()
	{
		$this->setProperties();
		//$properties=serialize($this->properties);

		parent::postEdit(false);
		$this->update();
	}

/*
+---------------------------------------------------------------------------
|   Editor Extra Field Display
+---------------------------------------------------------------------------
*/
	function getExtraAdd($template='course_extras')
	{
		return parent::getExtraAdd($template,$this->getPropertyNames(),$this->getPropertyDisplayFunctions(true));
	}

	function getExtraEdit($template='course_extras'){
		return parent::getExtraEdit($template,$this->getPropertyNames(),$this->getPropertyDisplayFunctions(true));
	}

	function getAddItem($template='additem_course')
	{
		return parent::getAddItem($template,$this->getPropertyNames(),$this->getPropertyDisplayFunctions(false));
	}
/*--------------------------------------------------------------------------*/	
	

	
	function setPreRequisites($val)
	{
		$this->properties['pre_requisites']=$val;
	}
	
	
	function getPreRequisitesDisplay($val){
		return $this->getPreRequisites($val,true);
	}
	function getPreRequisites($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['pre_requisites'];
		if($display){
			if(!empty($val)){
				$val=$this->getPropertyListItems($val);
			}
		}
		return ($display)?(!empty($val)?$this->getDisplayRow("Pre-Requisites",$val):""):$val;
	}
	
	function getPropertyListItems($val=''){
		$tmp=explode("|",$val);
		$val="";
		$i=0;
		$size=count($tmp);
		$tag="span";
		while($i<$size){
			if(!empty($tmp[$i]))$val.="<$tag>".$tmp[$i]."</$tag>";
			$i++;
		}
		//if(!empty($val)) $val="<ul>".$val."</ul>";
		return $val;
	}
	
	function setRelatedCourses($val)
	{
		$this->properties['related_courses']=$val;
	}
	
	function getRelatedCoursesDisplay($val){
		return $this->getRelatedCourses($val,true);
	}
	function getRelatedCourses($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['related_courses'];
		return ($display)?(!empty($val)?$this->getDisplayRow("Related Courses",$this->getRelatedCoursesList($val)):""):$val;
	}
	
	function setAssessment($val)
	{
		$this->properties['assessment']=$val;
	}
	
	function getAssessmentDisplay($val){
		return $this->getAssessment($val,true);
	}
	function getAssessment($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['assessment'];
		if($display){
			if(!empty($val)){
				$val=$this->getPropertyListItems($val);
			}
		}
		return ($display)?(!empty($val)?$this->getDisplayRow("Assessment",$val):""):$val;;
		//return $this->properties->getAssessment();
	}
	
	function setObjectives($val)
	{
		$this->properties['objectives']=$val;
	}
	
	function getObjectivesDisplay($val){
		return $this->getObjectives($val,true);
	}
	function getObjectives($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['objectives'];
		if($display){
			if(!empty($val)){
				$val=$this->getPropertyListItems($val);
			}
		}
		return ($display)?(!empty($val)?$this->getDisplayRow("Objectives",$val):""):$val;;
		//return $this->properties->getObjectives();
	}
	
	function setCourseType($val)
	{
		$this->properties['course_type']=$val;
	}
	
	function getCourseTypeDisplay($val){
		return $this->getCourseType($val,true);
	}
	function getCourseType($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['course_type'];
		return ($display)?(!empty($val)?$this->getDisplayRow("Delivery Style",$val):""):$val;;
		//return $this->properties->getCourseType();
	}
	
	function setOutcome($val)
	{
		$this->properties['outcome']=$val;
	}
	
	
	function getOutcomeDisplay($val){
		return $this->getOutcome($val,true);
	}
	function getOutcome($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['outcome'];
		return ($display)?(!empty($val)?$this->getDisplayRow("Training Outcome",$val):""):$val;
		//return $this->properties->getOutcome();
	}
	
	function setDuration($val)
	{
		$this->properties['duration']=$val;
	}

	function getDuration($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['duration'];
		return ($display)?(!empty($val)?$this->getDisplayRow("Course Duration",$val):""):$val;
		//return $this->properties->getDuration();
	}
	
	function setPrice($val)
	{
		$this->properties['price']=$val;
	}

	function getPrice($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['price'];
		return ($display)?(!empty($val)?$this->getDisplayRow("Price",$val):""):$val;
		//return $this->properties->getPrice();
	}
	
	function setTarget($val)
	{
		$this->properties['target']=$val;
	}

	function getTarget($val='',$display=false)
	{
		$val=($val!='')?$val:$this->properties['target'];
		if($display){
			if(!empty($val)){
				$val=$this->getPropertyListItems($val);
			}
		}
		return ($display)?(!empty($val)?$this->getDisplayRow("Target Audience",$val):""):$val;;
		//return $this->properties->getTarget();
	}
	
	function getDisplayRow($heading,$val){
		return '<div class="course_features"><b>'.$heading.':</b></div><div class="course_features">'.$val.'</div>';
	}

	function getDelimUrl(){
		return '|';
	}
	
	function getRelatedCoursesListDisplay($val=''){
		return $this->getRelatedCoursesCBO($val,false);
	}
	function getRelatedCoursesList($val=''){
		return $this->getRelatedCoursesCBO($val,false);
	}
	function getRelatedCoursesChooser($val=''){
		return $this->getRelatedCoursesCBO($val);
	}
	function getRelatedCoursesCBO($url_string="", $admin = true){
		// name of input field
		$related_field = "related_courses";
		if($admin){
			$s='
			<div id="add_'.$related_field.'" title="'.$url_string.'">';
		}else{
			$s='';
		}
		if(!empty($url_string)){
			ezw_db_connect();
			// get standard delimeter
			$delim = $this->getDelimUrl();
			
			// check if we have digits in the list
			if(preg_match("/^\d{1,5}/",$url_string)){
				$urls = explode($delim,$url_string);
			}else{
				$url_string = ezw_db_getfieldvalue("select n.id, n.name, n.url, t.name as type from node n, node_type t where n.node_type=t.id and n.name like '$url_string%' ");
			}
			// check the url_string again incase the fail safe returns nothing
			if(!empty($url_string)){
				// create array of urls and strip out duplicated items if any
				$urls=explode($delim, $url_string);
				$urls_array=array();
				$len=count($urls);
				for($i=0;$i<$len;$i++){
					if(!in_array($urls[$i], $urls_array)) $urls_array[]=$urls[$i];
				}
				// transfer the clean url_string array to a comma delimited list for in() clause
				$ids_in_string=implode(",",$urls_array);
				
				// retrieve related courses url properties
				$filter = " and n.id in($ids_in_string) ";
				$result = ezw_db_query("select n.id, n.name, n.url, t.name as type from node n, node_type t where n.node_type=t.id $filter") or die(ezw_db_error());
				while ($row = ezw_db_fetch_array($result))
				{
					// create the url...if we have a url type then use the url otherwise build url using node properties
					$url=(empty($row['url']))?$row['type'].'.php?id='.$row['id']:$row['url'];
					$value=$row['name'];
					// display admin links or straight hyperlinks
					if($admin){
						//<!--input type="checkbox" name="hyperlink[]" value="'.$row['id'].'" /-->&nbsp;
						$s .= '
					<a href="'.DIR_WS_PUBLIC.$url.'" target="url_preview">'.$value.'</a><br />';
					}else{
						$s .= '
					<a href="'.DIR_WS_PUBLIC.$url.'">'.$value.'</a>';
					}
				}
				ezw_db_close();
			}
		}
		if($admin){
			$s .= '
			</div>
			<input class="sfield" type="hidden" name="'.$related_field.'" id="id_'.$related_field.'" value="'.$url_string.'"><br />
			<input type="button" value="Add/Remove Hyperlink" title="Click here if you wish to add more hyperlinks to the list" onclick="return addHyperlinks(this.form.'.$related_field.', \'add_'.$related_field.'\', \'checkbox\')" />';
		}else{
			//if($s!='') $s = '<ul>'.$s.'</ul>';
		}
		return $s;
	}
/*
+---------------------------------------------------------------------------
|   ezwebmaker Container Generators
+---------------------------------------------------------------------------
*/

	function endRowCheck($pagecount, $numimages)
	{
		$thumb_cols = (defined('SEARCH_THUMB_COLS')) ? (int)SEARCH_THUMB_COLS : (int)DEFAULT_THUMB_COLS ;
		if($numimages==$pagecount){
			$colspan = $thumb_cols - $pagecount % $thumb_cols;
			return '<td colspan="$colspan"></td></tr>';
		}
		else
		{
			return '';
		}
	}
	
	function buildThumb()
	{
		if(defined('COURSE_THUMB_GEN') && COURSE_THUMB_GEN == 'true')
		{
			// Do a quick check to see if a thumbnail is set and exists
			if($this->getThumb() != '' && file_exists(DIR_FS_THUMBS.$this->getThumb()))
				$thumb = '<img src="'.DIR_WS_THUMBS.$this->getThumb().'" alt="'.$this->getTitle().'" title="'.$this->getTitle().'" border="0">';
			else
				$thumb = '<img src="'.DIR_WS_THUMBS.IMG_DEFAULT_PRODUCT_THUMB.'" alt="'.TXT_DEFAULT_PRODUCT_THUMB.'" title="'.TXT_DEFAULT_PRODUCT_THUMB.'" border="0">';
		}
		else
		{
			$thumb = '';
		}
		return $thumb;
	}


	function getCategoryThumbs($pagenum)
	{
		require_once(DIR_WS_CLASSES."template.php");
		$tpl       = new Template("templates/course_category_thumbs.htm");
		$parent    = new Course;
		$child     = new Course;
		// const to vars
		$thumb_cols = (defined('COURSE_THUMB_COLS')) ? (int)COURSE_THUMB_COLS : (int)DEFAULT_THUMB_COLS ;
		$thumb_rows = (defined('COURSE_THUMB_ROWS')) ? (int)COURSE_THUMB_ROWS : (int)DEFAULT_THUMB_ROWS ;
		$sum_length = (defined('COURSE_SUMMARY_LENGTH')) ? (int)COURSE_SUMMARY_LENGTH : (int)DEFAULT_SUMMARY_LENGTH ;

		// Level maths
		$level = $this->getLevel();

		$rows = '';
		$pagelinks = '';
		// Set the counter to keep track of which page we are upto
		$prodcount=1;
		$cells=$thumb_cols * $thumb_rows;
		// Number of thumbs allowed per page
		$offset=$cells * ($pagenum-1);
		// Loop through each page and fill the loop
		$children = $this->getChildrenLimited($offset, $cells);
		$totalnumpages = $this->numChildren();
		$numpages = sizeof($children);

		foreach($children as $childId)
		{
			$child->load($childId);
			$thumb = $child->buildThumb();

			// Check if we want a summary with the title
			if(defined('COURSE_SUMMARY_ON') && COURSE_SUMMARY_ON == 'true')
				$summary = substr( stripslashes( strip_tags( $child->getContent() ) ), 0, $sum_length );
			else
				$summary = '';

			// If we are at the base id, links will be to categories, not courses
			if(($prod_num_levels == 2 && $level==0) || ($prod_num_levels == 3 && ($level==0 || $level==1)))
			{
				$link = DIR_WS_PUBLIC.get_class($this).'.php?id='.$child->getId();
			}
			else
				$link = DIR_WS_PUBLIC.get_class($this).'.php?id='.$child->getId();

			/*********** Do Thumbnail Display ************/
			if($prodcount % ($thumb_cols) == 1){ // First <td> in the row
				$rows .= '<tr valign="bottom"><td valign="bottom" align="center"><a href="'.$link.'">'.$thumb.'<br>'.$child->getTitle().'</a></td>';
				$rows .= $this->endRowCheck($prodcount, $numimages);
			}elseif($prodcount % ($thumb_cols) == 0){ // End of the row
				$rows .= '<td valign="bottom" align="center"><a href="'.$link.'">'.$thumb.'<br>'.$child->getTitle().'</a></td></tr>';
			}else{ // Middle <td>
				$rows .= '<td valign="bottom" align="center"><a href="'.$link.'">'.$thumb.'<br>'.$child->getTitle().'</a></td>';
				$rows .= $this->endRowCheck($prodcount, $numimages);
			}
			/*********** Do Thumbnail Display ************/
			$prodcount++;
		}

		/********** Next Container Link Generation *************/
		if(($totalnumpages/$cells) > 1)
		{
				$previous=$pagenum-1;
				if($pagenum > 1)
					$pagelinks .= '<a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$previous.'">&lt;-- Previous</a>';
				$i=1;
				while($i < $pagenum)
				{
					$pagelinks .= '| <a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$i.'">'.$i.'</a>';
					$i++;
				}
				$pagelinks .= '| '.$pagenum.' ';
				$i++;
				while(($i-1) < ($totalnumpages/$cells))
				{
					$pagelinks .= '| <a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$i.'">'.$i.'</a>';
					$i++;
				}
				if($totalnumpages >= ($prodcount + $offset)){
					$next=$pagenum+1;
					$pagelinks .= '| <a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$next.'">Next--&gt;</a>';
				}
		}
		/********** Next Container Link Generation *************/

		$values = array(
			"rows"      => $rows,
			"title"     => $this->getTitle(),
			"content"   => $this->getContent(),
			"file"      => $this->getFileLink(),
			"pagelinks" => $pagelinks,
			"crumbs"    => $this->buildCrumbs()
		);
		$tpl->parse($values);
		return $tpl->template;
	}


	function getCategoryList($pagenum)
	{
		require_once(DIR_WS_CLASSES."template.php");
		$tpl       = new Template("templates/course_category_rows.htm");
		$parent    = new Course;
		$child     = new Course;
		$tpl->parse_loop("courseloop");
		
		// const to vars
		$num_rows = (defined('COURSE_NUM_ROWS')) ? (int)COURSE_NUM_ROWS : (int)DEFAULT_NUM_ROWS ;
		$sum_length = (defined('COURSE_SUMMARY_LENGTH')) ? (int)COURSE_SUMMARY_LENGTH : (int)DEFAULT_SUMMARY_LENGTH ;

		// Level maths
		$level = $this->getLevel();

		$rows = '';
		$pagelinks = '';
		// Set the counter to keep track of which page we are upto
		$prodcount=1;
		// Number of thumbs allowed per page
		$offset=$num_rows * ($pagenum-1);
		// Loop through each page and fill the loop
		$children = $this->getChildrenLimited($offset, $num_rows);
		$totalnumpages = $this->numChildren();
		$numpages = sizeof($children);

		foreach($children as $childId)
		{
			$child->load($childId);
			$thumb = $child->buildThumb();

			// Check if we want a summary with the title
			if(defined('COURSE_SUMMARY_ON') && COURSE_SUMMARY_ON == 'true')
				$summary = substr( stripslashes( strip_tags( $child->getContent() ) ), 0, $sum_length );
			else
				$summary = '';

			// If we are at the base id, links will be to categories, not courses
			if(($prod_num_levels == 2 && $level==0) || ($prod_num_levels == 3 && ($level==0 || $level==1)))
			{
				$link = DIR_WS_PUBLIC.get_class($this).'.php?id='.$child->getId();
			}
			else
			{
				$link = DIR_WS_PUBLIC.get_class($this).'.php?id='.$child->getId();
			}

			// Load up the array
			$values = array(
				"thumb"    => $thumb,
				"title"    => $child->getTitle(),
				"summary"  => $summary,
				"link"     => $link,
			);
			$rows .= $tpl->get_loop('courseloop', $values);
			$prodcount++;
		}


		/********** Next Container Link Generation *************/
		if(($totalnumpages/($num_rows)) > 1)
		{
			$previous=$pagenum-1;
			if($pagenum > 1)
				$pagelinks .= '<a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$previous.'">&lt;-- Previous</a>';
			$i=1;
			while($i < $pagenum)
			{
				$pagelinks .= '| <a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$i.'">'.$i.'</a>';
				$i++;
			}
			$pagelinks .= '| '.$pagenum.' ';
			$i++;
			while(($i-1) < ($totalnumpages/($num_rows)))
			{
				$pagelinks .= '| <a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$i.'">'.$i.'</a>';
				$i++;
			}
			if($totalnumpages >= ($prodcount + $offset)){
				$next=$pagenum+1;
				$pagelinks .= '| <a href="'.DIR_WS_PUBLIC.get_class($this).'.php?id='.$this->getId().'&page='.$next.'">Next--&gt;</a>';
			}
		}
		/********** Next Container Link Generation *************/

		$values = array(
			"rows"      => $rows,
			"title"     => $this->getTitle(),
			"content"   => $this->getContent(),
			"file"      => $this->getFileLink(),
			"pagelinks" => $pagelinks,
			"additem"   => $this->getAddItem()
		);
		$tpl->parse($values);

		
		return $tpl->template;
	}


	function getCourse()
	{
		require_once(DIR_WS_CLASSES."template.php");
		$tpl = new Template("templates/course.htm");

		// Check if we are shopping, if so - show the add item form
		$additem = $this->getAddItem();

		$values = array(
			"title"       => $this->getTitle(),
			"content"     => $this->getContent(),
			"file"        => $this->getFileLink(),
			"additem"     => $additem
		);
		$tpl->parse($values);
		return $tpl->template;
	}
	
	

	function get($pagenum)
	{
		if($this->hasChildren() == true)
		{
			if(defined('COURSE_RESULT_TYPE') && COURSE_RESULT_TYPE == "thumbs")
				$ret = $this->getCategoryThumbs($pagenum);
			else
				$ret = $this->getCategoryList($pagenum);
		}
		else
		{
			$ret = $this->getCourse($pagenum);
		}

		return $ret;
	}
}
?>