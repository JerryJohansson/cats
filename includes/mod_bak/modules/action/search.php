<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');

$search_filter = array(
	'name'=>'Search Actions Register',
	'filter'=>" lower(status) = 'open' and (MANAGED_BY_ID = 1 or ALLOCATED_TO_ID = 1) "
);
$editor=array();
$editor['names'] = array("ACTION_ID","REGISTER_ORIGIN","ACTION_TYPE","ACTION_TITLE","ACTION_DESCRIPTION","SCHEDULED_DATE","REMINDER_DATE","CHANGE_COMMENTS","MANAGED_BY_ID","ALLOCATED_TO_ID","NOTIFY_ALLOCATED_EMAIL","NOTIFY_ALLOCATED_PRINT","STATUS","CLOSING_DATE","CLOSED_BY","DATE_DUE_CHANGED_HISTORY","RECORD_LOCATION","RECORD_COMMENTS","ORIGIN_TABLE","REPORT_ID","SITE_ID","DEPARTMENT_ID","SECTION_ID","MANAGED_BY","M_SITE_ID","ALLOCATED_TO","A_SITE_ID","CLOSED_BY_NAME","C_SITE_ID","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","SECTION_DESCRIPTION","ACTIONCATEGORY");
$editor['columns'] = array("ACTION_ID","REGISTER_ORIGIN","ACTION_TYPE","ACTION_TITLE","ACTION_DESCRIPTION","SCHEDULED_DATE","REMINDER_DATE","CHANGE_COMMENTS","MANAGED_BY_ID","ALLOCATED_TO_ID","NOTIFY_ALLOCATED_EMAIL","NOTIFY_ALLOCATED_PRINT","STATUS","CLOSING_DATE","CLOSED_BY","DATE_DUE_CHANGED_HISTORY","RECORD_LOCATION","RECORD_COMMENTS","ORIGIN_TABLE","REPORT_ID","SITE_ID","DEPARTMENT_ID","SECTION_ID","MANAGED_BY","M_SITE_ID","ALLOCATED_TO","A_SITE_ID","CLOSED_BY_NAME","C_SITE_ID","SITE_DESCRIPTION","DEPARTMENT_DESCRIPTION","SECTION_DESCRIPTION","ACTIONCATEGORY");
$editor['tables'] = "VIEW_ACTION_DETAILS"; // can be an array but must match [columns] indexes
$editor['display_funcs'] = explode("|","")

?>
<link rel="stylesheet" href="style/forms.css" type="text/css" />
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script language="JavaScript" src="js/classes/menu/popup.js"></script>
<script language="JavaScript" src="js/classes/helper/helper.js"></script>
<script language="JavaScript" src="js/validation.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
document.onmousedown = function(evt){
	if(ACTIVE_HELPER && !_in(ACTIVE_HELPER.parentNode.parentNode,_target(evt))) ACTIVE_HELPER.style.display='none';
	if(top.doMenu) top.doMenu();
}
function _target(e){
	return (document.all)?event.srcElement:e.target;
}
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}

function init(){
	// start up script for each page
	//
	// Execute some start up script
	return true;
}


</script>
</head>
<body>
<form><table>
<tr valign="top">
	<td width="60%">		
	
		<!-- Actions Search Table -->
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();"><?php echo $search_filter['name'];?></a></caption>
		<tr>
			<td>
<?php
// Display workload review items

$employee_number = 90999;//$_SESSION['user_details']['user_id'];
$colattribs = array('',' width="60" ',' width="180" ',' width="150" ',' width="20" ');
$colarr=array('Action Title','Action No.','Scheduled Date','Action Origin','edit:index.php?m=action&p=edit&id=:ACTION_ID');
$sql="SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, 'edit:index.php?m=action&p=edit&id=:ACTION_ID' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id,Allocated_To_Id) ORDER BY Scheduled_Date ASC";
db_render_pager($sql,$colarr,$colattribs);
?>
			</td>
		</tr>
		</table>
		</fieldset>
		
	</td>
	<td width="40%">
<?php
// Message of the Day
$sql = "SELECT * FROM View_MOTD_Current WHERE MOTD_Type = 0 AND MOTD_Site LIKE '%" . $_SESSION['user_details']['site_id'] . "%'";
$rs = $db->Execute($sql);
if($rs->RecordCount()){
?>		
		<!-- Message Of The Day -->
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">Message of the Day</a></caption>
<?php
	// Message of the day body table
	$idx = 0;
	while ($arr = $rs->FetchRow()) {
		$link = (!empty($arr['MOTD_URL']))?"<div align=\"left\">Link:<a href=\"".$arr['MOTD_URL']."\" target=\"_blank\">".$arr['MOTD_SHORT']."</a></div>":"";
		$motd = (!empty($arr['MOTD_DESC']))?$arr['MOTD_DESC']:"No message today";
		$date = $arr['MOTD_STARTDATE'];
		$tr_hilite = ($idx % 2)?' bgcolor="#eeeeee" ':' bgcolor="$ffffff" ';
		$idx++;
?>
		<tr <?php echo $tr_hilite;?>>
			<td>
				<div align="justify"><?php echo $motd; ?></div>
				<?php echo $link;?>
				<div align="left"><?php echo $date;?></div>
			</td>
		</tr>
<?php
	} // END:: rs while loop
?>
		</table>
		</fieldset>
<?php
} // END:: rs->RecordCount()


// Links of Interest
$sql = "SELECT * FROM View_MOTD_Current WHERE MOTD_Type = 1 AND MOTD_Site LIKE '%" . $_SESSION['user_details']['site_id'] . "%'";
$rs = $db->Execute($sql);
if($rs->RecordCount()){
?>
		<!-- links of interest -->
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">Links of Interest</a></caption>
<?php
	// Links of Interest table body
	$idx = 0;
	while ($arr = $rs->FetchRow()) {
		$link = (!empty($arr['MOTD_URL']))?"<div align=\"left\">Link:<a href=\"".$arr['MOTD_URL']."\" target=\"_blank\">".$arr['MOTD_SHORT']."</a></div>":"";
		$motd = (!empty($arr['MOTD_DESC']))?$arr['MOTD_DESC']:"No links today";
		$date = $arr['MOTD_STARTDATE'];
		$tr_hilite = ($idx % 2)?' bgcolor="#eeeeee" ':' bgcolor="$ffffff" ';
		$idx++;
?>
		<tr <?php echo $tr_hilite;?>>
			<td>
				<?php echo $link;?>
				<div align="justify"><?php echo $motd; ?></div>
			</td>
		</tr>
<?php
	} // END:: rs while loop
?>
		</table>
		</fieldset>
<?php
} // END:: rs->RecordCount()
?>		
		<!-- User Details Table -->
		
		<fieldset class="tbar" id="right_panel">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();">My Details</a></caption>
		<tr>
			<td class="label"><label for="id_userid">Employee ID:</label></td>
			<td><?PHP echo $_SESSION['user_details']['user_id'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_username">Name:</label></td>
			<td><?PHP echo $_SESSION['user_details']['first_name']." ".$_SESSION['user_details']['last_name'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_email">Email:</label></td>
			<td><?PHP echo $_SESSION['user_details']['email'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_fname">Site:</label></td>
			<td><?PHP echo $_SESSION['user_details']['site_id'];?></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="tbar">
		<div align="right" class="bar">
			<a
			title="Edit My Account Details"
			href="index.php?m=employee&p=edit&id=<?php echo $_SESSION['user_details']['user_id'];?>"
			style="background-image:url(<?php echo WS_ICONS_PATH;?>nav_my_account.gif);float:right;">My Account</a>
		</div>
		</fieldset>
		
	</td>
</tr>
</table>
<iframe style="width:0px;height:0px" src="" id="ezw_buffer"></iframe>
<div id="html_content"></div>
</form>