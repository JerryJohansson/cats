<?PHP
require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');

$search_filter = array(
	'name'=>'Edit Actions',
	'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) ",
	'type' => "action"
);
$containerType = $search_filter['type'];
$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
$pid=isset($_REQUEST['pid'])?$_REQUEST['pid']:0;
$iTabs=0;

//$fields_raw = explode("|","Register_Origin|',none,''|Action_Type|',none,''|Action_Title|',none,''|Action_Description|',none,''|Scheduled_Date|',none,NULL|Reminder_Date|',none,NULL|Change_Comments|',none,''|Managed_By_Id|none,none,NULL|Allocated_To_Id|none,none,NULL|Department_Id|none,none,NULL|Site_Id|none,none,NULL|Notify_Allocated_Email|none,1,0|Notify_Allocated_Print|none,1,0|Status|',none,''|Closing_Date|',none,NULL|Closed_By|none,none,NULL|Record_Location|',none,''|Record_Comments|',none,''|Date_Due_Changed_History|',none,''|Section_Id|none,none,NULL");


/*Get the users Site Id from when they logged in and then check to see if this record belongs to their Site
If it doesn't then we kick them out.!*/
$user_site_id = $_SESSION['user_details']['site_id'];
$user_site_access = $_SESSION['user_details']['site_access'];

$sql = "SELECT * FROM VIEW_ACTION_DETAILS WHERE ACTION_ID = $id";
$actions = db_get_array($sql);
$site_id = $actions['SITE_ID'];
$closed_by_site_id = $actions['C_SITE_ID'];
$managed_by_site_id = $actions['M_SITE_ID'];
$allocated_to_site_id = $actions['A_SITE_ID'];
$action_access = false;
if(!empty($user_site_access)){
	$a_sites = explode("SITE_ID =",$user_site_access);
	foreach($a_sites as $site){
		$n = 0+$site;
		if($n == $site_id || $n == $closed_by_site_id || $n == $managed_by_site_id || $n == $allocated_to_site_id){
			$action_access = true;
			break;
		}
	}
}else{
	if($user_site_id == $site_id || $user_site_id == $closed_by_site_id || $user_site_id == $managed_by_site_id || $user_site_id == $allocated_to_site_id){
		$action_access = true;
	}
}
if($action_access == false){
	exit("Your do not have access to this action");
}

?>
<script type="text/javascript">
  _editor_url = "<?php echo dirname($_SERVER['SCRIPT_NAME']); ?>";
	_editor_src_url = "<?php echo WS_IMAGES_PATH; ?>";
	_editor_base_url = "<?PHP echo WS_PATH; ?>";
  _editor_lang = "en";
	var EDITOR_VALIDATE = "";
	function element(s){return document.getElementById(s);}
</script>
<link rel="stylesheet" href="style/forms.css" type="text/css" />
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script type="text/javascript" src="js/editor.js"></script>
<script language="JavaScript" src="js/classes/menu/popup.js"></script>
<script language="JavaScript" src="js/classes/helper/helper.js"></script>
<script language="JavaScript" src="js/validation.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
document.onmousedown = function(evt){
	if(ACTIVE_HELPER && !_in(ACTIVE_HELPER.parentNode.parentNode,_target(evt))) ACTIVE_HELPER.style.display='none';
	if(top.doMenu) top.doMenu();
}
function _target(e){
	return (document.all)?event.srcElement:e.target;
}
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}
//=====================================================================================================================
//
//=====================================================================================================================

var win=window;
function init(){
	// start up script for each page
	//
	// Execute some start up script
	initTabs();
	setFocus();
	document.onmousedown=function(){
		if(typeof(_hide_calendar)=="function") _hide_calendar();
	}
	setTimeout(_hide_message,1000);
	
	return true;
}

function printPage() { print(document); }


function _show_message(s){
	var o=element('loading_message');
	o.innerHTML='<table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2>'+s+'</h2></td></tr></table></td></tr></table>'
	//o.style.display='block';
	o.style.visibility = '';
	_show_hide_elements('select',false);
}
function _hide_message(){
	element('loading_message').style.visibility='hidden';
	_show_hide_elements('select',true);
}
function _show_hide_elements(tag_name, b_show){
	var sel=document.getElementsByTagName(tag_name);
	var i=0;
	var len=sel.length;
	var display = (b_show)?'':'none';
	for(i=0;i<len;i++){
		sel[i].style.display = display;
	}
}
function _preview_page(url){
	var win=window.open(url,"_preview_url","width=800, height=600, resizeable=1, scrollbars=1");
	win.focus();
}


function new<?PHP echo ucwords((string)$containerType);?>(){
	var f = document.forms.form;
	f.pid.value = f.id.value;
	f.id.value = "";
	f.action.value = "add";
	f.setAttribute("action","index.php?m=action&p=post");
	f.Submit.click();
}
function save<?PHP echo ucwords((string)$containerType);?>(){
	var f = document.forms.form;
	f.Submit.click();
}
function doCancel(){
	location.href='index.php'; // back to dashboard
}
function doDelete(){
	var f = document.forms.form;
	if(confirm("Are you sure you want to delete this page?\nWarning: Any children of this page will no longer be accessible!")){
		f.setAttribute("action","editor.php?action=del");
		f.Submit.click();
	}
}
if(!top.html_clipboard) top.html_clipboard=new html_clipboard();
function html_clipboard(){
	var args=arguments;
	this.clipboard = [];
	this.push = function(a){
		this.clipboard[this.clipboard.length] = a;
	}
	this.get = function(i){
		return this.clipboard[i];
	}
}
function copy(){
	var a=getFilesList();
	var s="";
	for(i=0;i<a.length;i++){
		a[i]="<a href=\""+a[i]+"\">"+a[i]+"</a>";
	}
	//alert(a.join("<br>"));
	top.html_clipboard.push(a);
}
function getFilesList(){
	var o=document.forms[0]["delfile[]"];
	var ln=[];
	if(o.length){
		for(i=0;i<o.length;i++){
			if(o[i].checked) ln[ln.length]=o[i].value;
		}
	}else ln[0]=o.value;
	return ln
}

var editor = null;

onload = function(){
	init();
}
</script>
<style type="text/css">
textarea { background-color: #fff; border: 1px solid #000000; width: 100% !important; }
a.indent { border: inset; background-color: #999999; font-weight:bold; }
</style>
<script src="/cats/js/validation.js" language="javascript1.2" type="text/javascript"></script>
</head>
<body class="tb">
<div id="loading_message" style="background-color:#CCCCCC;width:100%;height:100%;position:absolute;left:0px;top:0px;"><table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2 class="loading_message">Loading...</h2></td></tr></table></td></tr></table></div>
<form name="form" action="?m=action&p=post&a=<?PHP echo $action; ?>" method="POST" enctype="multipart/form-data">
<div id="tool_bar_c">
<fieldset class="tool_bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo ucwords((string)$containerType);?>"
	href="#" onClick="new<?PHP echo ucwords((string)$containerType);?>();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $containerType;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo ucwords((string)$containerType);?>"
	href="#" onClick="save<?PHP echo ucwords((string)$containerType);?>();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $containerType;?></a>
<a
	title="Cancel operation"
	href="#" onClick="doCancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Delete current <?PHP echo $containerType;?>"
	href="#" onClick="doDelete();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<a
	title="Refresh current <?PHP echo ucwords((string)$containerType);?> item"
	href="#" onClick="document.location.reload();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
<fieldset class="bar" id="tab_buttons">
<?php
$extras="1";
if($extras==''){
	$rs = db_query("select * from form_fields where field_pid=$id");
	$tab_buttons="";
	$tab_contents="";
	// loop through the tabs and create main tab buttons
	while($tab = $rs->FetchRow()){
		
		$name = $tab['FIELD_NAME'];
		// get a nice representation of the name e.g.
		// convert candy_apples_value -> CandyApplesValue
		$ucname = preg_replace("/\s/g","",ucwords(preg_replace("/_/g"," ",$name)));
		
		if($iTabs==0) $save_buttons = '<input type="submit" name="Submit" value="Save" onclick="return validateForm(this,EDITOR_VALIDATE);"><input type="button" name="Cancel" value="Cancel" onclick="doCancel();">';
		else $save_buttons = '<input type="button" name="Save'.$ucname.'" id="Editor" value="OK" onclick="showhide(this);">';
		$tab_buttons .= '
	<a
		title="'.$tab['LABEL'].' Details"
		id="Extras" class="indent"
		href="#" onclick="return tab_onclick(this);" 
		style="background-image: url(' . WS_STYLE_PATH . 'images/icons/' . strtolower($tab['NAME']) .'gif);">'.$tab['NAME'].'</a>';
		$tab_contents .= '
	<fieldset class="tbar" id="tab_panel['.$iTabs.']">
	<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
	'.$container->getTab($tab['TAB']).'
	</table>
	<fieldset class="tbar" style="text-align:right; ">'.$save_bttons.'
	</fieldset>
	</fieldset>
	';
		$iTabs++;
	}
	echo($tab_buttons.$tab_contents);
}else{
?>
<a
	title="Edit <?PHP echo $containerType;?> Details"
	id="Editor" class="indent"
	href="#" onClick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH . 'images/icons/' . strtolower($containerType); ?>.gif);"><?PHP echo $containerType;?> Properties</a>
<a
	title="View Originating Records"
	id="Originating" class="indent"
	href="#" onClick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">Originating Documents</a>
<a
	title="View Follow-up Actions"
	id="Followup" class="indent"
	href="#" onClick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">Followup Actions</a>
</fieldset>
</div>

<div id="Lcontentbody" style="padding-top:60px;">

<!-- Edit Main Fields -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<?php
require_once(CATS_CLASSES_PATH . 'editor.php');
require_once('classes/editor_actions.php');

//require_once(CATS_CLASSES_PATH . 'editor.php');
$ed=new Editor("edit","admin");
$ed->drawForm($ed->loadProperties("select * from ".$TABLES['edit_view']['name']." where ".$TABLES['edit']['id']."=$id",$TABLES["edit"],$FIELDS));
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="Submit" value="Save" onClick="return validateForm(this,EDITOR_VALIDATE);"><input type="button" name="Cancel" value="Cancel" onClick="doCancel();">
</fieldset>		
</fieldset>

<!-- Originiating Documents -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
<caption id="tog"><a name="todo" onClick="this.blur();"><?php echo $search_filter['name'];?></a></caption>
<tr>
	<td>
<?php
// Originiating Documents

$employee_number = 90999;//$_SESSION['user_details']['user_id'];
$colattribs = array('',' width="60" ',' width="180" ',' width="150" ',' width="20" ');
$colarr=array('Action Title','Action No.','Scheduled Date','Action Origin','edit:index.php?m=action&p=edit&id=:ACTION_ID');
$sql="SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, 'edit:index.php?m=action&p=edit&id=:ACTION_ID' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id,Allocated_To_Id) ORDER BY Scheduled_Date ASC";
db_render_pager($sql,$colarr,$colattribs,'',false,'','',1000);
?>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>

<!-- Followup Actions -->

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="<?php echo(WS_STYLE_PATH); ?>images/forms/info.gif" /></td>
			<td>
				<p>Enter help information and documentation for the user guide:</p>
				<ul>
					<li><b>Enter Help:</b> Describe the item and its purpose breifly.</li>
					<li><b>Enter User Guide :</b> Describe the item in detail making sure describe it's relationship with other items etc.</li>
				</ul>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label>Field Help</label></td><td><textarea class="sfield" name="FIELD_HELP"></textarea></td>
</tr>
<tr>
	<td class="label"><label>Field Help</label></td><td><textarea class="sfield" name="FIELD_USER_GUIDE"></textarea></td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onClick="showhide(this);">
</fieldset>
</fieldset>
<?php
} // fi extras=='';
?>
</form>
</div>

<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>
