<?php
function getPropertiesTables(){
	return array(
		"edit"=> array("name"=>"TBLACTION_DETAILS", "id"=>"ACTION_ID"),
		"search"=> array("name"=>"VIEW_ACTION_DETAILS","id"=>"ACTION_ID"),
		"edit_view"=> array("name"=>"VIEW_ACTION_DETAILS","id"=>"ACTION_ID")
	);
}
function getPropertiesFields(){
	return array(
		'REGISTER_ORIGIN' => array('label'=>'Register Origin', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'ACTION_TYPE' => array('label'=>'Action Type', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'ACTION_TITLE' => array('label'=>'Action Title', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'ACTION_DESCRIPTION' => array('label'=>'Action Description', 'value'=>'', 'func'=>'html_draw_textarea_field', 'col'=>1, 'col_span'=>2), 
		'SCHEDULED_DATE' => array('label'=>'Scheduled Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
		'REMINDER_DATE' => array('label'=>'Reminder Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
		'CHANGE_COMMENTS' => array('label'=>'Change Comments', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'MANAGED_BY_ID' => array('label'=>'Managed By Id', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
		'ALLOCATED_TO_ID' => array('label'=>'Allocated To Id', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
		'DEPARTMENT_ID' => array('label'=>'Department Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
		'SITE_ID' => array('label'=>'Site Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2), 
		'NOTIFY_ALLOCATED_EMAIL' => array('label'=>'Notify Allocated Email', 'value'=>0, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'NOTIFY_ALLOCATED_PRINT' => array('label'=>'Notify Allocated Print', 'value'=>0, 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'STATUS' => array('label'=>'Status', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'CLOSING_DATE' => array('label'=>'Closing Date', 'value'=>NULL, 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
		'CLOSED_BY' => array('label'=>'Closed By', 'value'=>NULL, 'func'=>'html_draw_employee_helper', 'col'=>1, 'col_span'=>2), 
		'RECORD_LOCATION' => array('label'=>'Record Location', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'RECORD_COMMENTS' => array('label'=>'Record Comments', 'value'=>'', 'func'=>'html_draw_input_field', 'col'=>1, 'col_span'=>2), 
		'DATE_DUE_CHANGED_HISTORY' => array('label'=>'Date Due Changed History', 'value'=>'', 'func'=>'html_get_calendar_date_field', 'col'=>1, 'col_span'=>2), 
		'SECTION_ID' => array('label'=>'Section Id', 'value'=>NULL, 'func'=>'html_form_show_hidden', 'col'=>1, 'col_span'=>2)
	);
}
$TABLES=getPropertiesTables();
$FIELDS=getPropertiesFields();
?>