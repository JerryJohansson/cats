<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
//require_once ('../../conf.php');
require_once (CATS_CLASSES_PATH . 'editor.php');

if(isset($a) && !empty($a)) $action = $a;
$ret = true;
switch($action){
	case "add": // Add record
		$editor = new Editor($action,"admin");
		$id = $editor->insert($id);
		$ret = true;
		break;
	case "edit": // Update node
		$editor = new Editor($action,"admin");
		$id = $editor->update($id);
		$ret = true;
		break;
	default:
		$ret = false;
		break;
}

if($ret) {
	main_redirect("index.php?m=admin&p=editor&a=edit&id=$id");
} else {
	//show_error();
	dprint(__FILE__,__LINE__,0,"Error occured while editing");
}

$db=db_close();

?>