<?php
$_SESSION['USER_GROUP'] = 1+2+4+8+16+32+46+128+256+512+1024+2048+4096;
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style/default.css">
<link rel="stylesheet" type="text/css" href="style/toolbars.css">
<?php if ($_SESSION['msie']) {?>
<link href="style/css.php" rel="stylesheet" type="text/css" title="Extra Cool" id="main" />
<?php } ?>
<script language="JavaScript" src="js/constants.php"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/forms.js"></script>
<script language="JavaScript" src="js/classes/html/html.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<style type="text/css">
div.block, div.step
{
    display: block;
    clear: both;
    padding: 0px;
    margin-top: 0.5ex;
    margin-bottom: 0.5ex;
}
div.step
{
	background-color: #f0f0f0;
	margin: 0ex;
	border-bottom: dashed 2px #808080;
}

div.buttonblock
{
	margin-top: 1ex;
	margin-bottom: 1ex;
	text-align:center;
}
input.halfbox {
	width: 230px;
}

.block label
{
	font-weight: bold;
	padding-right: 1ex;
	white-space: nowrap;
	display: block;
}
table.box {
	border: 1px dashed #666;
}

</style>
<script>
//================================================================================//
//	XML Requests/Responses
//================================================================================//
var sendObj=[
['name','Vernon Laskey'],
['address','11/20 Queens Cr'],
['email','vern@buzmedia.com.au'],
['elements',[['div[0]','tag is a div'],['text[0]','node: is a textnode'],['div[1]','tag is a div 2'],['text[1]','node: is a textnode 2']]]
]
function alertMe(s){alert(s);return s;}
var oXML=null;

function _login(f){
	var url = "users.php";
	var s="";
	var i=0;
	for(i=0;i<f.length;i++){
			s+=""+f[i].name+"="+escape(f[i].value)+"&";
	}
	oXML=_HTTPRequest(url,"post",return_login,s.replace(/&$/,""));
//alert(oXML);
		//var s = "";
		//for(e in return_value) if(typeof return_value[e] != "function") s+=e+":"+return_value[e]+"\n";
		//alert(s)
	
	//if(typeof(data['redirect'])!='undefined') top.location.href = data['redirect'];
	return false;
}
</script>
</head>
<body bgcolor=#ffffff leftmargin=0 topmargin=0 marginwidth="0" marginheight="0" class="login">
<table id="mainHeader" cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
<tr valign="middle">
	<td>
		<table width="350" border="0" align="center" cellpadding="0" cellspacing="0" class="box">
		<tr>
			<td align="center" valign="middle">
				<form method="POST" onSubmit="return _login(this);">
		
				<div class="maincontentheader">
					 <h4>Welcome to CATS</h4>
					 <p>Please enter a valid login and password.</p>
					 <p><!-- message --></p>
				</div>
				<table>
				<tr valign="top">
					<td><img src="icons/login.gif" alt=""/></td>
					<td>
						<div class="block">
						<label for="id1">Login</label>
						<input class="halfbox" type="text" size="10" name="uid" id="id1" value="" />
						</div>
						
						<div class="block">
						<label for="id2">Password</label>
						<input class="halfbox" type="password" size="10" name="pwd" id="id2" value="" />
						</div>
						
						<div class="buttonblock">
						<input type="submit" name="action" value="Login" />
						</div>
					</td>
				</tr>
				</table>
				
				<input type="hidden" name="rurl" value="<!-- redirect -->" />
				
				</form>	
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="copyright" height="40">Copyright	&copy; 2005 TiWest Joint Venture. All rights reserved.</td>
</tr>
</table>

</body>
</html>
