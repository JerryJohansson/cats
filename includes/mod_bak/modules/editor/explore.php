<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
error_reporting  (E_ERROR | E_WARNING | E_PARSE);
require_once ("../includes/config.php");
authenticate('admin');
require(DIR_WS_CLASSES."template.php");
require(DIR_WS_CLASSES."navigation.php");
$menu = new Navigation("tree", "admin");

$action = $_REQUEST['action'];
$dir = $_REQUEST['dir'];
$mode = $_REQUEST['mode'];
$id_list = $_REQUEST['id_list'];
$id = $_REQUEST['id'];
$root = $_REQUEST['root'];

if($action=="move") // Move node
{
	$menu->reorder($id,($action.$dir));
	header("Location: explore.php");
}
elseif($action=="paste")
{
	$menu->paste($mode,$id_list,$id);
	header("Location: explore.php");
}
elseif($action=="delete")
{
	$menu->delete($id);
	header("Location: explore.php");
}
elseif($action=="import")
{
	$menu->importNavigation();
	header("Location: explore.php");
}
if(!isset($root)) $root = 0;
$menu->setMenus($root);
$tpl = new Template("templates/explore.htm");
$tpl->parse(array(
	"base_url"				=> DIR_WS_ADMIN,
	"base_public"			=> DIR_WS_PUBLIC,
	"child_disabled_list"	=> implode("|",$CHILD_CREATION_DISABLED_LIST),
	"delete_disabled_list"	=> implode("|",$DELETE_DISABLED_LIST),
	"base_content_id"	=> HOME_BASE_ID,
	"base_news_id"		=> NEWS_BASE_ID,
	"base_product_id"	=> PRODUCT_BASE_ID,
	"onload"					=> (($id>0)?"var old=onload;onload=function(){old();initTree(".$id.");}":""),
	"menu"						=> $menu->getMenuHTML(), 
	"menujs"					=> $menu->getMenuJS()
	)
);
$tpl->output();
?>