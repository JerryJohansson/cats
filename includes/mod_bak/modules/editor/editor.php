<?php
authenticate('admin');
require(DIR_WS_CLASSES."template.php");
require(DIR_WS_CLASSES."ezweb.php");
require(DIR_WS_CLASSES."all.php");
require(DIR_WS_CLASSES."users.php");


	$action=$_REQUEST['action'];
	$id=$_REQUEST['id'];
	$pid=$_REQUEST['pid'];
	$type = (isset($_REQUEST['type']) && !empty($_REQUEST['type']))?$_REQUEST['type']:'';
	$class_pagetype=($type=='')?'Container':ucwords($type);
	
	if(empty($type)){
		switch($action){
			case 'add':
				if(isset($pid)){
					$allowed_types = ezw_get_allowed_pagetype($pid);
					if(is_array($allowed_types)){
						$allowed=split(",",$allowed_types[0][1]);
						if(count($allowed)>1){
							$tpl = new Template("templates/choose_pagetype.htm");
							
							$values = array( 
								"id"				=> $id,
								"pid"				=> $pid,
								"action"		=> $action,
								"type"			=> ezw_draw_pull_down_menu('type', ezw_get_pull_down_options("select name, insert(`name`,1,1,substring(upper(name),1,1)) from ".TABLE_NODE_TYPE." where id in(".implode(",", $allowed).") and status > 0 "), $type, (($action=="edit")?"disabled":""))
							);
							$tpl->parse($values);
							$pagetype_content=$tpl->get();
							$tpl = new Template("templates/no_hdr_main.htm");
							$values = array("main"=>$pagetype_content);
							$tpl->parse($values);
							exit($tpl->get());
						}else{
							$class_pagetype=ucwords(ezw_get_pagetype($allowed[0]));
						}
					}else{
						$class_pagetype=ucwords(ezw_get_parent_pagetype($pid));
					}
				}
				break;
			case 'edit':
				if(isset($id)){
					$class_pagetype=ucwords(ezw_get_parent_pagetype($id));
				}
				break;
			default:
				ezw_redirect("admin.php?mode=$action");
				break;
		}
	}
	
	if(empty($id)) $id=0;
	$container = new $class_pagetype($id);
	
	$user_group_mask=$_SESSION['user_details']['groups'];
	$user_role_mask=$_SESSION['user_details']['roles'];
	$theme				= TEMPLATE_DEFAULT_GROUP;

	if($action=="add")
	{
		$content = "<p>Insert body text here</p>";
		$title = "";
		$extras = "";
		$container->load($pid);
		$level 				= $container->getLevel();
		$type 				= (isset($_REQUEST['type']) && !empty($_REQUEST['type']))?$_REQUEST['type']:get_class($container);
		$date 				= ezw_db_getdate('',false,'j-M-Y');
		$edate 				= $date;
		$extras 			.= $container->getExtraAdd();
		$status 			= $container->getStatus();
		
		$template 		= $container->getTemplate();
		$file					= "";
		$thumb				= "";
		$theme				= TEMPLATE_DEFAULT_GROUP;
		
		if(empty($pid) || $pid == 0)
		{
			// can only add children to base node
			if(in_array(strtolower($type), $CHILD_CREATION_DISABLED_LIST)){ 
				eval("\$pid = ".strtoupper($type)."_BASE_ID;");
			}
			if(empty($pid) || $pid == 0) $pid = HOME_BASE_ID;
		}
		
	}
	elseif($action=="edit")
	{
		$container->load($id);
		$extras 			.= $container->getExtraEdit();
		$content			= stripslashes($container->getContent());
		$title				= $container->getTitle();
		$keywords			= $container->getKeywords();
		$description	= $container->getDescription();
		$type					= get_class($container);
		$pid					= $container->getPid();
		$status				= $container->getStatus();
		$files				= $container->getFileLink("admin");
		$template			= $container->getTemplate();
		$file					= $container->getFile();
		$thumb				= $container->getThumb();
		$theme				= $container->getTheme();
		
		// Format the date
		$date = ezw_db_getdate($container->getDate(),false,'j-M-Y');
		$edate = ezw_db_getdate($container->getEndDate(),false,'j-M-Y');
	}
	elseif($action=="del") // Show the delete confirmation
	{
		$ezweb = new Ezweb;
		if($id)
			$ezweb->postDelete($id);
		else
			echo("No page to delete");
	}
	else
	{
		ezw_redirect("admin.php");
	}
	$containerType = ucwords((string)get_class($container));
	$iTabs = 0; // Tab counter
	
	if(strtolower($containerType)=="url"){
		$url_extras = $extras;
		$extras = '';
	}
	$publisher_id = $container->getPublisherId();
?>
<html>
<head>
<title>Editing:</title>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)"> 
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.3)"> 
<link rel="stylesheet" type="text/css" href="<?php echo DIR_WS_PUBLIC; ?>templates/<?PHP echo $theme; ?>/classes.css" title="ed">

<script type="text/javascript">
  _editor_url = "<?php echo DIR_WS_ADMIN; ?>";
	_editor_src_url = "<?php echo DIR_WS_ADMIN; ?>";
	_editor_base_url = "<?PHP echo DIR_WS_PUBLIC; ?>";
  _editor_lang = "en";
	var EDITOR_VALIDATE = "";
	function element(s){return document.getElementById(s);}
</script>
<?php
if(strtolower($containerType)!="url"){
?>
<script type="text/javascript" src="js/htmlarea.js"></script>
<script type="text/javascript" src="js/getstyles.js"></script>
<?php
}
?>
<script type="text/javascript" src="js/global.js"></script>
<script type="text/javascript" src="js/editor.js"></script>
<script type="text/javascript">
<?php
if(strtolower($containerType)!="url"){
?>
HTMLArea.loadPlugin("CSS");
HTMLArea.loadPlugin("ContextMenu");
HTMLArea.loadPlugin("TableOperations");
HTMLArea.loadPlugin("ImageManager");
<?php
}
?>
var editor = null;

onload = function(){
<?php
if(strtolower($containerType)!="url"){
?>
	initEditor('<?PHP echo $theme; ?>');
<?php
}
?>
	initTabs();
	setFocus();
	document.onmousedown=function(){
		_hide_calendar();
	}
	setTimeout(_hide_message,1000);
}

onresize = function(){
	var height = getAvailabelHeight('content');
	if (editor.config.sizeIncludesToolbar) {
			// substract toolbar height
			height -= editor._toolbar.offsetHeight;
			height -= editor._statusBar.offsetHeight;
	}
	editor._iframe.style.height = height+"px";
}

function _show_message(s){
	var o=element('loading_message');
	o.innerHTML='<table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2>'+s+'</h2></td></tr></table></td></tr></table>'
	//o.style.display='block';
	o.style.visibility = '';
	_show_hide_elements('select',false);
}
function _hide_message(){
	element('loading_message').style.visibility='hidden';
	_show_hide_elements('select',true);
}
function _show_hide_elements(tag_name, b_show){
	var sel=document.getElementsByTagName(tag_name);
	var i=0;
	var len=sel.length;
	var display = (b_show)?'':'none';
	for(i=0;i<len;i++){
		sel[i].style.display = display;
	}
}
function _preview_page(url){
	var win=window.open(url,"_preview_url","width=800, height=600, resizeable=1, scrollbars=1");
	win.focus();
}


function setPublished(item){
	var pub = <?php echo MASK_PUBLISHED; ?>;
	var f = document.forms.form;
	var status = parseInt(f.status.value);
	
	if(item.checked==true && (status & pub) == 0)
		status += pub;
	
	if(item.checked==false && (status & pub)>0)
		status -= pub;
	
	f.status.value = status;
}
function new<?PHP echo ucwords((string)$containerType);?>(){
<?php 
// Check if we can add children
if(in_array(strtolower($containerType), $CHILD_CREATION_DISABLED_LIST)){ 
	eval("\$disabled_base_id = ".strtoupper($containerType)."_BASE_ID;");
	$disabled_base_id = '"'.$disabled_base_id.'"';
}else{
	$disabled_base_id = 'f.id.value';
}
?>
	var f = document.forms.form;
	f.pid.value = <?php echo $disabled_base_id; ?>;
	f.id.value = "";
	f.action.value = "add";
	f.setAttribute("action","editor.php?action=add");
	f.Submit.click();
}
function save<?PHP echo ucwords((string)$containerType);?>(){
	var f = document.forms.form;
	f.Submit.click();
}
function doCancel(){
	location.href='admin.php';
}
function doDelete(){
	var f = document.forms.form;
	if(confirm("Are you sure you want to delete this page?\nWarning: Any children of this page will no longer be accessible!")){
		f.setAttribute("action","editor.php?action=del");
		f.Submit.click();
	}
}
if(!top.ezw_clipboard) top.ezw_clipboard=new ezw_clipboard();
function ezw_clipboard(){
	var args=arguments;
	this.clipboard = [];
	this.push = function(a){
		this.clipboard[this.clipboard.length] = a;
	}
	this.get = function(i){
		return this.clipboard[i];
	}
}
function copy(){
	var a=getFilesList();
	var s="";
	for(i=0;i<a.length;i++){
		a[i]="<a href=\""+a[i]+"\">"+a[i]+"</a>";
	}
	//alert(a.join("<br>"));
	top.ezw_clipboard.push(a);
}
function getFilesList(){
	var o=document.forms[0]["delfile[]"];
	var ln=[];
	if(o.length){
		for(i=0;i<o.length;i++){
			if(o[i].checked) ln[ln.length]=o[i].value;
		}
	}else ln[0]=o.value;
	return ln
}
</script>
<style type="text/css">
textarea { background-color: #fff; border: 1px solid #000000; width: 100% !important; }
a.indent { border: 2px inset; background-color: #999999; font-weight:bold; }
</style>
<link rel="stylesheet" type="text/css" href="css/toolbars.css">

<script src="js/validation.js" language="javascript1.2" type="text/javascript"></script>
</head>
<body class="tb">
<div id="loading_message" style="background-color:#CCCCCC;width:100%;height:100%;position:absolute;left:0px;top:0px;"><table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2 class="loading_message">Loading...</h2></td></tr></table></td></tr></table></div>
<form name="form" action="post.php?action=<?PHP echo $action; ?>" method="POST" enctype="multipart/form-data">
<fieldset class="bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $containerType;?>"
	href="#" onclick="new<?PHP echo $containerType;?>();" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/new.gif);">New <?PHP echo $containerType;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $containerType;?>"
	href="#" onclick="save<?PHP echo $containerType;?>();" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/save.gif);">Save <?PHP echo $containerType;?></a>
<a
	title="Cancel operation"
	href="#" onclick="doCancel();" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/cancel.gif);">Cancel</a>
<a
	title="Delete current <?PHP echo $containerType;?>"
	href="#" onclick="doDelete();" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/delete.gif);">Delete</a>	
<div style="padding-top:3px;padding-left:5px;float:left;">
	<label for="id_status">Status:</label><?PHP 

// check for a published doc
$date_published = ezw_db_getdate($container->getDatePublished(),false,'j-M-Y H:i');
$published_filter = "";
if(!empty($date_published)){
	$published_filter = " and (status_sub_type is null or status_sub_type = 'published') ";
}
echo ezw_draw_pull_down_menu('status', ezw_get_pull_down_options("select status_mask, status_title from status_values where status_type='node_mask' $published_filter and (group_mask = 0 or (group_mask & ".$user_group_mask.")>0) and status > 0 "), $status);
	?>
<?php
if(($user_role_mask & 4)>0 && ($user_role_mask & 8)==0 ){
?>
	<label for="id_publisher_id">Approver:</label><?PHP echo ezw_draw_pull_down_menu('publisher_id', ezw_get_pull_down_options("select u.user_id, concat(d.firstname,' ',d.lastname) as name from user u, user_details d where u.user_id=d.user_id and (u.roles & 8)>0 and (u.status & ".(MASK_HIDDEN+MASK_DELETED+MASK_ARCHIVED).") = 0 "), $publisher_id);?>
<?php 
}
// show published date
if($action!='add'){
	if(!empty($date_published)){
?>
	<label for="id_published">Published on: <?php echo($date_published);?></label>
<?php 
	}
}
?>
</div>
</fieldset><div id="Lcontentbody">
<input type="hidden" name="id" value="<?PHP echo $id; ?>">
<input type="hidden" name="pid" value="<?PHP echo $pid; ?>">
<input type="hidden" name="type" value="<?PHP echo $type; ?>">

<input type="hidden" name="theme" value="<?PHP echo $theme; ?>">
<input type="hidden" name="thumb" value="<?PHP echo $thumb; ?>">
<fieldset class="bar" id="tab_buttons">
<?php
if(strtolower($containerType)!="url"){
?>
<a
	title="Edit document"
	id="Editor"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/edit.gif);">Editor</a>
<?php }else{ ?>
<a
	title="Edit Hyperlink details"
	id="Editor"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/url.gif);">Hyperlink</a>
<?php } ?>
<?PHP if($extras != '') { ?><a
	title="Edit <?PHP echo $containerType;?> Details"
	id="Extras" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(templates/<?PHP echo $theme . '/images/icons/' . strtolower($containerType); ?>.gif);"><?PHP echo $containerType;?></a><?PHP } ?>
<a
	title="Edit Extended Properties"
	id="Properties" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/properties.gif);">Properties</a>
<a
	title="Edit Permissions For this page"
	id="Security" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(templates/<?PHP echo $theme; ?>/images/icons/security.gif);">Security</a>
</fieldset>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td class="label"><label for="id_title">Title:</label></td>
	<td><input class="sfield" name="title" id="id_title" type="text" size="50" value="<?PHP echo $title; ?>"></td>
</tr>
<?php if(strtolower($containerType)!="url"){ ?>
<tr>
	<td class="label"><label for="id_template">Template:</label></td>
	<td>
		<select class="sfield" name="template" id="id_template" onchange="document.getElementById('template_description').innerHTML=this.options[this.selectedIndex].getAttribute('desc')">
			<?PHP 
			ezw_db_connect();
			$node_type_id = ezw_db_getfieldvalue("select id from node_type where name ='$type'", "id");
			$result = ezw_db_query("select * from template where (type='0' or type='$node_type_id') and group_name = 'Default' order by group_name") or die(ezw_db_error());
			while ($row = ezw_db_fetch_array($result))
			{
				if($tpl_group_name != $row['group_name']){
					if($tpl_group_name) echo('</optgroup>');
					echo('<optgroup label="'.$row['group_name'].'" title="'.$row['group_name'].'">');
					$tpl_group_name = $row['group_name'];
				}
				echo('<option value="'.$row['id'].'" desc="'.$row['description'].'" '.(($row['id']==$template)?"selected":"").'>'.$row['name'].'</option>');
			}
			ezw_db_close();
			?>
		</select><span id="template_description" style="padding: 0px 0px 0px 10px"></span>
	</td>
</tr>
<?php }else{ ?>
<input name="template" type="hidden" value="<?PHP echo $template; ?>">
<? } ?>
<tr>
	<td class="label"><label for="id_date">Start Date:</label></td>
	<td><?php echo(ezw_get_input_start_end_dates('date',$date,'edate',$edate)); ?></td>
</tr>
<?php
if(strtolower($containerType)=="url"){
?>
<tr>
	<td class="label"><label></label></td>
	<td>&nbsp;</td>
</tr>
<?php
	echo $url_extras;
	$edit_style = 'style="display:none"';
}
?>
<tr>
	<td colspan="2" align="center">
			<table width="100%" bordercolor="#9C9A9C" border="1" cellpadding="0" cellspacing="0"><tr bordercolor="#FFFFFF"><td>
			<textarea <?php echo $edit_style; ?> name="content" id="content"><?PHP echo $content; ?></textarea>
			</td></tr></table>
	</td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="Submit" value="Save" onclick="return validateForm(this,EDITOR_VALIDATE);"><input type="button" name="Cancel" value="Cancel" onclick="doCancel();">
</fieldset>
</fieldset>

<?PHP 
// Adding Extra content tab
if($extras!='') { 
?>
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<?PHP echo $extras; ?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveExtras" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<?PHP } ?>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="templates/<?PHP echo $theme; ?>/images/forms/info.gif" /></td>
			<td>
				<p>For best results with search engines please choose keywords and phrases
					that have relavance to the content of the document:</p>
					<?php if(strtolower($containerType)!="url"){ ?>
				<ul>
					<li><b>Adding Related Documents:</b> Click the Add/Remove Related Documents
						button. When the file browser appears, click on the files you
						wish to add to the list. Click the OK button when your are done.</li>
					<li><b>Removing Related Documents :</b> Click the Add/Remove Related Documents
						button. When the file browser appears, select a file from the
						Selected Files list and click the Remove Item button (<img src="plugins/ImageManager/img/edit_trash.gif" width="15" height="15">)
						to remove the selected item. Click the OK button when your are
						done.</li>
					</ul>
					<?php } ?>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label for="id_keywords">Keywords:</label></td>
	<td><textarea class="sfield" name="keywords" id="id_keywords" rows="2" style="width:98%;"><?PHP echo $keywords; ?></textarea></td>
</tr>
<tr>
	<td class="label"><label for="id_description">Meta Description:</label></td>
	<td><textarea class="sfield" name="description" id="id_description" rows="2" style="width:98%;"><?PHP echo $description; ?></textarea></td>
</tr>
<?php if(strtolower($containerType)!="url"){ ?>
<tr valign="top">
	<td class="label">
		<label for="id_userfile">Related Files:</label>
	</td>
	<td>
		<div id="addDocs"><?php echo $files; ?></div>
		<input type="button" value="Add/Remove Related Document" title="Click here if you wish to add more documents" onclick="editor._insertDocument(this.form.file,element('addDocs'),'multi');" />
		<label for="id_userfile" style="display:inline;float:left;font-weight:normal">To attach related documents to this page, Click the browse button below and select a file to upload.</label>
		<input name="file" type="hidden" value="<?php echo $file; ?>" />
	</td>
</tr>
<?php }else{ ?>
<input name="file" type="hidden" value="<?php echo $file; ?>" />
<?php } ?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc; border-top:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="templates/<?PHP echo $theme; ?>/images/forms/info.gif" /></td>
			<td>
				<p><b>Viewing Security:</b> - If you would like to make this page accessible only to logged in members or certain User Groups from the following list, then select 1 or more User Groups from the list using ctrl+click to select multiple items.</p>
				<?php if($_SESSION['user_details']['groups'] & EZW_ADMIN_MASK) { ?>
				<p><b>Editing Security:</b> - If you would like to restrict who edits this page, then select 1 or more User Roles from the list using ctrl+click to select multiple items.</p>
				<? } ?>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label for="id_user_group" title="Select the user groups this page is allowed to be viewed by">Viewing Permissions:</label></td>
	<td><?PHP 
	ezw_db_connect();
	$node_groups = ezw_db_getfieldvalue("select groups from node where id ='$id'", "groups");
	$users = new User;
	echo $users->get_user_groups_list($node_groups); ?></td>
</tr>
<?php if($_SESSION['user_details']['groups'] & EZW_ADMIN_MASK) { ?>
<tr>
	<td class="label"><label for="id_user_role" title="Select the user roles this page is allowed to be edited by">Editing Permissions:</label></td>
	<td><?PHP 
	$node_roles = ezw_db_getfieldvalue("select roles from node where id ='$id'", "roles");
	echo $users->get_user_roles_list($node_roles);
	ezw_db_close();
	?></td>
</tr>
<?php }else{ ?>
<input name="roles" type="hidden" value="<?php echo ezw_db_getfieldvalue("select roles from node where id ='$id'", "roles"); ?>" />
<?php } ?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveSecurity" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
</form>
</div>
</body>
</html>