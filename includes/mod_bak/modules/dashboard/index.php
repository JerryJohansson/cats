<?PHP
//include('testing.php');
//exit;

require_once(CATS_ADODB_PATH . 'adodb-pager.inc.php');

$search_filter = array(
	'name'=> array('Open Actions Managed By me','Open Actions Allocated To me'),
	'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "
);
?>
<link rel="stylesheet" href="style/forms.css" type="text/css" />
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script language="JavaScript" src="js/classes/menu/popup.js"></script>
<script language="JavaScript" src="js/classes/helper/helper.js"></script>
<script language="JavaScript" src="js/validation.js"></script>
<script language="JavaScript" src="js/calendar.js"></script>
<script>
document.onmousedown = function(evt){
	if(ACTIVE_HELPER && !_in(ACTIVE_HELPER.parentNode.parentNode,_target(evt))) ACTIVE_HELPER.style.display='none';
	if(top.doMenu) top.doMenu();
}
function _target(e){
	return (document.all)?event.srcElement:e.target;
}
function _in(cont,o){
	var e=o,_ret=false;
	while(e!=document){
		_ret=(e==cont);
		if(_ret) {break;}
		else e=e.parentNode;
	}
	return _ret;
}
//=====================================================================================================================
//
//=====================================================================================================================
//
//=====================================================================================================================
/*REF_EMPLOYEES = 'employees';
var CATS_MENU=null;
var bWRITE_MODE=true;*/
function get_employees(name){
	var s=_get_dd_select({'name':name,'onchange':"alert('what the????')"},"");
	document.write(s);
	var id = (arguments[1])?arguments[1]:1;
	var el=element(name);
	remote.get_options_add({'name':name,'type':'text','value':''},el,'get_employee_array','users',id);
}

function _get_dd_select(o,soptions){
	var name=o.name;
	var id=(typeof(o.id)=="string")?o.id:o.name;
	var onchange=(typeof(o.onchange)=="string")?' onchange="'+o.onchange+'" ':"";
	var onfocus=(typeof(o.onfocus)=="string")?' onfocus="'+o.onfocus+'" ':"";
	var onblur=(typeof(o.onblur)=="string")?' onblur="'+o.onblur+'" ':"";
	var attribs=(typeof(o.attributes)=="string")?' '+o.attributes+' ':"";
	return '<select name="'+name+'" id="'+id+'" '+onchange+onfocus+onblur+attribs+' >\n<option></option>\n'+soptions+'</select>\n';
}

function _get_dd_options(adata,selected,oatt){
	var i,checked,s="";
	for(i=0;i<adata.length;i+=2){
		checked = (selected == adata[i])?"selected":"";
		s+="<option value=\""+adata[i]+"\" "+checked+" >"+adata[i+1]+"</option>\n";
	}
	//alert(s);
	return s;
}



var win=window;
function dummy(){}
function init(){
	// start up script for each page
	//
	// Execute some start up script
	return true;
}


</script>
</head>
<body>
<form><table>
<tr valign="top">
	<td width="60%">		
<?php
//print_r($_SESSION);


if(!isset($_SESSION['user_details']['show_welcome_message'])){
	$_SESSION['user_details']['show_welcome_message']='done';
?>
	
		<!-- Show a Welcome message when user first logs in -->
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onClick="this.blur();">Welcome to CATS</a></caption>
		<tr>
			<td>
				<p>Welcome back <?PHP echo $_SESSION['user_details']['first_name'];?>, you last logged into the system on <?php echo $_SESSION['user_details']['lastlogdate'];?> at <?php echo $_SESSION['user_details']['lastlogtime'];?>hrs. If this time is incorrect please contact your <a href="mailto:<? echo $_SESSION['user_details']['email'];?>">website administrator</a>. You have logged into the system 22 times.</p>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
		</fieldset>
		
<?php
} // END:: if SESSION['show_welcome_message'] 
?>
		<!-- Workload Review Table -->
<?php
// Display workload review items

$employee_number = 90999;//$_SESSION['user_details']['user_id'];
$colattribs = array('',' width="60" ',' width="180" ',' width="150" ',' width="20" ');
$colarr=array('Action Title','Action No.','Scheduled Date','Action Origin','edit:index.php?m=action&p=edit&id=:ACTION_ID');
//$sql="SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, 'edit:index.php?m=action&p=edit&id=:ACTION_ID' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id,Allocated_To_Id) ORDER BY Scheduled_Date ASC";
$asql=array(
	"SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, 'edit:index.php?m=action&p=edit&id=:ACTION_ID' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Managed_By_Id) ORDER BY Scheduled_Date ASC",
	"SELECT DISTINCT Action_Title, Action_Id, Scheduled_Date, Register_Origin, 'edit:index.php?m=action&p=edit&id=:ACTION_ID' as edit FROM View_Action_Details WHERE Status='Open' AND $employee_number in (Allocated_To_Id) ORDER BY Scheduled_Date ASC"
);
$idx=0;
foreach($asql as $sql) db_render_pager($sql,$colarr,$colattribs,$search_filter['name'][$idx++]);
?>
	</td>
	<td>
<?php
// Message of the Day
$sql = "SELECT * FROM View_MOTD_Current WHERE MOTD_Type = 0 AND MOTD_Site LIKE '%" . $_SESSION['user_details']['site_id'] . "%'";
$rs = $db->Execute($sql);
if($rs->RecordCount()){
?>		
		<!-- Message Of The Day -->
		
		<fieldset class="tbar" style="margin-bottom:10px;">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onClick="this.blur();">Message of the Day</a></caption>
<?php
	// Message of the day body table
	$idx = 0;
	while ($arr = $rs->FetchRow()) {
		$link = (!empty($arr['MOTD_URL']))?"<div align=\"left\">Link:<a href=\"".$arr['MOTD_URL']."\" target=\"_blank\">".$arr['MOTD_SHORT']."</a></div>":"";
		$motd = (!empty($arr['MOTD_DESC']))?$arr['MOTD_DESC']:"No message today";
		$date = $arr['MOTD_STARTDATE'];
		$tr_hilite = ($idx % 2)?' bgcolor="#eeeeee" ':' bgcolor="$ffffff" ';
		$idx++;
?>
		<tr <?php echo $tr_hilite;?>>
			<td>
				<div align="justify"><?php echo $motd; ?></div>
				<?php echo $link;?>
				<div align="left"><?php echo $date;?></div>
			</td>
		</tr>
<?php
	} // END:: rs while loop
?>
		</table>
		</fieldset>
		
<?php
} // END:: rs->RecordCount()


// Links of Interest
$sql = "SELECT * FROM View_MOTD_Current WHERE MOTD_Type = 1 AND MOTD_Site LIKE '%" . $_SESSION['user_details']['site_id'] . "%'";
$rs = $db->Execute($sql);
if($rs->RecordCount()){
?>
		<!-- links of interest -->
		
		<fieldset class="tbar" style="margin-bottom:10px;">
		<table class="admin" border="0" cellspacing="2" cellpadding="0" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onClick="this.blur();">Links of Interest</a></caption>
<?php
	// Links of Interest table body
	$idx = 0;
	while ($arr = $rs->FetchRow()) {
		$link = (!empty($arr['MOTD_URL']))?"<div align=\"left\">Link:<a href=\"".$arr['MOTD_URL']."\" target=\"_blank\">".$arr['MOTD_SHORT']."</a></div>":"";
		$motd = (!empty($arr['MOTD_DESC']))?$arr['MOTD_DESC']:"No links today";
		$date = $arr['MOTD_STARTDATE'];
		$tr_hilite = ($idx % 2)?' bgcolor="#eeeeee" ':' bgcolor="$ffffff" ';
		$idx++;
?>
		<tr <?php echo $tr_hilite;?>>
			<td>
				<?php echo $link;?>
				<div align="justify"><?php echo $motd; ?></div>
			</td>
		</tr>
<?php
	} // END:: rs while loop
?>
		</table>
		</fieldset>
<?php
} // END:: rs->RecordCount()
?>		
		<!-- User Details Table -->
		
		<fieldset class="tbar" id="right_panel">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onClick="this.blur();">My Details</a></caption>
		<tr>
			<td class="label"><label for="id_userid">Employee ID:</label></td>
			<td><?PHP echo $_SESSION['user_details']['user_id'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_username">Name:</label></td>
			<td><?PHP echo $_SESSION['user_details']['first_name']." ".$_SESSION['user_details']['last_name'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_email">Email:</label></td>
			<td><?PHP echo $_SESSION['user_details']['email'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_site">Site:</label></td>
			<td><?PHP echo $_SESSION['user_details']['site_name'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_dep">Department:</label></td>
			<td><?PHP echo $_SESSION['user_details']['department'];?></td>
		</tr>
		<tr>
			<td class="label"><label for="id_pos">Position:</label></td>
			<td><?PHP echo $_SESSION['user_details']['position'];?></td>
		</tr>
		</table>
		</fieldset>
		<fieldset class="tbar" style="margin-bottom:10px;">
		<div align="right" class="bar">
			<a
			title="Edit My Account Details"
			href="index.php?m=employee&p=edit&id=<?php echo $_SESSION['user_details']['user_id'];?>"
			style="background-image:url(<?php echo WS_ICONS_PATH;?>nav_my_account.gif);float:right;">My Account</a>
		</div>
		</fieldset>
		
	</td>
</tr>
</table>
</form>