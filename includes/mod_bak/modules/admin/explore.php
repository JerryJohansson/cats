<?php
/*
+--------------------------------------------------------------------------
|   ezwebmaker CMS Admin - Event Handler
|   ========================================
|   by Vernon Laskey
|   (c) 2004 Buzmedia
|   http://www.buzmedia.com.au
|   Email: vern@buzmedia.com.au
+---------------------------------------------------------------------------
*/
//error_reporting  (E_ERROR | E_WARNING | E_PARSE);
//require_once ("../../conf.php");
require(CATS_CLASSES_PATH . "template.php");
require(CATS_CLASSES_PATH . "navigation.php");
$menu = new Navigation("tree", "admin");

$action = (isset($_REQUEST['action']))?$_REQUEST['action']:'';
if(isset($_REQUEST['dir'])) $dir = $_REQUEST['dir'];
if(isset($_REQUEST['mode'])) $mode = $_REQUEST['mode'];
if(isset($_REQUEST['id_list'])) $id_list = $_REQUEST['id_list'];
if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
if(isset($_REQUEST['root'])) $root = $_REQUEST['root'];

// other e.g.
//$ADODB_FETCH_MODE = ADODB_FETCH_MODE;
//$db=db_connect();
switch($action){
	case "move": // Move node
		$menu->reorder($id,($action.$dir));
		main_redirect("index.php?m=admin&p=explore");
		break;
	case "paste":
		$menu->paste($mode,$id_list,$id);
		main_redirect("index.php?m=admin&p=explore");
		break;
	case "delete":
		$menu->delete($id);
		main_redirect("index.php?m=admin&p=explore");
		break;
}
if(!isset($root)) $root = 0;
$menu->setMenus($root);
$baseUriDir=isset($_SERVER['SCRIPT_NAME']) ? dirname($_SERVER['SCRIPT_NAME']) : dirname(getenv('SCRIPT_NAME'));
$values = array(
	"base_url"				=> WS_PATH . "includes/modules/admin/",
	"base_public"			=> WS_PATH,
	"onload"					=> (($id>0)?"var old=onload;onload=function(){old();initTree(".$id.");}":""),
	"menu"						=> $menu->getMenuHTML(), 
	"menujs"					=> $menu->getMenuJS()
);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head><title>Navigation | Form Builder Explorer</title>
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.6)"> 
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.3)">
<style>
#MENU_1_CONTAINER_0{margin-left: 5px;margin-top: 5px;}
BODY, DIV,.scrollbars, #scrollbars {
	scrollbar-3dlight-color: #ccc;
	scrollbar-arrow-color: #000;
	scrollbar-base-color: white;
	scrollbar-darkshadow-color: #666;
	scrollbar-face-color: #999;
	scrollbar-highlight-color: #666;
	scrollbar-shadow-color: #666;
	scrollbar-track-color: #ccc;
	border:none;
}
body, html {border:0px; margin:0px; padding: 0px; height:100%; width:100%;}
</style>
<script language="JavaScript" src ="/cats/js/global.js" type="text/javascript"></script>
<script>
<!--
var _base_editor_frame = "frmMain";
var _base_url = "<?php echo $values['base_url'] ;?>";
var _base_public_url = "<?php echo $values['base_public'] ;?>";
function focusValue(item){
  var s1=item.value, s2=item.getAttribute("val");
	if(s1==s2) item.value="";
	return true;
}
function blurValue(item){
	var s1=item.value, s2=item.getAttribute("val");
	if(s1=="") item.value=s2;
	return true;
}
function initTree(id){
	var itm=document.getElementById(id);
	if(aMenus[0]) {
		aMenus[0].style.backgroundColor = "";
		aMenus[0].style.color = "#000";
	}
	aMenus[0] = itm.firstChild.firstChild.nextSibling;
	aMenus[0].style.backgroundColor = aMenus[1];
	aMenus[0].style.color = aMenus[2];
	expandTree(itm);
}
function expandTree(item){
	vcoMenuSystemCollection[0].openNode(item);
	item=item.parentNode;
	if(item.nodeName=="DIV") expandTree(item);
}
<?php echo $values['onload'] ;?>
onload=function(){
	_createTreeMenu();
}
/*
alter table form_fields rename column ftype to field_type_id

insert into form_fields (select * from form_fields where field_id=1)

select formfields_seq.currval from dual

update form_fields set field_pid=1,field_name='action_search',field_group_type='search' where field_id=101
update form_fields set field_pid=1,field_name='action_edit',field_group_type='edit' where field_id=102
update form_fields set field_pid=101,field_name='action_edit',field_group_type='edit' where field_pid=1
select field_id, field_name, field_group_type from form_fields where field_id=1
*/
//-->
</script>
<?php echo $values['menujs'] ;?>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" oncontextmenu="return popupMenu(event);">
<?php echo $values['menu'] ;?>
<script>
function go(url){
	void(open(url,_base_editor_frame));
}
</script>
<div id="block_body">
<form method="get" style="display:none">
<input name="id">
<input name="root">
<input type="submit" name="Submit" value="try again">
</form>
</div>
</body>
</html>
<?php
$db=db_close();
?><?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>