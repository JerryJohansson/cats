<?php
require_once(CATS_ADODB_PATH . "tohtml.inc.php");
require_once(CATS_CLASSES_PATH . "template.php");
require_once(CATS_CLASSES_PATH . "users.php");
require_once(CATS_CLASSES_PATH . "navigation.php");
require_once(CATS_CLASSES_PATH . "edit.php");

/*
+------------------------------------------------------------------------
|  Comment: Show type selection if we are in a new page and the parent page item allows different types as children
+------------------------------------------------------------------------
*/
	$action=isset($_REQUEST['a'])?$_REQUEST['a']:'edit';
	$id=isset($_REQUEST['id'])?$_REQUEST['id']:1;
	$pid=isset($_REQUEST['pid'])?$_REQUEST['pid']:0;
	$type = (isset($_REQUEST['c']) && !empty($_REQUEST['c']))?$_REQUEST['c']:'';
	$class_pagetype=($type=='')?'Edit':ucwords($type);

	if(empty($type)){
		switch($action){
			case 'add':
				if(isset($pid)){
					$allowed_types = db_get_allowed_pagetype($pid);
					exit($allowed_types.":what the...");
					if(is_array($allowed_types)){
						$allowed=split(",",$allowed_types[0][1]);
						if(count($allowed)>1){
							$tpl = new Template("templates/choose_pagetype.htm");
							
							$values = array( 
								"id"				=> $id,
								"pid"				=> $pid,
								"action"		=> $action,
								"type"			=> html_draw_pull_down_menu('type', html_get_pull_down_options("select name, insert(`name`,1,1,substring(upper(name),1,1)) from form_types where id in(".implode(",", $allowed).") and status > 0 "), $type, (($action=="edit")?"disabled":""))
							);
							$tpl->parse($values);
							$pagetype_content=$tpl->get();
							$tpl = new Template("templates/no_hdr_main.htm");
							$values = array("main"=>$pagetype_content);
							$tpl->parse($values);
							exit($tpl->get());
						}else{
							$class_pagetype=ucwords(db_get_pagetype($allowed[0]));
						}
					}else{
						$class_pagetype=ucwords(db_get_parent_pagetype($pid));
					}
				}
				break;
			case 'edit':
				if(isset($id)){
					$class_pagetype=ucwords(db_get_parent_pagetype($id));
				}
				break;
			default:
				main_redirect("admin.php?mode=$action");
				break;
		}
	}
	
	if(empty($id)) $id=0;
	//$container = new $class_pagetype($id);
	//$container = new Edit($id,"admin");
	$user_group_mask=$_SESSION['user_details']['groups'];
	$user_role_mask=$_SESSION['user_details']['roles'];
	$extras = array("No results");
	$mode = "admin";

	if($action=="add")
	{
		$container = new Edit($pid,$mode);
		//$container->load($pid,$mode);
		if(empty($pid) || $pid == 0)
		{
			$pid = 0;
		}
		$extras = $container->getFormAdd();
	}
	elseif($action=="edit")
	{
		$container = new Edit($id,$mode);
		//$container->load($id,$mode);
		$extras = $container->getFormEdit();
	}
	elseif($action=="del") // Show the delete confirmation
	{
		$nav = new Navigation;
		$nav->delete($id);
	}
	else
	{
		main_redirect("index.php?m=admin&p=splash");
	}
	$containerType = ucwords((string)get_class($container));
	$iTabs = 0; // Tab counter
	$status = $container->getStatus();
	if(!((bool)$status)) $status=0;
	$page_title = "Editing:";
?>
<link rel="stylesheet" type="text/css" href="style/forms.css" />
<script type="text/javascript">
  _editor_url = "<?php echo dirname($_SERVER['SCRIPT_NAME']); ?>";
	_editor_src_url = "<?php echo WS_IMAGES_PATH; ?>";
	_editor_base_url = "<?PHP echo WS_PATH; ?>";
  _editor_lang = "en";
	var EDITOR_VALIDATE = "";
	function element(s){return document.getElementById(s);}
</script>
<?php
if(strtolower($containerType)=="wysiwyg"){
?>
<script type="text/javascript" src="js/htmlarea.js"></script>
<?php
}
?>
<script language="JavaScript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/editor.js"></script>
<script type="text/javascript" src="js/remote.js"></script>
<script type="text/javascript">
<?php
if(strtolower($containerType)=="wysiwyg"){
?>
HTMLArea.loadPlugin("CSS");
HTMLArea.loadPlugin("ContextMenu");
HTMLArea.loadPlugin("TableOperations");
HTMLArea.loadPlugin("ImageManager");
<?php
}
?>
var editor = null;

onload = function(){
<?php
if(strtolower($containerType)=="wysiwyg"){
?>
	initEditor('<?PHP echo $theme; ?>');
<?php
}
?>
	initTabs();
	setFocus();
	document.onmousedown=function(){
		if(typeof(_hide_calendar)=="function") _hide_calendar();
	}
	setTimeout(_hide_message,1000);
}
<?php
if(strtolower($containerType)=="wysiwyg"){
?>
onresize = function(){
	var height = getAvailabelHeight('content');
	if (editor.config.sizeIncludesToolbar) {
			// substract toolbar height
			height -= editor._toolbar.offsetHeight;
			height -= editor._statusBar.offsetHeight;
	}
	editor._iframe.style.height = height+"px";
}
<?php
}
?>
function _show_message(s){
	var o=element('loading_message');
	o.innerHTML='<table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2>'+s+'</h2></td></tr></table></td></tr></table>'
	//o.style.display='block';
	o.style.visibility = '';
	_show_hide_elements('select',false);
}
function _hide_message(){
	element('loading_message').style.visibility='hidden';
	_show_hide_elements('select',true);
}
function _show_hide_elements(tag_name, b_show){
	var sel=document.getElementsByTagName(tag_name);
	var i=0;
	var len=sel.length;
	var display = (b_show)?'':'none';
	for(i=0;i<len;i++){
		sel[i].style.display = display;
	}
}
function _preview_page(url){
	var win=window.open(url,"_preview_url","width=800, height=600, resizeable=1, scrollbars=1");
	win.focus();
}


function setPublished(item){
	var pub = <?php echo MASK_PUBLISHED; ?>;
	var f = document.forms.form;
	var status = parseInt(f.status.value);
	
	if(item.checked==true && (status & pub) == 0)
		status += pub;
	
	if(item.checked==false && (status & pub)>0)
		status -= pub;
	
	f.status.value = status;
}
function new<?PHP echo ucwords((string)$containerType);?>(){
	var f = document.forms.form;
	f.pid.value = f.id.value;
	f.id.value = "";
	f.action.value = "add";
	f.setAttribute("action","editor.php?action=add");
	f.Submit.click();
}
function save<?PHP echo ucwords((string)$containerType);?>(){
	var f = document.forms.form;
	f.Submit.click();
}
function doCancel(){
	location.href='admin.php';
}
function doDelete(){
	var f = document.forms.form;
	if(confirm("Are you sure you want to delete this page?\nWarning: Any children of this page will no longer be accessible!")){
		f.setAttribute("action","editor.php?action=del");
		f.Submit.click();
	}
}
if(!top.html_clipboard) top.html_clipboard=new html_clipboard();
function html_clipboard(){
	var args=arguments;
	this.clipboard = [];
	this.push = function(a){
		this.clipboard[this.clipboard.length] = a;
	}
	this.get = function(i){
		return this.clipboard[i];
	}
}
function copy(){
	var a=getFilesList();
	var s="";
	for(i=0;i<a.length;i++){
		a[i]="<a href=\""+a[i]+"\">"+a[i]+"</a>";
	}
	//alert(a.join("<br>"));
	top.html_clipboard.push(a);
}
function getFilesList(){
	var o=document.forms[0]["delfile[]"];
	var ln=[];
	if(o.length){
		for(i=0;i<o.length;i++){
			if(o[i].checked) ln[ln.length]=o[i].value;
		}
	}else ln[0]=o.value;
	return ln
}
</script>
<style type="text/css">
textarea { background-color: #fff; border: 1px solid #000000; width: 100% !important; }
a.indent { border: inset; background-color: #999999; font-weight:bold; }
</style>
<script src="/cats/js/validation.js" language="javascript1.2" type="text/javascript"></script>
</head>
<body class="tb">
<div id="loading_message" style="background-color:#CCCCCC;width:100%;height:100%;position:absolute;left:0px;top:0px;"><table height="100%" width="100%"><tr><td align="center" valign="middle"><table align="center"><tr><td><h2 class="loading_message">Loading...</h2></td></tr></table></td></tr></table></div>
<form name="form" action="?m=admin&p=action&a=<?PHP echo $action; ?>" method="POST" enctype="multipart/form-data">
<div id="tool_bar">
<fieldset class="bar">
<?php
if($action=="edit"){
?>
<a
	title="Create New <?PHP echo $containerType;?>"
	href="#" onclick="new<?PHP echo $containerType;?>();" 
	class="main_new"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/new.gif);">New <?PHP echo $containerType;?></a>
<?php
}
?>
<a
	title="Save <?PHP echo $containerType;?>"
	href="#" onclick="save<?PHP echo $containerType;?>();" 
	class="main_save"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/save.gif);">Save <?PHP echo $containerType;?></a>
<a
	title="Cancel operation"
	href="#" onclick="doCancel();" 
	class="main_cancel"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/cancel.gif);">Cancel</a>
<a
	title="Delete current <?PHP echo $containerType;?>"
	href="#" onclick="doDelete();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/delete.gif);">Delete</a>
<a
	title="Refresh current <?PHP echo $containerType;?> item"
	href="#" onclick="document.location.reload();" 
	class="main_delete"
	style="background-image: url(<?php echo(WS_STYLE_PATH); ?>images/icons/refresh.gif);">Refresh</a>	
</fieldset>
<fieldset class="bar" id="tab_buttons">
<?php
if($extras==''){
	$rs = db_query("select * from form_fields where field_pid=$id");
	$tab_buttons="";
	$tab_contents="";
	// loop through the tabs and create main tab buttons
	while($tab = $rs->FetchRow()){
		
		$name = $tab['FIELD_NAME'];
		// get a nice representation of the name e.g.
		// convert candy_apples_value -> CandyApplesValue
		$ucname = preg_replace("/\s/g","",ucwords(preg_replace("/_/g"," ",$name)));
		
		if($iTabs==0) $save_buttons = '<input type="submit" name="Submit" value="Save" onclick="return validateForm(this,EDITOR_VALIDATE);"><input type="button" name="Cancel" value="Cancel" onclick="doCancel();">';
		else $save_buttons = '<input type="button" name="Save'.$ucname.'" id="Editor" value="OK" onclick="showhide(this);">';
		$tab_buttons .= '
	<a
		title="'.$tab['LABEL'].' Details"
		id="Extras" class="indent"
		href="#" onclick="return tab_onclick(this);" 
		style="background-image: url(' . WS_STYLE_PATH . 'images/icons/' . strtolower($tab['NAME']) .'gif);">'.$tab['NAME'].'</a>';
		$tab_contents .= '
	<fieldset class="tbar" id="tab_panel['.$iTabs.']">
	<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
	'.$container->getTab($tab['TAB']).'
	</table>
	<fieldset class="tbar" style="text-align:right; ">'.$save_bttons.'
	</fieldset>
	</fieldset>
	';
		$iTabs++;
	}
	echo($tab_buttons.$tab_contents);
}else{
?>
<a
	title="Edit <?PHP echo $containerType;?> Details"
	id="Editor" class="indent"
	href="#" onclick="return tab_onclick(this);" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH . 'images/icons/' . strtolower($containerType); ?>.gif);"><?PHP echo $containerType;?> Properties</a>
<a
	title="Edit Permissions For this page"
	id="Security" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/security.gif);">Security</a>
<a
	title="Edit Extended Properties"
	id="Properties" class="indent"
	href="#" onclick="return tab_onclick(this)" 
	style="background-image: url(<?PHP echo WS_STYLE_PATH; ?>images/icons/properties.gif);">Help/User Guide</a>
</fieldset>
</div>
<div id="Lcontentbody">
<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<?PHP $container->drawForm($extras); ?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="submit" name="Submit" value="Save" onclick="return validateForm(this,EDITOR_VALIDATE);"><input type="button" name="Cancel" value="Cancel" onclick="doCancel();">
</fieldset>
</fieldset>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc; border-top:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="<?php echo(WS_STYLE_PATH); ?>images/forms/info.gif" /></td>
			<td>
				<p><b>Viewing Security:</b> - If you would like to make this item accessible only to logged in members or certain User Groups from the following list, then select 1 or more User Groups from the list using ctrl+click to select multiple items.</p>
				<?php if($_SESSION['user_details']['groups'] & CATS_SA_MASK) { ?>
				<p><b>Editing Security:</b> - If you would like to restrict who edits this item, then select 1 or more User Roles from the list using ctrl+click to select multiple items.</p>
				<? } ?>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label for="id_user_group" title="Select the user groups allowed to view this item">Viewing Permissions:</label></td>
	<td><?PHP 
	$sql="select group_mask from form_fields where field_id=".$id;
	$node_groups = db_get_one($sql);
	echo html_user_groups_list($node_groups); ?></td>
</tr>
<?php if($_SESSION['user_details']['groups'] & CATS_SA_MASK) { ?>
<tr>
	<td class="label"><label for="id_user_role" title="Select the user roles this page is allowed to be edited by">Editing Permissions:</label></td>
	<td><?PHP 
	$sql = "select role_mask from form_fields where field_id=$id";
	$node_roles = db_get_one($sql);
	echo html_user_roles_list($node_roles);
	//db_close();
	?></td>
</tr>
<?php
}
?>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveSecurity" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>

<fieldset class="tbar" id="tab_panel[<?PHP echo ''.($iTabs++); ?>]" style="display:none;">
<table	cellspacing="0" border="0" class="admin" width="100%" style="background: #fff;">
<tr>
	<td colspan="2" style="border-bottom:1px solid #ccc;">
		<table>
		<tr>
			<td><img src="<?php echo(WS_STYLE_PATH); ?>images/forms/info.gif" /></td>
			<td>
				<p>Enter help information and documentation for the user guide:</p>
				<ul>
					<li><b>Enter Help:</b> Describe the item and its purpose breifly.</li>
					<li><b>Enter User Guide :</b> Describe the item in detail making sure describe it's relationship with other items etc.</li>
				</ul>
				<p>&nbsp;</p>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td class="label"><label>Field Help</label></td><td><textarea class="sfield" name="FIELD_HELP"></textarea></td>
</tr>
<tr>
	<td class="label"><label>Field Help</label></td><td><textarea class="sfield" name="FIELD_USER_GUIDE"></textarea></td>
</tr>
</table>
<fieldset class="tbar" style="text-align:right; ">
<input type="button" name="SaveProperties" id="Editor" value="OK" onclick="showhide(this);">
</fieldset>
</fieldset>
<?php
} // fi extras=='';
?>
</div>
</form>
<?php include(CATS_INCLUDE_PATH . 'bootstrap_style.inc.php'); ?>