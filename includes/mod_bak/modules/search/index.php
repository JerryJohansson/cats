<?PHP
$filter = array(
	'id'=>'pcr_search', // matches id of tab object
	'name'=>'Search Results',
	'filter'=>" status = 'Open' and (Managed_By = 1 or Allocated_To = 1) "
);
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/default.css">
<link rel="stylesheet" type="text/css" href="css/toolbars.css">
<?php if ($_SESSION['msie']) {?>
<link href="css.php" rel="stylesheet" type="text/css" title="Extra Cool" id="main" />
<?php } ?>
<script language="JavaScript" src="constants.php"></script>
<script language="JavaScript" src="js/global.js"></script>
<script language="JavaScript" src="js/prototype.js"></script>
<script language="JavaScript" src="js/general.js"></script>
<script language="JavaScript" src="js/forms.js"></script>
<script language="JavaScript" src="js/classes/html.js"></script>
<script language="JavaScript" src="js/remote.js"></script>
<script>
//================================================================================//
//	XML Requests/Responses
//================================================================================//
var sendObj=[
['name','Vernon Laskey'],
['address','11/20 Queens Cr'],
['email','vern@buzmedia.com.au'],
['elements',[['div[0]','tag is a div'],['text[0]','node: is a textnode'],['div[1]','tag is a div 2'],['text[1]','node: is a textnode 2']]]
]
function alertMe(s){alert(s);return s;}
var oXML=_HTTPRequest("test_remote.php","post",return_function,alertMe(return_query(sendObj)));



function get_fields(page){
	var url = "_remote/get_fields.php";
	var data=null;
	if(top.tabs[page]&&typeof(top.tabs[page]['search'])=="string"){
		data=top.tabs[page];
	}else{
		try{
			var return_value = RSExecute(url,"getfields", page);
		}catch(e){
			var return_value = {'return_value':"{'search':'"+top.tabs[page]['search']+"','int':"+top.tabs[page]['int']+"}"};
		}
		return_value_dump=return_value.return_value;
		eval("data="+return_value.return_value+";");
		top.tabs[page]=data;
		// *
		var s = "";
		for(e in return_value) if(typeof return_value[e] != "function") s+=e+":"+return_value[e]+"\n";
		//alert("get_fields()\n\n"+page+"\n\n"+data+"\n\n"+return_value.return_value);
		//alert(data.string)
		//*/
	}
	return data;
}
return_value_dump = "";
function get_tables(page){
	var url = "_remote/get_fields.php";
	var sql = "select * from form_fields ";
	var data=null;
	if(top.tabs[page]&&typeof(top.tabs[page]['search'])=="string"){
		data=top.tabs[page];
	}else{
		//try{
			var return_value = RSExecute(url,"getfieldvalues", RSPrepareSQL(sql));
		//}catch(e){
			//var return_value = {'return_value':"{'search':'"+top.tabs[page]['search']+"','int':"+top.tabs[page]['int']+"}"};
		//}
		return_value_dump=return_value.return_value;
		eval("data="+return_value.return_value+";");//.split("|");
		top.tabs[page]=data;
		// *
		var s = "";
		for(e in return_value) if(typeof return_value[e] != "function") s+=e+":"+return_value[e]+"\n";
		//alert("get_fields()\n\n"+page+"\n\n"+data+"\n\n"+return_value.return_value);
		alert(s)
		//*/
	}
	return data;
}
function doit(){return;
	//o=get_tables('audits and inspections');
	top.tabs[T_NAME].toolbar=new top._toolbar();
	top.tabs[T_NAME].toolbar.add('Search');
	top.tabs[T_NAME].toolbar.add('Results');
	
}



function _edit(){
	//top.toolbar.add('Edit');
	location.href = 'edit.php';
//	top.tabs.add('audits and inspections');
}
T_NAME = 'audits_and_inspections';
function doMenu(){};
top.tabs=new Object();
top.tabs[T_NAME]=new Object();
function createscreen(){
	_msg_screen=new _screen_messages("loading_message");
	//alert(_msg_screen._doc.innerHTML);
	//alert(element(_msg_screen.name).innerHTML);
	_msg_screen.show("loading");
	setTimeout(_msg_screen.hide,5000);
}
</script>
</head>
<body onload="createscreen();top.doMenu();doit();">
<div id="loading_message" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; display: none"></div>
<table width="100%">
<tr valign="top">
	<td width="100%">

		<!-- xtra_templates -->
		
		<fieldset class="tbar">
		<table class="admin" border="0" cellspacing="0" cellpadding="2" width="100%" style="background:#fff;">
		<caption id="tog"><a name="todo" onclick="this.blur();"><?php echo $filter['name'];?></a></caption>
		
		<?php 
/*

	`name` varchar (128), 
	`title` varchar (128), 
	`type` varchar (50), 
	`length` INTEGER (11), 
	`value` text, 
	`columns` SMALLINT (6), 
	`mandatory` SMALLINT (6),
	
	TODO::
		eventually we want the following function to except the field string.
		*/
		$col_heads = array(
			"actions"=>"Action Title,Action No.,Scheduled Date,Action Origin,Edit",
			"audits_and_inspections"=>"Type,Date,Site,Department,Location,Edit,Del",
			"government_inspections"=>"Inspection Type,Site,Department,Location,Date Completed,Edit,Del",
			"incidents"=>"Incident No.,Date,Incident Category/s,Description,Status,Count,Closed,Edit,Del",
			"pcr_search"=>"PCR No.,Criticality,Manager,Site,Department,Date Entered,Open,Closed,Edit,Del",
			"default"=>"Type,Date,Site,Department,Location,Edit,Del"
			// ""=>",,,,,,,,,,",
		);
		$col_conf = array(
			"actions"=>"t,n,d,s,s",
			"audits_and_inspections"=>"s,d,s,s,s,s,s",
			"government_inspections"=>"s,s,s,s,d,s,s",
			"incidents"=>"n,d,s,s,n,n,n,s,s",
			"pcr_search"=>"p,x,u,a,b,d,n,n,s,s",
			"default"=>"s,d,s,s,s,s,s"
			// ""=>",,,,,,,,,,",
		);
		echo get_filter_rows('',10,explode(',',$col_heads[$filter['id']]),explode(',',$col_conf[$filter['id']]));
		
		?>
		
		</table>
		</fieldset>
		
		<?php
		//print_r($_SERVER);
		//print_r($_REQUEST);
		
		?>
		
	</td>
</tr>
</table>
<div id="html_content"></div>

</body>
</html>
<?php
//<script language="JavaScript" src="_remote/rs.js">< /script>
//<script language="JavaScript">RSEnableRemoteScripting("_remote");< /script>
function get_filter_rows($filter='',$x=20,$arr,$conf)
{
	//$row_type="actions";
	$table = array(
		"a"=>array('Kwinana','Cooljaloo','Chandala','Kwinana','Chandala','Kwinana','Cooljaloo','Chandala','Kwinana','Chandala'),
		"b"=>array('Department 1','Environment','Safety','Administration','Department 2','Department 4','Environment','Safety','Administration','Department 6'),
		"n"=>array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30),
		"t"=>array('Scheduled Actions: Reminder to document code','Create client-side html object code library',
					'Document code library','Create code library for database layer','Create page prototype',
					'Approval of page prototype','Create form entry pages','Create functional pages',
					'Create cron jobs for back end processing','Testing goes on'),
		"s"=>array('Schedule','Action','Email','PCR','Meeting','Email','PCR','Meeting','Schedule','Action'),
		"d"=>array('17/12/2004','18/12/2004','19/12/2004','22/02/2004','13/12/2004','14/11/2004','22/06/2004','24/12/2004','11/12/2004','12/09/2004'),
		"u"=>array('Smith, John','Wayne, Dwane','Johnson, Jimmy','Andrews, Andrew','Dimebag, Darrel','Van Halen, Eddie','Vai, Steve','Hendrix, Jimi','Archivex, Xaviera','Jaxon, Action'),
		"x"=>array('TC','NC','NC','TC','PC','PC','NC','TC','TC','NC'),
		"p"=>array('PI122','PI302','PI402','PI042','PI892','PI902','PP902','CO002','CO102','PI102')
	);
	$action_title = array('Scheduled Actions: Reminder to document code','Create client-side html object code library',
		'Document code library','Create code library for database layer','Create page prototype',
		'Approval of page prototype','Create form entry pages','Create functional pages',
		'Create cron jobs for back end processing','Testing goes on');
	$action_no = array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30);
	$scheduled_date = array('17/12/2004','18/12/2004','19/12/2004','22/02/2004','13/12/2004','14/11/2004','22/06/2004','24/12/2004','11/12/2004','12/09/2004');
	$action_origin = array('Schedule','Action','Email','PCR','Meeting','Email','PCR','Meeting','Schedule','Action');
	$action_type = array('Inspection','Audit','Audit','Audit','Inspection','Audit','Inspection','Audit','Inspection','Audit');
	$action_site = array('Kwinana','Cooljarloo','Chandala','Bently','Kwinana','Bently','Kwinana','Bently','Chandala','Bently');
	$action_department = array('Kwinana','Cooljarloo','Chandala','Bently','Kwinana','Bently','Kwinana','Bently','Chandala','Bently');
	$action_location = array('\\server\file.doc','\\server\file.doc','\\server\file.doc','\\server\file.exe','\\server\file.swf','\\server\file.jpg','\\server\file.doc','\\server\file.doc','\\server\file.bmp','\\server\file.doc');
	//$x=10;
	$s = '<tr>';
	for($i=0;$i<count($arr);$i++){
		$s .= '<th>'.$arr[$i].'</th>';
	}
	$s .= '</tr>';
	//$s .= '<tr><td>conf='.implode(",",$conf).'arr='.implode(",",$arr).'</td></tr>';
	//reset($arr);
	for($i=0;$i<$x;$i++){
		$s .= '<tr onclick="_row_down(this);" onmouseover="_row_over(this);" onmouseout="_row_out(this);">
		';
		for($j=0;$j<count($arr);$j++){
			if($j==0){
				$s .= '<td><a href="javascript:_edit();">'.$table[$conf[$j]][rand(0,9)].'</a></td>
				';
			}else if($arr[$j]=='Edit'){
				$s .= '<td><img src="icons/btn_show_bg.gif" border="0" onclick="_edit();" /></td>
				';
			}else if($arr[$j]=='Del'){
				$s .= '<td><img src="icons/btn_delete_bg.gif" border="0" onclick="_delete();" /></td>
				';
			}else if($arr[$j]=='New'){
				$s .= '<td><img src="icons/btn_new_bg.gif" border="0" onclick="_new();" /></td>
				';
			}else{
				$s .= '<td>'.$table[$conf[$j]][rand(0,9)].'</td>
				';
			}
		}
		$s .= '
		</tr>
		';
	}
	$cols = count($arr);
	$max = rand(30,60);
	$pages = (int)($max / $x)+1;
	$s .= '
		<tr style="background:buttonface;">
			<td colspan="'.$cols.'">
			<!--hr /-->
				<table border="0" width="100%" cellspacing="0" cellpadding="2">
				<tr>
					<td class="smallText" valign="top">Displaying <b>1</b> to <b>'.$x.'</b> (of <b>'.$max.'</b> actions)</td>

					<td class="smallText" align="right"><form name="pages" action="search.php" method="get">&lt;&lt;&nbsp;&nbsp;Page <select class="sfield" name="page" onChange="this.form.submit();">';
					for($i=1;$i<=$pages;$i++){
						$s .= '<option value="'.$i.'">'.$i.'</option>';
					}
					$s .= '</select> of '.$pages.'&nbsp;&nbsp;<a href="search.php?gID=2&page=2" class="splitPageLink" style="display:inline;">&gt;&gt;</a><input type="hidden" name="gID" value="2"></form></td>
				</tr>
				</table>
			</td>
		</tr>
	';
	return $s;
}
?>
<script>
/*
if(!document.all){
top.tabs['splash']={'type':0,'int':6,'search':'Splash_Goes_Here|s|3|3|1|0'};
top.tabs['actions']={'type':0,'int':3,'search':'Site|s|3|Department|s|64|Section|s|64|Action_Managed_By|n|16|Allocated_To|n|16|Managed_By_Or_Allocated_To|n|16|Type|s|50|Origin|s|50|Category|s|50|Status|s|50|Action_Number|n|16|Due_Date_From|d|10|Due_Date_To|d|10|Closed_Date_From|d|10|Closed_Date_To|d|10'};
top.tabs['audits and inspections']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|2|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|2|1|0|Audit_Or_Inspection_Type|s|64|2|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['government inspections']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Inspection_Type|s|64|2|1|0|Government_Departments|s|64|2|1|0|Location|s|128|Kwinana|1|0|Date_Completed_From|d|10|21/02/2005|2|0|Date_Completed_To|d|10|23/02/2005|2|0'};
top.tabs['incidents']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|3|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['major hazards register']={'type':1,'int':6,'search':'Site|s|3|Kwinana|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|1|0|Date_To|d|10|23/02/2005|1|0','obj':{'fields':['Name','Site','Department'],'names':['Vernon','Kwinana','Software']}};
top.tabs['meeting minutes']={'type':0,'int':3,'search':'Site|s|3|Department|s|64|Section|s|64|Action_Managed_By|n|16|Allocated_To|n|16|Managed_By_Or_Allocated_To|n|16|Type|s|50|Origin|s|50|Category|s|50|Status|s|50|Action_Number|n|16|Due_Date_From|d|10|Due_Date_To|d|10|Closed_Date_From|d|10|Closed_Date_To|d|10'};
top.tabs['other records']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|2|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|2|1|0|Audit_Or_Inspection_Type|s|64|2|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['schedules']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Inspection_Type|s|64|2|1|0|Government_Departments|s|64|2|1|0|Location|s|128|Kwinana|1|0|Date_Completed_From|d|10|21/02/2005|2|0|Date_Completed_To|d|10|23/02/2005|2|0'};
top.tabs['site specific obligations']={'type':0,'int':6,'search':'Site|s|3|3|1|0|Group|g|3|Checkbox1,c,3,Now,1,0,Checkbox2,c,3,Then,1,0,Radio1,r,3,Now,1,0,Radio2,r,3,Then,1,0|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|2|0|Date_To|d|10|23/02/2005|2|0'};
top.tabs['workload review']={'type':1,'int':6,'search':'Site|s|3|Kwinana|1|0|Department|s|64|Software|1|0|Section|s|64|Hanger 18|1|0|Date_From|d|10|21/02/2005|1|0|Date_To|d|10|23/02/2005|1|0','obj':{'fields':['Name','Site','Department'],'names':['Vernon','Kwinana','Software']}};


//,Date_From,d,10,31/12/2004,1,0,Date_To,d,10,31/12/2005,1,0|1|0
}


function render_pg(){
	alert("hello:\n\n"+arguments[0]);
}

function render(obj_string){
	IDX_DEFAULT=1;
	var obj_string_name = string_to_id(obj_string);
	var a = null;
	//if(obj_string!="workload review")
		a = get_fields(obj_string);
	//else
		//a = get_tables(obj_string);
	//alert("a=get_fields('"+obj_string+"')\n"+a['search'])
	var o=null;
	var obj=(typeof(a['array'])=="object")?a['array']:a['search'].split("|");
	var interval=a['int'];
	var type=a['type'];
	//alert(obj+":"+interval+":"+type)
	if(typeof(top.tabs[obj_string])!="object") top.tabs[obj_string] = a;
	top.tabs[obj_string]._html=new _html( obj, interval, "", type );
	var s=top.tabs[obj_string]._html.gen();
	try{
		o=parent.element(_menu_prefix+obj_string_name);
		menu=o.innerHTML;
	}catch(e){
		var tmp=top.document.createElement("DIV");
		tmp.setAttribute("id",_menu_prefix+obj_string_name);
		o=top.document.body.appendChild(tmp);
		tmp=null;
		tmp=top.document.createElement("A");
		tmp.className = "def";
		tmp.setAttribute("href","javascript:showMenu('"+obj_string_name+"','"+obj_string+"');");
		tmp=top.element("menu_history_list").appendChild(tmp);
		tmp.innerHTML = obj_string;
	}
	o.innerHTML = s;
	o.style.display = 'none';
	top.showMenu(obj_string_name);
	//alert(s);
	//alert(return_value_dump);
}

function _pg(){
	var args=arguments;
	this.html = (args.length>0)?args[0]:"";
	this.container = (args.length>1)?(typeof(args[1])=="string"?element(args[1]):args[1]):_default;
	this.container.innerHTML = this.html;
}




*/
</script>